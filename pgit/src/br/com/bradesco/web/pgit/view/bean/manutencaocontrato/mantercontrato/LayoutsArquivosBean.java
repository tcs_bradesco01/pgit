/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.AGENCIA_CONTA;
import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.REMESSA;
import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.REMESSA_TIPO_SERVICO_MODALIDADE;
import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.SELECIONE;
import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.SEM_SEPARACAO;
import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.TIPO_SERVICO;
import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.TIPO_SERVICO_MODALIDADE;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarLayoutsContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarLayoutsContratoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarLayoutsContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarNumeroVersaoLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarNumeroVersaoLayoutOcorrSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarNumeroVersaoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoArquivoRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarConfiguracaoLayoutArqContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarLayoutServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarLayoutServicoListEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarLayoutServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarTipoRetornoLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConfiguracaoLayoutArquivoContratoEntradaDto;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarLayoutArquivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarLayoutArquivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaLayoutArquivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaLayoutArquivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaTipoRetornoLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharHistoricoLayoutContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharHistoricoLayoutContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharTipoRetornoLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirTipoRetornoLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirTipoRetornoLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarHistoricoLayoutContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarHistoricoLayoutContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarTipoLayoutContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarTipoLayoutContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.selectitem.SelectItemUtils;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: LayoutsArquivosBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class LayoutsArquivosBean {

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo manterContratoImpl. */
    private IManterContratoService manterContratoImpl;
    // utilizado nos m�todos para navega��o para eliminar redund�ncia apontada
    // pelo Sonar
    /** Atributo VOLTAR_JSP. */
    private static final String VOLTAR_JSP = "VOLTAR";
    // utilizado nos m�todos addInfoModalMessage do BradescoFacesUtils para
    // eliminar redund�ncia apontada pelo Sonar
    /** Atributo RETORNO_LAYOUTS_ARQUIVOS. */
    private static final String RETORNO_LAYOUTS_ARQUIVOS = "selRetornoLayoutsArquivos";
    // utilizado nos m�todos para navega��o para eliminar redund�ncia apontada
    // pelo Sonar
    /** Atributo ALTERAR_JSP. */
    private static final String ALTERAR_JSP = "ALTERAR";
    // utilizado nos m�todos addInfoModalMessage do BradescoFacesUtils para
    // eliminar redund�ncia apontada pelo Sonar
    /** Atributo LAYOUTS_ARQUIVOS. */
    private static final String LAYOUTS_ARQUIVOS = "selLayoutsArquivos";

    // Tela Inicial - Layouts de Arquivos - selLayoutsArquivos
    /** Atributo listaLayout. */
    private List<ConsultarListaLayoutArquivoContratoSaidaDTO> listaLayout;

    /** Atributo listaControleLayout. */
    private List<SelectItem> listaControleLayout;

    /** Atributo itemSelecionadoListaLayout. */
    private Integer itemSelecionadoListaLayout;

    /** Atributo itemSelecionadoListaLayoutAltera. */
    private boolean itemSelecionadoListaLayoutAltera;

    // Atributos pagina selRetornoLayoutsArquivos
    /** Atributo listaRetorno. */
    private List<ConsultarListaTipoRetornoLayoutSaidaDTO> listaRetorno;

    /** Atributo listaControleRetorno. */
    private List<SelectItem> listaControleRetorno;

    /** Atributo itemSelecionadoListaRetorno. */
    private Integer itemSelecionadoListaRetorno;

    /** Atributo hiddenObrigatoriedade. */
    private String hiddenObrigatoriedade;

    // Novos Campos
    /** Atributo cdIntervaloGeracao. */
    private Integer cdIntervaloGeracao;

    /** Atributo cdTipoMontagem. */
    private Integer cdTipoMontagem;

    /** Atributo cdTipoOcorrenciasArquivo. */
    private Integer cdTipoOcorrenciasArquivo;

    /** Atributo cdTipoClassificacaoRetorno. */
    private Integer cdTipoClassificacaoRetorno;

    /** Atributo cdOrdenacaoRegistros. */
    private Integer cdOrdenacaoRegistros;

    /** Atributo dsIntervaloGeracao. */
    private String dsIntervaloGeracao;

    /** Atributo dsTipoMontagem. */
    private String dsTipoMontagem;

    /** Atributo dsTipoOcorrenciasArquivo. */
    private String dsTipoOcorrenciasArquivo;

    /** Atributo dsTipoClassificacaoRetorno. */
    private String dsTipoClassificacaoRetorno;

    /** Atributo dsOrdenacaoRegistros. */
    private String dsOrdenacaoRegistros;

    // combos
    /** Atributo listaTipoArquivoRetorno. */
    private List<SelectItem> listaTipoArquivoRetorno = new ArrayList<SelectItem>();

    /** Atributo listaTipoArquivoRetornoHash. */
    private Map<Integer, String> listaTipoArquivoRetornoHash = new HashMap<Integer, String>();

    /** Atributo listaNivelControle. */
    private List<SelectItem> listaNivelControle = new ArrayList<SelectItem>();

    /** Atributo listaNivelControleHash. */
    private Map<Integer, String> listaNivelControleHash = new HashMap<Integer, String>();

    /** Atributo listaTipoControle. */
    private List<SelectItem> listaTipoControle = new ArrayList<SelectItem>();

    /** Atributo listaTipoControleHash. */
    private Map<Integer, String> listaTipoControleHash = new HashMap<Integer, String>();

    /** Atributo listaTipoLayoutArquivo. */
    private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();

    /** Atributo listaTipoLayoutArquivoHash. */
    private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();

    /** Atributo listaMeioTransmissaoRemessaPrincipal. */
    private List<SelectItem> listaMeioTransmissaoRemessaPrincipal = new ArrayList<SelectItem>();

    /** Atributo listaMeioTransmissaoRemessaPrincipalHash. */
    private Map<Integer, String> listaMeioTransmissaoRemessaPrincipalHash = new HashMap<Integer, String>();

    /** Atributo listaGridPesquisaServico. */
    private List<ListarTipoLayoutContratoSaidaDTO> listaGridPesquisaServico;

    /** Atributo itemSelecionadoListaServico. */
    private Integer itemSelecionadoListaServico;

    /** Atributo listaGridPesquisaServicoSelecionados. */
    private List<ListarTipoLayoutContratoSaidaDTO> listaGridPesquisaServicoSelecionados;

    /** Atributo listaControleCheckServicos. */
    private List<SelectItem> listaControleCheckServicos;

    // c�digos
    /** Atributo cdTipoArquivoRetorno. */
    private Integer cdTipoArquivoRetorno;

    /** Atributo cdTpRejeicAcolimento. */
    private Integer cdTpRejeicAcolimento;

    /** Atributo cdAppFormatacao. */
    private String cdAppFormatacao;

    /** Atributo cdSerieAplicacaoTransmissao. */
    private String cdSerieAplicacaoTransmissao;

    /** Atributo cdControleNumeroRemessa. */
    private Integer cdControleNumeroRemessa;

    // descri��es

    /** Atributo dsLayout. */
    private String dsLayout;

    /** Atributo dsAppFormatacao. */
    private String dsAppFormatacao;

    /** Atributo dsCentroCusto. */
    private String dsCentroCusto;

    /** Atributo dsNomeRemessa. */
    private String dsNomeRemessa;

    /** Atributo dsNivelControle. */
    private String dsNivelControle;

    /** Atributo dsTipoControle. */
    private String dsTipoControle;

    /** Atributo dsNrMaxRemessa. */
    private String dsNrMaxRemessa;

    /** Atributo dsPeriodicidade. */
    private String dsPeriodicidade;

    /** Atributo dsPeriodicidadeRemessa. */
    private String dsPeriodicidadeRemessa;

    /** Atributo dsTpRejeicAcolimento. */
    private String dsTpRejeicAcolimento;

    /** Atributo percRegInconsistentes. */
    private BigDecimal percRegInconsistentes;

    /** Atributo dsPercRegInconsistentes. */
    private String dsPercRegInconsistentes;

    /** Atributo dsTipoArquivoRetorno. */
    private String dsTipoArquivoRetorno;

    /** Atributo dsNivelControleRetorno. */
    private String dsNivelControleRetorno;

    /** Atributo dsTipoControleRetorno. */
    private String dsTipoControleRetorno;

    /** Atributo dsNrMaxRetorno. */
    private String dsNrMaxRetorno;

    /** Atributo dsNomeRetorno. */
    private String dsNomeRetorno;

    /** Atributo layout. */
    private Integer layout;

    /** Atributo numVersaoLayout. */
    private Integer numVersaoLayoutAlt;

    /** Atributo numVersaoLayoutAltConf. */
    private Integer numVersaoLayoutAltConf;

    /** Atributo numVersaoLayoutExc. */
    private Integer numVersaoLayoutExc;

    /** Atributo codLancamento. */
    private Integer codLancamento;

    /** Atributo descLayout. */
    private String descLayout;

    /** Atributo cdSituacao. */
    private String cdSituacao;

    /** Atributo dsSituacao. */
    private String dsSituacao;

    /** Atributo responsavel. */
    private Integer responsavel;

    /** Atributo percentual. */
    private BigDecimal percentual;

    /** Atributo dsResponsavel. */
    private String dsResponsavel;

    /** Atributo percentualCustoTransmissao. */
    private BigDecimal percentualCustoTransmissao;

    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;

    // Atributos pagina conHistoricoLayoutsArquivos
    /** Atributo listaHistoricoLayout. */
    private List<ListarConManLayoutSaidaDTO> listaHistoricoLayout;

    /** Atributo listaControleHistoricoLayout. */
    private List<SelectItem> listaControleHistoricoLayout;

    /** Atributo itemSelecionadoListaHistoricoLayout. */
    private Integer itemSelecionadoListaHistoricoLayout;

    // Historico Retorno
    /** Atributo listaHistoricoRetorno. */
    private List<ListarHistoricoLayoutContratoSaidaDTO> listaHistoricoRetorno;

    /** Atributo itemSelecionadoListaHistoricoRetorno. */
    private Integer itemSelecionadoListaHistoricoRetorno;

    /** Atributo listaControleHistoricoRetorno. */
    private List<SelectItem> listaControleHistoricoRetorno;

    // attributos de entrada.
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSequenciaContratoNegocio. */
    private String nrSequenciaContratoNegocio;

    /** Atributo cdTipoLayout. */
    private Integer cdTipoLayout;

    /** Atributo dsTipoLayout. */
    private String dsTipoLayout;

    /** Atributo dataInclusaoLayout. */
    private String dataInclusaoLayout;

    // trilha de auditoria
    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /* Substituir */
    /** Atributo substituirLayout. */
    private Integer substituirLayout;

    /** Atributo substituirLayoutAntigo. */
    private Integer substituirLayoutAntigo;

    /** Atributo aplicativoFormatacao. */
    private Integer aplicativoFormatacao;

    /** Atributo principalRetorno. */
    private Integer principalRetorno;

    /** Atributo principalRemessa. */
    private Integer principalRemessa;

    /** Atributo alternativoRemessa. */
    private Integer alternativoRemessa;

    /** Atributo alternativoRetorno. */
    private Integer alternativoRetorno;

    /** Atributo empresaResponsavelTransmissao. */
    private String empresaResponsavelTransmissao;

    /** Atributo cdSerieAplicacaoTransmissaoSubstituir. */
    private String cdSerieAplicacaoTransmissaoSubstituir;

    /** Atributo dsNomeArquivoRemessa. */
    private String dsNomeArquivoRemessa;

    /** Atributo dsNomeArquivoRetorno. */
    private String dsNomeArquivoRetorno;

    /** Atributo subPercRegInconsistentes. */
    private BigDecimal subPercRegInconsistentes;

    /** Atributo cdTipoLayoutArquivoAtual. */
    private Integer cdTipoLayoutArquivoAtual;

    /** Atributo username. */
    private String username;

    /** Atributo substituirLayoutDesc. */
    private String substituirLayoutDesc;

    /** Atributo principalRemessaDesc. */
    private String principalRemessaDesc;

    /** Atributo principalRetornoDesc. */
    private String principalRetornoDesc;

    /** Atributo alternativoRemessaDesc. */
    private String alternativoRemessaDesc;

    /** Atributo alternativoRetornoDesc. */
    private String alternativoRetornoDesc;

    /** Atributo cdPeriodicidadeDesc. */
    private String cdPeriodicidadeDesc;

    /** Atributo dsPercRegInconsistentesDetalhar. */
    private String dsPercRegInconsistentesDetalhar;

    /** Atributo layoutContratadoSelecionado. */
    private Integer layoutContratadoSelecionado;

    /** Atributo layoutContratadoServico. */
    private String layoutContratadoServico;

    /** Atributo substituirCentroCusto. */
    private String substituirCentroCusto;

    /** Atributo listaCentroCustoSubstituir. */
    private List<SelectItem> listaCentroCustoSubstituir = new ArrayList<SelectItem>();

    /** Atributo substituirCentroCustoSelecionado. */
    private String substituirCentroCustoSelecionado;

    /* Hist�rico */
    /** Atributo dataInicioHistorico. */
    private Date dataInicioHistorico;

    /** Atributo dataFimHistorico. */
    private Date dataFimHistorico;

    /** Atributo tipoLayoutHistorico. */
    private Integer tipoLayoutHistorico;

    /** Atributo dsDataInclusao. */
    private String dsDataInclusao;

    /** Atributo layoutContratadoHistorico. */
    private Integer layoutContratadoHistorico;

    /** Atributo habilitarServicoAlterar. */
    private boolean habilitarServicoAlterar;

    /** Atributo habilitarConsultar. */
    private boolean habilitarConsultar;

    // Atributo utilizado para controlar habilita��o de campos da tela de
    // Hist�rico
    /** Atributo habilitaHistorico. */
    private boolean habilitaHistorico;

    /** Atributo habilitaHistoricoRetorno. */
    private boolean habilitaHistoricoRetorno;

    /** Atributo habilitarPercentual. */
    private boolean habilitarPercentual;

    /** Atributo habilitarLancamento. */
    private boolean habilitarLancamento;

    // hidden
    /** Atributo obrigatoriedade. */
    private String obrigatoriedade;

    /** Atributo tamanhoLista. */
    private int tamanhoLista;

    /** Atributo cdArquivo. */
    private Integer cdArquivo;

    /** Atributo dsArquivo. */
    private String dsArquivo;

    /** Atributo dataInicioManutencao. */
    private Date dataInicioManutencao;

    /** Atributo dataFimManutencao. */
    private Date dataFimManutencao;

    // Tela Servi�os
    /** Atributo listaTipoLayoutArquivoHistorico. */
    private List<SelectItem> listaTipoLayoutArquivoHistorico = new ArrayList<SelectItem>();

    /** Atributo listaTipoLayoutArquivoHistorico. */
    private List<SelectItem> listaNumVersaoLayoutArq = new ArrayList<SelectItem>();

    /** Atributo listaTipoLayoutArquivoHistoricoHash. */
    private Map<Integer, String> listaTipoLayoutArquivoHistoricoHash = new HashMap<Integer, String>();

    /** Atributo listaTipoLayoutArquivoHistoricoHash. */
    private Map<Integer, Integer> listaNumVersaoLayoutArqHash = new HashMap<Integer, Integer>();

    /** Atributo telaDetalhe. */
    private boolean telaDetalhe;

    /** Atributo telaExcluir. */
    private boolean telaExcluir = false;

    /** Atributo telaIncluir. */
    private boolean telaIncluir = false;

    /** Atributo saidaTipoLayoutArquivoSaidaDTO. */
    private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;

    /** Atributo numeroVersaoLayoutArquivo. */
    private Integer numeroVersaoLayoutArquivo;

    /** Atributo saidaListarNumeroVersaoLayout. */
    private ListarNumeroVersaoLayoutSaidaDTO saidaListarNumeroVersaoLayout;

    /** Atributo comboTipoMontagem. */
    private List<SelectItem> comboTipoMontagem = new ArrayList<SelectItem>();

    /** Atributo CONF_PAGAMENTO. */
    private static final String CONF_PAGAMENTO = "CONFIRMACAO DO PAGAMENTO";

    /** Atributo EST_PAGAMENTO. */
    private static final String EST_PAGAMENTO = "ESTORNO DO PAGAMENTO";

    /** Atributo DEV_PAGAMENTO. */
    private static final String DEV_PAGAMENTO = "DEVOLUCAO DO PAGAMENTO";

    /** Atributo CONS_CLI_SALDO. */
    private static final String CONS_CLI_SALDO = "CONSULTA CICLICA DE SALDO";

    /** Atributo AGENDAMENTO. */
    private static final String AGENDAMENTO = "AGENDAMENTO";

    /** Atributo saidaValidarUsuario. */
    private ValidarUsuarioSaidaDTO saidaValidarUsuario;

    /** Atributo entradaValidarUsuario. */
    private ValidarUsuarioEntradaDTO entradaValidarUsuario;

    /** Atributo restringirAcessoBotoes. */
    private boolean restringirAcessoBotoes;

    /** Atributo contrNumeroPagamentoSelecionado. */
    private Integer contrNumeroPagamentoSelecionado;

    /** Atributo contrNumeroPagamentoSelecionado. */
    private String nomeContrNumeroPagamento;

    /** Atributo saidaConsultarLayoutArquivo. */
    private ConsultarLayoutArquivoContratoSaidaDTO saidaConsultarLayoutArquivo;

    /** Atributo saidaConsultarConManLayout. */
    private ConsultarConManLayoutSaidaDTO saidaConsultarConManLayout;

    /** Atributo bloquearCombos. */
    private boolean bloquearCombos;

    /** Atributo desabilitarCombosRetornoLayout. */
    private boolean desabilitarCombosRetornoLayout;
    
    /** Atributo cdAcessoDepartamento. */
    public Integer cdAcessoDepartamento;

    // Metodos - Inicio
    /**
     * Carregar descricoes.
     */
    private void carregarDescricoes() {
        ConsultarListaLayoutArquivoContratoSaidaDTO selectedLayout = listaLayout
        .get(itemSelecionadoListaLayout);

        ConsultarLayoutArquivoContratoEntradaDTO entradaDTO = new ConsultarLayoutArquivoContratoEntradaDTO();
        entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
        entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
        entradaDTO
        .setNrSequenciaContratoNegocio(Long
            .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                            .equals("")) ? getNrSequenciaContratoNegocio()
                                : "0"));
        entradaDTO.setCdTipoLayoutArquivo(selectedLayout.getCdTipoLayout());

        saidaConsultarLayoutArquivo = manterContratoImpl
        .consultarLayoutArquivoContrato(entradaDTO);

        setDsTipoLayout(selectedLayout.getDsTipoLayout());
        setCdTipoLayout(selectedLayout.getCdTipoLayout());
        setDsSituacao(saidaConsultarLayoutArquivo.getDsSituacaoLayoutContrato());
        setResponsavel(saidaConsultarLayoutArquivo.getCdRespostaCustoEmpr());
        setDsResponsavel(saidaConsultarLayoutArquivo.getDsRespostaCustoEmpr());
        setPercentualCustoTransmissao(saidaConsultarLayoutArquivo
            .getCdPercentualOrgnzTransmissao());
        setCodLancamento(saidaConsultarLayoutArquivo.getCdMensagemLinhaExtrato());

        if (telaDetalhe || telaExcluir || telaIncluir) {
            if (selectedLayout.getDsTipoLayout().equalsIgnoreCase("CREC92")
                            || selectedLayout.getDsTipoLayout().equalsIgnoreCase(
                            "CREC200")
                            || selectedLayout.getDsTipoLayout().equalsIgnoreCase(
                            "CREC240")) {
                // Exibe campo Cod Lancamento
                setHabilitarLancamento(true);
            } else {
                setHabilitarLancamento(false);
            }

            setCodLancamento(saidaConsultarLayoutArquivo.getCdMensagemLinhaExtrato() == 0 ? null
                : saidaConsultarLayoutArquivo.getCdMensagemLinhaExtrato());
        }

        setLayout(selectedLayout.getCdTipoLayout());
        setCdSituacao(String.valueOf(selectedLayout
            .getCdSituacaoLayoutContrato()));

        setSubstituirLayout(selectedLayout.getCdTipoLayout());
        setSubstituirLayoutAntigo(selectedLayout.getCdTipoLayout());
        cdTipoLayoutArquivoAtual = selectedLayout.getCdTipoLayout();

        setDsLayout(selectedLayout.getDsTipoLayout());

        // Tela de Substituir - CentroCusto
        buscarCentroCusto();

        setDataHoraInclusao(FormatarData.formatarDataTrilha(saidaConsultarLayoutArquivo
            .getHrInclusaoRegistro()));
        setUsuarioInclusao(saidaConsultarLayoutArquivo.getCdUsuarioInclusao());
        setTipoCanalInclusao((saidaConsultarLayoutArquivo.getCdTipoCanalInclusao() != 0) ? Integer
            .toString(saidaConsultarLayoutArquivo.getCdTipoCanalInclusao())
            + " - " + saidaConsultarLayoutArquivo.getDsTipoCanalInclusao() : "");
        setComplementoInclusao(saidaConsultarLayoutArquivo.getCdOperacaoCanalInclusao());

        setDataHoraManutencao(FormatarData.formatarDataTrilha(saidaConsultarLayoutArquivo
            .getHrManutencaoRegistro()));
        setUsuarioManutencao(saidaConsultarLayoutArquivo.getCdUsuarioManutencao());
        setTipoCanalManutencao((saidaConsultarLayoutArquivo.getCdTipoCanalManutencao() != 0) ? Integer
            .toString(saidaConsultarLayoutArquivo.getCdTipoCanalManutencao())
            + " - " + saidaConsultarLayoutArquivo.getDsTipoCanalManutencao()
            : "");
        setComplementoManutencao(saidaConsultarLayoutArquivo.getCdOperacaoCanalManutencao());

        if (isHabilitaNumVersLayoutArqDet()) {
            setNumVersaoLayoutExc(saidaConsultarLayoutArquivo.getNrVersaoLayoutArquivo());
        }

    }

    /**
     * Carregar descricoes layout.
     */
    private void carregarDescricoesLayout() {
        ConsultarListaLayoutArquivoContratoSaidaDTO selectedLayout = listaLayout
        .get(itemSelecionadoListaLayout);

        ConsultarLayoutArquivoContratoEntradaDTO entradaDTO = new ConsultarLayoutArquivoContratoEntradaDTO();
        entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
        entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
        entradaDTO
        .setNrSequenciaContratoNegocio(Long
            .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                            .equals("")) ? getNrSequenciaContratoNegocio()
                                : "0"));
        entradaDTO.setCdTipoLayoutArquivo(selectedLayout.getCdTipoLayout());

        ConsultarLayoutArquivoContratoSaidaDTO saidaDTO = manterContratoImpl
        .consultarLayoutArquivoContrato(entradaDTO);

        setDsTipoLayout(selectedLayout.getDsTipoLayout());
        setDsSituacao(saidaDTO.getDsSituacaoLayoutContrato());
        setResponsavel(saidaDTO.getCdRespostaCustoEmpr());
        setDsResponsavel(saidaDTO.getDsRespostaCustoEmpr());
        setPercentualCustoTransmissao(saidaDTO
            .getCdPercentualOrgnzTransmissao());
        setCodLancamento(saidaDTO.getCdMensagemLinhaExtrato());

        if (telaDetalhe || telaExcluir || telaIncluir) {
            if (selectedLayout.getDsTipoLayout().equalsIgnoreCase("CREC92")
                            || selectedLayout.getDsTipoLayout().equalsIgnoreCase(
                            "CREC200")
                            || selectedLayout.getDsTipoLayout().equalsIgnoreCase(
                            "CREC240")) {
                // Exibe campo Cod Lancamento
                setHabilitarLancamento(true);
            } else {
                setHabilitarLancamento(false);
            }

            setCodLancamento(saidaDTO.getCdMensagemLinhaExtrato() == 0 ? null
                : saidaDTO.getCdMensagemLinhaExtrato());
        }

        setLayout(selectedLayout.getCdTipoLayout());
        setCdSituacao(String.valueOf(selectedLayout
            .getCdSituacaoLayoutContrato()));

        setSubstituirLayout(selectedLayout.getCdTipoLayout());
        setSubstituirLayoutAntigo(selectedLayout.getCdTipoLayout());
        cdTipoLayoutArquivoAtual = selectedLayout.getCdTipoLayout();

        setDsLayout(selectedLayout.getDsTipoLayout());

        // Tela de Substituir - CentroCusto
        buscarCentroCusto();
    }

    /**
     * Limpar layout.
     * 
     * @return the string
     */
    public String limparLayout() {
        setLayout(0);
        setCdSituacao("");
        setResponsavel(0);
        setCodLancamento(null);
        setPercentual(null);
        return "";
    }

    /**
     * Preenche tipo layout arquivo historico.
     */
    private void preencheTipoLayoutArquivoHistorico() {
        listaTipoLayoutArquivoHistorico = new ArrayList<SelectItem>();
        List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
        TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();
        tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);

        saidaTipoLayoutArquivoSaidaDTO = comboService
        .listarTipoLayoutArquivo(tipoLoteEntradaDTO);
        listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
        listaTipoLayoutArquivoHistoricoHash.clear();
        for (TipoLayoutArquivoSaidaDTO combo : listaTipoLayout) {
            listaTipoLayoutArquivoHistoricoHash.put(combo
                .getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
            listaTipoLayoutArquivoHistorico.add(new SelectItem(combo
                .getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
        }
    }

    /**
     * Preenche tipo layout arquivo historico.
     */
    private void preencherComboNumVersaoLayout() {
        listaNumVersaoLayoutArq = new ArrayList<SelectItem>();
        List<ListarNumeroVersaoLayoutOcorrSaidaDTO> lista = new ArrayList<ListarNumeroVersaoLayoutOcorrSaidaDTO>();
        ListarNumeroVersaoLayoutEntradaDTO entradaDTO = new ListarNumeroVersaoLayoutEntradaDTO();
        entradaDTO.setNumeroCombo(0);

        saidaListarNumeroVersaoLayout = comboService
        .listarNumeroVersaoLayout(entradaDTO);
        lista = saidaListarNumeroVersaoLayout.getOcorrencias();
        listaNumVersaoLayoutArqHash.clear();
        for (ListarNumeroVersaoLayoutOcorrSaidaDTO combo : lista) {
            listaNumVersaoLayoutArqHash.put(combo
                .getNumeroVersaoLayoutArquivo(), combo
                .getNumeroVersaoLayoutArquivo());
            listaNumVersaoLayoutArq.add(new SelectItem(combo
                .getNumeroVersaoLayoutArquivo(), combo
                .getNumeroVersaoLayoutArquivo().toString()));
        }
    }

    /**
     * Recupera dados layout.
     */
    public void recuperaDadosLayout() {
        // recupera dados da Grid de Layout
        ConsultarListaLayoutArquivoContratoSaidaDTO saidaDTO = listaLayout
        .get(getItemSelecionadoListaLayout());
        setCdTipoLayout(saidaDTO.getCdTipoLayout());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());

    }

    /**
     * Carrega lista retorno.
     */
    public void carregaListaRetorno() {

        ConsultarListaTipoRetornoLayoutEntradaDTO listaDTO = new ConsultarListaTipoRetornoLayoutEntradaDTO();

        // atributos de entrada.
        listaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
        listaDTO.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
        listaDTO
        .setNrSequenciaContratoNegocio(Long
            .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                            .equals("")) ? getNrSequenciaContratoNegocio()
                                : "0"));
        listaDTO.setCdTipoLayoutArquivo(getCdTipoLayout());
        listaDTO.setCdTipoTela(1);

        setListaRetorno(getManterContratoImpl()
            .consultarListaTipoRetornoLayout(listaDTO));
        setDataInclusaoLayout(getListaRetorno().get(0).getDtInclusaoRegistro());

        this.listaControleRetorno = new ArrayList<SelectItem>();

        for (int i = 0; i < getListaRetorno().size(); i++) {
            this.listaControleRetorno.add(new SelectItem(i, " "));
        }

        setItemSelecionadoListaRetorno(null);
    }

    /**
     * Voltar historico retorno.
     * 
     * @return the string
     */
    public String voltarHistoricoRetorno() {
        setItemSelecionadoListaRetorno(null);
        setListaHistoricoRetorno(null);
        return "VOLTAR";
    }

    /**
     * Voltar historico det.
     * 
     * @return the string
     */
    public String voltarHistoricoDet() {
        setItemSelecionadoListaHistoricoRetorno(null);
        return "VOLTAR";
    }

    /**
     * Limpar historico layout.
     * 
     * @return the string
     */
    public String limparHistoricoLayout() {
        setListaHistoricoRetorno(null);
        setItemSelecionadoListaHistoricoRetorno(null);
        setHabilitarConsultar(true);
        setHabilitaHistoricoRetorno(true);
        return "";
    }

    /**
     * Detalhar historico retorno.
     * 
     * @return the string
     */
    public String detalharHistoricoRetorno() {
        try {
            DetalharHistoricoLayoutContratoEntradaDTO entrada = new DetalharHistoricoLayoutContratoEntradaDTO();
            ListarHistoricoLayoutContratoSaidaDTO itemSelecionado = getListaHistoricoRetorno()
            .get(getItemSelecionadoListaHistoricoRetorno());

            // Dados do Contrato
            entrada
            .setCdPessoaJuridicaNegocio(this.cdPessoaJuridica != null ? this.cdPessoaJuridica
                : 0);
            entrada
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            entrada
            .setCdTipoContratoNegocio(this.cdTipoContratoNegocio != null ? this.cdTipoContratoNegocio
                : 0);
            entrada.setCdTipoLayoutArquivo(getCdTipoLayout());
            entrada.setHrInclusaoRegistro(itemSelecionado
                .getHrInclusaoManutencao());
            entrada.setCdArquivoRetornoPagamento(itemSelecionado
                .getCdArquivoRetornoPagamento());

            DetalharHistoricoLayoutContratoSaidaDTO saida = this.manterContratoImpl
            .detalharHistoricoLayoutContrato(entrada);

            setDsTipoLayout(saida.getDsTipoLayoutArquivo());
            setDsArquivo(saida.getDsArquivoRetornoPagamento());
            setDsIntervaloGeracao(saida.getDsTipoGeracaoRetorno());
            setDsTipoMontagem(saida.getDsTipoMontaRetorno());
            setDsTipoOcorrenciasArquivo(saida.getDsTipoOcorRetorno());
            setDsOrdenacaoRegistros(saida.getDsTipoOrdemRegistroRetorno());
            setDsTipoClassificacaoRetorno(saida.getDsTipoClassificacaoRetorno());

            setDataHoraInclusao(FormatarData.formatarDataTrilha(saida
                .getHrInclusaoRegistro()));
            setUsuarioInclusao(saida.getUsuarioInclusaoFormatado());
            setTipoCanalInclusao(saida.getTipoCanalInclusaoFormatado());
            setComplementoInclusao(saida.getCdOperacaoCanalInclusao().equals(
            "0") ? "" : saida.getCdOperacaoCanalInclusao());

            setDataHoraManutencao(FormatarData.formatarDataTrilha(saida
                .getHrManutencaoRegistro()));
            setUsuarioManutencao(saida.getUsuarioManutencaoFormatado());
            setTipoCanalManutencao(saida.getTipoCanalManutencaoFormatado());
            setComplementoManutencao(saida.getCdOperacaoCanalManutencao()
                .equals("0") ? "" : saida.getCdOperacaoCanalManutencao());

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }
        return "DETALHAR";
    }

    /**
     * Carrega servicos.
     */
    public void carregaServicos() {
        try {
            ListarTipoLayoutContratoEntradaDTO entradaDTO = new ListarTipoLayoutContratoEntradaDTO();

            entradaDTO.setCdTipoContrato(getCdTipoContratoNegocio());
            entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
            entradaDTO
            .setNrSequenciaContrato(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            entradaDTO.setCdTipoLayout(obterCodTipoLayout());

            setListaGridPesquisaServico(getManterContratoImpl()
                .listarTipoLayoutContrato(entradaDTO));

            listaControleCheckServicos = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaGridPesquisaServico().size(); i++) {
                this.listaControleCheckServicos.add(new SelectItem(i, " "));
            }
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridPesquisaServico(null);
        }
    }

    /**
     * Obter cod tipo layout.
     * 
     * @return the integer
     */
    private Integer obterCodTipoLayout() {
        if (listaLayout != null && !listaLayout.isEmpty()) {
            if (itemSelecionadoListaLayout != null) {
                return listaLayout.get(itemSelecionadoListaLayout)
                .getCdTipoLayout();
            }
        }
        return Integer.valueOf(0);
    }

    /**
     * Limpar historico retorno.
     * 
     * @return the string
     */
    public String limparHistoricoRetorno() {
        setDataInicioManutencao(new Date());
        setDataFimManutencao(new Date());
        setHabilitarConsultar(true);
        setHabilitaHistoricoRetorno(true);
        setListaHistoricoRetorno(null);
        setItemSelecionadoListaHistoricoRetorno(null);
        return "";
    }

    /**
     * Voltar.
     * 
     * @return the string
     */
    public String voltar() {
        return VOLTAR_JSP;
    }

    /**
     * Detalhar.
     * 
     * @return the string
     */
    public String detalhar() {
        setTelaDetalhe(true);
        carregarDescricoes();
        preencheTipoLayoutArquivoHistorico();

        return "DETALHAR";
    }

    /**
     * Incluir layouts.
     * 
     * @return the string
     */
    public String incluirLayouts() {
        limparLayout();
        preencheTipoLayoutArquivoHistorico();
        setHabilitarPercentual(false);
        setHabilitarLancamento(true);
        if (saidaTipoLayoutArquivoSaidaDTO != null) {
            setNumeroVersaoLayoutArquivo(saidaTipoLayoutArquivoSaidaDTO
                .getNumeroVersaoLayoutArquivo());
        }

        return "INCLUIR_LAYOUTS";
    }

    /**
     * Habilitar campo.
     * 
     * @return the string
     */
    public String habilitarCampo() {
        setPercentual(null);
        setPercentualCustoTransmissao(null);
        if (getResponsavel() == 1 || getResponsavel() == 3) {
            setHabilitarPercentual(true);
        } else {
            setHabilitarPercentual(false);
        }
        return "";
    }

    /**
     * Habilitar Campos Incluir.
     */
    public void habilitaCamposIncluir() {
        habilitarCampoLancamento();
        isHabilitaNumVersLayoutArq();
    }

    /**
     * Habilitar campo lancamento.
     */
    public void habilitarCampoLancamento() {
        String layout = listaTipoLayoutArquivoHistoricoHash.get(getLayout());

        if (getLayout() != 0) {
            if (layout.equalsIgnoreCase("CREC200")
                            || layout.equalsIgnoreCase("CREC240")
                            || layout.equalsIgnoreCase("CREC92")) {
                setHabilitarLancamento(false);
            } else {
                setHabilitarLancamento(true);
                setCodLancamento(null);
            }
        } else {
            setHabilitarLancamento(true);
            setCodLancamento(null);
        }

    }

    /**
     * Excluir layouts.
     * 
     * @return the string
     */
    public String excluirLayouts() {
        setTelaDetalhe(false);
        setTelaExcluir(true);
        carregarDescricoes();
        preencheTipoLayoutArquivoHistorico();

        if (isHabilitaNumVersLayoutArqExc()) {
            setNumVersaoLayoutExc(saidaTipoLayoutArquivoSaidaDTO
                .getNumeroVersaoLayoutArquivo());
        }

        return "EXCLUIR_LAYOUTS";
    }

    /**
     * Alterar layouts.
     * 
     * @return the string
     */
    public String alterarLayouts() {
        setTelaDetalhe(false);

        carregarDescricoes();
        preencheTipoLayoutArquivoHistorico();

        if (isHabilitaNumVersLayoutArqAlt()) {
            preencherComboNumVersaoLayout();
            setNumVersaoLayoutAlt(saidaTipoLayoutArquivoSaidaDTO
                .getNumeroVersaoLayoutArquivo());
        }

        if (getResponsavel() == 1 || getResponsavel() == 3) {
            setHabilitarPercentual(true);
        } else {
            setHabilitarPercentual(false);
        }

        setContrNumeroPagamentoSelecionado(1);

        return "ALTERAR_LAYOUTS";
    }

    /**
     * Retorno.
     * 
     * @return the string
     */
    public String retorno() {
        recuperaDadosLayout();

        try {
            carregaListaRetorno();
        } catch (PdcAdapterFunctionalException p) {
            if ("PGIT0003".equals(StringUtils.right(p.getCode(), 8))) {
                setListaRetorno(new ArrayList<ConsultarListaTipoRetornoLayoutSaidaDTO>());
                return "RETORNO";
            }
        }	

        validarUsuario();

        return "RETORNO";
    }

    /**
     * Historico retorno.
     * 
     * @return the string
     */
    public String historicoRetorno() {
        listarTipoArquivoRetorno();
        ConsultarListaTipoRetornoLayoutSaidaDTO itemSelecionado = new ConsultarListaTipoRetornoLayoutSaidaDTO();
        if (getItemSelecionadoListaRetorno() != null) {
            itemSelecionado = getListaRetorno().get(
                getItemSelecionadoListaRetorno());
        }
        setCdTipoArquivoRetorno(itemSelecionado.getCdTipoArquivoRetorno());
        setDsArquivo(itemSelecionado.getDsTipoArquivoRetorno());
        setHabilitaHistoricoRetorno(true);
        setHabilitarConsultar(true);
        setDataInicioManutencao(new Date());
        setDataFimManutencao(new Date());
        return "HISTORICO";
    }

    /**
     * Servico.
     * 
     * @return the string
     */
    public String servico() {
        carregaServicos();
        setHabilitarServicoAlterar(false);
        validarUsuario();
        return "SERVICO";
    }

    /**
     * Historico.
     * 
     * @return the string
     */
    public String historico() {
        preencheTipoLayoutArquivoHistorico();

        setListaHistoricoLayout(null);
        setItemSelecionadoListaHistoricoLayout(null);
        setListaControleHistoricoLayout(new ArrayList<SelectItem>());
        if (getItemSelecionadoListaLayout() != null && getListaLayout() != null) {
            setTipoLayoutHistorico(getListaLayout().get(
                getItemSelecionadoListaLayout()).getCdTipoLayout());
            setDataInicioHistorico(new Date());
            setDataFimHistorico(new Date());
            setHabilitaHistorico(true);
            setHabilitarConsultar(true);

        } else {
            setTipoLayoutHistorico(null);
            setDataInicioHistorico(new Date());
            setDataFimHistorico(new Date());

            // Habilita campos para edi��o
            setHabilitaHistorico(true);
            setHabilitarConsultar(true);
        }

        // Tela: conHistoricoLayoutsArquivos
        return "HISTORICO";
    }

    /**
     * Avancar incluir layouts.
     * 
     * @return the string
     */
    public String avancarIncluirLayouts() {
        setDescLayout((String) listaTipoLayoutArquivoHistoricoHash
            .get(getLayout()));
        return "AVANCAR";
    }

    /**
     * Efetivar incluir layouts.
     * 
     * @return the string
     */
    public String efetivarIncluirLayouts() {
        try {
            IncluirLayoutEntradaDTO entradaDTO = new IncluirLayoutEntradaDTO();
            IncluirLayoutSaidaDTO saidaDTO = new IncluirLayoutSaidaDTO();
            entradaDTO.setCdPessoaJuridicaNegocio(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
            entradaDTO
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            entradaDTO.setCdTipoLayoutArquivo(getLayout());
            entradaDTO.setCdSituacaoLayoutContrato(1); // Passar 1 fixo conforme
            // pedido pela analista
            // Camila Vegas
            // 24/10/2012
            entradaDTO.setCdResponsavelCustoEmpresa(getResponsavel());
            if (getResponsavel() == 1 || getResponsavel() == 3) {
                entradaDTO
                .setPercentualCustoOrganizacaoTransmissao(getPercentual());
            } else {
                entradaDTO
                .setPercentualCustoOrganizacaoTransmissao(new BigDecimal(
                "0.00"));
            }
            entradaDTO.setCdMensagemLinhaExtrato(getCodLancamento());
            if (isHabilitaNumVersLayoutArq()) {
                entradaDTO
                .setNumeroVersaoLayoutArquivo(getNumeroVersaoLayoutArquivo());
            }

            saidaDTO = this.manterContratoImpl.incluirLayout(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(), "selLayoutsArquivos",
                BradescoViewExceptionActionType.ACTION, false);

            carregaListaLayoutRetorno();

            return "OK";
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }
    }

    /**
     * Confirmar excluir layouts.
     * 
     * @return the string
     */
    public String confirmarExcluirLayouts() {
        try {
            ExcluirLayoutEntradaDTO entradaDTO = new ExcluirLayoutEntradaDTO();
            ExcluirLayoutSaidaDTO saidaDTO = new ExcluirLayoutSaidaDTO();
            entradaDTO.setCdPessoaJuridicaNegocio(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
            entradaDTO
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            entradaDTO.setCdTipoLayoutArquivo(getSubstituirLayout());
            if (isHabilitaNumVersLayoutArqExc()) {
                entradaDTO.setNumeroVersaoLayoutArquivo(numVersaoLayoutExc);
            }

            saidaDTO = this.manterContratoImpl.excluirLayout(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(), "selLayoutsArquivos",
                BradescoViewExceptionActionType.ACTION, false);

            carregaListaLayoutRetorno();

            return "OK";
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }
    }

    /**
     * Limpar historico.
     */
    public void limparHistorico() {
        setDataInicioHistorico(new Date());
        setDataFimHistorico(new Date());
        setTipoLayoutHistorico(0);
        setItemSelecionadoListaHistoricoLayout(null);
        setListaHistoricoLayout(null);
        setHabilitaHistorico(true);
        setHabilitarConsultar(true);
    }

    /**
     * Carrega lista historico.
     */
    public void carregaListaHistorico() {
        try {
            ListarConManLayoutEntradaDTO entrada = new ListarConManLayoutEntradaDTO();

            // Dados de Contrato
            entrada
            .setCdPessoaJuridicaNegocio(this.cdPessoaJuridica != null ? this.cdPessoaJuridica
                : 0);
            entrada
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            entrada
            .setCdTipoContratoNegocio(this.cdTipoContratoNegocio != null ? this.cdTipoContratoNegocio
                : 0);

            if (!isHabilitaHistorico()) {
                entrada.setDtFim("");
                entrada.setDtInicio("");
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                entrada.setDtFim((getDataFimHistorico() == null) ? "" : sdf
                    .format(getDataFimHistorico()));
                entrada.setDtInicio((getDataInicioHistorico() == null) ? ""
                    : sdf.format(getDataInicioHistorico()));
            }

            entrada.setCdTipoLayoutArquivo(getTipoLayoutHistorico());

            setListaHistoricoLayout(manterContratoImpl
                .listarConManLayout(entrada));

            this.listaControleHistoricoLayout = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaHistoricoLayout().size(); i++) {
                this.listaControleHistoricoLayout.add(new SelectItem(i, " "));
            }

            setItemSelecionadoListaHistoricoLayout(null);
            setHabilitarConsultar(false);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaHistoricoLayout(null);
        }

    }

    /**
     * Carrega lista historico retorno.
     */
    public void carregaListaHistoricoRetorno() {
        try {
            ConsultarListaTipoRetornoLayoutSaidaDTO itemSelecionado = new ConsultarListaTipoRetornoLayoutSaidaDTO();
            ListarHistoricoLayoutContratoEntradaDTO entrada = new ListarHistoricoLayoutContratoEntradaDTO();
            if (getItemSelecionadoListaRetorno() != null) {
                itemSelecionado = getListaRetorno().get(
                    getItemSelecionadoListaRetorno());
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            // Dados de Contrato
            entrada
            .setCdPessoaJuridicaNegocio(this.cdPessoaJuridica != null ? this.cdPessoaJuridica
                : 0);
            entrada
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            entrada
            .setCdTipoContratoNegocio(this.cdTipoContratoNegocio != null ? this.cdTipoContratoNegocio
                : 0);
            entrada.setCdTipoLayoutArquivo(getCdTipoLayout());
            entrada
            .setDtInicioManutencao(sdf
                .format(getDataInicioManutencao()));
            entrada.setDtFimManutencao(sdf.format(getDataFimManutencao()));
            entrada.setCdArquivoRetornoPagamento(getCdTipoArquivoRetorno());

            setListaHistoricoRetorno(manterContratoImpl
                .listarHistoricoLayoutContrato(entrada));

            this.listaControleHistoricoLayout = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaHistoricoRetorno().size(); i++) {
                this.listaControleHistoricoLayout.add(new SelectItem(i, " "));
            }

            setItemSelecionadoListaHistoricoRetorno(null);
            setHabilitarConsultar(false);
            setHabilitaHistoricoRetorno(false);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaHistoricoRetorno(null);
        }
    }

    /**
     * Detalhar historico.
     * 
     * @return the string
     */
    public String detalharHistorico() {
        try {
            ConsultarConManLayoutEntradaDTO entrada = new ConsultarConManLayoutEntradaDTO();

            // Dados do Contrato
            entrada
            .setCdPessoaJuridicaContrato(this.cdPessoaJuridica != null ? this.cdPessoaJuridica
                : 0);
            entrada
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            entrada
            .setCdTipoContratoNegocio(this.cdTipoContratoNegocio != null ? this.cdTipoContratoNegocio
                : 0);

            entrada
            .setCdTipoLayoutArquivo(getListaHistoricoLayout().get(
                getItemSelecionadoListaHistoricoLayout())
                .getCdTipoLayout());
            entrada.setHrInclusaoRegistro(getListaHistoricoLayout().get(
                getItemSelecionadoListaHistoricoLayout())
                .getHrInclusaoRegistro());
            setDsTipoManutencao(getListaHistoricoLayout().get(
                getItemSelecionadoListaHistoricoLayout())
                .getDsTipoManutencao());

            saidaConsultarConManLayout = this.manterContratoImpl
            .consultarConManLayout(entrada);

            setDsTipoLayout(saidaConsultarConManLayout.getDsTipoLayoutArquivo());
            setCodLancamento(saidaConsultarConManLayout.getCdMsgLinExtrt());
            setDsSituacao(saidaConsultarConManLayout.getDsSituacaoLayoutContrato());
            setDsResponsavel(saidaConsultarConManLayout.getDsResponsavelCustoEmpresa());
            setPercentualCustoTransmissao(saidaConsultarConManLayout
                .getPercentualCustoOrganizacaoTransmissao());

            setDataHoraInclusao(saidaConsultarConManLayout.getHrInclusaoRegistro());
            setUsuarioInclusao(saidaConsultarConManLayout.getCdUsuarioInclusao());
            setTipoCanalInclusao((saidaConsultarConManLayout.getCdTipoCanalInclusao() != 0) ? Integer
                .toString(saidaConsultarConManLayout.getCdTipoCanalInclusao())
                + " - " + saidaConsultarConManLayout.getDsCanalInclusao()
                : "");
            setComplementoInclusao(saidaConsultarConManLayout.getCdOperacaoCanalInclusao());

            setDataHoraManutencao(saidaConsultarConManLayout.getHrManutencaoRegistro());
            setUsuarioManutencao(saidaConsultarConManLayout.getCdUsuarioManutencao());
            setTipoCanalManutencao(saidaConsultarConManLayout.getCdTipoCanalManutencao() != 0 ? Integer
                .toString(saidaConsultarConManLayout.getCdTipoCanalManutencao())
                + " - " + saidaConsultarConManLayout.getDsCanalManutencao()
                : "");
            setComplementoManutencao(saidaConsultarConManLayout.getCdOperacaoCanalManutencao());

            if (getDsTipoLayout().equalsIgnoreCase("CREC92")
                            || getDsTipoLayout().equalsIgnoreCase("CREC200")
                            || getDsTipoLayout().equalsIgnoreCase("CREC240")) {
                // Exibe campo Cod Lancamento
                setHabilitarLancamento(true);
            } else {
                setHabilitarLancamento(false);
            }

            if (getCodLancamento() != null) {
                setCodLancamento(getCodLancamento() == 0 ? null
                    : getCodLancamento());
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }

        return "DETALHAR_HISTORICO";
    }

    /**
     * Avancar layouts.
     * 
     * @return the string
     */
    public String avancarLayouts() {
        setDescLayout((String) listaTipoLayoutArquivoHistoricoHash
            .get(getLayout()));
        if (isHabilitaNumVersLayoutArq()) {
            setNumVersaoLayoutAltConf(getNumVersaoLayoutAlt());
        }

        if (contrNumeroPagamentoSelecionado != null && contrNumeroPagamentoSelecionado == 1) {
            setNomeContrNumeroPagamento("Cliente");
        }else if (contrNumeroPagamentoSelecionado != null && contrNumeroPagamentoSelecionado == 2) {
            setNomeContrNumeroPagamento("Banco");
        }

        return "AVANCAR";
    }

    /**
     * Confirmar alterar layout.
     * 
     * @return the string
     */
    public String confirmarAlterarLayout() {
        try {
            ConfiguracaoLayoutArquivoContratoEntradaDto entradaDTO = new ConfiguracaoLayoutArquivoContratoEntradaDto();
            AlterarConfiguracaoLayoutArqContratoSaidaDTO saidaDTO = new AlterarConfiguracaoLayoutArqContratoSaidaDTO();
            entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
            entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong((
                            getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio().equals("")) ? 
                                getNrSequenciaContratoNegocio() : "0"));
            entradaDTO.setCdTipoLayoutArquivo(getLayout());
            entradaDTO.setCdSituacaoLayoutContrato(1);
            entradaDTO.setCdResponsavelCustoEmpresa(getResponsavel());
            entradaDTO.setPercentualCustoOrganizacaoTransmissao(getPercentualCustoTransmissao());
            entradaDTO.setCdMensagemLinhaExtrato(getCodLancamento());
            if (isHabilitaNumVersLayoutArqAlt()) {
                entradaDTO.setNumeroVersaoLayoutArquivo(getNumVersaoLayoutAltConf());
            }

            entradaDTO.setCdTipoControlePagamento(getContrNumeroPagamentoSelecionado());
            saidaDTO = this.manterContratoImpl.alterarConfiguracaoLayoutArqContrato(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") "+ saidaDTO.getMensagem(), 
                "selLayoutsArquivos", BradescoViewExceptionActionType.ACTION, false);
            carregaListaLayoutRetorno();
            return "OK";
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }
    }

    /**
     * Pesquisar.
     * 
     * @param evt
     *            the evt
     * @return the string
     */
    public String pesquisar(ActionEvent evt) {
        // usado no dataScroller
        return "ok";
    }

    /**
     * Voltar retorno.
     * 
     * @return the string
     */
    public String voltarRetorno() {
        // para selLayoutsArquivos
        setItemSelecionadoListaLayout(null);
        return VOLTAR_JSP;
    }

    /**
     * Detalhar retorno.
     * 
     * @return the string
     */
    public String detalharRetorno() {
        setTelaDetalhe(true);
        this.carregarDescDetalheRetorno();
        return "DETALHAR";
    }

    /**
     * Carregar desc detalhe retorno.
     */
    private void carregarDescDetalheRetorno() {
        ConsultarListaLayoutArquivoContratoSaidaDTO selectedLayout = listaLayout
        .get(itemSelecionadoListaLayout);
        ConsultarListaTipoRetornoLayoutSaidaDTO selectedRetorno = listaRetorno
        .get(itemSelecionadoListaRetorno);

        DetalharTipoRetornoLayoutEntradaDTO entradaDTO = new DetalharTipoRetornoLayoutEntradaDTO();

        entradaDTO
        .setCdPessoaJuridicaContrato(this.cdPessoaJuridica != null ? this.cdPessoaJuridica
            : 0);
        entradaDTO
        .setCdTipoContratoNegocio(this.cdTipoContratoNegocio != null ? this.cdTipoContratoNegocio
            : 0);
        entradaDTO
        .setNrSequenciaContratoNegocio(Long
            .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                            .equals("")) ? getNrSequenciaContratoNegocio()
                                : "0"));
        entradaDTO.setCdTipoLayoutArquivo(selectedLayout.getCdTipoLayout());
        entradaDTO.setCdTipoArquivoRetorno(selectedRetorno
            .getCdTipoArquivoRetorno());

        DetalharTipoRetornoLayoutSaidaDTO saidaDTO = manterContratoImpl
        .detalharTipoRetornoLayout(entradaDTO);
        setCdIntervaloGeracao(saidaDTO.getCdTipoGeracaoRetorno());
        setDsIntervaloGeracao(saidaDTO.getDsTipoGeracaoRetorno());
        setCdTipoMontagem(saidaDTO.getCdTipoMontaRetorno());
        setDsTipoMontagem(saidaDTO.getDsTipoMontaRetorno());
        setCdTipoOcorrenciasArquivo(saidaDTO.getCdTipoOcorrenciaRetorno());
        setDsTipoOcorrenciasArquivo(saidaDTO.getDsTipoOcorrenciaRetorno());
        setCdTipoClassificacaoRetorno(saidaDTO.getCdTipoClassificacaoRetorno());
        setDsTipoClassificacaoRetorno(saidaDTO.getDsTipoClassificacaoRetorno());
        setCdOrdenacaoRegistros(saidaDTO.getCdTipoOrdemRegistroRetorno());
        setDsOrdenacaoRegistros(saidaDTO.getDsTipoOrdemRegistroRetorno());

        setCdTipoArquivoRetorno(selectedRetorno.getCdTipoArquivoRetorno());
        setDsTipoArquivoRetorno(selectedRetorno.getDsTipoArquivoRetorno());

        setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
        setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());

        setComplementoInclusao(saidaDTO.getCdOperacaoCanalInclusao()
            .equals("0") ? "" : saidaDTO.getCdOperacaoCanalInclusao());
        setComplementoManutencao(saidaDTO.getCdOperacaoCanalManutencao()
            .equals("0") ? "" : saidaDTO.getCdOperacaoCanalManutencao());

        setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() == 0 ? ""
            : saidaDTO.getCdTipoCanalInclusao() + " - "
            + saidaDTO.getDsTipoCanalInclusao());
        setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() == 0 ? ""
            : saidaDTO.getCdTipoCanalManutencao() + " - "
            + saidaDTO.getDsTipoCanalManutencao());

        setDataHoraInclusao(saidaDTO.getHrInclusaoRegistro());
        setDataHoraManutencao(saidaDTO.getHrManutencaoRegistro());

        carregarDescricoesLayout();
    }

    /**
     * Incluir.
     * 
     * @return the string
     */
    public String incluir() {
        setBloquearCombos(false);
        this.comboTipoMontagem = new ArrayList<SelectItem>();
        listarTipoArquivoRetorno();
        comboTipoMontagem();
        setTelaDetalhe(false);
        setTelaIncluir(true);
        carregarDescricoes();
        setCdTipoArquivoRetorno(null);
        setCdIntervaloGeracao(0);
        setCdTipoMontagem(0);
        setCdTipoOcorrenciasArquivo(0);
        setCdTipoClassificacaoRetorno(0);
        setCdOrdenacaoRegistros(null);		
        return "INCLUIR";
    }

    /**
     * Validar codigo tipo arquivo retorno incluir.
     */
    public void validarCodigoTipoArquivoRetornoIncluir(){
        if (getCdTipoArquivoRetorno() != null && getCdTipoArquivoRetorno() == 24) {			
            setCdIntervaloGeracao(2);
            setCdTipoMontagem(9);
            setCdTipoOcorrenciasArquivo(1);
            setBloquearCombos(true);
        }else {
            setCdIntervaloGeracao(null);
            setCdTipoMontagem(null);
            setCdTipoOcorrenciasArquivo(null);
            setBloquearCombos(false);
        }
    }

    /**
     * Alterar.
     * 
     * @return the string
     */
    public String alterar() {
        this.recuperaDadosLayout();
        carregarDescDetalheRetorno();
        listarTipoArquivoRetorno();	
        validarCdTipoArquivoRetornoAlterar();

        return ALTERAR_JSP;
    }

    /**
     * Validar cd tipo arquivo retorno alterar.
     */
    public void validarCdTipoArquivoRetornoAlterar(){
        if (getCdTipoArquivoRetorno() != null && getCdTipoArquivoRetorno() == 24) {
            setCdIntervaloGeracao(2);
            setCdTipoMontagem(9);
            setCdTipoOcorrenciasArquivo(1);
            setDesabilitarCombosRetornoLayout(true);
            setBloquearCombos(true);
        }
        else {
            setBloquearCombos(false);
            setDesabilitarCombosRetornoLayout(false);
        }
    }

    /**
     * Excluir.
     * 
     * @return the string
     */
    public String excluir() {
        setTelaExcluir(true);
        this.carregarDescDetalheRetorno();
        return "EXCLUIR";
    }

    /**
     * Voltar detalhe retorno.
     * 
     * @return the string
     */
    public String voltarDetalheRetorno() {
        setItemSelecionadoListaRetorno(null);
        return "VOLTAR";
    }

    /**
     * Voltar incluir.
     * 
     * @return the string
     */
    public String voltarIncluir() {
        // para selRetornoLayoutsArquivos
        setItemSelecionadoListaRetorno(null);
        setCdTipoArquivoRetorno(null);
        this.comboTipoMontagem = new ArrayList<SelectItem>();
        return VOLTAR_JSP;
    }

    /**
     * Avancar incluir.
     * 
     * @return the string
     */
    public String avancarIncluir() {
        // para incRetornoLayoutsArquivos2.jsp
        if (getHiddenObrigatoriedade().equals("T")) {
            montaDescricao();
            return "AVANCAR_INCLUIR";
        } else {

            return "";
        }
    }

    /**
     * Voltar incluir2.
     * 
     * @return the string
     */
    public String voltarIncluir2() {
        // para incRetornoLayoutsArquivos1
        return VOLTAR_JSP;
    }

    /**
     * Confirmar incluir.
     * 
     * @return the string
     */
    public String confirmarIncluir() {
        // Realiza a inclus�o
        // para selRetornoLayoutsArquivos

        try {
            IncluirTipoRetornoLayoutEntradaDTO entradaDTO = new IncluirTipoRetornoLayoutEntradaDTO();
            IncluirTipoRetornoLayoutSaidaDTO saidaDTO;

            entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
            entradaDTO
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));

            ConsultarListaLayoutArquivoContratoSaidaDTO selectedLayout = listaLayout
            .get(itemSelecionadoListaLayout);

            entradaDTO.setCdTipoLayoutArquivo(selectedLayout.getCdTipoLayout());
            entradaDTO.setCdArquivoRetornoPagamento(getCdTipoArquivoRetorno());
            entradaDTO.setCdTipoGeracaoRetorno(getCdIntervaloGeracao());
            entradaDTO.setCdTipoMontaRetorno(getCdTipoMontagem());
            entradaDTO
            .setCdTipoOcorrenciaRetorno(getCdTipoOcorrenciasArquivo());
            entradaDTO
            .setCdTipoClassificacaoRetorno(getCdTipoClassificacaoRetorno());
            entradaDTO.setCdTipoOrdemRegistroRetorno(getCdOrdenacaoRegistros());

            saidaDTO = this.manterContratoImpl
            .incluirTipoRetornoLayout(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(), RETORNO_LAYOUTS_ARQUIVOS,
                BradescoViewExceptionActionType.ACTION, false);

            carregaListaRetorno();

            return "OK";
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }
    }

    /**
     * Voltar alterar.
     * 
     * @return the string
     */
    public String voltarAlterar() {
        // para selRetornoLayoutsArquivos
        setItemSelecionadoListaRetorno(null);
        return VOLTAR_JSP;
    }

    /**
     * Alterar avancar.
     * 
     * @return the string
     */
    public String alterarAvancar() {
        return "AVANCAR";
    }

    /**
     * Voltar alterar2.
     * 
     * @return the string
     */
    public String voltarAlterar2() {
        // para altRetornoLayoutsArquivos1
        return VOLTAR_JSP;
    }

    /**
     * Confirmar alterar.
     * 
     * @return the string
     */
    public String confirmarAlterar() {

        try {
            AlterarTipoRetornoLayoutEntradaDTO entradaDTO = new AlterarTipoRetornoLayoutEntradaDTO();

            entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
            entradaDTO
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));

            ConsultarListaLayoutArquivoContratoSaidaDTO selectedLayout = listaLayout
            .get(itemSelecionadoListaLayout);

            entradaDTO.setCdTipoLayoutArquivo(selectedLayout.getCdTipoLayout());
            entradaDTO.setCdTipoGeracaoRetorno(getCdIntervaloGeracao());
            entradaDTO.setCdTipoMontaRetorno(getCdTipoMontagem());
            entradaDTO
            .setCdTipoOcorrenciaRetorno(getCdTipoOcorrenciasArquivo());
            entradaDTO
            .setCdTipoClassificacaoRetorno(getCdTipoClassificacaoRetorno());
            entradaDTO.setCdTipoOrdemRegistroRetorno(getCdOrdenacaoRegistros());
            entradaDTO.setCdArquivoRetornoPagamento(getCdTipoArquivoRetorno());

            AlterarTipoRetornoLayoutSaidaDTO saidaDTO = manterContratoImpl
            .alterarTipoRetornoLayout(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(), RETORNO_LAYOUTS_ARQUIVOS,
                BradescoViewExceptionActionType.ACTION, false);

            carregaListaRetorno();

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);

        }

        return "";
    }

    /**
     * Nome: validarUsuario
     * 
     * @exception
     * @param
     * @return
     * @throws
     * @see
     */
    public void validarUsuario() {
        setEntradaValidarUsuario(new ValidarUsuarioEntradaDTO());

        try {
            entradaValidarUsuario.setMaxOcorrencias(PgitUtil.verificaIntegerNulo(0));
            saidaValidarUsuario = manterContratoImpl.validarUsuario(entradaValidarUsuario);

            if(saidaValidarUsuario.getCdAcessoDepartamento() != null && saidaValidarUsuario.getCdAcessoDepartamento() == 1){
                setRestringirAcessoBotoes(false);
            }else if(saidaValidarUsuario.getCdAcessoDepartamento() != null && saidaValidarUsuario.getCdAcessoDepartamento() == 2){
                setRestringirAcessoBotoes(true);
            }

        } catch (PdcAdapterFunctionalException e) {
            BradescoFacesUtils.addInfoModalMessage("("
                + StringUtils.right(e.getCode(), 8) + ") "
                + e.getMessage(), false);
        }

    }

    /**
     * Voltar excluir.
     * 
     * @return the string
     */
    public String voltarExcluir() {
        // para selRetornoLayoutsArquivos
        setItemSelecionadoListaRetorno(null);
        return VOLTAR_JSP;
    }

    /**
     * Confirmar excluir.
     * 
     * @return the string
     */
    public String confirmarExcluir() {

        try {

            ConsultarListaLayoutArquivoContratoSaidaDTO selectedLayout = listaLayout
            .get(itemSelecionadoListaLayout);
            ConsultarListaTipoRetornoLayoutSaidaDTO selectedRetorno = listaRetorno
            .get(itemSelecionadoListaRetorno);

            ExcluirTipoRetornoLayoutEntradaDTO entradaDTO = new ExcluirTipoRetornoLayoutEntradaDTO();

            entradaDTO.setCdTipoLayoutArquivo(selectedLayout.getCdTipoLayout());
            entradaDTO.setCdArquivoRetornoPagamento(selectedRetorno
                .getCdTipoArquivoRetorno());
            entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContratoNegocio(this.cdTipoContratoNegocio);
            entradaDTO
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));

            ExcluirTipoRetornoLayoutSaidaDTO excluirTipoRetornoLayoutSaidaDTO = getManterContratoImpl()
            .excluirTipoRetornoLayout(entradaDTO);
            BradescoFacesUtils.addInfoModalMessage("("
                + excluirTipoRetornoLayoutSaidaDTO.getCodMensagem() + ") "
                + excluirTipoRetornoLayoutSaidaDTO.getMensagem(),
                RETORNO_LAYOUTS_ARQUIVOS,
                BradescoViewExceptionActionType.ACTION, false);

            carregaListaRetorno();

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }
        return "";

    }

    /**
     * Alterar servico.
     * 
     * @return the string
     */
    public String alterarServico() {
        preenchePesquisaServicoSelecionados();

        for (int i = 0; i < listaGridPesquisaServicoSelecionados.size(); i++) {
            ListarTipoLayoutContratoSaidaDTO tipoLayoutCorrente = listaGridPesquisaServicoSelecionados.get(i);
            tipoLayoutCorrente.setCdNovoLayout(0);
            preencherListaLayoutContratadoServico(tipoLayoutCorrente);
        }

        return ALTERAR_JSP;
    }

    /**
     * Preencher lista layout contratado servico.
     * 
     * @param tipoLayout
     *            the tipo layout
     */
    private void preencherListaLayoutContratadoServico(ListarTipoLayoutContratoSaidaDTO tipoLayout) {
        try {
            ListarLayoutsContratoEntradaDTO entrada = new ListarLayoutsContratoEntradaDTO();
            entrada.setMaxOcorrencias(50);
            entrada.setCdpessoaJuridicaContrato(getCdPessoaJuridica());
            entrada.setCdTipoContrato(getCdTipoContratoNegocio());
            entrada.setNrSequenciaContratoNegocio(PgitUtil.isStringNullLong(getNrSequenciaContratoNegocio()));
            entrada.setCdProdutoServicoOperacao(tipoLayout.getCdTipoServico());
            entrada.setCdProdutoOperacaoRelacionado(tipoLayout.getCdRelacionamentoProdutoProduto());

            ListarLayoutsContratoSaidaDTO saida = getComboService().listarLayoutsContrato(entrada);

            List<SelectItem> listaComboNovosLayouts = new ArrayList<SelectItem>();
            for (int i = 0; i < saida.getOcorrencias().size(); i++) {
                ListarLayoutsContratoOcorrenciasSaidaDTO ocorrencia = saida.getOcorrencias().get(i);
                if (ocorrencia.getDsTipoLayoutArquivo() != null && !"".equals(ocorrencia.getDsTipoLayoutArquivo())) {
                    listaComboNovosLayouts.add(new SelectItem(
                        ocorrencia.getCdTipoLayoutArquivo(), ocorrencia.getDsTipoLayoutArquivo()));
                }
            }
            tipoLayout.setListaNovosLayouts(listaComboNovosLayouts);
        } catch (PdcAdapterFunctionalException p) {
            tipoLayout.setListaNovosLayouts(new ArrayList<SelectItem>());
        }
    }

    /**
     * Nome: getComboTipoMontagem
     * 
     * @exception
     * @param
     * @return
     * @throws
     * @see
     */
    public void comboTipoMontagem() {

        comboTipoMontagem = getTipoMontagem();

        if (!"PTRB".equals(getDsTipoLayout())) {
            Integer codSelecionado = getCdTipoArquivoRetorno();
            if (codSelecionado != null) {
                if ((codSelecionado >= 1 && codSelecionado <= 5)
                                || codSelecionado == 23) {
                    comboTipoMontagem.add(new SelectItem(TIPO_SERVICO
                        .getCdTipoMontagem(), TIPO_SERVICO
                        .getDsTipoMontagem()));
                }
            }
        }		
        validarCodigoTipoArquivoRetornoIncluir();		
    }

    /**
     * Nome: getTipoMontagem
     * 
     * @exception
     * @param
     * @return
     * @throws
     * @see
     */
    public ArrayList<SelectItem> getTipoMontagem() {
        ArrayList<SelectItem> tipoMontagem = new ArrayList<SelectItem>();

        tipoMontagem.add(new SelectItem(SELECIONE.getCdTipoMontagem(),
            SELECIONE.getDsTipoMontagem()));
        tipoMontagem.add(new SelectItem(REMESSA.getCdTipoMontagem(),
            REMESSA.getDsTipoMontagem()));
        tipoMontagem.add(new SelectItem(AGENCIA_CONTA.getCdTipoMontagem(),
            AGENCIA_CONTA.getDsTipoMontagem()));
        tipoMontagem.add(new SelectItem(TIPO_SERVICO_MODALIDADE
            .getCdTipoMontagem(), TIPO_SERVICO_MODALIDADE
            .getDsTipoMontagem()));
        tipoMontagem.add(new SelectItem(REMESSA_TIPO_SERVICO_MODALIDADE
            .getCdTipoMontagem(), REMESSA_TIPO_SERVICO_MODALIDADE
            .getDsTipoMontagem()));
        tipoMontagem.add(new SelectItem(SEM_SEPARACAO.getCdTipoMontagem(),
            SEM_SEPARACAO.getDsTipoMontagem()));

        return tipoMontagem;
    }

    /**
     * Voltar servico.
     * 
     * @return the string
     */
    public String voltarServico() {
        for (int i = 0; i < getListaGridPesquisaServico().size(); i++) {
            getListaGridPesquisaServico().get(i).setCheck(false);
        }
        setHabilitarServicoAlterar(false);
        return "VOLTAR_SERVICO";
    }

    /**
     * Avancar servico.
     * 
     * @return the string
     */
    public String avancarServico() {
        for (int i = 0; i < getListaGridPesquisaServicoSelecionados().size(); i++) {
            ListarTipoLayoutContratoSaidaDTO tipoLayoutCorrente = getListaGridPesquisaServicoSelecionados().get(i);
            String descricaoNovoLayout =
                SelectItemUtils.obterLabelSelecItem(tipoLayoutCorrente.getListaNovosLayouts(), tipoLayoutCorrente
                    .getCdNovoLayout());
            tipoLayoutCorrente.setDsNovoLayout(descricaoNovoLayout);
        }

        return "AVANCAR_SERVICO";
    }

    /**
     * Voltar alterar servico.
     * 
     * @return the string
     */
    public String voltarAlterarServico() {
        return "VOLTAR_ALTERAR";
    }

    /**
     * Confirmar alterar servico.
     * 
     * @return the string
     */
    public String confirmarAlterarServico() {

        try {

            AlterarLayoutServicoEntradaDTO alterarLayoutServicoEntradaDTO = new AlterarLayoutServicoEntradaDTO();

            AlterarLayoutServicoListEntradaDTO entradaList = new AlterarLayoutServicoListEntradaDTO();

            List<AlterarLayoutServicoListEntradaDTO> lista = new ArrayList<AlterarLayoutServicoListEntradaDTO>();

            /* Dados do layout */

            for (int i = 0; i < listaGridPesquisaServicoSelecionados.size(); i++) {
                entradaList = new AlterarLayoutServicoListEntradaDTO();
                entradaList
                .setCdRelacionamentoProdutoProduto(getListaGridPesquisaServicoSelecionados()
                    .get(i).getCdRelacionamentoProdutoProduto());
                entradaList
                .setCdTipoLayout(getListaGridPesquisaServicoSelecionados()
                    .get(i).getCdTipoLayout());
                entradaList
                .setCdTipoServico(getListaGridPesquisaServicoSelecionados()
                    .get(i).getCdTipoServico());
                entradaList
                .setCdTipoLayoutNovo(getListaGridPesquisaServicoSelecionados()
                    .get(i).getCdNovoLayout());

                lista.add(entradaList);
            }
            alterarLayoutServicoEntradaDTO.setListaLayoutServicos(lista);

            /* Dados do Contrato */
            alterarLayoutServicoEntradaDTO
            .setCdPessoaJuridica(this.cdPessoaJuridica);
            alterarLayoutServicoEntradaDTO.setNrSequenciaContrato(Long
                .parseLong(this.nrSequenciaContratoNegocio));
            alterarLayoutServicoEntradaDTO
            .setCdTipoContrato(this.cdTipoContratoNegocio);

            AlterarLayoutServicoSaidaDTO alterarLayoutServicoSaidaDTO = getManterContratoImpl()
            .alterarLayoutServico(alterarLayoutServicoEntradaDTO);
            BradescoFacesUtils.addInfoModalMessage("("
                + alterarLayoutServicoSaidaDTO.getCodMensagem() + ") "
                + alterarLayoutServicoSaidaDTO.getMensagem(),
                LAYOUTS_ARQUIVOS, BradescoViewExceptionActionType.ACTION,
                false);
                carregaListaLayoutRetorno();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }

        return "";
    }

    /**
     * Buscar centro custo.
     */
    public void buscarCentroCusto() {

        if (getSubstituirCentroCusto() != null
                        && !getSubstituirCentroCusto().equals("")) {

            this.listaCentroCustoSubstituir = new ArrayList<SelectItem>();

            List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
            CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();

            centroCustoEntradaDTO.setCdSistema(getSubstituirCentroCusto());

            listaCentroCustoSaida = comboService
            .listarCentroCusto(centroCustoEntradaDTO);

            for (CentroCustoSaidaDTO combo : listaCentroCustoSaida) {
                this.listaCentroCustoSubstituir.add(new SelectItem(combo
                    .getCdCentroCusto(), combo.getCdCentroCusto() + "-"
                    + combo.getDsCentroCusto()));
            }
        } else {
            this.listaCentroCustoSubstituir = new ArrayList<SelectItem>();
        }
    }

    /**
     * Carrega lista layout.
     */
    public void carregaListaLayout() {

        ConsultarListaLayoutArquivoContratoEntradaDTO listaLayoutDTO = new ConsultarListaLayoutArquivoContratoEntradaDTO();

        // atributos de entrada.
        listaLayoutDTO
        .setCdPessoaJuridicaContrato(getCdPessoaJuridica() != null ? getCdPessoaJuridica()
            : 0);
        listaLayoutDTO
        .setCdTipoContratoNegocio(getCdTipoContratoNegocio() != null ? getCdTipoContratoNegocio()
            : 0);
        listaLayoutDTO
        .setNrSequenciaContratoNegocio(Long
            .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                            .equals("")) ? getNrSequenciaContratoNegocio()
                                : "0"));
        listaLayoutDTO.setCdProduto(0);

        setListaLayout(getManterContratoImpl()
            .consultarListaLayoutArquivoContrato(listaLayoutDTO));

        this.listaControleLayout = new ArrayList<SelectItem>();

        for (int i = 0; i < getListaLayout().size(); i++) {
            this.listaControleLayout.add(new SelectItem(i, " "));
        }

        setItemSelecionadoListaLayout(null);

    }

    /**
     * Carrega lista layout.
     */
    public void carregaListaLayoutRetorno() {

        try {
            ConsultarListaLayoutArquivoContratoEntradaDTO listaLayoutDTO = 
                new ConsultarListaLayoutArquivoContratoEntradaDTO();

            // atributos de entrada.
            listaLayoutDTO
            .setCdPessoaJuridicaContrato(getCdPessoaJuridica() != null ? getCdPessoaJuridica()
                : 0);
            listaLayoutDTO
            .setCdTipoContratoNegocio(getCdTipoContratoNegocio() != null ? getCdTipoContratoNegocio()
                : 0);
            listaLayoutDTO
            .setNrSequenciaContratoNegocio(Long
                .parseLong((getNrSequenciaContratoNegocio() != null && !getNrSequenciaContratoNegocio()
                                .equals("")) ? getNrSequenciaContratoNegocio()
                                    : "0"));
            listaLayoutDTO.setCdProduto(0);

            setListaLayout(getManterContratoImpl()
                .consultarListaLayoutArquivoContrato(listaLayoutDTO));

            this.listaControleLayout = new ArrayList<SelectItem>();

            for (int i = 0; i < getListaLayout().size(); i++) {
                this.listaControleLayout.add(new SelectItem(i, " "));
            }

            setItemSelecionadoListaLayout(null);

        } catch (PdcAdapterFunctionalException p) {
            if (!"PGIT0003".equals(StringUtils.right(p.getCode(), 8))) {
                BradescoFacesUtils.addInfoModalMessage("("
                    + StringUtils.right(p.getCode(), 8) + ") "
                    + p.getMessage(), false);
            }
            setListaLayout(new ArrayList<ConsultarListaLayoutArquivoContratoSaidaDTO>());
        }
    }

    /**
     * Preenche tipo layout arquivo.
     * 
     * @return the list< select item>
     */
    public List<SelectItem> preencheTipoLayoutArquivo() {
        listaTipoLayoutArquivo = new ArrayList<SelectItem>();
        List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
        TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();
        tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);

        saidaTipoLayoutArquivoSaidaDTO = comboService
        .listarTipoLayoutArquivo(tipoLoteEntradaDTO);
        listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
        listaTipoLayoutArquivoHash.clear();
        for (TipoLayoutArquivoSaidaDTO combo : listaTipoLayout) {
            listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(),
                combo.getDsTipoLayoutArquivo());
            listaTipoLayoutArquivo.add(new SelectItem(combo
                .getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
        }

        return listaTipoLayoutArquivo;
    }

    /**
     * Preenche meio transmissao principal.
     * 
     * @return the list< select item>
     */
    public List<SelectItem> preencheMeioTransmissaoPrincipal() {
        listaMeioTransmissaoRemessaPrincipal = new ArrayList<SelectItem>();
        List<ListarMeioTransmissaoSaidaDTO> listaMeioTransmissao = new ArrayList<ListarMeioTransmissaoSaidaDTO>();
        ListarMeioTransmissaoEntradaDTO entrada = new ListarMeioTransmissaoEntradaDTO();
        entrada.setCdSituacaoVinculacaoConta(0);

        listaMeioTransmissao = comboService.listarMeioTransmissao(entrada);

        listaMeioTransmissaoRemessaPrincipalHash.clear();
        for (ListarMeioTransmissaoSaidaDTO combo : listaMeioTransmissao) {
            listaMeioTransmissaoRemessaPrincipalHash.put(combo
                .getCdTipoMeioTransmissao(), combo
                .getDsTipoMeioTransmissao());
            listaMeioTransmissaoRemessaPrincipal.add(new SelectItem(combo
                .getCdTipoMeioTransmissao(), combo
                .getDsTipoMeioTransmissao()));
        }

        return listaMeioTransmissaoRemessaPrincipal;
    }

    /**
     * Voltar detalhe historico.
     * 
     * @return the string
     */
    public String voltarDetalheHistorico() {
        setItemSelecionadoListaHistoricoLayout(null);
        return "VOLTAR";
    }

    /**
     * Voltar excluir layout.
     * 
     * @return the string
     */
    public String voltarExcluirLayout() {
        setItemSelecionadoListaLayout(null);
        return "VOLTAR";
    }

    /**
     * Voltar incluir layout.
     * 
     * @return the string
     */
    public String voltarIncluirLayout() {
        setItemSelecionadoListaLayout(null);
        return "VOLTAR";
    }

    /**
     * Limpar retorno.
     */
    public void limparRetorno() {
        // desmarca grid de retorno
        setItemSelecionadoListaRetorno(null);
    }

    /**
     * Limpar.
     */
    public void limpar() {
        // desmarca grid
        setItemSelecionadoListaLayout(null);
    }

    /**
     * Monta descricao.
     */
    private void montaDescricao() {
        this.setDsTipoArquivoRetorno((String) listaTipoArquivoRetornoHash
            .get(getCdTipoArquivoRetorno()));
    }

    // TODO adequar combos aos met�dos de preenchimento corretos.
    // *******************

    /**
     * Listar tipo arquivo retorno.
     */
    private void listarTipoArquivoRetorno() {

        // preenche combo com tipo de retornos.
        this.listaTipoArquivoRetorno = new ArrayList<SelectItem>();
        ListarTipoArquivoRetornoEntradaDTO entrada = new ListarTipoArquivoRetornoEntradaDTO();
        entrada.setCdTipoLayout(getCdTipoLayout());
        List<ListarTipoArquivoRetornoSaidaDTO> listarTipoArquivoRetornoSaidaDTO = new ArrayList<ListarTipoArquivoRetornoSaidaDTO>();

        listarTipoArquivoRetornoSaidaDTO = this.getComboService()
        .listarTipoArquivoRetorno(entrada);
        listaTipoArquivoRetornoHash.clear();
        for (ListarTipoArquivoRetornoSaidaDTO combo : listarTipoArquivoRetornoSaidaDTO) {
            listaTipoArquivoRetornoHash.put(combo.getCdTipoArquivoRetorno(),
                combo.getDsTipoArquivoRetorno());
            listaTipoArquivoRetorno
            .add(new SelectItem(combo.getCdTipoArquivoRetorno(), combo
                .getDsTipoArquivoRetorno()));
        }
    }

    /**
     * Alterar remessa.
     * 
     * @return the string
     */
    public String alterarRemessa() {
        // altLayoutsArquivos1
        this.recuperaDadosLayout();

        // Carrega os Combos
        listarTipoArquivoRetorno();
        setTelaDetalhe(false);
        carregarDescricoes();

        return ALTERAR_JSP;
    }

    /**
     * Avancar alterar.
     * 
     * @return the string
     */
    public String avancarAlterar() {
        if (getHiddenObrigatoriedade().equals("T")) {
            montaDescricao();
            if (getCdTpRejeicAcolimento() == 2) {
                setPercRegInconsistentes(BigDecimal.ZERO);
            }
            return ALTERAR_JSP;
        } else {
            if (getCdTpRejeicAcolimento() == 2) {
                setPercRegInconsistentes(BigDecimal.ZERO);
            }
            return "";
        }
    }

    /**
     * Preenche pesquisa servico selecionados.
     */
    public void preenchePesquisaServicoSelecionados() {
        listaGridPesquisaServicoSelecionados = new ArrayList<ListarTipoLayoutContratoSaidaDTO>();

        // Corre a lista pegando apenas os registros selecionados
        for (int i = 0; i < listaGridPesquisaServico.size(); i++) {
            if (listaGridPesquisaServico.get(i).isCheck()) {
                listaGridPesquisaServicoSelecionados
                .add(listaGridPesquisaServico.get(i));
            }
        }

        this.setTamanhoLista(listaGridPesquisaServicoSelecionados.size());
    }

    /**
     * Habilita botao alterar servico.
     */
    public void habilitaBotaoAlterarServico() {
        setHabilitarServicoAlterar(false);

        for (int i = 0; i < getListaGridPesquisaServico().size(); i++) {
            if (getListaGridPesquisaServico().get(i).isCheck()) {
                setHabilitarServicoAlterar(true);
            }
        }
    }

    /**
     * Habilita botao alterar layout.
     */
    public void habilitaBotaoAlterarLayout() {
        Integer layout = getListaLayout().get(itemSelecionadoListaLayout).getCdTipoLayout();

        setItemSelecionadoListaLayoutAltera(true);
        
        if (layout != null && layout != 0) {
            if (getCdAcessoDepartamento() != null && getCdAcessoDepartamento() == 1) {
                if ((layout >= 2 && layout <= 5) || layout == 14) {
                    setItemSelecionadoListaLayoutAltera(false);
                }
            }
        }
    }

    /**
     * Habilita Numero Vers�o Layout Arquivo.
     */
    public boolean isHabilitaNumVersLayoutArq() {
        if (layout != 14) {
            return false;
        } else {
            return true;
        }
    }
    

    /**
     * Habilita Numero Vers�o Layout Arquivo Alterar.
     */
    public boolean isHabilitaNumVersLayoutArqAlt() {
        if (listaLayout != null && itemSelecionadoListaLayout != null) {
            if (listaLayout.get(itemSelecionadoListaLayout).getCdTipoLayout() != 14) {
                return false;
            }
        }
        return true;
    }

    /**
     * Habilita Numero Vers�o Layout Arquivo Excluir.
     */
    public boolean isHabilitaNumVersLayoutArqExc() {
        if (listaLayout != null && itemSelecionadoListaLayout != null &&
                        listaLayout.get(itemSelecionadoListaLayout).getCdTipoLayout() != 14) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Habilita Numero Vers�o Layout Arquivo detalhar.
     */
    public boolean isHabilitaNumVersLayoutArqDet() {
        if (listaLayout.get(itemSelecionadoListaLayout).getCdTipoLayout() != 14) {
            return false;
        } else {
            return true;
        }
    }

    /*
     * RETIRADO ESSE BOTAO DA PRIMEIRA TELA public String substituir(){
     * //Preencher combos preencheMeioTransmissaoPrincipal();
     * preencheTipoLayoutArquivo(); carregaListaPeriodicidade();
     * 
     * //Preenche valores da Tela de Substituir com os valores atuais.
     * carregarDescricoes();
     * 
     * return "SUBSTITUIR"; }
     * 
     * public String avancarSubstituir(){ setSubstituirLayoutDesc( (String)
     * getListaTipoLayoutArquivoHash().get(getSubstituirLayout()));
     * setPrincipalRemessaDesc( (String)
     * getListaMeioTransmissaoRemessaPrincipalHash
     * ().get(getPrincipalRemessa())); setPrincipalRetornoDesc( (String)
     * getListaMeioTransmissaoRemessaPrincipalHash
     * ().get(getPrincipalRetorno())); setAlternativoRemessaDesc( (String)
     * getListaMeioTransmissaoRemessaPrincipalHash
     * ().get(getAlternativoRemessa())); setAlternativoRetornoDesc( (String)
     * getListaMeioTransmissaoRemessaPrincipalHash
     * ().get(getAlternativoRetorno())); setCdPeriodicidadeDesc((String)
     * getListaPeriodicidadeHash().get(getCdPeriodicidade()));
     * 
     * return "SUBSTITUIR"; }
     * 
     * public String confirmarSubstituir(){ try {
     * SubstituirLayoutArqContratoEntradaDTO entrada = new
     * SubstituirLayoutArqContratoEntradaDTO();
     * 
     * //Dados do Contrato
     * entrada.setCdPessoaJuridicaContrato(this.cdPessoaJuridica!=
     * null?this.cdPessoaJuridica:0);
     * entrada.setNrSequenciaContratoNegocio(Long.parseLong(
     * (getNrSequenciaContratoNegocio()!= null ||
     * getNrSequenciaContratoNegocio()
     * .equals(""))?getNrSequenciaContratoNegocio(): "0"));
     * entrada.setCdTipoContratoNegocio(this.cdTipoContratoNegocio !=
     * null?this.cdTipoContratoNegocio:0);
     * 
     * entrada.setCdTipoLayoutArquivo(getCdTipoLayoutArquivoAtual() != null ?
     * getCdTipoLayoutArquivoAtual() : 0);
     * entrada.setCdTipoLayoutArquivoNovo(getSubstituirLayout() != null ?
     * getSubstituirLayout() : 0);
     * entrada.setCdSistema(getSubstituirCentroCustoSelecionado() != null &&
     * !getSubstituirCentroCustoSelecionado().equals("") ?
     * getSubstituirCentroCustoSelecionado() : "");
     * entrada.setCdAplicacaoTransmissaoArquivo(getAplicativoFormatacao() !=
     * null ? getAplicativoFormatacao() : 0);
     * entrada.setCdSerieAplicacaoTransmissao
     * (getCdSerieAplicacaoTransmissaoSubstituir() != null &&
     * !getCdSerieAplicacaoTransmissaoSubstituir().equals("") ?
     * getCdSerieAplicacaoTransmissaoSubstituir() : "");
     * entrada.setCdNivelControleRemessa(getCdNivelControle() != null ?
     * getCdNivelControle() : 0);
     * entrada.setCdControleNumeroRemessa(getCdTipoControle() != null ?
     * getCdTipoControle() : 0);
     * entrada.setCdPerdcContagemRemessa(getCdPeriodicidade()!= null ?
     * getCdPeriodicidade() : 0);
     * entrada.setNrMaximoContagemRemessa((getDsNrMaxRemessa() != null &&
     * !getDsNrMaxRemessa().equals(""))? Long.parseLong(getDsNrMaxRemessa()) :
     * 0); entrada.setCdRejeicaoAcolhimentoRemessa(getCdTpRejeicAcolimento() !=
     * null ? getCdTpRejeicAcolimento() : 0);
     * entrada.setCdPercentualRejeicaoRemessa(getSubPercRegInconsistentes() !=
     * null && !getSubPercRegInconsistentes().equals("") ?
     * getSubPercRegInconsistentes() : BigDecimal.ZERO);
     * entrada.setCdMeioPrincipalRetorno(getPrincipalRetorno() != null ?
     * getPrincipalRetorno() : 0);
     * entrada.setCdMeioAlternativoRetorno(getAlternativoRetorno() != null ?
     * getAlternativoRetorno() : 0);
     * entrada.setCdmeioAlternativoRemessa(getAlternativoRemessa()!= null ?
     * getAlternativoRemessa() : 0);
     * entrada.setCdMeioPrincipalRemessa(getPrincipalRemessa() != null ?
     * getPrincipalRemessa() : 0); entrada.setDsNomeCliente(getUsername());
     * entrada.setDsNomeArquivoRemessa(getDsNomeArquivoRemessa());
     * entrada.setDsNomeArquivoRetorno(getDsNomeArquivoRetorno());
     * entrada.setDsEmpresaTratamentoArquivo
     * (getEmpresaResponsavelTransmissao());
     * 
     * SubstituirLayoutArqContratoSaidaDTO saidaDTO =
     * manterContratoImpl.substituirLayoutArqContrato(entrada);
     * 
     * BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() +
     * ") " + saidaDTO.getMensagem(), LAYOUTS_ARQUIVOS,
     * BradescoViewExceptionActionType.ACTION, false);
     * 
     * carregaListaLayout();
     * 
     * } catch (PdcAdapterFunctionalException p) {
     * BradescoFacesUtils.addInfoModalMessage("(" +
     * StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false); return
     * null; }
     * 
     * return "ok"; }
     */

    // Fim

    // GETTERS E SETTERS
    /**
     * Get: comboService.
     * 
     * @return comboService
     */
    public IComboService getComboService() {
        return comboService;
    }

    /**
     * Set: comboService.
     * 
     * @param comboService
     *            the combo service
     */
    public void setComboService(IComboService comboService) {
        this.comboService = comboService;
    }

    /**
     * Get: manterContratoImpl.
     * 
     * @return manterContratoImpl
     */
    public IManterContratoService getManterContratoImpl() {
        return manterContratoImpl;
    }

    /**
     * Set: manterContratoImpl.
     * 
     * @param manterContratoImpl
     *            the manter contrato impl
     */
    public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
        this.manterContratoImpl = manterContratoImpl;
    }

    /**
     * Get: itemSelecionadoListaLayout.
     * 
     * @return itemSelecionadoListaLayout
     */
    public Integer getItemSelecionadoListaLayout() {
        return itemSelecionadoListaLayout;
    }

    /**
     * Set: itemSelecionadoListaLayout.
     * 
     * @param itemSelecionadoListaLayout
     *            the item selecionado lista layout
     */
    public void setItemSelecionadoListaLayout(Integer itemSelecionadoListaLayout) {
        this.itemSelecionadoListaLayout = itemSelecionadoListaLayout;
    }

    /**
     * Get: listaControleLayout.
     * 
     * @return listaControleLayout
     */
    public List<SelectItem> getListaControleLayout() {
        return listaControleLayout;
    }

    /**
     * Set: listaControleLayouts.
     * 
     * @param listaControleLayout
     *            the lista controle layouts
     */
    public void setListaControleLayouts(List<SelectItem> listaControleLayout) {
        this.listaControleLayout = listaControleLayout;
    }

    /**
     * Get: itemSelecionadoListaRetorno.
     * 
     * @return itemSelecionadoListaRetorno
     */
    public Integer getItemSelecionadoListaRetorno() {
        return itemSelecionadoListaRetorno;
    }

    /**
     * Set: itemSelecionadoListaRetorno.
     * 
     * @param itemSelecionadoListaRetorno
     *            the item selecionado lista retorno
     */
    public void setItemSelecionadoListaRetorno(
        Integer itemSelecionadoListaRetorno) {
        this.itemSelecionadoListaRetorno = itemSelecionadoListaRetorno;
    }

    /**
     * Get: listaControleRetorno.
     * 
     * @return listaControleRetorno
     */
    public List<SelectItem> getListaControleRetorno() {
        return listaControleRetorno;
    }

    /**
     * Set: listaControleRetorno.
     * 
     * @param listaControleRetorno
     *            the lista controle retorno
     */
    public void setListaControleRetorno(List<SelectItem> listaControleRetorno) {
        this.listaControleRetorno = listaControleRetorno;
    }

    /**
     * Set: listaLayout.
     * 
     * @param listaLayout
     *            the lista layout
     */
    public void setListaLayout(
        List<ConsultarListaLayoutArquivoContratoSaidaDTO> listaLayout) {
        this.listaLayout = listaLayout;
    }

    /**
     * Get: cdPessoaJuridica.
     * 
     * @return cdPessoaJuridica
     */
    public Long getCdPessoaJuridica() {
        return cdPessoaJuridica;
    }

    /**
     * Set: cdPessoaJuridica.
     * 
     * @param cdPessoaJuridica
     *            the cd pessoa juridica
     */
    public void setCdPessoaJuridica(Long cdPessoaJuridica) {
        this.cdPessoaJuridica = cdPessoaJuridica;
    }

    /**
     * Get: cdTipoContratoNegocio.
     * 
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     * 
     * @param cdTipoContratoNegocio
     *            the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: nrSequenciaContratoNegocio.
     * 
     * @return nrSequenciaContratoNegocio
     */
    public String getNrSequenciaContratoNegocio() {
        return nrSequenciaContratoNegocio;
    }

    /**
     * Set: nrSequenciaContratoNegocio.
     * 
     * @param nrSequenciaContratoNegocio
     *            the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(String nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Get: itemSelecionadoListaHistoricoLayout.
     * 
     * @return itemSelecionadoListaHistoricoLayout
     */
    public Integer getItemSelecionadoListaHistoricoLayout() {
        return itemSelecionadoListaHistoricoLayout;
    }

    /**
     * Set: itemSelecionadoListaHistoricoLayout.
     * 
     * @param itemSelecionadoListaHistoricoLayout
     *            the item selecionado lista historico layout
     */
    public void setItemSelecionadoListaHistoricoLayout(
        Integer itemSelecionadoListaHistoricoLayout) {
        this.itemSelecionadoListaHistoricoLayout = itemSelecionadoListaHistoricoLayout;
    }

    /**
     * Get: listaControleHistoricoLayout.
     * 
     * @return listaControleHistoricoLayout
     */
    public List<SelectItem> getListaControleHistoricoLayout() {
        return listaControleHistoricoLayout;
    }

    /**
     * Set: listaControleHistoricoLayout.
     * 
     * @param listaControleHistoricoLayout
     *            the lista controle historico layout
     */
    public void setListaControleHistoricoLayout(
        List<SelectItem> listaControleHistoricoLayout) {
        this.listaControleHistoricoLayout = listaControleHistoricoLayout;
    }

    /**
     * Get: listaHistoricoLayout.
     * 
     * @return listaHistoricoLayout
     */
    public List<ListarConManLayoutSaidaDTO> getListaHistoricoLayout() {
        return listaHistoricoLayout;
    }

    /**
     * Set: listaHistoricoLayout.
     * 
     * @param listaHistoricoLayout
     *            the lista historico layout
     */
    public void setListaHistoricoLayout(
        List<ListarConManLayoutSaidaDTO> listaHistoricoLayout) {
        this.listaHistoricoLayout = listaHistoricoLayout;
    }

    /**
     * Get: listaLayout.
     * 
     * @return listaLayout
     */
    public List<ConsultarListaLayoutArquivoContratoSaidaDTO> getListaLayout() {
        return listaLayout;
    }

    /**
     * Set: listaControleLayout.
     * 
     * @param listaControleLayout
     *            the lista controle layout
     */
    public void setListaControleLayout(List<SelectItem> listaControleLayout) {
        this.listaControleLayout = listaControleLayout;
    }

    /**
     * Get: listaRetorno.
     * 
     * @return listaRetorno
     */
    public List<ConsultarListaTipoRetornoLayoutSaidaDTO> getListaRetorno() {
        return listaRetorno;
    }

    /**
     * Set: listaRetorno.
     * 
     * @param listaRetorno
     *            the lista retorno
     */
    public void setListaRetorno(
        List<ConsultarListaTipoRetornoLayoutSaidaDTO> listaRetorno) {
        this.listaRetorno = listaRetorno;
    }

    /**
     * Get: dataInclusaoLayout.
     * 
     * @return dataInclusaoLayout
     */
    public String getDataInclusaoLayout() {
        return dataInclusaoLayout;
    }

    /**
     * Set: dataInclusaoLayout.
     * 
     * @param dataInclusaoLayout
     *            the data inclusao layout
     */
    public void setDataInclusaoLayout(String dataInclusaoLayout) {
        this.dataInclusaoLayout = dataInclusaoLayout;
    }

    /**
     * Get: cdTipoLayout.
     * 
     * @return cdTipoLayout
     */
    public Integer getCdTipoLayout() {
        return cdTipoLayout;
    }

    /**
     * Set: cdTipoLayout.
     * 
     * @param cdTipoLayout
     *            the cd tipo layout
     */
    public void setCdTipoLayout(Integer cdTipoLayout) {
        this.cdTipoLayout = cdTipoLayout;
    }

    /**
     * Get: dsTipoLayout.
     * 
     * @return dsTipoLayout
     */
    public String getDsTipoLayout() {
        return dsTipoLayout;
    }

    /**
     * Set: dsTipoLayout.
     * 
     * @param dsTipoLayout
     *            the ds tipo layout
     */
    public void setDsTipoLayout(String dsTipoLayout) {
        this.dsTipoLayout = dsTipoLayout;
    }

    /**
     * Get: cdTipoArquivoRetorno.
     * 
     * @return cdTipoArquivoRetorno
     */
    public Integer getCdTipoArquivoRetorno() {
        return cdTipoArquivoRetorno;
    }

    /**
     * Set: cdTipoArquivoRetorno.
     * 
     * @param cdTipoArquivoRetorno
     *            the cd tipo arquivo retorno
     */
    public void setCdTipoArquivoRetorno(Integer cdTipoArquivoRetorno) {
        this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
    }

    /**
     * Get: listaTipoArquivoRetorno.
     * 
     * @return listaTipoArquivoRetorno
     */
    public List<SelectItem> getListaTipoArquivoRetorno() {
        return listaTipoArquivoRetorno;
    }

    /**
     * Set: listaTipoArquivoRetorno.
     * 
     * @param listaTipoArquivoRetorno
     *            the lista tipo arquivo retorno
     */
    public void setListaTipoArquivoRetorno(
        List<SelectItem> listaTipoArquivoRetorno) {
        this.listaTipoArquivoRetorno = listaTipoArquivoRetorno;
    }

    /**
     * Get: listaTipoArquivoRetornoHash.
     * 
     * @return listaTipoArquivoRetornoHash
     */
    Map<Integer, String> getListaTipoArquivoRetornoHash() {
        return listaTipoArquivoRetornoHash;
    }

    /**
     * Set lista tipo arquivo retorno hash.
     * 
     * @param listaTipoArquivoRetornoHash
     *            the lista tipo arquivo retorno hash
     */
    void setListaTipoArquivoRetornoHash(
        Map<Integer, String> listaTipoArquivoRetornoHash) {
        this.listaTipoArquivoRetornoHash = listaTipoArquivoRetornoHash;
    }

    /**
     * Get: listaNivelControle.
     * 
     * @return listaNivelControle
     */
    public List<SelectItem> getListaNivelControle() {
        return listaNivelControle;
    }

    /**
     * Set: listaNivelControle.
     * 
     * @param listaNivelControle
     *            the lista nivel controle
     */
    public void setListaNivelControle(List<SelectItem> listaNivelControle) {
        this.listaNivelControle = listaNivelControle;
    }

    /**
     * Get: listaNivelControleHash.
     * 
     * @return listaNivelControleHash
     */
    Map<Integer, String> getListaNivelControleHash() {
        return listaNivelControleHash;
    }

    /**
     * Set lista nivel controle hash.
     * 
     * @param listaNivelControleHash
     *            the lista nivel controle hash
     */
    void setListaNivelControleHash(Map<Integer, String> listaNivelControleHash) {
        this.listaNivelControleHash = listaNivelControleHash;
    }

    /**
     * Get: listaTipoControle.
     * 
     * @return listaTipoControle
     */
    public List<SelectItem> getListaTipoControle() {
        return listaTipoControle;
    }

    /**
     * Set: listaTipoControle.
     * 
     * @param listaTipoControle
     *            the lista tipo controle
     */
    public void setListaTipoControle(List<SelectItem> listaTipoControle) {
        this.listaTipoControle = listaTipoControle;
    }

    /**
     * Get: listaTipoControleHash.
     * 
     * @return listaTipoControleHash
     */
    Map<Integer, String> getListaTipoControleHash() {
        return listaTipoControleHash;
    }

    /**
     * Set lista tipo controle hash.
     * 
     * @param listaTipoControleHash
     *            the lista tipo controle hash
     */
    void setListaTipoControleHash(Map<Integer, String> listaTipoControleHash) {
        this.listaTipoControleHash = listaTipoControleHash;
    }

    /**
     * Get: dsNivelControle.
     * 
     * @return dsNivelControle
     */
    public String getDsNivelControle() {
        return dsNivelControle;
    }

    /**
     * Set: dsNivelControle.
     * 
     * @param dsNivelControle
     *            the ds nivel controle
     */
    public void setDsNivelControle(String dsNivelControle) {
        this.dsNivelControle = dsNivelControle;
    }

    /**
     * Get: dsPeriodicidade.
     * 
     * @return dsPeriodicidade
     */
    public String getDsPeriodicidade() {
        return dsPeriodicidade;
    }

    /**
     * Set: dsPeriodicidade.
     * 
     * @param dsPeriodicidade
     *            the ds periodicidade
     */
    public void setDsPeriodicidade(String dsPeriodicidade) {
        this.dsPeriodicidade = dsPeriodicidade;
    }

    /**
     * Get: dsTipoArquivoRetorno.
     * 
     * @return dsTipoArquivoRetorno
     */
    public String getDsTipoArquivoRetorno() {
        return dsTipoArquivoRetorno;
    }

    /**
     * Set: dsTipoArquivoRetorno.
     * 
     * @param dsTipoArquivoRetorno
     *            the ds tipo arquivo retorno
     */
    public void setDsTipoArquivoRetorno(String dsTipoArquivoRetorno) {
        this.dsTipoArquivoRetorno = dsTipoArquivoRetorno;
    }

    /**
     * Get: dsTipoControle.
     * 
     * @return dsTipoControle
     */
    public String getDsTipoControle() {
        return dsTipoControle;
    }

    /**
     * Set: dsTipoControle.
     * 
     * @param dsTipoControle
     *            the ds tipo controle
     */
    public void setDsTipoControle(String dsTipoControle) {
        this.dsTipoControle = dsTipoControle;
    }

    /**
     * Get: dsAppFormatacao.
     * 
     * @return dsAppFormatacao
     */
    public String getDsAppFormatacao() {
        return dsAppFormatacao;
    }

    /**
     * Set: dsAppFormatacao.
     * 
     * @param dsAppFormatacao
     *            the ds app formatacao
     */
    public void setDsAppFormatacao(String dsAppFormatacao) {
        this.dsAppFormatacao = dsAppFormatacao;
    }

    /**
     * Get: dsCentroCusto.
     * 
     * @return dsCentroCusto
     */
    public String getDsCentroCusto() {
        return dsCentroCusto;
    }

    /**
     * Set: dsCentroCusto.
     * 
     * @param dsCentroCusto
     *            the ds centro custo
     */
    public void setDsCentroCusto(String dsCentroCusto) {
        this.dsCentroCusto = dsCentroCusto;
    }

    /**
     * Get: dsLayout.
     * 
     * @return dsLayout
     */
    public String getDsLayout() {
        return dsLayout;
    }

    /**
     * Set: dsLayout.
     * 
     * @param dsLayout
     *            the ds layout
     */
    public void setDsLayout(String dsLayout) {
        this.dsLayout = dsLayout;
    }

    /**
     * Get: dsNomeRemessa.
     * 
     * @return dsNomeRemessa
     */
    public String getDsNomeRemessa() {
        return dsNomeRemessa;
    }

    /**
     * Set: dsNomeRemessa.
     * 
     * @param dsNomeRemessa
     *            the ds nome remessa
     */
    public void setDsNomeRemessa(String dsNomeRemessa) {
        this.dsNomeRemessa = dsNomeRemessa;
    }

    /**
     * Get: dsNrMaxRemessa.
     * 
     * @return dsNrMaxRemessa
     */
    public String getDsNrMaxRemessa() {
        return dsNrMaxRemessa;
    }

    /**
     * Set: dsNrMaxRemessa.
     * 
     * @param dsNrMaxRemessa
     *            the ds nr max remessa
     */
    public void setDsNrMaxRemessa(String dsNrMaxRemessa) {
        this.dsNrMaxRemessa = dsNrMaxRemessa;
    }

    /**
     * Get: dsPercRegInconsistentes.
     * 
     * @return dsPercRegInconsistentes
     */
    public String getDsPercRegInconsistentes() {
        return dsPercRegInconsistentes;
    }

    /**
     * Set: dsPercRegInconsistentes.
     * 
     * @param dsPercRegInconsistentes
     *            the ds perc reg inconsistentes
     */
    public void setDsPercRegInconsistentes(String dsPercRegInconsistentes) {
        this.dsPercRegInconsistentes = dsPercRegInconsistentes;
    }

    /**
     * Get: percRegInconsistentes.
     * 
     * @return percRegInconsistentes
     */
    public BigDecimal getPercRegInconsistentes() {
        return percRegInconsistentes;
    }

    /**
     * Set: percRegInconsistentes.
     * 
     * @param percRegInconsistentes
     *            the perc reg inconsistentes
     */
    public void setPercRegInconsistentes(BigDecimal percRegInconsistentes) {
        this.percRegInconsistentes = percRegInconsistentes;
    }

    /**
     * Get: dsTpRejeicAcolimento.
     * 
     * @return dsTpRejeicAcolimento
     */
    public String getDsTpRejeicAcolimento() {
        return dsTpRejeicAcolimento;
    }

    /**
     * Set: dsTpRejeicAcolimento.
     * 
     * @param dsTpRejeicAcolimento
     *            the ds tp rejeic acolimento
     */
    public void setDsTpRejeicAcolimento(String dsTpRejeicAcolimento) {
        this.dsTpRejeicAcolimento = dsTpRejeicAcolimento;
    }

    /**
     * Get: cdTpRejeicAcolimento.
     * 
     * @return cdTpRejeicAcolimento
     */
    public Integer getCdTpRejeicAcolimento() {
        return cdTpRejeicAcolimento;
    }

    /**
     * Set: cdTpRejeicAcolimento.
     * 
     * @param cdTpRejeicAcolimento
     *            the cd tp rejeic acolimento
     */
    public void setCdTpRejeicAcolimento(Integer cdTpRejeicAcolimento) {
        this.cdTpRejeicAcolimento = cdTpRejeicAcolimento;
    }

    /**
     * Get: dsNivelControleRetorno.
     * 
     * @return dsNivelControleRetorno
     */
    public String getDsNivelControleRetorno() {
        return dsNivelControleRetorno;
    }

    /**
     * Set: dsNivelControleRetorno.
     * 
     * @param dsNivelControleRetorno
     *            the ds nivel controle retorno
     */
    public void setDsNivelControleRetorno(String dsNivelControleRetorno) {
        this.dsNivelControleRetorno = dsNivelControleRetorno;
    }

    /**
     * Get: dsNomeRetorno.
     * 
     * @return dsNomeRetorno
     */
    public String getDsNomeRetorno() {
        return dsNomeRetorno;
    }

    /**
     * Set: dsNomeRetorno.
     * 
     * @param dsNomeRetorno
     *            the ds nome retorno
     */
    public void setDsNomeRetorno(String dsNomeRetorno) {
        this.dsNomeRetorno = dsNomeRetorno;
    }

    /**
     * Get: dsNrMaxRetorno.
     * 
     * @return dsNrMaxRetorno
     */
    public String getDsNrMaxRetorno() {
        return dsNrMaxRetorno;
    }

    /**
     * Set: dsNrMaxRetorno.
     * 
     * @param dsNrMaxRetorno
     *            the ds nr max retorno
     */
    public void setDsNrMaxRetorno(String dsNrMaxRetorno) {
        this.dsNrMaxRetorno = dsNrMaxRetorno;
    }

    /**
     * Get: dsTipoControleRetorno.
     * 
     * @return dsTipoControleRetorno
     */
    public String getDsTipoControleRetorno() {
        return dsTipoControleRetorno;
    }

    /**
     * Set: dsTipoControleRetorno.
     * 
     * @param dsTipoControleRetorno
     *            the ds tipo controle retorno
     */
    public void setDsTipoControleRetorno(String dsTipoControleRetorno) {
        this.dsTipoControleRetorno = dsTipoControleRetorno;
    }

    /**
     * Get: hiddenObrigatoriedade.
     * 
     * @return hiddenObrigatoriedade
     */
    public String getHiddenObrigatoriedade() {
        return hiddenObrigatoriedade;
    }

    /**
     * Set: hiddenObrigatoriedade.
     * 
     * @param hiddenObrigatoriedade
     *            the hidden obrigatoriedade
     */
    public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
        this.hiddenObrigatoriedade = hiddenObrigatoriedade;
    }

    /**
     * Get: dsPeriodicidadeRemessa.
     * 
     * @return dsPeriodicidadeRemessa
     */
    public String getDsPeriodicidadeRemessa() {
        return dsPeriodicidadeRemessa;
    }

    /**
     * Set: dsPeriodicidadeRemessa.
     * 
     * @param dsPeriodicidadeRemessa
     *            the ds periodicidade remessa
     */
    public void setDsPeriodicidadeRemessa(String dsPeriodicidadeRemessa) {
        this.dsPeriodicidadeRemessa = dsPeriodicidadeRemessa;
    }

    /**
     * Get: complementoInclusao.
     * 
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
        return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     * 
     * @param complementoInclusao
     *            the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
        this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     * 
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
        return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     * 
     * @param complementoManutencao
     *            the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
        this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     * 
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     * 
     * @param dataHoraInclusao
     *            the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     * 
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
        return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     * 
     * @param dataHoraManutencao
     *            the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
        this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: tipoCanalInclusao.
     * 
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     * 
     * @param tipoCanalInclusao
     *            the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
        this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     * 
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     * 
     * @param tipoCanalManutencao
     *            the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
        this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     * 
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
        return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     * 
     * @param usuarioInclusao
     *            the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     * 
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
        return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     * 
     * @param usuarioManutencao
     *            the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
        this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: cdAppFormatacao.
     * 
     * @return cdAppFormatacao
     */
    public String getCdAppFormatacao() {
        return cdAppFormatacao;
    }

    /**
     * Set: cdAppFormatacao.
     * 
     * @param cdAppFormatacao
     *            the cd app formatacao
     */
    public void setCdAppFormatacao(String cdAppFormatacao) {
        this.cdAppFormatacao = cdAppFormatacao;
    }

    /**
     * Get: cdSerieAplicacaoTransmissao.
     * 
     * @return cdSerieAplicacaoTransmissao
     */
    public String getCdSerieAplicacaoTransmissao() {
        return cdSerieAplicacaoTransmissao;
    }

    /**
     * Set: cdSerieAplicacaoTransmissao.
     * 
     * @param cdSerieAplicacaoTransmissao
     *            the cd serie aplicacao transmissao
     */
    public void setCdSerieAplicacaoTransmissao(
        String cdSerieAplicacaoTransmissao) {
        this.cdSerieAplicacaoTransmissao = cdSerieAplicacaoTransmissao;
    }

    /**
     * Get: cdControleNumeroRemessa.
     * 
     * @return cdControleNumeroRemessa
     */
    public Integer getCdControleNumeroRemessa() {
        return cdControleNumeroRemessa;
    }

    /**
     * Set: cdControleNumeroRemessa.
     * 
     * @param cdControleNumeroRemessa
     *            the cd controle numero remessa
     */
    public void setCdControleNumeroRemessa(Integer cdControleNumeroRemessa) {
        this.cdControleNumeroRemessa = cdControleNumeroRemessa;
    }

    /**
     * Get: listaTipoLayoutArquivo.
     * 
     * @return listaTipoLayoutArquivo
     */
    public List<SelectItem> getListaTipoLayoutArquivo() {
        return listaTipoLayoutArquivo;
    }

    /**
     * Set: listaTipoLayoutArquivo.
     * 
     * @param listaTipoLayoutArquivo
     *            the lista tipo layout arquivo
     */
    public void setListaTipoLayoutArquivo(
        List<SelectItem> listaTipoLayoutArquivo) {
        this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
    }

    /**
     * Get: listaTipoLayoutArquivoHash.
     * 
     * @return listaTipoLayoutArquivoHash
     */
    Map<Integer, String> getListaTipoLayoutArquivoHash() {
        return listaTipoLayoutArquivoHash;
    }

    /**
     * Set lista tipo layout arquivo hash.
     * 
     * @param listaTipoLayoutArquivoHash
     *            the lista tipo layout arquivo hash
     */
    void setListaTipoLayoutArquivoHash(
        Map<Integer, String> listaTipoLayoutArquivoHash) {
        this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
    }

    /**
     * Get: substituirLayout.
     * 
     * @return substituirLayout
     */
    public Integer getSubstituirLayout() {
        return substituirLayout;
    }

    /**
     * Set: substituirLayout.
     * 
     * @param substituirLayout
     *            the substituir layout
     */
    public void setSubstituirLayout(Integer substituirLayout) {
        this.substituirLayout = substituirLayout;
    }

    /**
     * Get: listaMeioTransmissaoRemessaPrincipal.
     * 
     * @return listaMeioTransmissaoRemessaPrincipal
     */
    public List<SelectItem> getListaMeioTransmissaoRemessaPrincipal() {
        return listaMeioTransmissaoRemessaPrincipal;
    }

    /**
     * Set: listaMeioTransmissaoRemessaPrincipal.
     * 
     * @param listaMeioTransmissaoRemessaPrincipal
     *            the lista meio transmissao remessa principal
     */
    public void setListaMeioTransmissaoRemessaPrincipal(
        List<SelectItem> listaMeioTransmissaoRemessaPrincipal) {
        this.listaMeioTransmissaoRemessaPrincipal = listaMeioTransmissaoRemessaPrincipal;
    }

    /**
     * Get: listaMeioTransmissaoRemessaPrincipalHash.
     * 
     * @return listaMeioTransmissaoRemessaPrincipalHash
     */
    Map<Integer, String> getListaMeioTransmissaoRemessaPrincipalHash() {
        return listaMeioTransmissaoRemessaPrincipalHash;
    }

    /**
     * Set lista meio transmissao remessa principal hash.
     * 
     * @param listaMeioTransmissaoRemessaPrincipalHash
     *            the lista meio transmissao remessa principal hash
     */
    void setListaMeioTransmissaoRemessaPrincipalHash(
        Map<Integer, String> listaMeioTransmissaoRemessaPrincipalHash) {
        this.listaMeioTransmissaoRemessaPrincipalHash = listaMeioTransmissaoRemessaPrincipalHash;
    }

    /**
     * Get: aplicativoFormatacao.
     * 
     * @return aplicativoFormatacao
     */
    public Integer getAplicativoFormatacao() {
        return aplicativoFormatacao;
    }

    /**
     * Set: aplicativoFormatacao.
     * 
     * @param aplicativoFormatacao
     *            the aplicativo formatacao
     */
    public void setAplicativoFormatacao(Integer aplicativoFormatacao) {
        this.aplicativoFormatacao = aplicativoFormatacao;
    }

    /**
     * Get: principalRemessa.
     * 
     * @return principalRemessa
     */
    public Integer getPrincipalRemessa() {
        return principalRemessa;
    }

    /**
     * Set: principalRemessa.
     * 
     * @param principalRemessa
     *            the principal remessa
     */
    public void setPrincipalRemessa(Integer principalRemessa) {
        this.principalRemessa = principalRemessa;
    }

    /**
     * Get: principalRetorno.
     * 
     * @return principalRetorno
     */
    public Integer getPrincipalRetorno() {
        return principalRetorno;
    }

    /**
     * Set: principalRetorno.
     * 
     * @param principalRetorno
     *            the principal retorno
     */
    public void setPrincipalRetorno(Integer principalRetorno) {
        this.principalRetorno = principalRetorno;
    }

    /**
     * Get: alternativoRemessa.
     * 
     * @return alternativoRemessa
     */
    public Integer getAlternativoRemessa() {
        return alternativoRemessa;
    }

    /**
     * Set: alternativoRemessa.
     * 
     * @param alternativoRemessa
     *            the alternativo remessa
     */
    public void setAlternativoRemessa(Integer alternativoRemessa) {
        this.alternativoRemessa = alternativoRemessa;
    }

    /**
     * Get: alternativoRetorno.
     * 
     * @return alternativoRetorno
     */
    public Integer getAlternativoRetorno() {
        return alternativoRetorno;
    }

    /**
     * Set: alternativoRetorno.
     * 
     * @param alternativoRetorno
     *            the alternativo retorno
     */
    public void setAlternativoRetorno(Integer alternativoRetorno) {
        this.alternativoRetorno = alternativoRetorno;
    }

    /**
     * Get: empresaResponsavelTransmissao.
     * 
     * @return empresaResponsavelTransmissao
     */
    public String getEmpresaResponsavelTransmissao() {
        return empresaResponsavelTransmissao;
    }

    /**
     * Set: empresaResponsavelTransmissao.
     * 
     * @param empresaResponsavelTransmissao
     *            the empresa responsavel transmissao
     */
    public void setEmpresaResponsavelTransmissao(
        String empresaResponsavelTransmissao) {
        this.empresaResponsavelTransmissao = empresaResponsavelTransmissao;
    }

    /**
     * Get: substituirLayoutDesc.
     * 
     * @return substituirLayoutDesc
     */
    public String getSubstituirLayoutDesc() {
        return substituirLayoutDesc;
    }

    /**
     * Set: substituirLayoutDesc.
     * 
     * @param substituirLayoutDesc
     *            the substituir layout desc
     */
    public void setSubstituirLayoutDesc(String substituirLayoutDesc) {
        this.substituirLayoutDesc = substituirLayoutDesc;
    }

    /**
     * Get: alternativoRemessaDesc.
     * 
     * @return alternativoRemessaDesc
     */
    public String getAlternativoRemessaDesc() {
        return alternativoRemessaDesc;
    }

    /**
     * Set: alternativoRemessaDesc.
     * 
     * @param alternativoRemessaDesc
     *            the alternativo remessa desc
     */
    public void setAlternativoRemessaDesc(String alternativoRemessaDesc) {
        this.alternativoRemessaDesc = alternativoRemessaDesc;
    }

    /**
     * Get: alternativoRetornoDesc.
     * 
     * @return alternativoRetornoDesc
     */
    public String getAlternativoRetornoDesc() {
        return alternativoRetornoDesc;
    }

    /**
     * Set: alternativoRetornoDesc.
     * 
     * @param alternativoRetornoDesc
     *            the alternativo retorno desc
     */
    public void setAlternativoRetornoDesc(String alternativoRetornoDesc) {
        this.alternativoRetornoDesc = alternativoRetornoDesc;
    }

    /**
     * Get: cdPeriodicidadeDesc.
     * 
     * @return cdPeriodicidadeDesc
     */
    public String getCdPeriodicidadeDesc() {
        return cdPeriodicidadeDesc;
    }

    /**
     * Set: cdPeriodicidadeDesc.
     * 
     * @param cdPeriodicidadeDesc
     *            the cd periodicidade desc
     */
    public void setCdPeriodicidadeDesc(String cdPeriodicidadeDesc) {
        this.cdPeriodicidadeDesc = cdPeriodicidadeDesc;
    }

    /**
     * Get: principalRemessaDesc.
     * 
     * @return principalRemessaDesc
     */
    public String getPrincipalRemessaDesc() {
        return principalRemessaDesc;
    }

    /**
     * Set: principalRemessaDesc.
     * 
     * @param principalRemessaDesc
     *            the principal remessa desc
     */
    public void setPrincipalRemessaDesc(String principalRemessaDesc) {
        this.principalRemessaDesc = principalRemessaDesc;
    }

    /**
     * Get: principalRetornoDesc.
     * 
     * @return principalRetornoDesc
     */
    public String getPrincipalRetornoDesc() {
        return principalRetornoDesc;
    }

    /**
     * Set: principalRetornoDesc.
     * 
     * @param principalRetornoDesc
     *            the principal retorno desc
     */
    public void setPrincipalRetornoDesc(String principalRetornoDesc) {
        this.principalRetornoDesc = principalRetornoDesc;
    }

    /**
     * Get: username.
     * 
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set: username.
     * 
     * @param username
     *            the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get: dsPercRegInconsistentesDetalhar.
     * 
     * @return dsPercRegInconsistentesDetalhar
     */
    public String getDsPercRegInconsistentesDetalhar() {
        return dsPercRegInconsistentesDetalhar;
    }

    /**
     * Set: dsPercRegInconsistentesDetalhar.
     * 
     * @param dsPercRegInconsistentesDetalhar
     *            the ds perc reg inconsistentes detalhar
     */
    public void setDsPercRegInconsistentesDetalhar(
        String dsPercRegInconsistentesDetalhar) {
        this.dsPercRegInconsistentesDetalhar = dsPercRegInconsistentesDetalhar;
    }

    /**
     * Get: itemSelecionadoListaServico.
     * 
     * @return itemSelecionadoListaServico
     */
    public Integer getItemSelecionadoListaServico() {
        return itemSelecionadoListaServico;
    }

    /**
     * Set: itemSelecionadoListaServico.
     * 
     * @param itemSelecionadoListaServico
     *            the item selecionado lista servico
     */
    public void setItemSelecionadoListaServico(
        Integer itemSelecionadoListaServico) {
        this.itemSelecionadoListaServico = itemSelecionadoListaServico;
    }

    /**
     * Get: dataFimHistorico.
     * 
     * @return dataFimHistorico
     */
    public Date getDataFimHistorico() {
        return dataFimHistorico;
    }

    /**
     * Set: dataFimHistorico.
     * 
     * @param dataFimHistorico
     *            the data fim historico
     */
    public void setDataFimHistorico(Date dataFimHistorico) {
        this.dataFimHistorico = dataFimHistorico;
    }

    /**
     * Get: dataInicioHistorico.
     * 
     * @return dataInicioHistorico
     */
    public Date getDataInicioHistorico() {
        return dataInicioHistorico;
    }

    /**
     * Set: dataInicioHistorico.
     * 
     * @param dataInicioHistorico
     *            the data inicio historico
     */
    public void setDataInicioHistorico(Date dataInicioHistorico) {
        this.dataInicioHistorico = dataInicioHistorico;
    }

    /**
     * Get: listaTipoLayoutArquivoHistorico.
     * 
     * @return listaTipoLayoutArquivoHistorico
     */
    public List<SelectItem> getListaTipoLayoutArquivoHistorico() {
        return listaTipoLayoutArquivoHistorico;
    }

    /**
     * Set: listaTipoLayoutArquivoHistorico.
     * 
     * @param listaTipoLayoutArquivoHistorico
     *            the lista tipo layout arquivo historico
     */
    public void setListaTipoLayoutArquivoHistorico(
        List<SelectItem> listaTipoLayoutArquivoHistorico) {
        this.listaTipoLayoutArquivoHistorico = listaTipoLayoutArquivoHistorico;
    }

    /**
     * Get: listaTipoLayoutArquivoHistoricoHash.
     * 
     * @return listaTipoLayoutArquivoHistoricoHash
     */
    Map<Integer, String> getListaTipoLayoutArquivoHistoricoHash() {
        return listaTipoLayoutArquivoHistoricoHash;
    }

    /**
     * Set lista tipo layout arquivo historico hash.
     * 
     * @param listaTipoLayoutArquivoHistoricoHash
     *            the lista tipo layout arquivo historico hash
     */
    void setListaTipoLayoutArquivoHistoricoHash(
        Map<Integer, String> listaTipoLayoutArquivoHistoricoHash) {
        this.listaTipoLayoutArquivoHistoricoHash = listaTipoLayoutArquivoHistoricoHash;
    }

    /**
     * Get: tipoLayoutHistorico.
     * 
     * @return tipoLayoutHistorico
     */
    public Integer getTipoLayoutHistorico() {
        return tipoLayoutHistorico;
    }

    /**
     * Set: tipoLayoutHistorico.
     * 
     * @param tipoLayoutHistorico
     *            the tipo layout historico
     */
    public void setTipoLayoutHistorico(Integer tipoLayoutHistorico) {
        this.tipoLayoutHistorico = tipoLayoutHistorico;
    }

    /**
     * Get: dsDataInclusao.
     * 
     * @return dsDataInclusao
     */
    public String getDsDataInclusao() {
        return dsDataInclusao;
    }

    /**
     * Set: dsDataInclusao.
     * 
     * @param dsDataInclusao
     *            the ds data inclusao
     */
    public void setDsDataInclusao(String dsDataInclusao) {
        this.dsDataInclusao = dsDataInclusao;
    }

    /**
     * Is habilita historico.
     * 
     * @return true, if is habilita historico
     */
    public boolean isHabilitaHistorico() {
        return habilitaHistorico;
    }

    /**
     * Set: habilitaHistorico.
     * 
     * @param habilitaHistorico
     *            the habilita historico
     */
    public void setHabilitaHistorico(boolean habilitaHistorico) {
        this.habilitaHistorico = habilitaHistorico;
    }

    /**
     * Get: layoutContratadoHistorico.
     * 
     * @return layoutContratadoHistorico
     */
    public Integer getLayoutContratadoHistorico() {
        return layoutContratadoHistorico;
    }

    /**
     * Set: layoutContratadoHistorico.
     * 
     * @param layoutContratadoHistorico
     *            the layout contratado historico
     */
    public void setLayoutContratadoHistorico(Integer layoutContratadoHistorico) {
        this.layoutContratadoHistorico = layoutContratadoHistorico;
    }

    /**
     * Get: listaControleCheckServicos.
     * 
     * @return listaControleCheckServicos
     */
    public List<SelectItem> getListaControleCheckServicos() {
        return listaControleCheckServicos;
    }

    /**
     * Set: listaControleCheckServicos.
     * 
     * @param listaControleCheckServicos
     *            the lista controle check servicos
     */
    public void setListaControleCheckServicos(
        List<SelectItem> listaControleCheckServicos) {
        this.listaControleCheckServicos = listaControleCheckServicos;
    }

    /**
     * Is habilitar servico alterar.
     * 
     * @return true, if is habilitar servico alterar
     */
    public boolean isHabilitarServicoAlterar() {
        return habilitarServicoAlterar;
    }

    /**
     * Set: habilitarServicoAlterar.
     * 
     * @param habilitarServicoAlterar
     *            the habilitar servico alterar
     */
    public void setHabilitarServicoAlterar(boolean habilitarServicoAlterar) {
        this.habilitarServicoAlterar = habilitarServicoAlterar;
    }

    /**
     * Get: listaGridPesquisaServico.
     * 
     * @return listaGridPesquisaServico
     */
    public List<ListarTipoLayoutContratoSaidaDTO> getListaGridPesquisaServico() {
        return listaGridPesquisaServico;
    }

    /**
     * Set: listaGridPesquisaServico.
     * 
     * @param listaGridPesquisaServico
     *            the lista grid pesquisa servico
     */
    public void setListaGridPesquisaServico(
        List<ListarTipoLayoutContratoSaidaDTO> listaGridPesquisaServico) {
        this.listaGridPesquisaServico = listaGridPesquisaServico;
    }

    /**
     * Get: listaGridPesquisaServicoSelecionados.
     * 
     * @return listaGridPesquisaServicoSelecionados
     */
    public List<ListarTipoLayoutContratoSaidaDTO> getListaGridPesquisaServicoSelecionados() {
        return listaGridPesquisaServicoSelecionados;
    }

    /**
     * Set: listaGridPesquisaServicoSelecionados.
     * 
     * @param listaGridPesquisaServicoSelecionados
     *            the lista grid pesquisa servico selecionados
     */
    public void setListaGridPesquisaServicoSelecionados(
        List<ListarTipoLayoutContratoSaidaDTO> listaGridPesquisaServicoSelecionados) {
        this.listaGridPesquisaServicoSelecionados = listaGridPesquisaServicoSelecionados;
    }

    /**
     * Get: layoutContratadoSelecionado.
     * 
     * @return layoutContratadoSelecionado
     */
    public Integer getLayoutContratadoSelecionado() {
        return layoutContratadoSelecionado;
    }

    /**
     * Set: layoutContratadoSelecionado.
     * 
     * @param layoutContratadoSelecionado
     *            the layout contratado selecionado
     */
    public void setLayoutContratadoSelecionado(
        Integer layoutContratadoSelecionado) {
        this.layoutContratadoSelecionado = layoutContratadoSelecionado;
    }

    /**
     * Get: layoutContratadoServico.
     * 
     * @return layoutContratadoServico
     */
    public String getLayoutContratadoServico() {
        return layoutContratadoServico;
    }

    /**
     * Set: layoutContratadoServico.
     * 
     * @param layoutContratadoServico
     *            the layout contratado servico
     */
    public void setLayoutContratadoServico(String layoutContratadoServico) {
        this.layoutContratadoServico = layoutContratadoServico;
    }

    /**
     * Get: obrigatoriedade.
     * 
     * @return obrigatoriedade
     */
    public String getObrigatoriedade() {
        return obrigatoriedade;
    }

    /**
     * Set: obrigatoriedade.
     * 
     * @param obrigatoriedade
     *            the obrigatoriedade
     */
    public void setObrigatoriedade(String obrigatoriedade) {
        this.obrigatoriedade = obrigatoriedade;
    }

    /**
     * Get: tamanhoLista.
     * 
     * @return tamanhoLista
     */
    public int getTamanhoLista() {
        return tamanhoLista;
    }

    /**
     * Set: tamanhoLista.
     * 
     * @param tamanhoLista
     *            the tamanho lista
     */
    public void setTamanhoLista(int tamanhoLista) {
        this.tamanhoLista = tamanhoLista;
    }

    /**
     * Get: listaCentroCustoSubstituir.
     * 
     * @return listaCentroCustoSubstituir
     */
    public List<SelectItem> getListaCentroCustoSubstituir() {
        return listaCentroCustoSubstituir;
    }

    /**
     * Set: listaCentroCustoSubstituir.
     * 
     * @param listaCentroCustoSubstituir
     *            the lista centro custo substituir
     */
    public void setListaCentroCustoSubstituir(
        List<SelectItem> listaCentroCustoSubstituir) {
        this.listaCentroCustoSubstituir = listaCentroCustoSubstituir;
    }

    /**
     * Get: substituirCentroCusto.
     * 
     * @return substituirCentroCusto
     */
    public String getSubstituirCentroCusto() {
        return substituirCentroCusto;
    }

    /**
     * Set: substituirCentroCusto.
     * 
     * @param substituirCentroCusto
     *            the substituir centro custo
     */
    public void setSubstituirCentroCusto(String substituirCentroCusto) {
        this.substituirCentroCusto = substituirCentroCusto;
    }

    /**
     * Get: substituirCentroCustoSelecionado.
     * 
     * @return substituirCentroCustoSelecionado
     */
    public String getSubstituirCentroCustoSelecionado() {
        return substituirCentroCustoSelecionado;
    }

    /**
     * Set: substituirCentroCustoSelecionado.
     * 
     * @param substituirCentroCustoSelecionado
     *            the substituir centro custo selecionado
     */
    public void setSubstituirCentroCustoSelecionado(
        String substituirCentroCustoSelecionado) {
        this.substituirCentroCustoSelecionado = substituirCentroCustoSelecionado;
    }

    /**
     * Get: cdSerieAplicacaoTransmissaoSubstituir.
     * 
     * @return cdSerieAplicacaoTransmissaoSubstituir
     */
    public String getCdSerieAplicacaoTransmissaoSubstituir() {
        return cdSerieAplicacaoTransmissaoSubstituir;
    }

    /**
     * Set: cdSerieAplicacaoTransmissaoSubstituir.
     * 
     * @param cdSerieAplicacaoTransmissaoSubstituir
     *            the cd serie aplicacao transmissao substituir
     */
    public void setCdSerieAplicacaoTransmissaoSubstituir(
        String cdSerieAplicacaoTransmissaoSubstituir) {
        this.cdSerieAplicacaoTransmissaoSubstituir = cdSerieAplicacaoTransmissaoSubstituir;
    }

    /**
     * Get: substituirLayoutAntigo.
     * 
     * @return substituirLayoutAntigo
     */
    public Integer getSubstituirLayoutAntigo() {
        return substituirLayoutAntigo;
    }

    /**
     * Set: substituirLayoutAntigo.
     * 
     * @param substituirLayoutAntigo
     *            the substituir layout antigo
     */
    public void setSubstituirLayoutAntigo(Integer substituirLayoutAntigo) {
        this.substituirLayoutAntigo = substituirLayoutAntigo;
    }

    /**
     * Is habilitar consultar.
     * 
     * @return true, if is habilitar consultar
     */
    public boolean isHabilitarConsultar() {
        return habilitarConsultar;
    }

    /**
     * Set: habilitarConsultar.
     * 
     * @param habilitarConsultar
     *            the habilitar consultar
     */
    public void setHabilitarConsultar(boolean habilitarConsultar) {
        this.habilitarConsultar = habilitarConsultar;
    }

    /**
     * Get: cdIntervaloGeracao.
     * 
     * @return cdIntervaloGeracao
     */
    public Integer getCdIntervaloGeracao() {
        return cdIntervaloGeracao;
    }

    /**
     * Set: cdIntervaloGeracao.
     * 
     * @param cdIntervaloGeracao
     *            the cd intervalo geracao
     */
    public void setCdIntervaloGeracao(Integer cdIntervaloGeracao) {
        this.cdIntervaloGeracao = cdIntervaloGeracao;
    }

    /**
     * Get: cdOrdenacaoRegistros.
     * 
     * @return cdOrdenacaoRegistros
     */
    public Integer getCdOrdenacaoRegistros() {
        return cdOrdenacaoRegistros;
    }

    /**
     * Set: cdOrdenacaoRegistros.
     * 
     * @param cdOrdenacaoRegistros
     *            the cd ordenacao registros
     */
    public void setCdOrdenacaoRegistros(Integer cdOrdenacaoRegistros) {
        this.cdOrdenacaoRegistros = cdOrdenacaoRegistros;
    }

    /**
     * Get: cdTipoClassificacaoRetorno.
     * 
     * @return cdTipoClassificacaoRetorno
     */
    public Integer getCdTipoClassificacaoRetorno() {
        return cdTipoClassificacaoRetorno;
    }

    /**
     * Set: cdTipoClassificacaoRetorno.
     * 
     * @param cdTipoClassificacaoRetrono
     *            the cd tipo classificacao retorno
     */
    public void setCdTipoClassificacaoRetorno(Integer cdTipoClassificacaoRetrono) {
        this.cdTipoClassificacaoRetorno = cdTipoClassificacaoRetrono;
    }

    /**
     * Get: cdTipoMontagem.
     * 
     * @return cdTipoMontagem
     */
    public Integer getCdTipoMontagem() {
        return cdTipoMontagem;
    }

    /**
     * Set: cdTipoMontagem.
     * 
     * @param cdTipoMontagem
     *            the cd tipo montagem
     */
    public void setCdTipoMontagem(Integer cdTipoMontagem) {
        this.cdTipoMontagem = cdTipoMontagem;
    }

    /**
     * Get: cdTipoOcorrenciasArquivo.
     * 
     * @return cdTipoOcorrenciasArquivo
     */
    public Integer getCdTipoOcorrenciasArquivo() {
        return cdTipoOcorrenciasArquivo;
    }

    /**
     * Set: cdTipoOcorrenciasArquivo.
     * 
     * @param cdTipoOcorrenciasArquivo
     *            the cd tipo ocorrencias arquivo
     */
    public void setCdTipoOcorrenciasArquivo(Integer cdTipoOcorrenciasArquivo) {
        this.cdTipoOcorrenciasArquivo = cdTipoOcorrenciasArquivo;
    }

    /**
     * Get: dsNomeArquivoRemessa.
     * 
     * @return dsNomeArquivoRemessa
     */
    public String getDsNomeArquivoRemessa() {
        return dsNomeArquivoRemessa;
    }

    /**
     * Set: dsNomeArquivoRemessa.
     * 
     * @param dsNomeArquivoRemessa
     *            the ds nome arquivo remessa
     */
    public void setDsNomeArquivoRemessa(String dsNomeArquivoRemessa) {
        this.dsNomeArquivoRemessa = dsNomeArquivoRemessa;
    }

    /**
     * Get: dsNomeArquivoRetorno.
     * 
     * @return dsNomeArquivoRetorno
     */
    public String getDsNomeArquivoRetorno() {
        return dsNomeArquivoRetorno;
    }

    /**
     * Set: dsNomeArquivoRetorno.
     * 
     * @param dsNomeArquivoRetorno
     *            the ds nome arquivo retorno
     */
    public void setDsNomeArquivoRetorno(String dsNomeArquivoRetorno) {
        this.dsNomeArquivoRetorno = dsNomeArquivoRetorno;
    }

    /**
     * Get: subPercRegInconsistentes.
     * 
     * @return subPercRegInconsistentes
     */
    public BigDecimal getSubPercRegInconsistentes() {
        return subPercRegInconsistentes;
    }

    /**
     * Set: subPercRegInconsistentes.
     * 
     * @param subPercRegInconsistentes
     *            the sub perc reg inconsistentes
     */
    public void setSubPercRegInconsistentes(BigDecimal subPercRegInconsistentes) {
        this.subPercRegInconsistentes = subPercRegInconsistentes;
    }

    /**
     * Get: cdTipoLayoutArquivoAtual.
     * 
     * @return cdTipoLayoutArquivoAtual
     */
    public Integer getCdTipoLayoutArquivoAtual() {
        return cdTipoLayoutArquivoAtual;
    }

    /**
     * Set: cdTipoLayoutArquivoAtual.
     * 
     * @param cdTipoLayoutArquivoAtual
     *            the cd tipo layout arquivo atual
     */
    public void setCdTipoLayoutArquivoAtual(Integer cdTipoLayoutArquivoAtual) {
        this.cdTipoLayoutArquivoAtual = cdTipoLayoutArquivoAtual;
    }

    /**
     * Get: layout.
     * 
     * @return layout
     */
    public Integer getLayout() {
        return layout;
    }

    /**
     * Set: layout.
     * 
     * @param layout
     *            the layout
     */
    public void setLayout(Integer layout) {
        this.layout = layout;
    }

    /**
     * Get: cdSituacao.
     * 
     * @return cdSituacao
     */
    public String getCdSituacao() {
        return cdSituacao;
    }

    /**
     * Set: cdSituacao.
     * 
     * @param cdSituacao
     *            the cd situacao
     */
    public void setCdSituacao(String cdSituacao) {
        this.cdSituacao = cdSituacao;
    }

    /**
     * Get: percentual.
     * 
     * @return percentual
     */
    public BigDecimal getPercentual() {
        return percentual;
    }

    /**
     * Set: percentual.
     * 
     * @param percentual
     *            the percentual
     */
    public void setPercentual(BigDecimal percentual) {
        this.percentual = percentual;
    }

    /**
     * Get: responsavel.
     * 
     * @return responsavel
     */
    public Integer getResponsavel() {
        return responsavel;
    }

    /**
     * Set: responsavel.
     * 
     * @param responsavel
     *            the responsavel
     */
    public void setResponsavel(Integer responsavel) {
        this.responsavel = responsavel;
    }

    /**
     * Get: descLayout.
     * 
     * @return descLayout
     */
    public String getDescLayout() {
        return descLayout;
    }

    /**
     * Set: descLayout.
     * 
     * @param descLayout
     *            the desc layout
     */
    public void setDescLayout(String descLayout) {
        this.descLayout = descLayout;
    }

    /**
     * Get: dsSituacao.
     * 
     * @return dsSituacao
     */
    public String getDsSituacao() {
        return dsSituacao;
    }

    /**
     * Set: dsSituacao.
     * 
     * @param dsSituacao
     *            the ds situacao
     */
    public void setDsSituacao(String dsSituacao) {
        this.dsSituacao = dsSituacao;
    }

    /**
     * Get: dsResponsavel.
     * 
     * @return dsResponsavel
     */
    public String getDsResponsavel() {
        return dsResponsavel;
    }

    /**
     * Set: dsResponsavel.
     * 
     * @param dsResponsavel
     *            the ds responsavel
     */
    public void setDsResponsavel(String dsResponsavel) {
        this.dsResponsavel = dsResponsavel;
    }

    /**
     * Get: percentualCustoTransmissao.
     * 
     * @return percentualCustoTransmissao
     */
    public BigDecimal getPercentualCustoTransmissao() {
        return percentualCustoTransmissao;
    }

    /**
     * Set: percentualCustoTransmissao.
     * 
     * @param percentualCustoTransmissao
     *            the percentual custo transmissao
     */
    public void setPercentualCustoTransmissao(
        BigDecimal percentualCustoTransmissao) {
        this.percentualCustoTransmissao = percentualCustoTransmissao;
    }

    /**
     * Get: dsArquivo.
     * 
     * @return dsArquivo
     */
    public String getDsArquivo() {
        return dsArquivo;
    }

    /**
     * Set: dsArquivo.
     * 
     * @param dsArquivo
     *            the ds arquivo
     */
    public void setDsArquivo(String dsArquivo) {
        this.dsArquivo = dsArquivo;
    }

    /**
     * Get: itemSelecionadoListaHistoricoRetorno.
     * 
     * @return itemSelecionadoListaHistoricoRetorno
     */
    public Integer getItemSelecionadoListaHistoricoRetorno() {
        return itemSelecionadoListaHistoricoRetorno;
    }

    /**
     * Set: itemSelecionadoListaHistoricoRetorno.
     * 
     * @param itemSelecionadoListaHistoricoRetorno
     *            the item selecionado lista historico retorno
     */
    public void setItemSelecionadoListaHistoricoRetorno(
        Integer itemSelecionadoListaHistoricoRetorno) {
        this.itemSelecionadoListaHistoricoRetorno = itemSelecionadoListaHistoricoRetorno;
    }

    /**
     * Get: listaControleHistoricoRetorno.
     * 
     * @return listaControleHistoricoRetorno
     */
    public List<SelectItem> getListaControleHistoricoRetorno() {
        return listaControleHistoricoRetorno;
    }

    /**
     * Set: listaControleHistoricoRetorno.
     * 
     * @param listaControleHistoricoRetorno
     *            the lista controle historico retorno
     */
    public void setListaControleHistoricoRetorno(
        List<SelectItem> listaControleHistoricoRetorno) {
        this.listaControleHistoricoRetorno = listaControleHistoricoRetorno;
    }

    /**
     * Get: listaHistoricoRetorno.
     * 
     * @return listaHistoricoRetorno
     */
    public List<ListarHistoricoLayoutContratoSaidaDTO> getListaHistoricoRetorno() {
        return listaHistoricoRetorno;
    }

    /**
     * Set: listaHistoricoRetorno.
     * 
     * @param listaHistoricoRetorno
     *            the lista historico retorno
     */
    public void setListaHistoricoRetorno(
        List<ListarHistoricoLayoutContratoSaidaDTO> listaHistoricoRetorno) {
        this.listaHistoricoRetorno = listaHistoricoRetorno;
    }

    /**
     * Get: dsIntervaloGeracao.
     * 
     * @return dsIntervaloGeracao
     */
    public String getDsIntervaloGeracao() {
        return dsIntervaloGeracao;
    }

    /**
     * Set: dsIntervaloGeracao.
     * 
     * @param dsIntervaloGeracao
     *            the ds intervalo geracao
     */
    public void setDsIntervaloGeracao(String dsIntervaloGeracao) {
        this.dsIntervaloGeracao = dsIntervaloGeracao;
    }

    /**
     * Get: dsOrdenacaoRegistros.
     * 
     * @return dsOrdenacaoRegistros
     */
    public String getDsOrdenacaoRegistros() {
        return dsOrdenacaoRegistros;
    }

    /**
     * Set: dsOrdenacaoRegistros.
     * 
     * @param dsOrdenacaoRegistros
     *            the ds ordenacao registros
     */
    public void setDsOrdenacaoRegistros(String dsOrdenacaoRegistros) {
        this.dsOrdenacaoRegistros = dsOrdenacaoRegistros;
    }

    /**
     * Get: dsTipoClassificacaoRetorno.
     * 
     * @return dsTipoClassificacaoRetorno
     */
    public String getDsTipoClassificacaoRetorno() {
        return dsTipoClassificacaoRetorno;
    }

    /**
     * Set: dsTipoClassificacaoRetorno.
     * 
     * @param dsTipoClassificacaoRetorno
     *            the ds tipo classificacao retorno
     */
    public void setDsTipoClassificacaoRetorno(String dsTipoClassificacaoRetorno) {
        this.dsTipoClassificacaoRetorno = dsTipoClassificacaoRetorno;
    }

    /**
     * Get: dsTipoMontagem.
     * 
     * @return dsTipoMontagem
     */
    public String getDsTipoMontagem() {
        return dsTipoMontagem;
    }

    /**
     * Set: dsTipoMontagem.
     * 
     * @param dsTipoMontagem
     *            the ds tipo montagem
     */
    public void setDsTipoMontagem(String dsTipoMontagem) {
        this.dsTipoMontagem = dsTipoMontagem;
    }

    /**
     * Get: dsTipoOcorrenciasArquivo.
     * 
     * @return dsTipoOcorrenciasArquivo
     */
    public String getDsTipoOcorrenciasArquivo() {
        return dsTipoOcorrenciasArquivo;
    }

    /**
     * Set: dsTipoOcorrenciasArquivo.
     * 
     * @param dsTipoOcorrenciasArquivo
     *            the ds tipo ocorrencias arquivo
     */
    public void setDsTipoOcorrenciasArquivo(String dsTipoOcorrenciasArquivo) {
        this.dsTipoOcorrenciasArquivo = dsTipoOcorrenciasArquivo;
    }

    /**
     * Get: dataFimManutencao.
     * 
     * @return dataFimManutencao
     */
    public Date getDataFimManutencao() {
        return dataFimManutencao;
    }

    /**
     * Set: dataFimManutencao.
     * 
     * @param dataFimManutencao
     *            the data fim manutencao
     */
    public void setDataFimManutencao(Date dataFimManutencao) {
        this.dataFimManutencao = dataFimManutencao;
    }

    /**
     * Get: dataInicioManutencao.
     * 
     * @return dataInicioManutencao
     */
    public Date getDataInicioManutencao() {
        return dataInicioManutencao;
    }

    /**
     * Set: dataInicioManutencao.
     * 
     * @param dataInicioManutencao
     *            the data inicio manutencao
     */
    public void setDataInicioManutencao(Date dataInicioManutencao) {
        this.dataInicioManutencao = dataInicioManutencao;
    }

    /**
     * Is habilita historico retorno.
     * 
     * @return true, if is habilita historico retorno
     */
    public boolean isHabilitaHistoricoRetorno() {
        return habilitaHistoricoRetorno;
    }

    /**
     * Set: habilitaHistoricoRetorno.
     * 
     * @param habilitaHistoricoRetorno
     *            the habilita historico retorno
     */
    public void setHabilitaHistoricoRetorno(boolean habilitaHistoricoRetorno) {
        this.habilitaHistoricoRetorno = habilitaHistoricoRetorno;
    }

    /**
     * Get: cdArquivo.
     * 
     * @return cdArquivo
     */
    public Integer getCdArquivo() {
        return cdArquivo;
    }

    /**
     * Set: cdArquivo.
     * 
     * @param cdArquivo
     *            the cd arquivo
     */
    public void setCdArquivo(Integer cdArquivo) {
        this.cdArquivo = cdArquivo;
    }

    /**
     * Is habilitar percentual.
     * 
     * @return true, if is habilitar percentual
     */
    public boolean isHabilitarPercentual() {
        return habilitarPercentual;
    }

    /**
     * Set: habilitarPercentual.
     * 
     * @param habilitarPercentual
     *            the habilitar percentual
     */
    public void setHabilitarPercentual(boolean habilitarPercentual) {
        this.habilitarPercentual = habilitarPercentual;
    }

    /**
     * Get: dsTipoManutencao.
     * 
     * @return dsTipoManutencao
     */
    public String getDsTipoManutencao() {
        return dsTipoManutencao;
    }

    /**
     * Set: dsTipoManutencao.
     * 
     * @param dsTipoManutencao
     *            the ds tipo manutencao
     */
    public void setDsTipoManutencao(String dsTipoManutencao) {
        this.dsTipoManutencao = dsTipoManutencao;
    }

    /**
     * Get: codLancamento.
     * 
     * @return codLancamento
     */
    public Integer getCodLancamento() {
        return codLancamento;
    }

    /**
     * Set: codLancamento.
     * 
     * @param codLancamento
     *            the cod lancamento
     */
    public void setCodLancamento(Integer codLancamento) {
        this.codLancamento = codLancamento;
    }

    /**
     * Is habilitar lancamento.
     * 
     * @return true, if is habilitar lancamento
     */
    public boolean isHabilitarLancamento() {
        return habilitarLancamento;
    }

    /**
     * Set: habilitarLancamento.
     * 
     * @param habilitarLancamento
     *            the habilitar lancamento
     */
    public void setHabilitarLancamento(boolean habilitarLancamento) {
        this.habilitarLancamento = habilitarLancamento;
    }

    /**
     * Is item selecionado lista layout altera.
     * 
     * @return true, if is item selecionado lista layout altera
     */
    public boolean isItemSelecionadoListaLayoutAltera() {
        return itemSelecionadoListaLayoutAltera;
    }

    /**
     * Set: itemSelecionadoListaLayoutAltera.
     * 
     * @param itemSelecionadoListaLayoutAltera
     *            the item selecionado lista layout altera
     */
    public void setItemSelecionadoListaLayoutAltera(
        boolean itemSelecionadoListaLayoutAltera) {
        this.itemSelecionadoListaLayoutAltera = itemSelecionadoListaLayoutAltera;
    }

    /**
     * Is tela detalhe.
     * 
     * @return true, if is tela detalhe
     */
    public boolean isTelaDetalhe() {
        return telaDetalhe;
    }

    /**
     * Set: telaDetalhe.
     * 
     * @param telaDetalhe
     *            the tela detalhe
     */
    public void setTelaDetalhe(boolean telaDetalhe) {
        this.telaDetalhe = telaDetalhe;
    }

    /**
     * Is tela excluir.
     * 
     * @return true, if is tela excluir
     */
    public boolean isTelaExcluir() {
        return telaExcluir;
    }

    /**
     * Set: telaExcluir.
     * 
     * @param telaExcluir
     *            the tela excluir
     */
    public void setTelaExcluir(boolean telaExcluir) {
        this.telaExcluir = telaExcluir;
    }

    /**
     * Is tela incluir.
     * 
     * @return true, if is tela incluir
     */
    public boolean isTelaIncluir() {
        return telaIncluir;
    }

    /**
     * Set: telaIncluir.
     * 
     * @param telaIncluir
     *            the tela incluir
     */
    public void setTelaIncluir(boolean telaIncluir) {
        this.telaIncluir = telaIncluir;
    }

    /**
     * Nome: getSaidaTipoLayoutArquivoSaidaDTO
     * 
     * @exception
     * @throws
     * @return saidaTipoLayoutArquivoSaidaDTO
     */
    public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
        return saidaTipoLayoutArquivoSaidaDTO;
    }

    /**
     * Nome: setSaidaTipoLayoutArquivoSaidaDTO
     * 
     * @exception
     * @throws
     * @param saidaTipoLayoutArquivoSaidaDTO
     */
    public void setSaidaTipoLayoutArquivoSaidaDTO(
        TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
        this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
    }

    /**
     * Nome: setNumeroVersaoLayoutArquivo
     * 
     * @exception
     * @throws
     * @param numeroVersaoLayoutArquivo
     */
    public void setNumeroVersaoLayoutArquivo(Integer numeroVersaoLayoutArquivo) {
        this.numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
    }

    /**
     * Nome: getNumeroVersaoLayoutArquivo
     * 
     * @exception
     * @throws
     * @return numeroVersaoLayoutArquivo
     */
    public Integer getNumeroVersaoLayoutArquivo() {
        return numeroVersaoLayoutArquivo;
    }

    /**
     * Nome: setListaNumVersaoLayoutArq
     * 
     * @exception
     * @throws
     * @param listaNumVersaoLayoutArq
     */
    public void setListaNumVersaoLayoutArq(
        List<SelectItem> listaNumVersaoLayoutArq) {
        this.listaNumVersaoLayoutArq = listaNumVersaoLayoutArq;
    }

    /**
     * Nome: getListaNumVersaoLayoutArq
     * 
     * @exception
     * @throws
     * @return listaNumVersaoLayoutArq
     */
    public List<SelectItem> getListaNumVersaoLayoutArq() {
        return listaNumVersaoLayoutArq;
    }

    /**
     * Nome: setListaNumVersaoLayoutArqHash
     * 
     * @exception
     * @throws
     * @param listaNumVersaoLayoutArqHash
     */
    public void setListaNumVersaoLayoutArqHash(
        Map<Integer, Integer> listaNumVersaoLayoutArqHash) {
        this.listaNumVersaoLayoutArqHash = listaNumVersaoLayoutArqHash;
    }

    /**
     * Nome: getListaNumVersaoLayoutArqHash
     * 
     * @exception
     * @throws
     * @return listaNumVersaoLayoutArqHash
     */
    public Map<Integer, Integer> getListaNumVersaoLayoutArqHash() {
        return listaNumVersaoLayoutArqHash;
    }

    /**
     * Nome: setSaidaListarNumeroVersaoLayout
     * 
     * @exception
     * @throws
     * @param saidaListarNumeroVersaoLayout
     */
    public void setSaidaListarNumeroVersaoLayout(
        ListarNumeroVersaoLayoutSaidaDTO saidaListarNumeroVersaoLayout) {
        this.saidaListarNumeroVersaoLayout = saidaListarNumeroVersaoLayout;
    }

    /**
     * Nome: getSaidaListarNumeroVersaoLayout
     * 
     * @exception
     * @throws
     * @return saidaListarNumeroVersaoLayout
     */
    public ListarNumeroVersaoLayoutSaidaDTO getSaidaListarNumeroVersaoLayout() {
        return saidaListarNumeroVersaoLayout;
    }

    /**
     * Nome: setNumVersaoLayoutAlt
     * 
     * @exception
     * @throws
     * @param numVersaoLayoutAlt
     */
    public void setNumVersaoLayoutAlt(Integer numVersaoLayoutAlt) {
        this.numVersaoLayoutAlt = numVersaoLayoutAlt;
    }

    /**
     * Nome: getNumVersaoLayoutAlt
     * 
     * @exception
     * @throws
     * @return numVersaoLayoutAlt
     */
    public Integer getNumVersaoLayoutAlt() {
        return numVersaoLayoutAlt;
    }

    /**
     * Nome: setNumVersaoLayoutAltConf
     * 
     * @exception
     * @throws
     * @param numVersaoLayoutAltConf
     */
    public void setNumVersaoLayoutAltConf(Integer numVersaoLayoutAltConf) {
        this.numVersaoLayoutAltConf = numVersaoLayoutAltConf;
    }

    /**
     * Nome: getNumVersaoLayoutAltConf
     * 
     * @exception
     * @throws
     * @return numVersaoLayoutAltConf
     */
    public Integer getNumVersaoLayoutAltConf() {
        return numVersaoLayoutAltConf;
    }

    /**
     * Nome: setNumVersaoLayoutExc
     * 
     * @exception
     * @throws
     * @param numVersaoLayoutExc
     */
    public void setNumVersaoLayoutExc(Integer numVersaoLayoutExc) {
        this.numVersaoLayoutExc = numVersaoLayoutExc;
    }

    /**
     * Nome: getNumVersaoLayoutExc
     * 
     * @exception
     * @throws
     * @return numVersaoLayoutExc
     */
    public Integer getNumVersaoLayoutExc() {
        return numVersaoLayoutExc;
    }

    /**
     * @return the comboTipoMontagem
     */
    public List<SelectItem> getComboTipoMontagem() {
        return comboTipoMontagem;
    }

    /**
     * @param comboTipoMontagem
     *            the comboTipoMontagem to set
     */
    public void setComboTipoMontagem(List<SelectItem> comboTipoMontagem) {
        this.comboTipoMontagem = comboTipoMontagem;
    }

    /**
     * Get: saidaValidarUsuario.
     * 
     * @return saidaValidarUsuario
     */
    public ValidarUsuarioSaidaDTO getSaidaValidarUsuario() {
        return saidaValidarUsuario;
    }

    /**
     * Set: saidaValidarUsuario.
     * 
     * @param saidaValidarUsuario
     *            the saida validar usuario
     */
    public void setSaidaValidarUsuario(ValidarUsuarioSaidaDTO saidaValidarUsuario) {
        this.saidaValidarUsuario = saidaValidarUsuario;
    }

    /**
     * Get: entradaValidarUsuario.
     * 
     * @return entradaValidarUsuario
     */
    public ValidarUsuarioEntradaDTO getEntradaValidarUsuario() {
        return entradaValidarUsuario;
    }

    /**
     * Set: entradaValidarUsuario.
     * 
     * @param entradaValidarUsuario
     *            the entrada validar usuario
     */
    public void setEntradaValidarUsuario(
        ValidarUsuarioEntradaDTO entradaValidarUsuario) {
        this.entradaValidarUsuario = entradaValidarUsuario;
    }

    /**
     * Is restringir acesso botoes.
     * 
     * @return true, if is restringir acesso botoes
     */
    public boolean isRestringirAcessoBotoes() {
        return restringirAcessoBotoes;
    }

    /**
     * Set: restringirAcessoBotoes.
     * 
     * @param restringirAcessoBotoes
     *            the restringir acesso botoes
     */
    public void setRestringirAcessoBotoes(boolean restringirAcessoBotoes) {
        this.restringirAcessoBotoes = restringirAcessoBotoes;
    }

    /**
     * Get: contrNumeroPagamentoSelecionado.
     * 
     * @return contrNumeroPagamentoSelecionado
     */
    public Integer getContrNumeroPagamentoSelecionado() {
        return contrNumeroPagamentoSelecionado;
    }

    /**
     * Set: contrNumeroPagamentoSelecionado.
     * 
     * @param contrNumeroPagamentoSelecionado
     *            the contr numero pagamento selecionado
     */
    public void setContrNumeroPagamentoSelecionado(
        Integer contrNumeroPagamentoSelecionado) {
        this.contrNumeroPagamentoSelecionado = contrNumeroPagamentoSelecionado;
    }

    /**
     * Get: nomeContrNumeroPagamento.
     * 
     * @return nomeContrNumeroPagamento
     */
    public String getNomeContrNumeroPagamento() {
        return nomeContrNumeroPagamento;
    }

    /**
     * Set: nomeContrNumeroPagamento.
     * 
     * @param nomeContrNumeroPagamento
     *            the nome contr numero pagamento
     */
    public void setNomeContrNumeroPagamento(String nomeContrNumeroPagamento) {
        this.nomeContrNumeroPagamento = nomeContrNumeroPagamento;
    }

    /**
     * Get: saidaConsultarLayoutArquivo.
     * 
     * @return saidaConsultarLayoutArquivo
     */
    public ConsultarLayoutArquivoContratoSaidaDTO getSaidaConsultarLayoutArquivo() {
        return saidaConsultarLayoutArquivo;
    }

    /**
     * Set: saidaConsultarLayoutArquivo.
     * 
     * @param saidaConsultarLayoutArquivo
     *            the saida consultar layout arquivo
     */
    public void setSaidaConsultarLayoutArquivo(
        ConsultarLayoutArquivoContratoSaidaDTO saidaConsultarLayoutArquivo) {
        this.saidaConsultarLayoutArquivo = saidaConsultarLayoutArquivo;
    }

    /**
     * Get: saidaConsultarConManLayout.
     * 
     * @return saidaConsultarConManLayout
     */
    public ConsultarConManLayoutSaidaDTO getSaidaConsultarConManLayout() {
        return saidaConsultarConManLayout;
    }

    /**
     * Set: saidaConsultarConManLayout.
     * 
     * @param saidaConsultarConManLayout
     *            the saida consultar con man layout
     */
    public void setSaidaConsultarConManLayout(
        ConsultarConManLayoutSaidaDTO saidaConsultarConManLayout) {
        this.saidaConsultarConManLayout = saidaConsultarConManLayout;
    }

    /**
     * Is bloquear combos.
     * 
     * @return true, if is bloquear combos
     */
    public boolean isBloquearCombos() {
        return bloquearCombos;
    }

    /**
     * Set: bloquearCombos.
     * 
     * @param bloquearCombos
     *            the bloquear combos
     */
    public void setBloquearCombos(boolean bloquearCombos) {
        this.bloquearCombos = bloquearCombos;
    }

    /**
     * Is desabilitar combos retorno layout.
     * 
     * @return true, if is desabilitar combos retorno layout
     */
    public boolean isDesabilitarCombosRetornoLayout() {
        return desabilitarCombosRetornoLayout;
    }

    /**
     * Set: desabilitarCombosRetornoLayout.
     * 
     * @param desabilitarCombosRetornoLayout
     *            the desabilitar combos retorno layout
     */
    public void setDesabilitarCombosRetornoLayout(
        boolean desabilitarCombosRetornoLayout) {
        this.desabilitarCombosRetornoLayout = desabilitarCombosRetornoLayout;
    }

    /**
     * Get: cdAcessoDepartamento.
     * 
     * @return cdAcessoDepartamento
     */
    public Integer getCdAcessoDepartamento() {
        return cdAcessoDepartamento;
    }

    /**
     * Set: cdAcessoDepartamento.
     * 
     * @param cdAcessoDepartamento
     *            the cd acesso departamento
     */
    public void setCdAcessoDepartamento(Integer cdAcessoDepartamento) {
        this.cdAcessoDepartamento = cdAcessoDepartamento;
    }

}