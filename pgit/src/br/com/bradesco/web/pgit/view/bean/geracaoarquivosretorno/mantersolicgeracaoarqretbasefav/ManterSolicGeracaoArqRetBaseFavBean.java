/**
 * Nome: br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicgeracaoarqretbasefav
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicgeracaoarqretbasefav;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.IManterSolicGeracaoArquivoRetBaseFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.DetalharSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.DetalharSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ExcluirSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ExcluirSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.IncluirSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.IncluirSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ListarSolBaseFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarquivoretbasefav.bean.ListarSolBaseFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterSolicGeracaoArqRetBaseFavBean
 * <p>
 * Prop�sito: Implementa��o do Bean ManterSolicGeracaoArqRetBaseFavBean
 * </p>
 * 
 * @author todo! - Solu��es em Tecnologia / TI Melhorias - Arquitetura
 * @version 1.0
 */
public class ManterSolicGeracaoArqRetBaseFavBean {

	/** Atributo consultasService. */
	private IConsultasService consultasService;
	
	/** Atributo solEmissaoComprovantePagtoClientePagServiceImpl. */
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterSolicGeracaoArquivoRetBaseFavServiceImpl. */
	private IManterSolicGeracaoArquivoRetBaseFavService manterSolicGeracaoArquivoRetBaseFavServiceImpl;
	
	/** Atributo manterSolicitacaoRastFavService. */
	private IManterSolicitacaoRastFavService manterSolicitacaoRastFavService;
	
	/** Atributo atributoSoltc. */
	private VerificaraAtributoSoltcSaidaDTO atributoSoltc;

	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo cboSituacaoSolicitacao. */
	private Integer cboSituacaoSolicitacao;
	
	/** Atributo cboOrigemSolicitacao. */
	private Integer cboOrigemSolicitacao;
	
	/** Atributo dataSolicitacaoDe. */
	private Date dataSolicitacaoDe;
	
	/** Atributo dataSolicitacaoAte. */
	private Date dataSolicitacaoAte;
	
	/** Atributo numeroSolicitacao. */
	private Integer numeroSolicitacao;
	
	/** Atributo listaSituacaoSolicitacao. */
	private List<SelectItem> listaSituacaoSolicitacao;

	/** Atributo listaGridConsultar. */
	private List<ListarSolBaseFavorecidoSaidaDTO> listaGridConsultar;
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;
	
	/** Atributo listaGridControleRadio. */
	private List<SelectItem> listaGridControleRadio;

	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo tipoFavorecido. */
	private String tipoFavorecido;
	
	/** Atributo situacaoFavorecido. */
	private String situacaoFavorecido;
	
	/** Atributo destino. */
	private Integer destino;
	
	/** Atributo vlTarifaPadrao. */
	private BigDecimal vlTarifaPadrao;
	
	/** Atributo vlDescontoTarifa. */
	private BigDecimal vlDescontoTarifa;
	
	/** Atributo vlTarifaAtualizada. */
	private BigDecimal vlTarifaAtualizada;
	
	/** Atributo numeroSolic. */
	private Integer numeroSolic;
	
	/** Atributo horaSolicitacao. */
	private String horaSolicitacao;
	
	/** Atributo situacaoSolicitacao. */
	private String situacaoSolicitacao;
	
	/** Atributo horaAtendimento. */
	private String horaAtendimento;
	
	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;
	
	/** Atributo qtdeRegistros. */
	private Long qtdeRegistros;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo listaTipoFavorecido. */
	private List<SelectItem> listaTipoFavorecido;
	
	/** Atributo cboTipoFavorecido. */
	private Integer cboTipoFavorecido;
	
	/** Atributo cboSituacaoFavorecido. */
	private Integer cboSituacaoFavorecido;
	
	/** Atributo radioDestino. */
	private String radioDestino;

	/** Atributo listaTipoFavorecidoHash. */
	private Map<Integer, String> listaTipoFavorecidoHash = new HashMap<Integer, String>();

	/** Atributo indicadorDescontoBloqueio. */
	private boolean indicadorDescontoBloqueio;

	/**
	 * Inicializa tela.
	 *
	 * @param evt the evt
	 */
	public void inicializaTela(ActionEvent evt) {

		// limpar todos os campos da tela
		limparTela();

		// Carregamentos dos Combos
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();

		// Tela Filtro
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());

		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		filtroAgendamentoEfetivacaoEstornoBean
				.setEmpresaGestoraFiltro(2269651L);

		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno("conManterSolicGeracaoArqRetBaseFav");

		carregaListaSituacaoSolicitacao();
		carregaTipoFavorecido();
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Carrega lista situacao solicitacao.
	 */
	public void carregaListaSituacaoSolicitacao() {

		try {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();

			List<ListarSituacaoSolicitacaoSaidaDTO> listaSaida = comboService
					.listarSituacaoSolicitacao();

			for (int i = 0; i < listaSaida.size(); i++) {
				listaSituacaoSolicitacao.add(new SelectItem(listaSaida.get(i)
						.getCdSolicitacaoPagamentoIntegrado(), listaSaida
						.get(i).getDsTipoSolicitacaoPagamento()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Carrega tipo favorecido.
	 */
	public void carregaTipoFavorecido() {

		try {
			listaTipoFavorecido = new ArrayList<SelectItem>();

			List<TipoFavorecidoSaidaDTO> listaSaida = comboService
					.listarTipoFavorecido();
			listaTipoFavorecidoHash.clear();

			for (int i = 0; i < listaSaida.size(); i++) {
				listaTipoFavorecidoHash.put(listaSaida.get(i)
						.getCdTipoFavorecidos(), listaSaida.get(i)
						.getDsTipoFavorecidos());
				listaTipoFavorecido.add(new SelectItem(listaSaida.get(i)
						.getCdTipoFavorecidos(), listaSaida.get(i)
						.getDsTipoFavorecidos()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listaTipoFavorecido = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Limpar radios principais.
	 */
	public void limparRadiosPrincipais() {
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);

		return "";

	}

	/**
	 * Limpar radios principais incluir.
	 */
	public void limparRadiosPrincipaisIncluir() {
		vlDescontoTarifa = BigDecimal.ZERO;

		if (indicadorDescontoBloqueio) {
			vlTarifaAtualizada = vlTarifaPadrao;
		} else {
			vlTarifaAtualizada = BigDecimal.ZERO;
		}

		limparIncluir();
	}

	/**
	 * Limpar apos alterar opcao cliente incluir.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoClienteIncluir() {
		// Limpar Argumentos de Pesquisa e Lista
		limparIncluir();
		return "";
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato consultar.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContratoConsultar(
			ActionEvent evt) {
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionado(null);
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato incluir.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContratoIncluir(
			ActionEvent evt) {
		limparIncluir();
	}

	/**
	 * Limpar incluir.
	 */
	public void limparIncluir() {
		setCboTipoFavorecido(0);
		setCboSituacaoFavorecido(0);
		setRadioDestino("");
	}

	/**
	 * Limpar campos consultar.
	 */
	public void limparCamposConsultar() {
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setNumeroSolicitacao(null);
		setCboSituacaoSolicitacao(0);
		setCboOrigemSolicitacao(0);
	}

	/**
	 * Limpar tela.
	 */
	public void limparTela() {

		setRadioDestino("");
		limparCamposConsultar();
		limparRadiosPrincipais();
		setListaGridConsultar(null);
		setListaGridControleRadio(null);
		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);
		carregaListaSituacaoSolicitacao();
		carregaTipoFavorecido();
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar grid consultar.
	 *
	 * @return the string
	 */
	public String limparGridConsultar() {
		setListaGridConsultar(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		setItemSelecionado(null);
		setListaGridControleRadio(null);
		listaGridConsultar = new ArrayList<ListarSolBaseFavorecidoSaidaDTO>();

		try {
			ListarSolBaseFavorecidoEntradaDTO entradaDTO = new ListarSolBaseFavorecidoEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());
			entradaDTO
					.setDtSolicitacaoInicio((getDataSolicitacaoDe() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoDe())));
			entradaDTO
					.setDtSolicitacaoFim((getDataSolicitacaoAte() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoAte())));
			entradaDTO.setNrSolicitacaoPagamento(getNumeroSolicitacao());
			entradaDTO.setCdSituacaoSolicitacao(getCboSituacaoSolicitacao());
			entradaDTO.setCdOrigemSolicitacao(getCboOrigemSolicitacao());

			setListaGridConsultar(getManterSolicGeracaoArquivoRetBaseFavServiceImpl()
					.listarSolBaseFavorecido(entradaDTO));
			listaGridControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				listaGridControleRadio.add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridConsultar(null);
			setItemSelecionado(null);
			setDisableArgumentosConsulta(false);
		}

		return "";
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		limparAtributos();
		carregaCabecalho();

		try {

			DetalharSolBaseFavorecidoEntradaDTO entradaDTO = new DetalharSolBaseFavorecidoEntradaDTO();

			entradaDTO.setNrSolicitacaoPagamento(getListaGridConsultar().get(
					getItemSelecionado()).getNrSolicitacaoPagamento());

			DetalharSolBaseFavorecidoSaidaDTO saidaDTO = getManterSolicGeracaoArquivoRetBaseFavServiceImpl()
					.detalharSolBaseFavorecido(entradaDTO);
			setTipoFavorecido(saidaDTO.getDsTipoFavorecido());
			setSituacaoFavorecido(saidaDTO.getDsSituacaoFavorecido());
			setDestino(saidaDTO.getCdDestino());
			setNumeroSolic(saidaDTO.getNrSolicitacaoPagamento());
			setHoraSolicitacao(saidaDTO.getHrSolicitacaoFormatada());
			setSituacaoSolicitacao(saidaDTO.getDsSituacaoSolicitacao());
			setHoraAtendimento(saidaDTO.getHrAtendimentoSolicitacaoFormatada());
			setResultadoProcessamento(saidaDTO.getResultadoProcessamento());
			setQtdeRegistros(saidaDTO.getQtdeRegistroSolicitacao());
			setVlTarifaPadrao(saidaDTO.getTarifaPadrao());
			setVlDescontoTarifa(saidaDTO.getDescontoTarifa());
			setVlTarifaAtualizada(saidaDTO.getTarifaAtual());

			setDataHoraInclusao(saidaDTO.getHrInclusaoRegistroFormatada());
			setUsuarioInclusao(saidaDTO.getCdAutenticacaoSegurancaInclusao());
			setTipoCanalInclusao((saidaDTO.getCdCanalInclusao() == 0 ? ""
					: saidaDTO.getCdCanalInclusao() + " - "
							+ saidaDTO.getDsCanalInclusao()));
			setComplementoInclusao(saidaDTO.getNmOperacaoFluxo() == null
					|| saidaDTO.getNmOperacaoFluxo().equals("0") ? ""
					: saidaDTO.getNmOperacaoFluxo());
			setDataHoraManutencao(saidaDTO.getHrManutencaoRegistroFormatada());
			setUsuarioManutencao(saidaDTO
					.getCdAutenticacaoSegurancaManutencao());
			setTipoCanalManutencao((saidaDTO.getCdCanalManutencao() == 0 ? ""
					: saidaDTO.getCdCanalManutencao() + " - "
							+ saidaDTO.getCdCanalManutencao()));
			setComplementoManutencao(saidaDTO.getNmOperacaoFluxoManutencao() == null
					|| saidaDTO.getNmOperacaoFluxoManutencao().equals("0") ? ""
					: saidaDTO.getNmOperacaoFluxoManutencao());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}
		return "DETALHAR";
	}

	/**
	 * Carrega cabecalho.
	 */
	public void carregaCabecalho() {
		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()
				.equals("0")) { // Filtrar por cliente
			setNrCnpjCpf(String.valueOf(filtroAgendamentoEfetivacaoEstornoBean
					.getNrCpfCnpjCliente()));
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoRazaoSocialCliente());
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescCliente());
		} else {
			if (filtroAgendamentoEfetivacaoEstornoBean
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				try {
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO
							.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
									.getEmpresaGestoraFiltro());
					entradaDTO
							.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getCdTipoContratoNegocio());
					entradaDTO
							.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getNrSequenciaContratoNegocio());
					DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
							.detalharDadosContrato(entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException p) {
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
						.getEmpresaGestoraDescContrato());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getNumeroDescContrato());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getSituacaoDescContrato());
			}
		}

	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		limparAtributos();
		carregaCabecalho();
		detalhar();

		return "EXCLUIR";
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		setItemSelecionado(null);

		return "VOLTAR_CONSULTAR";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Limpar atributos.
	 */
	public void limparAtributos() {

		setNrCnpjCpf(null);
		setDsRazaoSocial(null);
		setDsEmpresa(null);
		setNroContrato(null);
		setDsContrato(null);
		setCdSituacaoContrato(null);

		setTipoFavorecido(null);
		setSituacaoFavorecido(null);
		setDestino(null);
		setVlTarifaPadrao(null);
		setVlDescontoTarifa(null);
		setVlTarifaAtualizada(null);
		setNumeroSolic(null);
		setHoraSolicitacao(null);
		setSituacaoSolicitacao(null);
		setHoraAtendimento(null);
		setResultadoProcessamento(null);
		setQtdeRegistros(null);

		setDataHoraInclusao(null);
		setUsuarioInclusao(null);
		setTipoCanalInclusao(null);
		setComplementoInclusao(null);

		setDataHoraManutencao(null);
		setUsuarioManutencao(null);
		setTipoCanalManutencao(null);
		setComplementoManutencao(null);
	}

	/**
	 * Confirmar incluir.
	 */
	public void confirmarIncluir() {
		IncluirSolBaseFavorecidoEntradaDTO entradaDTO = new IncluirSolBaseFavorecidoEntradaDTO();

		try {
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setCdTipoFavorecido(getCboTipoFavorecido());
			entradaDTO.setCdSituacaoFavorecido(getCboSituacaoFavorecido());
			entradaDTO.setCdDestino(Integer.parseInt(getRadioDestino()));
			entradaDTO.setVlTarifaPadrao(vlTarifaPadrao);
			entradaDTO.setNumPercentualDescTarifa(vlDescontoTarifa);
			entradaDTO.setVlrTarifaAtual(vlTarifaAtualizada);

			IncluirSolBaseFavorecidoSaidaDTO saidaDTO = getManterSolicGeracaoArquivoRetBaseFavServiceImpl()
					.incluirSolBaseFavorecido(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conManterSolicGeracaoArqRetBaseFav",
					BradescoViewExceptionActionType.ACTION, false);

			if (getListaGridConsultar() != null
					&& getListaGridConsultar().size() > 0) {
				consultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Confirmar excluir.
	 */
	public void confirmarExcluir() {
		try {

			ExcluirSolBaseFavorecidoEntradaDTO entradaDTO = new ExcluirSolBaseFavorecidoEntradaDTO();

			entradaDTO.setNrSolicitacaoGeracaoRetorno(getListaGridConsultar()
					.get(getItemSelecionado()).getNrSolicitacaoPagamento());
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());

			ExcluirSolBaseFavorecidoSaidaDTO saidaDTO = getManterSolicGeracaoArquivoRetBaseFavServiceImpl()
					.excluirSolBaseFavorecido(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conManterSolicGeracaoArqRetBaseFav",
					"#{manterSolicGeracaoArqRetBaseFavBean.consultar}", false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Nome: incluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String incluir() {
		vlTarifaPadrao = BigDecimal.ZERO;
		atributoSoltc = getManterSolicitacaoRastFavService()
				.verificaraAtributoSoltc(
						new VerificaraAtributoSoltcEntradaDTO(3));

		try {
			carregaCabecalho();
			carregaListaSituacaoSolicitacao();
			carregaTipoFavorecido();
			consultarAgenciaOpeTarifaPadrao();
			limparRadiosPrincipaisIncluir();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "INCLUIR";
	}

	/**
	 * Nome: avancarIncluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String avancarIncluir() {
		setTipoFavorecido((String) listaTipoFavorecidoHash
				.get(getCboTipoFavorecido()));

		calcularTarifaAtualizada();

		return "AVANCAR_INCLUIR";
	}

	/**
	 * Nome: calcularTarifaAtualizada
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void calcularTarifaAtualizada() {
		vlTarifaAtualizada = vlTarifaPadrao.subtract(vlTarifaPadrao.multiply(
				vlDescontoTarifa.divide(new BigDecimal(100))).setScale(2,
				BigDecimal.ROUND_HALF_UP));
	}

	/**
	 * Nome: consultarAgenciaOpeTarifaPadrao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void consultarAgenciaOpeTarifaPadrao() {
		try {
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaEmpresa(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setCdProdutoServicoOperacao(0);
			entradaDTO.setCdProdutoServicoRelacionado(0);
			entradaDTO.setCdTipoTarifa(9);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl()
					.consultarAgenciaOpeTarifaPadrao(entradaDTO);

			indicadorDescontoBloqueio = "N".equals(saidaDTO
					.getCdIndicadorDescontoBloqueio());

			setVlTarifaPadrao(saidaDTO.getVlTarifaPadrao());
		} catch (PdcAdapterFunctionalException p) {
			throw p;
		}
	}
	
	public boolean isDesabilitaCampoDescontoTarifa(){		
		
		if (indicadorDescontoBloqueio) {
			setVlDescontoTarifa(new BigDecimal(0));
			return true;
		}		
		
		return false;
	}

	// Set's and Getter's
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: manterSolicGeracaoArquivoRetBaseFavServiceImpl.
	 *
	 * @return manterSolicGeracaoArquivoRetBaseFavServiceImpl
	 */
	public IManterSolicGeracaoArquivoRetBaseFavService getManterSolicGeracaoArquivoRetBaseFavServiceImpl() {
		return manterSolicGeracaoArquivoRetBaseFavServiceImpl;
	}

	/**
	 * Set: manterSolicGeracaoArquivoRetBaseFavServiceImpl.
	 *
	 * @param manterSolicGeracaoArquivoRetBaseFavServiceImpl the manter solic geracao arquivo ret base fav service impl
	 */
	public void setManterSolicGeracaoArquivoRetBaseFavServiceImpl(
			IManterSolicGeracaoArquivoRetBaseFavService manterSolicGeracaoArquivoRetBaseFavServiceImpl) {
		this.manterSolicGeracaoArquivoRetBaseFavServiceImpl = manterSolicGeracaoArquivoRetBaseFavServiceImpl;
	}

	/**
	 * Get: cboOrigemSolicitacao.
	 *
	 * @return cboOrigemSolicitacao
	 */
	public Integer getCboOrigemSolicitacao() {
		return cboOrigemSolicitacao;
	}

	/**
	 * Set: cboOrigemSolicitacao.
	 *
	 * @param cboOrigemSolicitacao the cbo origem solicitacao
	 */
	public void setCboOrigemSolicitacao(Integer cboOrigemSolicitacao) {
		this.cboOrigemSolicitacao = cboOrigemSolicitacao;
	}

	/**
	 * Get: cboSituacaoSolicitacao.
	 *
	 * @return cboSituacaoSolicitacao
	 */
	public Integer getCboSituacaoSolicitacao() {
		return cboSituacaoSolicitacao;
	}

	/**
	 * Set: cboSituacaoSolicitacao.
	 *
	 * @param cboSituacaoSolicitacao the cbo situacao solicitacao
	 */
	public void setCboSituacaoSolicitacao(Integer cboSituacaoSolicitacao) {
		this.cboSituacaoSolicitacao = cboSituacaoSolicitacao;
	}

	/**
	 * Get: dataSolicitacaoAte.
	 *
	 * @return dataSolicitacaoAte
	 */
	public Date getDataSolicitacaoAte() {
		return dataSolicitacaoAte;
	}

	/**
	 * Set: dataSolicitacaoAte.
	 *
	 * @param dataSolicitacaoAte the data solicitacao ate
	 */
	public void setDataSolicitacaoAte(Date dataSolicitacaoAte) {
		this.dataSolicitacaoAte = dataSolicitacaoAte;
	}

	/**
	 * Get: dataSolicitacaoDe.
	 *
	 * @return dataSolicitacaoDe
	 */
	public Date getDataSolicitacaoDe() {
		return dataSolicitacaoDe;
	}

	/**
	 * Set: dataSolicitacaoDe.
	 *
	 * @param dataSolicitacaoDe the data solicitacao de
	 */
	public void setDataSolicitacaoDe(Date dataSolicitacaoDe) {
		this.dataSolicitacaoDe = dataSolicitacaoDe;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ListarSolBaseFavorecidoSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(
			List<ListarSolBaseFavorecidoSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: listaSituacaoSolicitacao.
	 *
	 * @return listaSituacaoSolicitacao
	 */
	public List<SelectItem> getListaSituacaoSolicitacao() {
		return listaSituacaoSolicitacao;
	}

	/**
	 * Set: listaSituacaoSolicitacao.
	 *
	 * @param listaSituacaoSolicitacao the lista situacao solicitacao
	 */
	public void setListaSituacaoSolicitacao(
			List<SelectItem> listaSituacaoSolicitacao) {
		this.listaSituacaoSolicitacao = listaSituacaoSolicitacao;
	}

	/**
	 * Get: numeroSolicitacao.
	 *
	 * @return numeroSolicitacao
	 */
	public Integer getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	/**
	 * Set: numeroSolicitacao.
	 *
	 * @param numeroSolicitacao the numero solicitacao
	 */
	public void setNumeroSolicitacao(Integer numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: listaGridControleRadio.
	 *
	 * @return listaGridControleRadio
	 */
	public List<SelectItem> getListaGridControleRadio() {
		return listaGridControleRadio;
	}

	/**
	 * Set: listaGridControleRadio.
	 *
	 * @param listaGridControleRadio the lista grid controle radio
	 */
	public void setListaGridControleRadio(
			List<SelectItem> listaGridControleRadio) {
		this.listaGridControleRadio = listaGridControleRadio;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: horaAtendimento.
	 *
	 * @return horaAtendimento
	 */
	public String getHoraAtendimento() {
		return horaAtendimento;
	}

	/**
	 * Set: horaAtendimento.
	 *
	 * @param horaAtendimento the hora atendimento
	 */
	public void setHoraAtendimento(String horaAtendimento) {
		this.horaAtendimento = horaAtendimento;
	}

	/**
	 * Get: horaSolicitacao.
	 *
	 * @return horaSolicitacao
	 */
	public String getHoraSolicitacao() {
		return horaSolicitacao;
	}

	/**
	 * Set: horaSolicitacao.
	 *
	 * @param horaSolicitacao the hora solicitacao
	 */
	public void setHoraSolicitacao(String horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}

	/**
	 * Get: numeroSolic.
	 *
	 * @return numeroSolic
	 */
	public Integer getNumeroSolic() {
		return numeroSolic;
	}

	/**
	 * Set: numeroSolic.
	 *
	 * @param numeroSolic the numero solic
	 */
	public void setNumeroSolic(Integer numeroSolic) {
		this.numeroSolic = numeroSolic;
	}

	/**
	 * Get: qtdeRegistros.
	 *
	 * @return qtdeRegistros
	 */
	public Long getQtdeRegistros() {
		return qtdeRegistros;
	}

	/**
	 * Set: qtdeRegistros.
	 *
	 * @param qtdeRegistros the qtde registros
	 */
	public void setQtdeRegistros(Long qtdeRegistros) {
		this.qtdeRegistros = qtdeRegistros;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}

	/**
	 * Get: situacaoSolicitacao.
	 *
	 * @return situacaoSolicitacao
	 */
	public String getSituacaoSolicitacao() {
		return situacaoSolicitacao;
	}

	/**
	 * Set: situacaoSolicitacao.
	 *
	 * @param situacaoSolicitacao the situacao solicitacao
	 */
	public void setSituacaoSolicitacao(String situacaoSolicitacao) {
		this.situacaoSolicitacao = situacaoSolicitacao;
	}

	/**
	 * Get: situacaoFavorecido.
	 *
	 * @return situacaoFavorecido
	 */
	public String getSituacaoFavorecido() {
		return situacaoFavorecido;
	}

	/**
	 * Set: situacaoFavorecido.
	 *
	 * @param situacaoFavorecido the situacao favorecido
	 */
	public void setSituacaoFavorecido(String situacaoFavorecido) {
		this.situacaoFavorecido = situacaoFavorecido;
	}

	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public String getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(String tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: destino.
	 *
	 * @return destino
	 */
	public Integer getDestino() {
		return destino;
	}

	/**
	 * Set: destino.
	 *
	 * @param destino the destino
	 */
	public void setDestino(Integer destino) {
		this.destino = destino;
	}

	/**
	 * Get: vlDescontoTarifa.
	 *
	 * @return vlDescontoTarifa
	 */
	public BigDecimal getVlDescontoTarifa() {
		return vlDescontoTarifa;
	}

	/**
	 * Set: vlDescontoTarifa.
	 *
	 * @param vlDescontoTarifa the vl desconto tarifa
	 */
	public void setVlDescontoTarifa(BigDecimal vlDescontoTarifa) {
		this.vlDescontoTarifa = vlDescontoTarifa;
	}

	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	/**
	 * Get: vlTarifaAtualizada.
	 *
	 * @return vlTarifaAtualizada
	 */
	public BigDecimal getVlTarifaAtualizada() {
		return vlTarifaAtualizada;
	}

	/**
	 * Set: vlTarifaAtualizada.
	 *
	 * @param vlTarifaAtualizada the vl tarifa atualizada
	 */
	public void setVlTarifaAtualizada(BigDecimal vlTarifaAtualizada) {
		this.vlTarifaAtualizada = vlTarifaAtualizada;
	}

	/**
	 * Get: cboSituacaoFavorecido.
	 *
	 * @return cboSituacaoFavorecido
	 */
	public Integer getCboSituacaoFavorecido() {
		return cboSituacaoFavorecido;
	}

	/**
	 * Set: cboSituacaoFavorecido.
	 *
	 * @param cboSituacaoFavorecido the cbo situacao favorecido
	 */
	public void setCboSituacaoFavorecido(Integer cboSituacaoFavorecido) {
		this.cboSituacaoFavorecido = cboSituacaoFavorecido;
	}

	/**
	 * Get: cboTipoFavorecido.
	 *
	 * @return cboTipoFavorecido
	 */
	public Integer getCboTipoFavorecido() {
		return cboTipoFavorecido;
	}

	/**
	 * Set: cboTipoFavorecido.
	 *
	 * @param cboTipoFavorecido the cbo tipo favorecido
	 */
	public void setCboTipoFavorecido(Integer cboTipoFavorecido) {
		this.cboTipoFavorecido = cboTipoFavorecido;
	}

	/**
	 * Get: listaTipoFavorecido.
	 *
	 * @return listaTipoFavorecido
	 */
	public List<SelectItem> getListaTipoFavorecido() {
		return listaTipoFavorecido;
	}

	/**
	 * Set: listaTipoFavorecido.
	 *
	 * @param listaTipoFavorecido the lista tipo favorecido
	 */
	public void setListaTipoFavorecido(List<SelectItem> listaTipoFavorecido) {
		this.listaTipoFavorecido = listaTipoFavorecido;
	}

	/**
	 * Get: radioDestino.
	 *
	 * @return radioDestino
	 */
	public String getRadioDestino() {
		return radioDestino;
	}

	/**
	 * Set: radioDestino.
	 *
	 * @param radioDestino the radio destino
	 */
	public void setRadioDestino(String radioDestino) {
		this.radioDestino = radioDestino;
	}

	/**
	 * Get: listaTipoFavorecidoHash.
	 *
	 * @return listaTipoFavorecidoHash
	 */
	public Map<Integer, String> getListaTipoFavorecidoHash() {
		return listaTipoFavorecidoHash;
	}

	/**
	 * Set lista tipo favorecido hash.
	 *
	 * @param listaTipoFavorecidoHash the lista tipo favorecido hash
	 */
	public void setListaTipoFavorecidoHash(
			Map<Integer, String> listaTipoFavorecidoHash) {
		this.listaTipoFavorecidoHash = listaTipoFavorecidoHash;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Get: manterSolicitacaoRastFavService.
	 *
	 * @return manterSolicitacaoRastFavService
	 */
	public IManterSolicitacaoRastFavService getManterSolicitacaoRastFavService() {
		return manterSolicitacaoRastFavService;
	}

	/**
	 * Set: manterSolicitacaoRastFavService.
	 *
	 * @param manterSolicitacaoRastFavService the manter solicitacao rast fav service
	 */
	public void setManterSolicitacaoRastFavService(
			IManterSolicitacaoRastFavService manterSolicitacaoRastFavService) {
		this.manterSolicitacaoRastFavService = manterSolicitacaoRastFavService;
	}

	/**
	 * Get: atributoSoltc.
	 *
	 * @return atributoSoltc
	 */
	public VerificaraAtributoSoltcSaidaDTO getAtributoSoltc() {
		return atributoSoltc;
	}

	/**
	 * Set: atributoSoltc.
	 *
	 * @param atributoSoltc the atributo soltc
	 */
	public void setAtributoSoltc(VerificaraAtributoSoltcSaidaDTO atributoSoltc) {
		this.atributoSoltc = atributoSoltc;
	}

	/**
	 * Is desabilita desconto tarifa.
	 *
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		if (atributoSoltc != null
				&& "S".equalsIgnoreCase(atributoSoltc.getDsTarifaBloqueio())) {
			return true;
		}
		return false;
	}

	/**
	 * Is indicador desconto bloqueio.
	 *
	 * @return true, if is indicador desconto bloqueio
	 */
	public boolean isIndicadorDescontoBloqueio() {
		return indicadorDescontoBloqueio;
	}

	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}
}
