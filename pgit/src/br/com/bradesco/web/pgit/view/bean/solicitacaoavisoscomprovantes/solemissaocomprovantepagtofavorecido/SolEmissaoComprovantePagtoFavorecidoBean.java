/*
 * Nome: br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.solemissaocomprovantepagtofavorecido
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.solemissaocomprovantepagtofavorecido;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoService;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarPagtoSolEmiCompFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarPagtoSolEmiCompFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.filtroenderecoemail.FiltroEnderecoEmailBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: SolEmissaoComprovantePagtoFavorecidoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolEmissaoComprovantePagtoFavorecidoBean{
	/** Atributos **/
	private IConsultasService consultasService;
	
	/** Atributo solEmissaoComprovantePagtoFavorecidoServiceImpl. */
	private ISolEmissaoComprovantePagtoFavorecidoService solEmissaoComprovantePagtoFavorecidoServiceImpl;
	
	/** Atributo solEmissaoComprovantePagtoClientePagServiceImpl. */
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo filtroEnderecoEmailBean. */
	private FiltroEnderecoEmailBean filtroEnderecoEmailBean;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo cdBancoContaDebito. */
	private Integer cdBancoContaDebito;
	
	/** Atributo cdAgenciaBancariaContaDebito. */
	private Integer cdAgenciaBancariaContaDebito;
	
	/** Atributo cdContaBancariaContaDebito. */
	private Long cdContaBancariaContaDebito;
	
	/** Atributo cdDigitoContaContaDebito. */
	private String cdDigitoContaContaDebito;
	
	/** Atributo cdBancoContaCredito. */
	private Integer cdBancoContaCredito;
	
	/** Atributo cdAgenciaBancariaContaCredito. */
	private Integer cdAgenciaBancariaContaCredito;
	
	/** Atributo cdContaBancariaContaCredito. */
	private Long cdContaBancariaContaCredito;
	
	/** Atributo cdDigitoContaCredito. */
	private String cdDigitoContaCredito;
	
	//solEmissaoComprovantePagtoFavorecido
	/** Atributo dtInicioSolicitacao. */
	private Date dtInicioSolicitacao;
	
	/** Atributo dtFimSolicitacao. */
	private Date dtFimSolicitacao;
	
	/** Atributo cboSituacaoSolicitacaoEstornoFiltro. */
	private Integer cboSituacaoSolicitacaoEstornoFiltro;
	
	/** Atributo listaSituacaoSolicitacaoEstornoFiltro. */
	private List<SelectItem> listaSituacaoSolicitacaoEstornoFiltro;
	
	/** Atributo listaGridSolEmiComprovanteFavorecido. */
	private List<ConsultarSolEmiComprovanteFavorecidoSaidaDTO> listaGridSolEmiComprovanteFavorecido;
	
	/** Atributo listaRadios. */
	private List<SelectItem> listaRadios;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	//detSolEmissaoComprovantePagtoFavorecido
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;
	
	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private Integer cdSolicitacaoPagamentoIntegrado;
    
    /** Atributo nrSolicitacaoPagamentoIntegrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;
    
    /** Atributo dtInicioPeriodoMovimentacao. */
    private String dtInicioPeriodoMovimentacao;
    
    /** Atributo dtFimPeriodoMovimentacao. */
    private String dtFimPeriodoMovimentacao;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo dsProdutoServicoOperacao. */
    private String dsProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo dsProdutoOperacaoRelacionado. */
    private String dsProdutoOperacaoRelacionado;
    
    /** Atributo cdTipoInscricaoRecebedor. */
    private Integer cdTipoInscricaoRecebedor;
    
    /** Atributo cdDestinoCorrespSolicitacao. */
    private Integer cdDestinoCorrespSolicitacao;
    
    /** Atributo cdTipoPostagemSolicitacao. */
    private String cdTipoPostagemSolicitacao;
    
    /** Atributo dsLogradouroPagador. */
    private String dsLogradouroPagador;
    
    /** Atributo dsNumeroLogradouroPagador. */
    private String dsNumeroLogradouroPagador;
    
    /** Atributo dsComplementoLogradouroPagador. */
    private String dsComplementoLogradouroPagador;
    
    /** Atributo dsBairroClientePagador. */
    private String dsBairroClientePagador;
    
    /** Atributo dsMunicipioClientePagador. */
    private String dsMunicipioClientePagador;
    
    /** Atributo cdSiglaUfPagador. */
    private String cdSiglaUfPagador;
    
    /** Atributo cdCepPagador. */
    private Integer cdCepPagador;
    
    /** Atributo cdCepComplementoPagador. */
    private Integer cdCepComplementoPagador;
    
    /** Atributo dsEmailClientePagador. */
    private String dsEmailClientePagador;
    
    /** Atributo cdAgenciaOperadora. */
    private Integer cdAgenciaOperadora;
    
    /** Atributo dsAgenciaOperadora. */
    private String dsAgenciaOperadora;
    
    /** Atributo dsDepartamentoUnidade. */
    private String dsDepartamentoUnidade;
    
    /** Atributo vlTarifa. */
    private BigDecimal vlTarifa;
    
    /** Atributo percentualDescTarifa. */
    private BigDecimal percentualDescTarifa;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;
    
    /** Atributo tarifaAtualizada. */
    private BigDecimal tarifaAtualizada;
    
    /** Atributo cepFormatado. */
    private String cepFormatado;
    
    /** Atributo cdSituacaoSolicitacao. */
    private Integer cdSituacaoSolicitacao;
    
    /** Atributo dsSituacaoSolicitacao. */
    private String dsSituacaoSolicitacao;
    
    /** Atributo comprovantes. */
    private List<DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO> comprovantes;
    
    //incSolEmissaoComprovantePagtoFavorecidoFiltro
    /** Atributo radioIncluirSelecionado. */
    private String radioIncluirSelecionado;
    
    /** Atributo radioIncluirFavorecido. */
    private String radioIncluirFavorecido;
    
    /** Atributo dtInicioPeriodoPagamento. */
    private Date dtInicioPeriodoPagamento;
    
    /** Atributo dtFimPeriodoPagamento. */
    private Date dtFimPeriodoPagamento;
    
    /** Atributo tipoServicoFiltro. */
    private Integer tipoServicoFiltro;
	
	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;
	
	/** Atributo numeroPagamentoDe. */
	private String numeroPagamentoDe;
	
	/** Atributo numeroPagamentoAte. */
	private String numeroPagamentoAte;
	
	/** Atributo valorPagamentoDe. */
	private BigDecimal valorPagamentoDe;
	
	/** Atributo valorPagamentoAte. */
	private BigDecimal valorPagamentoAte;
	
	/** Atributo cdFavorecido. */
	private Long cdFavorecido;
    
    /** Atributo inscricaoFavorecido. */
    private String inscricaoFavorecido;
	
	/** Atributo tipoInscricaoFiltro. */
	private Integer tipoInscricaoFiltro;
	
	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();
	
	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeHash. */
	private Map<Integer,String> listaModalidadeHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoInscricaoFiltro. */
	private List<SelectItem>  listaTipoInscricaoFiltro = new ArrayList<SelectItem>();
    
    /** Atributo listaIncluirComprovantes. */
    private List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> listaIncluirComprovantes;
    
    /** Atributo opcaoChecarTodos. */
    private boolean opcaoChecarTodos = false;
	
    //incSolEmissaoComprovantePagtoFavorecidoInfoAdicionais
	/** Atributo listaIncluirComprovantesSelecionados. */
    private List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> listaIncluirComprovantesSelecionados;
	
	/** Atributo radioInfoAdicionais. */
	private String radioInfoAdicionais;
	
	/** Atributo radioTipoPostagem. */
	private String radioTipoPostagem;
	
	/** Atributo radioAgenciaDepto. */
	private String radioAgenciaDepto;
	
	/** Atributo agenciaOperadora. */
	private String agenciaOperadora;
	
	/** Atributo codAgenciaOperadora. */
	private Integer codAgenciaOperadora;
	
	/** Atributo departamentoFiltro. */
	private String departamentoFiltro;
	
	/** Atributo cdPessoaJuridicaDepartamento. */
	private Long cdPessoaJuridicaDepartamento;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaDepartamento. */
    private Integer nrSequenciaDepartamento;
    
    /** Atributo habilitaCampoEndereco. */
    private boolean habilitaCampoEndereco;
    
    //Atributos Tela Incluir

	/** Atributo disableArgumentosConsulta. */
    private boolean disableArgumentosConsulta;
	
	/** Atributo disableArgumentosIncluir. */
	private boolean disableArgumentosIncluir;	
	
	/** Atributo panelBotoes. */
	private boolean panelBotoes;
	
	/** Atributo desabilitaDescontoTarifa. */
	private boolean desabilitaDescontoTarifa;
	
	/** M�todos **/
	//Inicializa��o
	public void iniciarTela(ActionEvent evt){
		limparTelaPrincipal();
		
		//Carregamento do Combo
		listarConsultarSituacaoSolicitacaoEstorno();		
		
		//Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("solEmissaoComprovantePagtoFavorecido");
		
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		
		//Tela de Filtro de Endere�o/Email
		getFiltroEnderecoEmailBean().setPaginaRetorno("incSolEmissaoComprovantePagtoFavorecidoInfAdicionais");		
		
		setPanelBotoes(false);
		setDisableArgumentosConsulta(false);
		setDesabilitaDescontoTarifa(false);
	}
	
	//Consultar Situa��o de Solicita��o de Estorno
	/**
	 * Listar consultar situacao solicitacao estorno.
	 */
	public void listarConsultarSituacaoSolicitacaoEstorno(){
		try{
			listaSituacaoSolicitacaoEstornoFiltro = new ArrayList<SelectItem>();
			
			ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada = new ConsultarSituacaoSolicitacaoEstornoEntradaDTO();
			
			entrada.setCdSituacao(0);
			entrada.setNumeroOcorrencias(30);
			entrada.setCdIndicador(1);
			
			List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> list = getComboService().consultarSituacaoSolicitacaoEstorno(entrada);
	
			for (ConsultarSituacaoSolicitacaoEstornoSaidaDTO saida : list){
				listaSituacaoSolicitacaoEstornoFiltro.add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
			}
		}catch(PdcAdapterFunctionalException p){
			listaSituacaoSolicitacaoEstornoFiltro = new ArrayList<SelectItem>();
		}		
	}
	
	/**
	 * Listar cmb tipo favorecido.
	 */
	public void listarCmbTipoFavorecido(){
		try{
			listaTipoInscricaoFiltro = new ArrayList<SelectItem>();
			/*
			List<InscricaoFavorecidosSaidaDTO> list = new ArrayList<InscricaoFavorecidosSaidaDTO>();
			
			list = comboService.listarTipoInscricao();
			listaTipoInscricaoFiltro.clear();
			
			for(InscricaoFavorecidosSaidaDTO combo : list){			
				listaTipoInscricaoFiltro.add(new SelectItem(combo.getCdTipoInscricaoFavorecidos(),combo.getDsTipoInscricaoFavorecidos()));
			}
			*/
			listaTipoInscricaoFiltro.add(new SelectItem(1,MessageHelperUtils.getI18nMessage("label_cpf")));
			listaTipoInscricaoFiltro.add(new SelectItem(2,MessageHelperUtils.getI18nMessage("label_cnpj")));
			
		}catch(PdcAdapterFunctionalException p){
			listaTipoInscricaoFiltro = new ArrayList<SelectItem>();
		}
	}
	
	//Controle Exibi��o de Dados	
	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho(){
		if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0")){ //Filtrar por cliente
			setNrCnpjCpf(filtroAgendamentoEfetivacaoEstornoBean.getNrCpfCnpjCliente());
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoRazaoSocialCliente());
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescCliente());
		}
		else{
			if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){ //Filtrar por contrato
				try{
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
					entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
					entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());				
					DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService().detalharDadosContrato(entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException p) {	
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescContrato());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescContrato());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescContrato());
			}
		}
	}
	
	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt){
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmiComprovanteFavorecido(null);
		setItemSelecionadoLista(null);
		setPanelBotoes(false);
	}
	
	/**
	 * Limpar argumentos incluir.
	 */
	public void limparArgumentosIncluir(){
		setRadioIncluirFavorecido("");
		setNumeroPagamentoDe("");
		setNumeroPagamentoAte("");
		setValorPagamentoDe(null);
		setValorPagamentoAte(null);
		limparArgumentosFavorecido();
		limparListaIncluir();
	}
	
	/**
	 * Limpar argumentos favorecido.
	 */
	public void limparArgumentosFavorecido(){
		setCdFavorecido(null);
	    setInscricaoFavorecido(null);
	    setTipoInscricaoFiltro(null);
	    limparListaIncluir();
	}
	
	/**
	 * Limpar info adicionais.
	 */
	public void limparInfoAdicionais(){
		setRadioTipoPostagem("");
		setRadioAgenciaDepto("");
		setDepartamentoFiltro("");
		limparTipoPostagem();
		getFiltroEnderecoEmailBean().limparEnderecoEmail();
		getFiltroEnderecoEmailBean().setEmpresaGestoraContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
	}
	
	/**
	 * Limpar tipo postagem.
	 */
	public void limparTipoPostagem(){
		getFiltroEnderecoEmailBean().limparEnderecoEmail();
		getFiltroEnderecoEmailBean().setFlagEnderecoEmail(getRadioTipoPostagem());

		setHabilitaCampoEndereco(true);
		setCdCepPagador(0);
		setCdCepComplementoPagador(0);
	}
	
	/**
	 * Limpar agencia operadora.
	 */
	public void limparAgenciaOperadora(){
		setDepartamentoFiltro("");
	}
	
	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmiComprovanteFavorecido(null);
		setItemSelecionadoLista(null);
		setPanelBotoes(false);
		return "";
	}
	
	/**
	 * Limpar grid consultar.
	 *
	 * @return the string
	 */
	public String limparGridConsultar(){
		setListaGridSolEmiComprovanteFavorecido(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		return "";
	}
	
	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente(){
		//Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmiComprovanteFavorecido(null);
		setItemSelecionadoLista(null);
		setPanelBotoes(false);
		
		return "";

	}	
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		limparTelaPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado("");
		setDisableArgumentosConsulta(false);
		return "";
	}
	
	/**
	 * Limpar variaveis detalhes.
	 */
	public void limparVariaveisDetalhes(){
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		setNrSolicitacaoPagamentoIntegrado(null);
	    setDtInicioPeriodoMovimentacao("");
	    setDtFimPeriodoMovimentacao("");
	    setDsProdutoServicoOperacao("");
	    setDsProdutoOperacaoRelacionado("");
	    setDsLogradouroPagador("");
	    setDsNumeroLogradouroPagador("");
	    setDsComplementoLogradouroPagador("");
	    setDsBairroClientePagador("");
	    setDsMunicipioClientePagador("");
	    setCdSiglaUfPagador("");
	    setCepFormatado("");
	    setDsEmailClientePagador("");
	    setDsAgenciaOperadora("");
	    setDsDepartamentoUnidade(null);
	    setVlTarifa(BigDecimal.ZERO);
	    setPercentualDescTarifa(BigDecimal.ZERO);
	    setTarifaAtualizada(BigDecimal.ZERO);
	    setHrInclusaoRegistro("");
	    setCdUsuarioInclusao("");
	    setTipoCanalInclusao("");
	    setCdOperacaoCanalInclusao("");
	}
	
	/**
	 * Limpar incluir.
	 */
	public void limparIncluir(){
		setRadioIncluirSelecionado("");
	    setRadioIncluirFavorecido("");
		setDtInicioPeriodoPagamento(new Date());
		setDtFimPeriodoPagamento(new Date());
	    setTipoServicoFiltro(null);
		setModalidadeFiltro(null);
		limparArgumentosIncluir();
		limparArgumentosFavorecido();
		limparListaIncluir();
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);				

		setListaModalidadeFiltro(new ArrayList<SelectItem>());		
	}
	
	/**
	 * Limpar lista incluir.
	 */
	public void limparListaIncluir(){
		setListaIncluirComprovantes(null);
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		setDisableArgumentosIncluir(false);
	}
	
	/**
	 * Consultar sol emi comprovante favorecido.
	 *
	 * @param evt the evt
	 */
	public void consultarSolEmiComprovanteFavorecido(ActionEvent evt){
		consultarSolEmiComprovanteFavorecido();
	}
	//Carrega Lista
	/**
	 * Consultar sol emi comprovante favorecido.
	 */
	public void consultarSolEmiComprovanteFavorecido(){
		try{
			ConsultarSolEmiComprovanteFavorecidoEntradaDTO entradaDTO = new ConsultarSolEmiComprovanteFavorecidoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato()	: 0L);
			entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() : 0);
			entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() : 0L);	
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entradaDTO.setDtInicioSolicitacao((getDtInicioSolicitacao() == null) ? "" : sdf.format(getDtInicioSolicitacao()) );
			entradaDTO.setDtFimSolicitacao((getDtFimSolicitacao() == null) ? "" : sdf.format(getDtFimSolicitacao()));
	    	entradaDTO.setCdSituacaoSolicitacao(getCboSituacaoSolicitacaoEstornoFiltro() != null ? getCboSituacaoSolicitacaoEstornoFiltro() : 0);
	    	
	    	setListaGridSolEmiComprovanteFavorecido(getSolEmissaoComprovantePagtoFavorecidoServiceImpl().consultarSolEmiComprovanteFavorecido(entradaDTO));
	    	
	    	listaRadios = new ArrayList<SelectItem>();
			
			for(int i=0;i<getListaGridSolEmiComprovanteFavorecido().size();i++){
				listaRadios.add(new SelectItem(i,""));
			}
			setDisableArgumentosConsulta(true);
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridSolEmiComprovanteFavorecido(null);
			setDisableArgumentosConsulta(false);
		}
		finally{
			setItemSelecionadoLista(null);
		}
	}
	
	/**
	 * Pesquisar detalhar excluir.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarDetalharExcluir(ActionEvent evt){
		preencheDadosSolEmiComprovanteFavorecido("");
		return "";
	}
	
	/**
	 * Preenche dados sol emi comprovante favorecido.
	 *
	 * @param retorno the retorno
	 * @return the string
	 */
	public String preencheDadosSolEmiComprovanteFavorecido(String retorno){
		try{
			DetalharSolEmiComprovanteFavorecidoEntradaDTO entradaDTO = new DetalharSolEmiComprovanteFavorecidoEntradaDTO();
			
			limparVariaveisDetalhes();
			carregaCabecalho();
			
			entradaDTO.setCdPessoaJuridicaEmpr(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getNrSequenciaContrato());
	    	entradaDTO.setCdTipoSolicitacaoPagamento(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getCdTipoSolicitacao());
	    	entradaDTO.setCdSolicitacao(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getNrSolicitacao());
	    	
	    	DetalharSolEmiComprovanteFavorecidoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoFavorecidoServiceImpl().detalharSolEmiComprovanteFavorecido(entradaDTO);
	    	
	    	setCdSolicitacaoPagamentoIntegrado(saidaDTO.getCdSolicitacaoPagamentoIntegrado());
	    	setNrSolicitacaoPagamentoIntegrado(saidaDTO.getNrSolicitacaoPagamentoIntegrado());
	    	setDtInicioPeriodoMovimentacao(saidaDTO.getDtInicioPeriodoMovimentacao());
			setDtFimPeriodoMovimentacao(saidaDTO.getDtFimPeriodoMovimentacao());
			setCdProdutoServicoOperacao(saidaDTO.getCdProdutoServicoOperacao());
		    setDsProdutoServicoOperacao(saidaDTO.getDsProdutoServicoOperacao());
		    setCdProdutoOperacaoRelacionado(saidaDTO.getCdProdutoOperacaoRelacionado());
		    setDsProdutoOperacaoRelacionado(saidaDTO.getDsProdutoOperacaoRelacionado());
		    setCdTipoInscricaoRecebedor(saidaDTO.getCdTipoInscricaoRecebedor());
		    
		    if(saidaDTO.getCdDestinoCorrespSolicitacao() == 1){
		    	if(saidaDTO.getCdTipoPostagemSolicitacao().equals("E")){
		    		setDsEmailClientePagador(saidaDTO.getDsEmailClientePagador());
		    	}else{
		    		if(saidaDTO.getCdTipoPostagemSolicitacao().equals("C")){
		    			setDsLogradouroPagador(saidaDTO.getDsLogradouroPagador());
					    setDsNumeroLogradouroPagador(saidaDTO.getDsNumeroLogradouroPagador());
					    setDsComplementoLogradouroPagador(saidaDTO.getDsComplementoLogradouroPagador());
					    setDsBairroClientePagador(saidaDTO.getDsBairroClientePagador());
					    setDsMunicipioClientePagador(saidaDTO.getDsMunicipioClientePagador());
					    setCdSiglaUfPagador(saidaDTO.getCdSiglaUfPagador());
					    setCepFormatado(saidaDTO.getCepFormatado());
		    		}
				}
		    }else{
		    	if(saidaDTO.getCdDestinoCorrespSolicitacao() == 2){
		    		if(saidaDTO.getCdTipoPostagemSolicitacao().equals("A")){
		    			setDsAgenciaOperadora(PgitUtil.concatenarCampos(saidaDTO.getCdAgenciaOperadora(), saidaDTO.getDsAgencia()));
		    		}else{
		    			if(saidaDTO.getCdTipoPostagemSolicitacao().equals("D")){
		    				setDsDepartamentoUnidade(PgitUtil.concatenarCampos(saidaDTO.getCdDepartamentoUnidade(), saidaDTO.getDsAgencia()));
		    			}
		    		}
		    	}
		    }
		   
		    setVlTarifa(saidaDTO.getVlTarifa());
		    setPercentualDescTarifa(saidaDTO.getCdPercentualDescTarifa());
		    setTarifaAtualizada(PgitUtil.calcularValorTarifaAtualizada(getVlTarifa(), getPercentualDescTarifa()));
		    setComprovantes(saidaDTO.getComprovantes());
		    
		    //Trilha
		    setCdUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
		    setHrInclusaoRegistro(saidaDTO.getHrInclusaoRegistro());
		    setCdOperacaoCanalInclusao(saidaDTO.getCdOperacaoCanalInclusao());
		    setTipoCanalInclusao(PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalInclusao(), saidaDTO.getDsTipoCanalInclusao()));
		
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setComprovantes(null);
			return "";
		}
		return retorno;
	}
	
	/**
	 * Excluir sol emi comprovante favorecido.
	 *
	 * @return the string
	 */
	public String excluirSolEmiComprovanteFavorecido(){
		try{
			ExcluirSolEmiComprovanteFavorecidoEntradaDTO entradaDTO = new ExcluirSolEmiComprovanteFavorecidoEntradaDTO();
			
			entradaDTO.setCdSolicitacaoPagamentoIntegrado(getCdSolicitacaoPagamentoIntegrado());
	    	entradaDTO.setNrSolicitacaoPagamentoIntegrado(getNrSolicitacaoPagamentoIntegrado());
	    	entradaDTO.setCdPessoaJuridicaContrato(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getCdPessoaJuridica());
	        entradaDTO.setNrSequenciaContratoNegocio(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getNrSequenciaContrato());
	        entradaDTO.setCdTipoContratoNegocio(getListaGridSolEmiComprovanteFavorecido().get(itemSelecionadoLista).getCdTipoContrato());
	        
	        ExcluirSolEmiComprovanteFavorecidoOcorrenciasDTO ocorrencia;
	        for(int i=0; i<getComprovantes().size(); i++){
	        	ocorrencia = new ExcluirSolEmiComprovanteFavorecidoOcorrenciasDTO();
	        	
	        	ocorrencia.setCdControlePagamento(getComprovantes().get(i).getCdControlePagamento());
	        	ocorrencia.setCdTipoCanal(getComprovantes().get(i).getCdTipoCanal());
	     	    entradaDTO.getComprovantes().add(ocorrencia);
	        }
	        
	        ExcluirSolEmiComprovanteFavorecidoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoFavorecidoServiceImpl().excluirSolEmiComprovanteFavorecido(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "solEmissaoComprovantePagtoFavorecido", "#{solEmissaoComprovantePagtoFavorecidoBean.consultarSolEmiComprovanteFavorecido}", false);
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Incluir sol emi comprovante favorecido.
	 *
	 * @return the string
	 */
	public String incluirSolEmiComprovanteFavorecido(){
		try{
			IncluirSolEmiComprovanteFavorecidoEntradaDTO entradaDTO = new IncluirSolEmiComprovanteFavorecidoEntradaDTO();
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
	        entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
	        entradaDTO.setDtInicioPeriodoMovimentacao(sdf.format(getDtInicioPeriodoPagamento()));
	        entradaDTO.setDtFimPeriodoMovimentacao(sdf.format(getDtFimPeriodoPagamento()));
			entradaDTO.setCdRecebedorCredito(getCdFavorecido());
			entradaDTO.setCdTipoInscricaoRecebedor(getTipoInscricaoFiltro());
			entradaDTO.setCdProdutoServicoOperacao(tipoServicoFiltro == null ? 0 : tipoServicoFiltro);
			entradaDTO.setCdProdutoOperacaoRelacionado(modalidadeFiltro == null ? 0 : modalidadeFiltro);

			setInscricaoFavorecido(PgitUtil.complementaDigito(PgitUtil.verificaStringNula(getInscricaoFavorecido()), 15));
			entradaDTO.setCdCpfCnpjRecebedor(Long.valueOf(getInscricaoFavorecido().substring(0, 9)));
	        entradaDTO.setCdFilialCnpjRecebedor(Integer.parseInt(getInscricaoFavorecido().substring(9, 13)));
	        entradaDTO.setCdControleCpfRecebedor(Integer.parseInt(getInscricaoFavorecido().substring(13, 15)));
			if(getRadioInfoAdicionais().equals("0") && getRadioTipoPostagem().equals("0")){
				entradaDTO.setCdTipoPostagemSolicitacao("C");
				entradaDTO.setCdDestinoCorrespSolicitacao(1);
			}else{
				if(getRadioInfoAdicionais().equals("0") && getRadioTipoPostagem().equals("1")){
					entradaDTO.setCdTipoPostagemSolicitacao("E");
					entradaDTO.setCdDestinoCorrespSolicitacao(1);
				}else{
					if(getRadioInfoAdicionais().equals("1") && getRadioAgenciaDepto().equals("0")){
						entradaDTO.setCdTipoPostagemSolicitacao("A");
						entradaDTO.setCdPessoaJuridicaDepto(getCdPessoaJuridicaDepartamento());
						entradaDTO.setNrSequenciaUnidadeDepto(getNrSequenciaDepartamento());
						entradaDTO.setCdDestinoCorrespSolicitacao(2);
					}else{
						if(getRadioInfoAdicionais().equals("1") && getRadioAgenciaDepto().equals("1")){
							entradaDTO.setCdTipoPostagemSolicitacao("D");
							entradaDTO.setCdPessoaJuridicaDepto(getCdPessoaJuridicaDepartamento());
							entradaDTO.setNrSequenciaUnidadeDepto(getNrSequenciaDepartamento());
							entradaDTO.setCdDestinoCorrespSolicitacao(2);
						}
					}
				}
			}
			entradaDTO.setDsLogradouroPagador(getDsLogradouroPagador());
	        entradaDTO.setDsNumeroLogradouroPagador(getDsNumeroLogradouroPagador());
	        entradaDTO.setDsComplementoLogradouroPagador(getDsComplementoLogradouroPagador());
	        entradaDTO.setDsBairroClientePagador(getDsBairroClientePagador());
	        entradaDTO.setDsMunicipioClientePagador(getDsMunicipioClientePagador());
	        entradaDTO.setCdSiglaUfPagador(getCdSiglaUfPagador());
	        entradaDTO.setCdCepPagador(getCdCepPagador());
	        entradaDTO.setCdCepComplementoPagador(getCdCepComplementoPagador());
	        entradaDTO.setDsEmailClientePagador(getDsEmailClientePagador());
	        entradaDTO.setCdPercentualBonifTarifaSolicitacao(getPercentualDescTarifa());
	        entradaDTO.setVlTarifaNegocioSolicitacao(getVlTarifa());
	        
	        IncluirSolEmiComprovanteFavorecidoOcorrenciasDTO ocorrencia;
	        for(int i=0; i<getListaIncluirComprovantesSelecionados().size(); i++){
	        	ocorrencia = new IncluirSolEmiComprovanteFavorecidoOcorrenciasDTO();
	        	
	        	ocorrencia.setCdControlePagamento(getListaIncluirComprovantesSelecionados().get(i).getCdControlePagamento());
	        	ocorrencia.setCdTipoCanal(0);
	     	    ocorrencia.setCdModalidadePagamentoCliente(getListaIncluirComprovantesSelecionados().get(i).getCdTipoTela());
	     	    
	     	    entradaDTO.getComprovantes().add(ocorrencia);
	        }
	        
	        entradaDTO.setCdDepartamentoUnidade(getDepartamentoFiltro().equals("")?0:Integer.parseInt(getDepartamentoFiltro()));
	        
			IncluirSolEmiComprovanteFavorecidoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoFavorecidoServiceImpl().incluirSolEmiComprovanteFavorecido(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "solEmissaoComprovantePagtoFavorecido", BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGridSolEmiComprovanteFavorecido() != null){
				consultarSolEmiComprovanteFavorecido();
			}
		
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "incSolEmissaoComprovantePagtoFavorecidoInfAdicionais", BradescoViewExceptionActionType.ACTION, false);
			setPanelBotoes(false);
			return null;
		}
		return "";
	}
	
	/**
	 * Pesquisar.
	 */
	public void pesquisar(){
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		for (int i = 0; i < getListaIncluirComprovantes().size(); i++) {
			getListaIncluirComprovantes().get(i).setCheck(false);			
		}
	}
	
	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes(){
		for(int i=0;i<getListaIncluirComprovantes().size();i++){
			if(getListaIncluirComprovantes().get(i).isRegistroSelecionado()){
				setPanelBotoes(true);
				return;
			}
		}
		setPanelBotoes(false);		
	}	
	
	/**
	 * Pesquisar incluir.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarIncluir(ActionEvent evt){
		listarIncluirSolEmiComprovante();
		return "";
	}
	
	/**
	 * Listar incluir sol emi comprovante.
	 */
	public void listarIncluirSolEmiComprovante(){
		try{
			ConsultarPagtoSolEmiCompFavorecidoEntradaDTO entradaDTO = new ConsultarPagtoSolEmiCompFavorecidoEntradaDTO();
			entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	        entradaDTO.setDtCreditoPagamentoInicio((getDtInicioPeriodoPagamento() == null) ? "" : sdf.format(getDtInicioPeriodoPagamento()));
	        entradaDTO.setDtCreditoPagamentoFim((getDtFimPeriodoPagamento() == null) ? "" : sdf.format(getDtFimPeriodoPagamento()));
	        entradaDTO.setCdProdutoServicoOperacao((getTipoServicoFiltro() == null) ? 0 : getTipoServicoFiltro());
	        entradaDTO.setCdProdutoServicoRelacionado((getModalidadeFiltro() == null) ? 0 :getModalidadeFiltro());
	        entradaDTO.setCdControlePagamentoDe(String.valueOf(getNumeroPagamentoDe()));
	        entradaDTO.setCdControlePagamentoAte(String.valueOf(getNumeroPagamentoAte()));
	        entradaDTO.setVlPagamentoDe((getValorPagamentoDe() == null) ? BigDecimal.ZERO : getValorPagamentoDe());
	        entradaDTO.setVlPagamentoAte((getValorPagamentoAte() == null) ? BigDecimal.ZERO : getValorPagamentoAte());
	        entradaDTO.setCdFavorecidoClientePagador((getCdFavorecido() == null) ? 0L : getCdFavorecido());
	        entradaDTO.setCdInscricaoFavorecido(getInscricaoFavorecido() != null ? Long.parseLong(getInscricaoFavorecido()) : 0L);
	        entradaDTO.setCdIdentificacaoInscricaoFavorecido((getTipoInscricaoFiltro() == null) ? 0 :getTipoInscricaoFiltro());
	        
	        setListaIncluirComprovantes(getSolEmissaoComprovantePagtoFavorecidoServiceImpl().consultarPagtoSolEmiCompFavorecido(entradaDTO));	
			
			setPanelBotoes(false);
			setOpcaoChecarTodos(false);			
			setDisableArgumentosIncluir(true);
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaIncluirComprovantes(null);
			setPanelBotoes(false);
			setDisableArgumentosIncluir(false);
		}
		finally{
			setOpcaoChecarTodos(false);
		}
	}
	
	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados(){
		for(int i=0;i<getListaIncluirComprovantes().size();i++){
			getListaIncluirComprovantes().get(i).setRegistroSelecionado(false);
		}
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);
	}	
	
	/**
	 * Consultar agencia operadora.
	 */
	public void consultarAgenciaOperadora(){
		try{
			ConsultarAgenciaOperadoraEntradaDTO entradaDTO = new ConsultarAgenciaOperadoraEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
	        entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
	        entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
	        
			ConsultarAgenciaOperadoraSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl().consultarAgenciaOperadora(entradaDTO);
			
			setCodAgenciaOperadora(saidaDTO.getCdAgencia());
		    setAgenciaOperadora(PgitUtil.concatenarCampos(saidaDTO.getCdAgencia(), saidaDTO.getDsAgencia()));
		    setCdPessoaJuridicaDepartamento(saidaDTO.getCdPessoaJuridicaDepartamento());
		    setNrSequenciaDepartamento(saidaDTO.getNrSequenciaDepartamento());
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
	}
	
	/**
	 * Consultar agencia ope tarifa padrao.
	 */
	public void consultarAgenciaOpeTarifaPadrao(){
		try{
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
	        entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
	        entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
	        entradaDTO.setCdProdutoServicoOperacao(0);
	        entradaDTO.setCdProdutoServicoRelacionado(0);
	        entradaDTO.setCdTipoTarifa(4);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl().consultarAgenciaOpeTarifaPadrao(entradaDTO);
			
			setVlTarifa(saidaDTO.getVlTarifaPadrao());
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
	}
	
	/**
	 * Carrega lista comprovantes selecionados.
	 */
	public void carregaListaComprovantesSelecionados(){		
		listaIncluirComprovantesSelecionados = new ArrayList<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO>();
		
		for(ConsultarPagtoSolEmiCompFavorecidoSaidaDTO saida : getListaIncluirComprovantes()){
			if(saida.isRegistroSelecionado()){
				listaIncluirComprovantesSelecionados.add(saida);
			}
		}
	}
	
	/**
	 * Checar todos.
	 */
	public void checarTodos(){
		for(ConsultarPagtoSolEmiCompFavorecidoSaidaDTO saida : getListaIncluirComprovantes()){
			saida.setRegistroSelecionado(isOpcaoChecarTodos());
		}
		setPanelBotoes(isOpcaoChecarTodos());
	}
	
	/**
	 * Listar tipo servico.
	 */
	public void listarTipoServico(){
		try{
			listaTipoServicoFiltro = new ArrayList<SelectItem>();	
			ConsultarServicoEntradaDTO entrada =  new ConsultarServicoEntradaDTO(); 
			
			entrada.setCdpessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
			entrada.setCdTipoContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
			entrada.setNrSequenciaContrato(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
			entrada.setNumeroOcorrencias(100);
			
			List<ConsultarServicoSaidaDTO> list = comboService.consultarServico(entrada);
			
			listaTipoServicoHash.clear();
			for (ConsultarServicoSaidaDTO saida : list){
				listaTipoServicoFiltro.add(new SelectItem(saida.getCdServico(), saida.getDsServico()));
				listaTipoServicoHash.put(saida.getCdServico(), saida.getDsServico());
			}
			
		} catch (PdcAdapterFunctionalException p) {
			if("PGIT1087".equals(StringUtils.right(p.getCode(), 8))  ){
	    		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),"#{solEmissaoComprovantePagtoFavorecidoBean.limparCampos}",BradescoViewExceptionActionType.ACTION, false);
    		}
		
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
			listaTipoServicoHash = new HashMap<Integer, String>();
		}			
	}	
	
	/**
	 * Consultar contrato.
	 */
	public void consultarContrato(){
		getFiltroAgendamentoEfetivacaoEstornoBean().consultarContrato();
		listarTipoServico();
		
	}
	
	/**
	 * Listar modalidades.
	 */
	public void listarModalidades(){
		try{
			setModalidadeFiltro(null);
			if ( getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0){ 
					listaModalidadeFiltro = new ArrayList<SelectItem>();
					
					ConsultarModalidadeEntradaDTO entrada = new ConsultarModalidadeEntradaDTO();
					entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro());
					
					
					List<ConsultarModalidadeSaidaDTO> list = comboService.consultarModalidade(entrada);
					listaModalidadeHash.clear();
					for (ConsultarModalidadeSaidaDTO saida : list){
						listaModalidadeFiltro.add(new SelectItem(saida.getCdProdutoOperacaoRelacionada(), saida.getDsProdutoOperacaoRelacionada()));
						listaModalidadeHash.put(saida.getCdProdutoOperacaoRelacionada(), saida.getDsProdutoOperacaoRelacionada());
					}
			}else{
				listaModalidadeFiltro = new ArrayList<SelectItem>();
			}
		} catch (PdcAdapterFunctionalException p) {
			listaModalidadeFiltro = new ArrayList<SelectItem>();
			listaModalidadeHash = new HashMap<Integer, String>();
		}
	}
	
	//Navega��o
	
	/**
	 * Limpar campos incluir.
	 */
	public void limparCamposIncluir(){
		setDsLogradouroPagador("");
		setDsNumeroLogradouroPagador("");
		setDsComplementoLogradouroPagador("");
		setDsBairroClientePagador("");
		setDsMunicipioClientePagador("");
		setCdSiglaUfPagador("");
		setCepFormatado("");
		setDsEmailClientePagador("");
		setDsAgenciaOperadora("");
		setDsDepartamentoUnidade("");
		setTarifaAtualizada(BigDecimal.ZERO);
	}
	
	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao(){
				
		//Dados Solicitacao
		setDtInicioPeriodoMovimentacao(FormatarData.formataDiaMesAno(getDtInicioPeriodoPagamento()));
		setDtFimPeriodoMovimentacao(FormatarData.formataDiaMesAno(getDtFimPeriodoPagamento()));
		setDsProdutoServicoOperacao(getListaTipoServicoHash().get(getTipoServicoFiltro()));
		setDsProdutoOperacaoRelacionado(getListaModalidadeHash().get(getModalidadeFiltro()));
		
		//DadosDestino
		limparCamposIncluir();
		if(getRadioInfoAdicionais().equals("0")){	
			if(getRadioTipoPostagem().equals("0")){
				ConsultarEnderecoSaidaDTO enderecoSelecionado = getFiltroEnderecoEmailBean().getEnderecoSelecionado();
				setDsLogradouroPagador(enderecoSelecionado.getDsLogradouroPagador());
				setDsNumeroLogradouroPagador(enderecoSelecionado.getDsNumeroLogradouroPagador());
				setDsComplementoLogradouroPagador(enderecoSelecionado.getDsComplementoLogradouroPagador());
				setDsBairroClientePagador(enderecoSelecionado.getDsBairroClientePagador());
				setDsMunicipioClientePagador(enderecoSelecionado.getDsMunicipioClientePagador());
				setCdSiglaUfPagador(enderecoSelecionado.getCdSiglaUfPagador().toUpperCase());
				setCepFormatado(enderecoSelecionado.getCepFormatado());				
				setCdCepPagador(enderecoSelecionado.getCdCepPagador());
			    setCdCepComplementoPagador(enderecoSelecionado.getCdCepComplementoPagador());
			}else{
				if(getRadioTipoPostagem().equals("1")){
					ConsultarEmailSaidaDTO emailSelecionado = getFiltroEnderecoEmailBean().getEmailSelecionado();							
					setDsEmailClientePagador(emailSelecionado.getCdEnderecoEletronico());
				}
			}
		}else{
			if(getRadioInfoAdicionais().equals("1")){
				if(getRadioAgenciaDepto().equals("0")){
					setDsAgenciaOperadora(getAgenciaOperadora());
				}else{
					if(getRadioAgenciaDepto().equals("1")){
						setDsDepartamentoUnidade(getDepartamentoFiltro());
					}
				}
			}
		}

		setPercentualDescTarifa(percentualDescTarifa == null ? new BigDecimal(0) : percentualDescTarifa);

		//Tarifa
		setTarifaAtualizada(PgitUtil.calcularValorTarifaAtualizada(getVlTarifa(), getPercentualDescTarifa()));

		return "CONFIRMARINCLUSAO";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		return preencheDadosSolEmiComprovanteFavorecido("DETALHAR");
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		return preencheDadosSolEmiComprovanteFavorecido("EXCLUIR");
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		limparIncluir();
		carregaCabecalho();
		listarTipoServico();
		listarCmbTipoFavorecido();
		setHabilitaCampoEndereco(true);
		setDisableArgumentosIncluir(false);
		return "INCLUIR";
	}
	
	/**
	 * Inf adicionais.
	 *
	 * @return the string
	 */
	public String infAdicionais(){
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());
		
		limparInfoAdicionais();
		setRadioInfoAdicionais("");
		carregaListaComprovantesSelecionados();
		consultarAgenciaOpeTarifaPadrao();
		consultarAgenciaOperadora();
		setPercentualDescTarifa(null);
		if("0.00".equals(getVlTarifa())){
			setDesabilitaDescontoTarifa(true);
		}
		return "INFADICIONAIS";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoLista(null);
		
		return "VOLTAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		limparSelecionados();
		
		return "VOLTAR";
	}
	
	/**
	 * Voltar inc inf.
	 *
	 * @return the string
	 */
	public String voltarIncInf(){
		return "VOLTAR";
	}	
		
	/**
	 * Pesquisar consulta.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsulta(ActionEvent evt){
		consultarSolEmiComprovanteFavorecido();
		return "";
	}
	
	/** Getters e Setters**/
	public List<SelectItem> getListaSituacaoSolicitacaoEstornoFiltro() {
		return listaSituacaoSolicitacaoEstornoFiltro;
	}
	
	/**
	 * Set: listaSituacaoSolicitacaoEstornoFiltro.
	 *
	 * @param listaSituacaoSolicitacaoEstornoFiltro the lista situacao solicitacao estorno filtro
	 */
	public void setListaSituacaoSolicitacaoEstornoFiltro(
			List<SelectItem> listaSituacaoSolicitacaoEstornoFiltro) {
		this.listaSituacaoSolicitacaoEstornoFiltro = listaSituacaoSolicitacaoEstornoFiltro;
	}
	
	/**
	 * Get: solEmissaoComprovantePagtoFavorecidoServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoFavorecidoServiceImpl
	 */
	public ISolEmissaoComprovantePagtoFavorecidoService getSolEmissaoComprovantePagtoFavorecidoServiceImpl() {
		return solEmissaoComprovantePagtoFavorecidoServiceImpl;
	}
	
	/**
	 * Set: solEmissaoComprovantePagtoFavorecidoServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoFavorecidoServiceImpl the sol emissao comprovante pagto favorecido service impl
	 */
	public void setSolEmissaoComprovantePagtoFavorecidoServiceImpl(
			ISolEmissaoComprovantePagtoFavorecidoService solEmissaoComprovantePagtoFavorecidoServiceImpl) {
		this.solEmissaoComprovantePagtoFavorecidoServiceImpl = solEmissaoComprovantePagtoFavorecidoServiceImpl;
	}

	/**
	 * Get: cboSituacaoSolicitacaoEstornoFiltro.
	 *
	 * @return cboSituacaoSolicitacaoEstornoFiltro
	 */
	public Integer getCboSituacaoSolicitacaoEstornoFiltro() {
		return cboSituacaoSolicitacaoEstornoFiltro;
	}

	/**
	 * Set: cboSituacaoSolicitacaoEstornoFiltro.
	 *
	 * @param cboSituacaoSolicitacaoEstornoFiltro the cbo situacao solicitacao estorno filtro
	 */
	public void setCboSituacaoSolicitacaoEstornoFiltro(
			Integer cboSituacaoSolicitacaoEstornoFiltro) {
		this.cboSituacaoSolicitacaoEstornoFiltro = cboSituacaoSolicitacaoEstornoFiltro;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaGridSolEmiComprovanteFavorecido.
	 *
	 * @return listaGridSolEmiComprovanteFavorecido
	 */
	public List<ConsultarSolEmiComprovanteFavorecidoSaidaDTO> getListaGridSolEmiComprovanteFavorecido() {
		return listaGridSolEmiComprovanteFavorecido;
	}

	/**
	 * Set: listaGridSolEmiComprovanteFavorecido.
	 *
	 * @param listaGridSolEmiComprovanteFavorecido the lista grid sol emi comprovante favorecido
	 */
	public void setListaGridSolEmiComprovanteFavorecido(
			List<ConsultarSolEmiComprovanteFavorecidoSaidaDTO> listaGridSolEmiComprovanteFavorecido) {
		this.listaGridSolEmiComprovanteFavorecido = listaGridSolEmiComprovanteFavorecido;
	}

	/**
	 * Get: listaRadios.
	 *
	 * @return listaRadios
	 */
	public List<SelectItem> getListaRadios() {
		return listaRadios;
	}

	/**
	 * Set: listaRadios.
	 *
	 * @param listaRadios the lista radios
	 */
	public void setListaRadios(List<SelectItem> listaRadios) {
		this.listaRadios = listaRadios;
	}

	/**
	 * Get: cdAgenciaBancariaContaCredito.
	 *
	 * @return cdAgenciaBancariaContaCredito
	 */
	public Integer getCdAgenciaBancariaContaCredito() {
		return cdAgenciaBancariaContaCredito;
	}

	/**
	 * Set: cdAgenciaBancariaContaCredito.
	 *
	 * @param cdAgenciaBancariaContaCredito the cd agencia bancaria conta credito
	 */
	public void setCdAgenciaBancariaContaCredito(
			Integer cdAgenciaBancariaContaCredito) {
		this.cdAgenciaBancariaContaCredito = cdAgenciaBancariaContaCredito;
	}

	/**
	 * Get: cdAgenciaBancariaContaDebito.
	 *
	 * @return cdAgenciaBancariaContaDebito
	 */
	public Integer getCdAgenciaBancariaContaDebito() {
		return cdAgenciaBancariaContaDebito;
	}

	/**
	 * Set: cdAgenciaBancariaContaDebito.
	 *
	 * @param cdAgenciaBancariaContaDebito the cd agencia bancaria conta debito
	 */
	public void setCdAgenciaBancariaContaDebito(Integer cdAgenciaBancariaContaDebito) {
		this.cdAgenciaBancariaContaDebito = cdAgenciaBancariaContaDebito;
	}

	/**
	 * Get: cdBancoContaCredito.
	 *
	 * @return cdBancoContaCredito
	 */
	public Integer getCdBancoContaCredito() {
		return cdBancoContaCredito;
	}

	/**
	 * Set: cdBancoContaCredito.
	 *
	 * @param cdBancoContaCredito the cd banco conta credito
	 */
	public void setCdBancoContaCredito(Integer cdBancoContaCredito) {
		this.cdBancoContaCredito = cdBancoContaCredito;
	}

	/**
	 * Get: cdBancoContaDebito.
	 *
	 * @return cdBancoContaDebito
	 */
	public Integer getCdBancoContaDebito() {
		return cdBancoContaDebito;
	}

	/**
	 * Set: cdBancoContaDebito.
	 *
	 * @param cdBancoContaDebito the cd banco conta debito
	 */
	public void setCdBancoContaDebito(Integer cdBancoContaDebito) {
		this.cdBancoContaDebito = cdBancoContaDebito;
	}

	/**
	 * Get: cdContaBancariaContaCredito.
	 *
	 * @return cdContaBancariaContaCredito
	 */
	public Long getCdContaBancariaContaCredito() {
		return cdContaBancariaContaCredito;
	}

	/**
	 * Set: cdContaBancariaContaCredito.
	 *
	 * @param cdContaBancariaContaCredito the cd conta bancaria conta credito
	 */
	public void setCdContaBancariaContaCredito(Long cdContaBancariaContaCredito) {
		this.cdContaBancariaContaCredito = cdContaBancariaContaCredito;
	}

	/**
	 * Get: cdContaBancariaContaDebito.
	 *
	 * @return cdContaBancariaContaDebito
	 */
	public Long getCdContaBancariaContaDebito() {
		return cdContaBancariaContaDebito;
	}

	/**
	 * Set: cdContaBancariaContaDebito.
	 *
	 * @param cdContaBancariaContaDebito the cd conta bancaria conta debito
	 */
	public void setCdContaBancariaContaDebito(Long cdContaBancariaContaDebito) {
		this.cdContaBancariaContaDebito = cdContaBancariaContaDebito;
	}

	/**
	 * Get: cdDigitoContaContaDebito.
	 *
	 * @return cdDigitoContaContaDebito
	 */
	public String getCdDigitoContaContaDebito() {
		return cdDigitoContaContaDebito;
	}

	/**
	 * Set: cdDigitoContaContaDebito.
	 *
	 * @param cdDigitoContaContaDebito the cd digito conta conta debito
	 */
	public void setCdDigitoContaContaDebito(String cdDigitoContaContaDebito) {
		this.cdDigitoContaContaDebito = cdDigitoContaContaDebito;
	}

	/**
	 * Get: cdDigitoContaCredito.
	 *
	 * @return cdDigitoContaCredito
	 */
	public String getCdDigitoContaCredito() {
		return cdDigitoContaCredito;
	}

	/**
	 * Set: cdDigitoContaCredito.
	 *
	 * @param cdDigitoContaCredito the cd digito conta credito
	 */
	public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
		this.cdDigitoContaCredito = cdDigitoContaCredito;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: dtFimSolicitacao.
	 *
	 * @return dtFimSolicitacao
	 */
	public Date getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}

	/**
	 * Set: dtFimSolicitacao.
	 *
	 * @param dtFimSolicitacao the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(Date dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}

	/**
	 * Get: dtInicioSolicitacao.
	 *
	 * @return dtInicioSolicitacao
	 */
	public Date getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}

	/**
	 * Set: dtInicioSolicitacao.
	 *
	 * @param dtInicioSolicitacao the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(Date dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}

	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}

	/**
	 * Get: cdCepComplementoPagador.
	 *
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador() {
		return cdCepComplementoPagador;
	}

	/**
	 * Set: cdCepComplementoPagador.
	 *
	 * @param cdCepComplementoPagador the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}

	/**
	 * Get: cdCepPagador.
	 *
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador() {
		return cdCepPagador;
	}

	/**
	 * Set: cdCepPagador.
	 *
	 * @param cdCepPagador the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador) {
		this.cdCepPagador = cdCepPagador;
	}

	/**
	 * Get: cdDestinoCorrespSolicitacao.
	 *
	 * @return cdDestinoCorrespSolicitacao
	 */
	public Integer getCdDestinoCorrespSolicitacao() {
		return cdDestinoCorrespSolicitacao;
	}

	/**
	 * Set: cdDestinoCorrespSolicitacao.
	 *
	 * @param cdDestinoCorrespSolicitacao the cd destino corresp solicitacao
	 */
	public void setCdDestinoCorrespSolicitacao(Integer cdDestinoCorrespSolicitacao) {
		this.cdDestinoCorrespSolicitacao = cdDestinoCorrespSolicitacao;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	

	/**
	 * Get: percentualDescTarifa.
	 *
	 * @return percentualDescTarifa
	 */
	public BigDecimal getPercentualDescTarifa() {
		return percentualDescTarifa;
	}

	/**
	 * Set: percentualDescTarifa.
	 *
	 * @param percentualDescTarifa the percentual desc tarifa
	 */
	public void setPercentualDescTarifa(BigDecimal percentualDescTarifa) {
		this.percentualDescTarifa = percentualDescTarifa;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdSiglaUfPagador.
	 *
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador() {
		return cdSiglaUfPagador;
	}

	/**
	 * Set: cdSiglaUfPagador.
	 *
	 * @param cdSiglaUfPagador the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}

	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Get: cdTipoInscricaoRecebedor.
	 *
	 * @return cdTipoInscricaoRecebedor
	 */
	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}

	/**
	 * Set: cdTipoInscricaoRecebedor.
	 *
	 * @param cdTipoInscricaoRecebedor the cd tipo inscricao recebedor
	 */
	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}

	/**
	 * Get: cdTipoPostagemSolicitacao.
	 *
	 * @return cdTipoPostagemSolicitacao
	 */
	public String getCdTipoPostagemSolicitacao() {
		return cdTipoPostagemSolicitacao;
	}

	/**
	 * Set: cdTipoPostagemSolicitacao.
	 *
	 * @param cdTipoPostagemSolicitacao the cd tipo postagem solicitacao
	 */
	public void setCdTipoPostagemSolicitacao(String cdTipoPostagemSolicitacao) {
		this.cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: comprovantes.
	 *
	 * @return comprovantes
	 */
	public List<DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO> getComprovantes() {
		return comprovantes;
	}

	/**
	 * Set: comprovantes.
	 *
	 * @param comprovantes the comprovantes
	 */
	public void setComprovantes(
			List<DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO> comprovantes) {
		this.comprovantes = comprovantes;
	}

	/**
	 * Get: dsBairroClientePagador.
	 *
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador() {
		return dsBairroClientePagador;
	}

	/**
	 * Set: dsBairroClientePagador.
	 *
	 * @param dsBairroClientePagador the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador) {
		this.dsBairroClientePagador = dsBairroClientePagador;
	}

	/**
	 * Get: dsComplementoLogradouroPagador.
	 *
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador() {
		return dsComplementoLogradouroPagador;
	}

	/**
	 * Set: dsComplementoLogradouroPagador.
	 *
	 * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(
			String dsComplementoLogradouroPagador) {
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}

	/**
	 * Get: dsEmailClientePagador.
	 *
	 * @return dsEmailClientePagador
	 */
	public String getDsEmailClientePagador() {
		return dsEmailClientePagador;
	}

	/**
	 * Set: dsEmailClientePagador.
	 *
	 * @param dsEmailClientePagador the ds email cliente pagador
	 */
	public void setDsEmailClientePagador(String dsEmailClientePagador) {
		this.dsEmailClientePagador = dsEmailClientePagador;
	}

	/**
	 * Get: dsLogradouroPagador.
	 *
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador() {
		return dsLogradouroPagador;
	}

	/**
	 * Set: dsLogradouroPagador.
	 *
	 * @param dsLogradouroPagador the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador) {
		this.dsLogradouroPagador = dsLogradouroPagador;
	}

	/**
	 * Get: dsMunicipioClientePagador.
	 *
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador() {
		return dsMunicipioClientePagador;
	}

	/**
	 * Set: dsMunicipioClientePagador.
	 *
	 * @param dsMunicipioClientePagador the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}

	/**
	 * Get: dsNumeroLogradouroPagador.
	 *
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador() {
		return dsNumeroLogradouroPagador;
	}

	/**
	 * Set: dsNumeroLogradouroPagador.
	 *
	 * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}

	/**
	 * Get: dtFimPeriodoMovimentacao.
	 *
	 * @return dtFimPeriodoMovimentacao
	 */
	public String getDtFimPeriodoMovimentacao() {
		return dtFimPeriodoMovimentacao;
	}

	/**
	 * Set: dtFimPeriodoMovimentacao.
	 *
	 * @param dtFimPeriodoMovimentacao the dt fim periodo movimentacao
	 */
	public void setDtFimPeriodoMovimentacao(String dtFimPeriodoMovimentacao) {
		this.dtFimPeriodoMovimentacao = dtFimPeriodoMovimentacao;
	}

	/**
	 * Get: dtInicioPeriodoMovimentacao.
	 *
	 * @return dtInicioPeriodoMovimentacao
	 */
	public String getDtInicioPeriodoMovimentacao() {
		return dtInicioPeriodoMovimentacao;
	}

	/**
	 * Set: dtInicioPeriodoMovimentacao.
	 *
	 * @param dtInicioPeriodoMovimentacao the dt inicio periodo movimentacao
	 */
	public void setDtInicioPeriodoMovimentacao(String dtInicioPeriodoMovimentacao) {
		this.dtInicioPeriodoMovimentacao = dtInicioPeriodoMovimentacao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Get: vlTarifa.
	 *
	 * @return vlTarifa
	 */
	public BigDecimal getVlTarifa() {
		return vlTarifa;
	}

	/**
	 * Set: vlTarifa.
	 *
	 * @param vlTarifa the vl tarifa
	 */
	public void setVlTarifa(BigDecimal vlTarifa) {
		this.vlTarifa = vlTarifa;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Get: listaIncluirComprovantes.
	 *
	 * @return listaIncluirComprovantes
	 */
	public List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> getListaIncluirComprovantes() {
		return listaIncluirComprovantes;
	}

	/**
	 * Set: listaIncluirComprovantes.
	 *
	 * @param listaIncluirComprovantes the lista incluir comprovantes
	 */
	public void setListaIncluirComprovantes(
			List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> listaIncluirComprovantes) {
		this.listaIncluirComprovantes = listaIncluirComprovantes;
	}
	
	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: dtFimPeriodoPagamento.
	 *
	 * @return dtFimPeriodoPagamento
	 */
	public Date getDtFimPeriodoPagamento() {
		return dtFimPeriodoPagamento;
	}

	/**
	 * Set: dtFimPeriodoPagamento.
	 *
	 * @param dtFimPeriodoPagamento the dt fim periodo pagamento
	 */
	public void setDtFimPeriodoPagamento(Date dtFimPeriodoPagamento) {
		this.dtFimPeriodoPagamento = dtFimPeriodoPagamento;
	}

	/**
	 * Get: dtInicioPeriodoPagamento.
	 *
	 * @return dtInicioPeriodoPagamento
	 */
	public Date getDtInicioPeriodoPagamento() {
		return dtInicioPeriodoPagamento;
	}

	/**
	 * Set: dtInicioPeriodoPagamento.
	 *
	 * @param dtInicioPeriodoPagamento the dt inicio periodo pagamento
	 */
	public void setDtInicioPeriodoPagamento(Date dtInicioPeriodoPagamento) {
		this.dtInicioPeriodoPagamento = dtInicioPeriodoPagamento;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: listaIncluirComprovantesSelecionados.
	 *
	 * @return listaIncluirComprovantesSelecionados
	 */
	public List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> getListaIncluirComprovantesSelecionados() {
		return listaIncluirComprovantesSelecionados;
	}

	/**
	 * Set: listaIncluirComprovantesSelecionados.
	 *
	 * @param listaIncluirComprovantesSelecionados the lista incluir comprovantes selecionados
	 */
	public void setListaIncluirComprovantesSelecionados(
			List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> listaIncluirComprovantesSelecionados) {
		this.listaIncluirComprovantesSelecionados = listaIncluirComprovantesSelecionados;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 *
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 *
	 * @param listaModalidadeFiltro the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 *
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 *
	 * @param listaTipoServicoFiltro the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: modalidadeFiltro.
	 *
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 *
	 * @param modalidadeFiltro the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: listaTipoInscricaoFiltro.
	 *
	 * @return listaTipoInscricaoFiltro
	 */
	public List<SelectItem> getListaTipoInscricaoFiltro() {
		return listaTipoInscricaoFiltro;
	}

	/**
	 * Set: listaTipoInscricaoFiltro.
	 *
	 * @param listaTipoInscricaoFiltro the lista tipo inscricao filtro
	 */
	public void setListaTipoInscricaoFiltro(
			List<SelectItem> listaTipoInscricaoFiltro) {
		this.listaTipoInscricaoFiltro = listaTipoInscricaoFiltro;
	}

	/**
	 * Get: tipoInscricaoFiltro.
	 *
	 * @return tipoInscricaoFiltro
	 */
	public Integer getTipoInscricaoFiltro() {
		return tipoInscricaoFiltro;
	}

	/**
	 * Set: tipoInscricaoFiltro.
	 *
	 * @param tipoInscricaoFiltro the tipo inscricao filtro
	 */
	public void setTipoInscricaoFiltro(Integer tipoInscricaoFiltro) {
		this.tipoInscricaoFiltro = tipoInscricaoFiltro;
	}

	/**
	 * Get: inscricaoFavorecido.
	 *
	 * @return inscricaoFavorecido
	 */
	public String getInscricaoFavorecido() {
		return inscricaoFavorecido;
	}

	/**
	 * Set: inscricaoFavorecido.
	 *
	 * @param inscricaoFavorecido the inscricao favorecido
	 */
	public void setInscricaoFavorecido(String inscricaoFavorecido) {
		this.inscricaoFavorecido = inscricaoFavorecido;
	}

	/**
	 * Get: radioIncluirFavorecido.
	 *
	 * @return radioIncluirFavorecido
	 */
	public String getRadioIncluirFavorecido() {
		return radioIncluirFavorecido;
	}

	/**
	 * Set: radioIncluirFavorecido.
	 *
	 * @param radioIncluirFavorecido the radio incluir favorecido
	 */
	public void setRadioIncluirFavorecido(String radioIncluirFavorecido) {
		this.radioIncluirFavorecido = radioIncluirFavorecido;
	}

	/**
	 * Get: radioIncluirSelecionado.
	 *
	 * @return radioIncluirSelecionado
	 */
	public String getRadioIncluirSelecionado() {
		return radioIncluirSelecionado;
	}

	/**
	 * Set: radioIncluirSelecionado.
	 *
	 * @param radioIncluirSelecionado the radio incluir selecionado
	 */
	public void setRadioIncluirSelecionado(String radioIncluirSelecionado) {
		this.radioIncluirSelecionado = radioIncluirSelecionado;
	}

	/**
	 * Get: valorPagamentoAte.
	 *
	 * @return valorPagamentoAte
	 */
	public BigDecimal getValorPagamentoAte() {
		return valorPagamentoAte;
	}

	/**
	 * Set: valorPagamentoAte.
	 *
	 * @param valorPagamentoAte the valor pagamento ate
	 */
	public void setValorPagamentoAte(BigDecimal valorPagamentoAte) {
		this.valorPagamentoAte = valorPagamentoAte;
	}

	/**
	 * Get: valorPagamentoDe.
	 *
	 * @return valorPagamentoDe
	 */
	public BigDecimal getValorPagamentoDe() {
		return valorPagamentoDe;
	}

	/**
	 * Set: valorPagamentoDe.
	 *
	 * @param valorPagamentoDe the valor pagamento de
	 */
	public void setValorPagamentoDe(BigDecimal valorPagamentoDe) {
		this.valorPagamentoDe = valorPagamentoDe;
	}

	/**
	 * Get: agenciaOperadora.
	 *
	 * @return agenciaOperadora
	 */
	public String getAgenciaOperadora() {
		return agenciaOperadora;
	}

	/**
	 * Set: agenciaOperadora.
	 *
	 * @param agenciaOperadora the agencia operadora
	 */
	public void setAgenciaOperadora(String agenciaOperadora) {
		this.agenciaOperadora = agenciaOperadora;
	}

	/**
	 * Get: departamentoFiltro.
	 *
	 * @return departamentoFiltro
	 */
	public String getDepartamentoFiltro() {
		return departamentoFiltro;
	}

	/**
	 * Set: departamentoFiltro.
	 *
	 * @param departamentoFiltro the departamento filtro
	 */
	public void setDepartamentoFiltro(String departamentoFiltro) {
		this.departamentoFiltro = departamentoFiltro;
	}

	/**
	 * Get: radioAgenciaDepto.
	 *
	 * @return radioAgenciaDepto
	 */
	public String getRadioAgenciaDepto() {
		return radioAgenciaDepto;
	}

	/**
	 * Set: radioAgenciaDepto.
	 *
	 * @param radioAgenciaDepto the radio agencia depto
	 */
	public void setRadioAgenciaDepto(String radioAgenciaDepto) {
		this.radioAgenciaDepto = radioAgenciaDepto;
	}

	/**
	 * Get: radioInfoAdicionais.
	 *
	 * @return radioInfoAdicionais
	 */
	public String getRadioInfoAdicionais() {
		return radioInfoAdicionais;
	}

	/**
	 * Set: radioInfoAdicionais.
	 *
	 * @param radioInfoAdicionais the radio info adicionais
	 */
	public void setRadioInfoAdicionais(String radioInfoAdicionais) {
		this.radioInfoAdicionais = radioInfoAdicionais;
	}

	/**
	 * Get: radioTipoPostagem.
	 *
	 * @return radioTipoPostagem
	 */
	public String getRadioTipoPostagem() {
		return radioTipoPostagem;
	}

	/**
	 * Set: radioTipoPostagem.
	 *
	 * @param radioTipoPostagem the radio tipo postagem
	 */
	public void setRadioTipoPostagem(String radioTipoPostagem) {
		this.radioTipoPostagem = radioTipoPostagem;
	}

	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}

	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}

	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}

	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	/**
	 * Get: codAgenciaOperadora.
	 *
	 * @return codAgenciaOperadora
	 */
	public Integer getCodAgenciaOperadora() {
		return codAgenciaOperadora;
	}

	/**
	 * Set: codAgenciaOperadora.
	 *
	 * @param codAgenciaOperadora the cod agencia operadora
	 */
	public void setCodAgenciaOperadora(Integer codAgenciaOperadora) {
		this.codAgenciaOperadora = codAgenciaOperadora;
	}

	/**
	 * Get: cdPessoaJuridicaDepartamento.
	 *
	 * @return cdPessoaJuridicaDepartamento
	 */
	public Long getCdPessoaJuridicaDepartamento() {
		return cdPessoaJuridicaDepartamento;
	}

	/**
	 * Set: cdPessoaJuridicaDepartamento.
	 *
	 * @param cdPessoaJuridicaDepartamento the cd pessoa juridica departamento
	 */
	public void setCdPessoaJuridicaDepartamento(Long cdPessoaJuridicaDepartamento) {
		this.cdPessoaJuridicaDepartamento = cdPessoaJuridicaDepartamento;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaDepartamento.
	 *
	 * @return nrSequenciaDepartamento
	 */
	public Integer getNrSequenciaDepartamento() {
		return nrSequenciaDepartamento;
	}

	/**
	 * Set: nrSequenciaDepartamento.
	 *
	 * @param nrSequenciaDepartamento the nr sequencia departamento
	 */
	public void setNrSequenciaDepartamento(Integer nrSequenciaDepartamento) {
		this.nrSequenciaDepartamento = nrSequenciaDepartamento;
	}

	/**
	 * Get: tarifaAtualizada.
	 *
	 * @return tarifaAtualizada
	 */
	public BigDecimal getTarifaAtualizada() {
		return tarifaAtualizada;
	}

	/**
	 * Set: tarifaAtualizada.
	 *
	 * @param tarifaAtualizada the tarifa atualizada
	 */
	public void setTarifaAtualizada(BigDecimal tarifaAtualizada) {
		this.tarifaAtualizada = tarifaAtualizada;
	}

	/**
	 * Get: cepFormatado.
	 *
	 * @return cepFormatado
	 */
	public String getCepFormatado() {
		return cepFormatado;
	}

	/**
	 * Set: cepFormatado.
	 *
	 * @param cepFormatado the cep formatado
	 */
	public void setCepFormatado(String cepFormatado) {
		this.cepFormatado = cepFormatado;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: dsAgenciaOperadora.
	 *
	 * @return dsAgenciaOperadora
	 */
	public String getDsAgenciaOperadora() {
		return dsAgenciaOperadora;
	}

	/**
	 * Set: dsAgenciaOperadora.
	 *
	 * @param dsAgenciaOperadora the ds agencia operadora
	 */
	public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
		this.dsAgenciaOperadora = dsAgenciaOperadora;
	}

	/**
	 * Get: dsDepartamentoUnidade.
	 *
	 * @return dsDepartamentoUnidade
	 */
	public String getDsDepartamentoUnidade() {
		return dsDepartamentoUnidade;
	}

	/**
	 * Set: dsDepartamentoUnidade.
	 *
	 * @param dsDepartamentoUnidade the ds departamento unidade
	 */
	public void setDsDepartamentoUnidade(String dsDepartamentoUnidade) {
		this.dsDepartamentoUnidade = dsDepartamentoUnidade;
	}
	


	/**
	 * Get: numeroPagamentoAte.
	 *
	 * @return numeroPagamentoAte
	 */
	public String getNumeroPagamentoAte() {
		return numeroPagamentoAte;
	}

	/**
	 * Set: numeroPagamentoAte.
	 *
	 * @param numeroPagamentoAte the numero pagamento ate
	 */
	public void setNumeroPagamentoAte(String numeroPagamentoAte) {
		this.numeroPagamentoAte = numeroPagamentoAte;
	}

	/**
	 * Get: numeroPagamentoDe.
	 *
	 * @return numeroPagamentoDe
	 */
	public String getNumeroPagamentoDe() {
		return numeroPagamentoDe;
	}

	/**
	 * Set: numeroPagamentoDe.
	 *
	 * @param numeroPagamentoDe the numero pagamento de
	 */
	public void setNumeroPagamentoDe(String numeroPagamentoDe) {
		this.numeroPagamentoDe = numeroPagamentoDe;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: listaModalidadeHash.
	 *
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 *
	 * @param listaModalidadeHash the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Is habilita campo endereco.
	 *
	 * @return true, if is habilita campo endereco
	 */
	public boolean isHabilitaCampoEndereco() {
		return habilitaCampoEndereco;
	}

	/**
	 * Set: habilitaCampoEndereco.
	 *
	 * @param habilitaCampoEndereco the habilita campo endereco
	 */
	public void setHabilitaCampoEndereco(boolean habilitaCampoEndereco) {
		this.habilitaCampoEndereco = habilitaCampoEndereco;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is disable argumentos incluir.
	 *
	 * @return true, if is disable argumentos incluir
	 */
	public boolean isDisableArgumentosIncluir() {
		return disableArgumentosIncluir;
	}

	/**
	 * Set: disableArgumentosIncluir.
	 *
	 * @param disableArgumentosIncluir the disable argumentos incluir
	 */
	public void setDisableArgumentosIncluir(boolean disableArgumentosIncluir) {
		this.disableArgumentosIncluir = disableArgumentosIncluir;
	}

	/**
	 * Get: cdSituacaoSolicitacao.
	 *
	 * @return cdSituacaoSolicitacao
	 */
	public Integer getCdSituacaoSolicitacao() {
		return cdSituacaoSolicitacao;
	}

	/**
	 * Set: cdSituacaoSolicitacao.
	 *
	 * @param cdSituacaoSolicitacao the cd situacao solicitacao
	 */
	public void setCdSituacaoSolicitacao(Integer cdSituacaoSolicitacao) {
		this.cdSituacaoSolicitacao = cdSituacaoSolicitacao;
	}

	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}

	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Is desabilita desconto tarifa.
	 *
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		return desabilitaDescontoTarifa;
	}

	/**
	 * Set: desabilitaDescontoTarifa.
	 *
	 * @param desabilitaDescontoTarifa the desabilita desconto tarifa
	 */
	public void setDesabilitaDescontoTarifa(boolean desabilitaDescontoTarifa) {
		this.desabilitaDescontoTarifa = desabilitaDescontoTarifa;
	}

	/**
	 * Get: filtroEnderecoEmailBean.
	 *
	 * @return filtroEnderecoEmailBean
	 */
	public FiltroEnderecoEmailBean getFiltroEnderecoEmailBean() {
		return filtroEnderecoEmailBean;
	}

	/**
	 * Set: filtroEnderecoEmailBean.
	 *
	 * @param filtroEnderecoEmailBean the filtro endereco email bean
	 */
	public void setFiltroEnderecoEmailBean(
			FiltroEnderecoEmailBean filtroEnderecoEmailBean) {
		this.filtroEnderecoEmailBean = filtroEnderecoEmailBean;
	}
}