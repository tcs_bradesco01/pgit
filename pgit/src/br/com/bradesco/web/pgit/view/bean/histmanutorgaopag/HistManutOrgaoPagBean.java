/*
 * Nome: br.com.bradesco.web.pgit.view.bean.histmanutorgaopag
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.histmanutorgaopag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.HistoricoOrgaoPagadorSaidaDTO;

/**
 * Nome: HistManutOrgaoPagBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class HistManutOrgaoPagBean {
	
	/** Atributo historicoOrgaoPagadorFiltro. */
	private HistoricoOrgaoPagadorEntradaDTO historicoOrgaoPagadorFiltro = new HistoricoOrgaoPagadorEntradaDTO();

	/** Atributo listaGridPesquisa. */
	private List<HistoricoOrgaoPagadorSaidaDTO> listaGridPesquisa = new ArrayList<HistoricoOrgaoPagadorSaidaDTO>();

	/** Atributo detalhe. */
	private HistoricoOrgaoPagadorSaidaDTO detalhe = new HistoricoOrgaoPagadorSaidaDTO();
	
	/** Atributo itemSelecionado. */
	private HistoricoOrgaoPagadorSaidaDTO itemSelecionado = null;
	
	/** Atributo orgaoPagadorService. */
	private IOrgaoPagadorService orgaoPagadorService;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;

	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar() {
		HistoricoOrgaoPagadorEntradaDTO entradaDTO = new HistoricoOrgaoPagadorEntradaDTO();
		entradaDTO.setCdOrgaoPagador(getItemSelecionado().getCdOrgaoPagador());
		entradaDTO.setHrInclusaoRegistro(getItemSelecionado().getHorario());

		setDetalhe(getOrgaoPagadorService().detalharHistoricoOrgaoPagador(entradaDTO).iterator().next());

		return "AVANCAR_DETALHAR";
	}
	
	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar(){
		setItemSelecionado(null);
		return "VOLTAR_CONSULTAR";
	}
	
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		setHistoricoOrgaoPagadorFiltro(new HistoricoOrgaoPagadorEntradaDTO());
		setItemSelecionado(null);
		setBtoAcionado(false);
		historicoOrgaoPagadorFiltro.setDtDe(new Date());
		historicoOrgaoPagadorFiltro.setDtAte(new Date());
		setListaGridPesquisa(null);
		return "PGIC0142";		
	}

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		try {
			setListaGridPesquisa(getOrgaoPagadorService().listarHistoricoOrgaoPagador(getHistoricoOrgaoPagadorFiltro()));
			setBtoAcionado(true);
		} catch (PdcAdapterFunctionalException e) {
			setListaGridPesquisa(new ArrayList<HistoricoOrgaoPagadorSaidaDTO>());
			throw e;
		}

		return "ok";		
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		carregaLista();

		return "ok";
	}

	/**
	 * Get: historicoOrgaoPagadorFiltro.
	 *
	 * @return historicoOrgaoPagadorFiltro
	 */
	public HistoricoOrgaoPagadorEntradaDTO getHistoricoOrgaoPagadorFiltro() {
		return historicoOrgaoPagadorFiltro;
	}
	
	/**
	 * Set: historicoOrgaoPagadorFiltro.
	 *
	 * @param historicoOrgaoPagadorFiltro the historico orgao pagador filtro
	 */
	public void setHistoricoOrgaoPagadorFiltro(
			HistoricoOrgaoPagadorEntradaDTO historicoOrgaoPagadorFiltro) {
		this.historicoOrgaoPagadorFiltro = historicoOrgaoPagadorFiltro;
	}

	/**
	 * Get: orgaoPagadorService.
	 *
	 * @return orgaoPagadorService
	 */
	public IOrgaoPagadorService getOrgaoPagadorService() {
		return orgaoPagadorService;
	}
	
	/**
	 * Set: orgaoPagadorService.
	 *
	 * @param orgaoPagadorService the orgao pagador service
	 */
	public void setOrgaoPagadorService(IOrgaoPagadorService orgaoPagadorService) {
		this.orgaoPagadorService = orgaoPagadorService;
	}
	
	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<HistoricoOrgaoPagadorSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}
	
	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<HistoricoOrgaoPagadorSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}
	
	/**
	 * Get: detalhe.
	 *
	 * @return detalhe
	 */
	public HistoricoOrgaoPagadorSaidaDTO getDetalhe() {
		return detalhe;
	}
	
	/**
	 * Set: detalhe.
	 *
	 * @param detalhe the detalhe
	 */
	public void setDetalhe(HistoricoOrgaoPagadorSaidaDTO detalhe) {
		this.detalhe = detalhe;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public HistoricoOrgaoPagadorSaidaDTO getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(HistoricoOrgaoPagadorSaidaDTO itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}
}
