/*
 * Nome: br.com.bradesco.web.pgit.view.bean.filtropesquisa
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.filtropesquisa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: IdentificacaoClienteBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IdentificacaoClienteBean {
	
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo PAGINA_EXTENSAO. */
	private static final String PAGINA_EXTENSAO = ".jsf";
	
	/** Atributo entradaConsultarListaClientePessoas. */
	private ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoas;
	
	/** Atributo entradaConsultarListaClientePessoasManutencao. */
	private ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoasManutencao;
	
	/** Atributo saidaConsultarListaClientePessoas. */
	private ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoas;
	
	/** Atributo saidaConsultarListaClientePessoasManutencao. */
	private ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoasManutencao;
	
	/** Atributo listaConsultarListaClientePessoas. */
	private List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoas;

	/** Atributo entradaConsultarListaContratosPessoas. */
	private ConsultarListaContratosPessoasEntradaDTO entradaConsultarListaContratosPessoas = new ConsultarListaContratosPessoasEntradaDTO();
	
	/** Atributo entradaConsultarListaContratosPessoasManutencao. */
	private ConsultarListaContratosPessoasEntradaDTO entradaConsultarListaContratosPessoasManutencao;
	
	/** Atributo saidaConsultarListaContratosPessoas. */
	private ConsultarListaContratosPessoasSaidaDTO saidaConsultarListaContratosPessoas = new ConsultarListaContratosPessoasSaidaDTO();
	
	/** Atributo saidaConsultarListaContratosPessoasManutencao. */
	private ConsultarListaContratosPessoasSaidaDTO saidaConsultarListaContratosPessoasManutencao;
	
	/** Atributo listaConsultarListaContratosPessoas. */
	private List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas;

	/** Atributo habilitaFiltroDeCliente. */
	private boolean habilitaFiltroDeCliente;
	
	/** Atributo saidaEmpresaGestora. */
	private EmpresaGestoraSaidaDTO saidaEmpresaGestora;
	
	/** Atributo listaEmpresaGestora. */
	private List<EmpresaGestoraSaidaDTO> listaEmpresaGestora;

	/** Atributo saidaTipoContrato. */
	private TipoContratoSaidaDTO saidaTipoContrato;
	
	/** Atributo listTipoContrato. */
	private List<TipoContratoSaidaDTO> listTipoContrato;

	/** Atributo filtroIdentificaoService. */
	private IFiltroIdentificaoService filtroIdentificaoService;
	
	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo preencherListaEmpresaGestora. */
	private List<SelectItem> preencherListaEmpresaGestora;
	
	/** Atributo preencherListaEmpresaGestoraHash. */
	private Map<Long,EmpresaGestoraSaidaDTO> preencherListaEmpresaGestoraHash =  new HashMap<Long,EmpresaGestoraSaidaDTO>();
	
	/** Atributo preencherTipoContrato. */
	private List<SelectItem> preencherTipoContrato;

	/** Atributo itemFiltroSelecionado. */
	private String itemFiltroSelecionado;
	
	/** Atributo itemFiltroSelecionadoManutencao. */
	private String itemFiltroSelecionadoManutencao;
	
	/** Atributo itemClienteSelecionado. */
	private Integer itemClienteSelecionado= -1;
	
	/** Atributo itemContratoSelecionado. */
	private Integer itemContratoSelecionado = -1;
	
	/** Atributo itemContratoSelec. */
	private Integer itemContratoSelec;
	
	/** Atributo habilitaAvancar. */
	private Boolean habilitaAvancar;
	
	/** Atributo habilitaLimpa. */
	private Boolean habilitaLimpa = false;
	
	/** Atributo bloqueaAgConta. */
	private Boolean bloqueaAgConta = true;
	
	/** Atributo limpaCamp. */
	private Boolean limpaCamp = true;

	/** Atributo itemEmpresaSelecionada. */
	private Long itemEmpresaSelecionada;
	
	/** Atributo itemTipoContratoSelecionada. */
	private Integer itemTipoContratoSelecionada;

	/** Atributo paginaRetorno. */
	private String paginaRetorno;
	
	/** Atributo paginaRetornoManutencao. */
	private String paginaRetornoManutencao;
	
	/** Atributo paginaCliente. */
	private String paginaCliente;
	
	/** Atributo paginaClienteManutencao. */
	private String paginaClienteManutencao;	
	
	/** Atributo paginaContrato. */
	private String paginaContrato;
	
	/** Atributo paginaContratoManutencao. */
	private String paginaContratoManutencao;

	/** Atributo contratoSelecionado. */
	private boolean contratoSelecionado;
	
	/** Atributo habilitaFiltroDeContrato. */
	private boolean habilitaFiltroDeContrato;
	
	/** Atributo habilitaFiltroDeContratoManutencao. */
	private boolean habilitaFiltroDeContratoManutencao;
	
	/** Atributo habilitaFiltroDeCadContaSalarioDet. */
	private boolean habilitaFiltroDeCadContaSalarioDet;
	
	/** Atributo desabilataFiltro. */
	private boolean desabilataFiltro;
	
	/** Atributo bloqueaRadio. */
	private boolean bloqueaRadio;
	
	/** Atributo bloqueaTipoContrato. */
	private boolean bloqueaTipoContrato;	
	
	/** Atributo bloqueaFiltro. */
	private boolean bloqueaFiltro;
	
	/** Atributo desabilatalimpar. */
	private boolean desabilatalimpar = true;
	
	/** Atributo dsEmpresaGestoraContrato. */
	private String dsEmpresaGestoraContrato;
	
	/** Atributo habilitaCamposAposConsContrato. */
	private boolean habilitaCamposAposConsContrato = false;
	
	/**
	 * Inicializar variaveis identificacao.
	 */
	public void inicializarVariaveisIdentificacao(){
		this.setItemFiltroSelecionado("0");
		this.setDesabilatalimpar(true);
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());
		this.setHabilitaFiltroDeContrato(true);
		this.setBloqueaTipoContrato(true);
		this.setDesabilataFiltro(true);
		this.setContratoSelecionado(false);
		this.setBloqueaRadio(false);
		this.loadEmpresaGestora();
		this.loadTipoContrato();
		this.setItemClienteSelecionado(-1);
		this.setItemContratoSelecionado(-1);
		this.habilitaCamposAposConsContrato = false;
		this.limpaCamp = true;
	}
	
	/**
	 * Is desabilatalimpar.
	 *
	 * @return true, if is desabilatalimpar
	 */
	public boolean isDesabilatalimpar() {
		return desabilatalimpar;
	}

	/**
	 * Set: desabilatalimpar.
	 *
	 * @param desabilatalimpar the desabilatalimpar
	 */
	public void setDesabilatalimpar(boolean desabilatalimpar) {
		this.desabilatalimpar = desabilatalimpar;
	}

	/**
	 * Identificacao cliente bean.
	 */
	public IdentificacaoClienteBean() {
	}

	/**
	 * Identificacao cliente bean.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public IdentificacaoClienteBean(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Identificacao cliente bean.
	 *
	 * @param paginaRetorno the pagina retorno
	 * @param paginaCliente the pagina cliente
	 * @param paginaContrato the pagina contrato
	 */
	public IdentificacaoClienteBean(String paginaRetorno, String paginaCliente, String paginaContrato) {
		this.paginaRetorno = paginaRetorno;
		this.paginaCliente = paginaCliente;
		this.paginaContrato = paginaContrato;
	}

	/**
	 * Pesquisar clientes.
	 *
	 * @param entrada the entrada
	 */
	private void pesquisarClientes(ConsultarListaClientePessoasEntradaDTO entrada) {
		this.setListaConsultarListaClientePessoas(this.getFiltroIdentificaoService().pesquisarClientes(entrada));
	}

	/**
	 * Pesquisar contratos de cliente.
	 *
	 * @param entrada the entrada
	 */
	private void pesquisarContratosDeCliente(ConsultarListaContratosPessoasEntradaDTO entrada) {
		
		this.getEntradaConsultarListaContratosPessoas().setCdClub(this.getSaidaConsultarListaClientePessoas() == null ? 0 : this.getSaidaConsultarListaClientePessoas().getCdClub() == null ? 0 : this.getSaidaConsultarListaClientePessoas().getCdClub());
		entrada.setCdClub(this.getSaidaConsultarListaClientePessoas() == null ? 0 : this.getSaidaConsultarListaClientePessoas().getCdClub() == null ? 0 : this.getSaidaConsultarListaClientePessoas().getCdClub());
		entrada.setCdControleCpfPssoa(this.getSaidaConsultarListaClientePessoas() == null ? 0 : PgitUtil.verificaIntegerNulo(this.getSaidaConsultarListaClientePessoas().getCdControleCnpj()));
		entrada.setCdCpfCnpjPssoa(this.getSaidaConsultarListaClientePessoas() == null ? 0 : PgitUtil.verificaLongNulo(this.getSaidaConsultarListaClientePessoas().getCdCpfCnpj()));
		entrada.setCdFilialCnpjPssoa(this.getSaidaConsultarListaClientePessoas() == null ? 0 : PgitUtil.verificaIntegerNulo(this.getSaidaConsultarListaClientePessoas().getCdFilialCnpj()));
	
		this.setListaConsultarListaContratosPessoas(this.getFiltroIdentificaoService().pesquisarContratos(entrada));
	}

	/**
	 * Consultar clientes.
	 *
	 * @return the string
	 */
	public String consultarClientes() {
		this.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.setHabilitaFiltroDeContrato(true);
		this.setBloqueaTipoContrato(false);
		this.setLimpaCamp(false);
		this.setBloqueaAgConta(true);
		this.setHabilitaFiltroDeCliente(true);

		try {
			this.setItemClienteSelecionado(null);
			this.pesquisarClientes(this.getEntradaConsultarListaClientePessoas());

			if (this.getListaConsultarListaClientePessoas().size() != 0) {
				if (this.getListaConsultarListaClientePessoas().size() == 1) {
					this.setSaidaConsultarListaClientePessoas(this.getListaConsultarListaClientePessoas().get(0));
					this.setHabilitaFiltroDeContrato(false);
					this.setBloqueaTipoContrato(false);
					this.setBloqueaRadio(true);
					//this.setItemFiltroSelecionado("");
					return this.getPaginaRetorno();
				} else {
					this.setBloqueaRadio(false);
					this.setHabilitaFiltroDeCliente(false);
					return this.getPaginaCliente();
				}
			} else {
				this.setHabilitaFiltroDeCliente(false);
				return this.getPaginaRetorno();
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "/" + this.getPaginaRetorno() + PAGINA_EXTENSAO , BradescoViewExceptionActionType.ACTION, false);
			
			setHabilitaFiltroDeContrato(true);
			this.setHabilitaFiltroDeCliente(false);
			this.setBloqueaTipoContrato(true);
			return null;
		}
	}

	/**
	 * Consultar clientes manutencao.
	 *
	 * @return the string
	 */
	public String consultarClientesManutencao() {
		setSaidaConsultarListaContratosPessoasManutencao(new ConsultarListaContratosPessoasSaidaDTO());
		setSaidaConsultarListaClientePessoasManutencao(new ConsultarListaClientePessoasSaidaDTO());
		setEntradaConsultarListaContratosPessoasManutencao(new ConsultarListaContratosPessoasEntradaDTO());
		setHabilitaFiltroDeContratoManutencao(true);
		try {
			this.pesquisarClientes(this.getEntradaConsultarListaClientePessoasManutencao());
			this.setItemClienteSelecionado(null);
			if (this.getListaConsultarListaClientePessoas().size() != 0) {
				if (this.getListaConsultarListaClientePessoas().size() == 1) {
					this.setSaidaConsultarListaClientePessoasManutencao(this.getListaConsultarListaClientePessoas().get(0));
					this.setHabilitaFiltroDeContratoManutencao(false);
					this.setItemFiltroSelecionado(null);
					return this.getPaginaRetornoManutencao();
				} else {
					return this.getPaginaClienteManutencao();
				}
			} else {
				return this.getPaginaRetornoManutencao();
			}

		} catch (PdcAdapterFunctionalException p) {
			setHabilitaFiltroDeContratoManutencao(true);
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "/" + this.getPaginaRetornoManutencao() + PAGINA_EXTENSAO, BradescoViewExceptionActionType.PATH, false);
			return null;
		}
	}

	/**
	 * Consultar contratos.
	 *
	 * @return the string
	 */
	public String consultarContratos() {

		try {
			this.setItemContratoSelecionado(null);
			this.pesquisarContratosDeCliente(this.getEntradaConsultarListaContratosPessoas());
			if (this.getListaConsultarListaContratosPessoas().size() != 0) {
				setHabilitaFiltroDeCadContaSalarioDet(false);
				if (this.getListaConsultarListaContratosPessoas().size() == 1) {
					this.setSaidaConsultarListaContratosPessoas(getListaConsultarListaContratosPessoas().get(0));
					
					if(preencherListaEmpresaGestoraHash.size() == 0){
					    setDsEmpresaGestoraContrato("");
					}else{
					    setDsEmpresaGestoraContrato(preencherListaEmpresaGestoraHash.get(getEntradaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato()).getDsCnpj());
					}
					
					this.setDesabilataFiltro(false);
					setContratoSelecionado(true);
					habilitaCamposAposConsContrato = true;
					limpaCamp = false;
					
					return this.getPaginaRetorno();
				} else {
					return this.getPaginaContrato();
				}
			} else {
				BradescoFacesUtils.addInfoModalMessage("(PGIT0301) CLIENTE NAO PARTICIPA DESTE CONTRATO", false);
				return this.getPaginaRetorno();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "/" + this.getPaginaRetorno() + PAGINA_EXTENSAO, BradescoViewExceptionActionType.PATH, false);
			return null;
		}
	}

	/** Atributo habilitaLimparTabela. */
	private boolean habilitaLimparTabela = true; 
	
	/**
	 * Desabilita consulta argumentos pesquisa.
	 */
	public void desabilitaConsultaArgumentosPesquisa(){
		desabilataFiltro = true;
		limpaCamp = true;
		habilitaLimparTabela = false;
	}

	/**
	 * Habilita consulta argumentos pesquisa.
	 */
	public void habilitaConsultaArgumentosPesquisa(){
		desabilataFiltro = false;
		limpaCamp = false;
		habilitaLimparTabela = true;
	}
	
	/**
	 * Consultar contratos manutencao.
	 *
	 * @return the string
	 */
	public String consultarContratosManutencao() {

		try {
			this.setItemContratoSelecionado(null);
			setHabilitaAvancar(false);
			this.pesquisarContratosDeCliente(this.getEntradaConsultarListaContratosPessoasManutencao());
			if (this.getListaConsultarListaContratosPessoas().size() != 0) {
				if (this.getListaConsultarListaContratosPessoas().size() == 1) {
					this.setSaidaConsultarListaContratosPessoasManutencao(getListaConsultarListaContratosPessoas().get(0));
					setHabilitaAvancar(true);
					return this.getPaginaRetornoManutencao();
				} else {
					return this.getPaginaContratoManutencao();
				}
			} else {
				return this.getPaginaRetornoManutencao();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "/" + this.getPaginaRetornoManutencao() + PAGINA_EXTENSAO, BradescoViewExceptionActionType.PATH, false);
			return null;
		}
	}

	/**
	 * Selecionar cliente.
	 *
	 * @return the string
	 */
	public String selecionarCliente() {
		this.setSaidaConsultarListaClientePessoas(getListaConsultarListaClientePessoas().get(this.getItemClienteSelecionado()));
		this.setHabilitaFiltroDeContrato(false);
		this.setBloqueaTipoContrato(false);
		this.setBloqueaRadio(true);
		this.setHabilitaFiltroDeCliente(true);
		return this.getPaginaRetorno();
	}

	/**
	 * Selecionar cliente manutencao.
	 *
	 * @return the string
	 */
	public String selecionarClienteManutencao() {
		this.setSaidaConsultarListaClientePessoasManutencao(getListaConsultarListaClientePessoas().get(this.getItemClienteSelecionado()));
		this.setHabilitaFiltroDeContratoManutencao(false);
		return this.getPaginaRetornoManutencao();
	}

	/**
	 * Selecionar contrato.
	 *
	 * @return the string
	 */
	public String selecionarContrato() {
		this.setSaidaConsultarListaContratosPessoas(getListaConsultarListaContratosPessoas().get(this.getItemContratoSelecionado()));
		
		if(preencherListaEmpresaGestoraHash.size() == 0){
		    setDsEmpresaGestoraContrato("");
		}else{
		    setDsEmpresaGestoraContrato(preencherListaEmpresaGestoraHash.get(getEntradaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato()).getDsCnpj());
		}
		this.setDesabilataFiltro(false);
		setContratoSelecionado(true);
		this.setBloqueaFiltro(false);
		this.setBloqueaTipoContrato(true);
		this.setBloqueaAgConta(false);
		return this.getPaginaRetorno();
	}

	/**
	 * Selecionar contrato manutencao.
	 *
	 * @return the string
	 */
	public String selecionarContratoManutencao() {
		this.setSaidaConsultarListaContratosPessoasManutencao(getListaConsultarListaContratosPessoas().get(this.getItemContratoSelecionado()));
		this.setDesabilataFiltro(false);
		setContratoSelecionado(true);		
		setHabilitaAvancar(true);
		return this.getPaginaRetornoManutencao();
	}

	/**
	 * Limpar campos de selecao.
	 *
	 * @return the string
	 */
	public String limparCamposDeSelecao() {
		this.getEntradaConsultarListaClientePessoas().setNumeroOcorrencias(null);
		this.getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
		this.getEntradaConsultarListaClientePessoas().setCdCpf(null);
		this.getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
		this.getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);
		this.getEntradaConsultarListaClientePessoas().setCdControleCpf(null);
		this.getEntradaConsultarListaClientePessoas().setCdBanco(null);
		this.getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
		this.getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		this.getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial(null);
		this.getEntradaConsultarListaClientePessoas().setCdTipoConta(null);
		
		return this.getPaginaRetorno();
	}
	
	/**
	 * Limpar label.
	 *
	 * @return the string
	 */
	public String limparLabel() {
	    	if (getSaidaConsultarListaClientePessoas() != null){
        		this.getSaidaConsultarListaClientePessoas().setCnpjOuCpfFormatado(null);
        		this.getSaidaConsultarListaClientePessoas().setDsNomeRazao(null);
        		this.getSaidaConsultarListaContratosPessoas().setDsPessoaJuridicaContrato(null);
        		this.getSaidaConsultarListaContratosPessoas().setDsTipoContratoNegocio(null);
        		this.getSaidaConsultarListaContratosPessoas().setNrSeqContratoNegocio(null);
        		this.getSaidaConsultarListaContratosPessoas().setDsTipoParticipacaoPessoa(null);
	    	}
	    	
		return this.getPaginaRetorno();
	}

	/**
	 * Load empresa gestora.
	 */
	public void loadEmpresaGestora() {
		try{
			List<EmpresaGestoraSaidaDTO> list = comboService.listarEmpresasGestoras();
			preencherListaEmpresaGestoraHash = new HashMap<Long, EmpresaGestoraSaidaDTO>();
			for (EmpresaGestoraSaidaDTO saida : list){
				preencherListaEmpresaGestora.add(new SelectItem(saida.getCdPessoaJuridicaContrato(), saida.getDsCnpj()));
				preencherListaEmpresaGestoraHash.put(saida.getCdPessoaJuridicaContrato(), saida);
				
			}
		}catch(PdcAdapterFunctionalException e){
			preencherListaEmpresaGestora = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Get: preencherEmpresaGestora.
	 *
	 * @return preencherEmpresaGestora
	 */
	public List<SelectItem> getPreencherEmpresaGestora() {
		return preencherListaEmpresaGestora;
	}

	/**
	 * Load tipo contrato.
	 */
	public void loadTipoContrato() {
		try{
			List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();
	
			for (TipoContratoSaidaDTO saida : list){
				preencherTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
			}
		}catch(PdcAdapterFunctionalException e){
			preencherTipoContrato = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Submit cotratos.
	 *
	 * @param event the event
	 */
	public void submitCotratos(ActionEvent event) {
		this.pesquisarContratosDeCliente(this.getEntradaConsultarListaContratosPessoas());
	}

	/**
	 * Submit cliente.
	 *
	 * @param event the event
	 */
	public void submitCliente(ActionEvent event) {
		this.pesquisarClientes(this.getEntradaConsultarListaClientePessoas());
	}

	/**
	 * Get: selectItemContrato.
	 *
	 * @return selectItemContrato
	 */
	public List<SelectItem> getSelectItemContrato() {
		if (this.getListaConsultarListaContratosPessoas() == null || this.getListaConsultarListaContratosPessoas().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaConsultarListaContratosPessoas().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Get: selectItemCliente.
	 *
	 * @return selectItemCliente
	 */
	public List<SelectItem> getSelectItemCliente() {
		if (this.getListaConsultarListaClientePessoas() == null || this.getListaConsultarListaClientePessoas().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaConsultarListaClientePessoas().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Limpar campos do filtro.
	 */
	public void limparCamposDoFiltro() {

		if (this.getItemFiltroSelecionado().equals("0")) {
			this.getEntradaConsultarListaClientePessoas().setCdCpf(null);
			this.getEntradaConsultarListaClientePessoas().setCdControleCpf(null);

			this.getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial("");

			this.getEntradaConsultarListaClientePessoas().setCdBanco(null);
			this.getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
			this.getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		}

		if (this.getItemFiltroSelecionado().equals("1")) {
			this.getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
			this.getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
			this.getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);

			this.getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial("");

			this.getEntradaConsultarListaClientePessoas().setCdBanco(null);
			this.getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
			this.getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		}

		if (this.getItemFiltroSelecionado().equals("2")) {
			this.getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
			this.getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
			this.getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);

			this.getEntradaConsultarListaClientePessoas().setCdCpf(null);
			this.getEntradaConsultarListaClientePessoas().setCdControleCpf(null);

			this.getEntradaConsultarListaClientePessoas().setCdBanco(null);
			this.getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
			this.getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		}
		
		if (this.getItemFiltroSelecionado().equals("3")) {
			this.getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
			this.getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
			this.getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);

			this.getEntradaConsultarListaClientePessoas().setCdCpf(null);
			this.getEntradaConsultarListaClientePessoas().setCdControleCpf(null);

			this.getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial("");
			
			this.getEntradaConsultarListaClientePessoas().setCdBanco(237);
		}
	}
	
	/**
	 * Limpar cliente.
	 */
	public void limparCliente(){
		setItemFiltroSelecionado("");
		setBloqueaRadio(false);
		setHabilitaFiltroDeCliente(false);
		getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
		getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
		getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);
		getEntradaConsultarListaClientePessoas().setCdCpf(null);
		getEntradaConsultarListaClientePessoas().setCdControleCpf(null);
		getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial("");
		getEntradaConsultarListaClientePessoas().setCdBanco(null);
		getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
		getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		getSaidaConsultarListaClientePessoas().setCnpjOuCpfFormatado("");
		getSaidaConsultarListaClientePessoas().setDsNomeRazao("");
	}
	
	/**
	 * Limpar contrato.
	 */
	public void limparContrato(){
		setDesabilataFiltro(true);
		setContratoSelecionado(false);		
		getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(2269651L);
		getEntradaConsultarListaContratosPessoas().setCdTipoContratoNegocio(0);
		getEntradaConsultarListaContratosPessoas().setNrSeqContratoNegocio(null);
		getSaidaConsultarListaContratosPessoas().setDsPessoaJuridicaContrato("");
		getSaidaConsultarListaContratosPessoas().setDsTipoContratoNegocio("");
		getSaidaConsultarListaContratosPessoas().setNrSeqContratoNegocio(null);
		getSaidaConsultarListaContratosPessoas().setDsContrato("");
		getSaidaConsultarListaContratosPessoas().setDsSituacaoContratoNegocio("");
	}

	/**
	 * Get: preencherTipoContrato.
	 *
	 * @return preencherTipoContrato
	 */
	public List<SelectItem> getPreencherTipoContrato() {
		return preencherTipoContrato;
	}

	/**
	 * Get: entradaConsultarListaClientePessoas.
	 *
	 * @return entradaConsultarListaClientePessoas
	 */
	public ConsultarListaClientePessoasEntradaDTO getEntradaConsultarListaClientePessoas() {
		return entradaConsultarListaClientePessoas;
	}

	/**
	 * Set: entradaConsultarListaClientePessoas.
	 *
	 * @param entradaConsultarListaClientePessoas the entrada consultar lista cliente pessoas
	 */
	public void setEntradaConsultarListaClientePessoas(ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoas) {
		this.entradaConsultarListaClientePessoas = entradaConsultarListaClientePessoas;
	}

	/**
	 * Get: entradaConsultarListaContratosPessoas.
	 *
	 * @return entradaConsultarListaContratosPessoas
	 */
	public ConsultarListaContratosPessoasEntradaDTO getEntradaConsultarListaContratosPessoas() {
		return entradaConsultarListaContratosPessoas;
	}

	/**
	 * Set: entradaConsultarListaContratosPessoas.
	 *
	 * @param entradaConsultarListaContratosPessoas the entrada consultar lista contratos pessoas
	 */
	public void setEntradaConsultarListaContratosPessoas(ConsultarListaContratosPessoasEntradaDTO entradaConsultarListaContratosPessoas) {
		this.entradaConsultarListaContratosPessoas = entradaConsultarListaContratosPessoas;
	}

	/**
	 * Get: filtroIdentificaoService.
	 *
	 * @return filtroIdentificaoService
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoService() {
		return filtroIdentificaoService;
	}

	/**
	 * Set: filtroIdentificaoService.
	 *
	 * @param filtroIdentificaoService the filtro identificao service
	 */
	public void setFiltroIdentificaoService(IFiltroIdentificaoService filtroIdentificaoService) {
		this.filtroIdentificaoService = filtroIdentificaoService;
	}

	/**
	 * Is habilita filtro de contrato.
	 *
	 * @return true, if is habilita filtro de contrato
	 */
	public boolean isHabilitaFiltroDeContrato() {
		return habilitaFiltroDeContrato;
	}

	/**
	 * Set: habilitaFiltroDeContrato.
	 *
	 * @param habilitaFiltroDeContrato the habilita filtro de contrato
	 */
	public void setHabilitaFiltroDeContrato(boolean habilitaFiltroDeContrato) {
		this.habilitaFiltroDeContrato = habilitaFiltroDeContrato;
	}

	/**
	 * Get: itemClienteSelecionado.
	 *
	 * @return itemClienteSelecionado
	 */
	public Integer getItemClienteSelecionado() {
		return itemClienteSelecionado;
	}

	/**
	 * Set: itemClienteSelecionado.
	 *
	 * @param itemClienteSelecionado the item cliente selecionado
	 */
	public void setItemClienteSelecionado(Integer itemClienteSelecionado) {
		this.itemClienteSelecionado = itemClienteSelecionado;
	}

	/**
	 * Get: itemContratoSelecionado.
	 *
	 * @return itemContratoSelecionado
	 */
	public Integer getItemContratoSelecionado() {
		return itemContratoSelecionado;
	}

	/**
	 * Set: itemContratoSelecionado.
	 *
	 * @param itemContratoSelecionado the item contrato selecionado
	 */
	public void setItemContratoSelecionado(Integer itemContratoSelecionado) {
		this.itemContratoSelecionado = itemContratoSelecionado;
	}

	/**
	 * Get: itemFiltroSelecionado.
	 *
	 * @return itemFiltroSelecionado
	 */
	public String getItemFiltroSelecionado() {
		return itemFiltroSelecionado;
	}

	/**
	 * Set: itemFiltroSelecionado.
	 *
	 * @param itemFiltroSelecionado the item filtro selecionado
	 */
	public void setItemFiltroSelecionado(String itemFiltroSelecionado) {
		this.itemFiltroSelecionado = itemFiltroSelecionado;
	}

	/**
	 * Get: listaConsultarListaClientePessoas.
	 *
	 * @return listaConsultarListaClientePessoas
	 */
	public List<ConsultarListaClientePessoasSaidaDTO> getListaConsultarListaClientePessoas() {
		return listaConsultarListaClientePessoas;
	}

	/**
	 * Set: listaConsultarListaClientePessoas.
	 *
	 * @param listaConsultarListaClientePessoas the lista consultar lista cliente pessoas
	 */
	public void setListaConsultarListaClientePessoas(List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoas) {
		this.listaConsultarListaClientePessoas = listaConsultarListaClientePessoas;
	}

	/**
	 * Get: listaConsultarListaContratosPessoas.
	 *
	 * @return listaConsultarListaContratosPessoas
	 */
	public List<ConsultarListaContratosPessoasSaidaDTO> getListaConsultarListaContratosPessoas() {
		return listaConsultarListaContratosPessoas;
	}

	/**
	 * Set: listaConsultarListaContratosPessoas.
	 *
	 * @param listaConsultarListaContratosPessoas the lista consultar lista contratos pessoas
	 */
	public void setListaConsultarListaContratosPessoas(List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas) {
		this.listaConsultarListaContratosPessoas = listaConsultarListaContratosPessoas;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<EmpresaGestoraSaidaDTO> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<EmpresaGestoraSaidaDTO> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listTipoContratoDTO.
	 *
	 * @return listTipoContratoDTO
	 */
	public List<TipoContratoSaidaDTO> getListTipoContratoDTO() {
		return listTipoContrato;
	}

	/**
	 * Set: listTipoContratoDTO.
	 *
	 * @param listTipoContratoDTO the list tipo contrato dto
	 */
	public void setListTipoContratoDTO(List<TipoContratoSaidaDTO> listTipoContratoDTO) {
		this.listTipoContrato = listTipoContratoDTO;
	}

	/**
	 * Get: paginaRetorno.
	 *
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Get: saidaConsultarListaClientePessoas.
	 *
	 * @return saidaConsultarListaClientePessoas
	 */
	public ConsultarListaClientePessoasSaidaDTO getSaidaConsultarListaClientePessoas() {
		return saidaConsultarListaClientePessoas;
	}

	/**
	 * Set: saidaConsultarListaClientePessoas.
	 *
	 * @param saidaConsultarListaClientePessoas the saida consultar lista cliente pessoas
	 */
	public void setSaidaConsultarListaClientePessoas(ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoas) {
		this.saidaConsultarListaClientePessoas = saidaConsultarListaClientePessoas;
	}

	/**
	 * Get: saidaConsultarListaContratosPessoas.
	 *
	 * @return saidaConsultarListaContratosPessoas
	 */
	public ConsultarListaContratosPessoasSaidaDTO getSaidaConsultarListaContratosPessoas() {
		return saidaConsultarListaContratosPessoas;
	}

	/**
	 * Set: saidaConsultarListaContratosPessoas.
	 *
	 * @param saidaConsultarListaContratosPessoas the saida consultar lista contratos pessoas
	 */
	public void setSaidaConsultarListaContratosPessoas(ConsultarListaContratosPessoasSaidaDTO saidaConsultarListaContratosPessoas) {
		this.saidaConsultarListaContratosPessoas = saidaConsultarListaContratosPessoas;
	}

	/**
	 * Get: saidaEmpresaGestora.
	 *
	 * @return saidaEmpresaGestora
	 */
	public EmpresaGestoraSaidaDTO getSaidaEmpresaGestora() {
		return saidaEmpresaGestora;
	}

	/**
	 * Set: saidaEmpresaGestora.
	 *
	 * @param saidaEmpresaGestora the saida empresa gestora
	 */
	public void setSaidaEmpresaGestora(EmpresaGestoraSaidaDTO saidaEmpresaGestora) {
		this.saidaEmpresaGestora = saidaEmpresaGestora;
	}

	/**
	 * Get: saidaTipoContrato.
	 *
	 * @return saidaTipoContrato
	 */
	public TipoContratoSaidaDTO getSaidaTipoContrato() {
		return saidaTipoContrato;
	}

	/**
	 * Set: saidaTipoContratoDTO.
	 *
	 * @param saidaTipoContrato the saida tipo contrato dto
	 */
	public void setSaidaTipoContratoDTO(TipoContratoSaidaDTO saidaTipoContrato) {
		this.saidaTipoContrato = saidaTipoContrato;
	}

	/**
	 * Get: itemTipoContratoSelecionada.
	 *
	 * @return itemTipoContratoSelecionada
	 */
	public Integer getItemTipoContratoSelecionada() {
		return itemTipoContratoSelecionada;
	}

	/**
	 * Set: itemTipoContratoSelecionada.
	 *
	 * @param itemTipoContratoSelecionada the item tipo contrato selecionada
	 */
	public void setItemTipoContratoSelecionada(Integer itemTipoContratoSelecionada) {
		this.itemTipoContratoSelecionada = itemTipoContratoSelecionada;
	}

	/**
	 * Get: itemEmpresaSelecionada.
	 *
	 * @return itemEmpresaSelecionada
	 */
	public Long getItemEmpresaSelecionada() {
		return itemEmpresaSelecionada;
	}

	/**
	 * Set: itemEmpresaSelecionada.
	 *
	 * @param itemEmpresaSelecionada the item empresa selecionada
	 */
	public void setItemEmpresaSelecionada(Long itemEmpresaSelecionada) {
		this.itemEmpresaSelecionada = itemEmpresaSelecionada;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Is desabilata filtro.
	 *
	 * @return true, if is desabilata filtro
	 */
	public boolean isDesabilataFiltro() {
		return desabilataFiltro;
	}

	/**
	 * Set: desabilataFiltro.
	 *
	 * @param desabilataFiltro the desabilata filtro
	 */
	public void setDesabilataFiltro(boolean desabilataFiltro) {
		this.desabilataFiltro = desabilataFiltro;
	}

	/**
	 * Get: listTipoContrato.
	 *
	 * @return listTipoContrato
	 */
	public List<TipoContratoSaidaDTO> getListTipoContrato() {
		return listTipoContrato;
	}

	/**
	 * Set: listTipoContrato.
	 *
	 * @param listTipoContrato the list tipo contrato
	 */
	public void setListTipoContrato(List<TipoContratoSaidaDTO> listTipoContrato) {
		this.listTipoContrato = listTipoContrato;
	}

	/**
	 * Get: preencherListaEmpresaGestora.
	 *
	 * @return preencherListaEmpresaGestora
	 */
	public List<SelectItem> getPreencherListaEmpresaGestora() {
		return preencherListaEmpresaGestora;
	}

	/**
	 * Set: preencherListaEmpresaGestora.
	 *
	 * @param preencherListaEmpresaGestora the preencher lista empresa gestora
	 */
	public void setPreencherListaEmpresaGestora(List<SelectItem> preencherListaEmpresaGestora) {
		this.preencherListaEmpresaGestora = preencherListaEmpresaGestora;
	}

	/**
	 * Set: preencherTipoContrato.
	 *
	 * @param preencherTipoContrato the preencher tipo contrato
	 */
	public void setPreencherTipoContrato(List<SelectItem> preencherTipoContrato) {
		this.preencherTipoContrato = preencherTipoContrato;
	}

	/**
	 * Set: saidaTipoContrato.
	 *
	 * @param saidaTipoContrato the saida tipo contrato
	 */
	public void setSaidaTipoContrato(TipoContratoSaidaDTO saidaTipoContrato) {
		this.saidaTipoContrato = saidaTipoContrato;
	}

	/**
	 * Get: itemFiltroSelecionadoManutencao.
	 *
	 * @return itemFiltroSelecionadoManutencao
	 */
	public String getItemFiltroSelecionadoManutencao() {
		return itemFiltroSelecionadoManutencao;
	}

	/**
	 * Set: itemFiltroSelecionadoManutencao.
	 *
	 * @param itemFiltroSelecionadoManutencao the item filtro selecionado manutencao
	 */
	public void setItemFiltroSelecionadoManutencao(String itemFiltroSelecionadoManutencao) {
		this.itemFiltroSelecionadoManutencao = itemFiltroSelecionadoManutencao;
	}

	/**
	 * Is habilita filtro de contrato manutencao.
	 *
	 * @return true, if is habilita filtro de contrato manutencao
	 */
	public boolean isHabilitaFiltroDeContratoManutencao() {
		return habilitaFiltroDeContratoManutencao;
	}

	/**
	 * Set: habilitaFiltroDeContratoManutencao.
	 *
	 * @param habilitaFiltroDeContratoManutencao the habilita filtro de contrato manutencao
	 */
	public void setHabilitaFiltroDeContratoManutencao(boolean habilitaFiltroDeContratoManutencao) {
		this.habilitaFiltroDeContratoManutencao = habilitaFiltroDeContratoManutencao;
	}

	/**
	 * Get: saidaConsultarListaClientePessoasManutencao.
	 *
	 * @return saidaConsultarListaClientePessoasManutencao
	 */
	public ConsultarListaClientePessoasSaidaDTO getSaidaConsultarListaClientePessoasManutencao() {
		return saidaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Set: saidaConsultarListaClientePessoasManutencao.
	 *
	 * @param saidaConsultarListaClientePessoasManutencao the saida consultar lista cliente pessoas manutencao
	 */
	public void setSaidaConsultarListaClientePessoasManutencao(ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoasManutencao) {
		this.saidaConsultarListaClientePessoasManutencao = saidaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Get: saidaConsultarListaContratosPessoasManutencao.
	 *
	 * @return saidaConsultarListaContratosPessoasManutencao
	 */
	public ConsultarListaContratosPessoasSaidaDTO getSaidaConsultarListaContratosPessoasManutencao() {
		return saidaConsultarListaContratosPessoasManutencao;
	}

	/**
	 * Set: saidaConsultarListaContratosPessoasManutencao.
	 *
	 * @param saidaConsultarListaContratosPessoasManutencao the saida consultar lista contratos pessoas manutencao
	 */
	public void setSaidaConsultarListaContratosPessoasManutencao(ConsultarListaContratosPessoasSaidaDTO saidaConsultarListaContratosPessoasManutencao) {
		this.saidaConsultarListaContratosPessoasManutencao = saidaConsultarListaContratosPessoasManutencao;
	}

	/**
	 * Get: entradaConsultarListaContratosPessoasManutencao.
	 *
	 * @return entradaConsultarListaContratosPessoasManutencao
	 */
	public ConsultarListaContratosPessoasEntradaDTO getEntradaConsultarListaContratosPessoasManutencao() {
		return entradaConsultarListaContratosPessoasManutencao;
	}

	/**
	 * Set: entradaConsultarListaContratosPessoasManutencao.
	 *
	 * @param entradaConsultarListaContratosPessoasManutencao the entrada consultar lista contratos pessoas manutencao
	 */
	public void setEntradaConsultarListaContratosPessoasManutencao(ConsultarListaContratosPessoasEntradaDTO entradaConsultarListaContratosPessoasManutencao) {
		this.entradaConsultarListaContratosPessoasManutencao = entradaConsultarListaContratosPessoasManutencao;
	}

	/**
	 * Get: paginaCliente.
	 *
	 * @return paginaCliente
	 */
	public String getPaginaCliente() {
		return paginaCliente;
	}

	/**
	 * Set: paginaCliente.
	 *
	 * @param paginaCliente the pagina cliente
	 */
	public void setPaginaCliente(String paginaCliente) {
		this.paginaCliente = paginaCliente;
	}

	/**
	 * Get: paginaContrato.
	 *
	 * @return paginaContrato
	 */
	public String getPaginaContrato() {
		return paginaContrato;
	}

	/**
	 * Set: paginaContrato.
	 *
	 * @param paginaContrato the pagina contrato
	 */
	public void setPaginaContrato(String paginaContrato) {
		this.paginaContrato = paginaContrato;
	}

	/**
	 * Get: entradaConsultarListaClientePessoasManutencao.
	 *
	 * @return entradaConsultarListaClientePessoasManutencao
	 */
	public ConsultarListaClientePessoasEntradaDTO getEntradaConsultarListaClientePessoasManutencao() {
		return entradaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Set: entradaConsultarListaClientePessoasManutencao.
	 *
	 * @param entradaConsultarListaClientePessoasManutencao the entrada consultar lista cliente pessoas manutencao
	 */
	public void setEntradaConsultarListaClientePessoasManutencao(ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoasManutencao) {
		this.entradaConsultarListaClientePessoasManutencao = entradaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Get: habilitaAvancar.
	 *
	 * @return habilitaAvancar
	 */
	public Boolean getHabilitaAvancar() {
		return habilitaAvancar;
	}

	/**
	 * Set: habilitaAvancar.
	 *
	 * @param habilitaAvancar the habilita avancar
	 */
	public void setHabilitaAvancar(Boolean habilitaAvancar) {
		this.habilitaAvancar = habilitaAvancar;
	}

	/**
	 * Is bloquea radio.
	 *
	 * @return true, if is bloquea radio
	 */
	public boolean isBloqueaRadio() {
		return bloqueaRadio;
	}

	/**
	 * Set: bloqueaRadio.
	 *
	 * @param bloqueaRadio the bloquea radio
	 */
	public void setBloqueaRadio(boolean bloqueaRadio) {
		this.bloqueaRadio = bloqueaRadio;
	}

	/**
	 * Is bloquea tipo contrato.
	 *
	 * @return true, if is bloquea tipo contrato
	 */
	public boolean isBloqueaTipoContrato() {
		return bloqueaTipoContrato;
	}

	/**
	 * Set: bloqueaTipoContrato.
	 *
	 * @param bloqueaTipoContrato the bloquea tipo contrato
	 */
	public void setBloqueaTipoContrato(boolean bloqueaTipoContrato) {
		this.bloqueaTipoContrato = bloqueaTipoContrato;
	}

	/**
	 * Is bloquea filtro.
	 *
	 * @return true, if is bloquea filtro
	 */
	public boolean isBloqueaFiltro() {
		return bloqueaFiltro;
	}

	/**
	 * Set: bloqueaFiltro.
	 *
	 * @param bloqueaFiltro the bloquea filtro
	 */
	public void setBloqueaFiltro(boolean bloqueaFiltro) {
		this.bloqueaFiltro = bloqueaFiltro;
	}

	/**
	 * Get: itemContratoSelec.
	 *
	 * @return itemContratoSelec
	 */
	public Integer getItemContratoSelec() {
		return itemContratoSelec;
	}

	/**
	 * Set: itemContratoSelec.
	 *
	 * @param itemContratoSelec the item contrato selec
	 */
	public void setItemContratoSelec(Integer itemContratoSelec) {
		this.itemContratoSelec = itemContratoSelec;
	}

	/**
	 * Get: habilitaLimpa.
	 *
	 * @return habilitaLimpa
	 */
	public Boolean getHabilitaLimpa() {
		return habilitaLimpa;
	}

	/**
	 * Set: habilitaLimpa.
	 *
	 * @param habilitaLimpa the habilita limpa
	 */
	public void setHabilitaLimpa(Boolean habilitaLimpa) {
		this.habilitaLimpa = habilitaLimpa;
	}

	/**
	 * Get: bloqueaAgConta.
	 *
	 * @return bloqueaAgConta
	 */
	public Boolean getBloqueaAgConta() {
		return bloqueaAgConta;
	}

	/**
	 * Set: bloqueaAgConta.
	 *
	 * @param bloqueaAgConta the bloquea ag conta
	 */
	public void setBloqueaAgConta(Boolean bloqueaAgConta) {
		this.bloqueaAgConta = bloqueaAgConta;
	}

	/**
	 * Get: limpaCamp.
	 *
	 * @return limpaCamp
	 */
	public Boolean getLimpaCamp() {
		return limpaCamp;
	}

	/**
	 * Set: limpaCamp.
	 *
	 * @param limpaCamp the limpa camp
	 */
	public void setLimpaCamp(Boolean limpaCamp) {
		this.limpaCamp = limpaCamp;
	}

	/**
	 * Is habilita filtro de cad conta salario det.
	 *
	 * @return true, if is habilita filtro de cad conta salario det
	 */
	public boolean isHabilitaFiltroDeCadContaSalarioDet() {
		return habilitaFiltroDeCadContaSalarioDet;
	}

	/**
	 * Set: habilitaFiltroDeCadContaSalarioDet.
	 *
	 * @param habilitaFiltroDeCadContaSalarioDet the habilita filtro de cad conta salario det
	 */
	public void setHabilitaFiltroDeCadContaSalarioDet(
			boolean habilitaFiltroDeCadContaSalarioDet) {
		this.habilitaFiltroDeCadContaSalarioDet = habilitaFiltroDeCadContaSalarioDet;
	}

	/**
	 * Get: paginaRetornoManutencao.
	 *
	 * @return paginaRetornoManutencao
	 */
	public String getPaginaRetornoManutencao() {
		return paginaRetornoManutencao;
	}

	/**
	 * Set: paginaRetornoManutencao.
	 *
	 * @param paginaRetornoManutencao the pagina retorno manutencao
	 */
	public void setPaginaRetornoManutencao(String paginaRetornoManutencao) {
		this.paginaRetornoManutencao = paginaRetornoManutencao;
	}

	/**
	 * Get: paginaClienteManutencao.
	 *
	 * @return paginaClienteManutencao
	 */
	public String getPaginaClienteManutencao() {
		return paginaClienteManutencao;
	}

	/**
	 * Set: paginaClienteManutencao.
	 *
	 * @param paginaClienteManutencao the pagina cliente manutencao
	 */
	public void setPaginaClienteManutencao(String paginaClienteManutencao) {
		this.paginaClienteManutencao = paginaClienteManutencao;
	}

	/**
	 * Get: paginaContratoManutencao.
	 *
	 * @return paginaContratoManutencao
	 */
	public String getPaginaContratoManutencao() {
		return paginaContratoManutencao;
	}

	/**
	 * Set: paginaContratoManutencao.
	 *
	 * @param paginaContratoManutencao the pagina contrato manutencao
	 */
	public void setPaginaContratoManutencao(String paginaContratoManutencao) {
		this.paginaContratoManutencao = paginaContratoManutencao;
	}

	/**
	 * Is habilita filtro de cliente.
	 *
	 * @return true, if is habilita filtro de cliente
	 */
	public boolean isHabilitaFiltroDeCliente() {
		return habilitaFiltroDeCliente;
	}

	/**
	 * Set: habilitaFiltroDeCliente.
	 *
	 * @param habilitaFiltroDeCliente the habilita filtro de cliente
	 */
	public void setHabilitaFiltroDeCliente(boolean habilitaFiltroDeCliente) {
		this.habilitaFiltroDeCliente = habilitaFiltroDeCliente;
	}

	/**
	 * Get: dsEmpresaGestoraContrato.
	 *
	 * @return dsEmpresaGestoraContrato
	 */
	public String getDsEmpresaGestoraContrato() {
	    return dsEmpresaGestoraContrato;
	}

	/**
	 * Set: dsEmpresaGestoraContrato.
	 *
	 * @param dsEmpresaGestoraContrato the ds empresa gestora contrato
	 */
	public void setDsEmpresaGestoraContrato(String dsEmpresaGestoraContrato) {
	    this.dsEmpresaGestoraContrato = dsEmpresaGestoraContrato;
	}

	/**
	 * Is contrato selecionado.
	 *
	 * @return true, if is contrato selecionado
	 */
	public boolean isContratoSelecionado() {
	    return contratoSelecionado;
	}

	/**
	 * Set: contratoSelecionado.
	 *
	 * @param contratoSelecionado the contrato selecionado
	 */
	public void setContratoSelecionado(boolean contratoSelecionado) {
	    this.contratoSelecionado = contratoSelecionado;
	}

	/**
	 * Is habilita campos apos cons contrato.
	 *
	 * @return true, if is habilita campos apos cons contrato
	 */
	public boolean isHabilitaCamposAposConsContrato() {
		return habilitaCamposAposConsContrato;
	}

	/**
	 * Set: habilitaCamposAposConsContrato.
	 *
	 * @param habilitaCamposAposConsContrato the habilita campos apos cons contrato
	 */
	public void setHabilitaCamposAposConsContrato(
			boolean habilitaCamposAposConsContrato) {
		this.habilitaCamposAposConsContrato = habilitaCamposAposConsContrato;
	}

	/**
	 * Set: habilitaLimparTabela.
	 *
	 * @param habilitaLimparTabela the habilita limpar tabela
	 */
	public void setHabilitaLimparTabela(boolean habilitaLimparTabela) {
		this.habilitaLimparTabela = habilitaLimparTabela;
	}

	/**
	 * Is habilita limpar tabela.
	 *
	 * @return true, if is habilita limpar tabela
	 */
	public boolean isHabilitaLimparTabela() {
		return habilitaLimparTabela;
	}

}	
