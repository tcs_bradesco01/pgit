/*
 * Nome: br.com.bradesco.web.pgit.view.converters
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters;

import java.text.ParseException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.pgit.utils.NumberUtils;

/**
 * Nome: InteiroMilharConverter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class InteiroMilharConverter implements Converter {

	/** Atributo INTEIRO_MILHAR_FORMAT. */
	private static final String INTEIRO_MILHAR_FORMAT = "#,###";  

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) throws ConverterException {
		if(arg2 == null || arg2.trim().equals("")) {
			return null;
		}

		try {
			return NumberUtils.convert(arg2, INTEIRO_MILHAR_FORMAT);
		} catch (ParseException e) {
			throw new ConverterException(e);
		}  
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2)	throws ConverterException {
		if(arg2 == null) {
			return null;
		}

		return NumberUtils.format((Number) arg2, INTEIRO_MILHAR_FORMAT);
	}

}
