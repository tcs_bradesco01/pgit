/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.solrelempresasacomp
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.solrelempresasacomp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.ISolRelEmpresasAcompService;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelempresasacomp.bean.SolicitacaoRelatorioEmpresasAcompOcorrencia;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.selectitem.SelectItemUtils;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: SolicitacaoRelatorioEmpresasAcompBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolicitacaoRelatorioEmpresasAcompBean {

	/** Atributo CONSULTA. */
	private static final String CONSULTA = "conSolicitRelatEmpresasAcomp";

	/** Atributo solRelEmpresasAcompService. */
	private ISolRelEmpresasAcompService solRelEmpresasAcompService;

	/** Atributo solicitacoes. */
	private List<SolicitacaoRelatorioEmpresasAcompOcorrencia> solicitacoes;
	
	/** Atributo entradaInclusaoEmpresaAcompanhada. */
	private IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO entradaInclusaoEmpresaAcompanhada;
	
	/** Atributo saidaInclusaoEmpresaAcompanhada. */
	private IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO saidaInclusaoEmpresaAcompanhada;

	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;

	/** Atributo dataInicial. */
	private Date dataInicial;

	/** Atributo dataFinal. */
	private Date dataFinal;
	
	/** Atributo usuarioSolicitanteDetalhe. */
	private String usuarioSolicitanteDetalhe;
	
	/** Atributo dataSolicicacaoDetalhe. */
	private String dataSolicicacaoDetalhe;
	
	/** Atributo validacao. */
	private boolean validacao = false;
	
	/**
	 * Iniciar tela.
	 *
	 * @return the string
	 */
	public String iniciarTela() {
		limparDados();
		return CONSULTA;
	}

	/**
	 * Consultar.
	 */
	public void consultar() {
		setSolicitacoes(solRelEmpresasAcompService.listarSolicitacoes(dataInicial, dataFinal));
	}

	/**
	 * Limpar dados.
	 */
	public void limparDados() {
		setDataInicial(new Date());
		setDataFinal(new Date());
		setValidacao(false);
	}

	/**
	 * Confirmar incluir.
	 */
	public void confirmarIncluir() {
		entradaInclusaoEmpresaAcompanhada = new IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO();
		
		if(isValidacao()){
			entradaInclusaoEmpresaAcompanhada.setCdTipoAcesso(1);
		}else{
			entradaInclusaoEmpresaAcompanhada.setCdTipoAcesso(2);
		}

		if(saidaInclusaoEmpresaAcompanhada!= null){
		entradaInclusaoEmpresaAcompanhada.setCdUsuarioInclusao(PgitUtil.verificaStringNula(saidaInclusaoEmpresaAcompanhada.getCdUsuarioInclusao()));
		entradaInclusaoEmpresaAcompanhada.setHrSolicitacaoPagamento(PgitUtil.verificaStringNula(saidaInclusaoEmpresaAcompanhada.getDtHoraPagamentoIntegrado()));
		}
		
		saidaInclusaoEmpresaAcompanhada = solRelEmpresasAcompService.incluirSolicitacoes(entradaInclusaoEmpresaAcompanhada);
		
		if(!isValidacao()){
			PgitFacesUtils.addInfoModalMessage(saidaInclusaoEmpresaAcompanhada.getCodMensagem(), saidaInclusaoEmpresaAcompanhada.getMensagem(),"#{solicitacaoRelatorioEmpresasAcompBean.consultarAposAcao}");
		}
		
		setValidacao(false);
	}
	
	/**
	 * Paginar emp acomp.
	 *
	 * @param evt the evt
	 */
	public void paginarEmpAcomp(ActionEvent evt) {
		consultar();
	}

	/**
	 * Confirmar excluir.
	 */
	public void confirmarExcluir() {
		SolicitacaoRelatorioEmpresasAcompOcorrencia solicitacaoSelecionada = solicitacoes.get(getItemSelecionado());
		ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO entradaExclusaoEmpresaAcompanhada = new ExcluirSoliRelatorioEmpresaAcompanhadaEntradaDTO();
		
		entradaExclusaoEmpresaAcompanhada.setCdSolicitacaoPagamentoIntegrado(solicitacaoSelecionada.getCdSolicitacaoPagamentoIntegrado());
		entradaExclusaoEmpresaAcompanhada.setNrSolicitacaoPagamentoIntegrado(solicitacaoSelecionada.getNrSolicitacaoPagamentoIntegrado());
	
		MensagemSaida saida = solRelEmpresasAcompService.excluirSolicitacoes(entradaExclusaoEmpresaAcompanhada);
		PgitFacesUtils.addInfoModalMessage(saida.getCodMensagem(), saida.getMensagem(),"#{solicitacaoRelatorioEmpresasAcompBean.consultarAposAcao}");
		
	}
	
	/**
	 * Consultar apos acao.
	 *
	 * @return the string
	 */
	public String consultarAposAcao() {
		try {
			consultar();
			setValidacao(false);
		} catch (PdcAdapterFunctionalException e) {		
			setSolicitacoes(new ArrayList<SolicitacaoRelatorioEmpresasAcompOcorrencia>());
			limparDados();
		}
		
		return CONSULTA;
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		setValidacao(true);
		confirmarIncluir();
		setDataSolicicacaoDetalhe(FormatarData.formataDiaMesAno(FormatarData.formataTimestampFromPdc(saidaInclusaoEmpresaAcompanhada.getDtHoraPagamentoIntegrado())));
		setUsuarioSolicitanteDetalhe(saidaInclusaoEmpresaAcompanhada.getCdUsuarioInclusao());
		return "INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		preencheDadosDetalhar();
		return "EXCLUIR";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		preencheDadosDetalhar();
		return "DETALHAR";
	}

	/**
	 * Voltar geral.
	 *
	 * @return the string
	 */
	public String voltarGeral(){
		limparDados();
		return "VOLTAR";
	}
	
	/**
	 * Preenche dados detalhar.
	 */
	public void preencheDadosDetalhar(){
		SolicitacaoRelatorioEmpresasAcompOcorrencia solicitacaoSelecionada = solicitacoes.get(getItemSelecionado());
		setDataSolicicacaoDetalhe(obterDataDetalhe(solicitacaoSelecionada));
		setUsuarioSolicitanteDetalhe(solicitacaoSelecionada.getCdUsuarioInclusao());
	}

	/**
	 * Obter data detalhe.
	 *
	 * @param solicitacaoSelecionada the solicitacao selecionada
	 * @return the string
	 */
	private String obterDataDetalhe(SolicitacaoRelatorioEmpresasAcompOcorrencia solicitacaoSelecionada) {
		if(solicitacaoSelecionada.getHrSolicitacaoPagamento()!= null && solicitacaoSelecionada.getHrSolicitacaoPagamento().length() >= 10){
		  String data = solicitacaoSelecionada.getHrSolicitacaoPagamento().substring(0,10);
		  return data;
		}
		return "";
	}
	
	/**
	 * Get: itensSelecionaveis.
	 *
	 * @return itensSelecionaveis
	 */
	public List<SelectItem> getItensSelecionaveis() {
		return SelectItemUtils.criarSelectItems(solicitacoes);
	}

	/**
	 * Get: solRelEmpresasAcompService.
	 *
	 * @return solRelEmpresasAcompService
	 */
	public ISolRelEmpresasAcompService getSolRelEmpresasAcompService() {
		return solRelEmpresasAcompService;
	}

	/**
	 * Set: solRelEmpresasAcompService.
	 *
	 * @param solRelEmpresasAcompService the sol rel empresas acomp service
	 */
	public void setSolRelEmpresasAcompService(ISolRelEmpresasAcompService solRelEmpresasAcompService) {
		this.solRelEmpresasAcompService = solRelEmpresasAcompService;
	}

	/**
	 * Get: dataInicial.
	 *
	 * @return dataInicial
	 */
	public Date getDataInicial() {
		return dataInicial;
	}

	/**
	 * Set: dataInicial.
	 *
	 * @param dataInicial the data inicial
	 */
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	/**
	 * Get: dataFinal.
	 *
	 * @return dataFinal
	 */
	public Date getDataFinal() {
		return dataFinal;
	}

	/**
	 * Set: dataFinal.
	 *
	 * @param dataFinal the data final
	 */
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	/**
	 * Get: solicitacoes.
	 *
	 * @return solicitacoes
	 */
	public List<SolicitacaoRelatorioEmpresasAcompOcorrencia> getSolicitacoes() {
		return solicitacoes;
	}

	/**
	 * Set: solicitacoes.
	 *
	 * @param solicitacoes the solicitacoes
	 */
	public void setSolicitacoes(List<SolicitacaoRelatorioEmpresasAcompOcorrencia> solicitacoes) {
		this.solicitacoes = solicitacoes;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: entradaInclusaoEmpresaAcompanhada.
	 *
	 * @return entradaInclusaoEmpresaAcompanhada
	 */
	public IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO getEntradaInclusaoEmpresaAcompanhada() {
		return entradaInclusaoEmpresaAcompanhada;
	}

	/**
	 * Set: entradaInclusaoEmpresaAcompanhada.
	 *
	 * @param entradaInclusaoEmpresaAcompanhada the entrada inclusao empresa acompanhada
	 */
	public void setEntradaInclusaoEmpresaAcompanhada(
			IncluirSoliRelatorioEmpresaAcompanhadaEntradaDTO entradaInclusaoEmpresaAcompanhada) {
		this.entradaInclusaoEmpresaAcompanhada = entradaInclusaoEmpresaAcompanhada;
	}

	/**
	 * Get: saidaInclusaoEmpresaAcompanhada.
	 *
	 * @return saidaInclusaoEmpresaAcompanhada
	 */
	public IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO getSaidaInclusaoEmpresaAcompanhada() {
		return saidaInclusaoEmpresaAcompanhada;
	}

	/**
	 * Set: saidaInclusaoEmpresaAcompanhada.
	 *
	 * @param saidaInclusaoEmpresaAcompanhada the saida inclusao empresa acompanhada
	 */
	public void setSaidaInclusaoEmpresaAcompanhada(
			IncluirSoliRelatorioEmpresaAcompanhadaSaidaDTO saidaInclusaoEmpresaAcompanhada) {
		this.saidaInclusaoEmpresaAcompanhada = saidaInclusaoEmpresaAcompanhada;
	}

	/**
	 * Get: usuarioSolicitanteDetalhe.
	 *
	 * @return usuarioSolicitanteDetalhe
	 */
	public String getUsuarioSolicitanteDetalhe() {
		return usuarioSolicitanteDetalhe;
	}

	/**
	 * Set: usuarioSolicitanteDetalhe.
	 *
	 * @param usuarioSolicitanteDetalhe the usuario solicitante detalhe
	 */
	public void setUsuarioSolicitanteDetalhe(String usuarioSolicitanteDetalhe) {
		this.usuarioSolicitanteDetalhe = usuarioSolicitanteDetalhe;
	}

	/**
	 * Is validacao.
	 *
	 * @return true, if is validacao
	 */
	public boolean isValidacao() {
		return validacao;
	}

	/**
	 * Set: validacao.
	 *
	 * @param validacao the validacao
	 */
	public void setValidacao(boolean validacao) {
		this.validacao = validacao;
	}

	/**
	 * Get: dataSolicicacaoDetalhe.
	 *
	 * @return dataSolicicacaoDetalhe
	 */
	public String getDataSolicicacaoDetalhe() {
		return dataSolicicacaoDetalhe;
	}

	/**
	 * Set: dataSolicicacaoDetalhe.
	 *
	 * @param dataSolicicacaoDetalhe the data solicicacao detalhe
	 */
	public void setDataSolicicacaoDetalhe(String dataSolicicacaoDetalhe) {
		this.dataSolicicacaoDetalhe = dataSolicicacaoDetalhe;
	}
}