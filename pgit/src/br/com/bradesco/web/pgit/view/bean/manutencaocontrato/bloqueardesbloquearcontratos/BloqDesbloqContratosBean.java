/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearcontratos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearcontratos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.IBloquearDesbloquearContratosService;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.bean.BloquearDesbloquearContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearcontratos.bean.BloquearDesbloquearContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.utils.MessageUtils;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: BloqDesbloqContratosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloqDesbloqContratosBean {
	
	/** Atributo bloqDesbloqFiltro. */
	private Integer bloqDesbloqFiltro;
	
	/** Atributo bloqDesbloqContratosEspelhadosFiltro. */
	private Integer bloqDesbloqContratosEspelhadosFiltro;
	
	/** Atributo motivoBloqueioFiltro. */
	private Integer motivoBloqueioFiltro;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo hiddenProsseguir. */
	private String hiddenProsseguir;
	
	/** Atributo listaMotivoBloqueio. */
	private List<SelectItem> listaMotivoBloqueio;
	
	/** Atributo listaMotivoBloqueioDesc. */
	private String listaMotivoBloqueioDesc;
	
	/** Atributo listaMotivoBloqueioHash. */
	private Map<Integer, String> listaMotivoBloqueioHash = new HashMap<Integer, String>();
	
	/** Atributo btnAvancar. */
	private boolean btnAvancar;
	
	/** Atributo bloquearDesbloquearContratosServiceImpl. */
	private IBloquearDesbloquearContratosService bloquearDesbloquearContratosServiceImpl;
	
//	Variaveis Cliente Master - Cliente - Contrato - Conta
	/** Atributo cpfCnpj. */
private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;
	
	/** Atributo nomeRazaoMaster. */
	private String nomeRazaoMaster;
	
	/** Atributo gerenteResponsavel. */
	private String gerenteResponsavel;
	
	/** Atributo clienteParticipante. */
	private String clienteParticipante;
	
	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;
	
	/** Atributo ativEconomicaMaster. */
	private String ativEconomicaMaster;
	
	/** Atributo segmentoMaster. */
	private String segmentoMaster;
	
	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;
	
	/** Atributo cpfCnpjCliente. */
	private String cpfCnpjCliente;
	
	/** Atributo nomeRazaoCliente. */
	private String nomeRazaoCliente;
	
	/** Atributo empresaContrato. */
	private String empresaContrato;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo motivoContrato. */
	private String motivoContrato;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo possuiAditivosContrato. */
	private String possuiAditivosContrato;
	
	/** Atributo dataHoraCadContrato. */
	private String dataHoraCadContrato;
	
	/** Atributo inicioVigenciaContrato. */
	private String inicioVigenciaContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo cdSituacaoContrato. */
	private Integer cdSituacaoContrato;
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo bancoConta. */
	private String bancoConta;
	
	/** Atributo agenciaConta. */
	private String agenciaConta;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo tipoConta. */
	private String tipoConta;
	
	/** Atributo tipoVinculoConta. */
	private String tipoVinculoConta;
	
	/** Atributo situacaoConta. */
	private String situacaoConta;
	
	/** Atributo dataVinculacaoConta. */
	private String dataVinculacaoConta;	
//Fim Variaveis Cliente Master - Cliente - Contrato - Conta
	/**
 * Habilita botao.
 */
public void habilitaBotao(){		
		if((this.motivoBloqueioFiltro != 0) && (this.bloqDesbloqContratosEspelhadosFiltro != null)){
			setBtnAvancar(true);			
		}else{
			
				setBtnAvancar(false);				
			
		}
		
	}
	
	/**
	 * Carrega lista bloqueio.
	 */
	public void carregaListaBloqueio(){
		this.listaMotivoBloqueio.clear();
		setMotivoBloqueioFiltro(0);
		if(this.getBloqDesbloqFiltro() == 1){
			listarMotivoBloqueio(4);			
		}
		else{
			if(this.getBloqDesbloqFiltro() == 2){
				listarMotivoBloqueio(1);
			}
		}
	}
	
	/**
	 * Listar motivo bloqueio.
	 *
	 * @param a the a
	 */
	public void listarMotivoBloqueio(int a){
		listaMotivoBloqueio = new ArrayList<SelectItem>();
		List<ListarMotivoSituacaoContratoSaidaDTO> list = new ArrayList<ListarMotivoSituacaoContratoSaidaDTO>();
		ListarMotivoSituacaoContratoEntradaDTO entradaDTO = new ListarMotivoSituacaoContratoEntradaDTO();
		
		entradaDTO.setCdSituacao(a);
		
		list = comboService.listarMotivoSituacaoContrato(entradaDTO);
		listaMotivoBloqueio.clear();
		for(ListarMotivoSituacaoContratoSaidaDTO combo : list){
			listaMotivoBloqueioHash.put(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato());
			listaMotivoBloqueio.add(new SelectItem(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato()));
		}
	}
	
	/**
	 * Get: hiddenProsseguir.
	 *
	 * @return hiddenProsseguir
	 */
	public String getHiddenProsseguir() {
		return hiddenProsseguir;
	}

	/**
	 * Set: hiddenProsseguir.
	 *
	 * @param hiddenProsseguir the hidden prosseguir
	 */
	public void setHiddenProsseguir(String hiddenProsseguir) {
		this.hiddenProsseguir = hiddenProsseguir;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: bloqDesbloqContratosEspelhadosFiltro.
	 *
	 * @return bloqDesbloqContratosEspelhadosFiltro
	 */
	public Integer getBloqDesbloqContratosEspelhadosFiltro() {
		return bloqDesbloqContratosEspelhadosFiltro;
	}

	/**
	 * Set: bloqDesbloqContratosEspelhadosFiltro.
	 *
	 * @param bloqDesbloqContratosEspelhadosFiltro the bloq desbloq contratos espelhados filtro
	 */
	public void setBloqDesbloqContratosEspelhadosFiltro(
			Integer bloqDesbloqContratosEspelhadosFiltro) {
		this.bloqDesbloqContratosEspelhadosFiltro = bloqDesbloqContratosEspelhadosFiltro;
	}

	/**
	 * Get: bloqDesbloqFiltro.
	 *
	 * @return bloqDesbloqFiltro
	 */
	public Integer getBloqDesbloqFiltro() {
		return bloqDesbloqFiltro;
	}

	/**
	 * Set: bloqDesbloqFiltro.
	 *
	 * @param bloqDesbloqFiltro the bloq desbloq filtro
	 */
	public void setBloqDesbloqFiltro(Integer bloqDesbloqFiltro) {
		this.bloqDesbloqFiltro = bloqDesbloqFiltro;
	}

	/**
	 * Get: motivoBloqueioFiltro.
	 *
	 * @return motivoBloqueioFiltro
	 */
	public Integer getMotivoBloqueioFiltro() {
		return motivoBloqueioFiltro;
	}

	/**
	 * Set: motivoBloqueioFiltro.
	 *
	 * @param motivoBloqueioFiltro the motivo bloqueio filtro
	 */
	public void setMotivoBloqueioFiltro(Integer motivoBloqueioFiltro) {
		this.motivoBloqueioFiltro = motivoBloqueioFiltro;
	}

	/**
	 * Votlar bloquear desbloquear.
	 *
	 * @return the string
	 */
	public String votlarBloquearDesbloquear(){
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Avancar.
	 *
	 * @return the string
	 */
	public String avancar(){
		this.setListaMotivoBloqueioDesc((String)listaMotivoBloqueioHash.get(this.getMotivoBloqueioFiltro()));
		return "AVANCAR";

	}
	
	/**
	 * Voltar confirmar.
	 *
	 * @return the string
	 */
	public String voltarConfirmar(){
		return "VOLTAR";
	}
	
	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar(){
		try{
			BloquearDesbloquearContratosEntradaDTO entradaDTO = new BloquearDesbloquearContratosEntradaDTO();
			
			entradaDTO.setCdMotivoBloqueioContrato(this.getMotivoBloqueioFiltro());
			entradaDTO.setCdPessoaJuridica(this.getCdPessoaJuridica());
			entradaDTO.setCdSituacaoContrato(this.getBloqDesbloqFiltro());
			entradaDTO.setCdTipoContratoNegocio(this.getCdTipoContrato());
			entradaDTO.setIndicadorBloqueioDesbloqueioContrato(this.getBloqDesbloqContratosEspelhadosFiltro() == 1 ? "S" : "N");
			entradaDTO.setNrSequenciaContratoNegocio(Long.valueOf(this.getNumeroContrato()));
			
			BloquearDesbloquearContratosSaidaDTO saidaDTO = getBloquearDesbloquearContratosServiceImpl().bloquearDesbloquearContratos(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), 
					"conBloquearDesbloquearContratos", "#{bloqDesbloqContratosBean.consultarAposAutorizar}",false);
			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";		
	}
	
	/**
    * Consultar apos autorizar.
    *
    * @param event the event
    * @return the string
    */
   public String consultarAposAutorizar(ActionEvent event) {
       try {
    	   identificacaoClienteContratoBean.consultarContrato();
       } catch (PdcAdapterFunctionalException e) {
           BradescoFacesUtils.addErrorModalMessage("(" + MessageUtils.getMessageCode(e) + ") " + e.getMessage());
           return "";
       }
       return "conBloquearDesbloquearContratos";
   }
	
	/**
	 * Bloquear desbloquear pesquisar.
	 *
	 * @return the string
	 */
	public String bloquearDesbloquearPesquisar(){
		limparFiltros();
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtivEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setCdTipoContrato(saidaDTO.getCdTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivosContrato(saidaDTO.getCdAditivo());
		//setDataHoraCadastramento(null);
		setDescricaoContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		if (getDataHoraCadContrato()!=null ){
			
			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			
			String stringData = getDataHoraCadContrato().substring(0, 19);
			
			try {
				setDataHoraCadContrato(formato2.format(formato1.parse(stringData)));
			}  catch (ParseException e) {
				setDataHoraCadContrato("");
			}
			
		}
		
		//setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		}

		setListaMotivoBloqueio(new ArrayList<SelectItem>());
		setBloqDesbloqContratosEspelhadosFiltro(null);
		setBtnAvancar(false);
		return "AVANCAR";
	}
		
	
	/**
	 * Limpar filtros.
	 */
	public void limparFiltros(){
		setBloqDesbloqContratosEspelhadosFiltro(null);
		setBloqDesbloqFiltro(null);
		setMotivoBloqueioFiltro(null);
		setBtnAvancar(false);
		setListaMotivoBloqueio(new ArrayList<SelectItem>());
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		setBloqDesbloqContratosEspelhadosFiltro(null);
		setBloqDesbloqFiltro(null);
		setMotivoBloqueioFiltro(null);
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteBloqDesbloqContratos");
		identificacaoClienteContratoBean.setPaginaRetorno("conBloquearDesbloquearContratos");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();	
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		
		
	}
	
	/**
	 * Limpar combo.
	 */
	public void limparCombo(){
		setMotivoBloqueioFiltro(0);
	}
	
	/**
	 * Limpar dados argumentos pesquisa.
	 */
	public void limparDadosArgumentosPesquisa(){
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(0l);
		identificacaoClienteContratoBean.setTipoContratoFiltro(0);
		identificacaoClienteContratoBean.setNumeroFiltro("");
		identificacaoClienteContratoBean.setAgenciaOperadoraFiltro("");
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.setCodigoFunc("");
		identificacaoClienteContratoBean.setSituacaoFiltro(0);		
		identificacaoClienteContratoBean.listarMotivoSituacao();
		identificacaoClienteContratoBean.setHabilitaCampos(false);
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setListaGridPesquisa(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setEmpresaGestoraContrato(null);
		setNumeroContrato(null);
		setDescricaoContrato(null);
		setSituacaoContrato(null);
		identificacaoClienteContratoBean.limparRadioFiltro();
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(false);
	}
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
		if (identificacaoClienteContratoBean.getObrigatoriedade() != null && identificacaoClienteContratoBean.getObrigatoriedade().equals("T")){

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

			try{
				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()!=null&&!identificacaoClienteContratoBean.getAgenciaOperadoraFiltro().equals("")?Integer.parseInt(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()):0);
				entradaDTO.setCdClubPessoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub()!=null?identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub():0L);
				entradaDTO.setCdFuncionario(identificacaoClienteContratoBean.getCodigoFunc()!=null&&!identificacaoClienteContratoBean.getCodigoFunc().equals("")?Long.parseLong(identificacaoClienteContratoBean.getCodigoFunc()):0L);				
				entradaDTO.setCdPessoaJuridica(identificacaoClienteContratoBean.getEmpresaGestoraFiltro()!=null?identificacaoClienteContratoBean.getEmpresaGestoraFiltro():0);
				entradaDTO.setCdSituacaoContrato(identificacaoClienteContratoBean.getSituacaoFiltro() != null ? identificacaoClienteContratoBean.getSituacaoFiltro() : 0);
				entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro()!=null?identificacaoClienteContratoBean.getTipoContratoFiltro():0);
				entradaDTO.setNrSequenciaContrato(identificacaoClienteContratoBean.getNumeroFiltro()!=null&&!identificacaoClienteContratoBean.getNumeroFiltro().equals("")?Long.valueOf(identificacaoClienteContratoBean.getNumeroFiltro()):Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdCpfCnpj());
				entradaDTO.setCdFilialCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdFilialCnpj());
				entradaDTO.setCdControleCpfPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdControleCnpj());
				
				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if(identificacaoClienteContratoBean.getPaginaRetorno().equals("conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqManterSolicitacoesMudancaAmbienteOperacao")) {
					entradaDTO.setParametroPrograma(6);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")){
					List<ListarContratosPgitSaidaDTO> listaGridTemp = identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO);

					identificacaoClienteContratoBean.setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());

					for (ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO : listaGridTemp) {
						if(listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 1 || listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 3){
							identificacaoClienteContratoBean.getListaGridPesquisa().add(listarContratosPgitSaidaDTO);
						}
					}
				}
				else{
					identificacaoClienteContratoBean.setSaidaContrato(new ListarContratosPgitSaidaDTO());
					identificacaoClienteContratoBean.setListaGridPesquisa(identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO));
					identificacaoClienteContratoBean.setSaidaContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(0));
					
				}

				this.identificacaoClienteContratoBean.setListaControleRadio(new ArrayList<SelectItem>());
				for (int i = 0; i < identificacaoClienteContratoBean.getListaGridPesquisa().size();i++) {
					this.identificacaoClienteContratoBean.getListaControleRadio().add(new SelectItem(i," "));
				}
				bloquearArgumentosPesquisaAposConsulta();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				identificacaoClienteContratoBean.setListaGridPesquisa(null);

			}

			return identificacaoClienteContratoBean.getPaginaRetorno();
		}
		
		return "";
	}
	
	/**
	 * Bloquear argumentos pesquisa apos consulta.
	 */
	public void bloquearArgumentosPesquisaAposConsulta(){
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}
	
	
	
	
	
	

	/**
	 * Get: agenciaConta.
	 *
	 * @return agenciaConta
	 */
	public String getAgenciaConta() {
		return agenciaConta;
	}

	/**
	 * Set: agenciaConta.
	 *
	 * @param agenciaConta the agencia conta
	 */
	public void setAgenciaConta(String agenciaConta) {
		this.agenciaConta = agenciaConta;
	}

	/**
	 * Get: ativEconomicaMaster.
	 *
	 * @return ativEconomicaMaster
	 */
	public String getAtivEconomicaMaster() {
		return ativEconomicaMaster;
	}

	/**
	 * Set: ativEconomicaMaster.
	 *
	 * @param ativEconomicaMaster the ativ economica master
	 */
	public void setAtivEconomicaMaster(String ativEconomicaMaster) {
		this.ativEconomicaMaster = ativEconomicaMaster;
	}

	/**
	 * Get: bancoConta.
	 *
	 * @return bancoConta
	 */
	public String getBancoConta() {
		return bancoConta;
	}

	/**
	 * Set: bancoConta.
	 *
	 * @param bancoConta the banco conta
	 */
	public void setBancoConta(String bancoConta) {
		this.bancoConta = bancoConta;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: cpfCnpjCliente.
	 *
	 * @return cpfCnpjCliente
	 */
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}

	/**
	 * Set: cpfCnpjCliente.
	 *
	 * @param cpfCnpjCliente the cpf cnpj cliente
	 */
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Get: dataHoraCadContrato.
	 *
	 * @return dataHoraCadContrato
	 */
	public String getDataHoraCadContrato() {
		return dataHoraCadContrato;
	}

	/**
	 * Set: dataHoraCadContrato.
	 *
	 * @param dataHoraCadContrato the data hora cad contrato
	 */
	public void setDataHoraCadContrato(String dataHoraCadContrato) {
		this.dataHoraCadContrato = dataHoraCadContrato;
	}

	/**
	 * Get: dataVinculacaoConta.
	 *
	 * @return dataVinculacaoConta
	 */
	public String getDataVinculacaoConta() {
		return dataVinculacaoConta;
	}

	/**
	 * Set: dataVinculacaoConta.
	 *
	 * @param dataVinculacaoConta the data vinculacao conta
	 */
	public void setDataVinculacaoConta(String dataVinculacaoConta) {
		this.dataVinculacaoConta = dataVinculacaoConta;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Get: inicioVigenciaContrato.
	 *
	 * @return inicioVigenciaContrato
	 */
	public String getInicioVigenciaContrato() {
		return inicioVigenciaContrato;
	}

	/**
	 * Set: inicioVigenciaContrato.
	 *
	 * @param inicioVigenciaContrato the inicio vigencia contrato
	 */
	public void setInicioVigenciaContrato(String inicioVigenciaContrato) {
		this.inicioVigenciaContrato = inicioVigenciaContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: nomeRazaoCliente.
	 *
	 * @return nomeRazaoCliente
	 */
	public String getNomeRazaoCliente() {
		return nomeRazaoCliente;
	}

	/**
	 * Set: nomeRazaoCliente.
	 *
	 * @param nomeRazaoCliente the nome razao cliente
	 */
	public void setNomeRazaoCliente(String nomeRazaoCliente) {
		this.nomeRazaoCliente = nomeRazaoCliente;
	}

	/**
	 * Get: nomeRazaoMaster.
	 *
	 * @return nomeRazaoMaster
	 */
	public String getNomeRazaoMaster() {
		return nomeRazaoMaster;
	}

	/**
	 * Set: nomeRazaoMaster.
	 *
	 * @param nomeRazaoMaster the nome razao master
	 */
	public void setNomeRazaoMaster(String nomeRazaoMaster) {
		this.nomeRazaoMaster = nomeRazaoMaster;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: possuiAditivosContrato.
	 *
	 * @return possuiAditivosContrato
	 */
	public String getPossuiAditivosContrato() {
		return possuiAditivosContrato;
	}

	/**
	 * Set: possuiAditivosContrato.
	 *
	 * @param possuiAditivosContrato the possui aditivos contrato
	 */
	public void setPossuiAditivosContrato(String possuiAditivosContrato) {
		this.possuiAditivosContrato = possuiAditivosContrato;
	}

	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Get: situacaoConta.
	 *
	 * @return situacaoConta
	 */
	public String getSituacaoConta() {
		return situacaoConta;
	}

	/**
	 * Set: situacaoConta.
	 *
	 * @param situacaoConta the situacao conta
	 */
	public void setSituacaoConta(String situacaoConta) {
		this.situacaoConta = situacaoConta;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Get: tipoConta.
	 *
	 * @return tipoConta
	 */
	public String getTipoConta() {
		return tipoConta;
	}

	/**
	 * Set: tipoConta.
	 *
	 * @param tipoConta the tipo conta
	 */
	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: tipoVinculoConta.
	 *
	 * @return tipoVinculoConta
	 */
	public String getTipoVinculoConta() {
		return tipoVinculoConta;
	}

	/**
	 * Set: tipoVinculoConta.
	 *
	 * @param tipoVinculoConta the tipo vinculo conta
	 */
	public void setTipoVinculoConta(String tipoVinculoConta) {
		this.tipoVinculoConta = tipoVinculoConta;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: listaMotivoBloqueio.
	 *
	 * @return listaMotivoBloqueio
	 */
	public List<SelectItem> getListaMotivoBloqueio() {
		return listaMotivoBloqueio;
	}

	/**
	 * Set: listaMotivoBloqueio.
	 *
	 * @param listaMotivoBloqueio the lista motivo bloqueio
	 */
	public void setListaMotivoBloqueio(List<SelectItem> listaMotivoBloqueio) {
		this.listaMotivoBloqueio = listaMotivoBloqueio;
	}

	/**
	 * Get: listaMotivoBloqueioDesc.
	 *
	 * @return listaMotivoBloqueioDesc
	 */
	public String getListaMotivoBloqueioDesc() {
		return listaMotivoBloqueioDesc;
	}

	/**
	 * Set: listaMotivoBloqueioDesc.
	 *
	 * @param listaMotivoBloqueioDesc the lista motivo bloqueio desc
	 */
	public void setListaMotivoBloqueioDesc(String listaMotivoBloqueioDesc) {
		this.listaMotivoBloqueioDesc = listaMotivoBloqueioDesc;
	}

	/**
	 * Get: listaMotivoBloqueioHash.
	 *
	 * @return listaMotivoBloqueioHash
	 */
	public Map<Integer, String> getListaMotivoBloqueioHash() {
		return listaMotivoBloqueioHash;
	}

	/**
	 * Set lista motivo bloqueio hash.
	 *
	 * @param listaMotivoBloqueioHash the lista motivo bloqueio hash
	 */
	public void setListaMotivoBloqueioHash(
			Map<Integer, String> listaMotivoBloqueioHash) {
		this.listaMotivoBloqueioHash = listaMotivoBloqueioHash;
	}

	/**
	 * Is btn avancar.
	 *
	 * @return true, if is btn avancar
	 */
	public boolean isBtnAvancar() {
		return btnAvancar;
	}

	/**
	 * Set: btnAvancar.
	 *
	 * @param btnAvancar the btn avancar
	 */
	public void setBtnAvancar(boolean btnAvancar) {
		this.btnAvancar = btnAvancar;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: bloquearDesbloquearContratosServiceImpl.
	 *
	 * @return bloquearDesbloquearContratosServiceImpl
	 */
	public IBloquearDesbloquearContratosService getBloquearDesbloquearContratosServiceImpl() {
		return bloquearDesbloquearContratosServiceImpl;
	}

	/**
	 * Set: bloquearDesbloquearContratosServiceImpl.
	 *
	 * @param bloquearDesbloquearContratosServiceImpl the bloquear desbloquear contratos service impl
	 */
	public void setBloquearDesbloquearContratosServiceImpl(
			IBloquearDesbloquearContratosService bloquearDesbloquearContratosServiceImpl) {
		this.bloquearDesbloquearContratosServiceImpl = bloquearDesbloquearContratosServiceImpl;
	}

	/**
	 * Get: gerenteResponsavel.
	 *
	 * @return gerenteResponsavel
	 */
	public String getGerenteResponsavel() {
		return gerenteResponsavel;
	}

	/**
	 * Set: gerenteResponsavel.
	 *
	 * @param gerenteResponsavel the gerente responsavel
	 */
	public void setGerenteResponsavel(String gerenteResponsavel) {
		this.gerenteResponsavel = gerenteResponsavel;
	}

	/**
	 * Get: clienteParticipante.
	 *
	 * @return clienteParticipante
	 */
	public String getClienteParticipante() {
		return clienteParticipante;
	}

	/**
	 * Set: clienteParticipante.
	 *
	 * @param clienteParticipante the cliente participante
	 */
	public void setClienteParticipante(String clienteParticipante) {
		this.clienteParticipante = clienteParticipante;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}	
	
}
