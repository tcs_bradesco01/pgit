/**
 * Nome: br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicretransmissaoarquivosretorno
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicretransmissaoarquivosretorno;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.IManterSolicRetransmissaoArquivosRetornoService;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.DetalharSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.DetalharSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ExcluirSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ExcluirSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.IncluirSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.IncluirSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArqRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArqRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.ListarSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicretransmissaoarquivosretorno.bean.OcorrenciasIncluirSolBaseRetransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.utils.CamposUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterSolicRetransmissaoArquivosRetornoBean
 * <p>
 * Prop�sito: Implementa��o do Bean ManterSolicRetransmissaoArquivosRetornoBean
 * </p>
 * 
 * @author todo! - Solu��es em Tecnologia / TI Melhorias - Arquitetura
 * @version 1.0
 */
public class ManterSolicRetransmissaoArquivosRetornoBean {

	/** Atributo consultasService. */
	private IConsultasService consultasService;
	
	/** Atributo solEmissaoComprovantePagtoClientePagServiceImpl. */
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo disableArgumentosIncluir. */
	private boolean disableArgumentosIncluir;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterSolicRetransmissaoArquivosRetornoServiceImpl. */
	private IManterSolicRetransmissaoArquivosRetornoService manterSolicRetransmissaoArquivosRetornoServiceImpl;
	
	/** Atributo manterSolicitacaoRastFavService. */
	private IManterSolicitacaoRastFavService manterSolicitacaoRastFavService;

	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo cboSituacaoSolicitacao. */
	private Integer cboSituacaoSolicitacao;
	
	/** Atributo cboOrigemSolicitacao. */
	private Integer cboOrigemSolicitacao;
	
	/** Atributo dataSolicitacaoDe. */
	private Date dataSolicitacaoDe;
	
	/** Atributo dataSolicitacaoAte. */
	private Date dataSolicitacaoAte;
	
	/** Atributo numeroSolicitacao. */
	private Integer numeroSolicitacao;
	
	/** Atributo listaSituacaoSolicitacao. */
	private List<SelectItem> listaSituacaoSolicitacao;
	
	/** Atributo atributoSoltc. */
	private VerificaraAtributoSoltcSaidaDTO atributoSoltc;

	/** Atributo listaGridConsultar. */
	private List<ListarSolBaseRetransmissaoSaidaDTO> listaGridConsultar;
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;
	
	/** Atributo listaGridControleRadio. */
	private List<SelectItem> listaGridControleRadio;

	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo listaDetalharSolBaseRetransmissao. */
	private List<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO> listaDetalharSolBaseRetransmissao;

	/** Atributo dsNumeroSolicitacao. */
	private Integer dsNumeroSolicitacao;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;
	
	/** Atributo dataHoraAtendimento. */
	private String dataHoraAtendimento;
	
	/** Atributo qtdeRegistros. */
	private String qtdeRegistros;

	/** Atributo vlTarifaPadrao. */
	private BigDecimal vlTarifaPadrao;
	
	/** Atributo vlDescontoTarifa. */
	private BigDecimal vlDescontoTarifa;
	
	/** Atributo vlTarifaAtualizada. */
	private BigDecimal vlTarifaAtualizada;
	// Trilha Auditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo cboLayoutArquivo. */
	private Integer cboLayoutArquivo;
	
	/** Atributo dataSolicitacaoDeRetorno. */
	private Date dataSolicitacaoDeRetorno;
	
	/** Atributo dataSolicitacaoAteRetorno. */
	private Date dataSolicitacaoAteRetorno;
	
	/** Atributo sequenciaRetorno. */
	private String sequenciaRetorno;
	
	/** Atributo listaSituacaoSolicitacaoRetorno. */
	private List<SelectItem> listaSituacaoSolicitacaoRetorno;
	
	/** Atributo listarTipoLayout. */
	private List<SelectItem> listarTipoLayout;

	/** Atributo listaGridConsultarIncluir. */
	private List<ListarArqRetransmissaoSaidaDTO> listaGridConsultarIncluir;
	
	/** Atributo itemSelecionadoIncluir. */
	private Integer itemSelecionadoIncluir;
	
	/** Atributo listaGridControleIncluir. */
	private List<SelectItem> listaGridControleIncluir;
	
	/** Atributo listaIncluirSelecionados. */
	private List<ListarArqRetransmissaoSaidaDTO> listaIncluirSelecionados;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos;
	
	/** Atributo panelBotoes. */
	private boolean panelBotoes;

	/** Atributo indicadorDescontoBloqueio. */
	private boolean indicadorDescontoBloqueio;

	/**
	 * Inicializa tela.
	 *
	 * @param evt the evt
	 */
	public void inicializaTela(ActionEvent evt) {

		limparTela();

		// Carregamentos dos Combos
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();

		// Tela Filtro
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setPanelBotoes(false);
		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		filtroAgendamentoEfetivacaoEstornoBean
				.setEmpresaGestoraFiltro(2269651L);

		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno("conManterSolicRetransmissaoArquivosRetorno");
		carregaListaSituacaoSolicitacao();
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar radios principais.
	 */
	public void limparRadiosPrincipais() {
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);

		return "";

	}

	/**
	 * Limpar radios principais incluir.
	 */
	public void limparRadiosPrincipaisIncluir() {
		limparCamposIncluir();
		setListaGridConsultarIncluir(null);
		setItemSelecionadoIncluir(null);
		setOpcaoChecarTodos(false);
	}

	/**
	 * Limpar apos alterar opcao cliente incluir.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoClienteIncluir() {
		// Limpar Argumentos de Pesquisa e Lista
		limparCamposIncluir();
		setListaGridConsultarIncluir(null);
		setItemSelecionadoIncluir(null);
		setOpcaoChecarTodos(false);

		return "";

	}

	/**
	 * Limpar campos consultar.
	 */
	public void limparCamposConsultar() {
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setNumeroSolicitacao(null);
		setCboSituacaoSolicitacao(0);
		setCboOrigemSolicitacao(0);
	}

	/**
	 * Limpar campos incluir.
	 */
	public void limparCamposIncluir() {
		setDataSolicitacaoDeRetorno(new Date());
		setDataSolicitacaoAteRetorno(new Date());
		setSequenciaRetorno(null);
		setCboLayoutArquivo(0);
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato consultar.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContratoConsultar(
			ActionEvent evt) {
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionado(null);

	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato incluir.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContratoIncluir(
			ActionEvent evt) {
		limparCamposIncluir();
		setListaGridConsultarIncluir(null);
		setItemSelecionadoIncluir(null);
		setOpcaoChecarTodos(false);
	}

	/**
	 * Limpar tela.
	 */
	public void limparTela() {

		limparCamposConsultar();
		limparRadiosPrincipais();
		setListaGridConsultar(null);
		setListaGridControleRadio(null);
		setItemSelecionado(null);
		setHabilitaArgumentosPesquisa(false);
		carregaListaSituacaoSolicitacao();
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar grid consultar.
	 *
	 * @return the string
	 */
	public String limparGridConsultar() {
		setListaGridConsultar(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Limpar grid incluir.
	 *
	 * @return the string
	 */
	public String limparGridIncluir() {

		setListaGridConsultarIncluir(null);
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		setDisableArgumentosIncluir(false);

		vlDescontoTarifa = BigDecimal.ZERO;

		if (indicadorDescontoBloqueio) {
			vlTarifaAtualizada = vlTarifaPadrao;
		} else {
			vlTarifaAtualizada = BigDecimal.ZERO;
		}

		return "";
	}

	/**
	 * Limpar tela incluir.
	 */
	public void limparTelaIncluir() {

		limparCamposIncluir();
		limparRadiosPrincipaisIncluir();
		setListaGridConsultarIncluir(null);
		setListaGridControleIncluir(null);
		setItemSelecionadoIncluir(null);
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		carregaListaSituacaoRetorno();
		// combo layout
		carregaListaSituacaoRetorno();
		carregaListaTipoLayout();
		setDisableArgumentosIncluir(false);

		vlDescontoTarifa = BigDecimal.ZERO;

		if (indicadorDescontoBloqueio) {
			vlTarifaAtualizada = vlTarifaPadrao;
		} else {
			vlTarifaAtualizada = BigDecimal.ZERO;
		}
	}

	/**
	 * Carrega lista situacao solicitacao.
	 */
	public void carregaListaSituacaoSolicitacao() {

		try {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();

			List<ListarSituacaoSolicitacaoSaidaDTO> listaSaida = comboService
					.listarSituacaoSolicitacao();

			for (int i = 0; i < listaSaida.size(); i++) {
				listaSituacaoSolicitacao.add(new SelectItem(listaSaida.get(i)
						.getCdSolicitacaoPagamentoIntegrado(), listaSaida
						.get(i).getDsTipoSolicitacaoPagamento()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Carrega lista situacao retorno.
	 */
	public void carregaListaSituacaoRetorno() {

		try {
			listaSituacaoSolicitacaoRetorno = new ArrayList<SelectItem>();

			List<ListarSituacaoRetornoSaidaDTO> listaSaida = comboService
					.listarSituacaoRetorno();

			for (int i = 0; i < listaSaida.size(); i++) {
				listaSituacaoSolicitacaoRetorno.add(new SelectItem(listaSaida
						.get(i).getCodigo(), listaSaida.get(i).getDescricao()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listaSituacaoSolicitacaoRetorno = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Carrega lista tipo layout.
	 */
	public void carregaListaTipoLayout() {

		try {
			listarTipoLayout = new ArrayList<SelectItem>();

			List<ListarTipoLayoutSaidaDTO> listaSaida = comboService
					.listarTipoLayout();

			for (int i = 0; i < listaSaida.size(); i++) {
				listarTipoLayout.add(new SelectItem(listaSaida.get(i)
						.getCdTipoLayoutArquivo(), listaSaida.get(i)
						.getDsTipoLayoutArquivo()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listarTipoLayout = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Consultar.
	 */
	public void consultar() {

		setItemSelecionado(null);
		setListaGridControleRadio(null);

		listaGridConsultar = new ArrayList<ListarSolBaseRetransmissaoSaidaDTO>();
		try {
			ListarSolBaseRetransmissaoEntradaDTO entradaDTO = new ListarSolBaseRetransmissaoEntradaDTO();
			entradaDTO
					.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());
			entradaDTO
					.setDtSolicitacaoInicio((getDataSolicitacaoDe() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoDe())));
			entradaDTO
					.setDtSolicitacaoFim((getDataSolicitacaoAte() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoAte())));
			entradaDTO.setNrSolicitacaoPagamento(getNumeroSolicitacao());
			entradaDTO.setCdSituacaoSolicitacao(getCboSituacaoSolicitacao());
			entradaDTO.setCdOrigemSolicitacao(getCboOrigemSolicitacao());

			setListaGridConsultar(getManterSolicRetransmissaoArquivosRetornoServiceImpl()
					.listarSolBaseRetransmissao(entradaDTO));
			listaGridControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				listaGridControleRadio.add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridConsultar(null);
			setItemSelecionado(null);
			setDisableArgumentosConsulta(false);
		}
	}

	/**
	 * Consultar incluir.
	 */
	public void consultarIncluir() {

		setItemSelecionadoIncluir(null);
		setListaGridControleIncluir(null);

		listaGridConsultarIncluir = new ArrayList<ListarArqRetransmissaoSaidaDTO>();

		try {

			ListarArqRetransmissaoEntradaDTO entradaDTO = new ListarArqRetransmissaoEntradaDTO();

			entradaDTO.setCdPessoa(0L);
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO
					.setDtInicioGeracaoRetorno((getDataSolicitacaoDeRetorno() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoDeRetorno())));
			entradaDTO
					.setDtFimGeracaoRetorno((getDataSolicitacaoAteRetorno() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoAteRetorno())));
			entradaDTO.setCdTipoLayoutArquivo(getCboLayoutArquivo());
			entradaDTO.setNrArquivoRetorno(getSequenciaRetorno() != null
					&& !getSequenciaRetorno().equals("") ? Long
					.parseLong(getSequenciaRetorno()) : 0L);
			entradaDTO.setCdSituacaoProcessamentoRetorno(2);

			setListaGridConsultarIncluir(getManterSolicRetransmissaoArquivosRetornoServiceImpl()
					.listarArquivoRetransmissao(entradaDTO));
			listaGridControleIncluir = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridConsultarIncluir().size(); i++) {
				listaGridControleIncluir.add(new SelectItem(i, " "));
			}
			setOpcaoChecarTodos(false);
			setDisableArgumentosIncluir(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridControleIncluir(null);
			setItemSelecionadoIncluir(null);
			setDisableArgumentosIncluir(false);
		}
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		calcularTarifaAtualizada();

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());
		preencheListaIncluirSelecionados();

		return "AVANCAR_INCLUIR";
	}

	/**
	 * Preenche lista incluir selecionados.
	 */
	public void preencheListaIncluirSelecionados() {
		listaIncluirSelecionados = new ArrayList<ListarArqRetransmissaoSaidaDTO>();
		
		if (getListaGridConsultarIncluir() != null) {
			for (int i = 0; i < getListaGridConsultarIncluir().size(); i++) {
				if (getListaGridConsultarIncluir().get(i).isCheck()) {
					listaIncluirSelecionados.add(getListaGridConsultarIncluir()
							.get(i));
				}
			}			
		}
	}

	/**
	 * Confirmar incluir.
	 */
	public void confirmarIncluir() {
		try {

			IncluirSolBaseRetransmissaoEntradaDTO entradaDTO = new IncluirSolBaseRetransmissaoEntradaDTO();
			List<OcorrenciasIncluirSolBaseRetransmissaoEntradaDTO> listaEntrada = new ArrayList<OcorrenciasIncluirSolBaseRetransmissaoEntradaDTO>();

			for (ListarArqRetransmissaoSaidaDTO ocorrencia : getListaIncluirSelecionados()) {

				OcorrenciasIncluirSolBaseRetransmissaoEntradaDTO ocorrenciasDTO = new OcorrenciasIncluirSolBaseRetransmissaoEntradaDTO();

				ocorrenciasDTO.setCdClienteTransferenciaArquivo(ocorrencia
						.getCdClienteTransferenciaArquivo());
				ocorrenciasDTO.setCdPessoa(ocorrencia.getCdPessoa());
				ocorrenciasDTO.setCdTipoLayoutArquivo(ocorrencia
						.getCdTipoLayoutArquivo());
				ocorrenciasDTO.setHrInclusaoArquivoRetorno(ocorrencia
						.getHrInclusao());
				ocorrenciasDTO.setNrSequenciaArquivoRetorno(ocorrencia
						.getNrArquivoRetorno());
				ocorrenciasDTO.setDsArquivoRetorno(PgitUtil.verificaStringNula(
						ocorrencia.getDsArquivoRetorno()).substring(0, 2));
				listaEntrada.add(ocorrenciasDTO);
			}

			entradaDTO
					.setListaOcorrenciasIncluirSolBaseRetransmissao(listaEntrada);
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setVlTarifaPadrao(vlTarifaPadrao);
			entradaDTO.setNumPercentualDescTarifa(vlDescontoTarifa);
			entradaDTO.setVlrTarifaAtual(vlTarifaAtualizada);

			entradaDTO
					.setListaOcorrenciasIncluirSolBaseRetransmissao(listaEntrada);
			
			if (!listaEntrada.isEmpty()) {
				entradaDTO.setNumeroLinhas(listaEntrada.size());				
			}

			IncluirSolBaseRetransmissaoSaidaDTO saidaDTO = getManterSolicRetransmissaoArquivosRetornoServiceImpl()
					.incluirSolBaseRetransmissao(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conManterSolicRetransmissaoArquivosRetorno",
					BradescoViewExceptionActionType.ACTION, false);

			if ((listaGridConsultar != null) && (!listaGridConsultar.isEmpty())) {
				consultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Carrega lista detalhar excluir.
	 */
	public void carregaListaDetalharExcluir() {
		listaDetalharSolBaseRetransmissao = new ArrayList<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO>();
		// Registro do da Tela Anterior
		ListarSolBaseRetransmissaoSaidaDTO registroSelecionado = getListaGridConsultar()
				.get(getItemSelecionado());

		try {
			DetalharSolBaseRetransmissaoEntradaDTO entradaDTO = new DetalharSolBaseRetransmissaoEntradaDTO();

			entradaDTO.setNrSolicitacaoPagamento(registroSelecionado
					.getNrSolicitacaoPagamento());

			setListaDetalharSolBaseRetransmissao(getManterSolicRetransmissaoArquivosRetornoServiceImpl()
					.detalharSolBaseRetransmissao(entradaDTO)
					.getListaDetalharSolBaseRetransmissao());
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaDetalharSolBaseRetransmissao(null);
		}
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {

		carregaCabecalho();

		listaDetalharSolBaseRetransmissao = new ArrayList<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO>();

		// Registro do da Tela Anterior
		ListarSolBaseRetransmissaoSaidaDTO registroSelecionado = getListaGridConsultar()
				.get(getItemSelecionado());

		try {
			DetalharSolBaseRetransmissaoEntradaDTO entradaDTO = new DetalharSolBaseRetransmissaoEntradaDTO();
			DetalharSolBaseRetransmissaoSaidaDTO saidaDTO = new DetalharSolBaseRetransmissaoSaidaDTO();

			entradaDTO.setNrSolicitacaoPagamento(registroSelecionado
					.getNrSolicitacaoPagamento());

			saidaDTO = getManterSolicRetransmissaoArquivosRetornoServiceImpl()
					.detalharSolBaseRetransmissao(entradaDTO);

			setDsNumeroSolicitacao(saidaDTO.getNrSolicitacao());
			setDataHoraSolicitacao(saidaDTO.getHrSolicitacaoFormatada());
			setSituacao(saidaDTO.getDsSituacaoRecuperacao());
			setDataHoraAtendimento(saidaDTO
					.getHrAtendimentoSolicitacaoFormatada());
			setResultadoProcessamento(saidaDTO.getDsMotivoSituacaoRecuperacao());
			setQtdeRegistros(saidaDTO.getQtdeRegistrosFormatada());
			setListaDetalharSolBaseRetransmissao(saidaDTO
					.getListaDetalharSolBaseRetransmissao());
			setVlTarifaPadrao(saidaDTO.getVlrTarifaPadrao());
			setVlDescontoTarifa(saidaDTO.getDescontoTarifa());
			setVlTarifaAtualizada(saidaDTO.getVlrTarifaAtual());

			// Trilha Auditoria
			setDataHoraInclusao(saidaDTO.getHrInclusaoRegistroFormatada());
			setUsuarioInclusao(saidaDTO.getCdAutenticacaoSegurancaInclusao());
			setTipoCanalInclusao(CamposUtils.formatadorCampos(saidaDTO
					.getCdCanalInclusao(), saidaDTO.getDsCanalInclusao()));
			setComplementoInclusao(saidaDTO.getNmOperacaoFluxoInclusao());
			setDataHoraManutencao(saidaDTO.getHrManutencaoRegistroFormatada());
			setUsuarioManutencao(saidaDTO
					.getCdAutenticacaoSegurancaManutencao());
			setTipoCanalManutencao(CamposUtils.formatadorCampos(saidaDTO
					.getCdCanalManutencao(), saidaDTO.getDsCanalManutencao()));
			setComplementoManutencao(saidaDTO.getNmOperacaoFluxoManutencao());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaDetalharSolBaseRetransmissao(null);
			return "";
		}
		return "DETALHAR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		carregaCabecalho();
		String retornoDetalhar = detalhar();
		return retornoDetalhar != null && !retornoDetalhar.equals("") ? "EXCLUIR"
				: "";
	}

	/**
	 * Confirmar excluir.
	 */
	public void confirmarExcluir() {

		try {

			ExcluirSolBaseRetransmissaoEntradaDTO entradaDTO = new ExcluirSolBaseRetransmissaoEntradaDTO();

			ListarSolBaseRetransmissaoSaidaDTO registroSelecionado = getListaGridConsultar()
					.get(getItemSelecionado());

			entradaDTO.setNrSolicitacaoGeracaoRetorno(registroSelecionado
					.getNrSolicitacaoPagamento());
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());

			ExcluirSolBaseRetransmissaoSaidaDTO saidaDTO = getManterSolicRetransmissaoArquivosRetornoServiceImpl()
					.excluirSolBaseRetransmissao(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conManterSolicRetransmissaoArquivosRetorno",
					"#{manterSolicRetransmissaoArquivosRetornoBean.consultar}",
					false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Pesquisar.
	 */
	public void pesquisar() {

		setOpcaoChecarTodos(false);

		for (int i = 0; i < getListaGridConsultarIncluir().size(); i++) {
			getListaGridConsultarIncluir().get(i).setCheck(false);
		}
	}

	/**
	 * Pesquisar consulta.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsulta(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Pesquisar excluir detalhar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarExcluirDetalhar(ActionEvent evt) {
		carregaListaDetalharExcluir();
		return "";
	}

	/**
	 * Pesquisar incluir.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarIncluir(ActionEvent evt) {
		consultarIncluir();
		return "";
	}

	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados() {
		for (int i = 0; i < getListaGridConsultarIncluir().size(); i++) {
			getListaGridConsultarIncluir().get(i).setCheck(false);
		}
		setOpcaoChecarTodos(false);
	}

	/**
	 * Carrega cabecalho.
	 */
	public void carregaCabecalho() {
		limparCabecalho();
		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()
				.equals("0")) { // Filtrar por cliente
			setNrCnpjCpf(String.valueOf(filtroAgendamentoEfetivacaoEstornoBean
					.getNrCpfCnpjCliente()));
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoRazaoSocialCliente());
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescCliente());
		} else {
			if (filtroAgendamentoEfetivacaoEstornoBean
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				try {
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO
							.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
									.getEmpresaGestoraFiltro());
					entradaDTO
							.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getCdTipoContratoNegocio());
					entradaDTO
							.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getNrSequenciaContratoNegocio());
					DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
							.detalharDadosContrato(entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException p) {
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
						.getEmpresaGestoraDescContrato());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getNumeroDescContrato());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getSituacaoDescContrato());
			}
		}
	}

	/**
	 * Limpar cabecalho.
	 */
	private void limparCabecalho() {
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		setItemSelecionado(null);

		return "VOLTAR_CONSULTAR";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		vlTarifaPadrao = BigDecimal.ZERO;
		atributoSoltc = manterSolicitacaoRastFavService
				.verificaraAtributoSoltc(new VerificaraAtributoSoltcEntradaDTO(
						5));

		try {
			carregaCabecalho();
			consultarAgenciaOpeTarifaPadrao();
			limparTelaIncluir();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "INCLUIR";
	}
	
	public boolean isDesabilitaCampoDescontoTarifa(){		
		
		if (indicadorDescontoBloqueio) {
			setVlDescontoTarifa(new BigDecimal(0));
			return true;
		}		
		
		return false;
	}

	/**
	 * Nome: calcularTarifaAtualizada
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void calcularTarifaAtualizada() {
		vlTarifaAtualizada = vlTarifaPadrao.subtract(vlTarifaPadrao.multiply(
				vlDescontoTarifa.divide(new BigDecimal(100))).setScale(2,
				BigDecimal.ROUND_HALF_UP));
		setPanelBotoes(true);
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		setOpcaoChecarTodos(false);
		
		if (getListaGridConsultarIncluir() != null) {
			for (int i = 0; i < getListaGridConsultarIncluir().size(); i++) {
				getListaGridConsultarIncluir().get(i).setCheck(false);
			}			
		}		

		setPanelBotoes(false);

		return "VOLTAR_INCLUIR";
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		for (int i = 0; i < getListaGridConsultarIncluir().size(); i++) {
			if (getListaGridConsultarIncluir().get(i).isCheck()) {
				setPanelBotoes(true);
				return;
			}
		}
		setPanelBotoes(false);
	}

	/**
	 * Nome: consultarAgenciaOpeTarifaPadrao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void consultarAgenciaOpeTarifaPadrao() {
		try {
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaEmpresa(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setCdProdutoServicoOperacao(0);
			entradaDTO.setCdProdutoServicoRelacionado(0);
			entradaDTO.setCdTipoTarifa(11);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl()
					.consultarAgenciaOpeTarifaPadrao(entradaDTO);

			indicadorDescontoBloqueio = "N".equals(saidaDTO
					.getCdIndicadorDescontoBloqueio());

			setVlTarifaPadrao(saidaDTO.getVlTarifaPadrao());
		} catch (PdcAdapterFunctionalException p) {
			throw p;
		}
	}

	// Set's and Getters
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: manterSolicRetransmissaoArquivosRetornoServiceImpl.
	 *
	 * @return manterSolicRetransmissaoArquivosRetornoServiceImpl
	 */
	public IManterSolicRetransmissaoArquivosRetornoService getManterSolicRetransmissaoArquivosRetornoServiceImpl() {
		return manterSolicRetransmissaoArquivosRetornoServiceImpl;
	}

	/**
	 * Set: manterSolicRetransmissaoArquivosRetornoServiceImpl.
	 *
	 * @param manterSolicRetransmissaoArquivosRetornoServiceImpl the manter solic retransmissao arquivos retorno service impl
	 */
	public void setManterSolicRetransmissaoArquivosRetornoServiceImpl(
			IManterSolicRetransmissaoArquivosRetornoService manterSolicRetransmissaoArquivosRetornoServiceImpl) {
		this.manterSolicRetransmissaoArquivosRetornoServiceImpl = manterSolicRetransmissaoArquivosRetornoServiceImpl;
	}

	/**
	 * Get: cboOrigemSolicitacao.
	 *
	 * @return cboOrigemSolicitacao
	 */
	public Integer getCboOrigemSolicitacao() {
		return cboOrigemSolicitacao;
	}

	/**
	 * Set: cboOrigemSolicitacao.
	 *
	 * @param cboOrigemSolicitacao the cbo origem solicitacao
	 */
	public void setCboOrigemSolicitacao(Integer cboOrigemSolicitacao) {
		this.cboOrigemSolicitacao = cboOrigemSolicitacao;
	}

	/**
	 * Get: cboSituacaoSolicitacao.
	 *
	 * @return cboSituacaoSolicitacao
	 */
	public Integer getCboSituacaoSolicitacao() {
		return cboSituacaoSolicitacao;
	}

	/**
	 * Set: cboSituacaoSolicitacao.
	 *
	 * @param cboSituacaoSolicitacao the cbo situacao solicitacao
	 */
	public void setCboSituacaoSolicitacao(Integer cboSituacaoSolicitacao) {
		this.cboSituacaoSolicitacao = cboSituacaoSolicitacao;
	}

	/**
	 * Get: dataSolicitacaoAte.
	 *
	 * @return dataSolicitacaoAte
	 */
	public Date getDataSolicitacaoAte() {
		return dataSolicitacaoAte;
	}

	/**
	 * Set: dataSolicitacaoAte.
	 *
	 * @param dataSolicitacaoAte the data solicitacao ate
	 */
	public void setDataSolicitacaoAte(Date dataSolicitacaoAte) {
		this.dataSolicitacaoAte = dataSolicitacaoAte;
	}

	/**
	 * Get: dataSolicitacaoDe.
	 *
	 * @return dataSolicitacaoDe
	 */
	public Date getDataSolicitacaoDe() {
		return dataSolicitacaoDe;
	}

	/**
	 * Set: dataSolicitacaoDe.
	 *
	 * @param dataSolicitacaoDe the data solicitacao de
	 */
	public void setDataSolicitacaoDe(Date dataSolicitacaoDe) {
		this.dataSolicitacaoDe = dataSolicitacaoDe;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ListarSolBaseRetransmissaoSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(
			List<ListarSolBaseRetransmissaoSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: listaGridControleRadio.
	 *
	 * @return listaGridControleRadio
	 */
	public List<SelectItem> getListaGridControleRadio() {
		return listaGridControleRadio;
	}

	/**
	 * Set: listaGridControleRadio.
	 *
	 * @param listaGridControleRadio the lista grid controle radio
	 */
	public void setListaGridControleRadio(
			List<SelectItem> listaGridControleRadio) {
		this.listaGridControleRadio = listaGridControleRadio;
	}

	/**
	 * Get: listaSituacaoSolicitacao.
	 *
	 * @return listaSituacaoSolicitacao
	 */
	public List<SelectItem> getListaSituacaoSolicitacao() {
		return listaSituacaoSolicitacao;
	}

	/**
	 * Set: listaSituacaoSolicitacao.
	 *
	 * @param listaSituacaoSolicitacao the lista situacao solicitacao
	 */
	public void setListaSituacaoSolicitacao(
			List<SelectItem> listaSituacaoSolicitacao) {
		this.listaSituacaoSolicitacao = listaSituacaoSolicitacao;
	}

	/**
	 * Get: numeroSolicitacao.
	 *
	 * @return numeroSolicitacao
	 */
	public Integer getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	/**
	 * Set: numeroSolicitacao.
	 *
	 * @param numeroSolicitacao the numero solicitacao
	 */
	public void setNumeroSolicitacao(Integer numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: listaDetalharSolBaseRetransmissao.
	 *
	 * @return listaDetalharSolBaseRetransmissao
	 */
	public List<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO> getListaDetalharSolBaseRetransmissao() {
		return listaDetalharSolBaseRetransmissao;
	}

	/**
	 * Set: listaDetalharSolBaseRetransmissao.
	 *
	 * @param listaDetalharSolBaseRetransmissao the lista detalhar sol base retransmissao
	 */
	public void setListaDetalharSolBaseRetransmissao(
			List<OcorrenciasDetalharSolBaseRetransmissaoSaidaDTO> listaDetalharSolBaseRetransmissao) {
		this.listaDetalharSolBaseRetransmissao = listaDetalharSolBaseRetransmissao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraAtendimento.
	 *
	 * @return dataHoraAtendimento
	 */
	public String getDataHoraAtendimento() {
		return dataHoraAtendimento;
	}

	/**
	 * Set: dataHoraAtendimento.
	 *
	 * @param dataHoraAtendimento the data hora atendimento
	 */
	public void setDataHoraAtendimento(String dataHoraAtendimento) {
		this.dataHoraAtendimento = dataHoraAtendimento;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}

	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: dsNumeroSolicitacao.
	 *
	 * @return dsNumeroSolicitacao
	 */
	public Integer getDsNumeroSolicitacao() {
		return dsNumeroSolicitacao;
	}

	/**
	 * Set: dsNumeroSolicitacao.
	 *
	 * @param dsNumeroSolicitacao the ds numero solicitacao
	 */
	public void setDsNumeroSolicitacao(Integer dsNumeroSolicitacao) {
		this.dsNumeroSolicitacao = dsNumeroSolicitacao;
	}

	/**
	 * Get: qtdeRegistros.
	 *
	 * @return qtdeRegistros
	 */
	public String getQtdeRegistros() {
		return qtdeRegistros;
	}

	/**
	 * Set: qtdeRegistros.
	 *
	 * @param qtdeRegistros the qtde registros
	 */
	public void setQtdeRegistros(String qtdeRegistros) {
		this.qtdeRegistros = qtdeRegistros;
	}

	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	/**
	 * Get: vlDescontoTarifa.
	 *
	 * @return vlDescontoTarifa
	 */
	public BigDecimal getVlDescontoTarifa() {
		return vlDescontoTarifa;
	}

	/**
	 * Set: vlDescontoTarifa.
	 *
	 * @param vlDescontoTarifa the vl desconto tarifa
	 */
	public void setVlDescontoTarifa(BigDecimal vlDescontoTarifa) {
		this.vlDescontoTarifa = vlDescontoTarifa;
	}

	/**
	 * Get: vlTarifaAtualizada.
	 *
	 * @return vlTarifaAtualizada
	 */
	public BigDecimal getVlTarifaAtualizada() {
		return vlTarifaAtualizada;
	}

	/**
	 * Set: vlTarifaAtualizada.
	 *
	 * @param vlTarifaAtualizada the vl tarifa atualizada
	 */
	public void setVlTarifaAtualizada(BigDecimal vlTarifaAtualizada) {
		this.vlTarifaAtualizada = vlTarifaAtualizada;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}

	/**
	 * Get: cboLayoutArquivo.
	 *
	 * @return cboLayoutArquivo
	 */
	public Integer getCboLayoutArquivo() {
		return cboLayoutArquivo;
	}

	/**
	 * Set: cboLayoutArquivo.
	 *
	 * @param cboLayoutArquivo the cbo layout arquivo
	 */
	public void setCboLayoutArquivo(Integer cboLayoutArquivo) {
		this.cboLayoutArquivo = cboLayoutArquivo;
	}

	/**
	 * Get: dataSolicitacaoAteRetorno.
	 *
	 * @return dataSolicitacaoAteRetorno
	 */
	public Date getDataSolicitacaoAteRetorno() {
		return dataSolicitacaoAteRetorno;
	}

	/**
	 * Set: dataSolicitacaoAteRetorno.
	 *
	 * @param dataSolicitacaoAteRetorno the data solicitacao ate retorno
	 */
	public void setDataSolicitacaoAteRetorno(Date dataSolicitacaoAteRetorno) {
		this.dataSolicitacaoAteRetorno = dataSolicitacaoAteRetorno;
	}

	/**
	 * Get: dataSolicitacaoDeRetorno.
	 *
	 * @return dataSolicitacaoDeRetorno
	 */
	public Date getDataSolicitacaoDeRetorno() {
		return dataSolicitacaoDeRetorno;
	}

	/**
	 * Set: dataSolicitacaoDeRetorno.
	 *
	 * @param dataSolicitacaoDeRetorno the data solicitacao de retorno
	 */
	public void setDataSolicitacaoDeRetorno(Date dataSolicitacaoDeRetorno) {
		this.dataSolicitacaoDeRetorno = dataSolicitacaoDeRetorno;
	}

	/**
	 * Get: listaSituacaoSolicitacaoRetorno.
	 *
	 * @return listaSituacaoSolicitacaoRetorno
	 */
	public List<SelectItem> getListaSituacaoSolicitacaoRetorno() {
		return listaSituacaoSolicitacaoRetorno;
	}

	/**
	 * Set: listaSituacaoSolicitacaoRetorno.
	 *
	 * @param listaSituacaoSolicitacaoRetorno the lista situacao solicitacao retorno
	 */
	public void setListaSituacaoSolicitacaoRetorno(
			List<SelectItem> listaSituacaoSolicitacaoRetorno) {
		this.listaSituacaoSolicitacaoRetorno = listaSituacaoSolicitacaoRetorno;
	}

	/**
	 * Get: itemSelecionadoIncluir.
	 *
	 * @return itemSelecionadoIncluir
	 */
	public Integer getItemSelecionadoIncluir() {
		return itemSelecionadoIncluir;
	}

	/**
	 * Set: itemSelecionadoIncluir.
	 *
	 * @param itemSelecionadoIncluir the item selecionado incluir
	 */
	public void setItemSelecionadoIncluir(Integer itemSelecionadoIncluir) {
		this.itemSelecionadoIncluir = itemSelecionadoIncluir;
	}

	/**
	 * Get: listaGridControleIncluir.
	 *
	 * @return listaGridControleIncluir
	 */
	public List<SelectItem> getListaGridControleIncluir() {
		return listaGridControleIncluir;
	}

	/**
	 * Set: listaGridControleIncluir.
	 *
	 * @param listaGridControleIncluir the lista grid controle incluir
	 */
	public void setListaGridControleIncluir(
			List<SelectItem> listaGridControleIncluir) {
		this.listaGridControleIncluir = listaGridControleIncluir;
	}

	/**
	 * Get: sequenciaRetorno.
	 *
	 * @return sequenciaRetorno
	 */
	public String getSequenciaRetorno() {
		return sequenciaRetorno;
	}

	/**
	 * Set: sequenciaRetorno.
	 *
	 * @param sequenciaRetorno the sequencia retorno
	 */
	public void setSequenciaRetorno(String sequenciaRetorno) {
		this.sequenciaRetorno = sequenciaRetorno;
	}

	/**
	 * Get: listarTipoLayout.
	 *
	 * @return listarTipoLayout
	 */
	public List<SelectItem> getListarTipoLayout() {
		return listarTipoLayout;
	}

	/**
	 * Set: listarTipoLayout.
	 *
	 * @param listarTipoLayout the listar tipo layout
	 */
	public void setListarTipoLayout(List<SelectItem> listarTipoLayout) {
		this.listarTipoLayout = listarTipoLayout;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is disable argumentos incluir.
	 *
	 * @return true, if is disable argumentos incluir
	 */
	public boolean isDisableArgumentosIncluir() {
		return disableArgumentosIncluir;
	}

	/**
	 * Set: disableArgumentosIncluir.
	 *
	 * @param disableArgumentosIncluir the disable argumentos incluir
	 */
	public void setDisableArgumentosIncluir(boolean disableArgumentosIncluir) {
		this.disableArgumentosIncluir = disableArgumentosIncluir;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Get: manterSolicitacaoRastFavService.
	 *
	 * @return manterSolicitacaoRastFavService
	 */
	public IManterSolicitacaoRastFavService getManterSolicitacaoRastFavService() {
		return manterSolicitacaoRastFavService;
	}

	/**
	 * Set: manterSolicitacaoRastFavService.
	 *
	 * @param manterSolicitacaoRastFavService the manter solicitacao rast fav service
	 */
	public void setManterSolicitacaoRastFavService(
			IManterSolicitacaoRastFavService manterSolicitacaoRastFavService) {
		this.manterSolicitacaoRastFavService = manterSolicitacaoRastFavService;
	}

	/**
	 * Get: atributoSoltc.
	 *
	 * @return atributoSoltc
	 */
	public VerificaraAtributoSoltcSaidaDTO getAtributoSoltc() {
		return atributoSoltc;
	}

	/**
	 * Set: atributoSoltc.
	 *
	 * @param atributoSoltc the atributo soltc
	 */
	public void setAtributoSoltc(VerificaraAtributoSoltcSaidaDTO atributoSoltc) {
		this.atributoSoltc = atributoSoltc;
	}

	/**
	 * Is desabilita desconto tarifa.
	 *
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		if (atributoSoltc != null
				&& "S".equalsIgnoreCase(atributoSoltc.getDsTarifaBloqueio())) {
			return true;
		}
		return false;
	}

	/**
	 * Is indicador desconto bloqueio.
	 *
	 * @return true, if is indicador desconto bloqueio
	 */
	public boolean isIndicadorDescontoBloqueio() {
		return indicadorDescontoBloqueio;
	}

	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Get: listaGridConsultarIncluir.
	 *
	 * @return listaGridConsultarIncluir
	 */
	public List<ListarArqRetransmissaoSaidaDTO> getListaGridConsultarIncluir() {
		return listaGridConsultarIncluir;
	}

	/**
	 * Set: listaGridConsultarIncluir.
	 *
	 * @param listaGridConsultarIncluir the lista grid consultar incluir
	 */
	public void setListaGridConsultarIncluir(
			List<ListarArqRetransmissaoSaidaDTO> listaGridConsultarIncluir) {
		this.listaGridConsultarIncluir = listaGridConsultarIncluir;
	}

	/**
	 * Set: listaIncluirSelecionados.
	 *
	 * @param listaIncluirSelecionados the lista incluir selecionados
	 */
	public void setListaIncluirSelecionados(
			List<ListarArqRetransmissaoSaidaDTO> listaIncluirSelecionados) {
		this.listaIncluirSelecionados = listaIncluirSelecionados;
	}

	/**
	 * Get: listaIncluirSelecionados.
	 *
	 * @return listaIncluirSelecionados
	 */
	public List<ListarArqRetransmissaoSaidaDTO> getListaIncluirSelecionados() {
		return listaIncluirSelecionados;
	}

}
