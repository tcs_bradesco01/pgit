/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.desautorizarlistasdebitos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.desautorizarlistasdebitos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeListaDebitoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.IDesautorizarListasDebitosService;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.AutDesListaDebitoIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.AutDesListaDebitoIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.AutDesListaDebitoParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.AutDesListaDebitoParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.ConsultarListaDebitoAutDesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.ConsultarListaDebitoAutDesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.DetalharListaDebitoAutDesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.desautorizarlistasdebitos.bean.DetalharListaDebitoAutDesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;

/**
 * Nome: DesautorizarListasDebitosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DesautorizarListasDebitosBean extends PagamentosBean {
	
	/** Atributo desautorizarListasDebitosServiceImpl. */
	private IDesautorizarListasDebitosService desautorizarListasDebitosServiceImpl;

	/** Atributo listaGridDesautorizarListasDebitos. */
	private List<ConsultarListaDebitoAutDesSaidaDTO> listaGridDesautorizarListasDebitos;
	
	/** Atributo listaControleDesautorizarListasDebitos. */
	private List<SelectItem> listaControleDesautorizarListasDebitos;
	
	/** Atributo itemSelecionadoDesautorizarListasDebitos. */
	private Integer itemSelecionadoDesautorizarListasDebitos;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	// Variaveis Cliente
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo empresaConglomerado. */
	private String empresaConglomerado;
	
	/** Atributo numeroPagamento. */
	private String numeroPagamento;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacao. */
	private String situacao;

	// Variaveis Conta Debito
	/** Atributo banco. */
	private String banco;
	
	/** Atributo agencia. */
	private String agencia;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo tipo. */
	private String tipo;

	// Variaveis Pagtos Lista Debito/Anteterior
	/** Atributo qtdPagtosAutorizados. */
	private Long qtdPagtosAutorizados;
	
	/** Atributo vlPagtosAutorizados. */
	private BigDecimal vlPagtosAutorizados;
	
	/** Atributo qtdPagtosDesautorizados. */
	private Long qtdPagtosDesautorizados;
	
	/** Atributo vlPagtosDesautorizados. */
	private BigDecimal vlPagtosDesautorizados;
	
	/** Atributo qtdPagtosEfetivados. */
	private Long qtdPagtosEfetivados;
	
	/** Atributo vlPagtosEfetivados. */
	private BigDecimal vlPagtosEfetivados;
	
	/** Atributo qtdPagtosNaoEfetivados. */
	private Long qtdPagtosNaoEfetivados;
	
	/** Atributo vlPagtosNaoEfetivados. */
	private BigDecimal vlPagtosNaoEfetivados;
	
	/** Atributo qtdPagtosTotal. */
	private Long qtdPagtosTotal;
	
	/** Atributo vlPagtosTotal. */
	private BigDecimal vlPagtosTotal;

	/** Atributo listaGridParcialIntegral. */
	private List<DetalharListaDebitoAutDesSaidaDTO> listaGridParcialIntegral;
	
	/** Atributo listaControleListaDebitoParcial. */
	private List<SelectItem> listaControleListaDebitoParcial;
	
	/** Atributo checkGrid. */
	private boolean checkGrid;

	/** Atributo listaGridParcialSelecionados. */
	private List<DetalharListaDebitoAutDesSaidaDTO> listaGridParcialSelecionados;

	// Variaveis Pagtos Lista Debito Atual
	/** Atributo qtdPagtosAutorizadosAtual. */
	private Long qtdPagtosAutorizadosAtual;
	
	/** Atributo vlPagtosAutorizadosAtual. */
	private BigDecimal vlPagtosAutorizadosAtual;
	
	/** Atributo qtdPagtosDesautorizadosAtual. */
	private Long qtdPagtosDesautorizadosAtual;
	
	/** Atributo vlPagtosDesautorizadosAtual. */
	private BigDecimal vlPagtosDesautorizadosAtual;
	
	/** Atributo qtdPagtosTotalAtual. */
	private Long qtdPagtosTotalAtual;
	
	/** Atributo vlPagtosTotalAtual. */
	private BigDecimal vlPagtosTotalAtual;

	/** Atributo panelBotoes. */
	private boolean panelBotoes;
	
	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo cdListaDebito. */
	private String cdListaDebito;
	
	// relatorio
	/** Atributo logoPath. */
	private String logoPath;

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limparCampos();
		limparInformacaoConsulta();

		// Carregamento dos combos
		listarTipoServicoPagto();
		listarInscricaoFavorecido();
		listarConsultarSituacaoPagamento();

		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();

		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"conDesautorizarListasDebitos");
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

		listarOrgaoPagador();
		setHabilitaArgumentosPesquisa(false);
		setPanelBotoes(false);

		getFiltroAgendamentoEfetivacaoEstornoBean().setVoltarParticipante(
				"conDesautorizarListasDebitos");
	}

	/**
	 * Consultar listas debitos.
	 *
	 * @param evt the evt
	 */
	public void consultarListasDebitos(ActionEvent evt) {
		consultarListasDebitos();
	}

	/**
	 * Consultar listas debitos.
	 *
	 * @return the string
	 */
	public String consultarListasDebitos() {
		try {
			limparInformacaoConsulta();
			ConsultarListaDebitoAutDesEntradaDTO entrada = new ConsultarListaDebitoAutDesEntradaDTO();

			/*
			 * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato�
			 * mover �1�. Se selecionado �Filtrar por Conta D�bito� mover �2�;
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				entrada.setCdTipoPesquisa(1);
			} else {
				entrada.setCdTipoPesquisa(2);
			}
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				/*
				 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da
				 * lista >> Se Selecionado Filtrar por Contrato Passar valor
				 * retornado da lista(Verificado por email), na especifica��o
				 * definia passar os combo e inputtext, o que poderia gerar
				 * problemas caso o usu�rio altere os valores do combo ap�s ter
				 * feito a pesquisa
				 */
				entrada
						.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato()
								: 0L);
				entrada
						.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdTipoContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdTipoContratoNegocio()
								: 0);
				entrada
						.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio()
								: 0L);
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("2")) {
				/*
				 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os
				 * cambos abaixo
				 */
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L);
			}

			/*
			 * Se estiver selecionado "Filtrar por Conta D�bito", passar os
			 * campos deste filtro Se estiver selecionado outro tipo de filtro,
			 * verificar se o "Conta D�bito" dos argumentos de pesquisa esta
			 * marcado Se estiver marcado, passar os valores correspondentes da
			 * tela,caso contr�rio passar zeros
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("2")) {
				entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getBancoContaDebitoFiltro());
				entrada
						.setCdAgencua(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getContaBancariaContaDebitoFiltro());
				entrada
						.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getDigitoContaDebitoFiltro());
			} else {
				if (isChkContaDebito()) {
					entrada.setCdBanco(getCdBancoContaDebito());
					entrada.setCdAgencua(getCdAgenciaBancariaContaDebito());
					entrada.setCdConta(getCdContaBancariaContaDebito());
					entrada
							.setCdDigitoConta(getCdDigitoContaContaDebito() != null
									&& !getCdDigitoContaContaDebito()
											.equals("") ? getCdDigitoContaContaDebito()
									: "0");
				} else {
					entrada.setCdBanco(0);
					entrada.setCdAgencua(0);
					entrada.setCdConta(0L);
					entrada.setCdDigitoConta("0");
				}
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entrada
					.setDtCreditoPagamentoInicial((getDataInicialPagamentoFiltro() == null) ? ""
							: sdf.format(getDataInicialPagamentoFiltro()));
			entrada
					.setDtCreditoPagamentoFinal((getDataFinalPagamentoFiltro() == null) ? ""
							: sdf.format(getDataFinalPagamentoFiltro()));

			if (isChkParticipanteContrato()) {
				entrada
						.setCdPessoaParticipacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCodigoParticipanteFiltro());
			} else {
				entrada.setCdPessoaParticipacaoContrato(0L);
			}

			entrada
					.setCdProdutoServicoOperacao(getTipoServicoFiltro() != null ? getTipoServicoFiltro()
							: 0);
			entrada
					.setCdProdutoServicoRelacionado(getModalidadeFiltro() != null ? getModalidadeFiltro()
							: 0);
			entrada
					.setCdListaDebitoPagamentoInicial(!getNumListaDebitoDeFiltro()
							.equals("") ? Long
							.parseLong(getNumListaDebitoDeFiltro()) : 0L);
			entrada
					.setCdListaDebitoPagamentoFinal(!getNumListaDebitoAteFiltro()
							.equals("") ? Long
							.parseLong(getNumListaDebitoAteFiltro()) : 0L);
			entrada.setCdDigitoAgencia(0);
			entrada.setCdTipoOperacao(2);
			
			for(int x=0; x<getListaModalidadeListaDebito().size();x++){
				ListarModalidadeListaDebitoSaidaDTO item = new ListarModalidadeListaDebitoSaidaDTO();
				
				item = getListaModalidadeListaDebito().get(x);
				
				if(item.getCdProdutoOperacaoRelacionado().equals(getModalidadeFiltro())){
					entrada.setCdServicoCompostoPagamento(item.getCdServicoCompostoPgit());
					break;
				}
			}

			setListaGridDesautorizarListasDebitos(getDesautorizarListasDebitosServiceImpl()
					.consultarListaDebitoAutDes(entrada));

			this.listaControleDesautorizarListasDebitos = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridDesautorizarListasDebitos().size(); i++) {
				this.listaControleDesautorizarListasDebitos.add(new SelectItem(
						i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridDesautorizarListasDebitos(null);
			setDisableArgumentosConsulta(false);
			setPanelBotoes(false);
		}

		return "";
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		// Limpar Argumentos de Pesquisa e Lista
		limparFiltroPrincipal();
		limparInformacaoConsulta();
		setPanelBotoes(false);
		limparCampos();

		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparInformacaoConsulta();
		setPanelBotoes(false);

		limparCampos();

		return "";
	}

	/**
	 * Limpar.
	 */
	public void limpar() {
		limparFiltroPrincipal();
		limparInformacaoConsulta();
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		setListaGridDesautorizarListasDebitos(null);
	}

	/**
	 * Limpar informacao consulta.
	 */
	public void limparInformacaoConsulta() {
		setListaGridDesautorizarListasDebitos(new ArrayList<ConsultarListaDebitoAutDesSaidaDTO>());
		setListaControleDesautorizarListasDebitos(new ArrayList<SelectItem>());
		setItemSelecionadoDesautorizarListasDebitos(null);
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Carrega lista parcial integral.
	 */
	public void carregaListaParcialIntegral() {

		setCheckGrid(false);

		DetalharListaDebitoAutDesEntradaDTO entradaDTO = new DetalharListaDebitoAutDesEntradaDTO();
		ConsultarListaDebitoAutDesSaidaDTO registroSelecionado = getListaGridDesautorizarListasDebitos()
				.get(getItemSelecionadoDesautorizarListasDebitos());

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrSequenciaContratoNegocio());
		entradaDTO.setCdTipoCanal(registroSelecionado.getCdCanal());
		entradaDTO.setCdListaDebitoPagamento(registroSelecionado
				.getCdListaDebitoPagamento());

		setListaGridParcialIntegral(getDesautorizarListasDebitosServiceImpl()
				.detalharListaDebitoAutDes(entradaDTO));

		listaControleListaDebitoParcial = new ArrayList<SelectItem>();
		for (int i = 0; i < getListaGridParcialIntegral().size(); i++) {
			listaControleListaDebitoParcial.add(new SelectItem(i, ""));
		}

	}

	/**
	 * Carrega variaveis lista de debito.
	 */
	public void carregaVariaveisListaDeDebito() {
		ConsultarListaDebitoAutDesSaidaDTO registroSelecionado = getListaGridDesautorizarListasDebitos()
				.get(getItemSelecionadoDesautorizarListasDebitos());

		setQtdPagtosAutorizados(registroSelecionado.getQtPagamentoAutorizado());
		setVlPagtosAutorizados(registroSelecionado.getVlPagamentoAutorizado());
		setQtdPagtosDesautorizados(registroSelecionado
				.getQtPagamentoDesautorizado());
		setVlPagtosDesautorizados(registroSelecionado
				.getVlPagamentoDesautorizado());
		setQtdPagtosTotal(registroSelecionado.getQtPagamentoTotal());
		setVlPagtosTotal(registroSelecionado.getVlPagamentoTotal());
		setQtdPagtosEfetivados(registroSelecionado.getQtPagamentoEfetivado());
		setVlPagtosEfetivados(registroSelecionado.getVlPagamentoEfetivado());
		setQtdPagtosNaoEfetivados(registroSelecionado
				.getQtPagamentoNaoEfetivado());
		setVlPagtosNaoEfetivados(registroSelecionado
				.getVlPagamentoNaoEfetivado());

		setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
		setCdListaDebito(registroSelecionado.getCdListaDebitoPagamento().toString());
		setDsModalidade(registroSelecionado.getDsProdutoServicoRelacionado());
		setDtPagamento(registroSelecionado.getDtPrevistaPagamentoFormatada());
	}

	/**
	 * Limpa variaveis.
	 */
	public void limpaVariaveis() {
		// Cliente e Contrato
		setCpfCnpj("");
		setNomeRazao("");
		setEmpresaConglomerado("");
		setNumeroContrato("");
		setDescricaoContrato("");
		setSituacao("");
		// Conta D�bito
		setBanco("");
		setAgencia("");
		setConta("");
		setTipo("");
	}

	/**
	 * Carrega cabecalho.
	 */
	public void carregaCabecalho() {
		limpaVariaveis();
		ConsultarListaDebitoAutDesSaidaDTO itemSelecionado = getListaGridDesautorizarListasDebitos()
				.get(getItemSelecionadoDesautorizarListasDebitos());

		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")) { // Filtrar por
															// cliente
			setCpfCnpj(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNrCpfCnpjCliente());
			setNomeRazao(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoRazaoSocialCliente());
			setEmpresaConglomerado(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getEmpresaGestoraDescCliente());
			setNumeroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNumeroDescCliente());
			setDescricaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoContratoDescCliente());
			setSituacao(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSituacaoDescCliente());
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
																// contrato
				setCpfCnpj(itemSelecionado.getNrCpfCnpjFormatado());
				setNomeRazao(itemSelecionado.getDsPessoaEmpresa());
				setEmpresaConglomerado(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getEmpresaGestoraDescContrato());
				setNumeroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getNumeroDescContrato());
				setDescricaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getDescricaoContratoDescContrato());
				setSituacao(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getSituacaoDescContrato());
			}
		}

		ConsultarListaDebitoAutDesSaidaDTO registroSelecionado = getListaGridDesautorizarListasDebitos()
				.get(getItemSelecionadoDesautorizarListasDebitos());
		setBanco(PgitUtil.formatBanco(registroSelecionado.getCdBanco(),
				registroSelecionado.getDsBanco(), false));
		setAgencia(PgitUtil.formatAgencia(registroSelecionado.getCdAgencia(),
				registroSelecionado.getCdDigitoAgencia(), registroSelecionado
						.getDsAgencia(), false));
		setConta(PgitUtil.formatConta(registroSelecionado.getCdConta(),
				registroSelecionado.getCdDigitoConta(), false));
		setTipo(registroSelecionado.getDsTipoConta());

		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("2")) { // Filtrar por Conta
															// D�bito
			setCpfCnpj(itemSelecionado.getNrCpfCnpjFormatado());
			setNomeRazao(itemSelecionado.getDsPessoaEmpresa());
			setEmpresaConglomerado(itemSelecionado.getDsEmpresa());
			setNumeroContrato(String.valueOf(itemSelecionado
					.getNrSequenciaContratoNegocio()));

			try {
				DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
				entradaDTO.setCdPessoaJuridicaContrato(itemSelecionado
						.getCdPessoaJuridicaContrato());
				entradaDTO.setCdTipoContratoNegocio(itemSelecionado
						.getCdTipoContratoNegocio());
				entradaDTO.setNrSequenciaContratoNegocio(itemSelecionado
						.getNrSequenciaContratoNegocio());

				DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
						.detalharDadosContrato(entradaDTO);
				setDescricaoContrato(saidaDTO.getDsContrato());
				setSituacao(saidaDTO.getDsSituacaoContratoNegocio());
			} catch (PdcAdapterFunctionalException e) {
				setDescricaoContrato("");
				setSituacao("");
			}

		}
	}

	/**
	 * Desautorizar parcial.
	 *
	 * @return the string
	 */
	public String desautorizarParcial() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());
		try {
			carregaCabecalho();
			carregaVariaveisListaDeDebito();
			carregaListaParcialIntegral();
			setPanelBotoes(false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridParcialIntegral(null);
			setCheckGrid(false);
			return "";
		}

		return "DESAUTORIZAR_PARCIAL";
	}

	/**
	 * Desautorizar selecionados.
	 *
	 * @return the string
	 */
	public String desautorizarSelecionados() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());

		setListaGridParcialSelecionados(new ArrayList<DetalharListaDebitoAutDesSaidaDTO>());

		/*
		 * setVlPagtosAutorizadosAtual(BigDecimal.ZERO);
		 * setVlPagtosDesautorizadosAtual(BigDecimal.ZERO);
		 */

		BigDecimal valor = BigDecimal.ZERO;

		for (DetalharListaDebitoAutDesSaidaDTO o : getListaGridParcialIntegral()) {
			if (o.isCheck()) {
				listaGridParcialSelecionados.add(o);
				valor = valor.add(o.getVlPagamento());

			}
		}

		carregaVariaveisListaDeDebito();

		/*
		 * Long totalQtAutorizado = 0L; BigDecimal totalVlAutorizado =
		 * BigDecimal.ZERO; Long totalQtDesautorizado = 0l; BigDecimal
		 * totalVlDesautorizado = BigDecimal.ZERO;
		 * 
		 * for(ConsultarListaDebitoAutDesSaidaDTO o :
		 * getListaGridDesautorizarListasDebitos()){ totalQtAutorizado =
		 * totalQtAutorizado + o.getQtPagamentoAutorizado(); totalVlAutorizado =
		 * totalVlAutorizado.add(o.getVlPagamentoAutorizado());
		 * totalQtDesautorizado = totalQtDesautorizado +
		 * o.getQtPagamentoDesautorizado(); totalVlDesautorizado =
		 * totalVlDesautorizado.add(o.getVlPagamentoDesautorizado()); }
		 */

		/*
		 * setQtdPagtosAutorizadosAtual(totalQtAutorizado -
		 * getListaGridParcialSelecionados().size());
		 * setVlPagtosAutorizadosAtual
		 * (totalVlAutorizado.subtract(getVlPagtosAutorizadosAtual()));
		 * setQtdPagtosDesautorizadosAtual(totalQtDesautorizado +
		 * getListaGridParcialSelecionados().size());
		 * setVlPagtosDesautorizadosAtual
		 * (totalVlDesautorizado.add(getVlPagtosDesautorizadosAtual()));
		 * setQtdPagtosTotalAtual(getQtdPagtosTotal());
		 * setVlPagtosTotalAtual(getVlPagtosTotal());
		 */

		setQtdPagtosAutorizadosAtual(getQtdPagtosAutorizados()
				- getListaGridParcialSelecionados().size());
		setVlPagtosAutorizadosAtual(getVlPagtosAutorizados().subtract(valor));
		setQtdPagtosDesautorizadosAtual(getQtdPagtosDesautorizados()
				+ getListaGridParcialSelecionados().size());
		setVlPagtosDesautorizadosAtual(getVlPagtosDesautorizados().add(valor));
		setQtdPagtosTotalAtual(getQtdPagtosTotal());
		setVlPagtosTotalAtual(getVlPagtosTotal());

		return "DESAUTORIZAR_SELECIONADOS";
	}

	/**
	 * Confirmar parcial.
	 *
	 * @return the string
	 */
	public String confirmarParcial() {
		try {
			List<AutDesListaDebitoParcialEntradaDTO> listaEntrada = new ArrayList<AutDesListaDebitoParcialEntradaDTO>();

			for (DetalharListaDebitoAutDesSaidaDTO o : getListaGridParcialSelecionados()) {
				AutDesListaDebitoParcialEntradaDTO ocorrencia = new AutDesListaDebitoParcialEntradaDTO();
				ocorrencia.setCdPessoaJuridicaLista(o
						.getCdPessoaJuridicaLista());
				ocorrencia.setCdTipoContratoLista(o.getCdTipoContratoLista());
				ocorrencia.setNrSequenciaContratoLista(o
						.getNrSequenciaContratoLista());
				ocorrencia.setCdTipoCanalLista(o.getCdTipoCanalLista());
				ocorrencia.setCdListaDebitoPagamento(o
						.getCdListaDebitoPagamento());
				ocorrencia.setCdPessoaJuridicaContrato(o
						.getCdPessoaJuridicaContrato());
				ocorrencia.setCdTipoContratoNegocio(o
						.getCdTipoContratoNegocio());
				ocorrencia.setNrSequenciaContratoNegocio(o
						.getNrSequenciaContratoNegoci());
				ocorrencia.setCdTipoCanal(o.getCdTipoCanal());
				ocorrencia.setCdControlePagamento(o.getCdControlePagamento());
				ocorrencia.setVlPagamento(o.getVlPagamento());

				listaEntrada.add(ocorrencia);
			}

			AutDesListaDebitoParcialSaidaDTO saidaDTO = getDesautorizarListasDebitosServiceImpl()
					.autDesListaDebitoParcial(listaEntrada);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conDesautorizarListasDebitos",
					"#{desautorizarListasDebitosBean.consultarListasDebitos}",
					false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Desautorizar integral.
	 *
	 * @return the string
	 */
	public String desautorizarIntegral() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());

		ConsultarListaDebitoAutDesSaidaDTO registroSelecionado = getListaGridDesautorizarListasDebitos()
				.get(getItemSelecionadoDesautorizarListasDebitos());
		carregaCabecalho();
		carregaVariaveisListaDeDebito();

		// Variaveis Pagtos Lista Debito Atual
		setQtdPagtosAutorizadosAtual(0L);
		setVlPagtosAutorizadosAtual(BigDecimal.ZERO);
		setQtdPagtosDesautorizadosAtual(registroSelecionado
				.getQtPagamentoDesautorizado()
				+ registroSelecionado.getQtPagamentoAutorizado());
		setVlPagtosDesautorizadosAtual(registroSelecionado
				.getVlPagamentoDesautorizado().add(
						registroSelecionado.getVlPagamentoAutorizado()));
		setQtdPagtosTotalAtual(registroSelecionado.getQtPagamentoTotal());
		setVlPagtosTotalAtual(registroSelecionado.getVlPagamentoTotal());
		try {
			carregaListaParcialIntegral();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridParcialIntegral(null);
			setCheckGrid(false);
			return "";
		}

		return "DESAUTORIZAR_INTEGRAL";
	}

	/**
	 * Confirmar integral.
	 *
	 * @return the string
	 */
	public String confirmarIntegral() {
		try {
			AutDesListaDebitoIntegralEntradaDTO entradaDTO = new AutDesListaDebitoIntegralEntradaDTO();
			ConsultarListaDebitoAutDesSaidaDTO registroSelecionado = getListaGridDesautorizarListasDebitos()
					.get(getItemSelecionadoDesautorizarListasDebitos());

			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
					.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado
					.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
					.getNrSequenciaContratoNegocio());
			entradaDTO.setCdTipoCanal(registroSelecionado.getCdCanal());
			entradaDTO.setCdListaDebitoPagamento(registroSelecionado
					.getCdListaDebitoPagamento());

			AutDesListaDebitoIntegralSaidaDTO saidaDTO = getDesautorizarListasDebitosServiceImpl()
					.autDesListaDebitoIntegral(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conDesautorizarListasDebitos",
					"#{desautorizarListasDebitosBean.consultarListasDebitos}",
					false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {

		setItemSelecionadoDesautorizarListasDebitos(null);
		return "VOLTAR";
	}

	/**
	 * Voltar parcial.
	 *
	 * @return the string
	 */
	public String voltarParcial() {
		for (int i = 0; i < getListaGridParcialIntegral().size(); i++) {
			getListaGridParcialIntegral().get(i).setCheck(false);
		}

		setCheckGrid(false);
		setPanelBotoes(false);

		return "VOLTAR";

	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		for (int i = 0; i < getListaGridParcialIntegral().size(); i++) {
			if (getListaGridParcialIntegral().get(i).isCheck()) {
				setPanelBotoes(true);
				return;
			}
		}
		setPanelBotoes(false);
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		consultarListasDebitos();
		return "";
	}

	/**
	 * Pesquisar integral parcial.
	 *
	 * @return the string
	 */
	public String pesquisarIntegralParcial() {
		carregaListaParcialIntegral();
		return "";
	}

	/**
	 * Pesquisar parcial.
	 *
	 * @param evt the evt
	 */
	public void pesquisarParcial(ActionEvent evt) {
		setCheckGrid(false);
		for (DetalharListaDebitoAutDesSaidaDTO o : getListaGridParcialIntegral()) {
			o.setCheck(false);
		}
	}

	/**
	 * Imprimir parcial.
	 */
	public void imprimirParcial() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Desautorizar Lista de D�bito de Pagamentos");

			par.put("cpfCnpj", getCpfCnpj());
			par.put("nomeRazaoSocial", getNomeRazao());
			par.put("empresaConglomerado", getEmpresaConglomerado());
			par.put("nrContrato", getNumeroContrato());
			par.put("dsContratacao", getDescricaoContrato());
			par.put("situacao", getSituacao());
			par.put("banco", getBanco());
			par.put("agencia", getAgencia());
			par.put("conta", getConta());
			par.put("tipo", getTipo());
			par.put("logoBradesco", getLogoPath());

			// lista d�bito anterior
			par.put("qtdePagtosAutorizadosAnterior", getQtdPagtosAutorizados());
			par.put("vlPagtosAutorizadosAnteior", getVlPagtosAutorizados());
			par.put("qtdePagtosDesautorizadosAnterior",
					getQtdPagtosDesautorizados());
			par.put("vlPagtosDesautorizadosAnterior",
					getVlPagtosDesautorizados());
			par.put("qtdeTotalPagtosAnterior", getQtdPagtosTotal());
			par.put("vlTotalPagtosAnterior", getVlPagtosTotal());

			// lista d�bito atual
			par.put("qtdePagtosAutorizadosAtual",
					getQtdPagtosAutorizadosAtual());
			par.put("vlPagtosAutorizadosAtual", getVlPagtosAutorizadosAtual());
			par.put("qtdePagtosDesautorizadosAtual",
					getQtdPagtosDesautorizadosAtual());
			par.put("vlPagtosDesautorizadosAtual",
					getVlPagtosDesautorizadosAtual());
			par.put("qtdeTotalPagtosAtual", getQtdPagtosTotalAtual());
			par.put("vlTotalPagtosAtual", getVlPagtosTotalAtual());

			par.put("qtdePagtosEfetivados", getQtdPagtosEfetivados());
			par.put("vlPagtosEfetivados", getVlPagtosEfetivados());
			par.put("qtdePagtosNaoEfetivados", getQtdPagtosNaoEfetivados());
			par.put("vlPagtosNaoEfetivados", getVlPagtosNaoEfetivados());

			par.put("tpServico", getDsTipoServico());
			par.put("listaDebito", getCdListaDebito());
			par.put("modalidade", getDsModalidade());
			par.put("dtPagamento", getDtPagamento());

			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/listaDebito/desautorizar/parcial",
								"imprimirDesautorizarListasDebitoParcial",
								getListaGridParcialSelecionados(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Imprimir integral.
	 */
	public void imprimirIntegral() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Desautorizar Lista de D�bito de Pagamentos");

			par.put("cpfCnpj", getCpfCnpj());
			par.put("nomeRazaoSocial", getNomeRazao());
			par.put("empresaConglomerado", getEmpresaConglomerado());
			par.put("nrContrato", getNumeroContrato());
			par.put("dsContratacao", getDescricaoContrato());
			par.put("situacao", getSituacao());
			par.put("banco", getBanco());
			par.put("agencia", getAgencia());
			par.put("conta", getConta());
			par.put("tipo", getTipo());
			par.put("logoBradesco", getLogoPath());

			// lista d�bito anterior
			par.put("qtdePagtosAutorizadosAnterior", getQtdPagtosAutorizados());
			par.put("vlPagtosAutorizadosAnteior", getVlPagtosAutorizados());
			par.put("qtdePagtosDesautorizadosAnterior",
					getQtdPagtosDesautorizados());
			par.put("vlPagtosDesautorizadosAnterior",
					getVlPagtosDesautorizados());
			par.put("qtdeTotalPagtosAnterior", getQtdPagtosTotal());
			par.put("vlTotalPagtosAnterior", getVlPagtosTotal());

			// lista d�bito atual
			par.put("qtdePagtosAutorizadosAtual",
					getQtdPagtosAutorizadosAtual());
			par.put("vlPagtosAutorizadosAtual", getVlPagtosAutorizadosAtual());
			par.put("qtdePagtosDesautorizadosAtual",
					getQtdPagtosDesautorizadosAtual());
			par.put("vlPagtosDesautorizadosAtual",
					getVlPagtosDesautorizadosAtual());
			par.put("qtdeTotalPagtosAtual", getQtdPagtosTotalAtual());
			par.put("vlTotalPagtosAtual", getVlPagtosTotalAtual());

			par.put("qtdePagtosEfetivados", getQtdPagtosEfetivados());
			par.put("vlPagtosEfetivados", getVlPagtosEfetivados());
			par.put("qtdePagtosNaoEfetivados", getQtdPagtosNaoEfetivados());
			par.put("vlPagtosNaoEfetivados", getVlPagtosNaoEfetivados());

			par.put("tpServico", getDsTipoServico());
			par.put("listaDebito", getCdListaDebito());
			par.put("modalidade", getDsModalidade());
			par.put("dtPagamento", getDtPagamento());

			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/listaDebito/desautorizar/integral",
								"imprimirDesautorizarListasDebitoIntegral",
								getListaGridParcialIntegral(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Get: desautorizarListasDebitosServiceImpl.
	 *
	 * @return desautorizarListasDebitosServiceImpl
	 */
	public IDesautorizarListasDebitosService getDesautorizarListasDebitosServiceImpl() {
		return desautorizarListasDebitosServiceImpl;
	}

	/**
	 * Set: desautorizarListasDebitosServiceImpl.
	 *
	 * @param desautorizarListasDebitosServiceImpl the desautorizar listas debitos service impl
	 */
	public void setDesautorizarListasDebitosServiceImpl(
			IDesautorizarListasDebitosService desautorizarListasDebitosServiceImpl) {
		this.desautorizarListasDebitosServiceImpl = desautorizarListasDebitosServiceImpl;
	}

	/**
	 * Get: itemSelecionadoDesautorizarListasDebitos.
	 *
	 * @return itemSelecionadoDesautorizarListasDebitos
	 */
	public Integer getItemSelecionadoDesautorizarListasDebitos() {
		return itemSelecionadoDesautorizarListasDebitos;
	}

	/**
	 * Set: itemSelecionadoDesautorizarListasDebitos.
	 *
	 * @param itemSelecionadoDesautorizarListasDebitos the item selecionado desautorizar listas debitos
	 */
	public void setItemSelecionadoDesautorizarListasDebitos(
			Integer itemSelecionadoDesautorizarListasDebitos) {
		this.itemSelecionadoDesautorizarListasDebitos = itemSelecionadoDesautorizarListasDebitos;
	}

	/**
	 * Get: listaControleDesautorizarListasDebitos.
	 *
	 * @return listaControleDesautorizarListasDebitos
	 */
	public List<SelectItem> getListaControleDesautorizarListasDebitos() {
		return listaControleDesautorizarListasDebitos;
	}

	/**
	 * Set: listaControleDesautorizarListasDebitos.
	 *
	 * @param listaControleDesautorizarListasDebitos the lista controle desautorizar listas debitos
	 */
	public void setListaControleDesautorizarListasDebitos(
			List<SelectItem> listaControleDesautorizarListasDebitos) {
		this.listaControleDesautorizarListasDebitos = listaControleDesautorizarListasDebitos;
	}

	/**
	 * Get: listaGridDesautorizarListasDebitos.
	 *
	 * @return listaGridDesautorizarListasDebitos
	 */
	public List<ConsultarListaDebitoAutDesSaidaDTO> getListaGridDesautorizarListasDebitos() {
		return listaGridDesautorizarListasDebitos;
	}

	/**
	 * Set: listaGridDesautorizarListasDebitos.
	 *
	 * @param listaGridDesautorizarListasDebitos the lista grid desautorizar listas debitos
	 */
	public void setListaGridDesautorizarListasDebitos(
			List<ConsultarListaDebitoAutDesSaidaDTO> listaGridDesautorizarListasDebitos) {
		this.listaGridDesautorizarListasDebitos = listaGridDesautorizarListasDebitos;
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaConglomerado.
	 *
	 * @return empresaConglomerado
	 */
	public String getEmpresaConglomerado() {
		return empresaConglomerado;
	}

	/**
	 * Set: empresaConglomerado.
	 *
	 * @param empresaConglomerado the empresa conglomerado
	 */
	public void setEmpresaConglomerado(String empresaConglomerado) {
		this.empresaConglomerado = empresaConglomerado;
	}

	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: qtdPagtosAutorizados.
	 *
	 * @return qtdPagtosAutorizados
	 */
	public Long getQtdPagtosAutorizados() {
		return qtdPagtosAutorizados;
	}

	/**
	 * Set: qtdPagtosAutorizados.
	 *
	 * @param qtdPagtosAutorizados the qtd pagtos autorizados
	 */
	public void setQtdPagtosAutorizados(Long qtdPagtosAutorizados) {
		this.qtdPagtosAutorizados = qtdPagtosAutorizados;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: listaControleListaDebitoParcial.
	 *
	 * @return listaControleListaDebitoParcial
	 */
	public List<SelectItem> getListaControleListaDebitoParcial() {
		return listaControleListaDebitoParcial;
	}

	/**
	 * Set: listaControleListaDebitoParcial.
	 *
	 * @param listaControleListaDebitoParcial the lista controle lista debito parcial
	 */
	public void setListaControleListaDebitoParcial(
			List<SelectItem> listaControleListaDebitoParcial) {
		this.listaControleListaDebitoParcial = listaControleListaDebitoParcial;
	}

	/**
	 * Is check grid.
	 *
	 * @return true, if is check grid
	 */
	public boolean isCheckGrid() {
		return checkGrid;
	}

	/**
	 * Set: checkGrid.
	 *
	 * @param checkGrid the check grid
	 */
	public void setCheckGrid(boolean checkGrid) {
		this.checkGrid = checkGrid;
	}

	/**
	 * Get: listaGridParcialSelecionados.
	 *
	 * @return listaGridParcialSelecionados
	 */
	public List<DetalharListaDebitoAutDesSaidaDTO> getListaGridParcialSelecionados() {
		return listaGridParcialSelecionados;
	}

	/**
	 * Set: listaGridParcialSelecionados.
	 *
	 * @param listaGridParcialSelecionados the lista grid parcial selecionados
	 */
	public void setListaGridParcialSelecionados(
			List<DetalharListaDebitoAutDesSaidaDTO> listaGridParcialSelecionados) {
		this.listaGridParcialSelecionados = listaGridParcialSelecionados;
	}

	/**
	 * Get: vlPagtosAutorizados.
	 *
	 * @return vlPagtosAutorizados
	 */
	public BigDecimal getVlPagtosAutorizados() {
		return vlPagtosAutorizados;
	}

	/**
	 * Set: vlPagtosAutorizados.
	 *
	 * @param vlPagtosAutorizados the vl pagtos autorizados
	 */
	public void setVlPagtosAutorizados(BigDecimal vlPagtosAutorizados) {
		this.vlPagtosAutorizados = vlPagtosAutorizados;
	}

	/**
	 * Get: qtdPagtosDesautorizados.
	 *
	 * @return qtdPagtosDesautorizados
	 */
	public Long getQtdPagtosDesautorizados() {
		return qtdPagtosDesautorizados;
	}

	/**
	 * Set: qtdPagtosDesautorizados.
	 *
	 * @param qtdPagtosDesautorizados the qtd pagtos desautorizados
	 */
	public void setQtdPagtosDesautorizados(Long qtdPagtosDesautorizados) {
		this.qtdPagtosDesautorizados = qtdPagtosDesautorizados;
	}

	/**
	 * Get: vlPagtosDesautorizados.
	 *
	 * @return vlPagtosDesautorizados
	 */
	public BigDecimal getVlPagtosDesautorizados() {
		return vlPagtosDesautorizados;
	}

	/**
	 * Set: vlPagtosDesautorizados.
	 *
	 * @param vlPagtosDesautorizados the vl pagtos desautorizados
	 */
	public void setVlPagtosDesautorizados(BigDecimal vlPagtosDesautorizados) {
		this.vlPagtosDesautorizados = vlPagtosDesautorizados;
	}

	/**
	 * Get: qtdPagtosEfetivados.
	 *
	 * @return qtdPagtosEfetivados
	 */
	public Long getQtdPagtosEfetivados() {
		return qtdPagtosEfetivados;
	}

	/**
	 * Set: qtdPagtosEfetivados.
	 *
	 * @param qtdPagtosEfetivados the qtd pagtos efetivados
	 */
	public void setQtdPagtosEfetivados(Long qtdPagtosEfetivados) {
		this.qtdPagtosEfetivados = qtdPagtosEfetivados;
	}

	/**
	 * Get: qtdPagtosTotal.
	 *
	 * @return qtdPagtosTotal
	 */
	public Long getQtdPagtosTotal() {
		return qtdPagtosTotal;
	}

	/**
	 * Set: qtdPagtosTotal.
	 *
	 * @param qtdPagtosTotal the qtd pagtos total
	 */
	public void setQtdPagtosTotal(Long qtdPagtosTotal) {
		this.qtdPagtosTotal = qtdPagtosTotal;
	}

	/**
	 * Get: vlPagtosEfetivados.
	 *
	 * @return vlPagtosEfetivados
	 */
	public BigDecimal getVlPagtosEfetivados() {
		return vlPagtosEfetivados;
	}

	/**
	 * Set: vlPagtosEfetivados.
	 *
	 * @param vlPagtosEfetivados the vl pagtos efetivados
	 */
	public void setVlPagtosEfetivados(BigDecimal vlPagtosEfetivados) {
		this.vlPagtosEfetivados = vlPagtosEfetivados;
	}

	/**
	 * Get: vlPagtosTotal.
	 *
	 * @return vlPagtosTotal
	 */
	public BigDecimal getVlPagtosTotal() {
		return vlPagtosTotal;
	}

	/**
	 * Set: vlPagtosTotal.
	 *
	 * @param vlPagtosTotal the vl pagtos total
	 */
	public void setVlPagtosTotal(BigDecimal vlPagtosTotal) {
		this.vlPagtosTotal = vlPagtosTotal;
	}

	/**
	 * Get: listaGridParcialIntegral.
	 *
	 * @return listaGridParcialIntegral
	 */
	public List<DetalharListaDebitoAutDesSaidaDTO> getListaGridParcialIntegral() {
		return listaGridParcialIntegral;
	}

	/**
	 * Set: listaGridParcialIntegral.
	 *
	 * @param listaGridParcialIntegral the lista grid parcial integral
	 */
	public void setListaGridParcialIntegral(
			List<DetalharListaDebitoAutDesSaidaDTO> listaGridParcialIntegral) {
		this.listaGridParcialIntegral = listaGridParcialIntegral;
	}

	/**
	 * Get: qtdPagtosAutorizadosAtual.
	 *
	 * @return qtdPagtosAutorizadosAtual
	 */
	public Long getQtdPagtosAutorizadosAtual() {
		return qtdPagtosAutorizadosAtual;
	}

	/**
	 * Set: qtdPagtosAutorizadosAtual.
	 *
	 * @param qtdPagtosAutorizadosAtual the qtd pagtos autorizados atual
	 */
	public void setQtdPagtosAutorizadosAtual(Long qtdPagtosAutorizadosAtual) {
		this.qtdPagtosAutorizadosAtual = qtdPagtosAutorizadosAtual;
	}

	/**
	 * Get: qtdPagtosDesautorizadosAtual.
	 *
	 * @return qtdPagtosDesautorizadosAtual
	 */
	public Long getQtdPagtosDesautorizadosAtual() {
		return qtdPagtosDesautorizadosAtual;
	}

	/**
	 * Set: qtdPagtosDesautorizadosAtual.
	 *
	 * @param qtdPagtosDesautorizadosAtual the qtd pagtos desautorizados atual
	 */
	public void setQtdPagtosDesautorizadosAtual(
			Long qtdPagtosDesautorizadosAtual) {
		this.qtdPagtosDesautorizadosAtual = qtdPagtosDesautorizadosAtual;
	}

	/**
	 * Get: qtdPagtosTotalAtual.
	 *
	 * @return qtdPagtosTotalAtual
	 */
	public Long getQtdPagtosTotalAtual() {
		return qtdPagtosTotalAtual;
	}

	/**
	 * Set: qtdPagtosTotalAtual.
	 *
	 * @param qtdPagtosTotalAtual the qtd pagtos total atual
	 */
	public void setQtdPagtosTotalAtual(Long qtdPagtosTotalAtual) {
		this.qtdPagtosTotalAtual = qtdPagtosTotalAtual;
	}

	/**
	 * Get: vlPagtosAutorizadosAtual.
	 *
	 * @return vlPagtosAutorizadosAtual
	 */
	public BigDecimal getVlPagtosAutorizadosAtual() {
		return vlPagtosAutorizadosAtual;
	}

	/**
	 * Set: vlPagtosAutorizadosAtual.
	 *
	 * @param vlPagtosAutorizadosAtual the vl pagtos autorizados atual
	 */
	public void setVlPagtosAutorizadosAtual(BigDecimal vlPagtosAutorizadosAtual) {
		this.vlPagtosAutorizadosAtual = vlPagtosAutorizadosAtual;
	}

	/**
	 * Get: vlPagtosDesautorizadosAtual.
	 *
	 * @return vlPagtosDesautorizadosAtual
	 */
	public BigDecimal getVlPagtosDesautorizadosAtual() {
		return vlPagtosDesautorizadosAtual;
	}

	/**
	 * Set: vlPagtosDesautorizadosAtual.
	 *
	 * @param vlPagtosDesautorizadosAtual the vl pagtos desautorizados atual
	 */
	public void setVlPagtosDesautorizadosAtual(
			BigDecimal vlPagtosDesautorizadosAtual) {
		this.vlPagtosDesautorizadosAtual = vlPagtosDesautorizadosAtual;
	}

	/**
	 * Get: vlPagtosTotalAtual.
	 *
	 * @return vlPagtosTotalAtual
	 */
	public BigDecimal getVlPagtosTotalAtual() {
		return vlPagtosTotalAtual;
	}

	/**
	 * Set: vlPagtosTotalAtual.
	 *
	 * @param vlPagtosTotalAtual the vl pagtos total atual
	 */
	public void setVlPagtosTotalAtual(BigDecimal vlPagtosTotalAtual) {
		this.vlPagtosTotalAtual = vlPagtosTotalAtual;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: logoPath.
	 *
	 * @return logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * Set: logoPath.
	 *
	 * @param logoPath the logo path
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: qtdPagtosNaoEfetivados.
	 *
	 * @return qtdPagtosNaoEfetivados
	 */
	public Long getQtdPagtosNaoEfetivados() {
		return qtdPagtosNaoEfetivados;
	}

	/**
	 * Set: qtdPagtosNaoEfetivados.
	 *
	 * @param qtdPagtosNaoEfetivados the qtd pagtos nao efetivados
	 */
	public void setQtdPagtosNaoEfetivados(Long qtdPagtosNaoEfetivados) {
		this.qtdPagtosNaoEfetivados = qtdPagtosNaoEfetivados;
	}

	/**
	 * Get: vlPagtosNaoEfetivados.
	 *
	 * @return vlPagtosNaoEfetivados
	 */
	public BigDecimal getVlPagtosNaoEfetivados() {
		return vlPagtosNaoEfetivados;
	}

	/**
	 * Set: vlPagtosNaoEfetivados.
	 *
	 * @param vlPagtosNaoEfetivados the vl pagtos nao efetivados
	 */
	public void setVlPagtosNaoEfetivados(BigDecimal vlPagtosNaoEfetivados) {
		this.vlPagtosNaoEfetivados = vlPagtosNaoEfetivados;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}

	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

}