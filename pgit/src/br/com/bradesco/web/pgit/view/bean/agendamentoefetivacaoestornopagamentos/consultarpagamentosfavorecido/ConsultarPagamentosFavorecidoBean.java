/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarpagamentosfavorecido
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarpagamentosfavorecido;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarTipoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOrgaoPagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.IConsultaAutorizanteService;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.IConsultarPagamentosFavorecidoService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean.ConsultarPagamentoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean.ConsultarPagamentoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;



/**
 * Nome: ConsultarPagamentosFavorecidoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagamentosFavorecidoBean {

    /** Atributo CODIGO_IDENTIFICACAO_DIVERSOS. */
    private static final String CODIGO_IDENTIFICACAO_DIVERSOS = "4";
    
    private static final Integer NUMERO_ZERO = 0;
    
    private static final String VAZIO = "";
    
    private static final Long NUMERO_ZERO_LONG = 0l;

    /** Atributo consultaAutorizanteService. */
    private IConsultaAutorizanteService consultaAutorizanteService;

    /** Atributo consultasService. */
    private IConsultasService consultasService = null;
    
    /** Atributo disableArgumentosConsulta. */
    private boolean disableArgumentosConsulta;

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo consultarPagamentosFavorecidoServiceImpl. */
    private IConsultarPagamentosFavorecidoService consultarPagamentosFavorecidoServiceImpl;

    /** Atributo consultarPagamentosIndividualServiceImpl. */
    private IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl;

    /** Atributo foco. */
    private String foco;

    /** Atributo exibeBeneficiario. */
    private boolean exibeBeneficiario;

    /** Atributo agendadosPagosNaoPagos. */
    private String agendadosPagosNaoPagos;

    /** Atributo dataPagamentoInicial. */
    private Date dataPagamentoInicial;

    /** Atributo dataPagamentoFinal. */
    private Date dataPagamentoFinal;

    /** Atributo chkTipoServico. */
    private boolean chkTipoServico;

    /** Atributo cmbTipoServico. */
    private Integer cmbTipoServico;

    /** Atributo listaCmbTipoServico. */
    private List<SelectItem> listaCmbTipoServico = new ArrayList<SelectItem>();

    /** Atributo cmbModalidade. */
    private Integer cmbModalidade;

    /** Atributo listaCmbModalidade. */
    private List<SelectItem> listaCmbModalidade = new ArrayList<SelectItem>();

    /** Atributo rdoPesquisa. */
    private String rdoPesquisa;

    /** Atributo rdoFavorecido. */
    private String rdoFavorecido;

    /** Atributo codigoFavorecido. */
    private String codigoFavorecido;

    /** Atributo inscricaoFavorecido. */
    private String inscricaoFavorecido;

    /** Atributo cmbTipoFavorecido. */
    private Integer cmbTipoFavorecido;

    /** Atributo listaCmbTipoFavorecido. */
    private List<SelectItem> listaCmbTipoFavorecido = new ArrayList<SelectItem>();

    /** Atributo banco. */
    private String banco;

    /** Atributo agencia. */
    private String agencia;

    /** Atributo conta. */
    private String conta;

    /** Atributo banco. */
    private Integer bancoSalario;

    /** Atributo agencia. */
    private Integer agenciaSalario;

    /** Atributo conta. */
    private Long contaSalario;

    /** Atributo contaDig. */
    private String contaDig;

    /** Atributo rdoBeneficiario. */
    private String rdoBeneficiario;

    /** Atributo codigoBeneficiario. */
    private String codigoBeneficiario;

    /** Atributo cmbTipoBenefeciario. */
    private Integer cmbTipoBenefeciario;

    /** Atributo cmbOrgaoPagador. */
    private Integer cmbOrgaoPagador;

    /** Atributo listaCmbOrgaoPagador. */
    private List<SelectItem> listaCmbOrgaoPagador = new ArrayList<SelectItem>();

    /** Atributo cmbTipoConta. */
    private Integer cmbTipoConta;

    /** Atributo listaCmbTipoConta. */
    private List<SelectItem> listaCmbTipoConta = new ArrayList<SelectItem>();

    /** Atributo listaSituacaoListaDebitoHash. */
    private Map<Integer, ConsultarTipoContaSaidaDTO> listaSituacaoListaDebitoHash = new HashMap<Integer, ConsultarTipoContaSaidaDTO>();

    /** Atributo listaGridPagamentosFavorecido. */
    private List<ConsultarPagamentoFavorecidoSaidaDTO> listaGridPagamentosFavorecido;

    /** Atributo listaControlePagamentosFavorecido. */
    private List<SelectItem> listaControlePagamentosFavorecido;

    /** Atributo itemSelecionadoLista. */
    private Integer itemSelecionadoLista;

    /** Atributo listaConsultarManutencaoFavorecido. */
    private List<ConsultarManutencaoPagamentoSaidaDTO> listaConsultarManutencaoFavorecido;

    /** Atributo itemSelecionadoListaManutencaoFavorecido. */
    private Integer itemSelecionadoListaManutencaoFavorecido;

    /** Atributo listaManutencaoFavorecidoControle. */
    private List<SelectItem> listaManutencaoFavorecidoControle;

    /** Atributo listaGridHistConsultaSaldo. */
    private List<ConsultarHistConsultaSaldoSaidaDTO> listaGridHistConsultaSaldo;

    /** Atributo itemSelecionadoListaHistConSaldo. */
    private Integer itemSelecionadoListaHistConSaldo;

    /** Atributo listaControleHistConSaldo. */
    private List<SelectItem> listaControleHistConSaldo;

    // Lista - histConSaldoPagIndvFavorecido
    /** Atributo tipoServicoGrid. */
    private String tipoServicoGrid;

    /** Atributo dsModalidadeGrid. */
    private String dsModalidadeGrid;

    /** Atributo numeroPagamentoGrid. */
    private String numeroPagamentoGrid;

    /** Atributo contaDebitoGrid. */
    private String contaDebitoGrid;

    // Cliente
    /** Atributo cpfCnpj. */
    private String cpfCnpj;

    /** Atributo nomeRazaoSocial. */
    private String nomeRazaoSocial;

    /** Atributo empresaConglomerado. */
    private String empresaConglomerado;

    /** Atributo nroContrato. */
    private String nroContrato;

    /** Atributo dsContrato. */
    private String dsContrato;

    /** Atributo situacao. */
    private String situacao;

    // Manutencao
    /** Atributo manutencao. */
    private boolean manutencao = true;

    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;

    /** Atributo situacaoPagamento. */
    private String situacaoPagamento;

    /** Atributo motivoPagamento. */
    private String motivoPagamento;

    /** Atributo cdTipoInscricaoFavorecido. */
    private String cdTipoInscricaoFavorecido;

    /** Atributo tipoContaFavorecido. */
    private String tipoContaFavorecido;

    /** Atributo bancoFavorecido. */
    private String bancoFavorecido;

    /** Atributo agenciaFavorecido. */
    private String agenciaFavorecido;

    /** Atributo contaFavorecido. */
    private String contaFavorecido;

    // ContaDebito
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;

    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;

    /** Atributo contaDebito. */
    private String contaDebito;

    /** Atributo tipoContaDebito. */
    private String tipoContaDebito;

    // Favorecido
    /** Atributo numeroInscricaoFavorecido. */
    private String numeroInscricaoFavorecido;

    /** Atributo tipoFavorecido. */
    private Integer tipoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo cdFavorecido. */
    private Long cdFavorecido;

    /** Atributo dsBancoFavorecido. */
    private String dsBancoFavorecido;

    /** Atributo dsAgenciaFavorecido. */
    private String dsAgenciaFavorecido;

    /** Atributo dsContaFavorecido. */
    private String dsContaFavorecido;

    /** Atributo dsTipoContaFavorecido. */
    private String dsTipoContaFavorecido;

    // Beneficiario
    /** Atributo numeroInscricaoBeneficiario. */
    private String numeroInscricaoBeneficiario;

    /** Atributo tipoBeneficiario. */
    private String tipoBeneficiario;

    /** Atributo nomeBeneficiario. */
    private String nomeBeneficiario;

    /** Atributo dtNascimentoBeneficiario. */
    private String dtNascimentoBeneficiario;

    /** Atributo cdTipoBeneficiario. */
    private Integer cdTipoBeneficiario;

    /** Atributo dsTipoBeneficiario. */
    private String dsTipoBeneficiario;

    // Pagamentos
    /** Atributo dsTipoServico. */
    private String dsTipoServico;

    /** Atributo dsIndicadorModalidade. */
    private String dsIndicadorModalidade;

    /** Atributo cdRemessa. */
    private String cdRemessa;

    /** Atributo dsOperacao. */
    private String dsOperacao;

    /** Atributo cdLote. */
    private String cdLote;

    /** Atributo nrLote. */
    private String nrLote;

    /** Atributo vlPagamento. */
    private BigDecimal vlPagamento;

    /** Atributo vlPagamentoFormatada. */
    private String vlPagamentoFormatada;

    /** Atributo nrPagamento. */
    private String nrPagamento;

    /** Atributo cdListaDebito. */
    private String cdListaDebito;

    /** Atributo dsCanal. */
    private String dsCanal;

    /** Atributo dtPagamento. */
    private String dtPagamento;

    /** Atributo dtDevolucaoEstorno. */
    private String dtDevolucaoEstorno;

    /** Atributo dtLimitePagamento. */
    private String dtLimitePagamento;

    /** Atributo dsRastreado. */
    private String dsRastreado;

    /** Atributo dtPagamentoGare. */
    private Date dtPagamentoGare;

    /** Atributo dtPagamentoManutencao. */
    private Date dtPagamentoManutencao;

    /** Atributo dsIdentificadorTransferenciaPagto. */
    private String dsIdentificadorTransferenciaPagto;

    /** Atributo dtVencimento. */
    private String dtVencimento;

    /** Atributo dtAgendamento. */
    private String dtAgendamento;

    /** Atributo cdIndicadorEconomicoMoeda. */
    private String cdIndicadorEconomicoMoeda;

    /** Atributo qtMoeda. */
    private BigDecimal qtMoeda;

    /** Atributo vlrAgendamento. */
    private BigDecimal vlrAgendamento;

    /** Atributo vlrEfetivacao. */
    private BigDecimal vlrEfetivacao;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private String cdSituacaoOperacaoPagamento;

    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;

    /** Atributo dsPagamento. */
    private String dsPagamento;

    /** Atributo nrDocumento. */
    private String nrDocumento;

    /** Atributo tipoDocumento. */
    private String tipoDocumento;

    /** Atributo cdSerieDocumento. */
    private String cdSerieDocumento;

    /** Atributo vlDocumento. */
    private BigDecimal vlDocumento;

    /** Atributo dtEmissaoDocumento. */
    private String dtEmissaoDocumento;

    /** Atributo dsUsoEmpresa. */
    private String dsUsoEmpresa;

    /** Atributo dsMensagemPrimeiraLinha. */
    private String dsMensagemPrimeiraLinha;

    /** Atributo dsMensagemSegundaLinha. */
    private String dsMensagemSegundaLinha;

    /** Atributo cdBancoCredito. */
    private String cdBancoCredito;

    /** Atributo dsBancoCredito. */
    private String dsBancoCredito;

    /** Atributo cdIspbPagamento. */
    private String cdIspbPagamento;

    /** Atributo agenciaDigitoCredito. */
    private String agenciaDigitoCredito;

    /** Atributo contaDigitoCredito. */
    private String contaDigitoCredito;

    /** Atributo tipoContaCredito. */
    private String tipoContaCredito;

    /** Atributo cdBancoDestino. */
    private String cdBancoDestino;

    /** Atributo agenciaDigitoDestino. */
    private String agenciaDigitoDestino;

    /** Atributo contaDigitoDestino. */
    private String contaDigitoDestino;

    /** Atributo tipoContaDestino. */
    private String tipoContaDestino;

    /** Atributo vlDesconto. */
    private BigDecimal vlDesconto;

    /** Atributo vlAbatimento. */
    private BigDecimal vlAbatimento;

    /** Atributo vlMulta. */
    private BigDecimal vlMulta;

    /** Atributo vlMora. */
    private BigDecimal vlMora;

    /** Atributo vlDeducao. */
    private BigDecimal vlDeducao;

    /** Atributo vlAcrescimo. */
    private BigDecimal vlAcrescimo;

    /** Atributo vlImpostoRenda. */
    private BigDecimal vlImpostoRenda;

    /** Atributo vlIss. */
    private BigDecimal vlIss;

    /** Atributo vlIof. */
    private BigDecimal vlIof;

    /** Atributo vlInss. */
    private BigDecimal vlInss;

    /** Atributo camaraCentralizadora. */
    private String camaraCentralizadora;

    /** Atributo finalidadeDoc. */
    private String finalidadeDoc;

    /** Atributo identificacaoTransferenciaDoc. */
    private String identificacaoTransferenciaDoc;

    /** Atributo identificacaoTitularidade. */
    private String identificacaoTitularidade;

    /** Atributo finalidadeTed. */
    private String finalidadeTed;

    /** Atributo identificacaoTransferenciaTed. */
    private String identificacaoTransferenciaTed;

    /** Atributo numeroOp. */
    private String numeroOp;

    /** Atributo dataExpiracaoCredito. */
    private String dataExpiracaoCredito;

    /** Atributo instrucaoPagamento. */
    private String instrucaoPagamento;

    /** Atributo numeroCheque. */
    private String numeroCheque;

    /** Atributo serieCheque. */
    private String serieCheque;

    /** Atributo indicadorAssociadoUnicaAgencia. */
    private String indicadorAssociadoUnicaAgencia;

    /** Atributo identificadorTipoRetirada. */
    private String identificadorTipoRetirada;

    /** Atributo identificadorRetirada. */
    private String identificadorRetirada;

    /** Atributo dataRetirada. */
    private String dataRetirada;

    /** Atributo vlImpostoMovFinanceira. */
    private BigDecimal vlImpostoMovFinanceira;

    /** Atributo codigoBarras. */
    private String codigoBarras;

    /** Atributo dddContribuinte. */
    private String dddContribuinte;

    /** Atributo telefoneContribuinte. */
    private String telefoneContribuinte;

    /** Atributo inscricaoEstadualContribuinte. */
    private String inscricaoEstadualContribuinte;

    /** Atributo agenteArrecadador. */
    private String agenteArrecadador;

    /** Atributo codigoReceita. */
    private String codigoReceita;

    /** Atributo numeroReferencia. */
    private String numeroReferencia;

    /** Atributo numeroCotaParcela. */
    private String numeroCotaParcela;

    /** Atributo percentualReceita. */
    private BigDecimal percentualReceita;

    /** Atributo periodoApuracao. */
    private String periodoApuracao;

    /** Atributo anoExercicio. */
    private String anoExercicio;

    /** Atributo dataReferencia. */
    private String dataReferencia;

    /** Atributo vlReceita. */
    private BigDecimal vlReceita;

    /** Atributo vlPrincipal. */
    private BigDecimal vlPrincipal;

    /** Atributo vlAtualizacaoMonetaria. */
    private BigDecimal vlAtualizacaoMonetaria;

    /** Atributo vlTotal. */
    private BigDecimal vlTotal;

    /** Atributo codigoCnae. */
    private String codigoCnae;

    /** Atributo placaVeiculo. */
    private String placaVeiculo;

    /** Atributo numeroParcelamento. */
    private String numeroParcelamento;

    /** Atributo codigoOrgaoFavorecido. */
    private String codigoOrgaoFavorecido;

    /** Atributo nomeOrgaoFavorecido. */
    private String nomeOrgaoFavorecido;

    /** Atributo codigoUfFavorecida. */
    private String codigoUfFavorecida;

    /** Atributo descricaoUfFavorecida. */
    private String descricaoUfFavorecida;

    /** Atributo vlAcrescimoFinanceiro. */
    private BigDecimal vlAcrescimoFinanceiro;

    /** Atributo vlHonorarioAdvogado. */
    private BigDecimal vlHonorarioAdvogado;

    /** Atributo observacaoTributo. */
    private String observacaoTributo;

    /** Atributo codigoInss. */
    private String codigoInss;

    /** Atributo dataCompetencia. */
    private String dataCompetencia;

    /** Atributo vlOutrasEntidades. */
    private BigDecimal vlOutrasEntidades;

    /** Atributo codigoTributo. */
    private String codigoTributo;

    /** Atributo codigoRenavam. */
    private String codigoRenavam;

    /** Atributo municipioTributo. */
    private String municipioTributo;

    /** Atributo ufTributo. */
    private String ufTributo;

    /** Atributo numeroNsu. */
    private String numeroNsu;

    /** Atributo opcaoTributo. */
    private String opcaoTributo;

    /** Atributo opcaoRetiradaTributo. */
    private String opcaoRetiradaTributo;

    /** Atributo clienteDepositoIdentificado. */
    private String clienteDepositoIdentificado;

    /** Atributo tipoDespositoIdentificado. */
    private String tipoDespositoIdentificado;

    /** Atributo produtoDepositoIdentificado. */
    private String produtoDepositoIdentificado;

    /** Atributo sequenciaProdutoDepositoIdentificado. */
    private String sequenciaProdutoDepositoIdentificado;

    /** Atributo bancoCartaoDepositoIdentificado. */
    private String bancoCartaoDepositoIdentificado;

    /** Atributo cartaoDepositoIdentificado. */
    private String cartaoDepositoIdentificado;

    /** Atributo bimCartaoDepositoIdentificado. */
    private String bimCartaoDepositoIdentificado;

    /** Atributo viaCartaoDepositoIdentificado. */
    private String viaCartaoDepositoIdentificado;

    /** Atributo codigoDepositante. */
    private String codigoDepositante;

    /** Atributo nomeDepositante. */
    private String nomeDepositante;

    /** Atributo codigoPagador. */
    private String codigoPagador;

    /** Atributo codigoOrdemCredito. */
    private String codigoOrdemCredito;

    /** Atributo nossoNumero. */
    private String nossoNumero;

    /** Atributo dsOrigemPagamento. */
    private String dsOrigemPagamento;

    // Consulta de Saldo
    /** Atributo dtaHraConSaldo. */
    private String dtaHraConSaldo;

    /** Atributo sinal. */
    private String sinal;

    /** Atributo saldoAntLancto. */
    private BigDecimal saldoAntLancto;

    /** Atributo saldoOperacional. */
    private BigDecimal saldoOperacional;

    /** Atributo saldoVincSemReserva. */
    private BigDecimal saldoVincSemReserva;

    /** Atributo saldoVincComReserva. */
    private BigDecimal saldoVincComReserva;

    /** Atributo saldoVincJudicial. */
    private BigDecimal saldoVincJudicial;

    /** Atributo saldoVincAdm. */
    private BigDecimal saldoVincAdm;

    /** Atributo saldoVincSeg. */
    private BigDecimal saldoVincSeg;

    /** Atributo saldoVincAdmLP. */
    private BigDecimal saldoVincAdmLP;

    /** Atributo saldoAgregFundo. */
    private BigDecimal saldoAgregFundo;

    /** Atributo saldoAgregCDB. */
    private BigDecimal saldoAgregCDB;

    /** Atributo saldoAgregPoupanca. */
    private BigDecimal saldoAgregPoupanca;

    /** Atributo saldoAgregCredRotativo. */
    private BigDecimal saldoAgregCredRotativo;

    /** Atributo saldoBonusCPMF. */
    private BigDecimal saldoBonusCPMF;

    /** Atributo descTipoFavorecido. */
    private String descTipoFavorecido;

    // Trilha de Auditoria
    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    /** Atributo dtVecimento. */
    private String dtVecimento;

    // relatorio
    /** Atributo logoPath. */
    private String logoPath;

    /** Atributo cdIndentificadorJudicial. */
    private String cdIndentificadorJudicial;

    /** Atributo dtLimiteDescontoPagamento. */
    private String dtLimiteDescontoPagamento;

    /** Atributo carteira. */
    private String carteira;

    /** Atributo cdSituacaoTranferenciaAutomatica. */
    private String cdSituacaoTranferenciaAutomatica;

    /** Atributo cdBancoOriginal. */
    private Integer cdBancoOriginal;

    /** Atributo dsBancoOriginal. */
    private String dsBancoOriginal;

    /** Atributo cdAgenciaBancariaOriginal. */
    private Integer cdAgenciaBancariaOriginal;

    /** Atributo cdDigitoAgenciaOriginal. */
    private String cdDigitoAgenciaOriginal;

    /** Atributo dsAgenciaOriginal. */
    private String dsAgenciaOriginal;

    /** Atributo cdContaBancariaOriginal. */
    private Long cdContaBancariaOriginal;

    /** Atributo cdDigitoContaOriginal. */
    private String cdDigitoContaOriginal;

    /** Atributo dsTipoContaOriginal. */
    private String dsTipoContaOriginal;

    /** Atributo dtFloatingPagamento. */
    private String dtFloatingPagamento;

    /** Atributo dtEfetivFloatPgto. */
    private String dtEfetivFloatPgto;

    /** Atributo vlFloatingPagamento. */
    private BigDecimal vlFloatingPagamento;

    /** Atributo cdOperacaoDcom. */
    private Long cdOperacaoDcom;

    /** Atributo dsSituacaoDcom. */
    private String dsSituacaoDcom;

    /** Atributo vlBonfcao. */
    private BigDecimal vlBonfcao;

    /** Atributo vlBonificacao. */
    private BigDecimal vlBonificacao;

    /** Atributo numControleInternoLote. */
    private Long numControleInternoLote;

    /** Atributo cdFuncEmissaoOp. */
    private String cdFuncEmissaoOp;

    /** Atributo dtEmissaoOp. */
    private String dtEmissaoOp;

    /** Atributo cdTerminalCaixaEmissaoOp. */
    private String cdTerminalCaixaEmissaoOp;

    /** Atributo hrEmissaoOp. */
    private String hrEmissaoOp;

    /** Atributo tipoTela. */
    private Integer tipoTela;

    //flag 
    /** Atributo exibeDcom. */
    private Boolean exibeDcom;

    /** The radio favorecido filtro. */
    private String radioFavorecidoFiltro; 

    /** The vl efetivacao debito pagamento. */
    private BigDecimal vlEfetivacaoDebitoPagamento;

    /** The vl efetivacao credito pagamento. */
    private BigDecimal vlEfetivacaoCreditoPagamento;

    /** The vl desconto pagamento. */
    private BigDecimal vlDescontoPagamento;

    /** The hr efetivacao credito pagamento. */
    private String hrEfetivacaoCreditoPagamento;

    /** The hr envio credito pagamento. */
    private String hrEnvioCreditoPagamento;

    /** The cd identificador transferencia pagto. */
    private Integer cdIdentificadorTransferenciaPagto;

    /** The cd mensagem lin extrato. */
    private Integer cdMensagemLinExtrato;

    /** The cd conve cta salarial. */
    private Long cdConveCtaSalarial;

    /** The flag exibe campos. */
    private boolean flagExibeCampos;

    /** Atributo dsEstornoPagamento. */
    private String dsEstornoPagamento;
    
    /** Atributo consultarAutorizantesOcorrencia. */
    private ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> consultarAutorizantesOcorrencia =  
        new ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO>();
    
    /** Atributo itemSelecionadoListaAutorizante. */
    private Integer itemSelecionadoListaAutorizante;
    
    /** Atributo listaRadioAutorizante. */
    private ArrayList<SelectItem> listaRadioAutorizante;
    
    /** Atributo detalharPagtoTed. */
    private DetalharPagtoTEDSaidaDTO detalharPagtoTed = null;
    
    /** Atributo dsNomeReclamante. */
    private String dsNomeReclamante = null;
    
    /** Atributo nrProcessoDepositoJudicial. */
    private String nrProcessoDepositoJudicial = null;
    
    /** Atributo nrPisPasep. */
    private String nrPisPasep = null; 
    
    /** Atributo cdFinalidadeTedDepositoJudicial. */
    private boolean cdFinalidadeTedDepositoJudicial = false;

    /** Atributo detalheOrdemPagamento. */
    private DetalharPagtoOrdemPagtoSaidaDTO detalheOrdemPagamento = null;
    
    /** Atributo listaFormaPagamento. */
    private List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento = null;
    
    private String cdBancoPagamento;
    private String ispb;
	private String cdContaPagamento;
	private String dsISPB;
	private String tipoContaDestinoPagamento;
    private String cdBancoDestinoPagamento;
	private String contaDigitoDestinoPagamento;
	private boolean exibeBancoCredito;
	private String cdContaPagamentoDet;
	 

    /** Atributo numeroInscricaoSacadorAvalista. */
    private String numeroInscricaoSacadorAvalista;

    /** Atributo dsSacadorAvalista. */
    private String dsSacadorAvalista;

    /** Atributo dsTipoSacadorAvalista. */
    private String dsTipoSacadorAvalista;
	
	// METODOS
    /**
     * Iniciar tela.
     *
     * @param evt the evt
     */
    public void iniciarTela(ActionEvent evt) {
        setAgendadosPagosNaoPagos("");

        setDataPagamentoInicial(new Date());
        setDataPagamentoFinal(new Date());

        setChkTipoServico(false);
        setCmbTipoServico(null);
        listarCmbTipoServico();

        setCmbModalidade(null);

        setRdoPesquisa("");

        setRdoFavorecido("");
        setCodigoFavorecido("");
        setInscricaoFavorecido("");
        setCmbTipoFavorecido(null);
        listarCmbTipoFavorecido();

        setBanco("");
        setAgencia("");
        setConta("");
        setContaDig("");

        setRdoBeneficiario("");
        setCodigoBeneficiario("");
        setCmbTipoBenefeciario(null);
        setCmbOrgaoPagador(null);
        listarCmbOrgaoPagador();

        setListaGridPagamentosFavorecido(null);
        setItemSelecionadoLista(null);
        setDisableArgumentosConsulta(false);
        limparFavorecidoManutencao();
    }

    /**
     * Consultar.
     */
    public void consultar() {
        listaGridPagamentosFavorecido = new ArrayList<ConsultarPagamentoFavorecidoSaidaDTO>();
        try {
            ConsultarPagamentoFavorecidoEntradaDTO entradaDTO = new ConsultarPagamentoFavorecidoEntradaDTO();

            entradaDTO.setAgePagoNaoPago(Integer
                .parseInt(getAgendadosPagosNaoPagos()));
            entradaDTO.setDtCreditoPagamentoInicial(FormatarData
                .formataDiaMesAno(getDataPagamentoInicial()));
            entradaDTO.setDtCreditoPagamentoFinal(FormatarData
                .formataDiaMesAno(getDataPagamentoFinal()));

            entradaDTO.setCdProdutoServicoOperacao(getCmbTipoServico());
            entradaDTO.setCdProdutoServicoRelacionado(getCmbModalidade());

            entradaDTO.setCdFavorecidoClientePagador(!getCodigoFavorecido()
                .equals("") ? Long.parseLong(getCodigoFavorecido()) : 0l);
            entradaDTO
            .setCdInscricaoFavorecidoCliente(!getInscricaoFavorecido()
                .equals("") ? Long
                    .parseLong(getInscricaoFavorecido()) : 0l);
            entradaDTO
            .setCdIdentificacaoInscricaoFavorecido(getCmbTipoFavorecido());

            entradaDTO.setCdBanco(!getBanco().equals("") ? Integer
                .parseInt(getBanco()) : 0);
            entradaDTO.setCdAgencia(!getAgencia().equals("") ? Integer
                .parseInt(getAgencia()) : 0);
            entradaDTO.setCdConta(!getConta().equals("") ? Long
                .parseLong(getConta()) : 0L);
            entradaDTO.setCdDigitoConta(getContaDig());

            entradaDTO.setNrUnidadeOrganizacional(getCmbOrgaoPagador());
            entradaDTO
            .setCdBeneficiario(!getCodigoBeneficiario().equals("") ? Long
                .parseLong(getCodigoBeneficiario())
                : 0l);
            entradaDTO.setCdEspecieBeneficioINSS(getCmbTipoBenefeciario());
            entradaDTO.setCdClassificacao("1");
            entradaDTO.setCdBancoSalarial(verificaIntegerNulo(getBancoSalario()));  
            entradaDTO.setCdAgencaSalarial(verificaIntegerNulo(getAgenciaSalario()));
            entradaDTO.setCdContaSalarial(verificaLongNulo(getContaSalario()));     

            if (getCmbTipoConta() != null && getCmbTipoConta() != 0) {
                entradaDTO
                .setCdEmpresaContrato(getListaSituacaoListaDebitoHash()
                    .get(getCmbTipoConta()).getCdEmpresa());
                entradaDTO.setCdTipoConta(getListaSituacaoListaDebitoHash()
                    .get(getCmbTipoConta()).getCdTipo());
                entradaDTO.setNrContrato(getListaSituacaoListaDebitoHash().get(
                    getCmbTipoConta()).getNrContrato());
            } else {
                entradaDTO.setCdEmpresaContrato(0L);
                entradaDTO.setCdTipoConta(0);
                entradaDTO.setNrContrato(0L);
            }

            setFlagExibeCampos(false);
            if(getCmbModalidade() != null && "90200024".equals(getCmbModalidade().toString())){
                setFlagExibeCampos(true);
            }

           
	            if(getRdoPesquisa() != null && getRdoPesquisa().equals("3") ){
	            	if(getCdBancoPagamento() == null || getCdBancoPagamento().equals("")){
	            		entradaDTO.setCdBanco(0);           		
	            	}else {
	            		entradaDTO.setCdBanco(Integer.valueOf(getCdBancoPagamento()));    
					}
	            	entradaDTO.setCdIspbPagtoDestino(getIspb());
	            	entradaDTO.setContaPagtoDestino(getCdContaPagamento());
	            }            


            setListaGridPagamentosFavorecido(getConsultarPagamentosFavorecidoServiceImpl()
                .consultarPagamentoFavorecido(entradaDTO));

            listaControlePagamentosFavorecido = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaGridPagamentosFavorecido().size(); i++) {
                listaControlePagamentosFavorecido.add(new SelectItem(i, ""));
            }
            setItemSelecionadoLista(null);
            setDisableArgumentosConsulta(true);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridPagamentosFavorecido(null);
            setItemSelecionadoLista(null);
            setDisableArgumentosConsulta(false);
        }
    }

    /**
     * Limpar favorecido manutencao.
     */
    public void limparFavorecidoManutencao() {
        setNumeroInscricaoFavorecido("");
        setDsFavorecido("");
        setTipoFavorecido(0);
        setDescTipoFavorecido("");
        setCdFavorecido(0L);
        setSituacaoPagamento("");
        setMotivoPagamento("");
        setInscricaoFavorecido("");
        setCdTipoInscricaoFavorecido("");
        setTipoContaFavorecido("");
        setBancoFavorecido("");
        setAgenciaFavorecido("");
        setContaFavorecido("");
    }

    /**
     * Listar manutencao.
     *
     * @return the string
     */
    public String listarManutencao() {
        String retorno = "CONSULTAR_MANUTENCAO";
        listaConsultarManutencaoFavorecido = new ArrayList<ConsultarManutencaoPagamentoSaidaDTO>();
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        try {
            ConsultarManutencaoPagamentoEntradaDTO entradaDTO = new ConsultarManutencaoPagamentoEntradaDTO();

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
                .getCdEmpresa());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado
                .getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
                .getNrContrato());
            entradaDTO.setCdControlePagamento(registroSelecionado
                .getCdControlePagamento());
            // Confome email do Samuel do dia 03/11 - foi solicitado para passar
            // cdTipoTela para o cdModalidade
            // nesse pdc
            entradaDTO.setCdModalidade(registroSelecionado.getDsTipoTela());
            entradaDTO.setCdTipoCanal(registroSelecionado.getCdTipoCanal());
            entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(this
                .getAgendadosPagosNaoPagos()));
            // entradaDTO.setCdDigitoAgenciaDebito("00");
            entradaDTO.setCdDigitoAgenciaDebito(String
                .valueOf(registroSelecionado.getCdDigitoAgenciaDebito()));
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
            entradaDTO.setCdAgenciaDebito(registroSelecionado
                .getCdAgenciaDebito());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
            entradaDTO.setDtPagamento(registroSelecionado.getDtCraditoPagamento());

            setListaConsultarManutencaoFavorecido(getConsultarPagamentosIndividualServiceImpl()
                .consultarManutencaoPagamento(entradaDTO));

            listaManutencaoFavorecidoControle = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaConsultarManutencaoFavorecido().size(); i++) {
                listaManutencaoFavorecidoControle.add(new SelectItem(i, ""));
            }
            setItemSelecionadoListaManutencaoFavorecido(null);

            if (getListaConsultarManutencaoFavorecido().size() > 0) {

                setDsTipoServico(registroSelecionado
                    .getCdResumoProdutoServico());
                setDsOperacao(registroSelecionado.getDsOperacaoProdutoServico());
                setCdRemessa(getListaConsultarManutencaoFavorecido().get(0)
                    .getNrArquivoRemessaPagamento());
                setNrLote(getListaConsultarManutencaoFavorecido().get(0)
                    .getCdLote());
                setVlPagamentoFormatada(getListaConsultarManutencaoFavorecido()
                    .get(0).getVlPagamentoFormatado());
                setNrPagamento(registroSelecionado.getCdControlePagamento());
                setCdListaDebito(getListaConsultarManutencaoFavorecido().get(0)
                    .getCdListaDebitoPagamento());
                setDsCanal(getListaConsultarManutencaoFavorecido().get(0)
                    .getDsTipoCanal());
                setDtPagamentoManutencao(getListaConsultarManutencaoFavorecido()
                    .get(0).getDtPagamento());
                setDtVencimento(getListaConsultarManutencaoFavorecido().get(0)
                    .getDtVencimentoFormatada());
            }

            setCpfCnpj(registroSelecionado.getCpfCnpjFormatado());
            setNomeRazaoSocial(registroSelecionado.getNmRazaoSocial());
            setEmpresaConglomerado(registroSelecionado.getDsEmpresa());
            setNroContrato(String.valueOf(registroSelecionado.getNrContrato()));
            setDsContrato(getListaConsultarManutencaoFavorecido().get(0)
                .getDsContrato());
            setSituacao(getListaConsultarManutencaoFavorecido().get(0)
                .getDsSituacaoContrato());
            setDsBancoDebito(PgitUtil.concatenarCampos(registroSelecionado
                .getBancoDebitoFormatado(),
                getListaConsultarManutencaoFavorecido().get(0)
                .getDsBancoDebito()));
            setDsAgenciaDebito(PgitUtil.concatenarCampos(registroSelecionado
                .getAgenciaDebitoFormatada(),
                getListaConsultarManutencaoFavorecido().get(0)
                .getDsAgenciaDebito()));
            setContaDebito(registroSelecionado.getContaDebitoFormatada());
            setTipoContaDebito(getListaConsultarManutencaoFavorecido().get(0)
                .getDsTipoContaDebito());

            limparFavorecidoManutencao();

            setSituacaoPagamento(getListaConsultarManutencaoFavorecido().get(0)
                .getDsSituacaoPagamentoCliente());
            setMotivoPagamento(getListaConsultarManutencaoFavorecido().get(0)
                .getDsMotivoPagamentoCliente());

            // Favorecido
            setBancoFavorecido(getListaConsultarManutencaoFavorecido().get(0)
                .getBancoFavorecidoFormatado());
            setAgenciaFavorecido(getListaConsultarManutencaoFavorecido().get(0)
                .getAgenciaFavorecidoFormatada());
            setContaFavorecido(getListaConsultarManutencaoFavorecido().get(0)
                .getContaFavorecidoFormatada());
            setTipoContaFavorecido(getListaConsultarManutencaoFavorecido().get(
                0).getCdTipoContaCreditoFavorecido());
            setDsFavorecido(registroSelecionado.getDsAbrevFavorecido());
            setTipoFavorecido(!PgitUtil.verificaStringNula(
                getListaConsultarManutencaoFavorecido().get(0)
                .getCdTipoInscricaoFavorecido()).equals("") ? Integer
                    .parseInt(getListaConsultarManutencaoFavorecido().get(0)
                        .getCdTipoInscricaoFavorecido())
                        : null);
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(
                getTipoFavorecido(),
                getListaConsultarManutencaoFavorecido().get(0)
                .getInscricaoFavorecido().toString()));
            setCdFavorecido(registroSelecionado.getCdInscricaoFavorecido() == null
                || registroSelecionado.getCdInscricaoFavorecido() == 0L ? null
                    : registroSelecionado.getCdInscricaoFavorecido());

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaConsultarManutencaoFavorecido(null);
            setItemSelecionadoListaManutencaoFavorecido(null);
            // Se der erro permanece na mesma p�gina
            retorno = "";
        }
        return retorno;
    }

    /**
     * Consultar manutencao.
     *
     * @return the string
     */
    public String consultarManutencao() {
        limpaVariaveisDetalhar();
        return listarManutencao();
    }

    /**
     * Listar historico.
     *
     * @return the string
     */
    public String listarHistorico() {

        listaGridHistConsultaSaldo = new ArrayList<ConsultarHistConsultaSaldoSaidaDTO>();
        try {

            ConsultarPagamentoFavorecidoSaidaDTO consultarPagamentoFavorecidoSaidaDTO = getListaGridPagamentosFavorecido()
            .get(getItemSelecionadoLista());

            ConsultarHistConsultaSaldoEntradaDTO entradaDTO = new ConsultarHistConsultaSaldoEntradaDTO();

            entradaDTO.setCdAgenciaDebito(consultarPagamentoFavorecidoSaidaDTO
                .getCdAgenciaDebito());
            entradaDTO.setCdBancoDebito(consultarPagamentoFavorecidoSaidaDTO
                .getCdBancoDebito());
            entradaDTO.setCdContaDebito(consultarPagamentoFavorecidoSaidaDTO
                .getCdContaDebito());
            entradaDTO.setCdDigitoAgencia(Integer
                .toString(consultarPagamentoFavorecidoSaidaDTO
                    .getCdDigitoAgenciaDebito()));
            entradaDTO
            .setCdControlePagamento(consultarPagamentoFavorecidoSaidaDTO
                .getCdControlePagamento());
            entradaDTO.setCdModalidade(consultarPagamentoFavorecidoSaidaDTO
                .getCdProdutoServicoRelacionado());
            entradaDTO
            .setCdPessoaJuridicaContrato(consultarPagamentoFavorecidoSaidaDTO
                .getCdEmpresa());
            entradaDTO.setCdTipoCanal(consultarPagamentoFavorecidoSaidaDTO
                .getCdTipoCanal());
            entradaDTO
            .setCdTipoContratoNegocio(consultarPagamentoFavorecidoSaidaDTO
                .getCdTipoContratoNegocio());
            entradaDTO
            .setNrSequenciaContratoNegocio(consultarPagamentoFavorecidoSaidaDTO
                .getNrContrato());

            entradaDTO.setCdControlePagamento(consultarPagamentoFavorecidoSaidaDTO.getCdControlePagamento());
            entradaDTO.setCdModalidade(consultarPagamentoFavorecidoSaidaDTO.getDsTipoTela());
            entradaDTO.setCdPessoaJuridicaContrato(consultarPagamentoFavorecidoSaidaDTO.getCdEmpresa());
            entradaDTO.setCdTipoCanal(consultarPagamentoFavorecidoSaidaDTO.getCdTipoCanal());
            entradaDTO.setCdTipoContratoNegocio(consultarPagamentoFavorecidoSaidaDTO.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(consultarPagamentoFavorecidoSaidaDTO.getNrContrato());        
            entradaDTO.setDtPagamento(consultarPagamentoFavorecidoSaidaDTO.getDtCraditoPagamento());

            setListaGridHistConsultaSaldo(getConsultarPagamentosIndividualServiceImpl()
                .consultarHistConsultaSaldo(entradaDTO));

            listaControleHistConSaldo = new ArrayList<SelectItem>();

            for (int i = 0; i < getListaGridHistConsultaSaldo().size(); i++) {
                listaControleHistConSaldo.add(new SelectItem(i, ""));
            }

            setDsContrato(getListaGridHistConsultaSaldo().get(0)
                .getDsContrato());
            setSituacao(getListaGridHistConsultaSaldo().get(0)
                .getDsSituacaoContrato());
            setDsBancoDebito(PgitUtil.concatenarCampos(
                consultarPagamentoFavorecidoSaidaDTO
                .getBancoDebitoFormatado(),
                getListaGridHistConsultaSaldo().get(0).getDsBancoDebito()));
            setDsAgenciaDebito(PgitUtil
                .concatenarCampos(consultarPagamentoFavorecidoSaidaDTO
                    .getAgenciaDebitoFormatada(),
                    getListaGridHistConsultaSaldo().get(0)
                    .getDsAgenciaDebito()));
            setContaDebito(consultarPagamentoFavorecidoSaidaDTO
                .getContaDebitoFormatada());
            setTipoContaDebito(getListaGridHistConsultaSaldo().get(0)
                .getDsTipoConta());
            setItemSelecionadoListaHistConSaldo(null);

            limparFavorecidoManutencao();

            setSituacaoPagamento(getListaGridHistConsultaSaldo().get(0)
                .getDsSituacaoPagamentoCliente());
            setMotivoPagamento(getListaGridHistConsultaSaldo().get(0)
                .getDsMotivoPagamentoCliente());

            // Favorecido
            setBancoFavorecido(getListaGridHistConsultaSaldo().get(0)
                .getBancoFavorecidoFormatado());
            setAgenciaFavorecido(getListaGridHistConsultaSaldo().get(0)
                .getAgenciaFavorecidoFormatada());
            setContaFavorecido(getListaGridHistConsultaSaldo().get(0)
                .getContaFavorecidoFormatada());
            setTipoContaFavorecido(getListaGridHistConsultaSaldo().get(0)
                .getCdTipoContaCreditoFavorecido());
            setDsFavorecido(consultarPagamentoFavorecidoSaidaDTO
                .getDsAbrevFavorecido());
            setTipoFavorecido(!PgitUtil.verificaStringNula(
                getListaGridHistConsultaSaldo().get(0)
                .getCdTipoInscricaoFavorecido()).equals("") ? Integer
                    .parseInt(getListaGridHistConsultaSaldo().get(0)
                        .getCdTipoInscricaoFavorecido())
                        : null);
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(
                getTipoFavorecido(), getListaGridHistConsultaSaldo().get(0)
                .getInscricaoFavorecido().toString()));
            setCdFavorecido(consultarPagamentoFavorecidoSaidaDTO
                .getCdInscricaoFavorecido());

            return "HISTORICO_SALDO";

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridHistConsultaSaldo(null);
            setItemSelecionadoListaHistConSaldo(null);
            setItemSelecionadoLista(null);
            return "";
        }

    }

    /**
     * Historico consulta saldo.
     *
     * @return the string
     */
    public String historicoConsultaSaldo() {
        limpaVariaveisDetalhar();
        preencheDadosHistSaldoFav();
        return listarHistorico();

    }

    /**
     * Voltar tela historico.
     *
     * @return the string
     */
    public String voltarTelaHistorico() {
        setItemSelecionadoListaHistConSaldo(null);

        return "VOLTAR";
    }

    /**
     * Voltar historico.
     *
     * @return the string
     */
    public String voltarHistorico() {
        setItemSelecionadoLista(null);

        return "VOLTAR";
    }

    /**
     * Voltar.
     *
     * @return the string
     */
    public String voltar() {
        return "VOLTAR";
    }

    /**
     * Voltar manutencao.
     *
     * @return the string
     */
    public String voltarManutencao() {
        setItemSelecionadoLista(null);

        return "VOLTAR";
    }

    /**
     * Detalhar manutencao favorecido.
     *
     * @return the string
     */
    public String detalharManutencaoFavorecido() {
        setManutencao(true);
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        if (registroSelecionado.getDsTipoTela() == 1) {
            preencheDadosManPagtoCreditoConta();
            return "DETALHE_MAN_CREDITO_CONTA";
        }
        if (registroSelecionado.getDsTipoTela() == 2) {
            preencheDadosManPagtoDebitoConta();
            return "DETALHE_MAN_DEBITO_CONTA";
        }
        if (registroSelecionado.getDsTipoTela() == 3) {
            preencheDadosManPagtoDOC();
            return "DETALHE_MAN_DOC";
        }
        if (registroSelecionado.getDsTipoTela() == 4) {
            preencheDadosManPagtoTED();
            return "DETALHE_MAN_TED";
        }
        if (registroSelecionado.getDsTipoTela() == 5) {
            preencheDadosManPagtoOrdemPagto();
            return "DETALHE_MAN_ORDEM_PAGTO";
        }
        if (registroSelecionado.getDsTipoTela() == 6) {
            preencheDadosManPagtoTituloBradesco();
            return "DETALHE_MAN_TITULOS_BRADESCO";
        }
        if (registroSelecionado.getDsTipoTela() == 7) {
            preencheDadosManPagtoTituloOutrosBancos();
            return "DETALHE_MAN_TITULOS_OUTROS_BANCOS";
        }
        if (registroSelecionado.getDsTipoTela() == 8) {
            preencheDadosManPagtoDARF();
            return "DETALHE_MAN_DARF";
        }
        if (registroSelecionado.getDsTipoTela() == 9) {
            preencheDadosManPagtoGARE();
            return "DETALHE_MAN_GARE";
        }
        if (registroSelecionado.getDsTipoTela() == 10) {
            preencheDadosManPagtoGPS();
            return "DETALHE_MAN_GPS";
        }
        if (registroSelecionado.getDsTipoTela() == 11) {
            preencheDadosManPagtoCodigoBarras();
            return "DETALHE_MAN_CODIGO_BARRAS";
        }
        if (registroSelecionado.getDsTipoTela() == 12) {
            preencheDadosManPagtoDebitoVeiculos();
            return "DETALHE_MAN_DEBITO_VEICULOS";
        }
        if (registroSelecionado.getDsTipoTela() == 13) {
            preencheDadosManPagtoDepIdentificado();
            return "DETALHE_MAN_DEPOSITO_IDENTIFICADO";
        }
        if (registroSelecionado.getDsTipoTela() == 14) {
            preencheDadosManPagtoOrdemCredito();
            return "DETALHE_MAN_ORDEM_CREDITO";
        }

        return "";
    }

    /**
     * Detalhar hist saldo.
     *
     * @return the string
     */
    public String detalharHistSaldo() {
        preencheDadosDetalharHistConSaldo();
        return "DETALHAR_HIST_SALDO";
    }

    // histConSaldoPagIndvFavorecido
    /**
     * Preenche dados hist saldo fav.
     */
    public void preencheDadosHistSaldoFav() {

        ConsultarPagamentoFavorecidoSaidaDTO consultarPagamentoFavorecidoSaidaDTO = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());

        setCpfCnpj(consultarPagamentoFavorecidoSaidaDTO.getCpfCnpjFormatado());
        setNomeRazaoSocial(consultarPagamentoFavorecidoSaidaDTO
            .getNmRazaoSocial());
        setEmpresaConglomerado(consultarPagamentoFavorecidoSaidaDTO
            .getDsEmpresa());
        setNroContrato(String.valueOf(consultarPagamentoFavorecidoSaidaDTO
            .getNrContrato()));

        setDsTipoServico(consultarPagamentoFavorecidoSaidaDTO
            .getCdResumoProdutoServico());
        setDsOperacao(consultarPagamentoFavorecidoSaidaDTO
            .getDsOperacaoProdutoServico());
        setVlPagamentoFormatada(consultarPagamentoFavorecidoSaidaDTO
            .getVlPagamentoFormatado());
        setNrPagamento(consultarPagamentoFavorecidoSaidaDTO
            .getCdControlePagamento());

    }

    // detHistConsultaSaldoPagIndFavorecido
    /**
     * Preenche dados detalhar hist con saldo.
     */
    public void preencheDadosDetalharHistConSaldo() {

        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionadoConsultar = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());

        ConsultarHistConsultaSaldoSaidaDTO registroSelecionadoHistorico = getListaGridHistConsultaSaldo()
        .get(getItemSelecionadoListaHistConSaldo());

        DetalharHistConsultaSaldoEntradaDTO entradaDTO = new DetalharHistConsultaSaldoEntradaDTO();

        /* Dados de Entrada */
        entradaDTO.setCdAgencia(registroSelecionadoConsultar
            .getCdAgenciaDebito());
        entradaDTO.setCdBanco(registroSelecionadoConsultar.getCdBancoDebito());
        entradaDTO.setCdConta(registroSelecionadoConsultar.getCdContaDebito());
        entradaDTO.setCdControlePagamento(registroSelecionadoConsultar
            .getCdControlePagamento());
        entradaDTO.setCdModalidade(registroSelecionadoConsultar
            .getCdProdutoServicoRelacionado());
        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar
            .getCdEmpresa());
        entradaDTO
        .setCdTipoCanal(registroSelecionadoConsultar.getCdTipoCanal());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar
            .getCdTipoContratoNegocio());
        entradaDTO.setHrConsultasSaldoPagamento(registroSelecionadoHistorico
            .getHrConsultasSaldoPagamento());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar
            .getNrContrato());
        entradaDTO.setDtPagamento(registroSelecionadoConsultar.getDtCraditoPagamento());

        DetalharHistConsultaSaldoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharHistConsultaSaldo(entradaDTO);

        /* Dados de Sa�da */
        // Cliente
        setCpfCnpj(registroSelecionadoConsultar.getCpfCnpjFormatado());
        setNomeRazaoSocial(registroSelecionadoConsultar.getNmRazaoSocial());
        setEmpresaConglomerado(registroSelecionadoConsultar.getDsEmpresa());
        setNroContrato(String.valueOf(registroSelecionadoConsultar
            .getNrContrato()));
        setDsContrato(registroSelecionadoHistorico.getDsContrato());
        setSituacao(registroSelecionadoHistorico.getDsSituacaoContrato());

        // Conta de D�bito
        setDsBancoDebito(PgitUtil.concatenarCampos(registroSelecionadoConsultar
            .getBancoDebitoFormatado(), registroSelecionadoHistorico
            .getDsBancoDebito()));
        setDsAgenciaDebito(PgitUtil.concatenarCampos(
            registroSelecionadoConsultar.getAgenciaDebitoFormatada(),
            registroSelecionadoHistorico.getDsAgenciaDebito()));
        setContaDebito(registroSelecionadoConsultar.getContaDebitoFormatada());
        setTipoContaDebito(registroSelecionadoHistorico.getDsTipoConta());

        // Pagamento
        setCdRemessa(saidaDTO.getNrArquivoRemessaPagamento());
        setCdLote(saidaDTO.getCdPessoaLoteRemessa());
        setVlPagamento(saidaDTO.getVlPagamento());
        setDtPagamento(saidaDTO.getDtCreditoPagamento());
        setCdListaDebito(saidaDTO.getCdListaDebitoPagamento());
        setDsCanal(saidaDTO.getDsCanal());
        setDtVencimento(saidaDTO.getDtVencimento());

        // Consulta de Saldo
        setDtaHraConSaldo(registroSelecionadoHistorico.getDataHoraFormatada());
        setSaldoAgregCDB(saidaDTO.getVlSaldoAgregadoCdb());
        setSaldoAgregCredRotativo(saidaDTO.getVlSaldoCreditoRotativo());
        setSaldoAgregFundo(saidaDTO.getVlSaldoAgregadoFundo());
        setSaldoAgregPoupanca(saidaDTO.getVlSaldoAgregadoPoupanca());
        setSaldoAntLancto(saidaDTO.getVlSaldoAnteriorLancamento());
        setSinal(saidaDTO.getSinal());   

        setSaldoOperacional(saidaDTO.getVlSaldoOperacional());
        setSaldoVincAdm(saidaDTO.getVlSaldoVinculadoAdministrativo());
        setSaldoVincAdmLP(saidaDTO.getVlSaldoVinculadoAdministrativoLp());
        setSaldoVincComReserva(saidaDTO.getVlSaldoComReserva());
        setSaldoVincJudicial(saidaDTO.getVlSaldoVinculadoJudicial());
        setSaldoVincSeg(saidaDTO.getVlSaldoVinculadoSeguranca());
        setSaldoVincSemReserva(saidaDTO.getVlSaldoSemReserva());
        setNumControleInternoLote(saidaDTO.getNumControleInternoLote());


        limparFavorecidoManutencao();

        setSituacaoPagamento(registroSelecionadoHistorico
            .getDsSituacaoPagamentoCliente());
        setMotivoPagamento(registroSelecionadoHistorico
            .getDsMotivoPagamentoCliente());

        // Favorecido
        setBancoFavorecido(registroSelecionadoHistorico
            .getBancoFavorecidoFormatado());
        setAgenciaFavorecido(registroSelecionadoHistorico
            .getAgenciaFavorecidoFormatada());
        setContaFavorecido(registroSelecionadoHistorico
            .getContaFavorecidoFormatada());
        setTipoContaFavorecido(registroSelecionadoHistorico
            .getCdTipoContaCreditoFavorecido());
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(Integer
            .parseInt(registroSelecionadoHistorico
                .getCdTipoInscricaoFavorecido()),
                registroSelecionadoHistorico.getInscricaoFavorecido()
                .toString()));
        setDsFavorecido(registroSelecionadoConsultar.getDsAbrevFavorecido());
        setTipoFavorecido(!PgitUtil.verificaStringNula(
            registroSelecionadoHistorico.getCdTipoInscricaoFavorecido())
            .equals("") ? Integer.parseInt(registroSelecionadoHistorico
                .getCdTipoInscricaoFavorecido()) : null);
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setCdFavorecido(registroSelecionadoConsultar.getCdInscricaoFavorecido());

        // Trilha de Auditoria
        setDataHoraManutencao(PgitUtil.concatenarCampos(saidaDTO
            .getDtManutencao(), saidaDTO.getHrManutencao()));
        setDataHoraInclusao(PgitUtil.concatenarCampos(saidaDTO.getDtInclusao(),
            saidaDTO.getHrInclusao()));

        setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() != 0 ? PgitUtil
            .concatenarCampos(saidaDTO.getCdTipoCanalInclusao(), saidaDTO
                .getDsTipoCanalInclusao()) : "");
        setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() == 0 ? ""
            : PgitUtil.concatenarCampos(
                saidaDTO.getCdTipoCanalManutencao(), saidaDTO
                .getDsTipoCanalManutencao()));

        setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
        setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());

        // confirmar se � esse valor vindo do pdc que preenche esse campo
        setComplementoManutencao(saidaDTO.getCdFluxoManutencao() == null
            || saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO
                .getCdFluxoManutencao());
        setComplementoInclusao(saidaDTO.getCdFluxoInclusao() == null
            || saidaDTO.getCdFluxoInclusao().equals("0") ? "" : saidaDTO
                .getCdFluxoInclusao());
    }

    /**
     * Voltar detalhe.
     *
     * @return the string
     */
    public String voltarDetalhe() {
        if (!isManutencao()) {
            setItemSelecionadoLista(null);
            return "VOLTAR1";
        } else {
            if (isManutencao()) {
                setItemSelecionadoListaManutencaoFavorecido(null);
                return "VOLTAR2";
            }
        }
        return "";
    }

    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {
        limpaVariaveisDetalhar();
        setManutencao(false);
        tipoTela = getListaGridPagamentosFavorecido().get(getItemSelecionadoLista()).getDsTipoTela();

        if (tipoTela == 1) {
            preencheDadosPagtoCreditoConta();
            return "DETALHE_CREDITO_CONTA";
        }
        if (tipoTela == 2) {
            preencheDadosPagtoDebitoConta();
            return "DETALHE_DEBITO_CONTA";
        }
        if (tipoTela == 3) {
            preencheDadosPagtoDOC();
            return "DETALHE_DOC";
        }
        if (tipoTela == 4) {
            preencheDadosPagtoTED();
            return "DETALHE_TED";
        }
        if (tipoTela == 5) {
            preencheDadosPagtoOrdemPagto();
            return "DETALHE_ORDEM_PAGTO";
        }
        if (tipoTela == 6) {
            preencheDadosPagtoTituloBradesco();
            return "DETALHE_TITULOS_BRADESCO";
        }
        if (tipoTela == 7) {
            preencheDadosPagtoTituloOutrosBancos();
            return "DETALHE_TITULOS_OUTROS_BANCOS";
        }
        if (tipoTela == 8) {
            preencheDadosPagtoDARF();
            return "DETALHE_DARF";
        }
        if (tipoTela == 9) {
            preencheDadosPagtoGARE();
            return "DETALHE_GARE";
        }
        if (tipoTela == 10) {
            preencheDadosPagtoGPS();
            return "DETALHE_GPS";
        }
        if (tipoTela == 11) {
            preencheDadosPagtoCodigoBarras();
            return "DETALHE_CODIGO_BARRAS";
        }
        if (tipoTela == 12) {
            preencheDadosPagtoDebitoVeiculos();
            return "DETALHE_DEBITO_VEICULOS";
        }
        if (tipoTela == 13) {
            preencheDadosPagtoDepIdentificado();
            return "DETALHE_DEPOSITO_IDENTIFICADO";
        }
        if (tipoTela == 14) {
            preencheDadosPagtoOrdemCredito();
            return "DETALHE_ORDEM_CREDITO";
        }

        return "";
    }

    /**
     * Limpar favorecido beneficiario.
     */
    public void limparFavorecidoBeneficiario() {
        setNumeroInscricaoFavorecido("");
        setTipoFavorecido(0);
        setDsFavorecido("");
        setCdFavorecido(null);
        setDsBancoFavorecido("");
        setDsAgenciaFavorecido("");
        setDescTipoFavorecido("");
        setDsContaFavorecido("");
        setDsTipoContaFavorecido("");
        setNumeroInscricaoBeneficiario("");
        setCdTipoBeneficiario(0);
        setNomeBeneficiario("");
        setDtNascimentoBeneficiario("");
        setDsTipoBeneficiario("");
    }

    // Inicio - Telas de detalhe - preenche dados
    // Detalhar Pagamentos Individuais - Cr�dito em Conta
    /**
     * Preenche dados pagto credito conta.
     */
    public void preencheDadosPagtoCreditoConta() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCreditoConta(entradaDTO);

        // Cabe�alho
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null
                || saidaDTO.getCdFavorecido().equals("") ? null : Long
                    .parseLong(saidaDTO.getCdFavorecido()));
            setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO
                .getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
                false));
            setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
                .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
                saidaDTO.getDsAgenciaFavorecido(), false));
            setDsContaFavorecido(PgitUtil.formatConta(saidaDTO
                .getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
                false));
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("");
            setExibeDcom(false);

        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
        setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setCdSituacaoTranferenciaAutomatica(saidaDTO
            .getCdSituacaoTranferenciaAutomatica());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

        // Favorecidos
        setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
        setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - "
            + saidaDTO.getDsBancoOriginal());
        setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
        setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
        setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoAgenciaOriginal() + " - "
            + saidaDTO.getDsAgenciaOriginal());
        setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
        setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoContaOriginal());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());

        SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
        SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        setVlEfetivacaoDebitoPagamento(saidaDTO.getVlEfetivacaoDebitoPagamento());
        setVlEfetivacaoCreditoPagamento(saidaDTO.getVlEfetivacaoCreditoPagamento());
        setVlDescontoPagamento(saidaDTO.getVlDescontoPagamento());
        try {
            setHrEfetivacaoCreditoPagamento(formato2.format(formato1.parse(saidaDTO.getHrEfetivacaoCreditoPagamento())));
            setHrEnvioCreditoPagamento(formato2.format(formato1.parse(saidaDTO.getHrEnvioCreditoPagamento())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setCdIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto());
        if ("".equals(saidaDTO.getDsIdentificadorTransferenciaPagto()) || 
                        "".equals(saidaDTO.getCdIdentificadorTransferenciaPagto())){
            setDsIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto() 
                + saidaDTO.getDsIdentificadorTransferenciaPagto());
        }else{
            setDsIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto() 
                +" - "+ saidaDTO.getDsIdentificadorTransferenciaPagto());
        }
        setCdMensagemLinExtrato(saidaDTO.getCdMensagemLinExtrato());
        setCdConveCtaSalarial(saidaDTO.getCdConveCtaSalarial());
        setDsEstornoPagamento(saidaDTO.getDsEstornoPagamento());
        
        
        setTipoContaDestinoPagamento(saidaDTO.getDsTipoContaDestino());
        setCdBancoDestinoPagamento(saidaDTO.getCdBancoDestino().toString());
        setContaDigitoDestinoPagamento(saidaDTO.getContaDestinoFormatada());
        setDsISPB(saidaDTO.getCdIspbPagtoDestino() + " - " + saidaDTO.getDsBancoDestino());
        setCdContaPagamentoDet(saidaDTO.getContaPagtoDestino());
        
        if(saidaDTO.getCdIndicadorModalidade() == 3){
        	setExibeBancoCredito(true);
        	if(saidaDTO.getContaPagtoDestino().equals("0")){
        		setCdBancoDestinoPagamento(null);
        		setContaDigitoDestinoPagamento(null);
        		setTipoContaDestinoPagamento(null);
        		setDsISPB(null);
        		setCdContaPagamentoDet(null);
        	}else {
        		setCdBancoDestino(null);
        		setAgenciaDigitoDestino(null);
        		setContaDigitoDestino(null);
        		setTipoContaDestino(null);
        	}
        	
        }else{
        	setExibeBancoCredito(false);
        }
        	
    }

    // Detalhar Pagamentos Individuais - D�bito em conta
    /**
     * Preenche dados pagto debito conta.
     */
    public void preencheDadosPagtoDebitoConta() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoConta(entradaDTO);

        // DadosCliente
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // DadosContaDebito
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido(), saidaDTO
            .getNmInscriFavorecido()));
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
            saidaDTO.getCdDigContaCredito(), false));
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - DOC
    /**
     * Preenche dados pagto doc.
     */
    public void preencheDadosPagtoDOC() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDOC(entradaDTO);

        // Cabe�alho
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null
                || saidaDTO.getCdFavorecido().equals("") ? null : Long
                    .parseLong(saidaDTO.getCdFavorecido()));
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
        setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
        setIdentificacaoTransferenciaDoc(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - TED
    /**
     * Preenche dados pagto ted.
     */
    public void preencheDadosPagtoTED() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        detalharPagtoTed = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTED(entradaDTO);

        // CLIENTE
        setCpfCnpj(detalharPagtoTed.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(detalharPagtoTed.getNomeCliente());
        setEmpresaConglomerado(detalharPagtoTed.getDsPessoaJuridicaContrato());
        setNroContrato(detalharPagtoTed.getNrSequenciaContratoNegocio());
        setDsContrato(detalharPagtoTed.getDsContrato());
        setSituacao(detalharPagtoTed.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(detalharPagtoTed.getBancoDebitoFormatado());
        setDsAgenciaDebito(detalharPagtoTed.getAgenciaDebitoFormatada());
        setContaDebito(detalharPagtoTed.getContaDebitoFormatada());
        setTipoContaDebito(detalharPagtoTed.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (detalharPagtoTed.getCdIndicadorModalidadePgit() == 1 || detalharPagtoTed.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(detalharPagtoTed
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(detalharPagtoTed.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(detalharPagtoTed.getDsNomeFavorecido());
            setCdFavorecido(detalharPagtoTed.getCdFavorecido() == null
                || detalharPagtoTed.getCdFavorecido().equals("") ? null : Long
                    .parseLong(detalharPagtoTed.getCdFavorecido()));
            setDsBancoFavorecido(detalharPagtoTed.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(detalharPagtoTed.getAgenciaCreditoFormatada());
            setDsContaFavorecido(detalharPagtoTed.getContaCreditoFormatada());
            setDsTipoContaFavorecido(detalharPagtoTed.getDsTipoContaFavorecido());
            setExibeDcom(detalharPagtoTed.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(detalharPagtoTed
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(detalharPagtoTed.getCdTipoInscriBeneficio());
            setNomeBeneficiario(detalharPagtoTed.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(detalharPagtoTed.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(detalharPagtoTed.getDsModalidadeRelacionado());
        setCdRemessa(detalharPagtoTed.getNrSequenciaArquivoRemessa());
        setCdLote(detalharPagtoTed.getNrLoteArquivoRemessa());
        setNrPagamento(detalharPagtoTed.getCdControlePagamento());
        setCdListaDebito(detalharPagtoTed.getCdListaDebito());
        setDtAgendamento(detalharPagtoTed.getDtAgendamento());
        setDtPagamento(detalharPagtoTed.getDtPagamento());
        setDtVencimento(detalharPagtoTed.getDtVencimento());
        setCdIndicadorEconomicoMoeda(detalharPagtoTed.getDsMoeda());
        setQtMoeda(detalharPagtoTed.getQtMoeda());
        setVlrAgendamento(detalharPagtoTed.getVlAgendado());
        setVlrEfetivacao(detalharPagtoTed.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(detalharPagtoTed.getCdSituacaoPagamento());
        setDsMotivoSituacao(detalharPagtoTed.getDsMotivoSituacao());
        setDsPagamento(detalharPagtoTed.getDsPagamento());
        setNrDocumento(detalharPagtoTed.getNrDocumento());
        setTipoDocumento(detalharPagtoTed.getDsTipoDocumento());
        setCdSerieDocumento(detalharPagtoTed.getCdSerieDocumento());
        setVlDocumento(detalharPagtoTed.getVlDocumento());
        setDtEmissaoDocumento(detalharPagtoTed.getDtEmissaoDocumento());
        setDsUsoEmpresa(detalharPagtoTed.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(detalharPagtoTed.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(detalharPagtoTed.getDsMensagemSegundaLinha());
        setCdBancoCredito(String.valueOf(detalharPagtoTed.getCdBancoCredito()));
        setDsBancoCredito(detalharPagtoTed.getDsBancoCredito());
        setCdIspbPagamento(detalharPagtoTed.getCdIspbPagamento());
        setDsNomeReclamante(detalharPagtoTed.getDsNomeReclamante());
        setNrProcessoDepositoJudicial(detalharPagtoTed.getNrProcessoDepositoJudicial());
        setNrPisPasep(NumberUtils.formatarNumeroPisPasep(detalharPagtoTed.getNrPisPasep()));
        setCdFinalidadeTedDepositoJudicial(detalharPagtoTed.isCdFinalidadeTedDepositoJudicial());        
        setAgenciaDigitoCredito(detalharPagtoTed.getAgenciaCreditoFormatada());
        setContaDigitoCredito(detalharPagtoTed.getContaCreditoFormatada());
        setTipoContaCredito(detalharPagtoTed.getDsTipoContaFavorecido());
        setCamaraCentralizadora(detalharPagtoTed.getCdCamaraCentral());
        setFinalidadeTed(PgitUtil.concatenarCampos(detalharPagtoTed.getCdFinalidadeTed(),detalharPagtoTed.getDsFinalidadeDocTed(), " - "));
        setIdentificacaoTransferenciaTed(detalharPagtoTed
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(detalharPagtoTed.getDsidentificacaoTitularPagto());
        setVlDesconto(detalharPagtoTed.getVlDesconto());
        setVlAbatimento(detalharPagtoTed.getVlAbatidoCredito());
        setVlMulta(detalharPagtoTed.getVlMultaCredito());
        setVlMora(detalharPagtoTed.getVlMoraJuros());
        setVlDeducao(detalharPagtoTed.getVlOutrasDeducoes());
        setVlAcrescimo(detalharPagtoTed.getVlOutroAcrescimo());
        setVlImpostoRenda(detalharPagtoTed.getVlIrCredito());
        setVlIss(detalharPagtoTed.getVlIssCredito());
        setVlIof(detalharPagtoTed.getVlIofCredito());
        setVlInss(detalharPagtoTed.getVlInssCredito());
        setCdIndentificadorJudicial(detalharPagtoTed.getCdIndentificadorJudicial());
        setDtDevolucaoEstorno(detalharPagtoTed.getDtDevolucaoEstorno());
        setDtFloatingPagamento(detalharPagtoTed.getDtFloatingPagamento());
        setDtEfetivFloatPgto(detalharPagtoTed.getDtEfetivFloatPgto());
        setVlFloatingPagamento(detalharPagtoTed.getVlFloatingPagamento());
        setCdOperacaoDcom(detalharPagtoTed.getCdOperacaoDcom());
        setDsSituacaoDcom(detalharPagtoTed.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(detalharPagtoTed.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(detalharPagtoTed.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(detalharPagtoTed.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(detalharPagtoTed.getComplementoInclusaoFormatado());
        setDataHoraManutencao(detalharPagtoTed.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(detalharPagtoTed.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(detalharPagtoTed.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(detalharPagtoTed.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - Ordem de Pagamento
    /**
     * Preenche dados pagto ordem pagto.
     */
    public void preencheDadosPagtoOrdemPagto() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        detalheOrdemPagamento = getConsultarPagamentosIndividualServiceImpl().detalharPagtoOrdemPagto(entradaDTO);

        // CLIENTE
        setCpfCnpj(detalheOrdemPagamento.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(detalheOrdemPagamento.getNomeCliente());
        setEmpresaConglomerado(detalheOrdemPagamento.getDsPessoaJuridicaContrato());
        setNroContrato(detalheOrdemPagamento.getNrSequenciaContratoNegocio());
        setDsContrato(detalheOrdemPagamento.getDsContrato());
        setSituacao(detalheOrdemPagamento.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(detalheOrdemPagamento.getBancoDebitoFormatado());
        setDsAgenciaDebito(detalheOrdemPagamento.getAgenciaDebitoFormatada());
        setContaDebito(detalheOrdemPagamento.getContaDebitoFormatada());
        setTipoContaDebito(detalheOrdemPagamento.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();

        exibeBeneficiario = false;
        if (detalheOrdemPagamento.getCdIndicadorModalidadePgit() == 1
            || detalheOrdemPagamento.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(detalheOrdemPagamento
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(detalheOrdemPagamento.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(detalheOrdemPagamento.getDsNomeFavorecido());
            setCdFavorecido(detalheOrdemPagamento.getCdFavorecido() == null
                || detalheOrdemPagamento.getCdFavorecido().equals("") ? null : Long
                    .parseLong(detalheOrdemPagamento.getCdFavorecido()));
            setDsBancoFavorecido(detalheOrdemPagamento.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(detalheOrdemPagamento.getAgenciaCreditoFormatada());
            setDsContaFavorecido(detalheOrdemPagamento.getContaCreditoFormatada());
            setDsTipoContaFavorecido(detalheOrdemPagamento.getDsTipoContaFavorecido());
            setExibeDcom(detalheOrdemPagamento.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(detalheOrdemPagamento
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(detalheOrdemPagamento.getCdTipoInscriBeneficio());
            setNomeBeneficiario(detalheOrdemPagamento.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(detalheOrdemPagamento.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(detalheOrdemPagamento.getDsModalidadeRelacionado());
        setCdRemessa(detalheOrdemPagamento.getNrSequenciaArquivoRemessa());
        setCdLote(detalheOrdemPagamento.getNrLoteArquivoRemessa());
        setNrPagamento(detalheOrdemPagamento.getCdControlePagamento());
        setCdListaDebito(detalheOrdemPagamento.getCdListaDebito());
        setDtAgendamento(detalheOrdemPagamento.getDtAgendamento());
        setDtPagamento(detalheOrdemPagamento.getDtPagamento());
        setDtVencimento(detalheOrdemPagamento.getDtVencimento());
        setCdIndicadorEconomicoMoeda(detalheOrdemPagamento.getDsMoeda());
        setQtMoeda(detalheOrdemPagamento.getQtMoeda());
        setVlrAgendamento(detalheOrdemPagamento.getVlAgendado());
        setVlrEfetivacao(detalheOrdemPagamento.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(detalheOrdemPagamento.getCdSituacaoPagamento());
        setDsMotivoSituacao(detalheOrdemPagamento.getDsMotivoSituacao());
        setDsPagamento(detalheOrdemPagamento.getDsPagamento());
        setNrDocumento(detalheOrdemPagamento.getNrDocumento());
        setTipoDocumento(detalheOrdemPagamento.getDsTipoDocumento());
        setCdSerieDocumento(detalheOrdemPagamento.getCdSerieDocumento());
        setVlDocumento(detalheOrdemPagamento.getVlDocumento());
        setDtEmissaoDocumento(detalheOrdemPagamento.getDtEmissaoDocumento());
        setDsUsoEmpresa(detalheOrdemPagamento.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(detalheOrdemPagamento.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(detalheOrdemPagamento.getDsMensagemSegundaLinha());
        setCdBancoCredito(detalheOrdemPagamento.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(detalheOrdemPagamento.getAgenciaCreditoFormatada());
        setContaDigitoCredito(detalheOrdemPagamento.getContaCreditoFormatada());
        setTipoContaCredito(detalheOrdemPagamento.getDsTipoContaFavorecido());
        setNumeroOp(detalheOrdemPagamento.getNrOperacao());
        setDataExpiracaoCredito(detalheOrdemPagamento.getDtExpedicaoCredito());
        setInstrucaoPagamento(detalheOrdemPagamento.getCdInsPagamento());
        setNumeroCheque(detalheOrdemPagamento.getNrCheque());
        setSerieCheque(detalheOrdemPagamento.getNrSerieCheque());
        setIndicadorAssociadoUnicaAgencia(detalheOrdemPagamento.getDsUnidadeAgencia());
        setIdentificadorTipoRetirada(detalheOrdemPagamento.getDsIdentificacaoTipoRetirada());
        setIdentificadorRetirada(detalheOrdemPagamento.getDsIdentificacaoRetirada());
        setDataRetirada(detalheOrdemPagamento.getDtRetirada());
        setVlImpostoMovFinanceira(detalheOrdemPagamento.getVlImpostoMovimentacaoFinanceira());
        setVlDesconto(detalheOrdemPagamento.getVlDescontoCredito());
        setVlAbatimento(detalheOrdemPagamento.getVlAbatidoCredito());
        setVlMulta(detalheOrdemPagamento.getVlMultaCredito());
        setVlMora(detalheOrdemPagamento.getVlMoraJuros());
        setVlDeducao(detalheOrdemPagamento.getVlOutrasDeducoes());
        setVlAcrescimo(detalheOrdemPagamento.getVlOutroAcrescimo());
        setVlImpostoRenda(detalheOrdemPagamento.getVlIrCredito());
        setVlIss(detalheOrdemPagamento.getVlIssCredito());
        setVlIof(detalheOrdemPagamento.getVlIofCredito());
        setVlInss(detalheOrdemPagamento.getVlInssCredito());
        setDtDevolucaoEstorno(detalheOrdemPagamento.getDtDevolucaoEstorno());
        setDtFloatingPagamento(detalheOrdemPagamento.getDtFloatingPagamento());
        setDtEfetivFloatPgto(detalheOrdemPagamento.getDtEfetivFloatPgto());
        setVlFloatingPagamento(detalheOrdemPagamento.getVlFloatingPagamento());
        setCdOperacaoDcom(detalheOrdemPagamento.getCdOperacaoDcom());
        setDsSituacaoDcom(detalheOrdemPagamento.getDsSituacaoDcom());

        // trilhaAuditoria
        setCdFuncEmissaoOp(detalheOrdemPagamento.getCdFuncEmissaoOp());
        setDtEmissaoOp(detalheOrdemPagamento.getDtEmissaoOp());
        setCdTerminalCaixaEmissaoOp(detalheOrdemPagamento.getCdTerminalCaixaEmissaoOp());
        setHrEmissaoOp(detalheOrdemPagamento.getHrEmissaoOp());
        setDataHoraInclusao(detalheOrdemPagamento.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(detalheOrdemPagamento.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(detalheOrdemPagamento.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(detalheOrdemPagamento.getComplementoInclusaoFormatado());
        setDataHoraManutencao(detalheOrdemPagamento.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(detalheOrdemPagamento.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(detalheOrdemPagamento.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(detalheOrdemPagamento.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - Titulos Bradesco
    /**
     * Preenche dados pagto titulo bradesco.
     */
    public void preencheDadosPagtoTituloBradesco() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloBradesco(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // SACADOR/AVALISTA
        setNumeroInscricaoSacadorAvalista(saidaDTO.getNumeroInscricaoSacadorAvalista());
        setDsSacadorAvalista(saidaDTO.getDsSacadorAvalista());
        setDsTipoSacadorAvalista(saidaDTO.getDsTipoSacadorAvalista());            
        
        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());
        setVlBonificacao(saidaDTO.getVlBonificacao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
    }

    // Detalhar Pagamentos Individuais - Titulos Outros Bancos
    /**
     * Preenche dados pagto titulo outros bancos.
     */
    public void preencheDadosPagtoTituloOutrosBancos() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloOutrosBancos(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // SACADOR/AVALISTA
        setNumeroInscricaoSacadorAvalista(saidaDTO.getNumeroInscricaoSacadorAvalista());
        setDsSacadorAvalista(saidaDTO.getDsSacadorAvalista());
        setDsTipoSacadorAvalista(saidaDTO.getDsTipoSacadorAvalista());            
        
        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());
        setVlBonificacao(saidaDTO.getVlBonificacao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - DARF
    /**
     * Preenche dados pagto darf.
     */
    public void preencheDadosPagtoDARF() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDARF(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getCdTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdDanoExercicio());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
        setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - GARE
    /**
     * Preenche dados pagto gare.
     */
    public void preencheDadosPagtoGARE() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoGARE(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
        setInscricaoEstadualContribuinte(saidaDTO
            .getCdInscricaoEstadualContribuinte());
        setCodigoReceita(saidaDTO.getCdReceita());
        setNumeroReferencia(saidaDTO.getNrReferencia());
        setNumeroCotaParcela(saidaDTO.getNrCota());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
        setDataReferencia(saidaDTO.getDtReferencia());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcela());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - GPS
    /**
     * Preenche dados pagto gps.
     */
    public void preencheDadosPagtoGPS() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoGPS(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setDataCompetencia(saidaDTO.getDsMesAnoCompt());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
        setCodigoInss(saidaDTO.getCdInss());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - C�digo de Barras
    /**
     * Preenche dados pagto codigo barras.
     */
    public void preencheDadosPagtoCodigoBarras() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCodigoBarras(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getNrTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
        setCodigoTributo(saidaDTO.getCdTributoArrecadado());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
        setPercentualReceita(saidaDTO.getCdPercentualTributo());
        setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setDataCompetencia(saidaDTO.getDtCompensacao());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsObservacao());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - D�bito de Veiculos
    /**
     * Preenche dados pagto debito veiculos.
     */
    public void preencheDadosPagtoDebitoVeiculos() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoVeiculos(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
        setAnoExercicio(saidaDTO.getDtAnoExercTributo());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
        setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
        setUfTributo(saidaDTO.getCdUfTributo());
        setNumeroNsu(saidaDTO.getNrNsuTributo());
        setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
        setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
        setVlPrincipal(saidaDTO.getVlPrincipalTributo());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - Deposito Identificado
    /**
     * Preenche dados pagto dep identificado.
     */
    public void preencheDadosPagtoDepIdentificado() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDepIdentificado(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
        setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
        setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
        setSequenciaProdutoDepositoIdentificado(saidaDTO
            .getCdSequenciaProdutoDepositoId());
        setBancoCartaoDepositoIdentificado(saidaDTO
            .getCdBancoCartaoDepositanteId());
        setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
        setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
        setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
        setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
        setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - Ordem de Cr�dito
    /**
     * Preenche dados pagto ordem credito.
     */
    public void preencheDadosPagtoOrdemCredito() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer
                .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoOrdemCredito(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCodigoPagador(saidaDTO.getCdPagador());
        setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Cr�dito em Conta (Alterado
    // para ultilizar
    // detalharPagtoCreditoConta)
    /**
     * Preenche dados man pagto credito conta.
     */
    public void preencheDadosManPagtoCreditoConta() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCreditoConta(entradaDTO);

        // Cabe�alho
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null
                || saidaDTO.getCdFavorecido().equals("") ? null : Long
                    .parseLong(saidaDTO.getCdFavorecido()));
            setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO
                .getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
                false));
            setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
                .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
                saidaDTO.getDsAgenciaFavorecido(), false));
            setDsContaFavorecido(PgitUtil.formatConta(saidaDTO
                .getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
                false));
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
        setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setCdSituacaoTranferenciaAutomatica(saidaDTO
            .getCdSituacaoTranferenciaAutomatica());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        // Favorecidos
        setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
        setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - "
            + saidaDTO.getDsBancoOriginal());
        setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
        setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
        setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoAgenciaOriginal() + " - "
            + saidaDTO.getDsAgenciaOriginal());
        setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
        setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoContaOriginal());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
        
        setTipoContaDestinoPagamento(saidaDTO.getDsTipoContaDestino());
        setCdBancoDestinoPagamento(saidaDTO.getCdBancoDestino().toString());
        setContaDigitoDestinoPagamento(saidaDTO.getContaDestinoFormatada());
        setDsISPB(saidaDTO.getCdIspbPagtoDestino() + " - " + saidaDTO.getDsBancoDestino());
        setCdContaPagamentoDet(saidaDTO.getContaPagtoDestino());
        
        if(saidaDTO.getCdIndicadorModalidade() == 3){
        	setExibeBancoCredito(true);
        	if(saidaDTO.getContaPagtoDestino().equals("0")){
        		setCdBancoDestinoPagamento(null);
        		setContaDigitoDestinoPagamento(null);
        		setTipoContaDestinoPagamento(null);
        		setDsISPB(null);
        		setCdContaPagamentoDet(null);
        	}else {
        		setCdBancoDestino(null);
        		setAgenciaDigitoDestino(null);
        		setContaDigitoDestino(null);
        		setTipoContaDestino(null);
        	}
        	
        }else{
        	setExibeBancoCredito(false);
        }
    }

    // Detalhar Manuten��o Pagamentos Individuais - D�bito em conta
    /**
     * Preenche dados man pagto debito conta.
     */
    public void preencheDadosManPagtoDebitoConta() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoDebitoContaEntradaDTO entradaDTO = new DetalharManPagtoDebitoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDebitoConta(entradaDTO);

        // Cabe�alho - Cliente
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());

        // Sempre Carregar Conta de D�bito
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido(), saidaDTO
            .getNmInscriFavorecido()));
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
            saidaDTO.getCdDigContaCredito(), false));
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - DOC
    /**
     * Preenche dados man pagto doc.
     */
    public void preencheDadosManPagtoDOC() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoDOCEntradaDTO entradaDTO = new DetalharManPagtoDOCEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDOC(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Cabe�alho
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null
                || saidaDTO.getCdFavorecido().equals("") ? null : Long
                    .parseLong(saidaDTO.getCdFavorecido()));
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("");
            setExibeDcom(true);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
        setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
        setIdentificacaoTransferenciaDoc(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - TED
    /**
     * Preenche dados man pagto ted.
     */
    public void preencheDadosManPagtoTED() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoTEDEntradaDTO entradaDTO = new DetalharManPagtoTEDEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgenPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
                
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoTED(entradaDTO);

        // DadosCliente
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // DadosContaDebito
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaDTO
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null
                || saidaDTO.getCdFavorecido().equals("") ? null : Long
                    .parseLong(saidaDTO.getCdFavorecido()));
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(String.valueOf(saidaDTO.getCdBancoCredito()));
        setDsBancoCredito(saidaDTO.getDsBancoCredito());
        setCdIspbPagamento(saidaDTO.getCdIspbPagamento());
        setDsNomeReclamante(saidaDTO.getDsNomeReclamante());
        setNrProcessoDepositoJudicial(saidaDTO.getNrProcessoDepositoJudicial());
        setNrPisPasep(NumberUtils.formatarNumeroPisPasep(saidaDTO.getNrPisPasep()));
        setCdFinalidadeTedDepositoJudicial(saidaDTO.isCdFinalidadeTedDepositoJudicial());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
        setFinalidadeTed(saidaDTO.getFinalidadeTedFormatada());
        setIdentificacaoTransferenciaTed(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Ordem de Pagamento
    /**
     * Preenche dados man pagto ordem pagto.
     */
    public void preencheDadosManPagtoOrdemPagto() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharManPagtoOrdemPagtoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoOrdemPagto(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        limparFavorecidoBeneficiario();

        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaDTO
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null
                || saidaDTO.getCdFavorecido().equals("") ? null : Long
                    .parseLong(saidaDTO.getCdFavorecido()));
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);

        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setNumeroOp(saidaDTO.getNrOperacao());
        setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
        setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
        setNumeroCheque(saidaDTO.getNrCheque());
        setSerieCheque(saidaDTO.getNrSerieCheque());
        setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());
        setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
        setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());
        setDataRetirada(saidaDTO.getDtRetirada());
        setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
        setVlDesconto(saidaDTO.getVlDescontoCredito());
        setVlAbatimento(saidaDTO.getVlAbatidoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlIrCredito());
        setVlIss(saidaDTO.getVlIssCredito());
        setVlIof(saidaDTO.getVlIofCredito());
        setVlInss(saidaDTO.getVlInssCredito());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Titulos Bradesco
    /**
     * Preenche dados man pagto titulo bradesco.
     */
    public void preencheDadosManPagtoTituloBradesco() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharManPagtoTituloBradescoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoTituloBradesco(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Maunten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());


        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Titulos Outros Bancos
    /**
     * Preenche dados man pagto titulo outros bancos.
     */
    public void preencheDadosManPagtoTituloOutrosBancos() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharManPagtoTituloOutrosBancosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoTituloOutrosBancos(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Maunten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setCarteira(saidaDTO.getCarteira());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - DARF
    /**
     * Preenche dados man pagto darf.
     */
    public void preencheDadosManPagtoDARF() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoDARFEntradaDTO entradaDTO = new DetalharManPagtoDARFEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDARF(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Maunten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getCdTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdDanoExercicio());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
        setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - GARE
    /**
     * Preenche dados man pagto gare.
     */
    public void preencheDadosManPagtoGARE() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoGAREEntradaDTO entradaDTO = new DetalharManPagtoGAREEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoGARE(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(registroSelecionado.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamentoGare(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
        setInscricaoEstadualContribuinte(saidaDTO
            .getCdInscricaoEstadualContribuinte());
        setCodigoReceita(saidaDTO.getCdReceita());
        setNumeroReferencia(saidaDTO.getNrReferencia());
        setNumeroCotaParcela(saidaDTO.getNrCota());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
        setDataReferencia(saidaDTO.getDtReferencia());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcela());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - GPS
    /**
     * Preenche dados man pagto gps.
     */
    public void preencheDadosManPagtoGPS() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoGPSEntradaDTO entradaDTO = new DetalharManPagtoGPSEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoGPS(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
        setCodigoInss(saidaDTO.getCdInss());
        setDataCompetencia(saidaDTO.getDsMesAnoCompt());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - C�digo de Barras
    /**
     * Preenche dados man pagto codigo barras.
     */
    public void preencheDadosManPagtoCodigoBarras() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharManPagtoCodigoBarrasEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoCodigoBarras(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getNrTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
        setCodigoTributo(saidaDTO.getCdTributoArrecadado());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
        setPercentualReceita(saidaDTO.getCdPercentualTributo());
        setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setDataCompetencia(saidaDTO.getDtCompensacao());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsObservacao());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - D�bito de Veiculos
    /**
     * Preenche dados man pagto debito veiculos.
     */
    public void preencheDadosManPagtoDebitoVeiculos() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharManPagtoDebitoVeiculosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDebitoVeiculos(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manunte��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
        setAnoExercicio(saidaDTO.getDtAnoExercTributo());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
        setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
        setUfTributo(saidaDTO.getCdUfTributo());
        setNumeroNsu(saidaDTO.getNrNsuTributo());
        setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
        setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
        setVlPrincipal(saidaDTO.getVlPrincipalTributo());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Deposito Identificado
    /**
     * Preenche dados man pagto dep identificado.
     */
    public void preencheDadosManPagtoDepIdentificado() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharManPagtoDepIdentificadoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDepIdentificado(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
        setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
        setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
        setSequenciaProdutoDepositoIdentificado(saidaDTO
            .getCdSequenciaProdutoDepositoId());
        setBancoCartaoDepositoIdentificado(saidaDTO
            .getCdBancoCartaoDepositanteId());
        setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
        setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
        setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
        setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
        setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Ordem de Cr�dito
    /**
     * Preenche dados man pagto ordem credito.
     */
    public void preencheDadosManPagtoOrdemCredito() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido()
        .get(getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencaoFavorecido()
        .get(getItemSelecionadoListaManutencaoFavorecido());
        DetalharManPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharManPagtoOrdemCreditoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado
            .getCdControlePagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado
            .getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(
            getAgendadosPagosNaoPagos()).equals("") ? 0
                : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoManutencao
            .getCdModalidade());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoOrdemCredito(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setCdRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setCdLote(saidaDTO.getNrLoteArquivoRemessa());
        setNrPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtAgendamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCodigoPagador(saidaDTO.getCdPagador());
        setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Fim - Telas de detalhe - preenche dados

    /**
     * Imprimir.
     *
     * @return the string
     */
    public String imprimir() {
        try {
            String caminho = "/images/Bradesco_logo.JPG";
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext
            .getExternalContext().getContext();
            logoPath = servletContext.getRealPath(caminho);

            Map<String, Object> par = new HashMap<String, Object>();
            par.put("titulo", "Consultar Pagamentos Favorecido");
            par.put("logoBradesco", getLogoPath());

            try {

                // M�todo que gera o relat�rio PDF.
                PgitUtil
                .geraRelatorioPdf(
                    "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/favorecido",
                    "imprimirPagamentosFavorecido",
                    listaGridPagamentosFavorecido, par);

            } /* Exce��o do relat�rio */
            catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "",
                    BradescoViewExceptionActionType.ACTION);
            }

        } /* Exce��o do PDC */
        catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }

        return "IMPRIMIR";
    }

    /**
     * Limpa variaveis detalhar.
     */
    public void limpaVariaveisDetalhar() {
        // Cliente
        setCpfCnpj("");
        setNomeRazaoSocial("");
        setEmpresaConglomerado("");
        setNroContrato("");
        setDsContrato("");
        setSituacao("");

        // ContaDebito
        setDsBancoDebito("");
        setDsAgenciaDebito("");
        setContaDebito(null);
        setTipoContaDebito("");

        // Pagamentos
        setDsTipoServico("");
        setDsOperacao("");
        setCdRemessa(null);
        setCdLote(null);
        setNrLote("");
        setVlPagamentoFormatada("");
        setNrPagamento("");
        setCdListaDebito(null);
        setDsCanal("");
        setDtPagamento("");
        setDtVencimento("");
    }

    /**
     * Limpar.
     */
    public void limpar() {
        setAgendadosPagosNaoPagos("");
        setDataPagamentoInicial(new Date());
        setDataPagamentoFinal(new Date());
        setChkTipoServico(false);
        setCmbTipoServico(0);
        setCmbModalidade(0);
        setRdoPesquisa("");
        setRdoFavorecido("");
        setCodigoFavorecido("");
        setInscricaoFavorecido("");
        setCmbTipoFavorecido(0);
        setBanco("");
        setAgencia("");
        setConta("");
        setContaDig("");
        setRdoBeneficiario("");
        setCodigoBeneficiario("");
        setCmbTipoBenefeciario(0);
        setCmbOrgaoPagador(0);
        setListaGridPagamentosFavorecido(null);
        setItemSelecionadoLista(null);
        limparTipoConta();
    }

    /**
     * Limpar pesquisa.
     */
    public void limparPesquisa() {
        setAgendadosPagosNaoPagos("");
        setDataPagamentoInicial(new Date());
        setDataPagamentoFinal(new Date());
        setChkTipoServico(false);
        setCmbTipoServico(0);
        listarCmbModalidade();
        setRdoPesquisa("");
        setRdoFavorecido("");
        setCodigoFavorecido("");
        setInscricaoFavorecido("");
        setCmbTipoFavorecido(0);
        setBanco("");
        setAgencia("");
        setConta("");
        setContaDig("");
        setRdoBeneficiario("");
        setCodigoBeneficiario("");
        setCmbTipoBenefeciario(0);
        setCmbOrgaoPagador(0);
        limparTipoConta();
        setItemSelecionadoLista(null);
        setListaGridPagamentosFavorecido(null);
        setDisableArgumentosConsulta(false);
        setBancoSalario(null);
        setAgenciaSalario(null);
        setContaSalario(null);
        setIspb(null);
		setCdContaPagamento(null);
		setCdBancoPagamento(null);
    }

    /**
     * Limpar radio pesquisa.
     */
    public void limparRadioPesquisa() {
        setRdoFavorecido("");
        setCodigoFavorecido("");
        setInscricaoFavorecido("");
        setCmbTipoFavorecido(0);
        setBanco("");
        setAgencia("");
        setConta("");
        setContaDig("");
        setRdoBeneficiario("");
        setCodigoBeneficiario("");
        setCmbTipoBenefeciario(0);
        setCmbOrgaoPagador(0);
        limparTipoConta();
        setAgenciaSalario(null);
        setContaSalario(null);
		setIspb(null);
		setCdContaPagamento(null);
		setCdBancoPagamento(null);
        if("2".equals(getRdoPesquisa())){
            setBancoSalario(237);
        }else{
            setBancoSalario(null);
        }
    }

    /**
     * Limpar radio favorecido.
     */
    public void limparRadioFavorecido() {
        setCodigoFavorecido("");
        setInscricaoFavorecido("");
        setCmbTipoFavorecido(0);
    }

    /**
     * Limpar radio beneficiario.
     */
    public void limparRadioBeneficiario() {
        setCodigoBeneficiario("");
        setCmbTipoBenefeciario(0);
        setCmbOrgaoPagador(0);
    }

    /**
     * Limpar grid.
     *
     * @return the string
     */
    public String limparGrid() {
        setItemSelecionadoLista(null);
        setListaGridPagamentosFavorecido(null);
        setDisableArgumentosConsulta(false);
        return "";
    }

    /**
     * Listar cmb tipo servico.
     */
    public void listarCmbTipoServico() {
        try {
            this.setCmbTipoServico(0);
            listaCmbTipoServico = new ArrayList<SelectItem>();
            List<ConsultarServicoPagtoSaidaDTO> list = new ArrayList<ConsultarServicoPagtoSaidaDTO>();

            list = comboService.consultarServicoPagto();
            listaCmbTipoServico.clear();
            for (ConsultarServicoPagtoSaidaDTO combo : list) {
                listaCmbTipoServico.add(new SelectItem(combo.getCdServico(),
                    combo.getDsServico()));
            }
        } catch (PdcAdapterFunctionalException p) {
            listaCmbTipoServico = new ArrayList<SelectItem>();
        }
    }

    /**
     * Listar cmb modalidade.
     */
    public void listarCmbModalidade() {
        try {
            setCmbModalidade(null);
            if (getCmbTipoServico() != null && getCmbTipoServico() != 0) {
                listaCmbModalidade = new ArrayList<SelectItem>();

                List<ConsultarModalidadePagtoSaidaDTO> list = comboService
                .consultarModalidadePagto(getCmbTipoServico());

                for (ConsultarModalidadePagtoSaidaDTO saida : list) {
                    listaCmbModalidade.add(new SelectItem(saida
                        .getCdProdutoOperacaoRelacionado(), saida
                        .getDsProdutoOperacaoRelacionado()));
                }
            } else {
                listaCmbModalidade = new ArrayList<SelectItem>();
            }
        } catch (PdcAdapterFunctionalException p) {
            listaCmbModalidade = new ArrayList<SelectItem>();
        }
    }

    /**
     * Pesquisar.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisar(ActionEvent evt) {
        consultar();
        return "";
    }

    /**
     * Pesquisar manutencao.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarManutencao(ActionEvent evt) {
        listarManutencao();
        return "";
    }

    /**
     * Pesquisar historico.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarHistorico(ActionEvent evt) {
        listarHistorico();
        return "";
    }

    /**
     * Listar cmb tipo favorecido.
     */
    public void listarCmbTipoFavorecido() {
        /*
         * try{ listaCmbTipoFavorecido = new ArrayList<SelectItem>();
         * List<InscricaoFavorecidosSaidaDTO> list = new
         * ArrayList<InscricaoFavorecidosSaidaDTO>();
         * 
         * list = comboService.listarTipoInscricao();
         * listaCmbTipoFavorecido.clear();
         * 
         * for(InscricaoFavorecidosSaidaDTO combo : list){
         * listaCmbTipoFavorecido.add(new
         * SelectItem(combo.getCdTipoInscricaoFavorecidos
         * (),combo.getDsTipoInscricaoFavorecidos())); } }catch
         * (PdcAdapterFunctionalException p){ listaCmbTipoFavorecido = new
         * ArrayList<SelectItem>(); }
         */

        listaCmbTipoFavorecido.add(new SelectItem(1, MessageHelperUtils
            .getI18nMessage("label_cpf")));
        listaCmbTipoFavorecido.add(new SelectItem(2, MessageHelperUtils
            .getI18nMessage("label_cnpj")));
        listaCmbTipoFavorecido.add(new SelectItem(9, MessageHelperUtils
            .getI18nMessage("label_todos")));

    }

    /**
     * Listar cmb orgao pagador.
     */
    public void listarCmbOrgaoPagador() {
        try {
            listaCmbOrgaoPagador = new ArrayList<SelectItem>();
            List<ListarOrgaoPagadorSaidaDTO> list = new ArrayList<ListarOrgaoPagadorSaidaDTO>();
            ListarOrgaoPagadorEntradaDTO entradaDTO = new ListarOrgaoPagadorEntradaDTO();

            entradaDTO.setMaximoOcorrencias(50);
            entradaDTO.setCdOrgaoPagadorOrganizacao(0);
            entradaDTO.setCdTipoUnidadeOrganizacao(0);
            entradaDTO.setCdPessoaJuridica(0L);
            entradaDTO.setNrSequenciaUnidadeOrganizacao(0);
            entradaDTO.setCdSituacaoOrgaoPagador(0);
            entradaDTO.setCdTarifaOrgaoPagador(0);

            list = comboService.listarOrgaoPagador(entradaDTO);
            // cdOrgaoPagadorOrganizacao - dsNumeroSequencialUnidadeOrganizacao
            // - altera��o solicitada beatriz.
            for (ListarOrgaoPagadorSaidaDTO combo : list) {
                listaCmbOrgaoPagador.add(new SelectItem(combo
                    .getCdOrgaoPagadorOrganizacao(), combo
                    .getCdOrgaoPagadorOrganizacao()
                    + " - "
                    + combo.getDsNumeroSequenciaUnidadeOrganizacao()));
            }
        } catch (PdcAdapterFunctionalException p) {
            listaCmbOrgaoPagador = new ArrayList<SelectItem>();
        }
    }

    /**
     * Consultar tipo conta.
     *
     * @return the string
     */
    public String consultarTipoConta() {
        try {
            ConsultarTipoContaEntradaDTO entrada = new ConsultarTipoContaEntradaDTO();
            entrada.setCdAgencia(Integer.parseInt(getAgencia()));
            entrada.setCdBanco(Integer.parseInt(getBanco()));
            entrada.setCdConta(Long.parseLong(getConta()));
            entrada.setCdDigitoConta(getContaDig());
            entrada.setCdDigitoAgencia(0);

            List<ConsultarTipoContaSaidaDTO> listaTipoConta = getComboService()
            .consultarTipoConta(entrada);

            listaSituacaoListaDebitoHash.clear();
            listaCmbTipoConta = new ArrayList<SelectItem>();

            for (int i = 0; i < listaTipoConta.size(); i++) {
                listaCmbTipoConta.add(new SelectItem(listaTipoConta.get(i)
                    .getCdTipo(), listaTipoConta.get(i).getDsTipo()));
                listaSituacaoListaDebitoHash.put(listaTipoConta.get(i)
                    .getCdTipo(), listaTipoConta.get(i));
            }
        } catch (PdcAdapterFunctionalException p) {
            listaCmbTipoConta = new ArrayList<SelectItem>();
            listaSituacaoListaDebitoHash = new HashMap<Integer, ConsultarTipoContaSaidaDTO>();
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }

        return "";
    }

    /**
     * Limpar tipo conta.
     */
    public void limparTipoConta() {
        setCmbTipoConta(null);
        this.listaCmbTipoConta = new ArrayList<SelectItem>();
    }

    /**
     * Controle servico modalidade.
     */
    public void controleServicoModalidade() {
        setCmbModalidade(null);
        setCmbTipoServico(null);
        listaCmbModalidade = new ArrayList<SelectItem>();
    }

    /**
     * Consultar autorizantes.
     *
     * @return the string
     */
    public String consultarAutorizantes(){
        setItemSelecionadoListaAutorizante(null);
        setListaRadioAutorizante(new ArrayList<SelectItem>());
        
        
        try {
            ConsultarAutorizantesNetEmpresaEntradaDTO entradaDTO = new ConsultarAutorizantesNetEmpresaEntradaDTO();
            ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado = getListaGridPagamentosFavorecido().get(
                getItemSelecionadoLista());

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
            entradaDTO.setCdTipoCanal(registroSelecionado.getCdTipoCanal());
            entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
            entradaDTO.setCdModalidade(registroSelecionado.getDsTipoTela());
            entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
            entradaDTO.setCdDigitoAgenciaDebito(String.valueOf(registroSelecionado.getCdDigitoAgenciaDebito()));

            ConsultarAutorizantesNetEmpresaSaidaDTO saida =
                consultaAutorizanteService.consultarAutorizantesNetEmpresa(entradaDTO);
            
            for (int i = 0; i < saida.getOcorrencias().size(); i++) {
                consultarAutorizantesOcorrencia.add(saida.getOcorrencias().get(i));
                listaRadioAutorizante.add(new SelectItem(i, ""));
            }

            consultarManutencao();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            return "";
        }

        return "CONSULTAR_AUTORIZANTE";
    }
    
    /**
     * Consultar autorizantes paginacao.
     *
     * @param evt the evt
     * @return the string
     */
    public String consultarAutorizantesPaginacao(ActionEvent evt) {
        consultarAutorizantes();
        return "";
    }
    
    /**
     * Habilita botoes.
     */
    public void habilitaBotoes() {
        isHabilitaBotaoConsultarAutorizantes();
    }

    /**
     * Is habilita botao consultar autorizantes.
     *
     * @return true, if is habilita botao consultar autorizantes
     */
    public boolean isHabilitaBotaoConsultarAutorizantes(){
        if (getItemSelecionadoLista() != null){
            return false;
        }
        return true;
    }

    /**
     * Voltar autorizante.
     *
     * @return the string
     */
    public String voltarAutorizante() {
        setItemSelecionadoLista(null);
        return "VOLTAR_AUTORIZANTE";
    }  

    /**
     * Is botao detalhar retirada visivel.
     * 
     * @return true, if is botao detalhar retirada visivel
     */
    public boolean isBotaoDetalharRetiradaVisivel() {
        return detalheOrdemPagamento != null && detalheOrdemPagamento.getCdIdentificacaoTipoRetirada() != null
            && detalheOrdemPagamento.getCdIdentificacaoTipoRetirada().equals(CODIGO_IDENTIFICACAO_DIVERSOS); 
    }
    
    /**
     * Detalhar retirada.
     * 
     * @return the string
     */
    public String detalharRetirada() {
        carregarListaFormaPagamento();

        return "DETALHE_RETIRADA";
    }

    /**
     * Carregar lista forma pagamento.
     */
    private void carregarListaFormaPagamento() {
        ConsultarPagamentoFavorecidoSaidaDTO registroSelecionado =
            getListaGridPagamentosFavorecido().get(getItemSelecionadoLista());

        ListarTipoRetiradaEntradaDTO entrada = new ListarTipoRetiradaEntradaDTO();
        entrada.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entrada.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
        entrada.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
        entrada.setCdProdutoOperacaoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entrada.setCdBancoPagador(detalheOrdemPagamento.getCdBancoDebito());
        entrada.setCdAgenciaBancariaPagador(detalheOrdemPagamento.getCdAgenciaDebito());
        entrada.setCdDigitoAgenciaPagador(detalheOrdemPagamento.getCdDigAgenciaDebto());
        entrada.setCdContaBancariaPagador(detalheOrdemPagamento.getCdContaDebito());
        entrada.setCdDigitoContaPagador(detalheOrdemPagamento.getDsDigAgenciaDebito());
        entrada.setCdBancoFavorecido(detalheOrdemPagamento.getCdBancoCredito());
        entrada.setCdAgenciaFavorecido(detalheOrdemPagamento.getCdAgenciaCredito());
        entrada.setCdDigitoAgenciaFavorecido(detalheOrdemPagamento.getCdDigAgenciaCredito());
        entrada.setCdContaFavorecido(detalheOrdemPagamento.getCdContaCredito());
        entrada.setCdDigitoContaFavorecido(detalheOrdemPagamento.getCdDigContaCredito());
        entrada.setDtPagamentoDe(detalheOrdemPagamento.getDtAgendamento());
        entrada.setDtPagamentoAte(detalheOrdemPagamento.getDtVencimento());

        ListarTipoRetiradaSaidaDTO saidaDetalheRetirada = consultasService.listarTipoRetirada(entrada);
        listaFormaPagamento = saidaDetalheRetirada.getOcorrencias();
    }

    /**
     * Paginar lista forma pagamento.
     * 
     * @param actionEvent
     *            the action event
     */
    public void paginarListaFormaPagamento(ActionEvent actionEvent) {
        carregarListaFormaPagamento();
    }

    /**
     * Voltar detalhe retirada.
     * 
     * @return the string
     */
    public String voltarDetalheRetirada() {
        return "VOLTAR_DETALHE";
    }

    // SETs GETs
    /**
     * Get: comboService.
     *
     * @return comboService
     */
    public IComboService getComboService() {
        return comboService;
    }

    /**
     * Set: comboService.
     *
     * @param comboService the combo service
     */
    public void setComboService(IComboService comboService) {
        this.comboService = comboService;
    }

    /**
     * Get: consultarPagamentosFavorecidoServiceImpl.
     *
     * @return consultarPagamentosFavorecidoServiceImpl
     */
    public IConsultarPagamentosFavorecidoService getConsultarPagamentosFavorecidoServiceImpl() {
        return consultarPagamentosFavorecidoServiceImpl;
    }

    /**
     * Set: consultarPagamentosFavorecidoServiceImpl.
     *
     * @param consultarPagamentosFavorecidoServiceImpl the consultar pagamentos favorecido service impl
     */
    public void setConsultarPagamentosFavorecidoServiceImpl(
        IConsultarPagamentosFavorecidoService consultarPagamentosFavorecidoServiceImpl) {
        this.consultarPagamentosFavorecidoServiceImpl = consultarPagamentosFavorecidoServiceImpl;
    }

    /**
     * Get: dataPagamentoFinal.
     *
     * @return dataPagamentoFinal
     */
    public Date getDataPagamentoFinal() {
        return dataPagamentoFinal;
    }

    /**
     * Set: dataPagamentoFinal.
     *
     * @param dataPagamentoFinal the data pagamento final
     */
    public void setDataPagamentoFinal(Date dataPagamentoFinal) {
        this.dataPagamentoFinal = dataPagamentoFinal;
    }

    /**
     * Get: dataPagamentoInicial.
     *
     * @return dataPagamentoInicial
     */
    public Date getDataPagamentoInicial() {
        return dataPagamentoInicial;
    }

    /**
     * Set: dataPagamentoInicial.
     *
     * @param dataPagamentoInicial the data pagamento inicial
     */
    public void setDataPagamentoInicial(Date dataPagamentoInicial) {
        this.dataPagamentoInicial = dataPagamentoInicial;
    }

    /**
     * Is chk tipo servico.
     *
     * @return true, if is chk tipo servico
     */
    public boolean isChkTipoServico() {
        return chkTipoServico;
    }

    /**
     * Set: chkTipoServico.
     *
     * @param chkTipoServico the chk tipo servico
     */
    public void setChkTipoServico(boolean chkTipoServico) {
        this.chkTipoServico = chkTipoServico;
    }

    /**
     * Get: cmbTipoServico.
     *
     * @return cmbTipoServico
     */
    public Integer getCmbTipoServico() {
        return cmbTipoServico;
    }

    /**
     * Set: cmbTipoServico.
     *
     * @param cmbTipoServico the cmb tipo servico
     */
    public void setCmbTipoServico(Integer cmbTipoServico) {
        this.cmbTipoServico = cmbTipoServico;
    }

    /**
     * Get: listaCmbTipoServico.
     *
     * @return listaCmbTipoServico
     */
    public List<SelectItem> getListaCmbTipoServico() {
        return listaCmbTipoServico;
    }

    /**
     * Set: listaCmbTipoServico.
     *
     * @param listaCmbTipoServico the lista cmb tipo servico
     */
    public void setListaCmbTipoServico(List<SelectItem> listaCmbTipoServico) {
        this.listaCmbTipoServico = listaCmbTipoServico;
    }

    /**
     * Get: cmbModalidade.
     *
     * @return cmbModalidade
     */
    public Integer getCmbModalidade() {
        return cmbModalidade;
    }

    /**
     * Set: cmbModalidade.
     *
     * @param cmbModalidade the cmb modalidade
     */
    public void setCmbModalidade(Integer cmbModalidade) {
        this.cmbModalidade = cmbModalidade;
    }

    /**
     * Get: listaCmbModalidade.
     *
     * @return listaCmbModalidade
     */
    public List<SelectItem> getListaCmbModalidade() {
        return listaCmbModalidade;
    }

    /**
     * Set: listaCmbModalidade.
     *
     * @param listaCmbModalidade the lista cmb modalidade
     */
    public void setListaCmbModalidade(List<SelectItem> listaCmbModalidade) {
        this.listaCmbModalidade = listaCmbModalidade;
    }

    /**
     * Get: codigoFavorecido.
     *
     * @return codigoFavorecido
     */
    public String getCodigoFavorecido() {
        return codigoFavorecido;
    }

    /**
     * Set: codigoFavorecido.
     *
     * @param codigoFavorecido the codigo favorecido
     */
    public void setCodigoFavorecido(String codigoFavorecido) {
        this.codigoFavorecido = codigoFavorecido;
    }

    /**
     * Get: rdoFavorecido.
     *
     * @return rdoFavorecido
     */
    public String getRdoFavorecido() {
        return rdoFavorecido;
    }

    /**
     * Set: rdoFavorecido.
     *
     * @param rdoFavorecido the rdo favorecido
     */
    public void setRdoFavorecido(String rdoFavorecido) {
        this.rdoFavorecido = rdoFavorecido;
    }

    /**
     * Get: rdoPesquisa.
     *
     * @return rdoPesquisa
     */
    public String getRdoPesquisa() {
        return rdoPesquisa;
    }

    /**
     * Set: rdoPesquisa.
     *
     * @param rdoPesquisa the rdo pesquisa
     */
    public void setRdoPesquisa(String rdoPesquisa) {
        this.rdoPesquisa = rdoPesquisa;
    }

    /**
     * Get: cmbTipoFavorecido.
     *
     * @return cmbTipoFavorecido
     */
    public Integer getCmbTipoFavorecido() {
        return cmbTipoFavorecido;
    }

    /**
     * Set: cmbTipoFavorecido.
     *
     * @param cmbTipoFavorecido the cmb tipo favorecido
     */
    public void setCmbTipoFavorecido(Integer cmbTipoFavorecido) {
        this.cmbTipoFavorecido = cmbTipoFavorecido;
    }

    /**
     * Get: inscricaoFavorecido.
     *
     * @return inscricaoFavorecido
     */
    public String getInscricaoFavorecido() {
        return inscricaoFavorecido;
    }

    /**
     * Set: inscricaoFavorecido.
     *
     * @param inscricaoFavorecido the inscricao favorecido
     */
    public void setInscricaoFavorecido(String inscricaoFavorecido) {
        this.inscricaoFavorecido = inscricaoFavorecido;
    }

    /**
     * Get: listaCmbTipoFavorecido.
     *
     * @return listaCmbTipoFavorecido
     */
    public List<SelectItem> getListaCmbTipoFavorecido() {
        return listaCmbTipoFavorecido;
    }

    /**
     * Set: listaCmbTipoFavorecido.
     *
     * @param listaCmbTipoFavorecido the lista cmb tipo favorecido
     */
    public void setListaCmbTipoFavorecido(
        List<SelectItem> listaCmbTipoFavorecido) {
        this.listaCmbTipoFavorecido = listaCmbTipoFavorecido;
    }

    /**
     * Get: agencia.
     *
     * @return agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Set: agencia.
     *
     * @param agencia the agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Get: banco.
     *
     * @return banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Set: banco.
     *
     * @param banco the banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * Get: contaDig.
     *
     * @return contaDig
     */
    public String getContaDig() {
        return contaDig;
    }

    /**
     * Set: contaDig.
     *
     * @param contaDig the conta dig
     */
    public void setContaDig(String contaDig) {
        this.contaDig = contaDig;
    }

    /**
     * Get: conta.
     *
     * @return conta
     */
    public String getConta() {
        return conta;
    }

    /**
     * Set: conta.
     *
     * @param conta the conta
     */
    public void setConta(String conta) {
        this.conta = conta;
    }

    /**
     * Get: codigoBeneficiario.
     *
     * @return codigoBeneficiario
     */
    public String getCodigoBeneficiario() {
        return codigoBeneficiario;
    }

    /**
     * Set: codigoBeneficiario.
     *
     * @param codigoBeneficiario the codigo beneficiario
     */
    public void setCodigoBeneficiario(String codigoBeneficiario) {
        this.codigoBeneficiario = codigoBeneficiario;
    }

    /**
     * Get: rdoBeneficiario.
     *
     * @return rdoBeneficiario
     */
    public String getRdoBeneficiario() {
        return rdoBeneficiario;
    }

    /**
     * Set: rdoBeneficiario.
     *
     * @param rdoBeneficiario the rdo beneficiario
     */
    public void setRdoBeneficiario(String rdoBeneficiario) {
        this.rdoBeneficiario = rdoBeneficiario;
    }

    /**
     * Get: cmbOrgaoPagador.
     *
     * @return cmbOrgaoPagador
     */
    public Integer getCmbOrgaoPagador() {
        return cmbOrgaoPagador;
    }

    /**
     * Set: cmbOrgaoPagador.
     *
     * @param cmbOrgaoPagador the cmb orgao pagador
     */
    public void setCmbOrgaoPagador(Integer cmbOrgaoPagador) {
        this.cmbOrgaoPagador = cmbOrgaoPagador;
    }

    /**
     * Get: listaCmbOrgaoPagador.
     *
     * @return listaCmbOrgaoPagador
     */
    public List<SelectItem> getListaCmbOrgaoPagador() {
        return listaCmbOrgaoPagador;
    }

    /**
     * Set: listaCmbOrgaoPagador.
     *
     * @param listaCmbOrgaoPagador the lista cmb orgao pagador
     */
    public void setListaCmbOrgaoPagador(List<SelectItem> listaCmbOrgaoPagador) {
        this.listaCmbOrgaoPagador = listaCmbOrgaoPagador;
    }

    /**
     * Get: cmbTipoBenefeciario.
     *
     * @return cmbTipoBenefeciario
     */
    public Integer getCmbTipoBenefeciario() {
        return cmbTipoBenefeciario;
    }

    /**
     * Set: cmbTipoBenefeciario.
     *
     * @param cmbTipoBenefeciario the cmb tipo benefeciario
     */
    public void setCmbTipoBenefeciario(Integer cmbTipoBenefeciario) {
        this.cmbTipoBenefeciario = cmbTipoBenefeciario;
    }

    /**
     * Get: itemSelecionadoLista.
     *
     * @return itemSelecionadoLista
     */
    public Integer getItemSelecionadoLista() {
        return itemSelecionadoLista;
    }

    /**
     * Set: itemSelecionadoLista.
     *
     * @param itemSelecionadoLista the item selecionado lista
     */
    public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
        this.itemSelecionadoLista = itemSelecionadoLista;
    }

    /**
     * Get: listaGridPagamentosFavorecido.
     *
     * @return listaGridPagamentosFavorecido
     */
    public List<ConsultarPagamentoFavorecidoSaidaDTO> getListaGridPagamentosFavorecido() {
        return listaGridPagamentosFavorecido;
    }

    /**
     * Set: listaGridPagamentosFavorecido.
     *
     * @param listaGridPagamentosFavorecido the lista grid pagamentos favorecido
     */
    public void setListaGridPagamentosFavorecido(
        List<ConsultarPagamentoFavorecidoSaidaDTO> listaGridPagamentosFavorecido) {
        this.listaGridPagamentosFavorecido = listaGridPagamentosFavorecido;
    }

    /**
     * Get: itemSelecionadoListaManutencaoFavorecido.
     *
     * @return itemSelecionadoListaManutencaoFavorecido
     */
    public Integer getItemSelecionadoListaManutencaoFavorecido() {
        return itemSelecionadoListaManutencaoFavorecido;
    }

    /**
     * Set: itemSelecionadoListaManutencaoFavorecido.
     *
     * @param itemSelecionadoListaManutencaoFavorecido the item selecionado lista manutencao favorecido
     */
    public void setItemSelecionadoListaManutencaoFavorecido(
        Integer itemSelecionadoListaManutencaoFavorecido) {
        this.itemSelecionadoListaManutencaoFavorecido = itemSelecionadoListaManutencaoFavorecido;
    }

    /**
     * Get: listaConsultarManutencaoFavorecido.
     *
     * @return listaConsultarManutencaoFavorecido
     */
    public List<ConsultarManutencaoPagamentoSaidaDTO> getListaConsultarManutencaoFavorecido() {
        return listaConsultarManutencaoFavorecido;
    }

    /**
     * Set: listaConsultarManutencaoFavorecido.
     *
     * @param listaConsultarManutencaoFavorecido the lista consultar manutencao favorecido
     */
    public void setListaConsultarManutencaoFavorecido(
        List<ConsultarManutencaoPagamentoSaidaDTO> listaConsultarManutencaoFavorecido) {
        this.listaConsultarManutencaoFavorecido = listaConsultarManutencaoFavorecido;
    }

    /**
     * Get: listaManutencaoFavorecidoControle.
     *
     * @return listaManutencaoFavorecidoControle
     */
    public List<SelectItem> getListaManutencaoFavorecidoControle() {
        return listaManutencaoFavorecidoControle;
    }

    /**
     * Set: listaManutencaoFavorecidoControle.
     *
     * @param listaManutencaoFavorecidoControle the lista manutencao favorecido controle
     */
    public void setListaManutencaoFavorecidoControle(
        List<SelectItem> listaManutencaoFavorecidoControle) {
        this.listaManutencaoFavorecidoControle = listaManutencaoFavorecidoControle;
    }

    /**
     * Get: consultarPagamentosIndividualServiceImpl.
     *
     * @return consultarPagamentosIndividualServiceImpl
     */
    public IConsultarPagamentosIndividualService getConsultarPagamentosIndividualServiceImpl() {
        return consultarPagamentosIndividualServiceImpl;
    }

    /**
     * Set: consultarPagamentosIndividualServiceImpl.
     *
     * @param consultarPagamentosIndividualServiceImpl the consultar pagamentos individual service impl
     */
    public void setConsultarPagamentosIndividualServiceImpl(
        IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl) {
        this.consultarPagamentosIndividualServiceImpl = consultarPagamentosIndividualServiceImpl;
    }

    /**
     * Get: listaControlePagamentosFavorecido.
     *
     * @return listaControlePagamentosFavorecido
     */
    public List<SelectItem> getListaControlePagamentosFavorecido() {
        return listaControlePagamentosFavorecido;
    }

    /**
     * Set: listaControlePagamentosFavorecido.
     *
     * @param listaControlePagamentosFavorecido the lista controle pagamentos favorecido
     */
    public void setListaControlePagamentosFavorecido(
        List<SelectItem> listaControlePagamentosFavorecido) {
        this.listaControlePagamentosFavorecido = listaControlePagamentosFavorecido;
    }

    /**
     * Get: dsContrato.
     *
     * @return dsContrato
     */
    public String getDsContrato() {
        return dsContrato;
    }

    /**
     * Set: dsContrato.
     *
     * @param dsContrato the ds contrato
     */
    public void setDsContrato(String dsContrato) {
        this.dsContrato = dsContrato;
    }

    /**
     * Get: empresaConglomerado.
     *
     * @return empresaConglomerado
     */
    public String getEmpresaConglomerado() {
        return empresaConglomerado;
    }

    /**
     * Set: empresaConglomerado.
     *
     * @param empresaConglomerado the empresa conglomerado
     */
    public void setEmpresaConglomerado(String empresaConglomerado) {
        this.empresaConglomerado = empresaConglomerado;
    }

    /**
     * Get: nomeRazaoSocial.
     *
     * @return nomeRazaoSocial
     */
    public String getNomeRazaoSocial() {
        return nomeRazaoSocial;
    }

    /**
     * Set: nomeRazaoSocial.
     *
     * @param nomeRazaoSocial the nome razao social
     */
    public void setNomeRazaoSocial(String nomeRazaoSocial) {
        this.nomeRazaoSocial = nomeRazaoSocial;
    }

    /**
     * Get: nroContrato.
     *
     * @return nroContrato
     */
    public String getNroContrato() {
        return nroContrato;
    }

    /**
     * Set: nroContrato.
     *
     * @param nroContrato the nro contrato
     */
    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    /**
     * Get: situacao.
     *
     * @return situacao
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Set: situacao.
     *
     * @param situacao the situacao
     */
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * Get: itemSelecionadoListaHistConSaldo.
     *
     * @return itemSelecionadoListaHistConSaldo
     */
    public Integer getItemSelecionadoListaHistConSaldo() {
        return itemSelecionadoListaHistConSaldo;
    }

    /**
     * Set: itemSelecionadoListaHistConSaldo.
     *
     * @param itemSelecionadoListaHistConSaldo the item selecionado lista hist con saldo
     */
    public void setItemSelecionadoListaHistConSaldo(
        Integer itemSelecionadoListaHistConSaldo) {
        this.itemSelecionadoListaHistConSaldo = itemSelecionadoListaHistConSaldo;
    }

    /**
     * Get: listaControleHistConSaldo.
     *
     * @return listaControleHistConSaldo
     */
    public List<SelectItem> getListaControleHistConSaldo() {
        return listaControleHistConSaldo;
    }

    /**
     * Set: listaControleHistConSaldo.
     *
     * @param listaControleHistConSaldo the lista controle hist con saldo
     */
    public void setListaControleHistConSaldo(
        List<SelectItem> listaControleHistConSaldo) {
        this.listaControleHistConSaldo = listaControleHistConSaldo;
    }

    /**
     * Get: listaGridHistConsultaSaldo.
     *
     * @return listaGridHistConsultaSaldo
     */
    public List<ConsultarHistConsultaSaldoSaidaDTO> getListaGridHistConsultaSaldo() {
        return listaGridHistConsultaSaldo;
    }

    /**
     * Set: listaGridHistConsultaSaldo.
     *
     * @param listaGridHistConsultaSaldo the lista grid hist consulta saldo
     */
    public void setListaGridHistConsultaSaldo(
        List<ConsultarHistConsultaSaldoSaidaDTO> listaGridHistConsultaSaldo) {
        this.listaGridHistConsultaSaldo = listaGridHistConsultaSaldo;
    }

    /**
     * Get: cdListaDebito.
     *
     * @return cdListaDebito
     */
    public String getCdListaDebito() {
        return cdListaDebito;
    }

    /**
     * Set: cdListaDebito.
     *
     * @param cdListaDebito the cd lista debito
     */
    public void setCdListaDebito(String cdListaDebito) {
        this.cdListaDebito = cdListaDebito;
    }

    /**
     * Get: cdLote.
     *
     * @return cdLote
     */
    public String getCdLote() {
        return cdLote;
    }

    /**
     * Set: cdLote.
     *
     * @param cdLote the cd lote
     */
    public void setCdLote(String cdLote) {
        this.cdLote = cdLote;
    }

    /**
     * Get: cdRemessa.
     *
     * @return cdRemessa
     */
    public String getCdRemessa() {
        return cdRemessa;
    }

    /**
     * Set: cdRemessa.
     *
     * @param cdRemessa the cd remessa
     */
    public void setCdRemessa(String cdRemessa) {
        this.cdRemessa = cdRemessa;
    }

    /**
     * Get: dsCanal.
     *
     * @return dsCanal
     */
    public String getDsCanal() {
        return dsCanal;
    }

    /**
     * Set: dsCanal.
     *
     * @param dsCanal the ds canal
     */
    public void setDsCanal(String dsCanal) {
        this.dsCanal = dsCanal;
    }

    /**
     * Get: dsOperacao.
     *
     * @return dsOperacao
     */
    public String getDsOperacao() {
        return dsOperacao;
    }

    /**
     * Set: dsOperacao.
     *
     * @param dsOperacao the ds operacao
     */
    public void setDsOperacao(String dsOperacao) {
        this.dsOperacao = dsOperacao;
    }

    /**
     * Get: dsTipoServico.
     *
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
        return dsTipoServico;
    }

    /**
     * Set: dsTipoServico.
     *
     * @param dsTipoServico the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
        this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: dtPagamento.
     *
     * @return dtPagamento
     */
    public String getDtPagamento() {
        return dtPagamento;
    }

    /**
     * Set: dtPagamento.
     *
     * @param dtPagamento the dt pagamento
     */
    public void setDtPagamento(String dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    /**
     * Get: camaraCentralizadora.
     *
     * @return camaraCentralizadora
     */
    public String getCamaraCentralizadora() {
        return camaraCentralizadora;
    }

    /**
     * Set: camaraCentralizadora.
     *
     * @param camaraCentralizadora the camara centralizadora
     */
    public void setCamaraCentralizadora(String camaraCentralizadora) {
        this.camaraCentralizadora = camaraCentralizadora;
    }

    /**
     * Get: dtVencimento.
     *
     * @return dtVencimento
     */
    public String getDtVencimento() {
        return dtVencimento;
    }

    /**
     * Set: dtVencimento.
     *
     * @param dtVencimento the dt vencimento
     */
    public void setDtVencimento(String dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    /**
     * Get: finalidadeDoc.
     *
     * @return finalidadeDoc
     */
    public String getFinalidadeDoc() {
        return finalidadeDoc;
    }

    /**
     * Set: finalidadeDoc.
     *
     * @param finalidadeDoc the finalidade doc
     */
    public void setFinalidadeDoc(String finalidadeDoc) {
        this.finalidadeDoc = finalidadeDoc;
    }

    /**
     * Get: identificacaoTitularidade.
     *
     * @return identificacaoTitularidade
     */
    public String getIdentificacaoTitularidade() {
        return identificacaoTitularidade;
    }

    /**
     * Set: identificacaoTitularidade.
     *
     * @param identificacaoTitularidade the identificacao titularidade
     */
    public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
        this.identificacaoTitularidade = identificacaoTitularidade;
    }

    /**
     * Get: identificacaoTransferenciaDoc.
     *
     * @return identificacaoTransferenciaDoc
     */
    public String getIdentificacaoTransferenciaDoc() {
        return identificacaoTransferenciaDoc;
    }

    /**
     * Set: identificacaoTransferenciaDoc.
     *
     * @param identificacaoTransferenciaDoc the identificacao transferencia doc
     */
    public void setIdentificacaoTransferenciaDoc(
        String identificacaoTransferenciaDoc) {
        this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
    }

    /**
     * Get: dtaHraConSaldo.
     *
     * @return dtaHraConSaldo
     */
    public String getDtaHraConSaldo() {
        return dtaHraConSaldo;
    }

    /**
     * Set: dtaHraConSaldo.
     *
     * @param dtaHraConSaldo the dta hra con saldo
     */
    public void setDtaHraConSaldo(String dtaHraConSaldo) {
        this.dtaHraConSaldo = dtaHraConSaldo;
    }

    /**
     * Get: nrPagamento.
     *
     * @return nrPagamento
     */
    public String getNrPagamento() {
        return nrPagamento;
    }

    /**
     * Set: nrPagamento.
     *
     * @param nrPagamento the nr pagamento
     */
    public void setNrPagamento(String nrPagamento) {
        this.nrPagamento = nrPagamento;
    }

    /**
     * Get: vlPagamento.
     *
     * @return vlPagamento
     */
    public BigDecimal getVlPagamento() {
        return vlPagamento;
    }

    /**
     * Set: vlPagamento.
     *
     * @param vlPagamento the vl pagamento
     */
    public void setVlPagamento(BigDecimal vlPagamento) {
        this.vlPagamento = vlPagamento;
    }

    /**
     * Get: saldoAgregCDB.
     *
     * @return saldoAgregCDB
     */
    public BigDecimal getSaldoAgregCDB() {
        return saldoAgregCDB;
    }

    /**
     * Set: saldoAgregCDB.
     *
     * @param saldoAgregCDB the saldo agreg cdb
     */
    public void setSaldoAgregCDB(BigDecimal saldoAgregCDB) {
        this.saldoAgregCDB = saldoAgregCDB;
    }

    /**
     * Get: saldoAgregCredRotativo.
     *
     * @return saldoAgregCredRotativo
     */
    public BigDecimal getSaldoAgregCredRotativo() {
        return saldoAgregCredRotativo;
    }

    /**
     * Set: saldoAgregCredRotativo.
     *
     * @param saldoAgregCredRotativo the saldo agreg cred rotativo
     */
    public void setSaldoAgregCredRotativo(BigDecimal saldoAgregCredRotativo) {
        this.saldoAgregCredRotativo = saldoAgregCredRotativo;
    }

    /**
     * Get: saldoAgregFundo.
     *
     * @return saldoAgregFundo
     */
    public BigDecimal getSaldoAgregFundo() {
        return saldoAgregFundo;
    }

    /**
     * Set: saldoAgregFundo.
     *
     * @param saldoAgregFundo the saldo agreg fundo
     */
    public void setSaldoAgregFundo(BigDecimal saldoAgregFundo) {
        this.saldoAgregFundo = saldoAgregFundo;
    }

    /**
     * Get: saldoAgregPoupanca.
     *
     * @return saldoAgregPoupanca
     */
    public BigDecimal getSaldoAgregPoupanca() {
        return saldoAgregPoupanca;
    }

    /**
     * Set: saldoAgregPoupanca.
     *
     * @param saldoAgregPoupanca the saldo agreg poupanca
     */
    public void setSaldoAgregPoupanca(BigDecimal saldoAgregPoupanca) {
        this.saldoAgregPoupanca = saldoAgregPoupanca;
    }

	/**
     * Get: sinal.
     *
     * @param sinal the more or less
     */
    
    public String getSinal() {
		return sinal;
	}

    /**
     * Set: sinal.
     *
     * @param sinal the more or less
     */
    
	public void setSinal(String sinal) {
		this.sinal = sinal;
	}

	/**
     * Get: saldoAntLancto.
     *
     * @return saldoAntLancto
     */
    public BigDecimal getSaldoAntLancto() {
        return saldoAntLancto;
    }

    /**
     * Set: saldoAntLancto.
     *
     * @param saldoAntLancto the saldo ant lancto
     */
    public void setSaldoAntLancto(BigDecimal saldoAntLancto) {
        this.saldoAntLancto = saldoAntLancto;
    }

    /**
     * Get: saldoBonusCPMF.
     *
     * @return saldoBonusCPMF
     */
    public BigDecimal getSaldoBonusCPMF() {
        return saldoBonusCPMF;
    }

    /**
     * Set: saldoBonusCPMF.
     *
     * @param saldoBonusCPMF the saldo bonus cpmf
     */
    public void setSaldoBonusCPMF(BigDecimal saldoBonusCPMF) {
        this.saldoBonusCPMF = saldoBonusCPMF;
    }

    /**
     * Get: saldoOperacional.
     *
     * @return saldoOperacional
     */
    public BigDecimal getSaldoOperacional() {
        return saldoOperacional;
    }

    /**
     * Set: saldoOperacional.
     *
     * @param saldoOperacional the saldo operacional
     */
    public void setSaldoOperacional(BigDecimal saldoOperacional) {
        this.saldoOperacional = saldoOperacional;
    }

    /**
     * Get: saldoVincAdm.
     *
     * @return saldoVincAdm
     */
    public BigDecimal getSaldoVincAdm() {
        return saldoVincAdm;
    }

    /**
     * Set: saldoVincAdm.
     *
     * @param saldoVincAdm the saldo vinc adm
     */
    public void setSaldoVincAdm(BigDecimal saldoVincAdm) {
        this.saldoVincAdm = saldoVincAdm;
    }

    /**
     * Get: saldoVincAdmLP.
     *
     * @return saldoVincAdmLP
     */
    public BigDecimal getSaldoVincAdmLP() {
        return saldoVincAdmLP;
    }

    /**
     * Set: saldoVincAdmLP.
     *
     * @param saldoVincAdmLP the saldo vinc adm lp
     */
    public void setSaldoVincAdmLP(BigDecimal saldoVincAdmLP) {
        this.saldoVincAdmLP = saldoVincAdmLP;
    }

    /**
     * Get: saldoVincComReserva.
     *
     * @return saldoVincComReserva
     */
    public BigDecimal getSaldoVincComReserva() {
        return saldoVincComReserva;
    }

    /**
     * Set: saldoVincComReserva.
     *
     * @param saldoVincComReserva the saldo vinc com reserva
     */
    public void setSaldoVincComReserva(BigDecimal saldoVincComReserva) {
        this.saldoVincComReserva = saldoVincComReserva;
    }

    /**
     * Set: saldoVincJudicial.
     *
     * @param saldoVincJudicial the saldo vinc judicial
     */
    public void setSaldoVincJudicial(BigDecimal saldoVincJudicial) {
        this.saldoVincJudicial = saldoVincJudicial;
    }

    /**
     * Set: saldoVincSeg.
     *
     * @param saldoVincSeg the saldo vinc seg
     */
    public void setSaldoVincSeg(BigDecimal saldoVincSeg) {
        this.saldoVincSeg = saldoVincSeg;
    }

    /**
     * Set: saldoVincSemReserva.
     *
     * @param saldoVincSemReserva the saldo vinc sem reserva
     */
    public void setSaldoVincSemReserva(BigDecimal saldoVincSemReserva) {
        this.saldoVincSemReserva = saldoVincSemReserva;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
        return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
        this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
        return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
        this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
        return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
        this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
        this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
        this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
        return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
        return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
        this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: saldoVincJudicial.
     *
     * @return saldoVincJudicial
     */
    public BigDecimal getSaldoVincJudicial() {
        return saldoVincJudicial;
    }

    /**
     * Get: saldoVincSeg.
     *
     * @return saldoVincSeg
     */
    public BigDecimal getSaldoVincSeg() {
        return saldoVincSeg;
    }

    /**
     * Get: saldoVincSemReserva.
     *
     * @return saldoVincSemReserva
     */
    public BigDecimal getSaldoVincSemReserva() {
        return saldoVincSemReserva;
    }

    /**
     * Get: agenciaDigitoDestino.
     *
     * @return agenciaDigitoDestino
     */
    public String getAgenciaDigitoDestino() {
        return agenciaDigitoDestino;
    }

    /**
     * Set: agenciaDigitoDestino.
     *
     * @param agenciaDigitoDestino the agencia digito destino
     */
    public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
        this.agenciaDigitoDestino = agenciaDigitoDestino;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public String getCdBancoCredito() {
        return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(String cdBancoCredito) {
        this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Set: agenciaDigitoCredito.
     *
     * @param agenciaDigitoCredito the agencia digito credito
     */
    public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
        this.agenciaDigitoCredito = agenciaDigitoCredito;
    }

    /**
     * Get: cdBancoDestino.
     *
     * @return cdBancoDestino
     */
    public String getCdBancoDestino() {
        return cdBancoDestino;
    }

    /**
     * Set: cdBancoDestino.
     *
     * @param cdBancoDestino the cd banco destino
     */
    public void setCdBancoDestino(String cdBancoDestino) {
        this.cdBancoDestino = cdBancoDestino;
    }

    /**
     * Get: cdIndicadorEconomicoMoeda.
     *
     * @return cdIndicadorEconomicoMoeda
     */
    public String getCdIndicadorEconomicoMoeda() {
        return cdIndicadorEconomicoMoeda;
    }

    /**
     * Set: cdIndicadorEconomicoMoeda.
     *
     * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
     */
    public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
        this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
    }

    /**
     * Get: cdSerieDocumento.
     *
     * @return cdSerieDocumento
     */
    public String getCdSerieDocumento() {
        return cdSerieDocumento;
    }

    /**
     * Set: cdSerieDocumento.
     *
     * @param cdSerieDocumento the cd serie documento
     */
    public void setCdSerieDocumento(String cdSerieDocumento) {
        this.cdSerieDocumento = cdSerieDocumento;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public String getCdSituacaoOperacaoPagamento() {
        return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(
        String cdSituacaoOperacaoPagamento) {
        this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: contaDigitoCredito.
     *
     * @return contaDigitoCredito
     */
    public String getContaDigitoCredito() {
        return contaDigitoCredito;
    }

    /**
     * Set: contaDigitoCredito.
     *
     * @param contaDigitoCredito the conta digito credito
     */
    public void setContaDigitoCredito(String contaDigitoCredito) {
        this.contaDigitoCredito = contaDigitoCredito;
    }

    /**
     * Get: contaDigitoDestino.
     *
     * @return contaDigitoDestino
     */
    public String getContaDigitoDestino() {
        return contaDigitoDestino;
    }

    /**
     * Set: contaDigitoDestino.
     *
     * @param contaDigitoDestino the conta digito destino
     */
    public void setContaDigitoDestino(String contaDigitoDestino) {
        this.contaDigitoDestino = contaDigitoDestino;
    }

    /**
     * Get: dsAgenciaDebito.
     *
     * @return dsAgenciaDebito
     */
    public String getDsAgenciaDebito() {
        return dsAgenciaDebito;
    }

    /**
     * Set: dsAgenciaDebito.
     *
     * @param dsAgenciaDebito the ds agencia debito
     */
    public void setDsAgenciaDebito(String dsAgenciaDebito) {
        this.dsAgenciaDebito = dsAgenciaDebito;
    }

    /**
     * Get: dsAgenciaFavorecido.
     *
     * @return dsAgenciaFavorecido
     */
    public String getDsAgenciaFavorecido() {
        return dsAgenciaFavorecido;
    }

    /**
     * Set: dsAgenciaFavorecido.
     *
     * @param dsAgenciaFavorecido the ds agencia favorecido
     */
    public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
        this.dsAgenciaFavorecido = dsAgenciaFavorecido;
    }

    /**
     * Get: dsBancoDebito.
     *
     * @return dsBancoDebito
     */
    public String getDsBancoDebito() {
        return dsBancoDebito;
    }

    /**
     * Set: dsBancoDebito.
     *
     * @param dsBancoDebito the ds banco debito
     */
    public void setDsBancoDebito(String dsBancoDebito) {
        this.dsBancoDebito = dsBancoDebito;
    }

    /**
     * Get: dsBancoFavorecido.
     *
     * @return dsBancoFavorecido
     */
    public String getDsBancoFavorecido() {
        return dsBancoFavorecido;
    }

    /**
     * Set: dsBancoFavorecido.
     *
     * @param dsBancoFavorecido the ds banco favorecido
     */
    public void setDsBancoFavorecido(String dsBancoFavorecido) {
        this.dsBancoFavorecido = dsBancoFavorecido;
    }

    /**
     * Get: dsContaFavorecido.
     *
     * @return dsContaFavorecido
     */
    public String getDsContaFavorecido() {
        return dsContaFavorecido;
    }

    /**
     * Set: dsContaFavorecido.
     *
     * @param dsContaFavorecido the ds conta favorecido
     */
    public void setDsContaFavorecido(String dsContaFavorecido) {
        this.dsContaFavorecido = dsContaFavorecido;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
        return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
        this.dsFavorecido = dsFavorecido;
    }

    /**
     * Get: dsIndicadorModalidade.
     *
     * @return dsIndicadorModalidade
     */
    public String getDsIndicadorModalidade() {
        return dsIndicadorModalidade;
    }

    /**
     * Set: dsIndicadorModalidade.
     *
     * @param dsIndicadorModalidade the ds indicador modalidade
     */
    public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
        this.dsIndicadorModalidade = dsIndicadorModalidade;
    }

    /**
     * Get: dsMensagemPrimeiraLinha.
     *
     * @return dsMensagemPrimeiraLinha
     */
    public String getDsMensagemPrimeiraLinha() {
        return dsMensagemPrimeiraLinha;
    }

    /**
     * Set: dsMensagemPrimeiraLinha.
     *
     * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
     */
    public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
        this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
    }

    /**
     * Get: dsMensagemSegundaLinha.
     *
     * @return dsMensagemSegundaLinha
     */
    public String getDsMensagemSegundaLinha() {
        return dsMensagemSegundaLinha;
    }

    /**
     * Set: dsMensagemSegundaLinha.
     *
     * @param dsMensagemSegundaLinha the ds mensagem segunda linha
     */
    public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
        this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
    }

    /**
     * Get: dsMotivoSituacao.
     *
     * @return dsMotivoSituacao
     */
    public String getDsMotivoSituacao() {
        return dsMotivoSituacao;
    }

    /**
     * Set: dsMotivoSituacao.
     *
     * @param dsMotivoSituacao the ds motivo situacao
     */
    public void setDsMotivoSituacao(String dsMotivoSituacao) {
        this.dsMotivoSituacao = dsMotivoSituacao;
    }

    /**
     * Get: cpfCnpj.
     *
     * @return cpfCnpj
     */
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Set: cpfCnpj.
     *
     * @param cpfCnpj the cpf cnpj
     */
    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    /**
     * Get: dsPagamento.
     *
     * @return dsPagamento
     */
    public String getDsPagamento() {
        return dsPagamento;
    }

    /**
     * Set: dsPagamento.
     *
     * @param dsPagamento the ds pagamento
     */
    public void setDsPagamento(String dsPagamento) {
        this.dsPagamento = dsPagamento;
    }

    /**
     * Get: dsTipoContaFavorecido.
     *
     * @return dsTipoContaFavorecido
     */
    public String getDsTipoContaFavorecido() {
        return dsTipoContaFavorecido;
    }

    /**
     * Set: dsTipoContaFavorecido.
     *
     * @param dsTipoContaFavorecido the ds tipo conta favorecido
     */
    public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
        this.dsTipoContaFavorecido = dsTipoContaFavorecido;
    }

    /**
     * Get: dsTipoManutencao.
     *
     * @return dsTipoManutencao
     */
    public String getDsTipoManutencao() {
        return dsTipoManutencao;
    }

    /**
     * Set: dsTipoManutencao.
     *
     * @param dsTipoManutencao the ds tipo manutencao
     */
    public void setDsTipoManutencao(String dsTipoManutencao) {
        this.dsTipoManutencao = dsTipoManutencao;
    }

    /**
     * Get: dsUsoEmpresa.
     *
     * @return dsUsoEmpresa
     */
    public String getDsUsoEmpresa() {
        return dsUsoEmpresa;
    }

    /**
     * Set: dsUsoEmpresa.
     *
     * @param dsUsoEmpresa the ds uso empresa
     */
    public void setDsUsoEmpresa(String dsUsoEmpresa) {
        this.dsUsoEmpresa = dsUsoEmpresa;
    }

    /**
     * Get: dtAgendamento.
     *
     * @return dtAgendamento
     */
    public String getDtAgendamento() {
        return dtAgendamento;
    }

    /**
     * Set: dtAgendamento.
     *
     * @param dtAgendamento the dt agendamento
     */
    public void setDtAgendamento(String dtAgendamento) {
        this.dtAgendamento = dtAgendamento;
    }

    /**
     * Get: dtEmissaoDocumento.
     *
     * @return dtEmissaoDocumento
     */
    public String getDtEmissaoDocumento() {
        return dtEmissaoDocumento;
    }

    /**
     * Set: dtEmissaoDocumento.
     *
     * @param dtEmissaoDocumento the dt emissao documento
     */
    public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
        this.dtEmissaoDocumento = dtEmissaoDocumento;
    }

    /**
     * Get: dtNascimentoBeneficiario.
     *
     * @return dtNascimentoBeneficiario
     */
    public String getDtNascimentoBeneficiario() {
        return dtNascimentoBeneficiario;
    }

    /**
     * Set: dtNascimentoBeneficiario.
     *
     * @param dtNascimentoBeneficiario the dt nascimento beneficiario
     */
    public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
        this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
    }

    /**
     * Get: nomeBeneficiario.
     *
     * @return nomeBeneficiario
     */
    public String getNomeBeneficiario() {
        return nomeBeneficiario;
    }

    /**
     * Set: nomeBeneficiario.
     *
     * @param nomeBeneficiario the nome beneficiario
     */
    public void setNomeBeneficiario(String nomeBeneficiario) {
        this.nomeBeneficiario = nomeBeneficiario;
    }

    /**
     * Get: nrDocumento.
     *
     * @return nrDocumento
     */
    public String getNrDocumento() {
        return nrDocumento;
    }

    /**
     * Set: nrDocumento.
     *
     * @param nrDocumento the nr documento
     */
    public void setNrDocumento(String nrDocumento) {
        this.nrDocumento = nrDocumento;
    }

    /**
     * Get: numeroInscricaoBeneficiario.
     *
     * @return numeroInscricaoBeneficiario
     */
    public String getNumeroInscricaoBeneficiario() {
        return numeroInscricaoBeneficiario;
    }

    /**
     * Set: numeroInscricaoBeneficiario.
     *
     * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
     */
    public void setNumeroInscricaoBeneficiario(
        String numeroInscricaoBeneficiario) {
        this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
    }

    /**
     * Get: numeroInscricaoFavorecido.
     *
     * @return numeroInscricaoFavorecido
     */
    public String getNumeroInscricaoFavorecido() {
        return numeroInscricaoFavorecido;
    }

    /**
     * Set: numeroInscricaoFavorecido.
     *
     * @param numeroInscricaoFavorecido the numero inscricao favorecido
     */
    public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
        this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
    }

    /**
     * Get: tipoBeneficiario.
     *
     * @return tipoBeneficiario
     */
    public String getTipoBeneficiario() {
        return tipoBeneficiario;
    }

    /**
     * Set: tipoBeneficiario.
     *
     * @param tipoBeneficiario the tipo beneficiario
     */
    public void setTipoBeneficiario(String tipoBeneficiario) {
        this.tipoBeneficiario = tipoBeneficiario;
    }

    /**
     * Get: tipoContaCredito.
     *
     * @return tipoContaCredito
     */
    public String getTipoContaCredito() {
        return tipoContaCredito;
    }

    /**
     * Set: tipoContaCredito.
     *
     * @param tipoContaCredito the tipo conta credito
     */
    public void setTipoContaCredito(String tipoContaCredito) {
        this.tipoContaCredito = tipoContaCredito;
    }

    /**
     * Get: tipoContaDebito.
     *
     * @return tipoContaDebito
     */
    public String getTipoContaDebito() {
        return tipoContaDebito;
    }

    /**
     * Set: tipoContaDebito.
     *
     * @param tipoContaDebito the tipo conta debito
     */
    public void setTipoContaDebito(String tipoContaDebito) {
        this.tipoContaDebito = tipoContaDebito;
    }

    /**
     * Get: tipoContaDestino.
     *
     * @return tipoContaDestino
     */
    public String getTipoContaDestino() {
        return tipoContaDestino;
    }

    /**
     * Set: tipoContaDestino.
     *
     * @param tipoContaDestino the tipo conta destino
     */
    public void setTipoContaDestino(String tipoContaDestino) {
        this.tipoContaDestino = tipoContaDestino;
    }

    /**
     * Get: tipoDocumento.
     *
     * @return tipoDocumento
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Set: tipoDocumento.
     *
     * @param tipoDocumento the tipo documento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     * Get: tipoFavorecido.
     *
     * @return tipoFavorecido
     */
    public Integer getTipoFavorecido() {
        return tipoFavorecido;
    }

    /**
     * Set: tipoFavorecido.
     *
     * @param tipoFavorecido the tipo favorecido
     */
    public void setTipoFavorecido(Integer tipoFavorecido) {
        this.tipoFavorecido = tipoFavorecido;
    }

    /**
     * Get: vlDocumento.
     *
     * @return vlDocumento
     */
    public BigDecimal getVlDocumento() {
        return vlDocumento;
    }

    /**
     * Set: vlDocumento.
     *
     * @param vlDocumento the vl documento
     */
    public void setVlDocumento(BigDecimal vlDocumento) {
        this.vlDocumento = vlDocumento;
    }

    /**
     * Get: vlAbatimento.
     *
     * @return vlAbatimento
     */
    public BigDecimal getVlAbatimento() {
        return vlAbatimento;
    }

    /**
     * Set: vlAbatimento.
     *
     * @param vlAbatimento the vl abatimento
     */
    public void setVlAbatimento(BigDecimal vlAbatimento) {
        this.vlAbatimento = vlAbatimento;
    }

    /**
     * Get: vlAcrescimo.
     *
     * @return vlAcrescimo
     */
    public BigDecimal getVlAcrescimo() {
        return vlAcrescimo;
    }

    /**
     * Set: vlAcrescimo.
     *
     * @param vlAcrescimo the vl acrescimo
     */
    public void setVlAcrescimo(BigDecimal vlAcrescimo) {
        this.vlAcrescimo = vlAcrescimo;
    }

    /**
     * Get: vlDeducao.
     *
     * @return vlDeducao
     */
    public BigDecimal getVlDeducao() {
        return vlDeducao;
    }

    /**
     * Set: vlDeducao.
     *
     * @param vlDeducao the vl deducao
     */
    public void setVlDeducao(BigDecimal vlDeducao) {
        this.vlDeducao = vlDeducao;
    }

    /**
     * Get: vlDesconto.
     *
     * @return vlDesconto
     */
    public BigDecimal getVlDesconto() {
        return vlDesconto;
    }

    /**
     * Set: vlDesconto.
     *
     * @param vlDesconto the vl desconto
     */
    public void setVlDesconto(BigDecimal vlDesconto) {
        this.vlDesconto = vlDesconto;
    }

    /**
     * Get: vlImpostoRenda.
     *
     * @return vlImpostoRenda
     */
    public BigDecimal getVlImpostoRenda() {
        return vlImpostoRenda;
    }

    /**
     * Set: vlImpostoRenda.
     *
     * @param vlImpostoRenda the vl imposto renda
     */
    public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
        this.vlImpostoRenda = vlImpostoRenda;
    }

    /**
     * Get: vlInss.
     *
     * @return vlInss
     */
    public BigDecimal getVlInss() {
        return vlInss;
    }

    /**
     * Set: vlInss.
     *
     * @param vlInss the vl inss
     */
    public void setVlInss(BigDecimal vlInss) {
        this.vlInss = vlInss;
    }

    /**
     * Get: vlIof.
     *
     * @return vlIof
     */
    public BigDecimal getVlIof() {
        return vlIof;
    }

    /**
     * Set: vlIof.
     *
     * @param vlIof the vl iof
     */
    public void setVlIof(BigDecimal vlIof) {
        this.vlIof = vlIof;
    }

    /**
     * Get: vlIss.
     *
     * @return vlIss
     */
    public BigDecimal getVlIss() {
        return vlIss;
    }

    /**
     * Set: vlIss.
     *
     * @param vlIss the vl iss
     */
    public void setVlIss(BigDecimal vlIss) {
        this.vlIss = vlIss;
    }

    /**
     * Get: vlMora.
     *
     * @return vlMora
     */
    public BigDecimal getVlMora() {
        return vlMora;
    }

    /**
     * Set: vlMora.
     *
     * @param vlMora the vl mora
     */
    public void setVlMora(BigDecimal vlMora) {
        this.vlMora = vlMora;
    }

    /**
     * Get: vlMulta.
     *
     * @return vlMulta
     */
    public BigDecimal getVlMulta() {
        return vlMulta;
    }

    /**
     * Set: vlMulta.
     *
     * @param vlMulta the vl multa
     */
    public void setVlMulta(BigDecimal vlMulta) {
        this.vlMulta = vlMulta;
    }

    /**
     * Get: qtMoeda.
     *
     * @return qtMoeda
     */
    public BigDecimal getQtMoeda() {
        return qtMoeda;
    }

    /**
     * Set: qtMoeda.
     *
     * @param qtMoeda the qt moeda
     */
    public void setQtMoeda(BigDecimal qtMoeda) {
        this.qtMoeda = qtMoeda;
    }

    /**
     * Get: vlrAgendamento.
     *
     * @return vlrAgendamento
     */
    public BigDecimal getVlrAgendamento() {
        return vlrAgendamento;
    }

    /**
     * Set: vlrAgendamento.
     *
     * @param vlrAgendamento the vlr agendamento
     */
    public void setVlrAgendamento(BigDecimal vlrAgendamento) {
        this.vlrAgendamento = vlrAgendamento;
    }

    /**
     * Get: vlrEfetivacao.
     *
     * @return vlrEfetivacao
     */
    public BigDecimal getVlrEfetivacao() {
        return vlrEfetivacao;
    }

    /**
     * Set: vlrEfetivacao.
     *
     * @param vlrEfetivacao the vlr efetivacao
     */
    public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
        this.vlrEfetivacao = vlrEfetivacao;
    }

    /**
     * Get: contaDebitoGrid.
     *
     * @return contaDebitoGrid
     */
    public String getContaDebitoGrid() {
        return contaDebitoGrid;
    }

    /**
     * Set: contaDebitoGrid.
     *
     * @param contaDebitoGrid the conta debito grid
     */
    public void setContaDebitoGrid(String contaDebitoGrid) {
        this.contaDebitoGrid = contaDebitoGrid;
    }

    /**
     * Get: dsModalidadeGrid.
     *
     * @return dsModalidadeGrid
     */
    public String getDsModalidadeGrid() {
        return dsModalidadeGrid;
    }

    /**
     * Set: dsModalidadeGrid.
     *
     * @param dsModalidadeGrid the ds modalidade grid
     */
    public void setDsModalidadeGrid(String dsModalidadeGrid) {
        this.dsModalidadeGrid = dsModalidadeGrid;
    }

    /**
     * Get: numeroPagamentoGrid.
     *
     * @return numeroPagamentoGrid
     */
    public String getNumeroPagamentoGrid() {
        return numeroPagamentoGrid;
    }

    /**
     * Set: numeroPagamentoGrid.
     *
     * @param numeroPagamentoGrid the numero pagamento grid
     */
    public void setNumeroPagamentoGrid(String numeroPagamentoGrid) {
        this.numeroPagamentoGrid = numeroPagamentoGrid;
    }

    /**
     * Get: tipoServicoGrid.
     *
     * @return tipoServicoGrid
     */
    public String getTipoServicoGrid() {
        return tipoServicoGrid;
    }

    /**
     * Set: tipoServicoGrid.
     *
     * @param tipoServicoGrid the tipo servico grid
     */
    public void setTipoServicoGrid(String tipoServicoGrid) {
        this.tipoServicoGrid = tipoServicoGrid;
    }

    /**
     * Is manutencao.
     *
     * @return true, if is manutencao
     */
    public boolean isManutencao() {
        return manutencao;
    }

    /**
     * Set: manutencao.
     *
     * @param manutencao the manutencao
     */
    public void setManutencao(boolean manutencao) {
        this.manutencao = manutencao;
    }

    /**
     * Get: agendadosPagosNaoPagos.
     *
     * @return agendadosPagosNaoPagos
     */
    public String getAgendadosPagosNaoPagos() {
        return agendadosPagosNaoPagos;
    }

    /**
     * Set: agendadosPagosNaoPagos.
     *
     * @param agendadosPagosNaoPagos the agendados pagos nao pagos
     */
    public void setAgendadosPagosNaoPagos(String agendadosPagosNaoPagos) {
        this.agendadosPagosNaoPagos = agendadosPagosNaoPagos;
    }

    /**
     * Get: finalidadeTed.
     *
     * @return finalidadeTed
     */
    public String getFinalidadeTed() {
        return finalidadeTed;
    }

    /**
     * Set: finalidadeTed.
     *
     * @param finalidadeTed the finalidade ted
     */
    public void setFinalidadeTed(String finalidadeTed) {
        this.finalidadeTed = finalidadeTed;
    }

    /**
     * Get: identificacaoTransferenciaTed.
     *
     * @return identificacaoTransferenciaTed
     */
    public String getIdentificacaoTransferenciaTed() {
        return identificacaoTransferenciaTed;
    }

    /**
     * Set: identificacaoTransferenciaTed.
     *
     * @param identificacaoTransferenciaTed the identificacao transferencia ted
     */
    public void setIdentificacaoTransferenciaTed(
        String identificacaoTransferenciaTed) {
        this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
    }

    /**
     * Get: dataExpiracaoCredito.
     *
     * @return dataExpiracaoCredito
     */
    public String getDataExpiracaoCredito() {
        return dataExpiracaoCredito;
    }

    /**
     * Set: dataExpiracaoCredito.
     *
     * @param dataExpiracaoCredito the data expiracao credito
     */
    public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
        this.dataExpiracaoCredito = dataExpiracaoCredito;
    }

    /**
     * Get: dataRetirada.
     *
     * @return dataRetirada
     */
    public String getDataRetirada() {
        return dataRetirada;
    }

    /**
     * Set: dataRetirada.
     *
     * @param dataRetirada the data retirada
     */
    public void setDataRetirada(String dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    /**
     * Get: instrucaoPagamento.
     *
     * @return instrucaoPagamento
     */
    public String getInstrucaoPagamento() {
        return instrucaoPagamento;
    }

    /**
     * Set: instrucaoPagamento.
     *
     * @param instrucaoPagamento the instrucao pagamento
     */
    public void setInstrucaoPagamento(String instrucaoPagamento) {
        this.instrucaoPagamento = instrucaoPagamento;
    }

    /**
     * Get: serieCheque.
     *
     * @return serieCheque
     */
    public String getSerieCheque() {
        return serieCheque;
    }

    /**
     * Set: serieCheque.
     *
     * @param serieCheque the serie cheque
     */
    public void setSerieCheque(String serieCheque) {
        this.serieCheque = serieCheque;
    }

    /**
     * Get: identificadorRetirada.
     *
     * @return identificadorRetirada
     */
    public String getIdentificadorRetirada() {
        return identificadorRetirada;
    }

    /**
     * Set: identificadorRetirada.
     *
     * @param identificadorRetirada the identificador retirada
     */
    public void setIdentificadorRetirada(String identificadorRetirada) {
        this.identificadorRetirada = identificadorRetirada;
    }

    /**
     * Get: identificadorTipoRetirada.
     *
     * @return identificadorTipoRetirada
     */
    public String getIdentificadorTipoRetirada() {
        return identificadorTipoRetirada;
    }

    /**
     * Set: identificadorTipoRetirada.
     *
     * @param identificadorTipoRetirada the identificador tipo retirada
     */
    public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
        this.identificadorTipoRetirada = identificadorTipoRetirada;
    }

    /**
     * Get: indicadorAssociadoUnicaAgencia.
     *
     * @return indicadorAssociadoUnicaAgencia
     */
    public String getIndicadorAssociadoUnicaAgencia() {
        return indicadorAssociadoUnicaAgencia;
    }

    /**
     * Set: indicadorAssociadoUnicaAgencia.
     *
     * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
     */
    public void setIndicadorAssociadoUnicaAgencia(
        String indicadorAssociadoUnicaAgencia) {
        this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
    }

    /**
     * Get: numeroCheque.
     *
     * @return numeroCheque
     */
    public String getNumeroCheque() {
        return numeroCheque;
    }

    /**
     * Set: numeroCheque.
     *
     * @param numeroCheque the numero cheque
     */
    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    /**
     * Get: numeroOp.
     *
     * @return numeroOp
     */
    public String getNumeroOp() {
        return numeroOp;
    }

    /**
     * Set: numeroOp.
     *
     * @param numeroOp the numero op
     */
    public void setNumeroOp(String numeroOp) {
        this.numeroOp = numeroOp;
    }

    /**
     * Get: vlImpostoMovFinanceira.
     *
     * @return vlImpostoMovFinanceira
     */
    public BigDecimal getVlImpostoMovFinanceira() {
        return vlImpostoMovFinanceira;
    }

    /**
     * Set: vlImpostoMovFinanceira.
     *
     * @param vlImpostoMovFinanceira the vl imposto mov financeira
     */
    public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
        this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
    }

    /**
     * Get: codigoBarras.
     *
     * @return codigoBarras
     */
    public String getCodigoBarras() {
        return codigoBarras;
    }

    /**
     * Set: codigoBarras.
     *
     * @param codigoBarras the codigo barras
     */
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    /**
     * Get: codigoReceita.
     *
     * @return codigoReceita
     */
    public String getCodigoReceita() {
        return codigoReceita;
    }

    /**
     * Set: codigoReceita.
     *
     * @param codigoReceita the codigo receita
     */
    public void setCodigoReceita(String codigoReceita) {
        this.codigoReceita = codigoReceita;
    }

    /**
     * Get: periodoApuracao.
     *
     * @return periodoApuracao
     */
    public String getPeriodoApuracao() {
        return periodoApuracao;
    }

    /**
     * Set: periodoApuracao.
     *
     * @param periodoApuracao the periodo apuracao
     */
    public void setPeriodoApuracao(String periodoApuracao) {
        this.periodoApuracao = periodoApuracao;
    }

    /**
     * Get: telefoneContribuinte.
     *
     * @return telefoneContribuinte
     */
    public String getTelefoneContribuinte() {
        return telefoneContribuinte;
    }

    /**
     * Set: telefoneContribuinte.
     *
     * @param telefoneContribuinte the telefone contribuinte
     */
    public void setTelefoneContribuinte(String telefoneContribuinte) {
        this.telefoneContribuinte = telefoneContribuinte;
    }

    /**
     * Get: agenteArrecadador.
     *
     * @return agenteArrecadador
     */
    public String getAgenteArrecadador() {
        return agenteArrecadador;
    }

    /**
     * Set: agenteArrecadador.
     *
     * @param agenteArrecadador the agente arrecadador
     */
    public void setAgenteArrecadador(String agenteArrecadador) {
        this.agenteArrecadador = agenteArrecadador;
    }

    /**
     * Get: anoExercicio.
     *
     * @return anoExercicio
     */
    public String getAnoExercicio() {
        return anoExercicio;
    }

    /**
     * Set: anoExercicio.
     *
     * @param anoExercicio the ano exercicio
     */
    public void setAnoExercicio(String anoExercicio) {
        this.anoExercicio = anoExercicio;
    }

    /**
     * Get: dataReferencia.
     *
     * @return dataReferencia
     */
    public String getDataReferencia() {
        return dataReferencia;
    }

    /**
     * Set: dataReferencia.
     *
     * @param dataReferencia the data referencia
     */
    public void setDataReferencia(String dataReferencia) {
        this.dataReferencia = dataReferencia;
    }

    /**
     * Get: dddContribuinte.
     *
     * @return dddContribuinte
     */
    public String getDddContribuinte() {
        return dddContribuinte;
    }

    /**
     * Set: dddContribuinte.
     *
     * @param dddContribuinte the ddd contribuinte
     */
    public void setDddContribuinte(String dddContribuinte) {
        this.dddContribuinte = dddContribuinte;
    }

    /**
     * Get: inscricaoEstadualContribuinte.
     *
     * @return inscricaoEstadualContribuinte
     */
    public String getInscricaoEstadualContribuinte() {
        return inscricaoEstadualContribuinte;
    }

    /**
     * Set: inscricaoEstadualContribuinte.
     *
     * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
     */
    public void setInscricaoEstadualContribuinte(
        String inscricaoEstadualContribuinte) {
        this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
    }

    /**
     * Get: numeroCotaParcela.
     *
     * @return numeroCotaParcela
     */
    public String getNumeroCotaParcela() {
        return numeroCotaParcela;
    }

    /**
     * Set: numeroCotaParcela.
     *
     * @param numeroCotaParcela the numero cota parcela
     */
    public void setNumeroCotaParcela(String numeroCotaParcela) {
        this.numeroCotaParcela = numeroCotaParcela;
    }

    /**
     * Get: numeroReferencia.
     *
     * @return numeroReferencia
     */
    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    /**
     * Set: numeroReferencia.
     *
     * @param numeroReferencia the numero referencia
     */
    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    /**
     * Get: percentualReceita.
     *
     * @return percentualReceita
     */
    public BigDecimal getPercentualReceita() {
        return percentualReceita;
    }

    /**
     * Set: percentualReceita.
     *
     * @param percentualReceita the percentual receita
     */
    public void setPercentualReceita(BigDecimal percentualReceita) {
        this.percentualReceita = percentualReceita;
    }

    /**
     * Get: vlAtualizacaoMonetaria.
     *
     * @return vlAtualizacaoMonetaria
     */
    public BigDecimal getVlAtualizacaoMonetaria() {
        return vlAtualizacaoMonetaria;
    }

    /**
     * Set: vlAtualizacaoMonetaria.
     *
     * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
     */
    public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
        this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
    }

    /**
     * Get: vlPrincipal.
     *
     * @return vlPrincipal
     */
    public BigDecimal getVlPrincipal() {
        return vlPrincipal;
    }

    /**
     * Set: vlPrincipal.
     *
     * @param vlPrincipal the vl principal
     */
    public void setVlPrincipal(BigDecimal vlPrincipal) {
        this.vlPrincipal = vlPrincipal;
    }

    /**
     * Get: vlReceita.
     *
     * @return vlReceita
     */
    public BigDecimal getVlReceita() {
        return vlReceita;
    }

    /**
     * Set: vlReceita.
     *
     * @param vlReceita the vl receita
     */
    public void setVlReceita(BigDecimal vlReceita) {
        this.vlReceita = vlReceita;
    }

    /**
     * Get: vlTotal.
     *
     * @return vlTotal
     */
    public BigDecimal getVlTotal() {
        return vlTotal;
    }

    /**
     * Set: vlTotal.
     *
     * @param vlTotal the vl total
     */
    public void setVlTotal(BigDecimal vlTotal) {
        this.vlTotal = vlTotal;
    }

    /**
     * Get: codigoUfFavorecida.
     *
     * @return codigoUfFavorecida
     */
    public String getCodigoUfFavorecida() {
        return codigoUfFavorecida;
    }

    /**
     * Set: codigoUfFavorecida.
     *
     * @param codigoUfFavorecida the codigo uf favorecida
     */
    public void setCodigoUfFavorecida(String codigoUfFavorecida) {
        this.codigoUfFavorecida = codigoUfFavorecida;
    }

    /**
     * Get: descricaoUfFavorecida.
     *
     * @return descricaoUfFavorecida
     */
    public String getDescricaoUfFavorecida() {
        return descricaoUfFavorecida;
    }

    /**
     * Set: descricaoUfFavorecida.
     *
     * @param descricaoUfFavorecida the descricao uf favorecida
     */
    public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
        this.descricaoUfFavorecida = descricaoUfFavorecida;
    }

    /**
     * Get: nomeOrgaoFavorecido.
     *
     * @return nomeOrgaoFavorecido
     */
    public String getNomeOrgaoFavorecido() {
        return nomeOrgaoFavorecido;
    }

    /**
     * Set: nomeOrgaoFavorecido.
     *
     * @param nomeOrgaoFavorecido the nome orgao favorecido
     */
    public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
        this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
    }

    /**
     * Get: observacaoTributo.
     *
     * @return observacaoTributo
     */
    public String getObservacaoTributo() {
        return observacaoTributo;
    }

    /**
     * Set: observacaoTributo.
     *
     * @param observacaoTributo the observacao tributo
     */
    public void setObservacaoTributo(String observacaoTributo) {
        this.observacaoTributo = observacaoTributo;
    }

    /**
     * Get: placaVeiculo.
     *
     * @return placaVeiculo
     */
    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    /**
     * Set: placaVeiculo.
     *
     * @param placaVeiculo the placa veiculo
     */
    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    /**
     * Get: codigoCnae.
     *
     * @return codigoCnae
     */
    public String getCodigoCnae() {
        return codigoCnae;
    }

    /**
     * Set: codigoCnae.
     *
     * @param codigoCnae the codigo cnae
     */
    public void setCodigoCnae(String codigoCnae) {
        this.codigoCnae = codigoCnae;
    }

    /**
     * Get: codigoOrgaoFavorecido.
     *
     * @return codigoOrgaoFavorecido
     */
    public String getCodigoOrgaoFavorecido() {
        return codigoOrgaoFavorecido;
    }

    /**
     * Set: codigoOrgaoFavorecido.
     *
     * @param codigoOrgaoFavorecido the codigo orgao favorecido
     */
    public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
        this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
    }

    /**
     * Get: numeroParcelamento.
     *
     * @return numeroParcelamento
     */
    public String getNumeroParcelamento() {
        return numeroParcelamento;
    }

    /**
     * Set: numeroParcelamento.
     *
     * @param numeroParcelamento the numero parcelamento
     */
    public void setNumeroParcelamento(String numeroParcelamento) {
        this.numeroParcelamento = numeroParcelamento;
    }

    /**
     * Get: vlAcrescimoFinanceiro.
     *
     * @return vlAcrescimoFinanceiro
     */
    public BigDecimal getVlAcrescimoFinanceiro() {
        return vlAcrescimoFinanceiro;
    }

    /**
     * Set: vlAcrescimoFinanceiro.
     *
     * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
     */
    public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
        this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
    }

    /**
     * Get: vlHonorarioAdvogado.
     *
     * @return vlHonorarioAdvogado
     */
    public BigDecimal getVlHonorarioAdvogado() {
        return vlHonorarioAdvogado;
    }

    /**
     * Set: vlHonorarioAdvogado.
     *
     * @param vlHonorarioAdvogado the vl honorario advogado
     */
    public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
        this.vlHonorarioAdvogado = vlHonorarioAdvogado;
    }

    /**
     * Get: codigoTributo.
     *
     * @return codigoTributo
     */
    public String getCodigoTributo() {
        return codigoTributo;
    }

    /**
     * Set: codigoTributo.
     *
     * @param codigoTributo the codigo tributo
     */
    public void setCodigoTributo(String codigoTributo) {
        this.codigoTributo = codigoTributo;
    }

    /**
     * Get: codigoRenavam.
     *
     * @return codigoRenavam
     */
    public String getCodigoRenavam() {
        return codigoRenavam;
    }

    /**
     * Set: codigoRenavam.
     *
     * @param codigoRenavam the codigo renavam
     */
    public void setCodigoRenavam(String codigoRenavam) {
        this.codigoRenavam = codigoRenavam;
    }

    /**
     * Get: municipioTributo.
     *
     * @return municipioTributo
     */
    public String getMunicipioTributo() {
        return municipioTributo;
    }

    /**
     * Set: municipioTributo.
     *
     * @param municipioTributo the municipio tributo
     */
    public void setMunicipioTributo(String municipioTributo) {
        this.municipioTributo = municipioTributo;
    }

    /**
     * Get: numeroNsu.
     *
     * @return numeroNsu
     */
    public String getNumeroNsu() {
        return numeroNsu;
    }

    /**
     * Set: numeroNsu.
     *
     * @param numeroNsu the numero nsu
     */
    public void setNumeroNsu(String numeroNsu) {
        this.numeroNsu = numeroNsu;
    }

    /**
     * Get: opcaoRetiradaTributo.
     *
     * @return opcaoRetiradaTributo
     */
    public String getOpcaoRetiradaTributo() {
        return opcaoRetiradaTributo;
    }

    /**
     * Set: opcaoRetiradaTributo.
     *
     * @param opcaoRetiradaTributo the opcao retirada tributo
     */
    public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
        this.opcaoRetiradaTributo = opcaoRetiradaTributo;
    }

    /**
     * Get: opcaoTributo.
     *
     * @return opcaoTributo
     */
    public String getOpcaoTributo() {
        return opcaoTributo;
    }

    /**
     * Set: opcaoTributo.
     *
     * @param opcaoTributo the opcao tributo
     */
    public void setOpcaoTributo(String opcaoTributo) {
        this.opcaoTributo = opcaoTributo;
    }

    /**
     * Get: ufTributo.
     *
     * @return ufTributo
     */
    public String getUfTributo() {
        return ufTributo;
    }

    /**
     * Set: ufTributo.
     *
     * @param ufTributo the uf tributo
     */
    public void setUfTributo(String ufTributo) {
        this.ufTributo = ufTributo;
    }

    /**
     * Get: codigoInss.
     *
     * @return codigoInss
     */
    public String getCodigoInss() {
        return codigoInss;
    }

    /**
     * Set: codigoInss.
     *
     * @param codigoInss the codigo inss
     */
    public void setCodigoInss(String codigoInss) {
        this.codigoInss = codigoInss;
    }

    /**
     * Get: dataCompetencia.
     *
     * @return dataCompetencia
     */
    public String getDataCompetencia() {
        return dataCompetencia;
    }

    /**
     * Set: dataCompetencia.
     *
     * @param dataCompetencia the data competencia
     */
    public void setDataCompetencia(String dataCompetencia) {
        this.dataCompetencia = dataCompetencia;
    }

    /**
     * Get: vlOutrasEntidades.
     *
     * @return vlOutrasEntidades
     */
    public BigDecimal getVlOutrasEntidades() {
        return vlOutrasEntidades;
    }

    /**
     * Set: vlOutrasEntidades.
     *
     * @param vlOutrasEntidades the vl outras entidades
     */
    public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
        this.vlOutrasEntidades = vlOutrasEntidades;
    }

    /**
     * Get: bancoCartaoDepositoIdentificado.
     *
     * @return bancoCartaoDepositoIdentificado
     */
    public String getBancoCartaoDepositoIdentificado() {
        return bancoCartaoDepositoIdentificado;
    }

    /**
     * Set: bancoCartaoDepositoIdentificado.
     *
     * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
     */
    public void setBancoCartaoDepositoIdentificado(
        String bancoCartaoDepositoIdentificado) {
        this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
    }

    /**
     * Get: bimCartaoDepositoIdentificado.
     *
     * @return bimCartaoDepositoIdentificado
     */
    public String getBimCartaoDepositoIdentificado() {
        return bimCartaoDepositoIdentificado;
    }

    /**
     * Set: bimCartaoDepositoIdentificado.
     *
     * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
     */
    public void setBimCartaoDepositoIdentificado(
        String bimCartaoDepositoIdentificado) {
        this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
    }

    /**
     * Get: cartaoDepositoIdentificado.
     *
     * @return cartaoDepositoIdentificado
     */
    public String getCartaoDepositoIdentificado() {
        return cartaoDepositoIdentificado;
    }

    /**
     * Set: cartaoDepositoIdentificado.
     *
     * @param cartaoDepositoIdentificado the cartao deposito identificado
     */
    public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
        this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
    }

    /**
     * Get: clienteDepositoIdentificado.
     *
     * @return clienteDepositoIdentificado
     */
    public String getClienteDepositoIdentificado() {
        return clienteDepositoIdentificado;
    }

    /**
     * Set: clienteDepositoIdentificado.
     *
     * @param clienteDepositoIdentificado the cliente deposito identificado
     */
    public void setClienteDepositoIdentificado(
        String clienteDepositoIdentificado) {
        this.clienteDepositoIdentificado = clienteDepositoIdentificado;
    }

    /**
     * Get: codigoDepositante.
     *
     * @return codigoDepositante
     */
    public String getCodigoDepositante() {
        return codigoDepositante;
    }

    /**
     * Set: codigoDepositante.
     *
     * @param codigoDepositante the codigo depositante
     */
    public void setCodigoDepositante(String codigoDepositante) {
        this.codigoDepositante = codigoDepositante;
    }

    /**
     * Get: nomeDepositante.
     *
     * @return nomeDepositante
     */
    public String getNomeDepositante() {
        return nomeDepositante;
    }

    /**
     * Set: nomeDepositante.
     *
     * @param nomeDepositante the nome depositante
     */
    public void setNomeDepositante(String nomeDepositante) {
        this.nomeDepositante = nomeDepositante;
    }

    /**
     * Get: produtoDepositoIdentificado.
     *
     * @return produtoDepositoIdentificado
     */
    public String getProdutoDepositoIdentificado() {
        return produtoDepositoIdentificado;
    }

    /**
     * Set: produtoDepositoIdentificado.
     *
     * @param produtoDepositoIdentificado the produto deposito identificado
     */
    public void setProdutoDepositoIdentificado(
        String produtoDepositoIdentificado) {
        this.produtoDepositoIdentificado = produtoDepositoIdentificado;
    }

    /**
     * Get: sequenciaProdutoDepositoIdentificado.
     *
     * @return sequenciaProdutoDepositoIdentificado
     */
    public String getSequenciaProdutoDepositoIdentificado() {
        return sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Set: sequenciaProdutoDepositoIdentificado.
     *
     * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
     */
    public void setSequenciaProdutoDepositoIdentificado(
        String sequenciaProdutoDepositoIdentificado) {
        this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Get: tipoDespositoIdentificado.
     *
     * @return tipoDespositoIdentificado
     */
    public String getTipoDespositoIdentificado() {
        return tipoDespositoIdentificado;
    }

    /**
     * Set: tipoDespositoIdentificado.
     *
     * @param tipoDespositoIdentificado the tipo desposito identificado
     */
    public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
        this.tipoDespositoIdentificado = tipoDespositoIdentificado;
    }

    /**
     * Get: viaCartaoDepositoIdentificado.
     *
     * @return viaCartaoDepositoIdentificado
     */
    public String getViaCartaoDepositoIdentificado() {
        return viaCartaoDepositoIdentificado;
    }

    /**
     * Set: viaCartaoDepositoIdentificado.
     *
     * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
     */
    public void setViaCartaoDepositoIdentificado(
        String viaCartaoDepositoIdentificado) {
        this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
    }

    /**
     * Get: codigoOrdemCredito.
     *
     * @return codigoOrdemCredito
     */
    public String getCodigoOrdemCredito() {
        return codigoOrdemCredito;
    }

    /**
     * Set: codigoOrdemCredito.
     *
     * @param codigoOrdemCredito the codigo ordem credito
     */
    public void setCodigoOrdemCredito(String codigoOrdemCredito) {
        this.codigoOrdemCredito = codigoOrdemCredito;
    }

    /**
     * Get: codigoPagador.
     *
     * @return codigoPagador
     */
    public String getCodigoPagador() {
        return codigoPagador;
    }

    /**
     * Set: codigoPagador.
     *
     * @param codigoPagador the codigo pagador
     */
    public void setCodigoPagador(String codigoPagador) {
        this.codigoPagador = codigoPagador;
    }

    /**
     * Get: cdTipoBeneficiario.
     *
     * @return cdTipoBeneficiario
     */
    public Integer getCdTipoBeneficiario() {
        return cdTipoBeneficiario;
    }

    /**
     * Set: cdTipoBeneficiario.
     *
     * @param cdTipoBeneficiario the cd tipo beneficiario
     */
    public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
        this.cdTipoBeneficiario = cdTipoBeneficiario;
    }

    /**
     * Get: agenciaDigitoCredito.
     *
     * @return agenciaDigitoCredito
     */
    public String getAgenciaDigitoCredito() {
        return agenciaDigitoCredito;
    }

    /**
     * Get: cmbTipoConta.
     *
     * @return cmbTipoConta
     */
    public Integer getCmbTipoConta() {
        return cmbTipoConta;
    }

    /**
     * Set: cmbTipoConta.
     *
     * @param cmbTipoConta the cmb tipo conta
     */
    public void setCmbTipoConta(Integer cmbTipoConta) {
        this.cmbTipoConta = cmbTipoConta;
    }

    /**
     * Get: listaCmbTipoConta.
     *
     * @return listaCmbTipoConta
     */
    public List<SelectItem> getListaCmbTipoConta() {
        return listaCmbTipoConta;
    }

    /**
     * Set: listaCmbTipoConta.
     *
     * @param listaCmbTipoConta the lista cmb tipo conta
     */
    public void setListaCmbTipoConta(List<SelectItem> listaCmbTipoConta) {
        this.listaCmbTipoConta = listaCmbTipoConta;
    }

    /**
     * Get: vlPagamentoFormatada.
     *
     * @return vlPagamentoFormatada
     */
    public String getVlPagamentoFormatada() {
        return vlPagamentoFormatada;
    }

    /**
     * Set: vlPagamentoFormatada.
     *
     * @param vlPagamentoFormatada the vl pagamento formatada
     */
    public void setVlPagamentoFormatada(String vlPagamentoFormatada) {
        this.vlPagamentoFormatada = vlPagamentoFormatada;
    }

    /**
     * Get: listaSituacaoListaDebitoHash.
     *
     * @return listaSituacaoListaDebitoHash
     */
    public Map<Integer, ConsultarTipoContaSaidaDTO> getListaSituacaoListaDebitoHash() {
        return listaSituacaoListaDebitoHash;
    }

    /**
     * Set lista situacao lista debito hash.
     *
     * @param listaSituacaoListaDebitoHash the lista situacao lista debito hash
     */
    public void setListaSituacaoListaDebitoHash(
        Map<Integer, ConsultarTipoContaSaidaDTO> listaSituacaoListaDebitoHash) {
        this.listaSituacaoListaDebitoHash = listaSituacaoListaDebitoHash;
    }

    /**
     * Get: logoPath.
     *
     * @return logoPath
     */
    public String getLogoPath() {
        return logoPath;
    }

    /**
     * Set: logoPath.
     *
     * @param logoPath the logo path
     */
    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    /**
     * Get: contaDebito.
     *
     * @return contaDebito
     */
    public String getContaDebito() {
        return contaDebito;
    }

    /**
     * Set: contaDebito.
     *
     * @param contaDebito the conta debito
     */
    public void setContaDebito(String contaDebito) {
        this.contaDebito = contaDebito;
    }

    /**
     * Get: cdFavorecido.
     *
     * @return cdFavorecido
     */
    public Long getCdFavorecido() {
        return cdFavorecido;
    }

    /**
     * Set: cdFavorecido.
     *
     * @param cdFavorecido the cd favorecido
     */
    public void setCdFavorecido(Long cdFavorecido) {
        this.cdFavorecido = cdFavorecido;
    }

    /**
     * Get: dtVecimento.
     *
     * @return dtVecimento
     */
    public String getDtVecimento() {
        return dtVecimento;
    }

    /**
     * Set: dtVecimento.
     *
     * @param dtVecimento the dt vecimento
     */
    public void setDtVecimento(String dtVecimento) {
        this.dtVecimento = dtVecimento;
    }

    /**
     * Get: dsOrigemPagamento.
     *
     * @return dsOrigemPagamento
     */
    public String getDsOrigemPagamento() {
        return dsOrigemPagamento;
    }

    /**
     * Set: dsOrigemPagamento.
     *
     * @param dsOrigemPagamento the ds origem pagamento
     */
    public void setDsOrigemPagamento(String dsOrigemPagamento) {
        this.dsOrigemPagamento = dsOrigemPagamento;
    }

    /**
     * Get: nossoNumero.
     *
     * @return nossoNumero
     */
    public String getNossoNumero() {
        return nossoNumero;
    }

    /**
     * Set: nossoNumero.
     *
     * @param nossoNumero the nosso numero
     */
    public void setNossoNumero(String nossoNumero) {
        this.nossoNumero = nossoNumero;
    }

    /**
     * Is disable argumentos consulta.
     *
     * @return true, if is disable argumentos consulta
     */
    public boolean isDisableArgumentosConsulta() {
        return disableArgumentosConsulta;
    }

    /**
     * Set: disableArgumentosConsulta.
     *
     * @param disableArgumentosConsulta the disable argumentos consulta
     */
    public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
        this.disableArgumentosConsulta = disableArgumentosConsulta;
    }

    /**
     * Get: foco.
     *
     * @return foco
     */
    public String getFoco() {
        return foco;
    }

    /**
     * Set: foco.
     *
     * @param foco the foco
     */
    public void setFoco(String foco) {
        this.foco = foco;
    }

    /**
     * Is exibe beneficiario.
     *
     * @return true, if is exibe beneficiario
     */
    public boolean isExibeBeneficiario() {
        return exibeBeneficiario;
    }

    /**
     * Set: exibeBeneficiario.
     *
     * @param exibeBeneficiario the exibe beneficiario
     */
    public void setExibeBeneficiario(boolean exibeBeneficiario) {
        this.exibeBeneficiario = exibeBeneficiario;
    }

    /**
     * Get: carteira.
     *
     * @return carteira
     */
    public String getCarteira() {
        return carteira;
    }

    /**
     * Set: carteira.
     *
     * @param carteira the carteira
     */
    public void setCarteira(String carteira) {
        this.carteira = carteira;
    }

    /**
     * Get: cdIndentificadorJudicial.
     *
     * @return cdIndentificadorJudicial
     */
    public String getCdIndentificadorJudicial() {
        return cdIndentificadorJudicial;
    }

    /**
     * Set: cdIndentificadorJudicial.
     *
     * @param cdIndentificadorJudicial the cd indentificador judicial
     */
    public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
        this.cdIndentificadorJudicial = cdIndentificadorJudicial;
    }

    /**
     * Get: cdSituacaoTranferenciaAutomatica.
     *
     * @return cdSituacaoTranferenciaAutomatica
     */
    public String getCdSituacaoTranferenciaAutomatica() {
        return cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Set: cdSituacaoTranferenciaAutomatica.
     *
     * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
     */
    public void setCdSituacaoTranferenciaAutomatica(
        String cdSituacaoTranferenciaAutomatica) {
        this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Get: dtLimiteDescontoPagamento.
     *
     * @return dtLimiteDescontoPagamento
     */
    public String getDtLimiteDescontoPagamento() {
        return dtLimiteDescontoPagamento;
    }

    /**
     * Set: dtLimiteDescontoPagamento.
     *
     * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
     */
    public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
        this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
    }

    /**
     * Get: agenciaFavorecido.
     *
     * @return agenciaFavorecido
     */
    public String getAgenciaFavorecido() {
        return agenciaFavorecido;
    }

    /**
     * Set: agenciaFavorecido.
     *
     * @param agenciaFavorecido the agencia favorecido
     */
    public void setAgenciaFavorecido(String agenciaFavorecido) {
        this.agenciaFavorecido = agenciaFavorecido;
    }

    /**
     * Get: bancoFavorecido.
     *
     * @return bancoFavorecido
     */
    public String getBancoFavorecido() {
        return bancoFavorecido;
    }

    /**
     * Set: bancoFavorecido.
     *
     * @param bancoFavorecido the banco favorecido
     */
    public void setBancoFavorecido(String bancoFavorecido) {
        this.bancoFavorecido = bancoFavorecido;
    }

    /**
     * Get: cdTipoInscricaoFavorecido.
     *
     * @return cdTipoInscricaoFavorecido
     */
    public String getCdTipoInscricaoFavorecido() {
        return cdTipoInscricaoFavorecido;
    }

    /**
     * Set: cdTipoInscricaoFavorecido.
     *
     * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
     */
    public void setCdTipoInscricaoFavorecido(String cdTipoInscricaoFavorecido) {
        this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
    }

    /**
     * Get: contaFavorecido.
     *
     * @return contaFavorecido
     */
    public String getContaFavorecido() {
        return contaFavorecido;
    }

    /**
     * Set: contaFavorecido.
     *
     * @param contaFavorecido the conta favorecido
     */
    public void setContaFavorecido(String contaFavorecido) {
        this.contaFavorecido = contaFavorecido;
    }

    /**
     * Get: motivoPagamento.
     *
     * @return motivoPagamento
     */
    public String getMotivoPagamento() {
        return motivoPagamento;
    }

    /**
     * Set: motivoPagamento.
     *
     * @param motivoPagamento the motivo pagamento
     */
    public void setMotivoPagamento(String motivoPagamento) {
        this.motivoPagamento = motivoPagamento;
    }

    /**
     * Get: situacaoPagamento.
     *
     * @return situacaoPagamento
     */
    public String getSituacaoPagamento() {
        return situacaoPagamento;
    }

    /**
     * Set: situacaoPagamento.
     *
     * @param situacaoPagamento the situacao pagamento
     */
    public void setSituacaoPagamento(String situacaoPagamento) {
        this.situacaoPagamento = situacaoPagamento;
    }

    /**
     * Get: tipoContaFavorecido.
     *
     * @return tipoContaFavorecido
     */
    public String getTipoContaFavorecido() {
        return tipoContaFavorecido;
    }

    /**
     * Set: tipoContaFavorecido.
     *
     * @param tipoContaFavorecido the tipo conta favorecido
     */
    public void setTipoContaFavorecido(String tipoContaFavorecido) {
        this.tipoContaFavorecido = tipoContaFavorecido;
    }

    /**
     * Get: descTipoFavorecido.
     *
     * @return descTipoFavorecido
     */
    public String getDescTipoFavorecido() {
        return descTipoFavorecido;
    }

    /**
     * Set: descTipoFavorecido.
     *
     * @param descTipoFavorecido the desc tipo favorecido
     */
    public void setDescTipoFavorecido(String descTipoFavorecido) {
        this.descTipoFavorecido = descTipoFavorecido;
    }

    /**
     * Get: dsTipoBeneficiario.
     *
     * @return dsTipoBeneficiario
     */
    public String getDsTipoBeneficiario() {
        return dsTipoBeneficiario;
    }

    /**
     * Set: dsTipoBeneficiario.
     *
     * @param dsTipoBeneficiario the ds tipo beneficiario
     */
    public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
        this.dsTipoBeneficiario = dsTipoBeneficiario;
    }

    /**
     * Get: dtPagamentoGare.
     *
     * @return dtPagamentoGare
     */
    public Date getDtPagamentoGare() {
        return dtPagamentoGare;
    }

    /**
     * Set: dtPagamentoGare.
     *
     * @param dtPagamentoGare the dt pagamento gare
     */
    public void setDtPagamentoGare(Date dtPagamentoGare) {
        this.dtPagamentoGare = dtPagamentoGare;
    }

    /**
     * Get: dtPagamentoManutencao.
     *
     * @return dtPagamentoManutencao
     */
    public Date getDtPagamentoManutencao() {
        return dtPagamentoManutencao;
    }

    /**
     * Set: dtPagamentoManutencao.
     *
     * @param dtPagamentoManutencao the dt pagamento manutencao
     */
    public void setDtPagamentoManutencao(Date dtPagamentoManutencao) {
        this.dtPagamentoManutencao = dtPagamentoManutencao;
    }

    /**
     * Get: cdBancoOriginal.
     *
     * @return cdBancoOriginal
     */
    public Integer getCdBancoOriginal() {
        return cdBancoOriginal;
    }

    /**
     * Set: cdBancoOriginal.
     *
     * @param cdBancoOriginal the cd banco original
     */
    public void setCdBancoOriginal(Integer cdBancoOriginal) {
        this.cdBancoOriginal = cdBancoOriginal;
    }

    /**
     * Get: dsBancoOriginal.
     *
     * @return dsBancoOriginal
     */
    public String getDsBancoOriginal() {
        return dsBancoOriginal;
    }

    /**
     * Set: dsBancoOriginal.
     *
     * @param dsBancoOriginal the ds banco original
     */
    public void setDsBancoOriginal(String dsBancoOriginal) {
        this.dsBancoOriginal = dsBancoOriginal;
    }

    /**
     * Get: cdAgenciaBancariaOriginal.
     *
     * @return cdAgenciaBancariaOriginal
     */
    public Integer getCdAgenciaBancariaOriginal() {
        return cdAgenciaBancariaOriginal;
    }

    /**
     * Set: cdAgenciaBancariaOriginal.
     *
     * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
     */
    public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
        this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
    }

    /**
     * Get: cdDigitoAgenciaOriginal.
     *
     * @return cdDigitoAgenciaOriginal
     */
    public String getCdDigitoAgenciaOriginal() {
        return cdDigitoAgenciaOriginal;
    }

    /**
     * Set: cdDigitoAgenciaOriginal.
     *
     * @param cdDigitoAgenciaOriginal the cd digito agencia original
     */
    public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
        this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
    }

    /**
     * Get: dsAgenciaOriginal.
     *
     * @return dsAgenciaOriginal
     */
    public String getDsAgenciaOriginal() {
        return dsAgenciaOriginal;
    }

    /**
     * Set: dsAgenciaOriginal.
     *
     * @param dsAgenciaOriginal the ds agencia original
     */
    public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
        this.dsAgenciaOriginal = dsAgenciaOriginal;
    }

    /**
     * Get: cdContaBancariaOriginal.
     *
     * @return cdContaBancariaOriginal
     */
    public Long getCdContaBancariaOriginal() {
        return cdContaBancariaOriginal;
    }

    /**
     * Set: cdContaBancariaOriginal.
     *
     * @param cdContaBancariaOriginal the cd conta bancaria original
     */
    public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
        this.cdContaBancariaOriginal = cdContaBancariaOriginal;
    }

    /**
     * Get: cdDigitoContaOriginal.
     *
     * @return cdDigitoContaOriginal
     */
    public String getCdDigitoContaOriginal() {
        return cdDigitoContaOriginal;
    }

    /**
     * Set: cdDigitoContaOriginal.
     *
     * @param cdDigitoContaOriginal the cd digito conta original
     */
    public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
        this.cdDigitoContaOriginal = cdDigitoContaOriginal;
    }

    /**
     * Get: dsTipoContaOriginal.
     *
     * @return dsTipoContaOriginal
     */
    public String getDsTipoContaOriginal() {
        return dsTipoContaOriginal;
    }

    /**
     * Set: dsTipoContaOriginal.
     *
     * @param dsTipoContaOriginal the ds tipo conta original
     */
    public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
        this.dsTipoContaOriginal = dsTipoContaOriginal;
    }

    /**
     * Get: dtDevolucaoEstorno.
     *
     * @return dtDevolucaoEstorno
     */
    public String getDtDevolucaoEstorno() {
        return dtDevolucaoEstorno;
    }

    /**
     * Set: dtDevolucaoEstorno.
     *
     * @param dtDevolucaoEstorno the dt devolucao estorno
     */
    public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
        this.dtDevolucaoEstorno = dtDevolucaoEstorno;
    }

    /**
     * Get: dtLimitePagamento.
     *
     * @return dtLimitePagamento
     */
    public String getDtLimitePagamento() {
        return dtLimitePagamento;
    }

    /**
     * Set: dtLimitePagamento.
     *
     * @param dtLimitePagamento the dt limite pagamento
     */
    public void setDtLimitePagamento(String dtLimitePagamento) {
        this.dtLimitePagamento = dtLimitePagamento;
    }

    /**
     * Get: dsRastreado.
     *
     * @return dsRastreado
     */
    public String getDsRastreado() {
        return dsRastreado;
    }

    /**
     * Set: dsRastreado.
     *
     * @param dsRastreado the ds rastreado
     */
    public void setDsRastreado(String dsRastreado) {
        this.dsRastreado = dsRastreado;
    }

    /**
     * Get: dtFloatingPagamento.
     *
     * @return dtFloatingPagamento
     */
    public String getDtFloatingPagamento() {
        return dtFloatingPagamento;
    }

    /**
     * Set: dtFloatingPagamento.
     *
     * @param dtFloatingPagamento the dt floating pagamento
     */
    public void setDtFloatingPagamento(String dtFloatingPagamento) {
        this.dtFloatingPagamento = dtFloatingPagamento;
    }

    /**
     * Get: dtEfetivFloatPgto.
     *
     * @return dtEfetivFloatPgto
     */
    public String getDtEfetivFloatPgto() {
        return dtEfetivFloatPgto;
    }

    /**
     * Set: dtEfetivFloatPgto.
     *
     * @param dtEfetivFloatPgto the dt efetiv float pgto
     */
    public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
        this.dtEfetivFloatPgto = dtEfetivFloatPgto;
    }

    /**
     * Get: vlFloatingPagamento.
     *
     * @return vlFloatingPagamento
     */
    public BigDecimal getVlFloatingPagamento() {
        return vlFloatingPagamento;
    }

    /**
     * Set: vlFloatingPagamento.
     *
     * @param vlFloatingPagamento the vl floating pagamento
     */
    public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
        this.vlFloatingPagamento = vlFloatingPagamento;
    }

    /**
     * Get: cdOperacaoDcom.
     *
     * @return cdOperacaoDcom
     */
    public Long getCdOperacaoDcom() {
        return cdOperacaoDcom;
    }

    /**
     * Set: cdOperacaoDcom.
     *
     * @param cdOperacaoDcom the cd operacao dcom
     */
    public void setCdOperacaoDcom(Long cdOperacaoDcom) {
        this.cdOperacaoDcom = cdOperacaoDcom;
    }

    /**
     * Get: dsSituacaoDcom.
     *
     * @return dsSituacaoDcom
     */
    public String getDsSituacaoDcom() {
        return dsSituacaoDcom;
    }

    /**
     * Set: dsSituacaoDcom.
     *
     * @param dsSituacaoDcom the ds situacao dcom
     */
    public void setDsSituacaoDcom(String dsSituacaoDcom) {
        this.dsSituacaoDcom = dsSituacaoDcom;
    }

    /**
     * Get: exibeDcom.
     *
     * @return exibeDcom
     */
    public Boolean getExibeDcom() {
        return exibeDcom;
    }

    /**
     * Set: exibeDcom.
     *
     * @param exibeDcom the exibe dcom
     */
    public void setExibeDcom(Boolean exibeDcom) {
        this.exibeDcom = exibeDcom;
    }

    /**
     * Get: vlBonfcao.
     *
     * @return vlBonfcao
     */
    public BigDecimal getVlBonfcao() {
        return vlBonfcao;
    }

    /**
     * Set: vlBonfcao.
     *
     * @param vlBonfcao the vl bonfcao
     */
    public void setVlBonfcao(BigDecimal vlBonfcao) {
        this.vlBonfcao = vlBonfcao;
    }

    /**
     * Get: numControleInternoLote.
     *
     * @return numControleInternoLote
     */
    public Long getNumControleInternoLote() {
        return numControleInternoLote;
    }

    /**
     * Set: numControleInternoLote.
     *
     * @param numControleInternoLote the num controle interno lote
     */
    public void setNumControleInternoLote(Long numControleInternoLote) {
        this.numControleInternoLote = numControleInternoLote;
    }

    /**
     * Get: nrLote.
     *
     * @return nrLote
     */
    public String getNrLote() {
        return nrLote;
    }

    /**
     * Set: nrLote.
     *
     * @param nrLote the nr lote
     */
    public void setNrLote(String nrLote) {
        this.nrLote = nrLote;
    }

    /**
     * Get: vlBonificacao.
     *
     * @return vlBonificacao
     */
    public BigDecimal getVlBonificacao() {
        return vlBonificacao;
    }

    /**
     * Set: vlBonificacao.
     *
     * @param vlBonificacao the vl bonificacao
     */
    public void setVlBonificacao(BigDecimal vlBonificacao) {
        this.vlBonificacao = vlBonificacao;
    }

    /**
     * Set: cdFuncEmissaoOp.
     *
     * @param cdFuncEmissaoOp the cd func emissao op
     */
    public void setCdFuncEmissaoOp(String cdFuncEmissaoOp) {
        this.cdFuncEmissaoOp = cdFuncEmissaoOp;
    }

    /**
     * Get: cdFuncEmissaoOp.
     *
     * @return cdFuncEmissaoOp
     */
    public String getCdFuncEmissaoOp() {
        return cdFuncEmissaoOp;
    }

    /**
     * Set: dtEmissaoOp.
     *
     * @param dtEmissaoOp the dt emissao op
     */
    public void setDtEmissaoOp(String dtEmissaoOp) {
        this.dtEmissaoOp = dtEmissaoOp;
    }

    /**
     * Get: dtEmissaoOp.
     *
     * @return dtEmissaoOp
     */
    public String getDtEmissaoOp() {
        return dtEmissaoOp;
    }

    /**
     * Set: cdTerminalCaixaEmissaoOp.
     *
     * @param cdTerminalCaixaEmissaoOp the cd terminal caixa emissao op
     */
    public void setCdTerminalCaixaEmissaoOp(String cdTerminalCaixaEmissaoOp) {
        this.cdTerminalCaixaEmissaoOp = cdTerminalCaixaEmissaoOp;
    }

    /**
     * Get: cdTerminalCaixaEmissaoOp.
     *
     * @return cdTerminalCaixaEmissaoOp
     */
    public String getCdTerminalCaixaEmissaoOp() {
        return cdTerminalCaixaEmissaoOp;
    }

    /**
     * Set: hrEmissaoOp.
     *
     * @param hrEmissaoOp the hr emissao op
     */
    public void setHrEmissaoOp(String hrEmissaoOp) {
        this.hrEmissaoOp = hrEmissaoOp;
    }

    /**
     * Get: hrEmissaoOp.
     *
     * @return hrEmissaoOp
     */
    public String getHrEmissaoOp() {
        return hrEmissaoOp;
    }

    /**
     * Set: tipoTela.
     *
     * @param tipoTela the tipo tela
     */
    public void setTipoTela(Integer tipoTela) {
        this.tipoTela = tipoTela;
    }

    /**
     * Get: tipoTela.
     *
     * @return tipoTela
     */
    public Integer getTipoTela() {
        return tipoTela;
    }

    /**
     * Set: radioFavorecidoFiltro.
     *
     * @param radioFavorecidoFiltro the radio favorecido filtro
     */
    public void setRadioFavorecidoFiltro(String radioFavorecidoFiltro) {
        this.radioFavorecidoFiltro = radioFavorecidoFiltro;
    }

    /**
     * Get: radioFavorecidoFiltro.
     *
     * @return radioFavorecidoFiltro
     */
    public String getRadioFavorecidoFiltro() {
        return radioFavorecidoFiltro;
    }

    /**
     * Get: bancoSalario.
     *
     * @return the bancoSalario
     */
    public Integer getBancoSalario() {
        return bancoSalario;
    }

    /**
     * Set: bancoSalario.
     *
     * @param bancoSalario the bancoSalario to set
     */
    public void setBancoSalario(Integer bancoSalario) {
        this.bancoSalario = bancoSalario;
    }

    /**
     * Get: agenciaSalario.
     *
     * @return the agenciaSalario
     */
    public Integer getAgenciaSalario() {
        return agenciaSalario;
    }

    /**
     * Set: agenciaSalario.
     *
     * @param agenciaSalario the agenciaSalario to set
     */
    public void setAgenciaSalario(Integer agenciaSalario) {
        this.agenciaSalario = agenciaSalario;
    }

    /**
     * Get: contaSalario.
     *
     * @return the contaSalario
     */
    public Long getContaSalario() {
        return contaSalario;
    }

    /**
     * Set: contaSalario.
     *
     * @param contaSalario the contaSalario to set
     */
    public void setContaSalario(Long contaSalario) {
        this.contaSalario = contaSalario;
    }

    /**
     * Get: vlEfetivacaoDebitoPagamento.
     *
     * @return the vlEfetivacaoDebitoPagamento
     */
    public BigDecimal getVlEfetivacaoDebitoPagamento() {
        return vlEfetivacaoDebitoPagamento;
    }

    /**
     * Set: vlEfetivacaoDebitoPagamento.
     *
     * @param vlEfetivacaoDebitoPagamento the vlEfetivacaoDebitoPagamento to set
     */
    public void setVlEfetivacaoDebitoPagamento(
        BigDecimal vlEfetivacaoDebitoPagamento) {
        this.vlEfetivacaoDebitoPagamento = vlEfetivacaoDebitoPagamento;
    }

    /**
     * Get: vlEfetivacaoCreditoPagamento.
     *
     * @return the vlEfetivacaoCreditoPagamento
     */
    public BigDecimal getVlEfetivacaoCreditoPagamento() {
        return vlEfetivacaoCreditoPagamento;
    }

    /**
     * Set: vlEfetivacaoCreditoPagamento.
     *
     * @param vlEfetivacaoCreditoPagamento the vlEfetivacaoCreditoPagamento to set
     */
    public void setVlEfetivacaoCreditoPagamento(
        BigDecimal vlEfetivacaoCreditoPagamento) {
        this.vlEfetivacaoCreditoPagamento = vlEfetivacaoCreditoPagamento;
    }

    /**
     * Get: vlDescontoPagamento.
     *
     * @return the vlDescontoPagamento
     */
    public BigDecimal getVlDescontoPagamento() {
        return vlDescontoPagamento;
    }

    /**
     * Set: vlDescontoPagamento.
     *
     * @param vlDescontoPagamento the vlDescontoPagamento to set
     */
    public void setVlDescontoPagamento(BigDecimal vlDescontoPagamento) {
        this.vlDescontoPagamento = vlDescontoPagamento;
    }

    /**
     * Get: hrEfetivacaoCreditoPagamento.
     *
     * @return the hrEfetivacaoCreditoPagamento
     */
    public String getHrEfetivacaoCreditoPagamento() {
        return hrEfetivacaoCreditoPagamento;
    }

    /**
     * Set: hrEfetivacaoCreditoPagamento.
     *
     * @param hrEfetivacaoCreditoPagamento the hrEfetivacaoCreditoPagamento to set
     */
    public void setHrEfetivacaoCreditoPagamento(String hrEfetivacaoCreditoPagamento) {
        this.hrEfetivacaoCreditoPagamento = hrEfetivacaoCreditoPagamento;
    }

    /**
     * Get: hrEnvioCreditoPagamento.
     *
     * @return the hrEnvioCreditoPagamento
     */
    public String getHrEnvioCreditoPagamento() {
        return hrEnvioCreditoPagamento;
    }

    /**
     * Set: hrEnvioCreditoPagamento.
     *
     * @param hrEnvioCreditoPagamento the hrEnvioCreditoPagamento to set
     */
    public void setHrEnvioCreditoPagamento(String hrEnvioCreditoPagamento) {
        this.hrEnvioCreditoPagamento = hrEnvioCreditoPagamento;
    }

    /**
     * Get: cdIdentificadorTransferenciaPagto.
     *
     * @return the cdIdentificadorTransferenciaPagto
     */
    public Integer getCdIdentificadorTransferenciaPagto() {
        return cdIdentificadorTransferenciaPagto;
    }

    /**
     * Set: cdIdentificadorTransferenciaPagto.
     *
     * @param cdIdentificadorTransferenciaPagto the cdIdentificadorTransferenciaPagto to set
     */
    public void setCdIdentificadorTransferenciaPagto(
        Integer cdIdentificadorTransferenciaPagto) {
        this.cdIdentificadorTransferenciaPagto = cdIdentificadorTransferenciaPagto;
    }

    /**
     * Get: cdMensagemLinExtrato.
     *
     * @return the cdMensagemLinExtrato
     */
    public Integer getCdMensagemLinExtrato() {
        return cdMensagemLinExtrato;
    }

    /**
     * Set: cdMensagemLinExtrato.
     *
     * @param cdMensagemLinExtrato the cdMensagemLinExtrato to set
     */
    public void setCdMensagemLinExtrato(Integer cdMensagemLinExtrato) {
        this.cdMensagemLinExtrato = cdMensagemLinExtrato;
    }

    /**
     * Get: cdConveCtaSalarial.
     *
     * @return the cdConveCtaSalarial
     */
    public Long getCdConveCtaSalarial() {
        return cdConveCtaSalarial;
    }

    /**
     * Set: cdConveCtaSalarial.
     *
     * @param cdConveCtaSalarial the cdConveCtaSalarial to set
     */
    public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
        this.cdConveCtaSalarial = cdConveCtaSalarial;
    }

    /**
     * Is flag exibe campos.
     *
     * @return the flagExibeCampos
     */
    public boolean isFlagExibeCampos() {
        return flagExibeCampos;
    }

    /**
     * Set: flagExibeCampos.
     *
     * @param flagExibeCampos the flagExibeCampos to set
     */
    public void setFlagExibeCampos(boolean flagExibeCampos) {
        this.flagExibeCampos = flagExibeCampos;
    }

    /**
     * Set: dsEstornoPagamento.
     *
     * @param dsEstornoPagamento the ds estorno pagamento
     */
    public void setDsEstornoPagamento(String dsEstornoPagamento) {
        this.dsEstornoPagamento = dsEstornoPagamento;
    }

    /**
     * Get: dsEstornoPagamento.
     *
     * @return dsEstornoPagamento
     */
    public String getDsEstornoPagamento() {
        return dsEstornoPagamento;
    }

    /**
     * Set: dsIdentificadorTransferenciaPagto.
     *
     * @param dsIdentificadorTransferenciaPagto the ds identificador transferencia pagto
     */
    public void setDsIdentificadorTransferenciaPagto(
        String dsIdentificadorTransferenciaPagto) {
        this.dsIdentificadorTransferenciaPagto = dsIdentificadorTransferenciaPagto;
    }

    /**
     * Get: dsIdentificadorTransferenciaPagto.
     *
     * @return dsIdentificadorTransferenciaPagto
     */
    public String getDsIdentificadorTransferenciaPagto() {
        return dsIdentificadorTransferenciaPagto;
    }

    /**
     * Set: dsBancoCredito.
     *
     * @param dsBancoCredito the dsBancoCredito to set
     */
    public void setDsBancoCredito(String dsBancoCredito) {
        this.dsBancoCredito = dsBancoCredito;
    }

    /**
     * Get: dsBancoCredito.
     *
     * @return the dsBancoCredito
     */
    public String getDsBancoCredito() {
        return dsBancoCredito;
    }

    /**
     * Set: cdIspbPagamento.
     *
     * @param cdIspbPagamento the cdIspbPagamento to set
     */
    public void setCdIspbPagamento(String cdIspbPagamento) {
        this.cdIspbPagamento = cdIspbPagamento;
    }

    /**
     * Get: cdIspbPagamento.
     *
     * @return the cdIspbPagamento
     */
    public String getCdIspbPagamento() {
        return cdIspbPagamento;
    }

    /**
     * Get: consultaAutorizanteService.
     *
     * @return consultaAutorizanteService
     */
    public IConsultaAutorizanteService getConsultaAutorizanteService() {
        return consultaAutorizanteService;
    }

    /**
     * Set: consultaAutorizanteService.
     *
     * @param consultaAutorizanteService the consulta autorizante service
     */
    public void setConsultaAutorizanteService(IConsultaAutorizanteService consultaAutorizanteService) {
        this.consultaAutorizanteService = consultaAutorizanteService;
    }

    /**
     * Get: consultarAutorizantesOcorrencia.
     *
     * @return consultarAutorizantesOcorrencia
     */
    public ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> getConsultarAutorizantesOcorrencia() {
        return consultarAutorizantesOcorrencia;
    }

    /**
     * Set: consultarAutorizantesOcorrencia.
     *
     * @param consultarAutorizantesOcorrencia the consultar autorizantes ocorrencia
     */
    public void setConsultarAutorizantesOcorrencia(
        ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> consultarAutorizantesOcorrencia) {
        this.consultarAutorizantesOcorrencia = consultarAutorizantesOcorrencia;
    }

    /**
     * Get: itemSelecionadoListaAutorizante.
     *
     * @return itemSelecionadoListaAutorizante
     */
    public Integer getItemSelecionadoListaAutorizante() {
        return itemSelecionadoListaAutorizante;
    }

    /**
     * Set: itemSelecionadoListaAutorizante.
     *
     * @param itemSelecionadoListaAutorizante the item selecionado lista autorizante
     */
    public void setItemSelecionadoListaAutorizante(Integer itemSelecionadoListaAutorizante) {
        this.itemSelecionadoListaAutorizante = itemSelecionadoListaAutorizante;
    }

    /**
     * Get: listaRadioAutorizante.
     *
     * @return listaRadioAutorizante
     */
    public ArrayList<SelectItem> getListaRadioAutorizante() {
        return listaRadioAutorizante;
    }

    /**
     * Set: listaRadioAutorizante.
     *
     * @param listaRadioAutorizante the lista radio autorizante
     */
    public void setListaRadioAutorizante(ArrayList<SelectItem> listaRadioAutorizante) {
        this.listaRadioAutorizante = listaRadioAutorizante;
    }

    /**
     * Get: detalharPagtoTed.
     *
     * @return detalharPagtoTed
     */
    public DetalharPagtoTEDSaidaDTO getDetalharPagtoTed() {
        return detalharPagtoTed;
    }

    /**
     * Set: detalharPagtoTed.
     *
     * @param detalharPagtoTed the detalhar pagto ted
     */
    public void setDetalharPagtoTed(DetalharPagtoTEDSaidaDTO detalharPagtoTed) {
        this.detalharPagtoTed = detalharPagtoTed;
    }

    /**
     * Get: dsNomeReclamante.
     *
     * @return dsNomeReclamante
     */
    public String getDsNomeReclamante() {
        return dsNomeReclamante;
    }

    /**
     * Set: dsNomeReclamante.
     *
     * @param dsNomeReclamante the ds nome reclamante
     */
    public void setDsNomeReclamante(String dsNomeReclamante) {
        this.dsNomeReclamante = dsNomeReclamante;
    }

    /**
     * Get: nrProcessoDepositoJudicial.
     *
     * @return nrProcessoDepositoJudicial
     */
    public String getNrProcessoDepositoJudicial() {
        return nrProcessoDepositoJudicial;
    }

    /**
     * Set: nrProcessoDepositoJudicial.
     *
     * @param nrProcessoDepositoJudicial the nr processo deposito judicial
     */
    public void setNrProcessoDepositoJudicial(String nrProcessoDepositoJudicial) {
        this.nrProcessoDepositoJudicial = nrProcessoDepositoJudicial;
    }

    /**
     * Get: nrPisPasep.
     *
     * @return nrPisPasep
     */
    public String getNrPisPasep() {
        return nrPisPasep;
    }

    /**
     * Set: nrPisPasep.
     *
     * @param nrPisPasep the nr pis pasep
     */
    public void setNrPisPasep(String nrPisPasep) {
        this.nrPisPasep = nrPisPasep;
    }

    /**
     * Is cd finalidade ted deposito judicial.
     *
     * @return true, if is cd finalidade ted deposito judicial
     */
    public boolean isCdFinalidadeTedDepositoJudicial() {
        return cdFinalidadeTedDepositoJudicial;
    }

    /**
     * Set: cdFinalidadeTedDepositoJudicial.
     *
     * @param cdFinalidadeTedDepositoJudicial the cd finalidade ted deposito judicial
     */
    public void setCdFinalidadeTedDepositoJudicial(boolean cdFinalidadeTedDepositoJudicial) {
        this.cdFinalidadeTedDepositoJudicial = cdFinalidadeTedDepositoJudicial;
    }

    /**
     * Nome: getListaFormaPagamento
     *
     * @return listaFormaPagamento
     */
    public List<ListarTipoRetiradaOcorrenciasSaidaDTO> getListaFormaPagamento() {
        return listaFormaPagamento;
    }

    /**
     * Nome: setListaFormaPagamento
     *
     * @param listaFormaPagamento
     */
    public void setListaFormaPagamento(List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento) {
        this.listaFormaPagamento = listaFormaPagamento;
    }

    /**
     * Nome: getConsultasService
     *
     * @return consultasService
     */
    public IConsultasService getConsultasService() {
        return consultasService;
    }

    /**
     * Nome: setConsultasService
     *
     * @param consultasService
     */
    public void setConsultasService(IConsultasService consultasService) {
        this.consultasService = consultasService;
    }

	public String getCdBancoPagamento() {
		return cdBancoPagamento;
	}

	public void setCdBancoPagamento(String cdBancoPagamento) {
		this.cdBancoPagamento = cdBancoPagamento;
	}

	public String getIspb() {
		return ispb;
	}

	public void setIspb(String ispb) {
		this.ispb = ispb;
	}

	public String getCdContaPagamento() {
		return cdContaPagamento;
	}

	public void setCdContaPagamento(String cdContaPagamento) {
		this.cdContaPagamento = cdContaPagamento;
	}

	public String getDsISPB() {
		return dsISPB;
	}

	public void setDsISPB(String dsISPB) {
		this.dsISPB = dsISPB;
	}

	public String getTipoContaDestinoPagamento() {
		return tipoContaDestinoPagamento;
	}

	public void setTipoContaDestinoPagamento(String tipoContaDestinoPagamento) {
		this.tipoContaDestinoPagamento = tipoContaDestinoPagamento;
	}

	public String getCdBancoDestinoPagamento() {
		return cdBancoDestinoPagamento;
	}

	public void setCdBancoDestinoPagamento(String cdBancoDestinoPagamento) {
		this.cdBancoDestinoPagamento = cdBancoDestinoPagamento;
	}

	public String getContaDigitoDestinoPagamento() {
		return contaDigitoDestinoPagamento;
	}

	public void setContaDigitoDestinoPagamento(String contaDigitoDestinoPagamento) {
		this.contaDigitoDestinoPagamento = contaDigitoDestinoPagamento;
	}

	public boolean isExibeBancoCredito() {
		return exibeBancoCredito;
	}

	public void setExibeBancoCredito(boolean exibeBancoCredito) {
		this.exibeBancoCredito = exibeBancoCredito;
	}

	public String getCdContaPagamentoDet() {
		return cdContaPagamentoDet;
	}

	public void setCdContaPagamentoDet(String cdContaPagamentoDet) {
		this.cdContaPagamentoDet = cdContaPagamentoDet;
	}

	public String getNumeroInscricaoSacadorAvalista() {
		return numeroInscricaoSacadorAvalista;
	}

	public void setNumeroInscricaoSacadorAvalista(
			String numeroInscricaoSacadorAvalista) {
		this.numeroInscricaoSacadorAvalista = numeroInscricaoSacadorAvalista;
	}

	public String getDsSacadorAvalista() {
		return dsSacadorAvalista;
	}

	public void setDsSacadorAvalista(String dsSacadorAvalista) {
		this.dsSacadorAvalista = dsSacadorAvalista;
	}

	public String getDsTipoSacadorAvalista() {
		return dsTipoSacadorAvalista;
	}

	public void setDsTipoSacadorAvalista(String dsTipoSacadorAvalista) {
		this.dsTipoSacadorAvalista = dsTipoSacadorAvalista;
	}
	
	public String getAgenciaEstornoFormatado() {
		if(NUMERO_ZERO != detalheOrdemPagamento.getCdAgenciaEstorno() && !VAZIO.equals(detalheOrdemPagamento.getCdDigitoAgenciaEstorno())){			
			return detalheOrdemPagamento.getCdAgenciaEstorno() + "-" + detalheOrdemPagamento.getCdDigitoAgenciaEstorno();			
		}else{
			return VAZIO;
		} 
	}	
	
	public String getContaEstornoFormatado() {
		if(NUMERO_ZERO_LONG != detalheOrdemPagamento.getCdContaEstorno() && !VAZIO.equals(detalheOrdemPagamento.getCdDigitoContaEstorno())){
			return detalheOrdemPagamento.getCdContaEstorno() + "-" + detalheOrdemPagamento.getCdDigitoContaEstorno();
		}else{
			return VAZIO;
		}		
	}

	public DetalharPagtoOrdemPagtoSaidaDTO getDetalheOrdemPagamento() {
		return detalheOrdemPagamento;
	}

	public void setDetalheOrdemPagamento(
			DetalharPagtoOrdemPagtoSaidaDTO detalheOrdemPagamento) {
		this.detalheOrdemPagamento = detalheOrdemPagamento;
	}
	
	

}