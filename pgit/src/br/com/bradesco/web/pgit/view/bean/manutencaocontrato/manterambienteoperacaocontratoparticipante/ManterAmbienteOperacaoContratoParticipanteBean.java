/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterambienteoperacaocontratoparticipante
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterambienteoperacaocontratoparticipante;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarContratoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbPartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbientePartcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbientePartcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterAmbienteOperacaoContratoParticipanteBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ManterAmbienteOperacaoContratoParticipanteBean {

	private static final String DESC_AMBIENTE_PRODUCAO = "PROD";

	private static final String DESC_AMBIENTE_TESTE = "TEST";

	// //////////////////////////
	// DECLARA��O DE VARI�VEIS//
	// ////////////////////////

	// Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo VOLTAR. */
	private static final String VOLTAR = "VOLTAR";

	/** Atributo CPF_CNPJ_TODOS_PARTICIPANTES. */
	private static final String CPF_CNPJ_TODOS_PARTICIPANTES = "999.999.999/9999-99";

	/** Atributo DESC_TODOS_PARTICIPANTES. */
	private static final String DESC_TODOS_PARTICIPANTES = "TODOS OS PARTICIPANTES";

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterContratoService. */
	private IManterContratoService manterContratoService;

	/** Atributo manterAmbienteOperacaoContratoServiceImpl. */
	private IManterAmbienteOperacaoContratoService manterAmbienteOperacaoContratoServiceImpl;

	/** Atributo manterVincPerfilTrocaArqPartImpl. */
	private IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl;

	/** Atributo cpfCnpjClienteMaster. */
	private String cpfCnpjClienteMaster;

	/** Atributo nomeRazaoSocialClienteMaster. */
	private String nomeRazaoSocialClienteMaster;

	/** Atributo grupoEconomico. */
	private String grupoEconomico;

	/** Atributo atividadeEconomica. */
	private String atividadeEconomica;

	/** Atributo segmento. */
	private String segmento;

	/** Atributo subSegmento. */
	private String subSegmento;

	/** Atributo empresa. */
	private String empresa;

	/** Atributo tipo. */
	private String tipo;

	/** Atributo numero. */
	private String numero;

	/** Atributo situacao. */
	private String situacao;

	/** Atributo motivo. */
	private String motivo;

	/** Atributo participacao. */
	private String participacao;

	/** Atributo cdEmpresa. */
	private Long cdEmpresa;

	/** Atributo cdClubPartcInc. */
	private Long cdClubPartcInc;

	/** Atributo cdClub. */
	private Long cdClub;

	/** Atributo cdTipo. */
	private Integer cdTipo;

	/** Atributo cnpjCpf. */
	private String cnpjCpf;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo periodoSolicitacaoInicio. */
	private Date periodoSolicitacaoInicio;

	/** Atributo periodoSolicitacaoFinal. */
	private Date periodoSolicitacaoFinal;

	/** Atributo listaGrid. */
	private List<ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO> listaGrid;

	/** Atributo listaGridParticipantes. */
	private List<ListarParticipantesSaidaDTO> listaGridParticipantes;

	/** Atributo listaGridControleParticipantes. */
	private List<SelectItem> listaGridControleParticipantes;

	/** Atributo itemSelecionadoGrid. */
	private Integer itemSelecionadoGrid;

	/** Atributo itemSelecionadoGridParticipantes. */
	private Integer itemSelecionadoGridParticipantes;

	/** Atributo listaControle. */
	private List<SelectItem> listaControle;

	private List<SelectItem> listaControleTelaConsulta;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	private Integer itemSelecionadoListaTelaConsulta;

	/** Atributo mostraLimpar. */
	private boolean mostraLimpar = false;

	/** Atributo servicoDesc. */
	private String servicoDesc;

	/** Atributo servico. */
	private Integer servico;

	/** Atributo modalidade. */
	private Integer modalidade;

	/** Atributo modalidadeDesc. */
	private String modalidadeDesc;

	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();

	/** Atributo listaTipoServicoHash. */
	private Map<Integer, ListarAmbienteOcorrenciaSaidaDTO> listaTipoServicoHash = new HashMap<Integer, ListarAmbienteOcorrenciaSaidaDTO>();

	/** Atributo listaModalidade. */
	private List<SelectItem> listaModalidade = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeHash. */
	private Map<Integer, String> listaModalidadeHash = new HashMap<Integer, String>();

	/** Atributo dataProgramada. */
	private Date dataProgramada;

	/** Atributo dataProgramadaSolicitacaoDesc. */
	private String dataProgramadaSolicitacaoDesc;

	/** Atributo habilitaRadioAmbiente. */
	private Boolean habilitaRadioAmbiente;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo tipoMudancaSolicitacao. */
	private String tipoMudancaSolicitacao;

	/** Atributo habilitaMudancaSolicitacao. */
	private boolean habilitaMudancaSolicitacao;

	/** Atributo formaEfetivacaoSolicitacao. */
	private String formaEfetivacaoSolicitacao;

	/** Atributo manterOperacoesAmbienteAtualSolicitacao. */
	private String manterOperacoesAmbienteAtualSolicitacao;

	/** Atributo dataProgramadaSolicitacao. */
	private Date dataProgramadaSolicitacao;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo hiddenObrigatoriedade. */
	private String hiddenObrigatoriedade;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo listaServico. */
	private List<ListarAmbienteOcorrenciaSaidaDTO> listaServico = new ArrayList<ListarAmbienteOcorrenciaSaidaDTO>();

	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;

	/** Atributo cpfCnpjParticipanteSelecionado. */
	private String cpfCnpjParticipanteSelecionado;

	/** Atributo nomeRazaoParticipanteSelecionado. */
	private String nomeRazaoParticipanteSelecionado;

	/** Atributo tipoParticipante. */
	private String tipoParticipante;

	/** Atributo tipoServicoValue. */
	private Integer tipoServicoValue;

	/** Atributo situacaoSolicitacao. */
	private String situacaoSolicitacao;

	private List<ConsultarContratoOcorrenciasDTO> listaGridContratos;

	private String ambiente;
	
	private String mudancaAmbienteOperacao;

	// ///////////////////////
	// M�TODOS DIVERSOS //
	// /////////////////////

	/**
	 * Valorizar tipo ambiente.
	 */
	public void valorizarTipoAmbiente() {

		if (servico != null && servico > 0) {

			ListarAmbienteOcorrenciaSaidaDTO saidaAux = new ListarAmbienteOcorrenciaSaidaDTO();

			String dsAmbiente = "";

			for (int x = 0; x < listaServico.size(); x++) {
				saidaAux = listaServico.get(x);

				if (saidaAux.getCdProdutoOperacaoRelacionado().equals(servico)) {
					dsAmbiente = saidaAux.getDsAmbienteOperacao();
					break;
				}
			}

			if (dsAmbiente.equalsIgnoreCase("TESTE")) {
				setTipoMudancaSolicitacao("0");
			} else {
				setTipoMudancaSolicitacao("1");
			}

		} else {

			setTipoMudancaSolicitacao(null);

		}

		desabilitarManterPagamentosAgendadosProducao();
	}

	public void limparPaginaTeste(ActionEvent evt) {
		setAmbiente(DESC_AMBIENTE_PRODUCAO);

		identificacaoClienteContratoBean
				.setPaginaRetorno("pesqManterSolicitacoesMudancaAmbienteOperacaoParticipanteTeste");

		limparPagina(evt);
	}

	public void limparPaginaProducao(ActionEvent evt) {
		
		setAmbiente(DESC_AMBIENTE_TESTE);

		identificacaoClienteContratoBean
				.setPaginaRetorno("pesqManterSolicitacoesMudancaAmbienteOperacaoParticipanteProducao");

		limparPagina(evt);
	}

	/**
	 * Limpar pagina.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparPagina(ActionEvent evt) {
		identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		identificacaoClienteContratoBean
				.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
				.setPaginaCliente("identificacaoClienteMantAmbienteOperacaoContratoParticipante");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);

		identificacaoClienteContratoBean.setBloqueiaRadio(false);
		identificacaoClienteContratoBean
				.setHabilitaEmpresaGestoraTipoContrato(true);

		setMostraLimpar(false);
	}

	/**
	 * Carregar combo tipo servico.
	 */
	public void carregarComboTipoServico() {

		this.listaTipoServico = new ArrayList<SelectItem>();
		setCdClub(null);

		ListarAmbienteEntradaDTO entrada = new ListarAmbienteEntradaDTO();

		ConsultarContratoOcorrenciasDTO listarContratosPgitSaidaDTO = getListaGridContratos()
				.get(getItemSelecionadoListaTelaConsulta());

		if ("0".equals(tipoParticipante)) {
			entrada.setCdPessoa(listarContratosPgitSaidaDTO
					.getCdClubRepresentante());
			setCdClub(listarContratosPgitSaidaDTO.getCdClubRepresentante());
		}
		if ("1".equals(tipoParticipante)) {
			entrada.setCdPessoa(getCdClubPartcInc());
			setCdClub(getCdClubPartcInc());
		}

		entrada.setCdPessoaJuridica(listarContratosPgitSaidaDTO
				.getCdPessoaJuridica());
		entrada.setCdTipoContrato(listarContratosPgitSaidaDTO
				.getCdTipoContrato());
		entrada.setCdProdutoOperacaoRelacionamento(0);
		entrada.setMaxOcorrencias(50);
		entrada.setNrSequenciaContrato(listarContratosPgitSaidaDTO
				.getNrSequenciaContrato());
		
		if(!DESC_AMBIENTE_PRODUCAO.equals(getAmbiente())){
			entrada.setDsAmbiente(DESC_AMBIENTE_PRODUCAO);
		} else {
			entrada.setDsAmbiente(DESC_AMBIENTE_TESTE);
		}

		listaServico = manterAmbienteOperacaoContratoServiceImpl
				.listarAmbiente(entrada);

		listaTipoServicoHash.clear();

		for (ListarAmbienteOcorrenciaSaidaDTO combo : listaServico) {
			listaTipoServicoHash.put(combo.getCdTipoServico(), combo);
			listaTipoServico.add(new SelectItem(combo.getCdTipoServico(),
					combo.getDsTipoServico()));
		}

	}

	/**
	 * Validar tipo partc selecionado.
	 */
	public String validarTipoPartcSelecionado() {
		setServico(0);
		setTipoMudancaSolicitacao(null);
		setFormaEfetivacaoSolicitacao(null);
		
		if (tipoParticipante != null && "0".equals(tipoParticipante)) {
			setNomeRazaoParticipanteSelecionado(null);
			setCpfCnpjParticipanteSelecionado(null);
			setCdClubPartcInc(null);
			
			try{
				carregarComboTipoServico();

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(),"#{manterAmbienteOperacaoContratoParticipanteBean.voltarIncluir}",
						BradescoViewExceptionActionType.ACTION, false);
			}
			
			setFormaEfetivacaoSolicitacao("0");			
		}

		if (tipoParticipante != null && "1".equals(tipoParticipante)) {
			setNomeRazaoParticipanteSelecionado(null);
			setCpfCnpjParticipanteSelecionado(null);
			setCdClubPartcInc(null);
		}
		
		return "";

	}

	/**
	 * Carrega lista.
	 * 
	 * @return the string
	 */
	public String carregaLista() {
		try {

			setMostraLimpar(true);

			ConsultarSolicitacaoMudancaAmbPartcEntradaDTO entradaDTO = new ConsultarSolicitacaoMudancaAmbPartcEntradaDTO();

			ConsultarContratoOcorrenciasDTO saidaDTO = getListaGridContratos()
					.get(getItemSelecionadoListaTelaConsulta());

			/* Dados do Contrato */

			entradaDTO.setCdpessoaJuridicaContrato(saidaDTO
					.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(saidaDTO
					.getNrSequenciaContrato());
			entradaDTO.setDsAmbiente(getAmbiente());

			/* Dados Argumentos de Pesquisa */

			entradaDTO.setDtFimSolicitacao("0000000000");
			entradaDTO.setDtInicioSolicitacao("0000000000");
			entradaDTO.setNrOcorrencias(50);

			setListaGrid(getManterAmbienteOperacaoContratoServiceImpl()
					.consultarSolicitacaoMudancaAmbPartc(entradaDTO));

			listaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < listaGrid.size(); i++) {
				listaControle.add(new SelectItem(i, ""));
			}

			setItemSelecionadoLista(null);

			if (listaGrid.size() == 0) {
				setMostraLimpar(false);
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setMostraLimpar(false);

		}

		return "CONSULTAR";
	}

	/**
	 * Carrega lista retorno.
	 * 
	 * @return the string
	 */
	public String carregaListaRetorno() {
		try {

			setMostraLimpar(true);

			ConsultarSolicitacaoMudancaAmbPartcEntradaDTO entradaDTO = new ConsultarSolicitacaoMudancaAmbPartcEntradaDTO();

			ConsultarContratoOcorrenciasDTO saidaDTO = getListaGridContratos()
					.get(getItemSelecionadoListaTelaConsulta());

			/* Dados do Contrato */

			entradaDTO.setCdpessoaJuridicaContrato(saidaDTO
					.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(saidaDTO
					.getNrSequenciaContrato());

			/* Dados Argumentos de Pesquisa */

			entradaDTO.setDtFimSolicitacao("0000000000");
			entradaDTO.setDtInicioSolicitacao("0000000000");

			setListaGrid(getManterAmbienteOperacaoContratoServiceImpl()
					.consultarSolicitacaoMudancaAmbPartc(entradaDTO));

			listaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < listaGrid.size(); i++) {
				listaControle.add(new SelectItem(i, ""));
			}

			setItemSelecionadoLista(null);

			if (listaGrid.size() == 0) {
				setMostraLimpar(false);
			}

		} catch (PdcAdapterFunctionalException p) {
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setMostraLimpar(false);

		}

		return "CONSULTAR";
	}

	/**
	 * Limpar dados.
	 * 
	 * @return the string
	 */
	public String limparDados() {
		setMostraLimpar(false);
		setPeriodoSolicitacaoFinal(new Date());
		setPeriodoSolicitacaoInicio(new Date());
		setListaControle(null);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		return "LIMPAR_DADOS";
	}

	/**
	 * Limpar.
	 * 
	 * @return the string
	 */
	public String limpar() {
		setItemSelecionadoLista(null);
		return "LIMPAR";
	}

	public void limparTela() {
		// limparDadosCliente();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setBloqueiaRadio(false);

		// limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.setHabilitaCampos(false);
		setListaGridContratos(new ArrayList<ConsultarContratoOcorrenciasDTO>());
		setItemSelecionadoListaTelaConsulta(null);
		identificacaoClienteContratoBean
				.setHabilitaEmpresaGestoraTipoContrato(true);
	}

	public String consultarContrato() {

		if (getIdentificacaoClienteContratoBean().getObrigatoriedade() != null && getIdentificacaoClienteContratoBean().getObrigatoriedade().equals("T")) {

			setItemSelecionadoLista(null);
			identificacaoClienteContratoBean
					.setHabilitaEmpresaGestoraTipoContrato(true);

			try {
				ConsultarContratoEntradaDTO entrada = new ConsultarContratoEntradaDTO();
				entrada.setDsAmbiente(getAmbiente());

				entrada.setCdAgencia(0);
				if (identificacaoClienteContratoBean
						.getAgenciaOperadoraFiltro() != null
						&& !identificacaoClienteContratoBean
								.getAgenciaOperadoraFiltro().equals("")) {
					entrada.setCdAgencia(Integer
							.parseInt(identificacaoClienteContratoBean
									.getAgenciaOperadoraFiltro()));
				}

				entrada.setCdClubPessoa(0L);
				if (identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas().getCdClub() != null) {
					entrada
							.setCdClubPessoa(identificacaoClienteContratoBean
									.getSaidaConsultarListaClientePessoas()
									.getCdClub());
				}

				entrada.setCdFuncionarioBradesco(0L);
				if (identificacaoClienteContratoBean.getCodigoFunc() != null
						&& !identificacaoClienteContratoBean.getCodigoFunc()
								.equals("")) {
					entrada.setCdFuncionarioBradesco(Long
							.valueOf(identificacaoClienteContratoBean
									.getCodigoFunc()));
				}

				entrada.setCdPessoaJuridica(0L);
				if (identificacaoClienteContratoBean.getEmpresaGestoraFiltro() != null) {
					entrada
							.setCdPessoaJuridica(identificacaoClienteContratoBean
									.getEmpresaGestoraFiltro());
				}

				entrada.setCdSituacaoContrato(0);
				if (identificacaoClienteContratoBean.getSituacaoFiltro() != null) {
					entrada
							.setCdSituacaoContrato(identificacaoClienteContratoBean
									.getSituacaoFiltro());
				}

				entrada.setCdTipoContrato(0);
				if (identificacaoClienteContratoBean.getTipoContratoFiltro() != null) {
					entrada.setCdTipoContrato(identificacaoClienteContratoBean
							.getTipoContratoFiltro());
				}

				entrada.setNrSequenciaContrato(0L);
				if (identificacaoClienteContratoBean.getNumeroFiltro() != null
						&& !identificacaoClienteContratoBean.getNumeroFiltro()
								.equals("")) {
					entrada.setNrSequenciaContrato(Long
							.valueOf(identificacaoClienteContratoBean
									.getNumeroFiltro()));
				}

				entrada.setCdCpfCnpjPssoa(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas().getCdCpfCnpj());
				entrada.setCdFilialCnpjPssoa(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCdFilialCnpj());
				entrada.setCdControleCpfPssoa(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCdControleCnpj());

				setListaGridContratos(getManterAmbienteOperacaoContratoServiceImpl()
						.consultarContratos(entrada));

				this.listaControleTelaConsulta = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridContratos().size(); i++) {
					this.listaControleTelaConsulta.add(new SelectItem(i, " "));
				}

				identificacaoClienteContratoBean
						.bloquearArgumentosPesquisaAposConsulta();

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setListaGridContratos(null);

			}
		}

		return "";
	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"solManterSolicitacoesMudancaAmbienteOperacaoParticipante",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "DETALHAR";
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {

		ConsultarSolicitacaoAmbientePartcEntradaDTO entrada = new ConsultarSolicitacaoAmbientePartcEntradaDTO(); // Detalhar

		ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO consultarSolicitacaoMudancaAmbienteSaidaDTO = getListaGrid()
				.get(getItemSelecionadoLista()); // Pega o item selecionado da
		// Lista

		entrada
				.setCdSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO
						.getCdSolicitacao());
		entrada
				.setNrSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO
						.getNrSolicitacao());

		ConsultarSolicitacaoAmbientePartcSaidaDTO saida = getManterAmbienteOperacaoContratoServiceImpl()
				.consultarSolicitacaoAmbientePartc(entrada); // Detalhar

		/* Dados de Saida */
		setServicoDesc(saida.getDsServico());
		setDataProgramadaSolicitacaoDesc(saida.getDtProgramada());
		setFormaEfetivacaoSolicitacao(saida.getCdFormaEfetivacao());
		setTipoMudancaSolicitacao(saida.getCdTipoMudanca());
		setManterOperacoesAmbienteAtualSolicitacao("NAO".equals(saida
				.getCdManterOperacao()) ? saida.getCdManterOperacao()
				+ " (A limpeza dos dados � efetivada em processamento noturno)"
				: saida.getCdManterOperacao());

		setDataHoraManutencao(saida.getDtManutencao());
		setDataHoraInclusao(saida.getDtInclusao());

		setTipoCanalInclusao(saida.getCdTipoCanalInclusao() != 0 ? saida
				.getCdTipoCanalInclusao()
				+ " - " + saida.getDsCanalInclusao() : "");
		setTipoCanalManutencao(saida.getCdTipoCanalManutencao() == 0 ? ""
				: saida.getCdTipoCanalManutencao() + " - "
						+ saida.getDsCanalManutencao());

		setUsuarioInclusao(saida.getCdUsuarioInclusao());
		setUsuarioManutencao(saida.getCdUsuarioManutencao());

		setComplementoInclusao(saida.getDsCanalInclusao() == null
				|| saida.getDsCanalInclusao().equals("0") ? "" : saida
				.getDsCanalInclusao());
		setComplementoManutencao(saida.getDsCanalManutencao() == null
				|| saida.getDsCanalManutencao().equals("0") ? "" : saida
				.getDsCanalManutencao());

		// em 11/8/2010 para atender slide 175 do doc 27, 28 e 29.07.10 -
		// Corre��es Telas da homologa��o do dia 14 e
		// 15.07.10.ppt
		setModalidadeDesc(saida.getDsModalidade());

		setCpfCnpjParticipanteSelecionado(saida.getCpfCnpjFormatado());
		setNomeRazaoParticipanteSelecionado(saida.getDsNomeRazaoParticipante());
		setSituacaoSolicitacao(saida.getCdSituacao());
	}

	/**
	 * Consultar participantes.
	 * 
	 * @return the string
	 */
	public String consultarParticipantes() {
		listaGridParticipantes = new ArrayList<ListarParticipantesSaidaDTO>();
		try {

			ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

			ConsultarContratoOcorrenciasDTO listarContratosPgitSaidaDTO = getListaGridContratos()
					.get(getItemSelecionadoListaTelaConsulta());

			/* Dados do Contrato */

			entradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO
					.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(listarContratosPgitSaidaDTO
					.getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(listarContratosPgitSaidaDTO
					.getNrSequenciaContrato());

			setListaGridParticipantes(getManterVincPerfilTrocaArqPartImpl()
					.listarParticipantes(entradaDTO));

			this.listaGridControleParticipantes = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridParticipantes().size(); i++) {
				listaGridControleParticipantes.add(new SelectItem(i, " "));
			}

			setItemSelecionadoGridParticipantes(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridParticipantes(null);
			setItemSelecionadoGridParticipantes(null);
		}

		return "SEL_PARTICIPANTE";
	}

	
	
	public void pesquisarParticipantesIncluir(ActionEvent evt) {
	    consultarParticipantes();
	}
	
	/**
	 * Selecionar participante.
	 * 
	 * @return the string
	 */
	public String selecionarParticipante() {
		ListarParticipantesSaidaDTO itemSelecionado = getListaGridParticipantes()
				.get(getItemSelecionadoGridParticipantes());
		setNomeRazaoParticipanteSelecionado(itemSelecionado.getNomeRazao());
		setCpfCnpjParticipanteSelecionado(itemSelecionado.getCpf());
		setCdClubPartcInc(itemSelecionado.getCdParticipante());
		try{
			carregarComboTipoServico();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(),"#{manterAmbienteOperacaoContratoParticipanteBean.voltarIncluir}",
                BradescoViewExceptionActionType.ACTION, false);
		}
		return "CONFIRMAR";
	}

	/**
	 * Excluir.
	 * 
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}
		return "EXCLUIR";
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		String ambienteAtivoSolicitacao = "";

		if (itemSelecionadoLista != null) {
			ambienteAtivoSolicitacao = getListaGrid().get(itemSelecionadoLista)
					.getCdAmbienteAtivoSolicitacao();
		}

		if (ambienteAtivoSolicitacao.equalsIgnoreCase("TESTE")) {
			setTipoMudancaSolicitacao("0");
			setHabilitaMudancaSolicitacao(true);
		} else {
			setTipoMudancaSolicitacao(null);
			setHabilitaMudancaSolicitacao(false);
		}
		setTipoParticipante(null);
		setServico(0);
		setFormaEfetivacaoSolicitacao("");
		setManterOperacoesAmbienteAtualSolicitacao("");
		setDataProgramadaSolicitacao(new Date());
		setModalidade(0);
		listaModalidade.clear();
		setNomeRazaoParticipanteSelecionado(null);
		setCpfCnpjParticipanteSelecionado(null);
		setCdClubPartcInc(null);
		// carregaListaTipoServico();
		listaServico = new ArrayList<ListarAmbienteOcorrenciaSaidaDTO>();
		return "INCLUIR";
	}

	/**
	 * Avancar incluir.
	 * 
	 * @return the string
	 */
	public String avancarIncluir() {
		setTipoServicoValue(null);

		SimpleDateFormat simpleDataFormat = new SimpleDateFormat("dd/MM/yyyy");
		if (getDataProgramadaSolicitacao() != null) {
			setDataProgramadaSolicitacaoDesc(simpleDataFormat
					.format(getDataProgramadaSolicitacao()));
		}
		if ("1".equalsIgnoreCase(formaEfetivacaoSolicitacao)) {
			setDataProgramadaSolicitacaoDesc(null);
		}
		setTipoServicoValue(getServico());
		setServicoDesc(getListaTipoServicoHash().get(getServico())
				.getDsTipoServico());
		setModalidadeDesc(getListaModalidadeHash().get(getModalidade()));
		if (tipoParticipante != null && "0".equals(tipoParticipante)) {
			setNomeRazaoParticipanteSelecionado(DESC_TODOS_PARTICIPANTES);
			setCpfCnpjParticipanteSelecionado(CPF_CNPJ_TODOS_PARTICIPANTES);
			setCdClub(new Long("9999999999"));
		}
		return "AVANCAR";

	}

	/**
	 * Confirmar incluir.
	 * 
	 * @return the string
	 */
	public String confirmarIncluir() {

		try {
			IncluirAlteracaoAmbientePartcEntradaDTO incluirAlteracaoAmbienteEntradaDTO = new IncluirAlteracaoAmbientePartcEntradaDTO();

			/* Dados Contratos */
			ConsultarContratoOcorrenciasDTO saidaDTO = getListaGridContratos()
					.get(getItemSelecionadoListaTelaConsulta());

			incluirAlteracaoAmbienteEntradaDTO
					.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			incluirAlteracaoAmbienteEntradaDTO
					.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			incluirAlteracaoAmbienteEntradaDTO
					.setNrSequenciaContratoNegocio(saidaDTO
							.getNrSequenciaContrato());

			// Esse campo deve ser valorizados com ZERO - Email dia sexta-feira,
			// 23 de julho de 2010 15:01
			incluirAlteracaoAmbienteEntradaDTO.setCdRelacionamentoProduto(0);

			// Esse campo deve ser valorizado com 01 � quando a op��o na tela
			// �manter Opera��o no ambiente atual:� for
			// Sim e com 02 Quando a op��o for N�O.
			incluirAlteracaoAmbienteEntradaDTO
					.setCdIndicadorArmazenamentoInformacao("0"
							.equals(getManterOperacoesAmbienteAtualSolicitacao()) ? 1
							: 2);

			/*
			 * O campo cdAmbienteAtivoSolicitacao deve ser formatado com PROD
			 * quando for secionada a op��o �TESTE PARA PRODUCAO� e com TEST
			 * quando for escolhido a op��o �PRODUCAO PARA TESTE�
			 */
			
			//palhares 
			
			incluirAlteracaoAmbienteEntradaDTO
					.setCdAmbienteAtivoSolicitacao(getAmbiente().toString());
			
			//if (getFormaEfetivacaoSolicitacao().equals("0"))
				//incluirAlteracaoAmbienteEntradaDTO
				//		.setCdAmbienteAtivoSolicitacao(DESC_AMBIENTE_TESTE);
				
				//if (getTipoMudancaSolicitacao().equals("0")) {
				//	incluirAlteracaoAmbienteEntradaDTO
				//			.setCdAmbienteAtivoSolicitacao(DESC_AMBIENTE_PRODUCAO);
				//}
			
			// o Campo cdFormaMudanca deve ser formatado com 1 quando for
			// �BATCH� , e com 2 quando for de �ON-LINE�
			incluirAlteracaoAmbienteEntradaDTO.setCdFormaMudanca(2);
			if (getFormaEfetivacaoSolicitacao().equals("0")) {
				incluirAlteracaoAmbienteEntradaDTO.setCdFormaMudanca(1);
			}

			// incluirAlteracaoAmbienteEntradaDTO.setCdAmbienteAtivoSolicitacao(listaTipoServicoHash.get(servico)

			incluirAlteracaoAmbienteEntradaDTO.setCdPessoa(cdClub);
			incluirAlteracaoAmbienteEntradaDTO
					.setCdProdutoOperacaoRelacionado(0);
			incluirAlteracaoAmbienteEntradaDTO
					.setCdProdutoServicoOperacao(getTipoServicoValue());
			incluirAlteracaoAmbienteEntradaDTO
					.setCdTipoParticipacaoPessoa(listaTipoServicoHash.get(
							servico).getCdTipoParticipacaoPessoa());
			incluirAlteracaoAmbienteEntradaDTO
					.setDtMudancaAmbiente(FormatarData
							.formataDiaMesAnoToPdc(dataProgramadaSolicitacao));

			if (getFormaEfetivacaoSolicitacao().equals("0")) {
				incluirAlteracaoAmbienteEntradaDTO
						.setDtMudancaAmbiente(FormatarData
								.formataDiaMesAnoToPdc(getDataProgramadaSolicitacao()));
			}
			if ("1".equalsIgnoreCase(formaEfetivacaoSolicitacao)) {
				incluirAlteracaoAmbienteEntradaDTO.setDtMudancaAmbiente(null);
			}

			IncluirAlteracaoAmbientePartcSaidaDTO incluirAlteracaoAmbienteSaidaDTO = getManterAmbienteOperacaoContratoServiceImpl()
					.incluirAlteracaoAmbientePartc(
							incluirAlteracaoAmbienteEntradaDTO);

			setItemSelecionadoLista(null);
			BradescoFacesUtils
					.addInfoModalMessage(
							"("
									+ incluirAlteracaoAmbienteSaidaDTO
											.getCodMensagem()
									+ ") "
									+ incluirAlteracaoAmbienteSaidaDTO
											.getMensagem(),
							"#{manterAmbienteOperacaoContratoParticipanteBean.solicitar}",
							BradescoViewExceptionActionType.ACTION, false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Confirmar excluir.
	 * 
	 * @return the string
	 */
	public String confirmarExcluir() {

		try {

			ExcluirAlteracaoAmbientePartcEntradaDTO excluirAlteracaoAmbienteEntradaDTO = new ExcluirAlteracaoAmbientePartcEntradaDTO();

			ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO consultarSolicitacaoMudancaAmbienteSaidaDTO = getListaGrid()
					.get(getItemSelecionadoLista()); // Item selecionado da
			// Lista
			ConsultarContratoOcorrenciasDTO saidaDTO = getListaGridContratos()
					.get(getItemSelecionadoListaTelaConsulta());

			excluirAlteracaoAmbienteEntradaDTO
					.setCdSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO
							.getCdSolicitacao());
			excluirAlteracaoAmbienteEntradaDTO
					.setNrSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO
							.getNrSolicitacao());
			excluirAlteracaoAmbienteEntradaDTO
					.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			excluirAlteracaoAmbienteEntradaDTO
					.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			excluirAlteracaoAmbienteEntradaDTO
					.setNrSequenciaContratoNegocio(saidaDTO
							.getNrSequenciaContrato());

			ExcluirAlteracaoAmbientePartcSaidaDTO excluirAlteracaoAmbienteSaidaDTO = getManterAmbienteOperacaoContratoServiceImpl()
					.excluirAlteracaoAmbientePartc(
							excluirAlteracaoAmbienteEntradaDTO);

			BradescoFacesUtils
					.addInfoModalMessage(
							"("
									+ excluirAlteracaoAmbienteSaidaDTO
											.getCodMensagem()
									+ ") "
									+ excluirAlteracaoAmbienteSaidaDTO
											.getMensagem(),
							"#{manterAmbienteOperacaoContratoParticipanteBean.solicitar}",
							BradescoViewExceptionActionType.ACTION, false);

			setItemSelecionadoLista(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return "";

	}

	/**
	 * Solicitar.
	 * 
	 * @return the string
	 */
	public String solicitar() {

		ConsultarContratoOcorrenciasDTO saidaDTO = getListaGridContratos().get(
				getItemSelecionadoListaTelaConsulta());

		setCpfCnpjClienteMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialClienteMaster(saidaDTO.getNmRazaoRepresentante());
		setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomica(saidaDTO.getDsAtividadeEconomica());
		setSegmento(saidaDTO.getDsSegmentoCliente());
		setSubSegmento(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacao(saidaDTO.getDsSituacaoContrato());
		setMotivo(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - "
				+ saidaDTO.getDsFuncionarioBradesco());

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
		}

		setPeriodoSolicitacaoInicio(new Date());
		setPeriodoSolicitacaoFinal(new Date());

		carregaLista();

		return "SOLICITAR";
	}

	/**
	 * Desabilitar manter pagamentos agendados producao.
	 */
	public void desabilitarManterPagamentosAgendadosProducao() {
		if ("0".equals(tipoMudancaSolicitacao)) {
			setHabilitaRadioAmbiente(true);
			setManterOperacoesAmbienteAtualSolicitacao("1");
		} else {
			setHabilitaRadioAmbiente(false);
			setManterOperacoesAmbienteAtualSolicitacao("");
		}
	}

	/**
	 * Limpar pesquisar.
	 * 
	 * @return the string
	 */
	public String limparPesquisar() {
		return "LIMPAR";
	}

	/**
	 * Voltar detalhar.
	 * 
	 * @return the string
	 */
	public String voltarDetalhar() {
		return VOLTAR;
	}

	/**
	 * Voltar excluir.
	 * 
	 * @return the string
	 */
	public String voltarExcluir() {
		return VOLTAR;
	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		return VOLTAR;
	}

	/**
	 * Voltar incluir.
	 * 
	 * @return the string
	 */
	public String voltarIncluir() {
		return VOLTAR;
	}

	/**
	 * Voltar incluir2.
	 * 
	 * @return the string
	 */
	public String voltarIncluir2() {
		return VOLTAR;
	}

	// ////////////////////
	// GETTERS E SETTERS//
	// //////////////////

	/**
	 * Get: atividadeEconomica.
	 * 
	 * @return atividadeEconomica
	 */
	public String getAtividadeEconomica() {
		return atividadeEconomica;
	}

	/**
	 * Set: atividadeEconomica.
	 * 
	 * @param atividadeEconomica
	 *            the atividade economica
	 */
	public void setAtividadeEconomica(String atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}

	/**
	 * Get: cpfCnpjClienteMaster.
	 * 
	 * @return cpfCnpjClienteMaster
	 */
	public String getCpfCnpjClienteMaster() {
		return cpfCnpjClienteMaster;
	}

	/**
	 * Set: cpfCnpjClienteMaster.
	 * 
	 * @param cpfCnpjClienteMaster
	 *            the cpf cnpj cliente master
	 */
	public void setCpfCnpjClienteMaster(String cpfCnpjClienteMaster) {
		this.cpfCnpjClienteMaster = cpfCnpjClienteMaster;
	}

	/**
	 * Get: grupoEconomico.
	 * 
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}

	/**
	 * Set: grupoEconomico.
	 * 
	 * @param grupoEconomico
	 *            the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}

	/**
	 * Get: nomeRazaoSocialClienteMaster.
	 * 
	 * @return nomeRazaoSocialClienteMaster
	 */
	public String getNomeRazaoSocialClienteMaster() {
		return nomeRazaoSocialClienteMaster;
	}

	/**
	 * Set: nomeRazaoSocialClienteMaster.
	 * 
	 * @param nomeRazaoSocialClienteMaster
	 *            the nome razao social cliente master
	 */
	public void setNomeRazaoSocialClienteMaster(
			String nomeRazaoSocialClienteMaster) {
		this.nomeRazaoSocialClienteMaster = nomeRazaoSocialClienteMaster;
	}

	/**
	 * Get: segmento.
	 * 
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}

	/**
	 * Set: segmento.
	 * 
	 * @param segmento
	 *            the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	/**
	 * Get: subSegmento.
	 * 
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}

	/**
	 * Set: subSegmento.
	 * 
	 * @param subSegmento
	 *            the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}

	/**
	 * Get: empresa.
	 * 
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 * 
	 * @param empresa
	 *            the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: motivo.
	 * 
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * Set: motivo.
	 * 
	 * @param motivo
	 *            the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * Get: numero.
	 * 
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 * 
	 * @param numero
	 *            the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: participacao.
	 * 
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 * 
	 * @param participacao
	 *            the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Get: situacao.
	 * 
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 * 
	 * @param situacao
	 *            the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: tipo.
	 * 
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 * 
	 * @param tipo
	 *            the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: periodoSolicitacaoFinal.
	 * 
	 * @return periodoSolicitacaoFinal
	 */
	public Date getPeriodoSolicitacaoFinal() {
		return periodoSolicitacaoFinal;
	}

	/**
	 * Set: periodoSolicitacaoFinal.
	 * 
	 * @param periodoSolicitacaoFinal
	 *            the periodo solicitacao final
	 */
	public void setPeriodoSolicitacaoFinal(Date periodoSolicitacaoFinal) {
		this.periodoSolicitacaoFinal = periodoSolicitacaoFinal;
	}

	/**
	 * Get: periodoSolicitacaoInicio.
	 * 
	 * @return periodoSolicitacaoInicio
	 */
	public Date getPeriodoSolicitacaoInicio() {
		return periodoSolicitacaoInicio;
	}

	/**
	 * Set: periodoSolicitacaoInicio.
	 * 
	 * @param periodoSolicitacaoInicio
	 *            the periodo solicitacao inicio
	 */
	public void setPeriodoSolicitacaoInicio(Date periodoSolicitacaoInicio) {
		this.periodoSolicitacaoInicio = periodoSolicitacaoInicio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControle.
	 * 
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 * 
	 * @param listaControle
	 *            the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Is mostra limpar.
	 * 
	 * @return true, if is mostra limpar
	 */
	public boolean isMostraLimpar() {
		return mostraLimpar;
	}

	/**
	 * Set: mostraLimpar.
	 * 
	 * @param mostraLimpar
	 *            the mostra limpar
	 */
	public void setMostraLimpar(boolean mostraLimpar) {
		this.mostraLimpar = mostraLimpar;
	}

	/**
	 * Get: dataProgramada.
	 * 
	 * @return dataProgramada
	 */
	public Date getDataProgramada() {
		return dataProgramada;
	}

	/**
	 * Set: dataProgramada.
	 * 
	 * @param dataProgramada
	 *            the data programada
	 */
	public void setDataProgramada(Date dataProgramada) {
		this.dataProgramada = dataProgramada;
	}

	/**
	 * Get: servicoDesc.
	 * 
	 * @return servicoDesc
	 */
	public String getServicoDesc() {
		return servicoDesc;
	}

	/**
	 * Set: servicoDesc.
	 * 
	 * @param servico
	 *            the servico desc
	 */
	public void setServicoDesc(String servico) {
		this.servicoDesc = servico;
	}

	/**
	 * Get: complementoInclusao.
	 * 
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 * 
	 * @param complementoInclusao
	 *            the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 * 
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 * 
	 * @param complementoManutencao
	 *            the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 * 
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 * 
	 * @param dataHoraInclusao
	 *            the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 * 
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 * 
	 * @param dataHoraManutencao
	 *            the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 * 
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 * 
	 * @param tipoCanalInclusao
	 *            the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 * 
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 * 
	 * @param tipoCanalManutencao
	 *            the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 * 
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 * 
	 * @param usuarioInclusao
	 *            the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 * 
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 * 
	 * @param usuarioManutencao
	 *            the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: dataProgramadaSolicitacao.
	 * 
	 * @return dataProgramadaSolicitacao
	 */
	public Date getDataProgramadaSolicitacao() {
		return dataProgramadaSolicitacao;
	}

	/**
	 * Set: dataProgramadaSolicitacao.
	 * 
	 * @param dataProgramadaSolicitacao
	 *            the data programada solicitacao
	 */
	public void setDataProgramadaSolicitacao(Date dataProgramadaSolicitacao) {
		this.dataProgramadaSolicitacao = dataProgramadaSolicitacao;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 * 
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 * 
	 * @param identificacaoClienteContratoBean
	 *            the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: manterAmbienteOperacaoContratoServiceImpl.
	 * 
	 * @return manterAmbienteOperacaoContratoServiceImpl
	 */
	public IManterAmbienteOperacaoContratoService getManterAmbienteOperacaoContratoServiceImpl() {
		return manterAmbienteOperacaoContratoServiceImpl;
	}

	/**
	 * Set: manterAmbienteOperacaoContratoServiceImpl.
	 * 
	 * @param manterAmbienteOperacaoContratoServiceImpl
	 *            the manter ambiente operacao contrato service impl
	 */
	public void setManterAmbienteOperacaoContratoServiceImpl(
			IManterAmbienteOperacaoContratoService manterAmbienteOperacaoContratoServiceImpl) {
		this.manterAmbienteOperacaoContratoServiceImpl = manterAmbienteOperacaoContratoServiceImpl;
	}

	/**
	 * Get: hiddenObrigatoriedade.
	 * 
	 * @return hiddenObrigatoriedade
	 */
	public String getHiddenObrigatoriedade() {
		return hiddenObrigatoriedade;
	}

	/**
	 * Set: hiddenObrigatoriedade.
	 * 
	 * @param hiddenObrigatoriedade
	 *            the hidden obrigatoriedade
	 */
	public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
		this.hiddenObrigatoriedade = hiddenObrigatoriedade;
	}

	/**
	 * Get: cdEmpresa.
	 * 
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 * 
	 * @param cdEmpresa
	 *            the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 * 
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 * 
	 * @param cdTipo
	 *            the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: cnpjCpf.
	 * 
	 * @return cnpjCpf
	 */
	public String getCnpjCpf() {
		return cnpjCpf;
	}

	/**
	 * Set: cnpjCpf.
	 * 
	 * @param cnpjCpf
	 *            the cnpj cpf
	 */
	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	/**
	 * Get: nomeRazaoSocial.
	 * 
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 * 
	 * @param nomeRazaoSocial
	 *            the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: listaTipoServico.
	 * 
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 * 
	 * @param listaTipoServico
	 *            the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: servico.
	 * 
	 * @return servico
	 */
	public Integer getServico() {
		return servico;
	}

	/**
	 * Set: servico.
	 * 
	 * @param servico
	 *            the servico
	 */
	public void setServico(Integer servico) {
		this.servico = servico;
	}

	/**
	 * Get: dataProgramadaSolicitacaoDesc.
	 * 
	 * @return dataProgramadaSolicitacaoDesc
	 */
	public String getDataProgramadaSolicitacaoDesc() {
		return dataProgramadaSolicitacaoDesc;
	}

	/**
	 * Set: dataProgramadaSolicitacaoDesc.
	 * 
	 * @param dataProgramadaSolicitacaoDesc
	 *            the data programada solicitacao desc
	 */
	public void setDataProgramadaSolicitacaoDesc(
			String dataProgramadaSolicitacaoDesc) {
		this.dataProgramadaSolicitacaoDesc = dataProgramadaSolicitacaoDesc;
	}

	/**
	 * Get: descricaoContrato.
	 * 
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 * 
	 * @param descricaoContrato
	 *            the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: dsAgenciaGestora.
	 * 
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 * 
	 * @param dsAgenciaGestora
	 *            the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: formaEfetivacaoSolicitacao.
	 * 
	 * @return formaEfetivacaoSolicitacao
	 */
	public String getFormaEfetivacaoSolicitacao() {
		return formaEfetivacaoSolicitacao;
	}

	/**
	 * Set: formaEfetivacaoSolicitacao.
	 * 
	 * @param formaEfetivacaoSolicitacao
	 *            the forma efetivacao solicitacao
	 */
	public void setFormaEfetivacaoSolicitacao(String formaEfetivacaoSolicitacao) {
		this.formaEfetivacaoSolicitacao = formaEfetivacaoSolicitacao;
	}

	/**
	 * Get: manterOperacoesAmbienteAtualSolicitacao.
	 * 
	 * @return manterOperacoesAmbienteAtualSolicitacao
	 */
	public String getManterOperacoesAmbienteAtualSolicitacao() {
		return manterOperacoesAmbienteAtualSolicitacao;
	}

	/**
	 * Set: manterOperacoesAmbienteAtualSolicitacao.
	 * 
	 * @param manterOperacoesAmbienteAtualSolicitacao
	 *            the manter operacoes ambiente atual solicitacao
	 */
	public void setManterOperacoesAmbienteAtualSolicitacao(
			String manterOperacoesAmbienteAtualSolicitacao) {
		this.manterOperacoesAmbienteAtualSolicitacao = manterOperacoesAmbienteAtualSolicitacao;
	}

	/**
	 * Get: tipoMudancaSolicitacao.
	 * 
	 * @return tipoMudancaSolicitacao
	 */
	public String getTipoMudancaSolicitacao() {
		return tipoMudancaSolicitacao;
	}

	/**
	 * Set: tipoMudancaSolicitacao.
	 * 
	 * @param tipoMudancaSolicitacao
	 *            the tipo mudanca solicitacao
	 */
	public void setTipoMudancaSolicitacao(String tipoMudancaSolicitacao) {
		this.tipoMudancaSolicitacao = tipoMudancaSolicitacao;
	}

	/**
	 * Get: listaModalidade.
	 * 
	 * @return listaModalidade
	 */
	public List<SelectItem> getListaModalidade() {
		return listaModalidade;
	}

	/**
	 * Set: listaModalidade.
	 * 
	 * @param listaModalidade
	 *            the lista modalidade
	 */
	public void setListaModalidade(List<SelectItem> listaModalidade) {
		this.listaModalidade = listaModalidade;
	}

	/**
	 * Get: modalidade.
	 * 
	 * @return modalidade
	 */
	public Integer getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 * 
	 * @param modalidade
	 *            the modalidade
	 */
	public void setModalidade(Integer modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Get: modalidadeDesc.
	 * 
	 * @return modalidadeDesc
	 */
	public String getModalidadeDesc() {
		return modalidadeDesc;
	}

	/**
	 * Set: modalidadeDesc.
	 * 
	 * @param modalidadeDesc
	 *            the modalidade desc
	 */
	public void setModalidadeDesc(String modalidadeDesc) {
		this.modalidadeDesc = modalidadeDesc;
	}

	/**
	 * Get: listaModalidadeHash.
	 * 
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 * 
	 * @param listaModalidadeHash
	 *            the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: habilitaRadioAmbiente.
	 * 
	 * @return habilitaRadioAmbiente
	 */
	public Boolean getHabilitaRadioAmbiente() {
		return habilitaRadioAmbiente;
	}

	/**
	 * Set: habilitaRadioAmbiente.
	 * 
	 * @param habilitaRadioAmbiente
	 *            the habilita radio ambiente
	 */
	public void setHabilitaRadioAmbiente(Boolean habilitaRadioAmbiente) {
		this.habilitaRadioAmbiente = habilitaRadioAmbiente;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 * 
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 * 
	 * @param dsGerenteResponsavel
	 *            the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: voltar.
	 * 
	 * @return voltar
	 */
	public static String getVoltar() {
		return VOLTAR;
	}

	/**
	 * Is habilita mudanca solicitacao.
	 * 
	 * @return true, if is habilita mudanca solicitacao
	 */
	public boolean isHabilitaMudancaSolicitacao() {
		return habilitaMudancaSolicitacao;
	}

	/**
	 * Set: habilitaMudancaSolicitacao.
	 * 
	 * @param habilitaMudancaSolicitacao
	 *            the habilita mudanca solicitacao
	 */
	public void setHabilitaMudancaSolicitacao(boolean habilitaMudancaSolicitacao) {
		this.habilitaMudancaSolicitacao = habilitaMudancaSolicitacao;
	}

	/**
	 * Get: manterContratoService.
	 * 
	 * @return manterContratoService
	 */
	public IManterContratoService getManterContratoService() {
		return manterContratoService;
	}

	/**
	 * Set: manterContratoService.
	 * 
	 * @param manterContratoService
	 *            the manter contrato service
	 */
	public void setManterContratoService(
			IManterContratoService manterContratoService) {
		this.manterContratoService = manterContratoService;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 * 
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 * 
	 * @param cpfCnpjParticipante
	 *            the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 * 
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 * 
	 * @param nomeRazaoParticipante
	 *            the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Set: listaGrid.
	 * 
	 * @param listaGrid
	 *            the lista grid
	 */
	public void setListaGrid(
			List<ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaGrid.
	 * 
	 * @return listaGrid
	 */
	public List<ConsultarSolicitacaoMudancaAmbPartcOcorrenciasSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaServico.
	 * 
	 * @param listaServico
	 *            the lista servico
	 */
	public void setListaServico(
			List<ListarAmbienteOcorrenciaSaidaDTO> listaServico) {
		this.listaServico = listaServico;
	}

	/**
	 * Get: listaServico.
	 * 
	 * @return listaServico
	 */
	public List<ListarAmbienteOcorrenciaSaidaDTO> getListaServico() {
		return listaServico;
	}

	/**
	 * Get: manterVincPerfilTrocaArqPartImpl.
	 * 
	 * @return manterVincPerfilTrocaArqPartImpl
	 */
	public IManterVincPerfilTrocaArqPartService getManterVincPerfilTrocaArqPartImpl() {
		return manterVincPerfilTrocaArqPartImpl;
	}

	/**
	 * Set: manterVincPerfilTrocaArqPartImpl.
	 * 
	 * @param manterVincPerfilTrocaArqPartImpl
	 *            the manter vinc perfil troca arq part impl
	 */
	public void setManterVincPerfilTrocaArqPartImpl(
			IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl) {
		this.manterVincPerfilTrocaArqPartImpl = manterVincPerfilTrocaArqPartImpl;
	}

	/**
	 * Get: listaGridParticipantes.
	 * 
	 * @return listaGridParticipantes
	 */
	public List<ListarParticipantesSaidaDTO> getListaGridParticipantes() {
		return listaGridParticipantes;
	}

	/**
	 * Set: listaGridParticipantes.
	 * 
	 * @param listaGridParticipantes
	 *            the lista grid participantes
	 */
	public void setListaGridParticipantes(
			List<ListarParticipantesSaidaDTO> listaGridParticipantes) {
		this.listaGridParticipantes = listaGridParticipantes;
	}

	/**
	 * Get: itemSelecionadoGrid.
	 * 
	 * @return itemSelecionadoGrid
	 */
	public Integer getItemSelecionadoGrid() {
		return itemSelecionadoGrid;
	}

	/**
	 * Set: itemSelecionadoGrid.
	 * 
	 * @param itemSelecionadoGrid
	 *            the item selecionado grid
	 */
	public void setItemSelecionadoGrid(Integer itemSelecionadoGrid) {
		this.itemSelecionadoGrid = itemSelecionadoGrid;
	}

	/**
	 * Get: listaGridControleParticipantes.
	 * 
	 * @return listaGridControleParticipantes
	 */
	public List<SelectItem> getListaGridControleParticipantes() {
		return listaGridControleParticipantes;
	}

	/**
	 * Set: listaGridControleParticipantes.
	 * 
	 * @param listaGridControleParticipantes
	 *            the lista grid controle participantes
	 */
	public void setListaGridControleParticipantes(
			List<SelectItem> listaGridControleParticipantes) {
		this.listaGridControleParticipantes = listaGridControleParticipantes;
	}

	/**
	 * Get: itemSelecionadoGridParticipantes.
	 * 
	 * @return itemSelecionadoGridParticipantes
	 */
	public Integer getItemSelecionadoGridParticipantes() {
		return itemSelecionadoGridParticipantes;
	}

	/**
	 * Set: itemSelecionadoGridParticipantes.
	 * 
	 * @param itemSelecionadoGridParticipantes
	 *            the item selecionado grid participantes
	 */
	public void setItemSelecionadoGridParticipantes(
			Integer itemSelecionadoGridParticipantes) {
		this.itemSelecionadoGridParticipantes = itemSelecionadoGridParticipantes;
	}

	/**
	 * Get: cdClubPartcInc.
	 * 
	 * @return cdClubPartcInc
	 */
	public Long getCdClubPartcInc() {
		return cdClubPartcInc;
	}

	/**
	 * Set: cdClubPartcInc.
	 * 
	 * @param cdClubPartcInc
	 *            the cd club partc inc
	 */
	public void setCdClubPartcInc(Long cdClubPartcInc) {
		this.cdClubPartcInc = cdClubPartcInc;
	}

	/**
	 * Get: cpfCnpjParticipanteSelecionado.
	 * 
	 * @return cpfCnpjParticipanteSelecionado
	 */
	public String getCpfCnpjParticipanteSelecionado() {
		return cpfCnpjParticipanteSelecionado;
	}

	/**
	 * Set: cpfCnpjParticipanteSelecionado.
	 * 
	 * @param cpfCnpjParticipanteSelecionado
	 *            the cpf cnpj participante selecionado
	 */
	public void setCpfCnpjParticipanteSelecionado(
			String cpfCnpjParticipanteSelecionado) {
		this.cpfCnpjParticipanteSelecionado = cpfCnpjParticipanteSelecionado;
	}

	/**
	 * Get: nomeRazaoParticipanteSelecionado.
	 * 
	 * @return nomeRazaoParticipanteSelecionado
	 */
	public String getNomeRazaoParticipanteSelecionado() {
		return nomeRazaoParticipanteSelecionado;
	}

	/**
	 * Set: nomeRazaoParticipanteSelecionado.
	 * 
	 * @param nomeRazaoParticipanteSelecionado
	 *            the nome razao participante selecionado
	 */
	public void setNomeRazaoParticipanteSelecionado(
			String nomeRazaoParticipanteSelecionado) {
		this.nomeRazaoParticipanteSelecionado = nomeRazaoParticipanteSelecionado;
	}

	/**
	 * Get: cpfCnpjTodosParticipantes.
	 * 
	 * @return cpfCnpjTodosParticipantes
	 */
	public static String getCpfCnpjTodosParticipantes() {
		return CPF_CNPJ_TODOS_PARTICIPANTES;
	}

	/**
	 * Get: descTodosParticipantes.
	 * 
	 * @return descTodosParticipantes
	 */
	public static String getDescTodosParticipantes() {
		return DESC_TODOS_PARTICIPANTES;
	}

	/**
	 * Set: tipoParticipante.
	 * 
	 * @param tipoParticipante
	 *            the tipo participante
	 */
	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}

	/**
	 * Get: tipoParticipante.
	 * 
	 * @return tipoParticipante
	 */
	public String getTipoParticipante() {
		return tipoParticipante;
	}

	/**
	 * Get: listaTipoServicoHash.
	 * 
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, ListarAmbienteOcorrenciaSaidaDTO> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 * 
	 * @param listaTipoServicoHash
	 *            the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(
			Map<Integer, ListarAmbienteOcorrenciaSaidaDTO> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: cdClub.
	 * 
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 * 
	 * @param cdClub
	 *            the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	/**
	 * Is disabled combo tipo servico.
	 * 
	 * @return true, if is disabled combo tipo servico
	 */
	public boolean isDisabledComboTipoServico() {
		if (tipoParticipante != null && "0".equals(tipoParticipante)) {
			return false;
		} else if (tipoParticipante != null
				&& "1".equals(tipoParticipante)
				&& (cpfCnpjParticipanteSelecionado != null && nomeRazaoParticipanteSelecionado != null)) {
			return false;
		}
		return true;
	}

	/**
	 * Is desabilita forma efetivacao.
	 * 
	 * @return true, if is desabilita forma efetivacao
	 */
	public boolean isDesabilitaFormaEfetivacao() {
		if (tipoParticipante != null && "0".equals(tipoParticipante)) {
			return true;
		}
		return false;
	}

	/**
	 * Get: tipoServicoValue.
	 * 
	 * @return tipoServicoValue
	 */
	public Integer getTipoServicoValue() {
		return tipoServicoValue;
	}

	/**
	 * Set: tipoServicoValue.
	 * 
	 * @param tipoServicoValue
	 *            the tipo servico value
	 */
	public void setTipoServicoValue(Integer tipoServicoValue) {
		this.tipoServicoValue = tipoServicoValue;
	}

	/**
	 * Get: situacaoSolicitacao.
	 * 
	 * @return situacaoSolicitacao
	 */
	public String getSituacaoSolicitacao() {
		return situacaoSolicitacao;
	}

	/**
	 * Set: situacaoSolicitacao.
	 * 
	 * @param situacaoSolicitacao
	 *            the situacao solicitacao
	 */
	public void setSituacaoSolicitacao(String situacaoSolicitacao) {
		this.situacaoSolicitacao = situacaoSolicitacao;
	}

	public List<ConsultarContratoOcorrenciasDTO> getListaGridContratos() {
		return listaGridContratos;
	}

	public void setListaGridContratos(
			List<ConsultarContratoOcorrenciasDTO> listaGridContratos) {
		this.listaGridContratos = listaGridContratos;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public List<SelectItem> getListaControleTelaConsulta() {
		return listaControleTelaConsulta;
	}

	public void setListaControleTelaConsulta(
			List<SelectItem> listaControleTelaConsulta) {
		this.listaControleTelaConsulta = listaControleTelaConsulta;
	}

	public Integer getItemSelecionadoListaTelaConsulta() {
		return itemSelecionadoListaTelaConsulta;
	}

	public void setItemSelecionadoListaTelaConsulta(
			Integer itemSelecionadoListaTelaConsulta) {
		this.itemSelecionadoListaTelaConsulta = itemSelecionadoListaTelaConsulta;
	}

	/**
	 * @return the mudancaAmbienteOperacao
	 */
	public String getMudancaAmbienteOperacao() {
		return mudancaAmbienteOperacao;
	}

	/**
	 * @param mudancaAmbienteOperacao the mudancaAmbienteOperacao to set
	 */
	public void setMudancaAmbienteOperacao(String mudancaAmbienteOperacao) {
		this.mudancaAmbienteOperacao = mudancaAmbienteOperacao;
	}

}
