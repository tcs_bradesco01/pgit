package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterservicocomposto;

public enum ServicoComposto {
	PAGTO_FOR_DOC(9L), PAGTO_FOR_TED(10L), PAGTO_FOR_OP(11L), PAGTO_FOR_CC(8L), PAGTO_FOR_TB(
			12L), PAGTO_FOR_TOB(13L), PAGTO_SAL_DOC(19L), PAGTO_SAL_TED(20L), PAGTO_SAL_OP(
			21L), PAGTO_SAL_CC(18L), PAGTO_TRIB_DARF(22L), PAGTO_TRIB_GARE(23L), PAGTO_TRIB_GPS(
			24L), PAGTO_TRIB_CODB(25L);

	private final Long codigoServicoComposto;

	private ServicoComposto(Long codigoServicoComposto) {
		this.codigoServicoComposto = codigoServicoComposto;
	}

	public Long getCodigoServicoComposto() {
		return codigoServicoComposto;
	}

}
