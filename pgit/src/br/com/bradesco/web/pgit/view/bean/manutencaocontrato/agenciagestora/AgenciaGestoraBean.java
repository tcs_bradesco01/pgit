/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.agenciagestora
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 29/07/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.agenciagestora;

import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarAgenciaGestoraEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarAgenciaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: AgenciaGestoraBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class AgenciaGestoraBean {
	
	/** The identificacao cliente contrato bean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = null;
	
	/** Atributo manterCadContaDestinoService. */
	private IManterCadContaDestinoService manterCadContaDestinoService = null;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl = null;
	
	/** The banco agencia dto. */
	private ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO = new ConsultarBancoAgenciaSaidaDTO();
	
	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster = null;
	
	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster = null;
	
	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster = null;
	
	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster = null;
	
	/** Atributo segmentoMaster. */
	private String segmentoMaster = null;
	
	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster = null;
	
	/** Atributo empresa. */
	private String empresa = null;
	
	/** Atributo cdEmpresa. */
	private Long cdEmpresa = null;
	
	/** Atributo tipo. */
	private String tipo = null;
	
	/** Atributo cdTipo. */
	private Integer cdTipo = null;
	
	/** Atributo numero. */
	private String numero = null;
	
	/** Atributo numeroContrato. */
	private Long numeroContrato = null;
	
	/** Atributo situacaoDesc. */
	private String situacaoDesc = null;
	
	/** Atributo motivoDesc. */
	private String motivoDesc = null;
	
	/** Atributo participacao. */
	private String participacao = null;
	
	/** Atributo possuiAditivos. */
	private String possuiAditivos = null;
	
	/** Atributo dataHoraCadastramento. */
	private String dataHoraCadastramento = null;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato = null;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora = null;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj = null;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial = null;
	
	/** Atributo codigoAgenciaGestora. */
	private Integer codigoAgenciaGestora = null;
	
	/** Atributo codDescAgenciaGestora. */
	private String codDescAgenciaGestora = null;

	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado = null;

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(final ActionEvent evt) {
		identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conAgenciaGestora");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		identificacaoClienteContratoBean.setBloqueiaRadio(false);
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Alterar agencia gestora.
	 *
	 * @return the string
	 */
	public String alterarAgenciaGestora() {
		setCodigoAgenciaGestora(null);
		preencherInformacoesTela();
		setCodDescAgenciaGestora("");
		bancoAgenciaDTO.setDsAgencia("");
		return "ALT_EMPRESA_GESTORA";
	}

	/**
	 * Preencher informacoes tela.
	 */
	private void preencherInformacoesTela() {
		ListarContratosPgitSaidaDTO contratoSelecionado = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		setCpfCnpjMaster(contratoSelecionado.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(contratoSelecionado.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(contratoSelecionado.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(contratoSelecionado.getDsAtividadeEconomica());
		setSegmentoMaster(contratoSelecionado.getDsSegmentoCliente());
		setSubSegmentoMaster(contratoSelecionado.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(contratoSelecionado.getDsPessoaJuridica()));
		setCdEmpresa(contratoSelecionado.getCdPessoaJuridica());
		setTipo(contratoSelecionado.getDsTipoContrato());
		setCdTipo(contratoSelecionado.getCdTipoContrato());
		setNumero(String.valueOf(contratoSelecionado.getNrSequenciaContrato()));
		setNumeroContrato(contratoSelecionado.getNrSequenciaContrato());
		setSituacaoDesc(contratoSelecionado.getDsSituacaoContrato());
		setMotivoDesc(contratoSelecionado.getDsMotivoSituacao());
		setParticipacao(contratoSelecionado.getCdTipoParticipacao());
		setPossuiAditivos(contratoSelecionado.getCdAditivo());
		setGerenteResponsavelFormatado(String.format("%s - %s",
				contratoSelecionado.getCdFuncionarioBradesco(), contratoSelecionado.getNmFuncionarioBradesco()));
		setDescricaoContrato(contratoSelecionado.getDsContrato());
		setDsAgenciaGestora(contratoSelecionado.getDsAgenciaOperadora());
		setDataHoraCadastramento(null);

		if (getDataHoraCadastramento() != null) {
			String dataHoraCadastramento = getDataHoraCadastramento().substring(0, 19);
			setDataHoraCadastramento(FormatarData.formatarDataTrilha(dataHoraCadastramento));
		}

		tratarCpfCnpjNomeCliente();
	}

	/**
	 * Tratar cpf cnpj nome cliente.
	 */
	private void tratarCpfCnpjNomeCliente() {
		ConsultarListaClientePessoasSaidaDTO cliente = identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas();
		String cnpjCpfCliente = cliente.getCnpjOuCpfFormatado();
		if (StringUtils.isNotEmpty(cnpjCpfCliente)) {
			setCpfCnpj(cnpjCpfCliente);
			setNomeRazaoSocial(cliente.getDsNomeRazao());
		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		if (StringUtils.isEmpty(cnpjCpfCliente)) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(cnpjCpfCliente == null ? "" : cnpjCpfCliente);
			setNomeRazaoSocial(cliente.getDsNomeRazao() == null ? "" : cliente.getDsNomeRazao());
		}
	}

	/**
	 * Avancar confirmar agencia gestora.
	 *
	 * @return the string
	 */
	public String avancarConfirmarAgenciaGestora() {
		setBancoAgenciaDTO(new ConsultarBancoAgenciaSaidaDTO());

		if (codigoAgenciaGestora != null && codigoAgenciaGestora != 0) {
			try {
				ConsultarBancoAgenciaEntradaDTO entrada = new ConsultarBancoAgenciaEntradaDTO();
				entrada.setCdBanco(237);
				entrada.setCdAgencia(getCodigoAgenciaGestora());
				entrada.setCdDigitoAgencia(00);
				entrada.setCdPessoaJuridicaContrato(getCdEmpresa());

				setBancoAgenciaDTO(getManterCadContaDestinoService().consultarDescricaoBancoAgencia(entrada));

				setCodDescAgenciaGestora(String.format("%s - %s", PgitUtil
						.verificaIntegerNulo(getCodigoAgenciaGestora()),
						PgitUtil.verificaStringNula(getBancoAgenciaDTO().getDsAgencia())));

			} catch (PdcAdapterFunctionalException p) {
				bancoAgenciaDTO.setDsAgencia("");
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), "VOLTAR", BradescoViewExceptionActionType.ACTION, false);
			}
		}

		preencherInformacoesTela();

		return "CONFIRMAR";
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		bancoAgenciaDTO.setDsAgencia("");
		setCodigoAgenciaGestora(null);
		return "conManterContrato";
	}

	/**
	 * Voltar agencia gestora.
	 *
	 * @return the string
	 */
	public String voltarAgenciaGestora() {
		return "VOLTAR";
	}

	/**
	 * Confirmar alterar agencia gestora.
	 *
	 * @return the string
	 */
	public String confirmarAlterarAgenciaGestora() {
		try {
			AlterarAgenciaGestoraEntradaDTO entrada = new AlterarAgenciaGestoraEntradaDTO();
			entrada.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(getCdEmpresa()));
			entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(getCdTipo()));
			entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(getNumeroContrato()));
			entrada.setNuAgencia(PgitUtil.verificaIntegerNulo(getCodigoAgenciaGestora()));

			AlterarAgenciaGestoraSaidaDTO saida = getManterContratoImpl().alterarAgenciaGestora(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), "conAgenciaGestora", BradescoViewExceptionActionType.ACTION, false);
			identificacaoClienteContratoBean.pesquisar(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Gets the identificacao cliente contrato bean.
	 *
	 * @return the identificacao cliente contrato bean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Sets the identificacao cliente contrato bean.
	 *
	 * @param identificacaoClienteContratoBean the new identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Gets the banco agencia dto.
	 *
	 * @return the banco agencia dto
	 */
	public ConsultarBancoAgenciaSaidaDTO getBancoAgenciaDTO() {
		return bancoAgenciaDTO;
	}

	/**
	 * Sets the banco agencia dto.
	 *
	 * @param bancoAgenciaDTO the new banco agencia dto
	 */
	public void setBancoAgenciaDTO(ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO) {
		this.bancoAgenciaDTO = bancoAgenciaDTO;
	}

	/**
	 * Gets the cpf cnpj master.
	 *
	 * @return the cpf cnpj master
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Sets the cpf cnpj master.
	 *
	 * @param cpfCnpjMaster the new cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Gets the nome razao social master.
	 *
	 * @return the nome razao social master
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Sets the nome razao social master.
	 *
	 * @param nomeRazaoSocialMaster the new nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Gets the grupo economico master.
	 *
	 * @return the grupo economico master
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Sets the grupo economico master.
	 *
	 * @param grupoEconomicoMaster the new grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Gets the atividade economica master.
	 *
	 * @return the atividade economica master
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}

	/**
	 * Sets the atividade economica master.
	 *
	 * @param atividadeEconomicaMaster the new atividade economica master
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}

	/**
	 * Gets the segmento master.
	 *
	 * @return the segmento master
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Sets the segmento master.
	 *
	 * @param segmentoMaster the new segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Gets the sub segmento master.
	 *
	 * @return the sub segmento master
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Sets the sub segmento master.
	 *
	 * @param subSegmentoMaster the new sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Gets the empresa.
	 *
	 * @return the empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Sets the empresa.
	 *
	 * @param empresa the new empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Gets the cd empresa.
	 *
	 * @return the cd empresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Sets the cd empresa.
	 *
	 * @param cdEmpresa the new cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Gets the tipo.
	 *
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Sets the tipo.
	 *
	 * @param tipo the new tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Gets the cd tipo.
	 *
	 * @return the cd tipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Sets the cd tipo.
	 *
	 * @param cdTipo the new cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Sets the numero.
	 *
	 * @param numero the new numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Gets the numero contrato.
	 *
	 * @return the numero contrato
	 */
	public Long getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Sets the numero contrato.
	 *
	 * @param numeroContrato the new numero contrato
	 */
	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Gets the motivo desc.
	 *
	 * @return the motivo desc
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Sets the motivo desc.
	 *
	 * @param motivoDesc the new motivo desc
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Gets the situacao desc.
	 *
	 * @return the situacao desc
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Sets the situacao desc.
	 *
	 * @param situacaoDesc the new situacao desc
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	/**
	 * Gets the participacao.
	 *
	 * @return the participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Sets the participacao.
	 *
	 * @param participacao the new participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Gets the possui aditivos.
	 *
	 * @return the possui aditivos
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}

	/**
	 * Sets the possui aditivos.
	 *
	 * @param possuiAditivos the new possui aditivos
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}

	/**
	 * Gets the data hora cadastramento.
	 *
	 * @return the data hora cadastramento
	 */
	public String getDataHoraCadastramento() {
		return dataHoraCadastramento;
	}

	/**
	 * Sets the data hora cadastramento.
	 *
	 * @param dataHoraCadastramento the new data hora cadastramento
	 */
	public void setDataHoraCadastramento(String dataHoraCadastramento) {
		this.dataHoraCadastramento = dataHoraCadastramento;
	}

	/**
	 * Gets the descricao contrato.
	 *
	 * @return the descricao contrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Sets the descricao contrato.
	 *
	 * @param descricaoContrato the new descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Gets the ds agencia gestora.
	 *
	 * @return the ds agencia gestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Sets the ds agencia gestora.
	 *
	 * @param dsAgenciaGestora the new ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Gets the cpf cnpj.
	 *
	 * @return the cpf cnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Sets the cpf cnpj.
	 *
	 * @param cpfCnpj the new cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Gets the nome razao social.
	 *
	 * @return the nome razao social
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Sets the nome razao social.
	 *
	 * @param nomeRazaoSocial the new nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Gets the codigo agencia gestora.
	 *
	 * @return the codigo agencia gestora
	 */
	public Integer getCodigoAgenciaGestora() {
		return codigoAgenciaGestora;
	}

	/**
	 * Sets the codigo agencia gestora.
	 *
	 * @param codigoAgenciaGestora the new codigo agencia gestora
	 */
	public void setCodigoAgenciaGestora(Integer codigoAgenciaGestora) {
		this.codigoAgenciaGestora = codigoAgenciaGestora;
	}

	/**
	 * Gets the cod desc agencia gestora.
	 *
	 * @return the cod desc agencia gestora
	 */
	public String getCodDescAgenciaGestora() {
		return codDescAgenciaGestora;
	}

	/**
	 * Sets the cod desc agencia gestora.
	 *
	 * @param codDescAgenciaGestora the new cod desc agencia gestora
	 */
	public void setCodDescAgenciaGestora(String codDescAgenciaGestora) {
		this.codDescAgenciaGestora = codDescAgenciaGestora;
	}

	/**
	 * Gets the gerente responsavel formatado.
	 *
	 * @return the gerente responsavel formatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Sets the gerente responsavel formatado.
	 *
	 * @param gerenteResponsavelFormatado the new gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Gets the manter cad conta destino service.
	 *
	 * @return the manter cad conta destino service
	 */
	public IManterCadContaDestinoService getManterCadContaDestinoService() {
		return manterCadContaDestinoService;
	}

	/**
	 * Sets the manter cad conta destino service.
	 *
	 * @param manterCadContaDestinoService the new manter cad conta destino service
	 */
	public void setManterCadContaDestinoService(
			IManterCadContaDestinoService manterCadContaDestinoService) {
		this.manterCadContaDestinoService = manterCadContaDestinoService;
	}

	/**
	 * Gets the manter contrato impl.
	 *
	 * @return the manter contrato impl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Sets the manter contrato impl.
	 *
	 * @param manterContratoImpl the new manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

}