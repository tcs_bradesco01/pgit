/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assoclayoutarqprodserv
 * 
 * Compilador:
 * 
 * Propósito:
 * 
 * Data da criação: 17/09/2014
 * 
 * Parâmetros de Compilação:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assoclayoutarqprodserv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServService;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.AlterarArquivoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.AlterarArquivoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.DetalharAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.DetalharAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ExcluirAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ExcluirAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.IncluirAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.IncluirAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.FinalidadeLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;

/**
 * Nome: AssociacaoLayoutArqProdServBean
 * <p>
 * Propósito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssociacaoLayoutArqProdServBean {
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;
	
	/** Atributo listaGrid. */
	private List<ListarAssLayoutArqProdServicoSaidaDTO> listaGrid = new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>();
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoLista. */
	private ListarAssLayoutArqProdServicoSaidaDTO itemSelecionadoLista;
	
	/** Atributo filtroServico. */
	private Integer filtroServico;
	
	/** Atributo filtroServicoIncluir. */
	private Integer filtroServicoIncluir;
	
	/** Atributo filtroTipoLayoutArquivo. */
	private Integer filtroTipoLayoutArquivo;
	
	/** Atributo filtroFinalidadeLayoutArquivo. */
	private Integer filtroFinalidadeLayoutArquivo;
	
	/** Atributo filtroTipoLayoutArquivoIncluir. */
	private Integer filtroTipoLayoutArquivoIncluir;	
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();
	
	/** Atributo listaFinalidadeLayoutArquivo. */
	private List<SelectItem> listaFinalidadeLayoutArquivo = new ArrayList<SelectItem>();
	
	/** Atributo listaFinalidadeLayout. */
	private List<FinalidadeLayoutArquivoSaidaDTO> listaFinalidadeLayout = new ArrayList<FinalidadeLayoutArquivoSaidaDTO>();
	
	/** Atributo tipoServico. */
	private String tipoServico;
	
	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo tipoLayoutArquivo. */
	private String tipoLayoutArquivo;
	
	/** Atributo tipoLayoutArquivoDesc. */
	private String tipoLayoutArquivoDesc;
	
	/** Atributo finalidadeLayoutArquivoDesc. */
	private String finalidadeLayoutArquivoDesc;
	
	/** Atributo cdIndicadorLayoutDefault. */
	private Integer cdIndicadorLayoutDefault;
	
	/** Atributo assocLayoutArqProdServImpl. */
	private IAssocLayoutArqProdServService assocLayoutArqProdServImpl;	
	
	/** Atributo dataHora. */
	private String dataHora;
	
	/** Atributo usuario. */
	private String usuario;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo cdFinalidade. */
	private String cdFinalidade;
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;
	
	/** Atributo rdoLayoutDefault. */
	private Integer rdoLayoutDefault = null;
	
	/** Atributo saidaAlterarArquivoServico. */
	private AlterarArquivoServicoSaidaDTO saidaAlterarArquivoServico = null;	

		
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		setFiltroServico(null);
		setFiltroTipoLayoutArquivo(null);
		setFiltroFinalidadeLayoutArquivo(null);
		setListaGrid(null); 
		this.itemSelecionadoLista = null;
		setBtoAcionado(false);
		return "PESQUISA";
	}
	 
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		
		setListaGrid(null);
		setFiltroServico(null);
		setFiltroTipoLayoutArquivo(null);
		this.itemSelecionadoLista = null;
		listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		listaTipoServico = new ArrayList<SelectItem>();
		preencheListaTipoLayoutArquivo();
		preencheListaTipoServico();
		setRdoLayoutDefault(1);
		setBtoAcionado(false);
	}
	
	/**
	 * Submit.
	 *
	 * @return the string
	 */
	public String submit(){
		carregaLista();
		return "PESQUISA";
	}
	
	/**
	 * Carrega lista.
	 */
	private void carregaLista(){
		listaGrid = new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>();
		ListarAssLayoutArqProdServicoEntradaDTO entradaDTO = new ListarAssLayoutArqProdServicoEntradaDTO();
			
		entradaDTO.setTipoServico(getFiltroServico());
		entradaDTO.setTipoLayoutArquivo(getFiltroTipoLayoutArquivo());
		entradaDTO.setCdIdentificadorLayoutNegocio(getFiltroFinalidadeLayoutArquivo());

		try{
			setListaGrid(getAssocLayoutArqProdServImpl().listarAssocicaoLayoutArqProdutoServico(entradaDTO));
			setBtoAcionado(true);
			this.itemSelecionadoLista = null;
		} catch (PdcAdapterException p) {
			this.setListaGrid(new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>());
			throw p;
		} 
	}
	
	/**
	 * Carrega lista sucesso.
	 *
	 * @return the string
	 */
	public String carregaListaSucesso(){
		listaGrid = new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>();
		ListarAssLayoutArqProdServicoEntradaDTO entradaDTO = new ListarAssLayoutArqProdServicoEntradaDTO();
			
		entradaDTO.setTipoServico(getFiltroServico());
		entradaDTO.setTipoLayoutArquivo(getFiltroTipoLayoutArquivo());
		entradaDTO.setCdIdentificadorLayoutNegocio(0);

		try{
			setListaGrid(getAssocLayoutArqProdServImpl().listarAssocicaoLayoutArqProdutoServico(entradaDTO));
			
			this.itemSelecionadoLista = null;
		} catch (PdcAdapterFunctionalException p) {
			if(!StringUtils.right(p.getCode(), 8).equals("PGIT0003")){
				this.setListaGrid(new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>());
				throw p;
			}
		} 
			
		return "PESQUISA";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		detalhar();
		return "EXCLUIR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setFiltroServicoIncluir(null);
		setFiltroTipoLayoutArquivoIncluir(null);
		setCdFinalidade(null);
		setRdoLayoutDefault(1);
		return "INCLUIR";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		preencheDados();
		return "ALTERAR";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		preencheDados();
		return "DETALHAR";
	}
	
	/**
	 * Preenche dados.
	 */
	@SuppressWarnings("unused")
	private void preencheDados(){
		DetalharAssLayoutArqProdServicoEntradaDTO entradaDTO = new DetalharAssLayoutArqProdServicoEntradaDTO();
		
		entradaDTO.setTipoServico(getItemSelecionadoLista().getCdTipoServico());
		entradaDTO.setTipoLayoutArquivo(getItemSelecionadoLista().getCdTipoLayoutArquivo());

		DetalharAssLayoutArqProdServicoSaidaDTO saidaDTO = getAssocLayoutArqProdServImpl().detalharAssocicaoLayoutArqProdutoServico(entradaDTO);

		setTipoLayoutArquivo(String.valueOf(saidaDTO.getCdTipoLayoutArquivo()));
		setTipoServico(String.valueOf(saidaDTO.getCdTipoServico()));
		
		setTipoLayoutArquivoDesc(saidaDTO.getDsTipoLayoutArquivo());
		setTipoServicoDesc(saidaDTO.getDsTipoServico());
		setFinalidadeLayoutArquivoDesc(saidaDTO.getDsIdentificadorLayoutNegocio());
		setCdIndicadorLayoutDefault(saidaDTO.getCdIndicadorLayoutDefault());
		setCdFinalidade(String.valueOf(saidaDTO.getCdIdentificadorLayoutNegocio()));
		
		setDataHoraInclusao(saidaDTO.getDataHoraInclusao());
		setDataHora(saidaDTO.getDataHoraManutencao());
		
		setUsuarioInclusao(saidaDTO.getUsuarioInclusao());
		setUsuario(saidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(saidaDTO.getComplementoInclusao() == null || saidaDTO.getComplementoInclusao().equals("0") ? "": saidaDTO.getComplementoInclusao());
		setComplementoManutencao(saidaDTO.getComplementoManutencao() == null || saidaDTO.getComplementoManutencao().equals("0")?"": saidaDTO.getComplementoManutencao());
		
		setTipoCanalInclusao(saidaDTO.getCdCanalInclusao()==0 ? "": saidaDTO.getCdCanalInclusao() + " - "  + saidaDTO.getDsCanalInclusao());
		setTipoCanalManutencao(saidaDTO.getCdCanalManutencao()==0 ? "": saidaDTO.getCdCanalManutencao() + " - "  + saidaDTO.getDsCanalManutencao());
		
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		listaGrid = new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>();
		ListarAssLayoutArqProdServicoEntradaDTO entradaDTO = new ListarAssLayoutArqProdServicoEntradaDTO();
	
		entradaDTO.setTipoServico(getFiltroServico());
		entradaDTO.setTipoLayoutArquivo(getFiltroTipoLayoutArquivo());
		entradaDTO.setCdIdentificadorLayoutNegocio(getFiltroFinalidadeLayoutArquivo());

		try{
			setListaGrid(getAssocLayoutArqProdServImpl().listarAssocicaoLayoutArqProdutoServico(entradaDTO));
			setBtoAcionado(true);
			this.itemSelecionadoLista = null;
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + p.getMessage() + ")" ,"#{associacaoLayoutArqProdServBean.voltar}", BradescoViewExceptionActionType.ACTION, false);
			this.setListaGrid(new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>());
			
		} 
		setItemSelecionadoLista(null);
		
		return "VOLTAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		return "VOLTAR";
	}

	/**
	 * Excluir confirmar.
	 *
	 * @return the string
	 */
	public String excluirConfirmar(){

		ExcluirAssLayoutArqProdServicoEntradaDTO entradaDTO  = new ExcluirAssLayoutArqProdServicoEntradaDTO();
		
		entradaDTO.setTipoLayoutArquivo(Integer.parseInt(getTipoLayoutArquivo()));
		entradaDTO.setTipoServico(Integer.parseInt(getTipoServico()));
		
		ExcluirAssLayoutArqProdServicoSaidaDTO saidaDTO = getAssocLayoutArqProdServImpl().excluirAssocicaoLayoutArqProdutoServico(entradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conAssociacaoLayoutArquivoProdutoServico", BradescoViewExceptionActionType.ACTION, false);
		carregaListaSucesso();
		return "CONFIRMAR";
	}
	
	/**
	 * Incluir confirmar.
	 *
	 * @return the string
	 */
	public String incluirConfirmar(){
		
		IncluirAssLayoutArqProdServicoEntradaDTO entradaDTO  = new IncluirAssLayoutArqProdServicoEntradaDTO();
		
		entradaDTO.setTipoLayoutArquivo(getFiltroTipoLayoutArquivoIncluir());
		entradaDTO.setTipoServico(getFiltroServicoIncluir());
		entradaDTO.setCdIdentificadorLayoutNegocio(Integer.parseInt(getCdFinalidade()));
		entradaDTO.setCdIndicadorLayoutDefault(getRdoLayoutDefault());
		
		IncluirAssLayoutArqProdServicoSaidaDTO saidaDTO = getAssocLayoutArqProdServImpl().incluirAssocicaoLayoutArqProdutoServico(entradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conAssociacaoLayoutArquivoProdutoServico", BradescoViewExceptionActionType.ACTION, false);
		
		if(getListaGrid() != null){
			carregaListaSucesso();
		}
		return "CONFIRMAR";
	}
	
	/**
	 * Incluir avancar.
	 *
	 * @return the string
	 */
	public String incluirAvancar(){
		
		setTipoLayoutArquivoDesc((String)getListaTipoLayoutArquivoHash().get(Integer.valueOf(getFiltroTipoLayoutArquivoIncluir())));
		setTipoServicoDesc((String)getListaTipoServicoHash().get(Integer.valueOf(getFiltroServicoIncluir())));
		
		return "AVANCAR";
	}

	/**
	 * alterarAvançar.
	 *
	 * @return the string
	 */
	public String alterarAvancar(){
		return "AVANCAR";
	}
	
	/**
	 * alterarConfirmar.
	 *
	 * @return the string
	 */
	public String alterarConfirmar(){
		
		AlterarArquivoServicoEntradaDTO entrada = new AlterarArquivoServicoEntradaDTO();
		
		entrada.setCdProdutoServicoOperacao(getItemSelecionadoLista().getCdTipoServico());
		entrada.setCdTipoLayoutArquivo(getItemSelecionadoLista().getCdTipoLayoutArquivo());
		entrada.setCdIdentificadorLayoutNegocio(Integer.valueOf(getCdFinalidade()));
		entrada.setCdIndicadorLayoutDefault(getCdIndicadorLayoutDefault());
		
		saidaAlterarArquivoServico = getAssocLayoutArqProdServImpl().alterarArquivoServico(entrada);
		BradescoFacesUtils.addInfoModalMessage("(" + saidaAlterarArquivoServico.getCodMensagem() + ") " + saidaAlterarArquivoServico.getMensagem(), "conAssociacaoLayoutArquivoProdutoServico", BradescoViewExceptionActionType.ACTION, false);		

		if(getListaGrid() != null){
			carregaListaSucesso();
		}
		
		return "CONFIRMAR";
	}
	
	/**
	 * voltarAlterar.
	 *
	 * @return the string
	 */	
	public String voltarAlterar(){
		return "VOLTAR_ALTERAR";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		carregaLista();
		
		return "ok";		
	}
	
	/**
	 * Preenche lista tipo servico.
	 *
	 * @return the list< select item>
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> preencheListaTipoServico() {
		try {
			listaTipoServico = new ArrayList<SelectItem>();
			List<ListarServicosSaidaDTO> listaTipoLayout = new ArrayList<ListarServicosSaidaDTO>();

			listaTipoLayout = comboService.listarTipoServicos(0);
			listaTipoServicoHash.clear();
			for(ListarServicosSaidaDTO combo : listaTipoLayout){
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		} catch (PdcAdapterFunctionalException e){
			listaTipoServico = new ArrayList<SelectItem>();
		}
		
		return listaTipoServico;
	}
	
	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
				
		return listaTipoServico;
	}
	
	/**
	 * Preenche lista tipo layout arquivo.
	 *
	 * @return the list< select item>
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> preencheListaTipoLayoutArquivo() {
		try {
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();		
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);
			
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLoteEntradaDTO);
			listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			listaTipoLayoutArquivoHash.clear();
			for(TipoLayoutArquivoSaidaDTO combo : listaTipoLayout){
				listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo.getCdTipoLayoutArquivo(),combo.getDsTipoLayoutArquivo()));
			}
		} catch (PdcAdapterFunctionalException e){
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}

		return listaTipoLayoutArquivo;
	}

	/**
	 * Preenche lista finalidade layout arquivo.
	 *
	 * @return the list< select item>
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> preencheListaFinalidadeLayoutArquivo() {
		try {
			listaFinalidadeLayoutArquivo = new ArrayList<SelectItem>();
			listaFinalidadeLayout = new ArrayList<FinalidadeLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();		
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);

			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLoteEntradaDTO);

			atribuirOcorrenciasTipoLayout();
			listaTipoLayoutArquivoHash.clear();
			for(FinalidadeLayoutArquivoSaidaDTO combo : listaFinalidadeLayout){
				listaTipoLayoutArquivoHash.put(combo.getCdFinalidadeLayoutArquivo(), combo.getDsFinalidadeLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo.getCdFinalidadeLayoutArquivo(), combo.getDsFinalidadeLayoutArquivo()));
			}
		} catch(PdcAdapterFunctionalException e){
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}

		return listaTipoLayoutArquivo;
	}

	/**
	 * Atribuir Ocorrencias Tipo Layout.
	 *
	 */
	private void atribuirOcorrenciasTipoLayout() {
		List<TipoLayoutArquivoSaidaDTO> ocorrencias = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
		FinalidadeLayoutArquivoSaidaDTO o = null;
		for (TipoLayoutArquivoSaidaDTO tipoLayout : ocorrencias) {
			o = new FinalidadeLayoutArquivoSaidaDTO();
			o.setCdFinalidadeLayoutArquivo(tipoLayout.getCdTipoLayoutArquivo());
			o.setDsFinalidadeLayoutArquivo(tipoLayout.getDsTipoLayoutArquivo());
			listaFinalidadeLayout.add(o);
		}
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaTipoLayoutArquivo() {
		
		return listaTipoLayoutArquivo;
	}
	
	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}
	
	/**
	 * Get: filtroServico.
	 *
	 * @return filtroServico
	 */
	public Integer getFiltroServico() {
		return filtroServico;
	}
	
	/**
	 * Set: filtroServico.
	 *
	 * @param servico the filtro servico
	 */
	public void setFiltroServico(Integer servico) {
		this.filtroServico = servico;
	}
	
	/**
	 * Get: filtroTipoLayoutArquivo.
	 *
	 * @return filtroTipoLayoutArquivo
	 */
	public Integer getFiltroTipoLayoutArquivo() {
		return filtroTipoLayoutArquivo;
	}
	
	/**
	 * Set: filtroTipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the filtro tipo layout arquivo
	 */
	public void setFiltroTipoLayoutArquivo(Integer tipoLayoutArquivo) {
		this.filtroTipoLayoutArquivo = tipoLayoutArquivo;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}
	
	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}
	
	/**
	 * Is mostra botoes.
	 *
	 * @return true, if is mostra botoes
	 */
	public boolean isMostraBotoes() {
		return mostraBotoes;
	}
	
	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param servico the tipo servico
	 */
	public void setTipoServico(String servico) {
		this.tipoServico = servico;
	}

	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public String getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}

	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(String tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}

	/**
	 * Get: assocLayoutArqProdServImpl.
	 *
	 * @return assocLayoutArqProdServImpl
	 */
	public IAssocLayoutArqProdServService getAssocLayoutArqProdServImpl() {
		return assocLayoutArqProdServImpl;
	}

	/**
	 * Set: assocLayoutArqProdServImpl.
	 *
	 * @param assocLayoutArqProdServImpl the assoc layout arq prod serv impl
	 */
	public void setAssocLayoutArqProdServImpl(
			IAssocLayoutArqProdServService assocLayoutArqProdServImpl) {
		this.assocLayoutArqProdServImpl = assocLayoutArqProdServImpl;
	}

	

	/**
	 * Get: dataHora.
	 *
	 * @return dataHora
	 */
	public String getDataHora() {
		return dataHora;
	}
	
	/**
	 * Set: dataHora.
	 *
	 * @param dataHora the data hora
	 */
	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	/**
	 * Get: usuario.
	 *
	 * @return usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Set: usuario.
	 *
	 * @param usuario the usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarAssLayoutArqProdServicoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarAssLayoutArqProdServicoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: tipoLayoutArquivoDesc.
	 *
	 * @return tipoLayoutArquivoDesc
	 */
	public String getTipoLayoutArquivoDesc() {
		return tipoLayoutArquivoDesc;
	}

	/**
	 * Set: tipoLayoutArquivoDesc.
	 *
	 * @param tipoLayoutArquivoDesc the tipo layout arquivo desc
	 */
	public void setTipoLayoutArquivoDesc(String tipoLayoutArquivoDesc) {
		this.tipoLayoutArquivoDesc = tipoLayoutArquivoDesc;
	}

	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}

	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}

	/**
	 * Get: filtroTipoLayoutArquivoIncluir.
	 *
	 * @return filtroTipoLayoutArquivoIncluir
	 */
	public Integer getFiltroTipoLayoutArquivoIncluir() {
		return filtroTipoLayoutArquivoIncluir;
	}

	/**
	 * Set: filtroTipoLayoutArquivoIncluir.
	 *
	 * @param filtroTipoLayoutArquivoIncluir the filtro tipo layout arquivo incluir
	 */
	public void setFiltroTipoLayoutArquivoIncluir(
			Integer filtroTipoLayoutArquivoIncluir) {
		this.filtroTipoLayoutArquivoIncluir = filtroTipoLayoutArquivoIncluir;
	}

	/**
	 * Get: filtroServicoIncluir.
	 *
	 * @return filtroServicoIncluir
	 */
	public Integer getFiltroServicoIncluir() {
		return filtroServicoIncluir;
	}

	/**
	 * Set: filtroServicoIncluir.
	 *
	 * @param filtroServicoIncluir the filtro servico incluir
	 */
	public void setFiltroServicoIncluir(Integer filtroServicoIncluir) {
		this.filtroServicoIncluir = filtroServicoIncluir;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public ListarAssLayoutArqProdServicoSaidaDTO getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(
			ListarAssLayoutArqProdServicoSaidaDTO itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: cdFinalidade.
	 *
	 * @return cdFinalidade
	 */
	public String getCdFinalidade() {
		return cdFinalidade;
	}

	/**
	 * Set: cdFinalidade.
	 *
	 * @param cdFinalidade the cd finalidade
	 */
	public void setCdFinalidade(String cdFinalidade) {
		this.cdFinalidade = cdFinalidade;
	}

	/**
	 * Get: finalidadeLayoutArquivoDesc.
	 *
	 * @return finalidadeLayoutArquivoDesc
	 */
	public String getFinalidadeLayoutArquivoDesc() {
	    return finalidadeLayoutArquivoDesc;
	}

	/**
	 * Set: finalidadeLayoutArquivoDesc.
	 *
	 * @param finalidadeLayoutArquivoDesc the finalidade layout arquivo desc
	 */
	public void setFinalidadeLayoutArquivoDesc(String finalidadeLayoutArquivoDesc) {
	    this.finalidadeLayoutArquivoDesc = finalidadeLayoutArquivoDesc;
	}

	/**
	 * Get: listaFinalidadeLayoutArquivo.
	 *
	 * @return listaFinalidadeLayoutArquivo
	 */
	public List<SelectItem> getListaFinalidadeLayoutArquivo() {
		return listaFinalidadeLayoutArquivo;
	}

	/**
	 * Set: listaFinalidadeLayoutArquivo.
	 *
	 * @param listaFinalidadeLayoutArquivo the lista finalidade layout arquivo
	 */
	public void setListaFinalidadeLayoutArquivo(
			List<SelectItem> listaFinalidadeLayoutArquivo) {
		this.listaFinalidadeLayoutArquivo = listaFinalidadeLayoutArquivo;
	}

	/**
	 * Get: listaFinalidadeLayout.
	 *
	 * @return listaFinalidadeLayout
	 */
	public List<FinalidadeLayoutArquivoSaidaDTO> getListaFinalidadeLayout() {
		return listaFinalidadeLayout;
	}

	/**
	 * Set: listaFinalidadeLayout.
	 *
	 * @param listaFinalidadeLayout the lista finalidade layout
	 */
	public void setListaFinalidadeLayout(List<FinalidadeLayoutArquivoSaidaDTO> listaFinalidadeLayout) {
		this.listaFinalidadeLayout = listaFinalidadeLayout;
	}

	/**
	 * Get: filtroFinalidadeLayoutArquivo.
	 *
	 * @return filtroFinalidadeLayoutArquivo
	 */
	public Integer getFiltroFinalidadeLayoutArquivo() {
		return filtroFinalidadeLayoutArquivo;
	}

	/**
	 * Set: filtroFinalidadeLayoutArquivo.
	 *
	 * @param filtroFinalidadeLayoutArquivo the filtro finalidade layout arquivo
	 */
	public void setFiltroFinalidadeLayoutArquivo(
			Integer filtroFinalidadeLayoutArquivo) {
		this.filtroFinalidadeLayoutArquivo = filtroFinalidadeLayoutArquivo;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}

	public Integer getRdoLayoutDefault() {
		return rdoLayoutDefault;
	}

	public void setRdoLayoutDefault(Integer rdoLayoutDefault) {
		this.rdoLayoutDefault = rdoLayoutDefault;
	}

	/**
	 * @param cdIndicadorLayoutDefault the cdIndicadorLayoutDefault to set
	 */
	public void setCdIndicadorLayoutDefault(Integer cdIndicadorLayoutDefault) {
		this.cdIndicadorLayoutDefault = cdIndicadorLayoutDefault;
	}

	/**
	 * @return the cdIndicadorLayoutDefault
	 */
	public Integer getCdIndicadorLayoutDefault() {
		return cdIndicadorLayoutDefault;
	}

	public AlterarArquivoServicoSaidaDTO getSaidaAlterarArquivoServico() {
		return saidaAlterarArquivoServico;
	}

	public void setSaidaAlterarArquivoServico(
			AlterarArquivoServicoSaidaDTO saidaAlterarArquivoServico) {
		this.saidaAlterarArquivoServico = saidaAlterarArquivoServico;
	}

	
}

