/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.autorizarpagamentosconsolidados
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.autorizarpagamentosconsolidados;

import static br.com.bradesco.web.pgit.utils.PgitUtil.isStringNullLong;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.IAutorizarPagamentosConsolidadosService;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.AutorizarParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.ConsultarPagtosConsPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.ConsultarPagtosConsPendAutorizacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.DetalharPagtosConsPendAutorizacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.DetalharPagtosConsPendAutorizacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.autorizarpagamentosconsolidados.bean.OcorrenciasDetPagConsPendAutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;

// TODO: Auto-generated Javadoc
/**
 * Nome: AutorizarPagamentosConsolidadosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AutorizarPagamentosConsolidadosBean extends PagamentosBean {
    /*
     * Atributos
     */
    // Gerais
    /** Atributo autorizarPagamentosConsolidadosServiceImpl. */
    private IAutorizarPagamentosConsolidadosService autorizarPagamentosConsolidadosServiceImpl;

    // autorizarPagamentosConsolidados
    /** Atributo listaGridAutorizarPagamentosConsolidado. */
    private List<ConsultarPagtosConsPendAutorizacaoSaidaDTO> listaGridAutorizarPagamentosConsolidado;

    /** Atributo listaRadiosAutorizarPagamentosConsolidado. */
    private List<SelectItem> listaRadiosAutorizarPagamentosConsolidado;

    /** Atributo itemSelecionadoListaAutorizarPagamentosConsolidado. */
    private Integer itemSelecionadoListaAutorizarPagamentosConsolidado;

    /** Atributo chkServico. */
    private boolean chkServico;

    /** Atributo chkModalidade. */
    private boolean chkModalidade;

    /** Atributo listaAutorizarSituacaoPagamentoFiltro. */
    private List<SelectItem> listaAutorizarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();

    /** Atributo listaAutorizarSituacaoPagamentoFiltroHash. */
    private Map<Integer, String> listaAutorizarSituacaoPagamentoFiltroHash = new HashMap<Integer, String>();

    /** Atributo listaAutorizarMotivoSituacaoPagamentoFiltro. */
    private List<SelectItem> listaAutorizarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();

    /** Atributo listaAutorizarMotivoSituacaoPagamentoFiltroHash. */
    private Map<Integer, String> listaAutorizarMotivoSituacaoPagamentoFiltroHash = new HashMap<Integer, String>();

    /** Atributo panelBotoes. */
    private boolean panelBotoes;

    // relatorio
    /** Atributo logoPath. */
    private String logoPath;

    // detAutorizarPagamentosConsolidadosFiltro
    // Cliente
    /** Atributo cpfCnpj. */
    private String cpfCnpj;

    /** Atributo nomeRazaoSocial. */
    private String nomeRazaoSocial;

    /** Atributo empresaConglomerado. */
    private String empresaConglomerado;

    /** Atributo nrContrato. */
    private Long nrContrato;

    /** Atributo dsContratacao. */
    private String dsContratacao;

    /** Atributo situacao. */
    private String situacao;

    /** Atributo qtdePagamentos. */
    private Long qtdePagamentos;

    /** Atributo vlTotalPagamentos. */
    private BigDecimal vlTotalPagamentos;

    // Variaveis Conta Debito
    /** Atributo banco. */
    private String banco;

    /** Atributo agencia. */
    private String agencia;

    /** Atributo conta. */
    private String conta;

    /** Atributo tipo. */
    private String tipo;

    /** Atributo listaDetAutPagamentosConsolidados. */
    private List<OcorrenciasDetPagConsPendAutSaidaDTO> listaDetAutPagamentosConsolidados;

    /** Atributo itemSelecionadoListaDetAutPagamentosConsolidados. */
    private Integer itemSelecionadoListaDetAutPagamentosConsolidados;

    /** Atributo listaControleDetAutPagamentosConsolidados. */
    private List<SelectItem> listaControleDetAutPagamentosConsolidados;

    /** Atributo tipoCanal. */
    private Integer tipoCanal;

    // autorizarPagamentosConsolidadosParcial
    /** Atributo tipoServico. */
    private String tipoServico;

    /** Atributo modalidade. */
    private String modalidade;

    /** Atributo qtdePagamentosAutorizados. */
    private Long qtdePagamentosAutorizados;

    /** Atributo vlTotalPagamentosAutorizados. */
    private BigDecimal vlTotalPagamentosAutorizados;

    /** Atributo listaGridAutorizarPagamentosConsolidadosParcial. */
    private List<OcorrenciasDetPagConsPendAutSaidaDTO> listaGridAutorizarPagamentosConsolidadosParcial;

    /** Atributo listaControleAutorizarPagamentosConsolidadosParcial. */
    private List<SelectItem> listaControleAutorizarPagamentosConsolidadosParcial;

    /** Atributo itemSelecionadolistaAutorizarPagamentosConsolidadosParcial. */
    private Integer itemSelecionadolistaAutorizarPagamentosConsolidadosParcial;

    /** Atributo btnChecaTodos. */
    private boolean btnChecaTodos;

    /** Atributo btnAutorizaoSelecionados. */
    private boolean btnAutorizaoSelecionados;

    /** Atributo listaGridAutorizarPagamentosConsolidadosSelecionados. */
    private List<OcorrenciasDetPagConsPendAutSaidaDTO> listaGridAutorizarPagamentosConsolidadosSelecionados;

    /** Atributo hiddenConfirma. */
    private boolean hiddenConfirma;

    /** Atributo listaGridAutorizarPagamentosConsolidadosIntegral. */
    private List<OcorrenciasDetPagConsPendAutSaidaDTO> listaGridAutorizarPagamentosConsolidadosIntegral;

    // Favorecido
    /** Atributo numeroInscricaoFavorecido. */
    private String numeroInscricaoFavorecido;

    /** Atributo tipoFavorecido. */
    private Integer tipoFavorecido;

    /** Atributo descTipoFavorecido. */
    private String descTipoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo cdFavorecido. */
    private String cdFavorecido;

    /** Atributo dsBancoFavorecido. */
    private String dsBancoFavorecido;

    /** Atributo dsAgenciaFavorecido. */
    private String dsAgenciaFavorecido;

    /** Atributo dsContaFavorecido. */
    private String dsContaFavorecido;

    /** Atributo dsTipoContaFavorecido. */
    private String dsTipoContaFavorecido;

    /** Atributo numControleInternoLote. */
    private Long numControleInternoLote;

    // Beneficiario
    /** Atributo numeroInscricaoBeneficiario. */
    private String numeroInscricaoBeneficiario;

    /** Atributo cdTipoBeneficiario. */
    private Integer cdTipoBeneficiario;

    /** Atributo dsTipoBeneficiario. */
    private String dsTipoBeneficiario;

    /** Atributo tipoBeneficiario. */
    private String tipoBeneficiario;

    /** Atributo nomeBeneficiario. */
    private String nomeBeneficiario;

    /** Atributo dtNascimentoBeneficiario. */
    private String dtNascimentoBeneficiario;

    /** Atributo exibeBeneficiario. */
    private boolean exibeBeneficiario;

    // Pagamento
    /** Atributo dsTipoServico. */
    private String dsTipoServico;

    /** Atributo dsIndicadorModalidade. */
    private String dsIndicadorModalidade;

    /** Atributo nrSequenciaArquivoRemessa. */
    private String nrSequenciaArquivoRemessa;

    /** Atributo nrLoteArquivoRemessa. */
    private String nrLoteArquivoRemessa;

    /** Atributo nroPagamento. */
    private String nroPagamento;

    /** Atributo cdListaDebito. */
    private String cdListaDebito;

    /** Atributo dtAgendamento. */
    private String dtAgendamento;

    /** Atributo dtPagamento. */
    private String dtPagamento;

    /** Atributo dtDevolucaoEstorno. */
    private String dtDevolucaoEstorno;

    /** Atributo dtVencimento. */
    private String dtVencimento;		

    /** Atributo cdIndicadorEconomicoMoeda. */
    private String cdIndicadorEconomicoMoeda;

    /** Atributo qtMoeda. */
    private BigDecimal qtMoeda;

    /** Atributo vlrAgendamento. */
    private BigDecimal vlrAgendamento;

    /** Atributo vlrEfetivacao. */
    private BigDecimal vlrEfetivacao;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private String cdSituacaoOperacaoPagamento;

    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;

    /** Atributo dsPagamento. */
    private String dsPagamento;

    /** Atributo nrDocumento. */
    private String nrDocumento;

    /** Atributo tipoDocumento. */
    private String tipoDocumento;

    /** Atributo cdSerieDocumento. */
    private String cdSerieDocumento;

    /** Atributo vlDocumento. */
    private BigDecimal vlDocumento;

    /** Atributo dtEmissaoDocumento. */
    private String dtEmissaoDocumento;

    /** Atributo dsUsoEmpresa. */
    private String dsUsoEmpresa;

    /** Atributo dsMensagemPrimeiraLinha. */
    private String dsMensagemPrimeiraLinha;

    /** Atributo dsMensagemSegundaLinha. */
    private String dsMensagemSegundaLinha;

    /** Atributo cdBancoCredito. */
    private String cdBancoCredito;

    /** Atributo agenciaDigitoCredito. */
    private String agenciaDigitoCredito;

    /** Atributo contaDigitoCredito. */
    private String contaDigitoCredito;

    /** Atributo tipoContaCredito. */
    private String tipoContaCredito;

    /** Atributo cdBancoDestino. */
    private String cdBancoDestino;

    /** Atributo agenciaDigitoDestino. */
    private String agenciaDigitoDestino;

    /** Atributo contaDigitoDestino. */
    private String contaDigitoDestino;

    /** Atributo tipoContaDestino. */
    private String tipoContaDestino;

    /** Atributo vlDesconto. */
    private BigDecimal vlDesconto;

    /** Atributo vlAbatimento. */
    private BigDecimal vlAbatimento;

    /** Atributo vlMulta. */
    private BigDecimal vlMulta;

    /** Atributo vlMora. */
    private BigDecimal vlMora;

    /** Atributo vlDeducao. */
    private BigDecimal vlDeducao;

    /** Atributo vlAcrescimo. */
    private BigDecimal vlAcrescimo;

    /** Atributo vlImpostoRenda. */
    private BigDecimal vlImpostoRenda;

    /** Atributo vlIss. */
    private BigDecimal vlIss;

    /** Atributo vlIof. */
    private BigDecimal vlIof;

    /** Atributo vlInss. */
    private BigDecimal vlInss;

    /** Atributo camaraCentralizadora. */
    private String camaraCentralizadora;

    /** Atributo finalidadeDoc. */
    private String finalidadeDoc;

    /** Atributo identificacaoTransferenciaDoc. */
    private String identificacaoTransferenciaDoc;

    /** Atributo identificacaoTitularidade. */
    private String identificacaoTitularidade;

    /** Atributo finalidadeTed. */
    private String finalidadeTed;

    /** Atributo identificacaoTransferenciaTed. */
    private String identificacaoTransferenciaTed;

    /** Atributo numeroOp. */
    private String numeroOp;

    /** Atributo dataExpiracaoCredito. */
    private String dataExpiracaoCredito;

    /** Atributo instrucaoPagamento. */
    private String instrucaoPagamento;

    /** Atributo numeroCheque. */
    private String numeroCheque;

    /** Atributo serieCheque. */
    private String serieCheque;

    /** Atributo indicadorAssociadoUnicaAgencia. */
    private String indicadorAssociadoUnicaAgencia;

    /** Atributo identificadorTipoRetirada. */
    private String identificadorTipoRetirada;

    /** Atributo identificadorRetirada. */
    private String identificadorRetirada;

    /** Atributo dataRetirada. */
    private String dataRetirada;

    /** Atributo vlImpostoMovFinanceira. */
    private BigDecimal vlImpostoMovFinanceira;

    /** Atributo codigoBarras. */
    private String codigoBarras;

    /** Atributo dddContribuinte. */
    private String dddContribuinte;

    /** Atributo telefoneContribuinte. */
    private String telefoneContribuinte;

    /** Atributo inscricaoEstadualContribuinte. */
    private String inscricaoEstadualContribuinte;

    /** Atributo agenteArrecadador. */
    private String agenteArrecadador;

    /** Atributo codigoReceita. */
    private String codigoReceita;

    /** Atributo numeroReferencia. */
    private String numeroReferencia;

    /** Atributo numeroCotaParcela. */
    private String numeroCotaParcela;

    /** Atributo percentualReceita. */
    private BigDecimal percentualReceita;

    /** Atributo periodoApuracao. */
    private String periodoApuracao;

    /** Atributo anoExercicio. */
    private String anoExercicio;

    /** Atributo dataReferencia. */
    private String dataReferencia;

    /** Atributo vlReceita. */
    private BigDecimal vlReceita;

    /** Atributo vlPrincipal. */
    private BigDecimal vlPrincipal;

    /** Atributo vlAtualizacaoMonetaria. */
    private BigDecimal vlAtualizacaoMonetaria;

    /** Atributo vlTotal. */
    private BigDecimal vlTotal;

    /** Atributo codigoCnae. */
    private String codigoCnae;

    /** Atributo placaVeiculo. */
    private String placaVeiculo;

    /** Atributo numeroParcelamento. */
    private String numeroParcelamento;

    /** Atributo codigoOrgaoFavorecido. */
    private String codigoOrgaoFavorecido;

    /** Atributo nomeOrgaoFavorecido. */
    private String nomeOrgaoFavorecido;

    /** Atributo codigoUfFavorecida. */
    private String codigoUfFavorecida;

    /** Atributo descricaoUfFavorecida. */
    private String descricaoUfFavorecida;

    /** Atributo vlAcrescimoFinanceiro. */
    private BigDecimal vlAcrescimoFinanceiro;

    /** Atributo vlHonorarioAdvogado. */
    private BigDecimal vlHonorarioAdvogado;

    /** Atributo observacaoTributo. */
    private String observacaoTributo;

    /** Atributo codigoInss. */
    private String codigoInss;

    /** Atributo dataCompetencia. */
    private String dataCompetencia;

    /** Atributo vlOutrasEntidades. */
    private BigDecimal vlOutrasEntidades;

    /** Atributo codigoTributo. */
    private String codigoTributo;

    /** Atributo codigoRenavam. */
    private String codigoRenavam;

    /** Atributo municipioTributo. */
    private String municipioTributo;

    /** Atributo ufTributo. */
    private String ufTributo;

    /** Atributo numeroNsu. */
    private String numeroNsu;

    /** Atributo opcaoTributo. */
    private String opcaoTributo;

    /** Atributo opcaoRetiradaTributo. */
    private String opcaoRetiradaTributo;

    /** Atributo clienteDepositoIdentificado. */
    private String clienteDepositoIdentificado;

    /** Atributo tipoDespositoIdentificado. */
    private String tipoDespositoIdentificado;

    /** Atributo produtoDepositoIdentificado. */
    private String produtoDepositoIdentificado;

    /** Atributo sequenciaProdutoDepositoIdentificado. */
    private String sequenciaProdutoDepositoIdentificado;

    /** Atributo bancoCartaoDepositoIdentificado. */
    private String bancoCartaoDepositoIdentificado;

    /** Atributo cartaoDepositoIdentificado. */
    private String cartaoDepositoIdentificado;

    /** Atributo bimCartaoDepositoIdentificado. */
    private String bimCartaoDepositoIdentificado;

    /** Atributo viaCartaoDepositoIdentificado. */
    private String viaCartaoDepositoIdentificado;

    /** Atributo codigoDepositante. */
    private String codigoDepositante;

    /** Atributo nomeDepositante. */
    private String nomeDepositante;

    /** Atributo codigoPagador. */
    private String codigoPagador;

    /** Atributo codigoOrdemCredito. */
    private String codigoOrdemCredito;

    /** Atributo nossoNumero. */
    private String nossoNumero;

    /** Atributo dsOrigemPagamento. */
    private String dsOrigemPagamento;

    /** Atributo cdIndentificadorJudicial. */
    private String cdIndentificadorJudicial;

    /** Atributo dtLimiteDescontoPagamento. */
    private String dtLimiteDescontoPagamento;

    /** Atributo dtLimitePagamento. */
    private String dtLimitePagamento;

    /** Atributo dsRastreado. */
    private String dsRastreado;

    /** Atributo carteira. */
    private String carteira;

    /** Atributo cdSituacaoTranferenciaAutomatica. */
    private String cdSituacaoTranferenciaAutomatica;

    /** Atributo nrRemessa. */
    private Long nrRemessa;

    /** Atributo dtFloatingPagamento. */
    private String dtFloatingPagamento;

    /** Atributo dtEfetivFloatPgto. */
    private String dtEfetivFloatPgto;

    /** Atributo vlFloatingPagamento. */
    private BigDecimal vlFloatingPagamento;

    /** Atributo cdOperacaoDcom. */
    private Long cdOperacaoDcom;

    /** Atributo dsSituacaoDcom. */
    private String dsSituacaoDcom;

    // Trilha Auditoria
    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo disableArgumentosConsulta. */
    private boolean disableArgumentosConsulta;

    /** Atributo cdBancoOriginal. */
    private Integer cdBancoOriginal;

    /** Atributo dsBancoOriginal. */
    private String dsBancoOriginal;

    /** Atributo cdAgenciaBancariaOriginal. */
    private Integer cdAgenciaBancariaOriginal;

    /** Atributo cdDigitoAgenciaOriginal. */
    private String cdDigitoAgenciaOriginal;

    /** Atributo dsAgenciaOriginal. */
    private String dsAgenciaOriginal;

    /** Atributo cdContaBancariaOriginal. */
    private Long cdContaBancariaOriginal;

    /** Atributo cdDigitoContaOriginal. */
    private String cdDigitoContaOriginal;

    /** Atributo dsTipoContaOriginal. */
    private String dsTipoContaOriginal;

    // Flag
    /** Atributo exibeDcom. */
    private Boolean exibeDcom;

    // Navega��o
    /**
     * Autorizar integral.
     *
     * @return the string
     */
    public String autorizarIntegral() {
        limparPagamentos();
        try {
            carregaGridAutorizarPagamentosConsolidadosIntegral();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridAutorizarPagamentosConsolidadosIntegral(null);
            return "";
        }
        return "AUTORIZAR_INTEGRAL";
    }

    /**
     * Autorizar parcial.
     *
     * @return the string
     */
    public String autorizarParcial() {
        limparPagamentos();
        setPanelBotoes(true);
        try {
            carregaGridAutorizarPagamentosConsolidadosParcial();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridAutorizarPagamentosConsolidadosParcial(null);
            setItemSelecionadolistaAutorizarPagamentosConsolidadosParcial(null);
            return "";
        }

        return "AUTORIZAR_PARCIAL";
    }

    /**
     * Detalhar filtro.
     *
     * @return the string
     */
    public String detalharFiltro() {
        limparPagamentos();
        try {
            carregaListaListaDetAutPagamentosConsolidados();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaDetAutPagamentosConsolidados(null);
            setItemSelecionadoListaDetAutPagamentosConsolidados(null);
            return "";
        }

        return "DETALHAR_FILTRO";
    }

    /**
     * Pesquisar det aut pagamentos consolidados.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarDetAutPagamentosConsolidados(ActionEvent evt) {
        try {
            carregaListaListaDetAutPagamentosConsolidados();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaDetAutPagamentosConsolidados(null);
            setItemSelecionadoListaDetAutPagamentosConsolidados(null);
            return "";
        }
        return "";
    }

    /**
     * Voltar autorizar pagamentos consolidados.
     *
     * @return the string
     */
    public String voltarAutorizarPagamentosConsolidados() {
        setItemSelecionadoListaAutorizarPagamentosConsolidado(null);

        return "VOLTAR";
    }

    /**
     * Voltar.
     *
     * @return the string
     */
    public String voltar() {
        for (int i = 0; i < getListaGridAutorizarPagamentosConsolidadosParcial()
        .size(); i++) {
            getListaGridAutorizarPagamentosConsolidadosParcial().get(i)
            .setCheck(false);
        }

        setBtnChecaTodos(false);

        return "VOLTAR";
    }

    // M�todos autorizarPagamentosConsolidados
    /**
     * Limpar args lista apos pesquisa cliente contrato.
     *
     * @param evt the evt
     */
    public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
        limparCampos();
        setListaGridAutorizarPagamentosConsolidado(null);
        setItemSelecionadoListaAutorizarPagamentosConsolidado(null);
    }

    /**
     * Limpar argumentos pesquisa.
     *
     * @return the string
     */
    public String limparArgumentosPesquisa() {
        limparCampos();
        setHabilitaArgumentosPesquisa(false);
        setListaGridAutorizarPagamentosConsolidado(null);
        setItemSelecionadoListaAutorizarPagamentosConsolidado(null);
        setListaRadiosAutorizarPagamentosConsolidado(null);

        return "";

    }

    /**
     * Limpar consulta.
     *
     * @return the string
     */
    public String limparConsulta() {
        setListaGridAutorizarPagamentosConsolidado(null);
        setItemSelecionadoListaAutorizarPagamentosConsolidado(null);
        setListaRadiosAutorizarPagamentosConsolidado(null);
        setDisableArgumentosConsulta(false);
        return "";
    }

    /**
     * Limpar tela.
     *
     * @return the string
     */
    public String limparTela() {
        setDisableArgumentosConsulta(false);
        limparArgumentosPesquisa();
        getFiltroAgendamentoEfetivacaoEstornoBean().limparFiltroPrincipal();
        getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
            null);
        return "";
    }

    /**
     * Limpar tela principal.
     */
    public void limparTelaPrincipal() {
        limparCampos();
        limparFiltroPrincipal();
        setListaGridAutorizarPagamentosConsolidado(null);
        setItemSelecionadoListaAutorizarPagamentosConsolidado(null);
    }

    /**
     * Limpar apos alterar opcao cliente.
     *
     * @return the string
     */
    public String limparAposAlterarOpcaoCliente() {
        // Limpar Argumentos de Pesquisa e Lista
        getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
        setListaGridAutorizarPagamentosConsolidado(null);
        setItemSelecionadoListaAutorizarPagamentosConsolidado(null);

        limparCampos();

        return "";

    }

    // Inicializa��o
    /**
     * Iniciar tela.
     *
     * @param e the e
     */
    public void iniciarTela(ActionEvent e) {
        // Tela de Filtro
        getFiltroAgendamentoEfetivacaoEstornoBean()
        .setEntradaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasEntradaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean()
        .setSaidaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasSaidaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
        getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
        getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
        "autorizarPagamentosConsolidados");
        limparCampos();
        setListaGridAutorizarPagamentosConsolidado(null);
        setItemSelecionadoListaAutorizarPagamentosConsolidado(null);

        // Carregamento dos combos
        listarTipoServicoPagto();
        listarInscricaoFavorecido();
        listarConsultarSituacaoPagamento();

        listarOrgaoPagador();
        setHabilitaArgumentosPesquisa(false);
        setDisableArgumentosConsulta(false);
        getFiltroAgendamentoEfetivacaoEstornoBean()
        .setClienteContratoSelecionado(false);
        getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

        // Radio de Agendados � Pagos/N�o Pagos, ficara sempre protegido e com a
        // op��o �Agendados� selecionada;
        setAgendadosPagosNaoPagos("1");
    }

    // M�todo sobreposto para n�o limpar os agendados - pagos/nao pagos
    /**
     * (non-Javadoc).
     *
     * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparCampos()
     */
    public void limparCampos() {
        getFiltroAgendamentoEfetivacaoEstornoBean()
        .limparParticipanteContrato();
        setChkContaDebito(false);
        limparContaDebito();
        limparRemessaFavorecidoBeneficiarioLinha();
        limparSituacaoMotivo();
        limparTipoServicoModalidade();

        setDataInicialPagamentoFiltro(new Date());
        setDataFinalPagamentoFiltro(new Date());
        setChkParticipanteContrato(false);
        setChkRemessa(false);
        setChkServicoModalidade(false);
        setChkSituacaoMotivo(false);
        setRadioPesquisaFiltroPrincipal("");
        setRadioFavorecidoFiltro("");
        setRadioBeneficiarioFiltro("");

        setClassificacaoFiltro(null);
    }

    /**
     * Pesquisar.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisar(ActionEvent evt) {
        carregaListaAutorizarPagamentosConsolidados();
        return "";
    }

    /**
     * Pesquisar parcial.
     */
    public void pesquisarParcial() {
        setBtnChecaTodos(false);
        for (int i = 0; i < getListaGridAutorizarPagamentosConsolidadosParcial()
        .size(); i++) {
            getListaGridAutorizarPagamentosConsolidadosParcial().get(i)
            .setCheck(false);
        }
    }

    /**
     * Carrega lista autorizar pagamentos consolidados.
     *
     * @param evt the evt
     */
    public void carregaListaAutorizarPagamentosConsolidados(ActionEvent evt) {
        carregaListaAutorizarPagamentosConsolidados();
    }

    /**
     * Carrega lista autorizar pagamentos consolidados.
     */
    public void carregaListaAutorizarPagamentosConsolidados() {
        try {
            ConsultarPagtosConsPendAutorizacaoEntradaDTO entrada = new ConsultarPagtosConsPendAutorizacaoEntradaDTO();

            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("0")) {
                entrada.setCdTipoPesquisa(1);
                entrada
                .setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getCdPessoaJuridicaContrato() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getCdPessoaJuridicaContrato()
                        : 0L);
                entrada
                .setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getCdTipoContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getCdTipoContratoNegocio()
                        : 0);
                entrada
                .setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getNrSequenciaContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getNrSequenciaContratoNegocio()
                        : 0L);
                entrada.setCdBanco(0);
                entrada.setCdAgencia(0);
                entrada.setCdConta(0L);
                entrada.setCdDigitoConta("00");
            }
            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("1")) {
                entrada.setCdTipoPesquisa(1);
                entrada
                .setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getEmpresaGestoraFiltro() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getEmpresaGestoraFiltro()
                        : 0L);
                entrada
                .setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getTipoContratoFiltro() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getTipoContratoFiltro()
                        : 0);
                entrada
                .setNrSequenciaContratoNegocio(!getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getNumeroFiltro().equals("") ? Long
                        .parseLong(getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getNumeroFiltro())
                            : 0L);
                entrada.setCdBanco(0);
                entrada.setCdAgencia(0);
                entrada.setCdConta(0L);
                entrada.setCdDigitoConta("00");
            }
            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("2")) {
                entrada.setCdTipoPesquisa(2);
                entrada.setCdPessoaJuridicaContrato(0L);
                entrada.setCdTipoContratoNegocio(0);
                entrada.setNrSequenciaContratoNegocio(0L);
                entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getBancoContaDebitoFiltro());
                entrada
                .setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getAgenciaContaDebitoBancariaFiltro());
                entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getContaBancariaContaDebitoFiltro());
                entrada
                .setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getDigitoContaDebitoFiltro());
            } else {
                if (isChkContaDebito()) {
                    entrada.setCdBanco(getCdBancoContaDebito());
                    entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
                    entrada.setCdConta(getCdContaBancariaContaDebito());
                    entrada
                    .setCdDigitoConta(getCdDigitoContaContaDebito() != null
                        && !getCdDigitoContaContaDebito()
                        .equals("") ? getCdDigitoContaContaDebito()
                            : "00");
                } else {
                    entrada.setCdBanco(0);
                    entrada.setCdAgencia(0);
                    entrada.setCdConta(0L);
                    entrada.setCdDigitoConta("00");
                }
            }

            entrada.setCdAgendadosPagosNaoPagos(Integer
                .parseInt(getAgendadosPagosNaoPagos()));

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            entrada
            .setDtCreditoPagamentoInicio((getDataInicialPagamentoFiltro() == null) ? ""
                : sdf.format(getDataInicialPagamentoFiltro()));
            entrada
            .setDtCreditoPagamentoFim((getDataFinalPagamentoFiltro() == null) ? ""
                : sdf.format(getDataFinalPagamentoFiltro()));
            entrada
            .setNrArquivoRemessaPagamento(getNumeroRemessaFiltro()
                .equals("") ? 0L : Long
                    .parseLong(getNumeroRemessaFiltro()));

            entrada
            .setCdProdutoServicoOperacao(getTipoServicoFiltro() != null ? getTipoServicoFiltro()
                : 0);
            entrada
            .setCdProdutoServicoRelacionado(getModalidadeFiltro() != null ? getModalidadeFiltro()
                : 0);
            entrada.setCdSituacaoOperacaoPagamento(0);
            entrada.setCdMotivoSituacaoPagamento(0);

            setListaGridAutorizarPagamentosConsolidado(getAutorizarPagamentosConsolidadosServiceImpl()
                .consultarPagtosConsPendAutorizacao(entrada));

            this.listaRadiosAutorizarPagamentosConsolidado = new ArrayList<SelectItem>();

            for (int i = 0; i < getListaGridAutorizarPagamentosConsolidado()
            .size(); i++) {
                this.listaRadiosAutorizarPagamentosConsolidado
                .add(new SelectItem(i, " "));
            }

            setItemSelecionadoListaAutorizarPagamentosConsolidado(null);
            setDisableArgumentosConsulta(true);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridAutorizarPagamentosConsolidado(null);
            setItemSelecionadoListaAutorizarPagamentosConsolidado(null);
            setDisableArgumentosConsulta(false);
        }
    }

    /**
     * Limpar pagamentos.
     *
     * @return the string
     */
    public String limparPagamentos() {
        setQtdePagamentos(0L);
        setVlTotalPagamentos(null);
        setDsTipoServico("");
        setModalidade("");
        return "";
    }

    /**
     * Carrega grid autorizar pagamentos consolidados parcial.
     */
    public void carregaGridAutorizarPagamentosConsolidadosParcial() {

        listaGridAutorizarPagamentosConsolidadosParcial = new ArrayList<OcorrenciasDetPagConsPendAutSaidaDTO>();

        DetalharPagtosConsPendAutorizacaoEntradaDTO entradaDTO = new DetalharPagtosConsPendAutorizacaoEntradaDTO();

        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());

        // Pagamento
        setQtdePagamentos(registroSelecionado.getQtPagamento());
        setVlTotalPagamentos(registroSelecionado.getVlEfetivoPagamentoCliente());
        setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
        setModalidade(registroSelecionado.getDsOperacaoProdutoServico());

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setNrCnpjCpf(registroSelecionado.getNrCnpjCpf());
        entradaDTO.setNrFilialcnpjCpf(registroSelecionado.getNrFilialCnpjCpf());
        entradaDTO.setNrControleCnpjCpf(registroSelecionado
            .getNrDigitoCnpjCpf());
        entradaDTO.setNrArquivoRemessaPagamento(registroSelecionado
            .getNrArquivoRemessaPagamento());
        entradaDTO.setDtCreditoPagamento(registroSelecionado
            .getDtCreditoPagamento());
        entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
        entradaDTO.setCdConta(registroSelecionado.getCdConta());
        entradaDTO.setCdDigitoConta(SiteUtil.formatNumber(registroSelecionado
            .getCdDigitoConta(), 2));
        entradaDTO.setCdProdutoServicoOperacao(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado
            .getCdSituacaoOperacaoPagamento());
        entradaDTO.setCdAgendadoPagaoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdPessoaContratoDebito(registroSelecionado
            .getCdPessoaContratoDebito());
        entradaDTO.setCdTipoContratoDebito(registroSelecionado
            .getCdTipoContratoDebito());
        entradaDTO.setNrSequenciaContratoDebito(registroSelecionado
            .getNrSequenciaContratoDebito());

        DetalharPagtosConsPendAutorizacaoSaidaDTO saidaDTO = getAutorizarPagamentosConsolidadosServiceImpl()
        .detalharPagtosConsPendAutorizacao(entradaDTO);

        setListaGridAutorizarPagamentosConsolidadosParcial(saidaDTO
            .getListaDetPagtosConsPendAutorizacao());

        // in�cio do preenchimento do cabecalho
        setCpfCnpj(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado
            .getNrCnpjCpf(), registroSelecionado.getNrFilialCnpjCpf(),
            registroSelecionado.getNrDigitoCnpjCpf()));
        setEmpresaConglomerado(registroSelecionado.getDsRazaoSocial());
        setNrContrato(registroSelecionado.getNrContratoOrigem());

        setNomeRazaoSocial(saidaDTO.getNmCliente());
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getDsSituacaoContrato());

        setBanco(PgitUtil.formatBanco(registroSelecionado.getCdBanco(),
            saidaDTO.getDsBancoDebito(), false));
        setAgencia(PgitUtil.formatAgencia(registroSelecionado.getCdAgencia(),
            registroSelecionado.getCdDigitoAgencia(), saidaDTO
            .getDsAgenciaDebito(), false));
        setConta(PgitUtil.formatConta(registroSelecionado.getCdConta(),
            registroSelecionado.getCdDigitoConta(), false));
        setTipo(saidaDTO.getDsTipoContaDebito());
        // fim do preenchimento do cabecalho

        listaControleAutorizarPagamentosConsolidadosParcial = new ArrayList<SelectItem>();
        for (int i = 0; i < getListaGridAutorizarPagamentosConsolidadosParcial()
        .size(); i++) {
            listaControleAutorizarPagamentosConsolidadosParcial
            .add(new SelectItem(i, ""));
        }
        setItemSelecionadolistaAutorizarPagamentosConsolidadosParcial(null);

    }

    /**
     * Habilitar panel botoes.
     */
    public void habilitarPanelBotoes() {
        for (int i = 0; i < getListaGridAutorizarPagamentosConsolidadosParcial()
        .size(); i++) {
            if (getListaGridAutorizarPagamentosConsolidadosParcial().get(i)
                            .isCheck()) {
                setPanelBotoes(true);
                return;
            }
        }
        setPanelBotoes(false);
    }

    /**
     * Carrega grid autorizar pagamentos consolidados integral.
     */
    public void carregaGridAutorizarPagamentosConsolidadosIntegral() {

        listaGridAutorizarPagamentosConsolidadosIntegral = new ArrayList<OcorrenciasDetPagConsPendAutSaidaDTO>();

        DetalharPagtosConsPendAutorizacaoEntradaDTO entradaDTO = new DetalharPagtosConsPendAutorizacaoEntradaDTO();
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());

        // Pagamento
        setQtdePagamentos(registroSelecionado.getQtPagamento());
        setVlTotalPagamentos(registroSelecionado.getVlEfetivoPagamentoCliente());
        setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
        setModalidade(registroSelecionado.getDsOperacaoProdutoServico());

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setNrCnpjCpf(registroSelecionado.getNrCnpjCpf());
        entradaDTO.setNrFilialcnpjCpf(registroSelecionado.getNrFilialCnpjCpf());
        entradaDTO.setNrControleCnpjCpf(registroSelecionado
            .getNrDigitoCnpjCpf());
        entradaDTO.setNrArquivoRemessaPagamento(registroSelecionado
            .getNrArquivoRemessaPagamento());
        entradaDTO.setDtCreditoPagamento(registroSelecionado
            .getDtCreditoPagamento());
        entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
        entradaDTO.setCdConta(registroSelecionado.getCdConta());
        entradaDTO.setCdDigitoConta(SiteUtil.formatNumber(registroSelecionado
            .getCdDigitoConta(), 2));
        entradaDTO.setCdProdutoServicoOperacao(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado
            .getCdSituacaoOperacaoPagamento());
        entradaDTO.setCdAgendadoPagaoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdPessoaContratoDebito(registroSelecionado
            .getCdPessoaContratoDebito());
        entradaDTO.setCdTipoContratoDebito(registroSelecionado
            .getCdTipoContratoDebito());
        entradaDTO.setNrSequenciaContratoDebito(registroSelecionado
            .getNrSequenciaContratoDebito());

        DetalharPagtosConsPendAutorizacaoSaidaDTO saidaDTO = getAutorizarPagamentosConsolidadosServiceImpl()
        .detalharPagtosConsPendAutorizacao(entradaDTO);

        setListaGridAutorizarPagamentosConsolidadosIntegral(saidaDTO
            .getListaDetPagtosConsPendAutorizacao());

        // in�cio do preenchimento do cabecalho
        setCpfCnpj(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado
            .getNrCnpjCpf(), registroSelecionado.getNrFilialCnpjCpf(),
            registroSelecionado.getNrDigitoCnpjCpf()));
        setEmpresaConglomerado(registroSelecionado.getDsRazaoSocial());
        setNrContrato(registroSelecionado.getNrContratoOrigem());

        setNomeRazaoSocial(saidaDTO.getNmCliente());
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getDsSituacaoContrato());

        setBanco(PgitUtil.formatBanco(registroSelecionado.getCdBanco(),
            saidaDTO.getDsBancoDebito(), false));
        setAgencia(PgitUtil.formatAgencia(registroSelecionado.getCdAgencia(),
            registroSelecionado.getCdDigitoAgencia(), saidaDTO
            .getDsAgenciaDebito(), false));
        setConta(PgitUtil.formatConta(registroSelecionado.getCdConta(),
            registroSelecionado.getCdDigitoConta(), false));
        setTipo(saidaDTO.getDsTipoContaDebito());
        // fim do preenchimento do cabecalho

    }

    /**
     * Habilita botao selecionar autorizar pagamentos consolidados parcial.
     */
    public void habilitaBotaoSelecionarAutorizarPagamentosConsolidadosParcial() {
        setBtnAutorizaoSelecionados(false);

        for (int i = 0; i < getListaGridAutorizarPagamentosConsolidadosParcial()
        .size(); i++) {
            if (getListaGridAutorizarPagamentosConsolidadosParcial().get(i)
                            .isCheck()) {
                setBtnAutorizaoSelecionados(true);
            }
        }
    }

    /**
     * Checa todos.
     */
    public void checaTodos() {
        setBtnAutorizaoSelecionados(isBtnChecaTodos());
        for (int i = 0; i < (getListaGridAutorizarPagamentosConsolidadosParcial()
                        .size()); i++) {
            getListaGridAutorizarPagamentosConsolidadosParcial().get(i)
            .setCheck(isBtnChecaTodos());
        }
    }

    /**
     * Pesquisar aut pagamentos consolidados parcial.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarAutPagamentosConsolidadosParcial(ActionEvent evt) {
        try {
            carregaGridAutorizarPagamentosConsolidadosParcial();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridAutorizarPagamentosConsolidadosParcial(null);
            setItemSelecionadolistaAutorizarPagamentosConsolidadosParcial(null);
        }
        return "";
    }

    /**
     * Pesquisar parcial selecionados.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarParcialSelecionados(ActionEvent evt) {
        return "";
    }

    /**
     * Pesquisar aut pagamentos consolidados integral.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarAutPagamentosConsolidadosIntegral(ActionEvent evt) {

        try {
            carregaGridAutorizarPagamentosConsolidadosIntegral();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridAutorizarPagamentosConsolidadosIntegral(null);
            return "";
        }
        return "";
    }

    /**
     * Autorizar selecionados.
     *
     * @return the string
     */
    public String autorizarSelecionados() {

        BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
            new PDCDataBean());

        vlTotalPagamentosAutorizados = BigDecimal.ZERO;
        listaGridAutorizarPagamentosConsolidadosSelecionados = new ArrayList<OcorrenciasDetPagConsPendAutSaidaDTO>();

        for (int i = 0; i < (getListaGridAutorizarPagamentosConsolidadosParcial()
                        .size()); i++) {
            if (getListaGridAutorizarPagamentosConsolidadosParcial().get(i)
                            .isCheck()) {
                listaGridAutorizarPagamentosConsolidadosSelecionados
                .add(getListaGridAutorizarPagamentosConsolidadosParcial()
                    .get(i));
                this.vlTotalPagamentosAutorizados = this.vlTotalPagamentosAutorizados
                .add(getListaGridAutorizarPagamentosConsolidadosParcial()
                    .get(i).getVlPagamento());

            }
        }

        Long totalQtAutorizado = 0L;

        for (int i = 0; i < getListaGridAutorizarPagamentosConsolidado().size(); i++) {
            totalQtAutorizado = totalQtAutorizado
            + getListaGridAutorizarPagamentosConsolidado().get(i)
            .getQtPagamento();
        }

        setQtdePagamentosAutorizados(Long
            .parseLong(String
                .valueOf(getListaGridAutorizarPagamentosConsolidadosSelecionados()
                    .size())));

        setVlTotalPagamentosAutorizados(getVlTotalPagamentosAutorizados());

        return "CONFIRMAR_PAGAMENTO_SELECIONADOS";
    }

    // os registros selecionados s�o desmarcados -
    // autorizarPagamentosConsolidadosParcial
    /**
     * Pesquisar autorizar pagtos consolidados.
     *
     * @param evt the evt
     */
    public void pesquisarAutorizarPagtosConsolidados(ActionEvent evt) {
        for (int i = 0; i < listaGridAutorizarPagamentosConsolidadosParcial
        .size(); i++) {
            listaGridAutorizarPagamentosConsolidadosParcial.get(i).setCheck(
                false);
        }
        // setOpcaoChecarTodos(false);

    }

    /**
     * Confirmar pagamento selecionados.
     *
     * @return the string
     */
    public String confirmarPagamentoSelecionados() {

        try {
            AutorizarParcialEntradaDTO ocorrencia;
            List<AutorizarParcialEntradaDTO> listaOcorrenciasAutorizarParcial = new ArrayList<AutorizarParcialEntradaDTO>();
            ConsultarPagtosConsPendAutorizacaoSaidaDTO registroSelecionado = getListaGridAutorizarPagamentosConsolidado()
            .get(
                getItemSelecionadoListaAutorizarPagamentosConsolidado());

            for (int i = 0; i < (getListaGridAutorizarPagamentosConsolidadosSelecionados()
                            .size()); i++) {
                ocorrencia = new AutorizarParcialEntradaDTO();

                // Preenchidos com informa��es vindas da tela Autorizar
                // Pagamentos Consolidados
                ocorrencia.setCdPessoaJuridicaContrato(registroSelecionado
                    .getCdPessoaJuridicaContrato());
                ocorrencia.setCdTipoContratoNegocio(registroSelecionado
                    .getCdTipoContratoNegocio());
                ocorrencia.setNrSequenciaContratoNegocio(registroSelecionado
                    .getNrContratoOrigem());
                ocorrencia.setCdProdutoServicoOperacao(registroSelecionado
                    .getCdProdutoServicoOperacao());
                ocorrencia.setCdOperacaoProdutoServico(registroSelecionado
                    .getCdProdutoServicoRelacionado());
                // Preenchidos com informa��es vindas da tela Autorizar
                // Pagamentos Consolidados Parcial
                ocorrencia
                .setCdTipoCanal(getListaGridAutorizarPagamentosConsolidadosSelecionados()
                    .get(i).getCdTipoCanal());
                ocorrencia
                .setCdControlePagamento(getListaGridAutorizarPagamentosConsolidadosSelecionados()
                    .get(i).getNrPagamento());

                listaOcorrenciasAutorizarParcial.add(ocorrencia);
            }

            AutorizarParcialSaidaDTO saida = getAutorizarPagamentosConsolidadosServiceImpl()
            .autorizarParcial(listaOcorrenciasAutorizarParcial);
            BradescoFacesUtils
            .addInfoModalMessage(
                "(" + saida.getCodMensagem() + ") "
                + saida.getMensagem(),
                "autorizarPagamentosConsolidados",
                "#{autorizarPagamentosConsolidadosBean.carregaListaAutorizarPagamentosConsolidados}",
                false);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }

        return "";

    }

    /**
     * Confirmar pagamento integral.
     *
     * @return the string
     */
    public String confirmarPagamentoIntegral() {

        try {

            AutorizarIntegralEntradaDTO entradaDTO = new AutorizarIntegralEntradaDTO();
            ConsultarPagtosConsPendAutorizacaoSaidaDTO registroSelecionado = getListaGridAutorizarPagamentosConsolidado()
            .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());

            // Pagamento 
            setQtdePagamentos(formataStringEmLong(registroSelecionado.getQtPagamentoFormatado()));
            setVlTotalPagamentos(registroSelecionado.getVlEfetivoPagamentoCliente());

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
            entradaDTO.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
            entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
            entradaDTO.setCdCorpoCnpjCpf(registroSelecionado.getNrCnpjCpf());
            entradaDTO.setCdFilialCnpjCpf(registroSelecionado.getNrFilialCnpjCpf());
            entradaDTO.setCdControleCnpjCpf(registroSelecionado.getNrDigitoCnpjCpf());
            entradaDTO.setNrSequenciaArquivoRemessa(registroSelecionado.getNrArquivoRemessaPagamento());
            entradaDTO.setHrInclusaoRemessaSistema("");
            entradaDTO.setDtAgendamentoPagamento(registroSelecionado.getDtCreditoPagamento());
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
            entradaDTO.setCdSituacao(registroSelecionado.getCdSituacaoOperacaoPagamento());

            AutorizarIntegralSaidaDTO saidaDTO = getAutorizarPagamentosConsolidadosServiceImpl()
            .autorizarIntegral(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(), "autorizarPagamentosConsolidados",
                "#{autorizarPagamentosConsolidadosBean.carregaListaAutorizarPagamentosConsolidados}", false);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " 
                + p.getMessage(), false);
            return null;
        }

        return "";

    }
    
    private Long formataStringEmLong(String valor){
        String valorFormatado = valor.replaceAll(".", "");
        return isStringNullLong(valorFormatado.replaceAll(",", ""));
        
    }

    /**
     * Carrega lista lista det aut pagamentos consolidados.
     */
    public void carregaListaListaDetAutPagamentosConsolidados() {

        listaDetAutPagamentosConsolidados = new ArrayList<OcorrenciasDetPagConsPendAutSaidaDTO>();

        DetalharPagtosConsPendAutorizacaoEntradaDTO entradaDTO = new DetalharPagtosConsPendAutorizacaoEntradaDTO();

        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());

        // Pagamento
        setQtdePagamentos(registroSelecionado.getQtPagamento());
        setVlTotalPagamentos(registroSelecionado.getVlEfetivoPagamentoCliente());

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setNrCnpjCpf(registroSelecionado.getNrCnpjCpf());
        entradaDTO.setNrFilialcnpjCpf(registroSelecionado.getNrFilialCnpjCpf());
        entradaDTO.setNrControleCnpjCpf(registroSelecionado
            .getNrDigitoCnpjCpf());
        entradaDTO.setNrArquivoRemessaPagamento(registroSelecionado
            .getNrArquivoRemessaPagamento());
        entradaDTO.setDtCreditoPagamento(registroSelecionado
            .getDtCreditoPagamento());
        entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
        entradaDTO.setCdConta(registroSelecionado.getCdConta());
        entradaDTO.setCdDigitoConta(SiteUtil.formatNumber(registroSelecionado
            .getCdDigitoConta(), 2));
        entradaDTO.setCdProdutoServicoOperacao(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado
            .getCdSituacaoOperacaoPagamento());
        entradaDTO.setCdAgendadoPagaoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdPessoaContratoDebito(registroSelecionado
            .getCdPessoaContratoDebito());
        entradaDTO.setCdTipoContratoDebito(registroSelecionado
            .getCdTipoContratoDebito());
        entradaDTO.setNrSequenciaContratoDebito(registroSelecionado
            .getNrSequenciaContratoDebito());

        DetalharPagtosConsPendAutorizacaoSaidaDTO saidaDTO = getAutorizarPagamentosConsolidadosServiceImpl()
        .detalharPagtosConsPendAutorizacao(entradaDTO);

        setListaDetAutPagamentosConsolidados(saidaDTO
            .getListaDetPagtosConsPendAutorizacao());

        // in�cio do preenchimento do cabecalho
        setCpfCnpj(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado
            .getNrCnpjCpf(), registroSelecionado.getNrFilialCnpjCpf(),
            registroSelecionado.getNrDigitoCnpjCpf()));
        setEmpresaConglomerado(registroSelecionado.getDsRazaoSocial());
        setNrContrato(registroSelecionado.getNrContratoOrigem());

        setNomeRazaoSocial(saidaDTO.getNmCliente());
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getDsSituacaoContrato());

        setBanco(PgitUtil.formatBanco(registroSelecionado.getCdBanco(),
            saidaDTO.getDsBancoDebito(), false));
        setAgencia(PgitUtil.formatAgencia(registroSelecionado.getCdAgencia(),
            registroSelecionado.getCdDigitoAgencia(), saidaDTO
            .getDsAgenciaDebito(), false));
        setConta(PgitUtil.formatConta(registroSelecionado.getCdConta(),
            registroSelecionado.getCdDigitoConta(), false));
        setTipo(saidaDTO.getDsTipoContaDebito());
        setDtPagamento(registroSelecionado.getDtCreditoPagamento());
        setNrRemessa(registroSelecionado.getNrArquivoRemessaPagamento());
        // fim do preenchimento do cabecalho

        listaControleDetAutPagamentosConsolidados = new ArrayList<SelectItem>();
        for (int i = 0; i < getListaDetAutPagamentosConsolidados().size(); i++) {
            listaControleDetAutPagamentosConsolidados
            .add(new SelectItem(i, ""));
        }
        setItemSelecionadoListaDetAutPagamentosConsolidados(null);

    }

    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {

        try {
            OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
            .get(getItemSelecionadoListaDetAutPagamentosConsolidados());

            switch (registroSelecionado.getCdTipoTela()) {
                case 1:
                    preencheDadosPagtoCreditoConta();
                    return "DETALHE_CREDITO_CONTA";
                case 2:
                    preencheDadosPagtoDebitoConta();
                    return "DETALHE_DEBITO_CONTA";
                case 3:
                    preencheDadosPagtoDOC();
                    return "DETALHE_DOC";
                case 4:
                    preencheDadosPagtoTED();
                    return "DETALHE_TED";
                case 5:
                    preencheDadosPagtoOrdemPagto();
                    return "DETALHE_ORDEM_PAGTO";
                case 6:
                    preencheDadosPagtoTituloBradesco();
                    return "DETALHE_TITULOS_BRADESCO";
                case 7:
                    preencheDadosPagtoTituloOutrosBancos();
                    return "DETALHE_TITULOS_OUTROS_BANCOS";
                case 8:
                    preencheDadosPagtoDARF();
                    return "DETALHE_DARF";
                case 9:
                    preencheDadosPagtoGARE();
                    return "DETALHE_GARE";
                case 10:
                    preencheDadosPagtoGPS();
                    return "DETALHE_GPS";
                case 11:
                    preencheDadosPagtoCodigoBarras();
                    return "DETALHE_CODIGO_BARRAS";
                case 12:
                    preencheDadosPagtoDebitoVeiculos();
                    return "DETALHE_DEBITO_VEICULOS";
                case 13:
                    preencheDadosPagtoDepIdentificado();
                    return "DETALHE_DEPOSITO_IDENTIFICADO";
                case 14:
                    preencheDadosPagtoOrdemCredito();
                    return "DETALHE_ORDEM_CREDITO";
                default:
                    return "";
            }
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }
    }

    /**
     * Voltar detalhe.
     *
     * @return the string
     */
    public String voltarDetalhe() {
        setItemSelecionadoListaDetAutPagamentosConsolidados(null);

        return "VOLTAR";
    }

    /**
     * Limpar variaveis.
     */
    public void limparVariaveis() {
        setCpfCnpj(null);
        setNomeRazaoSocial("");
        setEmpresaConglomerado("");
        setNrContrato(null);
        setDsContratacao("");
        setSituacao("");
        setBanco("");
        setAgencia("");
        setConta(null);
        setTipo("");
    }

    /**
     * Limpar campos favorecido beneficiario.
     */
    public void limparCamposFavorecidoBeneficiario() {
        setNumeroInscricaoFavorecido("");
        setTipoFavorecido(0);
        setDsFavorecido("");
        setCdFavorecido("");
        setDsBancoFavorecido("");
        setDsAgenciaFavorecido("");
        setDsContaFavorecido("");
        setDsTipoContaFavorecido("");
        setNumeroInscricaoBeneficiario("");
        setCdTipoBeneficiario(0);
        setNomeBeneficiario("");
        setDtNascimentoBeneficiario("");
        setDsTipoBeneficiario("");
    }

    /**
     * Preenche dados pagto credito conta.
     */
    public void preencheDadosPagtoCreditoConta() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCreditoConta(entradaDTO);

        limparCamposFavorecidoBeneficiario();

        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(saidaDTO.getNrSequenciaContratoNegocio() == null
            || saidaDTO.getNrSequenciaContratoNegocio().equals("") ? null
                : Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());
        setBanco(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO
            .getDsBancoDebito(), false));
        setAgencia(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(),
            saidaDTO.getCdDigAgenciaDebto(), saidaDTO.getDsAgenciaDebito(),
            false));
        setConta(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO
            .getDsDigAgenciaDebito(), false));
        setTipo(saidaDTO.getDsTipoContaDebito());

        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido());
            /*
             * Preenchendo banco/ag�ncia/conta da parte de favorecidos com os
             * dados da lista da tela anterior (campos Banco/ag�ncia/conta de
             * Cr�dito) conforme solicitado por Fabio De Almeida via email
             * ter�a-feira, 15 de fevereiro de 2011
             */
            setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO
                .getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
                false));
            setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
                .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
                saidaDTO.getDsAgenciaFavorecido(), false));
            setDsContaFavorecido(PgitUtil.formatConta(saidaDTO
                .getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
                false));
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (PgitUtil.verificaIntegerNulo(getCdTipoBeneficiario()) != 0)
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(
                    getDtNascimentoBeneficiario()).equals("");
            setExibeDcom(false);

        }

        // Pagamento
        setCdSituacaoTranferenciaAutomatica(saidaDTO
            .getCdSituacaoTranferenciaAutomatica());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
        setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        //Favorecidos
        setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
        setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
        setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
        setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
        setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal() + " - " + saidaDTO.getDsAgenciaOriginal());
        setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
        setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
    }

    /**
     * Preenche dados pagto debito conta.
     */
    public void preencheDadosPagtoDebitoConta() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoConta(entradaDTO);

        // Cabe�alho - Cliente
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.valueOf(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // Sempre Carregar Conta de D�bito
        setBanco(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO
            .getDsBancoDebito(), false));
        setAgencia(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(),
            saidaDTO.getCdDigAgenciaDebto(), saidaDTO.getDsAgenciaDebito(),
            false));
        setConta(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO
            .getDsDigAgenciaDebito(), false));
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido(), saidaDTO
            .getNmInscriFavorecido()));
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
            saidaDTO.getCdDigContaCredito(), false));
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());


        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto doc.
     */
    public void preencheDadosPagtoDOC() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDOC(entradaDTO);

        limparCamposFavorecidoBeneficiario();

        // Cabe�alho
        setCpfCnpj(saidaDTO.getCdCpfCnpjCliente() == null
            || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(saidaDTO.getNrSequenciaContratoNegocio() == null
            || saidaDTO.getNrSequenciaContratoNegocio().equals("") ? null
                : Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido());
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
        setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
        setIdentificacaoTransferenciaDoc(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto ted.
     */
    public void preencheDadosPagtoTED() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTED(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.valueOf(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();

        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaDTO
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido());
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
        setFinalidadeTed(PgitUtil.concatenarCampos(saidaDTO.getCdFinalidadeTed() , saidaDTO.getDsFinalidadeDocTed() , " - "));
        setIdentificacaoTransferenciaTed(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatidoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlIrCredito());
        setVlIss(saidaDTO.getVlIssCredito());
        setVlIof(saidaDTO.getVlIofCredito());
        setVlInss(saidaDTO.getVlInssCredito());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto ordem pagto.
     */
    public void preencheDadosPagtoOrdemPagto() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoOrdemPagto(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.valueOf(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();

        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaDTO
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido());
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setNumeroOp(saidaDTO.getNrOperacao());
        setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
        setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
        setNumeroCheque(saidaDTO.getNrCheque());
        setSerieCheque(saidaDTO.getNrSerieCheque());

        setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());
        setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
        setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());

        setDataRetirada(saidaDTO.getDtRetirada());
        setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
        setVlDesconto(saidaDTO.getVlDescontoCredito());
        setVlAbatimento(saidaDTO.getVlAbatidoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlIrCredito());
        setVlIss(saidaDTO.getVlIssCredito());
        setVlIof(saidaDTO.getVlIofCredito());
        setVlInss(saidaDTO.getVlInssCredito());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto titulo bradesco.
     */
    public void preencheDadosPagtoTituloBradesco() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloBradesco(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.valueOf(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
                            .getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto titulo outros bancos.
     */
    public void preencheDadosPagtoTituloOutrosBancos() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloOutrosBancos(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.valueOf(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao((saidaDTO.getDsContrato()));
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
                            .getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto darf.
     */
    public void preencheDadosPagtoDARF() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDARF(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.valueOf(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getCdTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdDanoExercicio());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
        setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto gare.
     */
    public void preencheDadosPagtoGARE() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoGARE(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
                            .getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
        setInscricaoEstadualContribuinte(saidaDTO
            .getCdInscricaoEstadualContribuinte());
        setCodigoReceita(saidaDTO.getCdReceita());
        setNumeroReferencia(saidaDTO.getNrReferencia());
        setNumeroCotaParcela(saidaDTO.getNrCota());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
        setDataReferencia(saidaDTO.getDtReferencia());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcela());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto gps.
     */
    public void preencheDadosPagtoGPS() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoGPS(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDataCompetencia(saidaDTO.getDsMesAnoCompt());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
        setCodigoInss(saidaDTO.getCdInss());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto codigo barras.
     */
    public void preencheDadosPagtoCodigoBarras() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCodigoBarras(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getNrTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
        setCodigoTributo(saidaDTO.getCdTributoArrecadado());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
        setPercentualReceita(saidaDTO.getCdPercentualTributo());
        setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setDataCompetencia(saidaDTO.getDtCompensacao());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsObservacao());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto debito veiculos.
     */
    public void preencheDadosPagtoDebitoVeiculos() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoVeiculos(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
                            .getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
        setAnoExercicio(saidaDTO.getDtAnoExercTributo());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
        setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
        setUfTributo(saidaDTO.getCdUfTributo());
        setNumeroNsu(saidaDTO.getNrNsuTributo());
        setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
        setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
        setVlPrincipal(saidaDTO.getVlPrincipalTributo());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto dep identificado.
     */
    public void preencheDadosPagtoDepIdentificado() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDepIdentificado(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
                            .getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
        setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
        setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
        setSequenciaProdutoDepositoIdentificado(saidaDTO
            .getCdSequenciaProdutoDepositoId());
        setBancoCartaoDepositoIdentificado(saidaDTO
            .getCdBancoCartaoDepositanteId());
        setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
        setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
        setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
        setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
        setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto ordem credito.
     */
    public void preencheDadosPagtoOrdemCredito() {
        OcorrenciasDetPagConsPendAutSaidaDTO registroSelecionado = getListaDetAutPagamentosConsolidados()
        .get(getItemSelecionadoListaDetAutPagamentosConsolidados());
        ConsultarPagtosConsPendAutorizacaoSaidaDTO registroAnteriorSelecionado = getListaGridAutorizarPagamentosConsolidado()
        .get(getItemSelecionadoListaAutorizarPagamentosConsolidado());
        DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento("");
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoOrdemCredito(entradaDTO);

        // CLIENTE
        setCpfCnpj(saidaDTO.getCpfCnpjClienteFormatado());
        setNomeRazaoSocial(saidaDTO.getNomeCliente());
        setEmpresaConglomerado(saidaDTO.getDsPessoaJuridicaContrato());
        setNrContrato(Long.parseLong(saidaDTO.getNrSequenciaContratoNegocio()));
        setDsContratacao(saidaDTO.getDsContrato());
        setSituacao(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setBanco(saidaDTO.getBancoDebitoFormatado());
        setAgencia(saidaDTO.getAgenciaDebitoFormatada());
        setConta(saidaDTO.getContaDebitoFormatada());
        setTipo(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil
            .validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null
            || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
                            .getCdFavorecido()));
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCodigoPagador(saidaDTO.getCdPagador());
        setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Inicio Relat�rio
    // parcial
    /**
     * Imprimir autorizar pagto parcial.
     */
    public void imprimirAutorizarPagtoParcial() {

        try {

            String caminho = "/images/Bradesco_logo.JPG";
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext
            .getExternalContext().getContext();
            logoPath = servletContext.getRealPath(caminho);

            // Par�metros do Relat�rio
            Map<String, Object> par = new HashMap<String, Object>();
            par.put("titulo", "Autorizar Pagamentos Consolidados");

            par.put("cpfCnpj", getCpfCnpj());
            par.put("nomeRazaoSocial", getNomeRazaoSocial());
            par.put("empresaConglomerado", getEmpresaConglomerado());
            par.put("nrContrato", getNrContrato());
            par.put("dsContratacao", getDsContratacao());
            par.put("situacao", getSituacao());
            par.put("banco", getBanco());
            par.put("agencia", getAgencia());
            par.put("conta", getConta());
            par.put("tipo", getTipo());
            par.put("logoBradesco", getLogoPath());

            par.put("qtdePagamentos", getQtdePagamentos());
            par.put("vlTotalPagamentos", getVlTotalPagamentos());
            par
            .put("qtdePagamentosAutorizados",
                getQtdePagamentosAutorizados());
            par.put("vlTotalPagamentosAutorizados",
                getVlTotalPagamentosAutorizados());

            try {

                // M�todo que gera o relat�rio PDF.
                PgitUtil
                .geraRelatorioPdf(
                    "/relatorios/agendamentoEfetivacaoEstorno/autorizarDesautorizarPagamentos/autorizarPagamentosConsolidados/parcial",
                    "imprimirAutorizarPagtoConsolidadoParcial",
                    getListaGridAutorizarPagamentosConsolidadosSelecionados(),
                    par);

            } /* Exce��o do relat�rio */
            catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "",
                    BradescoViewExceptionActionType.ACTION);
            }

        } /* Exce��o do PDC */
        catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }
    }

    // integral
    /**
     * Imprimir autorizar pagto integral.
     */
    public void imprimirAutorizarPagtoIntegral() {
        try {

            String caminho = "/images/Bradesco_logo.JPG";
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext
            .getExternalContext().getContext();
            logoPath = servletContext.getRealPath(caminho);

            // Par�metros do Relat�rio
            Map<String, Object> par = new HashMap<String, Object>();
            par.put("titulo", "Autorizar Pagamentos Consolidados");

            par.put("cpfCnpj", getCpfCnpj());
            par.put("nomeRazaoSocial", getNomeRazaoSocial());
            par.put("empresaConglomerado", getEmpresaConglomerado());
            par.put("nrContrato", getNrContrato());
            par.put("dsContratacao", getDsContratacao());
            par.put("situacao", getSituacao());
            par.put("banco", getBanco());
            par.put("agencia", getAgencia());
            par.put("conta", getConta());
            par.put("tipo", getTipo());
            par.put("logoBradesco", getLogoPath());

            par.put("qtdePagamentos", getQtdePagamentos());
            par.put("vlTotalPagamentos", getVlTotalPagamentos());
            par
            .put("qtdePagamentosAutorizados",
                getQtdePagamentosAutorizados());
            par.put("vlTotalPagamentosAutorizados",
                getVlTotalPagamentosAutorizados());

            try {

                // M�todo que gera o relat�rio PDF.
                PgitUtil
                .geraRelatorioPdf(
                    "/relatorios/agendamentoEfetivacaoEstorno/autorizarDesautorizarPagamentos/autorizarPagamentosConsolidados/integral",
                    "imprimirAutorizarPagtoConsolidadoIntegral",
                    getListaGridAutorizarPagamentosConsolidadosIntegral(),
                    par);

            } /* Exce��o do relat�rio */
            catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "",
                    BradescoViewExceptionActionType.ACTION);
            }

        } /* Exce��o do PDC */
        catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }
    }

    // Getter's e Setter's

    /**
     * Get: agencia.
     *
     * @return agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Set: agencia.
     *
     * @param agencia the agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Get: agenciaDigitoCredito.
     *
     * @return agenciaDigitoCredito
     */
    public String getAgenciaDigitoCredito() {
        return agenciaDigitoCredito;
    }

    /**
     * Set: agenciaDigitoCredito.
     *
     * @param agenciaDigitoCredito the agencia digito credito
     */
    public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
        this.agenciaDigitoCredito = agenciaDigitoCredito;
    }

    /**
     * Get: agenciaDigitoDestino.
     *
     * @return agenciaDigitoDestino
     */
    public String getAgenciaDigitoDestino() {
        return agenciaDigitoDestino;
    }

    /**
     * Set: agenciaDigitoDestino.
     *
     * @param agenciaDigitoDestino the agencia digito destino
     */
    public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
        this.agenciaDigitoDestino = agenciaDigitoDestino;
    }

    /**
     * Get: agenteArrecadador.
     *
     * @return agenteArrecadador
     */
    public String getAgenteArrecadador() {
        return agenteArrecadador;
    }

    /**
     * Set: agenteArrecadador.
     *
     * @param agenteArrecadador the agente arrecadador
     */
    public void setAgenteArrecadador(String agenteArrecadador) {
        this.agenteArrecadador = agenteArrecadador;
    }

    /**
     * Get: anoExercicio.
     *
     * @return anoExercicio
     */
    public String getAnoExercicio() {
        return anoExercicio;
    }

    /**
     * Set: anoExercicio.
     *
     * @param anoExercicio the ano exercicio
     */
    public void setAnoExercicio(String anoExercicio) {
        this.anoExercicio = anoExercicio;
    }

    /**
     * Get: autorizarPagamentosConsolidadosServiceImpl.
     *
     * @return autorizarPagamentosConsolidadosServiceImpl
     */
    public IAutorizarPagamentosConsolidadosService getAutorizarPagamentosConsolidadosServiceImpl() {
        return autorizarPagamentosConsolidadosServiceImpl;
    }

    /**
     * Set: autorizarPagamentosConsolidadosServiceImpl.
     *
     * @param autorizarPagamentosConsolidadosServiceImpl the autorizar pagamentos consolidados service impl
     */
    public void setAutorizarPagamentosConsolidadosServiceImpl(
        IAutorizarPagamentosConsolidadosService autorizarPagamentosConsolidadosServiceImpl) {
        this.autorizarPagamentosConsolidadosServiceImpl = autorizarPagamentosConsolidadosServiceImpl;
    }

    /**
     * Get: banco.
     *
     * @return banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Set: banco.
     *
     * @param banco the banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * Get: bancoCartaoDepositoIdentificado.
     *
     * @return bancoCartaoDepositoIdentificado
     */
    public String getBancoCartaoDepositoIdentificado() {
        return bancoCartaoDepositoIdentificado;
    }

    /**
     * Set: bancoCartaoDepositoIdentificado.
     *
     * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
     */
    public void setBancoCartaoDepositoIdentificado(
        String bancoCartaoDepositoIdentificado) {
        this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
    }

    /**
     * Get: bimCartaoDepositoIdentificado.
     *
     * @return bimCartaoDepositoIdentificado
     */
    public String getBimCartaoDepositoIdentificado() {
        return bimCartaoDepositoIdentificado;
    }

    /**
     * Set: bimCartaoDepositoIdentificado.
     *
     * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
     */
    public void setBimCartaoDepositoIdentificado(
        String bimCartaoDepositoIdentificado) {
        this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
    }

    /**
     * Is btn autorizao selecionados.
     *
     * @return true, if is btn autorizao selecionados
     */
    public boolean isBtnAutorizaoSelecionados() {
        return btnAutorizaoSelecionados;
    }

    /**
     * Set: btnAutorizaoSelecionados.
     *
     * @param btnAutorizaoSelecionados the btn autorizao selecionados
     */
    public void setBtnAutorizaoSelecionados(boolean btnAutorizaoSelecionados) {
        this.btnAutorizaoSelecionados = btnAutorizaoSelecionados;
    }

    /**
     * Is btn checa todos.
     *
     * @return true, if is btn checa todos
     */
    public boolean isBtnChecaTodos() {
        return btnChecaTodos;
    }

    /**
     * Set: btnChecaTodos.
     *
     * @param btnChecaTodos the btn checa todos
     */
    public void setBtnChecaTodos(boolean btnChecaTodos) {
        this.btnChecaTodos = btnChecaTodos;
    }

    /**
     * Get: camaraCentralizadora.
     *
     * @return camaraCentralizadora
     */
    public String getCamaraCentralizadora() {
        return camaraCentralizadora;
    }

    /**
     * Set: camaraCentralizadora.
     *
     * @param camaraCentralizadora the camara centralizadora
     */
    public void setCamaraCentralizadora(String camaraCentralizadora) {
        this.camaraCentralizadora = camaraCentralizadora;
    }

    /**
     * Get: cartaoDepositoIdentificado.
     *
     * @return cartaoDepositoIdentificado
     */
    public String getCartaoDepositoIdentificado() {
        return cartaoDepositoIdentificado;
    }

    /**
     * Set: cartaoDepositoIdentificado.
     *
     * @param cartaoDepositoIdentificado the cartao deposito identificado
     */
    public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
        this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public String getCdBancoCredito() {
        return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(String cdBancoCredito) {
        this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: cdBancoDestino.
     *
     * @return cdBancoDestino
     */
    public String getCdBancoDestino() {
        return cdBancoDestino;
    }

    /**
     * Set: cdBancoDestino.
     *
     * @param cdBancoDestino the cd banco destino
     */
    public void setCdBancoDestino(String cdBancoDestino) {
        this.cdBancoDestino = cdBancoDestino;
    }

    /**
     * Get: cdFavorecido.
     *
     * @return cdFavorecido
     */
    public String getCdFavorecido() {
        return cdFavorecido;
    }

    /**
     * Set: cdFavorecido.
     *
     * @param cdFavorecido the cd favorecido
     */
    public void setCdFavorecido(String cdFavorecido) {
        this.cdFavorecido = cdFavorecido;
    }

    /**
     * Get: cdIndicadorEconomicoMoeda.
     *
     * @return cdIndicadorEconomicoMoeda
     */
    public String getCdIndicadorEconomicoMoeda() {
        return cdIndicadorEconomicoMoeda;
    }

    /**
     * Set: cdIndicadorEconomicoMoeda.
     *
     * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
     */
    public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
        this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
    }

    /**
     * Get: cdListaDebito.
     *
     * @return cdListaDebito
     */
    public String getCdListaDebito() {
        return cdListaDebito;
    }

    /**
     * Set: cdListaDebito.
     *
     * @param cdListaDebito the cd lista debito
     */
    public void setCdListaDebito(String cdListaDebito) {
        this.cdListaDebito = cdListaDebito;
    }

    /**
     * Get: cdSerieDocumento.
     *
     * @return cdSerieDocumento
     */
    public String getCdSerieDocumento() {
        return cdSerieDocumento;
    }

    /**
     * Set: cdSerieDocumento.
     *
     * @param cdSerieDocumento the cd serie documento
     */
    public void setCdSerieDocumento(String cdSerieDocumento) {
        this.cdSerieDocumento = cdSerieDocumento;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public String getCdSituacaoOperacaoPagamento() {
        return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(
        String cdSituacaoOperacaoPagamento) {
        this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoBeneficiario.
     *
     * @return cdTipoBeneficiario
     */
    public Integer getCdTipoBeneficiario() {
        return cdTipoBeneficiario;
    }

    /**
     * Set: cdTipoBeneficiario.
     *
     * @param cdTipoBeneficiario the cd tipo beneficiario
     */
    public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
        this.cdTipoBeneficiario = cdTipoBeneficiario;
    }

    /**
     * Is chk modalidade.
     *
     * @return true, if is chk modalidade
     */
    public boolean isChkModalidade() {
        return chkModalidade;
    }

    /**
     * Set: chkModalidade.
     *
     * @param chkModalidade the chk modalidade
     */
    public void setChkModalidade(boolean chkModalidade) {
        this.chkModalidade = chkModalidade;
    }

    /**
     * Is chk servico.
     *
     * @return true, if is chk servico
     */
    public boolean isChkServico() {
        return chkServico;
    }

    /**
     * Set: chkServico.
     *
     * @param chkServico the chk servico
     */
    public void setChkServico(boolean chkServico) {
        this.chkServico = chkServico;
    }

    /**
     * Get: clienteDepositoIdentificado.
     *
     * @return clienteDepositoIdentificado
     */
    public String getClienteDepositoIdentificado() {
        return clienteDepositoIdentificado;
    }

    /**
     * Set: clienteDepositoIdentificado.
     *
     * @param clienteDepositoIdentificado the cliente deposito identificado
     */
    public void setClienteDepositoIdentificado(
        String clienteDepositoIdentificado) {
        this.clienteDepositoIdentificado = clienteDepositoIdentificado;
    }

    /**
     * Get: codigoBarras.
     *
     * @return codigoBarras
     */
    public String getCodigoBarras() {
        return codigoBarras;
    }

    /**
     * Set: codigoBarras.
     *
     * @param codigoBarras the codigo barras
     */
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    /**
     * Get: codigoCnae.
     *
     * @return codigoCnae
     */
    public String getCodigoCnae() {
        return codigoCnae;
    }

    /**
     * Set: codigoCnae.
     *
     * @param codigoCnae the codigo cnae
     */
    public void setCodigoCnae(String codigoCnae) {
        this.codigoCnae = codigoCnae;
    }

    /**
     * Get: codigoDepositante.
     *
     * @return codigoDepositante
     */
    public String getCodigoDepositante() {
        return codigoDepositante;
    }

    /**
     * Set: codigoDepositante.
     *
     * @param codigoDepositante the codigo depositante
     */
    public void setCodigoDepositante(String codigoDepositante) {
        this.codigoDepositante = codigoDepositante;
    }

    /**
     * Get: codigoInss.
     *
     * @return codigoInss
     */
    public String getCodigoInss() {
        return codigoInss;
    }

    /**
     * Set: codigoInss.
     *
     * @param codigoInss the codigo inss
     */
    public void setCodigoInss(String codigoInss) {
        this.codigoInss = codigoInss;
    }

    /**
     * Get: codigoOrdemCredito.
     *
     * @return codigoOrdemCredito
     */
    public String getCodigoOrdemCredito() {
        return codigoOrdemCredito;
    }

    /**
     * Set: codigoOrdemCredito.
     *
     * @param codigoOrdemCredito the codigo ordem credito
     */
    public void setCodigoOrdemCredito(String codigoOrdemCredito) {
        this.codigoOrdemCredito = codigoOrdemCredito;
    }

    /**
     * Get: codigoOrgaoFavorecido.
     *
     * @return codigoOrgaoFavorecido
     */
    public String getCodigoOrgaoFavorecido() {
        return codigoOrgaoFavorecido;
    }

    /**
     * Set: codigoOrgaoFavorecido.
     *
     * @param codigoOrgaoFavorecido the codigo orgao favorecido
     */
    public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
        this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
    }

    /**
     * Get: codigoPagador.
     *
     * @return codigoPagador
     */
    public String getCodigoPagador() {
        return codigoPagador;
    }

    /**
     * Set: codigoPagador.
     *
     * @param codigoPagador the codigo pagador
     */
    public void setCodigoPagador(String codigoPagador) {
        this.codigoPagador = codigoPagador;
    }

    /**
     * Get: codigoReceita.
     *
     * @return codigoReceita
     */
    public String getCodigoReceita() {
        return codigoReceita;
    }

    /**
     * Set: codigoReceita.
     *
     * @param codigoReceita the codigo receita
     */
    public void setCodigoReceita(String codigoReceita) {
        this.codigoReceita = codigoReceita;
    }

    /**
     * Get: codigoRenavam.
     *
     * @return codigoRenavam
     */
    public String getCodigoRenavam() {
        return codigoRenavam;
    }

    /**
     * Set: codigoRenavam.
     *
     * @param codigoRenavam the codigo renavam
     */
    public void setCodigoRenavam(String codigoRenavam) {
        this.codigoRenavam = codigoRenavam;
    }

    /**
     * Get: codigoTributo.
     *
     * @return codigoTributo
     */
    public String getCodigoTributo() {
        return codigoTributo;
    }

    /**
     * Set: codigoTributo.
     *
     * @param codigoTributo the codigo tributo
     */
    public void setCodigoTributo(String codigoTributo) {
        this.codigoTributo = codigoTributo;
    }

    /**
     * Get: codigoUfFavorecida.
     *
     * @return codigoUfFavorecida
     */
    public String getCodigoUfFavorecida() {
        return codigoUfFavorecida;
    }

    /**
     * Set: codigoUfFavorecida.
     *
     * @param codigoUfFavorecida the codigo uf favorecida
     */
    public void setCodigoUfFavorecida(String codigoUfFavorecida) {
        this.codigoUfFavorecida = codigoUfFavorecida;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
        return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
        this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
        return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
        this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: conta.
     *
     * @return conta
     */
    public String getConta() {
        return conta;
    }

    /**
     * Set: conta.
     *
     * @param conta the conta
     */
    public void setConta(String conta) {
        this.conta = conta;
    }

    /**
     * Get: contaDigitoCredito.
     *
     * @return contaDigitoCredito
     */
    public String getContaDigitoCredito() {
        return contaDigitoCredito;
    }

    /**
     * Set: contaDigitoCredito.
     *
     * @param contaDigitoCredito the conta digito credito
     */
    public void setContaDigitoCredito(String contaDigitoCredito) {
        this.contaDigitoCredito = contaDigitoCredito;
    }

    /**
     * Get: contaDigitoDestino.
     *
     * @return contaDigitoDestino
     */
    public String getContaDigitoDestino() {
        return contaDigitoDestino;
    }

    /**
     * Set: contaDigitoDestino.
     *
     * @param contaDigitoDestino the conta digito destino
     */
    public void setContaDigitoDestino(String contaDigitoDestino) {
        this.contaDigitoDestino = contaDigitoDestino;
    }

    /**
     * Get: cpfCnpj.
     *
     * @return cpfCnpj
     */
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Set: cpfCnpj.
     *
     * @param cpfCnpj the cpf cnpj
     */
    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    /**
     * Get: dataCompetencia.
     *
     * @return dataCompetencia
     */
    public String getDataCompetencia() {
        return dataCompetencia;
    }

    /**
     * Set: dataCompetencia.
     *
     * @param dataCompetencia the data competencia
     */
    public void setDataCompetencia(String dataCompetencia) {
        this.dataCompetencia = dataCompetencia;
    }

    /**
     * Get: dataExpiracaoCredito.
     *
     * @return dataExpiracaoCredito
     */
    public String getDataExpiracaoCredito() {
        return dataExpiracaoCredito;
    }

    /**
     * Set: dataExpiracaoCredito.
     *
     * @param dataExpiracaoCredito the data expiracao credito
     */
    public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
        this.dataExpiracaoCredito = dataExpiracaoCredito;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
        return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
        this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: dataReferencia.
     *
     * @return dataReferencia
     */
    public String getDataReferencia() {
        return dataReferencia;
    }

    /**
     * Set: dataReferencia.
     *
     * @param dataReferencia the data referencia
     */
    public void setDataReferencia(String dataReferencia) {
        this.dataReferencia = dataReferencia;
    }

    /**
     * Get: dataRetirada.
     *
     * @return dataRetirada
     */
    public String getDataRetirada() {
        return dataRetirada;
    }

    /**
     * Set: dataRetirada.
     *
     * @param dataRetirada the data retirada
     */
    public void setDataRetirada(String dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    /**
     * Get: dddContribuinte.
     *
     * @return dddContribuinte
     */
    public String getDddContribuinte() {
        return dddContribuinte;
    }

    /**
     * Set: dddContribuinte.
     *
     * @param dddContribuinte the ddd contribuinte
     */
    public void setDddContribuinte(String dddContribuinte) {
        this.dddContribuinte = dddContribuinte;
    }

    /**
     * Get: descricaoUfFavorecida.
     *
     * @return descricaoUfFavorecida
     */
    public String getDescricaoUfFavorecida() {
        return descricaoUfFavorecida;
    }

    /**
     * Set: descricaoUfFavorecida.
     *
     * @param descricaoUfFavorecida the descricao uf favorecida
     */
    public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
        this.descricaoUfFavorecida = descricaoUfFavorecida;
    }

    /**
     * Get: dsAgenciaFavorecido.
     *
     * @return dsAgenciaFavorecido
     */
    public String getDsAgenciaFavorecido() {
        return dsAgenciaFavorecido;
    }

    /**
     * Set: dsAgenciaFavorecido.
     *
     * @param dsAgenciaFavorecido the ds agencia favorecido
     */
    public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
        this.dsAgenciaFavorecido = dsAgenciaFavorecido;
    }

    /**
     * Get: dsBancoFavorecido.
     *
     * @return dsBancoFavorecido
     */
    public String getDsBancoFavorecido() {
        return dsBancoFavorecido;
    }

    /**
     * Set: dsBancoFavorecido.
     *
     * @param dsBancoFavorecido the ds banco favorecido
     */
    public void setDsBancoFavorecido(String dsBancoFavorecido) {
        this.dsBancoFavorecido = dsBancoFavorecido;
    }

    /**
     * Get: dsContaFavorecido.
     *
     * @return dsContaFavorecido
     */
    public String getDsContaFavorecido() {
        return dsContaFavorecido;
    }

    /**
     * Set: dsContaFavorecido.
     *
     * @param dsContaFavorecido the ds conta favorecido
     */
    public void setDsContaFavorecido(String dsContaFavorecido) {
        this.dsContaFavorecido = dsContaFavorecido;
    }

    /**
     * Get: dsContratacao.
     *
     * @return dsContratacao
     */
    public String getDsContratacao() {
        return dsContratacao;
    }

    /**
     * Set: dsContratacao.
     *
     * @param dsContratacao the ds contratacao
     */
    public void setDsContratacao(String dsContratacao) {
        this.dsContratacao = dsContratacao;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
        return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
        this.dsFavorecido = dsFavorecido;
    }

    /**
     * Get: dsIndicadorModalidade.
     *
     * @return dsIndicadorModalidade
     */
    public String getDsIndicadorModalidade() {
        return dsIndicadorModalidade;
    }

    /**
     * Set: dsIndicadorModalidade.
     *
     * @param dsIndicadorModalidade the ds indicador modalidade
     */
    public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
        this.dsIndicadorModalidade = dsIndicadorModalidade;
    }

    /**
     * Get: dsMensagemPrimeiraLinha.
     *
     * @return dsMensagemPrimeiraLinha
     */
    public String getDsMensagemPrimeiraLinha() {
        return dsMensagemPrimeiraLinha;
    }

    /**
     * Set: dsMensagemPrimeiraLinha.
     *
     * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
     */
    public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
        this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
    }

    /**
     * Get: dsMensagemSegundaLinha.
     *
     * @return dsMensagemSegundaLinha
     */
    public String getDsMensagemSegundaLinha() {
        return dsMensagemSegundaLinha;
    }

    /**
     * Set: dsMensagemSegundaLinha.
     *
     * @param dsMensagemSegundaLinha the ds mensagem segunda linha
     */
    public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
        this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
    }

    /**
     * Get: dsMotivoSituacao.
     *
     * @return dsMotivoSituacao
     */
    public String getDsMotivoSituacao() {
        return dsMotivoSituacao;
    }

    /**
     * Set: dsMotivoSituacao.
     *
     * @param dsMotivoSituacao the ds motivo situacao
     */
    public void setDsMotivoSituacao(String dsMotivoSituacao) {
        this.dsMotivoSituacao = dsMotivoSituacao;
    }

    /**
     * Get: dsPagamento.
     *
     * @return dsPagamento
     */
    public String getDsPagamento() {
        return dsPagamento;
    }

    /**
     * Set: dsPagamento.
     *
     * @param dsPagamento the ds pagamento
     */
    public void setDsPagamento(String dsPagamento) {
        this.dsPagamento = dsPagamento;
    }

    /**
     * Get: dsTipoContaFavorecido.
     *
     * @return dsTipoContaFavorecido
     */
    public String getDsTipoContaFavorecido() {
        return dsTipoContaFavorecido;
    }

    /**
     * Set: dsTipoContaFavorecido.
     *
     * @param dsTipoContaFavorecido the ds tipo conta favorecido
     */
    public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
        this.dsTipoContaFavorecido = dsTipoContaFavorecido;
    }

    /**
     * Get: dsTipoServico.
     *
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
        return dsTipoServico;
    }

    /**
     * Set: dsTipoServico.
     *
     * @param dsTipoServico the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
        this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: dsUsoEmpresa.
     *
     * @return dsUsoEmpresa
     */
    public String getDsUsoEmpresa() {
        return dsUsoEmpresa;
    }

    /**
     * Set: dsUsoEmpresa.
     *
     * @param dsUsoEmpresa the ds uso empresa
     */
    public void setDsUsoEmpresa(String dsUsoEmpresa) {
        this.dsUsoEmpresa = dsUsoEmpresa;
    }

    /**
     * Get: dtAgendamento.
     *
     * @return dtAgendamento
     */
    public String getDtAgendamento() {
        return dtAgendamento;
    }

    /**
     * Set: dtAgendamento.
     *
     * @param dtAgendamento the dt agendamento
     */
    public void setDtAgendamento(String dtAgendamento) {
        this.dtAgendamento = dtAgendamento;
    }

    /**
     * Get: dtEmissaoDocumento.
     *
     * @return dtEmissaoDocumento
     */
    public String getDtEmissaoDocumento() {
        return dtEmissaoDocumento;
    }

    /**
     * Set: dtEmissaoDocumento.
     *
     * @param dtEmissaoDocumento the dt emissao documento
     */
    public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
        this.dtEmissaoDocumento = dtEmissaoDocumento;
    }

    /**
     * Get: dtNascimentoBeneficiario.
     *
     * @return dtNascimentoBeneficiario
     */
    public String getDtNascimentoBeneficiario() {
        return dtNascimentoBeneficiario;
    }

    /**
     * Set: dtNascimentoBeneficiario.
     *
     * @param dtNascimentoBeneficiario the dt nascimento beneficiario
     */
    public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
        this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
    }

    /**
     * Get: dtPagamento.
     *
     * @return dtPagamento
     */
    public String getDtPagamento() {
        return dtPagamento;
    }

    /**
     * Set: dtPagamento.
     *
     * @param dtPagamento the dt pagamento
     */
    public void setDtPagamento(String dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    /**
     * Get: dtVencimento.
     *
     * @return dtVencimento
     */
    public String getDtVencimento() {
        return dtVencimento;
    }

    /**
     * Set: dtVencimento.
     *
     * @param dtVencimento the dt vencimento
     */
    public void setDtVencimento(String dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    /**
     * Get: empresaConglomerado.
     *
     * @return empresaConglomerado
     */
    public String getEmpresaConglomerado() {
        return empresaConglomerado;
    }

    /**
     * Set: empresaConglomerado.
     *
     * @param empresaConglomerado the empresa conglomerado
     */
    public void setEmpresaConglomerado(String empresaConglomerado) {
        this.empresaConglomerado = empresaConglomerado;
    }

    /**
     * Get: finalidadeDoc.
     *
     * @return finalidadeDoc
     */
    public String getFinalidadeDoc() {
        return finalidadeDoc;
    }

    /**
     * Set: finalidadeDoc.
     *
     * @param finalidadeDoc the finalidade doc
     */
    public void setFinalidadeDoc(String finalidadeDoc) {
        this.finalidadeDoc = finalidadeDoc;
    }

    /**
     * Get: finalidadeTed.
     *
     * @return finalidadeTed
     */
    public String getFinalidadeTed() {
        return finalidadeTed;
    }

    /**
     * Set: finalidadeTed.
     *
     * @param finalidadeTed the finalidade ted
     */
    public void setFinalidadeTed(String finalidadeTed) {
        this.finalidadeTed = finalidadeTed;
    }

    /**
     * Is hidden confirma.
     *
     * @return true, if is hidden confirma
     */
    public boolean isHiddenConfirma() {
        return hiddenConfirma;
    }

    /**
     * Set: hiddenConfirma.
     *
     * @param hiddenConfirma the hidden confirma
     */
    public void setHiddenConfirma(boolean hiddenConfirma) {
        this.hiddenConfirma = hiddenConfirma;
    }

    /**
     * Get: identificacaoTitularidade.
     *
     * @return identificacaoTitularidade
     */
    public String getIdentificacaoTitularidade() {
        return identificacaoTitularidade;
    }

    /**
     * Set: identificacaoTitularidade.
     *
     * @param identificacaoTitularidade the identificacao titularidade
     */
    public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
        this.identificacaoTitularidade = identificacaoTitularidade;
    }

    /**
     * Get: identificacaoTransferenciaDoc.
     *
     * @return identificacaoTransferenciaDoc
     */
    public String getIdentificacaoTransferenciaDoc() {
        return identificacaoTransferenciaDoc;
    }

    /**
     * Set: identificacaoTransferenciaDoc.
     *
     * @param identificacaoTransferenciaDoc the identificacao transferencia doc
     */
    public void setIdentificacaoTransferenciaDoc(
        String identificacaoTransferenciaDoc) {
        this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
    }

    /**
     * Get: identificacaoTransferenciaTed.
     *
     * @return identificacaoTransferenciaTed
     */
    public String getIdentificacaoTransferenciaTed() {
        return identificacaoTransferenciaTed;
    }

    /**
     * Set: identificacaoTransferenciaTed.
     *
     * @param identificacaoTransferenciaTed the identificacao transferencia ted
     */
    public void setIdentificacaoTransferenciaTed(
        String identificacaoTransferenciaTed) {
        this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
    }

    /**
     * Get: identificadorRetirada.
     *
     * @return identificadorRetirada
     */
    public String getIdentificadorRetirada() {
        return identificadorRetirada;
    }

    /**
     * Set: identificadorRetirada.
     *
     * @param identificadorRetirada the identificador retirada
     */
    public void setIdentificadorRetirada(String identificadorRetirada) {
        this.identificadorRetirada = identificadorRetirada;
    }

    /**
     * Get: identificadorTipoRetirada.
     *
     * @return identificadorTipoRetirada
     */
    public String getIdentificadorTipoRetirada() {
        return identificadorTipoRetirada;
    }

    /**
     * Set: identificadorTipoRetirada.
     *
     * @param identificadorTipoRetirada the identificador tipo retirada
     */
    public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
        this.identificadorTipoRetirada = identificadorTipoRetirada;
    }

    /**
     * Get: indicadorAssociadoUnicaAgencia.
     *
     * @return indicadorAssociadoUnicaAgencia
     */
    public String getIndicadorAssociadoUnicaAgencia() {
        return indicadorAssociadoUnicaAgencia;
    }

    /**
     * Set: indicadorAssociadoUnicaAgencia.
     *
     * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
     */
    public void setIndicadorAssociadoUnicaAgencia(
        String indicadorAssociadoUnicaAgencia) {
        this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
    }

    /**
     * Get: inscricaoEstadualContribuinte.
     *
     * @return inscricaoEstadualContribuinte
     */
    public String getInscricaoEstadualContribuinte() {
        return inscricaoEstadualContribuinte;
    }

    /**
     * Set: inscricaoEstadualContribuinte.
     *
     * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
     */
    public void setInscricaoEstadualContribuinte(
        String inscricaoEstadualContribuinte) {
        this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
    }

    /**
     * Get: instrucaoPagamento.
     *
     * @return instrucaoPagamento
     */
    public String getInstrucaoPagamento() {
        return instrucaoPagamento;
    }

    /**
     * Set: instrucaoPagamento.
     *
     * @param instrucaoPagamento the instrucao pagamento
     */
    public void setInstrucaoPagamento(String instrucaoPagamento) {
        this.instrucaoPagamento = instrucaoPagamento;
    }

    /**
     * Get: itemSelecionadoListaAutorizarPagamentosConsolidado.
     *
     * @return itemSelecionadoListaAutorizarPagamentosConsolidado
     */
    public Integer getItemSelecionadoListaAutorizarPagamentosConsolidado() {
        return itemSelecionadoListaAutorizarPagamentosConsolidado;
    }

    /**
     * Set: itemSelecionadoListaAutorizarPagamentosConsolidado.
     *
     * @param itemSelecionadoListaAutorizarPagamentosConsolidado the item selecionado lista autorizar pagamentos consolidado
     */
    public void setItemSelecionadoListaAutorizarPagamentosConsolidado(
        Integer itemSelecionadoListaAutorizarPagamentosConsolidado) {
        this.itemSelecionadoListaAutorizarPagamentosConsolidado = itemSelecionadoListaAutorizarPagamentosConsolidado;
    }

    /**
     * Get: itemSelecionadolistaAutorizarPagamentosConsolidadosParcial.
     *
     * @return itemSelecionadolistaAutorizarPagamentosConsolidadosParcial
     */
    public Integer getItemSelecionadolistaAutorizarPagamentosConsolidadosParcial() {
        return itemSelecionadolistaAutorizarPagamentosConsolidadosParcial;
    }

    /**
     * Set: itemSelecionadolistaAutorizarPagamentosConsolidadosParcial.
     *
     * @param itemSelecionadolistaAutorizarPagamentosConsolidadosParcial the item selecionadolista autorizar pagamentos consolidados parcial
     */
    public void setItemSelecionadolistaAutorizarPagamentosConsolidadosParcial(
        Integer itemSelecionadolistaAutorizarPagamentosConsolidadosParcial) {
        this.itemSelecionadolistaAutorizarPagamentosConsolidadosParcial = itemSelecionadolistaAutorizarPagamentosConsolidadosParcial;
    }

    /**
     * Get: itemSelecionadoListaDetAutPagamentosConsolidados.
     *
     * @return itemSelecionadoListaDetAutPagamentosConsolidados
     */
    public Integer getItemSelecionadoListaDetAutPagamentosConsolidados() {
        return itemSelecionadoListaDetAutPagamentosConsolidados;
    }

    /**
     * Set: itemSelecionadoListaDetAutPagamentosConsolidados.
     *
     * @param itemSelecionadoListaDetAutPagamentosConsolidados the item selecionado lista det aut pagamentos consolidados
     */
    public void setItemSelecionadoListaDetAutPagamentosConsolidados(
        Integer itemSelecionadoListaDetAutPagamentosConsolidados) {
        this.itemSelecionadoListaDetAutPagamentosConsolidados = itemSelecionadoListaDetAutPagamentosConsolidados;
    }

    /**
     * Get: listaAutorizarMotivoSituacaoPagamentoFiltro.
     *
     * @return listaAutorizarMotivoSituacaoPagamentoFiltro
     */
    public List<SelectItem> getListaAutorizarMotivoSituacaoPagamentoFiltro() {
        return listaAutorizarMotivoSituacaoPagamentoFiltro;
    }

    /**
     * Set: listaAutorizarMotivoSituacaoPagamentoFiltro.
     *
     * @param listaAutorizarMotivoSituacaoPagamentoFiltro the lista autorizar motivo situacao pagamento filtro
     */
    public void setListaAutorizarMotivoSituacaoPagamentoFiltro(
        List<SelectItem> listaAutorizarMotivoSituacaoPagamentoFiltro) {
        this.listaAutorizarMotivoSituacaoPagamentoFiltro = listaAutorizarMotivoSituacaoPagamentoFiltro;
    }

    /**
     * Get: listaAutorizarMotivoSituacaoPagamentoFiltroHash.
     *
     * @return listaAutorizarMotivoSituacaoPagamentoFiltroHash
     */
    public Map<Integer, String> getListaAutorizarMotivoSituacaoPagamentoFiltroHash() {
        return listaAutorizarMotivoSituacaoPagamentoFiltroHash;
    }

    /**
     * Set lista autorizar motivo situacao pagamento filtro hash.
     *
     * @param listaAutorizarMotivoSituacaoPagamentoFiltroHash the lista autorizar motivo situacao pagamento filtro hash
     */
    public void setListaAutorizarMotivoSituacaoPagamentoFiltroHash(
        Map<Integer, String> listaAutorizarMotivoSituacaoPagamentoFiltroHash) {
        this.listaAutorizarMotivoSituacaoPagamentoFiltroHash = listaAutorizarMotivoSituacaoPagamentoFiltroHash;
    }

    /**
     * Get: listaAutorizarSituacaoPagamentoFiltro.
     *
     * @return listaAutorizarSituacaoPagamentoFiltro
     */
    public List<SelectItem> getListaAutorizarSituacaoPagamentoFiltro() {
        return listaAutorizarSituacaoPagamentoFiltro;
    }

    /**
     * Set: listaAutorizarSituacaoPagamentoFiltro.
     *
     * @param listaAutorizarSituacaoPagamentoFiltro the lista autorizar situacao pagamento filtro
     */
    public void setListaAutorizarSituacaoPagamentoFiltro(
        List<SelectItem> listaAutorizarSituacaoPagamentoFiltro) {
        this.listaAutorizarSituacaoPagamentoFiltro = listaAutorizarSituacaoPagamentoFiltro;
    }

    /**
     * Get: listaAutorizarSituacaoPagamentoFiltroHash.
     *
     * @return listaAutorizarSituacaoPagamentoFiltroHash
     */
    public Map<Integer, String> getListaAutorizarSituacaoPagamentoFiltroHash() {
        return listaAutorizarSituacaoPagamentoFiltroHash;
    }

    /**
     * Set lista autorizar situacao pagamento filtro hash.
     *
     * @param listaAutorizarSituacaoPagamentoFiltroHash the lista autorizar situacao pagamento filtro hash
     */
    public void setListaAutorizarSituacaoPagamentoFiltroHash(
        Map<Integer, String> listaAutorizarSituacaoPagamentoFiltroHash) {
        this.listaAutorizarSituacaoPagamentoFiltroHash = listaAutorizarSituacaoPagamentoFiltroHash;
    }

    /**
     * Get: listaControleAutorizarPagamentosConsolidadosParcial.
     *
     * @return listaControleAutorizarPagamentosConsolidadosParcial
     */
    public List<SelectItem> getListaControleAutorizarPagamentosConsolidadosParcial() {
        return listaControleAutorizarPagamentosConsolidadosParcial;
    }

    /**
     * Set: listaControleAutorizarPagamentosConsolidadosParcial.
     *
     * @param listaControleAutorizarPagamentosConsolidadosParcial the lista controle autorizar pagamentos consolidados parcial
     */
    public void setListaControleAutorizarPagamentosConsolidadosParcial(
        List<SelectItem> listaControleAutorizarPagamentosConsolidadosParcial) {
        this.listaControleAutorizarPagamentosConsolidadosParcial = listaControleAutorizarPagamentosConsolidadosParcial;
    }

    /**
     * Get: listaControleDetAutPagamentosConsolidados.
     *
     * @return listaControleDetAutPagamentosConsolidados
     */
    public List<SelectItem> getListaControleDetAutPagamentosConsolidados() {
        return listaControleDetAutPagamentosConsolidados;
    }

    /**
     * Set: listaControleDetAutPagamentosConsolidados.
     *
     * @param listaControleDetAutPagamentosConsolidados the lista controle det aut pagamentos consolidados
     */
    public void setListaControleDetAutPagamentosConsolidados(
        List<SelectItem> listaControleDetAutPagamentosConsolidados) {
        this.listaControleDetAutPagamentosConsolidados = listaControleDetAutPagamentosConsolidados;
    }

    /**
     * Get: listaDetAutPagamentosConsolidados.
     *
     * @return listaDetAutPagamentosConsolidados
     */
    public List<OcorrenciasDetPagConsPendAutSaidaDTO> getListaDetAutPagamentosConsolidados() {
        return listaDetAutPagamentosConsolidados;
    }

    /**
     * Set: listaDetAutPagamentosConsolidados.
     *
     * @param listaDetAutPagamentosConsolidados the lista det aut pagamentos consolidados
     */
    public void setListaDetAutPagamentosConsolidados(
        List<OcorrenciasDetPagConsPendAutSaidaDTO> listaDetAutPagamentosConsolidados) {
        this.listaDetAutPagamentosConsolidados = listaDetAutPagamentosConsolidados;
    }

    /**
     * Get: listaGridAutorizarPagamentosConsolidado.
     *
     * @return listaGridAutorizarPagamentosConsolidado
     */
    public List<ConsultarPagtosConsPendAutorizacaoSaidaDTO> getListaGridAutorizarPagamentosConsolidado() {
        return listaGridAutorizarPagamentosConsolidado;
    }

    /**
     * Set: listaGridAutorizarPagamentosConsolidado.
     *
     * @param listaGridAutorizarPagamentosConsolidado the lista grid autorizar pagamentos consolidado
     */
    public void setListaGridAutorizarPagamentosConsolidado(
        List<ConsultarPagtosConsPendAutorizacaoSaidaDTO> listaGridAutorizarPagamentosConsolidado) {
        this.listaGridAutorizarPagamentosConsolidado = listaGridAutorizarPagamentosConsolidado;
    }

    /**
     * Get: listaGridAutorizarPagamentosConsolidadosIntegral.
     *
     * @return listaGridAutorizarPagamentosConsolidadosIntegral
     */
    public List<OcorrenciasDetPagConsPendAutSaidaDTO> getListaGridAutorizarPagamentosConsolidadosIntegral() {
        return listaGridAutorizarPagamentosConsolidadosIntegral;
    }

    /**
     * Set: listaGridAutorizarPagamentosConsolidadosIntegral.
     *
     * @param listaGridAutorizarPagamentosConsolidadosIntegral the lista grid autorizar pagamentos consolidados integral
     */
    public void setListaGridAutorizarPagamentosConsolidadosIntegral(
        List<OcorrenciasDetPagConsPendAutSaidaDTO> listaGridAutorizarPagamentosConsolidadosIntegral) {
        this.listaGridAutorizarPagamentosConsolidadosIntegral = listaGridAutorizarPagamentosConsolidadosIntegral;
    }

    /**
     * Get: listaGridAutorizarPagamentosConsolidadosParcial.
     *
     * @return listaGridAutorizarPagamentosConsolidadosParcial
     */
    public List<OcorrenciasDetPagConsPendAutSaidaDTO> getListaGridAutorizarPagamentosConsolidadosParcial() {
        return listaGridAutorizarPagamentosConsolidadosParcial;
    }

    /**
     * Set: listaGridAutorizarPagamentosConsolidadosParcial.
     *
     * @param listaGridAutorizarPagamentosConsolidadosParcial the lista grid autorizar pagamentos consolidados parcial
     */
    public void setListaGridAutorizarPagamentosConsolidadosParcial(
        List<OcorrenciasDetPagConsPendAutSaidaDTO> listaGridAutorizarPagamentosConsolidadosParcial) {
        this.listaGridAutorizarPagamentosConsolidadosParcial = listaGridAutorizarPagamentosConsolidadosParcial;
    }

    /**
     * Get: listaGridAutorizarPagamentosConsolidadosSelecionados.
     *
     * @return listaGridAutorizarPagamentosConsolidadosSelecionados
     */
    public List<OcorrenciasDetPagConsPendAutSaidaDTO> getListaGridAutorizarPagamentosConsolidadosSelecionados() {
        return listaGridAutorizarPagamentosConsolidadosSelecionados;
    }

    /**
     * Set: listaGridAutorizarPagamentosConsolidadosSelecionados.
     *
     * @param listaGridAutorizarPagamentosConsolidadosSelecionados the lista grid autorizar pagamentos consolidados selecionados
     */
    public void setListaGridAutorizarPagamentosConsolidadosSelecionados(
        List<OcorrenciasDetPagConsPendAutSaidaDTO> listaGridAutorizarPagamentosConsolidadosSelecionados) {
        this.listaGridAutorizarPagamentosConsolidadosSelecionados = listaGridAutorizarPagamentosConsolidadosSelecionados;
    }

    /**
     * Get: listaRadiosAutorizarPagamentosConsolidado.
     *
     * @return listaRadiosAutorizarPagamentosConsolidado
     */
    public List<SelectItem> getListaRadiosAutorizarPagamentosConsolidado() {
        return listaRadiosAutorizarPagamentosConsolidado;
    }

    /**
     * Set: listaRadiosAutorizarPagamentosConsolidado.
     *
     * @param listaRadiosAutorizarPagamentosConsolidado the lista radios autorizar pagamentos consolidado
     */
    public void setListaRadiosAutorizarPagamentosConsolidado(
        List<SelectItem> listaRadiosAutorizarPagamentosConsolidado) {
        this.listaRadiosAutorizarPagamentosConsolidado = listaRadiosAutorizarPagamentosConsolidado;
    }

    /**
     * Get: logoPath.
     *
     * @return logoPath
     */
    public String getLogoPath() {
        return logoPath;
    }

    /**
     * Set: logoPath.
     *
     * @param logoPath the logo path
     */
    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    /**
     * Get: modalidade.
     *
     * @return modalidade
     */
    public String getModalidade() {
        return modalidade;
    }

    /**
     * Set: modalidade.
     *
     * @param modalidade the modalidade
     */
    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    /**
     * Get: municipioTributo.
     *
     * @return municipioTributo
     */
    public String getMunicipioTributo() {
        return municipioTributo;
    }

    /**
     * Set: municipioTributo.
     *
     * @param municipioTributo the municipio tributo
     */
    public void setMunicipioTributo(String municipioTributo) {
        this.municipioTributo = municipioTributo;
    }

    /**
     * Get: nomeBeneficiario.
     *
     * @return nomeBeneficiario
     */
    public String getNomeBeneficiario() {
        return nomeBeneficiario;
    }

    /**
     * Set: nomeBeneficiario.
     *
     * @param nomeBeneficiario the nome beneficiario
     */
    public void setNomeBeneficiario(String nomeBeneficiario) {
        this.nomeBeneficiario = nomeBeneficiario;
    }

    /**
     * Get: nomeDepositante.
     *
     * @return nomeDepositante
     */
    public String getNomeDepositante() {
        return nomeDepositante;
    }

    /**
     * Set: nomeDepositante.
     *
     * @param nomeDepositante the nome depositante
     */
    public void setNomeDepositante(String nomeDepositante) {
        this.nomeDepositante = nomeDepositante;
    }

    /**
     * Get: nomeOrgaoFavorecido.
     *
     * @return nomeOrgaoFavorecido
     */
    public String getNomeOrgaoFavorecido() {
        return nomeOrgaoFavorecido;
    }

    /**
     * Set: nomeOrgaoFavorecido.
     *
     * @param nomeOrgaoFavorecido the nome orgao favorecido
     */
    public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
        this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
    }

    /**
     * Get: nomeRazaoSocial.
     *
     * @return nomeRazaoSocial
     */
    public String getNomeRazaoSocial() {
        return nomeRazaoSocial;
    }

    /**
     * Set: nomeRazaoSocial.
     *
     * @param nomeRazaoSocial the nome razao social
     */
    public void setNomeRazaoSocial(String nomeRazaoSocial) {
        this.nomeRazaoSocial = nomeRazaoSocial;
    }

    /**
     * Get: nrContrato.
     *
     * @return nrContrato
     */
    public Long getNrContrato() {
        return nrContrato;
    }

    /**
     * Set: nrContrato.
     *
     * @param nrContrato the nr contrato
     */
    public void setNrContrato(Long nrContrato) {
        this.nrContrato = nrContrato;
    }

    /**
     * Get: nrDocumento.
     *
     * @return nrDocumento
     */
    public String getNrDocumento() {
        return nrDocumento;
    }

    /**
     * Set: nrDocumento.
     *
     * @param nrDocumento the nr documento
     */
    public void setNrDocumento(String nrDocumento) {
        this.nrDocumento = nrDocumento;
    }

    /**
     * Get: nrLoteArquivoRemessa.
     *
     * @return nrLoteArquivoRemessa
     */
    public String getNrLoteArquivoRemessa() {
        return nrLoteArquivoRemessa;
    }

    /**
     * Set: nrLoteArquivoRemessa.
     *
     * @param nrLoteArquivoRemessa the nr lote arquivo remessa
     */
    public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
        this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
    }

    /**
     * Get: nroPagamento.
     *
     * @return nroPagamento
     */
    public String getNroPagamento() {
        return nroPagamento;
    }

    /**
     * Set: nroPagamento.
     *
     * @param nroPagamento the nro pagamento
     */
    public void setNroPagamento(String nroPagamento) {
        this.nroPagamento = nroPagamento;
    }

    /**
     * Get: nrSequenciaArquivoRemessa.
     *
     * @return nrSequenciaArquivoRemessa
     */
    public String getNrSequenciaArquivoRemessa() {
        return nrSequenciaArquivoRemessa;
    }

    /**
     * Set: nrSequenciaArquivoRemessa.
     *
     * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
     */
    public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
        this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
    }

    /**
     * Get: numeroCheque.
     *
     * @return numeroCheque
     */
    public String getNumeroCheque() {
        return numeroCheque;
    }

    /**
     * Set: numeroCheque.
     *
     * @param numeroCheque the numero cheque
     */
    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    /**
     * Get: numeroCotaParcela.
     *
     * @return numeroCotaParcela
     */
    public String getNumeroCotaParcela() {
        return numeroCotaParcela;
    }

    /**
     * Set: numeroCotaParcela.
     *
     * @param numeroCotaParcela the numero cota parcela
     */
    public void setNumeroCotaParcela(String numeroCotaParcela) {
        this.numeroCotaParcela = numeroCotaParcela;
    }

    /**
     * Get: numeroInscricaoBeneficiario.
     *
     * @return numeroInscricaoBeneficiario
     */
    public String getNumeroInscricaoBeneficiario() {
        return numeroInscricaoBeneficiario;
    }

    /**
     * Set: numeroInscricaoBeneficiario.
     *
     * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
     */
    public void setNumeroInscricaoBeneficiario(
        String numeroInscricaoBeneficiario) {
        this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
    }

    /**
     * Get: numeroInscricaoFavorecido.
     *
     * @return numeroInscricaoFavorecido
     */
    public String getNumeroInscricaoFavorecido() {
        return numeroInscricaoFavorecido;
    }

    /**
     * Set: numeroInscricaoFavorecido.
     *
     * @param numeroInscricaoFavorecido the numero inscricao favorecido
     */
    public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
        this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
    }

    /**
     * Get: numeroNsu.
     *
     * @return numeroNsu
     */
    public String getNumeroNsu() {
        return numeroNsu;
    }

    /**
     * Set: numeroNsu.
     *
     * @param numeroNsu the numero nsu
     */
    public void setNumeroNsu(String numeroNsu) {
        this.numeroNsu = numeroNsu;
    }

    /**
     * Get: numeroOp.
     *
     * @return numeroOp
     */
    public String getNumeroOp() {
        return numeroOp;
    }

    /**
     * Set: numeroOp.
     *
     * @param numeroOp the numero op
     */
    public void setNumeroOp(String numeroOp) {
        this.numeroOp = numeroOp;
    }

    /**
     * Get: numeroParcelamento.
     *
     * @return numeroParcelamento
     */
    public String getNumeroParcelamento() {
        return numeroParcelamento;
    }

    /**
     * Set: numeroParcelamento.
     *
     * @param numeroParcelamento the numero parcelamento
     */
    public void setNumeroParcelamento(String numeroParcelamento) {
        this.numeroParcelamento = numeroParcelamento;
    }

    /**
     * Get: numeroReferencia.
     *
     * @return numeroReferencia
     */
    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    /**
     * Set: numeroReferencia.
     *
     * @param numeroReferencia the numero referencia
     */
    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    /**
     * Get: observacaoTributo.
     *
     * @return observacaoTributo
     */
    public String getObservacaoTributo() {
        return observacaoTributo;
    }

    /**
     * Set: observacaoTributo.
     *
     * @param observacaoTributo the observacao tributo
     */
    public void setObservacaoTributo(String observacaoTributo) {
        this.observacaoTributo = observacaoTributo;
    }

    /**
     * Get: opcaoRetiradaTributo.
     *
     * @return opcaoRetiradaTributo
     */
    public String getOpcaoRetiradaTributo() {
        return opcaoRetiradaTributo;
    }

    /**
     * Set: opcaoRetiradaTributo.
     *
     * @param opcaoRetiradaTributo the opcao retirada tributo
     */
    public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
        this.opcaoRetiradaTributo = opcaoRetiradaTributo;
    }

    /**
     * Get: opcaoTributo.
     *
     * @return opcaoTributo
     */
    public String getOpcaoTributo() {
        return opcaoTributo;
    }

    /**
     * Set: opcaoTributo.
     *
     * @param opcaoTributo the opcao tributo
     */
    public void setOpcaoTributo(String opcaoTributo) {
        this.opcaoTributo = opcaoTributo;
    }

    /**
     * Is panel botoes.
     *
     * @return true, if is panel botoes
     */
    public boolean isPanelBotoes() {
        return panelBotoes;
    }

    /**
     * Set: panelBotoes.
     *
     * @param panelBotoes the panel botoes
     */
    public void setPanelBotoes(boolean panelBotoes) {
        this.panelBotoes = panelBotoes;
    }

    /**
     * Get: percentualReceita.
     *
     * @return percentualReceita
     */
    public BigDecimal getPercentualReceita() {
        return percentualReceita;
    }

    /**
     * Set: percentualReceita.
     *
     * @param percentualReceita the percentual receita
     */
    public void setPercentualReceita(BigDecimal percentualReceita) {
        this.percentualReceita = percentualReceita;
    }

    /**
     * Get: periodoApuracao.
     *
     * @return periodoApuracao
     */
    public String getPeriodoApuracao() {
        return periodoApuracao;
    }

    /**
     * Set: periodoApuracao.
     *
     * @param periodoApuracao the periodo apuracao
     */
    public void setPeriodoApuracao(String periodoApuracao) {
        this.periodoApuracao = periodoApuracao;
    }

    /**
     * Get: placaVeiculo.
     *
     * @return placaVeiculo
     */
    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    /**
     * Set: placaVeiculo.
     *
     * @param placaVeiculo the placa veiculo
     */
    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    /**
     * Get: produtoDepositoIdentificado.
     *
     * @return produtoDepositoIdentificado
     */
    public String getProdutoDepositoIdentificado() {
        return produtoDepositoIdentificado;
    }

    /**
     * Set: produtoDepositoIdentificado.
     *
     * @param produtoDepositoIdentificado the produto deposito identificado
     */
    public void setProdutoDepositoIdentificado(
        String produtoDepositoIdentificado) {
        this.produtoDepositoIdentificado = produtoDepositoIdentificado;
    }

    /**
     * Get: qtdePagamentos.
     *
     * @return qtdePagamentos
     */
    public Long getQtdePagamentos() {
        return qtdePagamentos;
    }

    /**
     * Set: qtdePagamentos.
     *
     * @param qtdePagamentos the qtde pagamentos
     */
    public void setQtdePagamentos(Long qtdePagamentos) {
        this.qtdePagamentos = qtdePagamentos;
    }

    /**
     * Get: qtdePagamentosAutorizados.
     *
     * @return qtdePagamentosAutorizados
     */
    public Long getQtdePagamentosAutorizados() {
        return qtdePagamentosAutorizados;
    }

    /**
     * Set: qtdePagamentosAutorizados.
     *
     * @param qtdePagamentosAutorizados the qtde pagamentos autorizados
     */
    public void setQtdePagamentosAutorizados(Long qtdePagamentosAutorizados) {
        this.qtdePagamentosAutorizados = qtdePagamentosAutorizados;
    }

    /**
     * Get: qtMoeda.
     *
     * @return qtMoeda
     */
    public BigDecimal getQtMoeda() {
        return qtMoeda;
    }

    /**
     * Set: qtMoeda.
     *
     * @param qtMoeda the qt moeda
     */
    public void setQtMoeda(BigDecimal qtMoeda) {
        this.qtMoeda = qtMoeda;
    }

    /**
     * Get: sequenciaProdutoDepositoIdentificado.
     *
     * @return sequenciaProdutoDepositoIdentificado
     */
    public String getSequenciaProdutoDepositoIdentificado() {
        return sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Set: sequenciaProdutoDepositoIdentificado.
     *
     * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
     */
    public void setSequenciaProdutoDepositoIdentificado(
        String sequenciaProdutoDepositoIdentificado) {
        this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Get: serieCheque.
     *
     * @return serieCheque
     */
    public String getSerieCheque() {
        return serieCheque;
    }

    /**
     * Set: serieCheque.
     *
     * @param serieCheque the serie cheque
     */
    public void setSerieCheque(String serieCheque) {
        this.serieCheque = serieCheque;
    }

    /**
     * Get: situacao.
     *
     * @return situacao
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Set: situacao.
     *
     * @param situacao the situacao
     */
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * Get: telefoneContribuinte.
     *
     * @return telefoneContribuinte
     */
    public String getTelefoneContribuinte() {
        return telefoneContribuinte;
    }

    /**
     * Set: telefoneContribuinte.
     *
     * @param telefoneContribuinte the telefone contribuinte
     */
    public void setTelefoneContribuinte(String telefoneContribuinte) {
        this.telefoneContribuinte = telefoneContribuinte;
    }

    /**
     * Get: tipo.
     *
     * @return tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set: tipo.
     *
     * @param tipo the tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Get: tipoBeneficiario.
     *
     * @return tipoBeneficiario
     */
    public String getTipoBeneficiario() {
        return tipoBeneficiario;
    }

    /**
     * Set: tipoBeneficiario.
     *
     * @param tipoBeneficiario the tipo beneficiario
     */
    public void setTipoBeneficiario(String tipoBeneficiario) {
        this.tipoBeneficiario = tipoBeneficiario;
    }

    /**
     * Get: tipoCanal.
     *
     * @return tipoCanal
     */
    public Integer getTipoCanal() {
        return tipoCanal;
    }

    /**
     * Set: tipoCanal.
     *
     * @param tipoCanal the tipo canal
     */
    public void setTipoCanal(Integer tipoCanal) {
        this.tipoCanal = tipoCanal;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
        this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
        this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: tipoContaCredito.
     *
     * @return tipoContaCredito
     */
    public String getTipoContaCredito() {
        return tipoContaCredito;
    }

    /**
     * Set: tipoContaCredito.
     *
     * @param tipoContaCredito the tipo conta credito
     */
    public void setTipoContaCredito(String tipoContaCredito) {
        this.tipoContaCredito = tipoContaCredito;
    }

    /**
     * Get: tipoContaDestino.
     *
     * @return tipoContaDestino
     */
    public String getTipoContaDestino() {
        return tipoContaDestino;
    }

    /**
     * Set: tipoContaDestino.
     *
     * @param tipoContaDestino the tipo conta destino
     */
    public void setTipoContaDestino(String tipoContaDestino) {
        this.tipoContaDestino = tipoContaDestino;
    }

    /**
     * Get: tipoDespositoIdentificado.
     *
     * @return tipoDespositoIdentificado
     */
    public String getTipoDespositoIdentificado() {
        return tipoDespositoIdentificado;
    }

    /**
     * Set: tipoDespositoIdentificado.
     *
     * @param tipoDespositoIdentificado the tipo desposito identificado
     */
    public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
        this.tipoDespositoIdentificado = tipoDespositoIdentificado;
    }

    /**
     * Get: tipoDocumento.
     *
     * @return tipoDocumento
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Set: tipoDocumento.
     *
     * @param tipoDocumento the tipo documento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     * Get: tipoFavorecido.
     *
     * @return tipoFavorecido
     */
    public Integer getTipoFavorecido() {
        return tipoFavorecido;
    }

    /**
     * Set: tipoFavorecido.
     *
     * @param tipoFavorecido the tipo favorecido
     */
    public void setTipoFavorecido(Integer tipoFavorecido) {
        this.tipoFavorecido = tipoFavorecido;
    }

    /**
     * Get: tipoServico.
     *
     * @return tipoServico
     */
    public String getTipoServico() {
        return tipoServico;
    }

    /**
     * Set: tipoServico.
     *
     * @param tipoServico the tipo servico
     */
    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    /**
     * Get: ufTributo.
     *
     * @return ufTributo
     */
    public String getUfTributo() {
        return ufTributo;
    }

    /**
     * Set: ufTributo.
     *
     * @param ufTributo the uf tributo
     */
    public void setUfTributo(String ufTributo) {
        this.ufTributo = ufTributo;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
        return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
        return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
        this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: viaCartaoDepositoIdentificado.
     *
     * @return viaCartaoDepositoIdentificado
     */
    public String getViaCartaoDepositoIdentificado() {
        return viaCartaoDepositoIdentificado;
    }

    /**
     * Set: viaCartaoDepositoIdentificado.
     *
     * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
     */
    public void setViaCartaoDepositoIdentificado(
        String viaCartaoDepositoIdentificado) {
        this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
    }

    /**
     * Get: vlAbatimento.
     *
     * @return vlAbatimento
     */
    public BigDecimal getVlAbatimento() {
        return vlAbatimento;
    }

    /**
     * Set: vlAbatimento.
     *
     * @param vlAbatimento the vl abatimento
     */
    public void setVlAbatimento(BigDecimal vlAbatimento) {
        this.vlAbatimento = vlAbatimento;
    }

    /**
     * Get: vlAcrescimo.
     *
     * @return vlAcrescimo
     */
    public BigDecimal getVlAcrescimo() {
        return vlAcrescimo;
    }

    /**
     * Set: vlAcrescimo.
     *
     * @param vlAcrescimo the vl acrescimo
     */
    public void setVlAcrescimo(BigDecimal vlAcrescimo) {
        this.vlAcrescimo = vlAcrescimo;
    }

    /**
     * Get: vlAcrescimoFinanceiro.
     *
     * @return vlAcrescimoFinanceiro
     */
    public BigDecimal getVlAcrescimoFinanceiro() {
        return vlAcrescimoFinanceiro;
    }

    /**
     * Set: vlAcrescimoFinanceiro.
     *
     * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
     */
    public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
        this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
    }

    /**
     * Get: vlAtualizacaoMonetaria.
     *
     * @return vlAtualizacaoMonetaria
     */
    public BigDecimal getVlAtualizacaoMonetaria() {
        return vlAtualizacaoMonetaria;
    }

    /**
     * Set: vlAtualizacaoMonetaria.
     *
     * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
     */
    public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
        this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
    }

    /**
     * Get: vlDeducao.
     *
     * @return vlDeducao
     */
    public BigDecimal getVlDeducao() {
        return vlDeducao;
    }

    /**
     * Set: vlDeducao.
     *
     * @param vlDeducao the vl deducao
     */
    public void setVlDeducao(BigDecimal vlDeducao) {
        this.vlDeducao = vlDeducao;
    }

    /**
     * Get: vlDesconto.
     *
     * @return vlDesconto
     */
    public BigDecimal getVlDesconto() {
        return vlDesconto;
    }

    /**
     * Set: vlDesconto.
     *
     * @param vlDesconto the vl desconto
     */
    public void setVlDesconto(BigDecimal vlDesconto) {
        this.vlDesconto = vlDesconto;
    }

    /**
     * Get: vlDocumento.
     *
     * @return vlDocumento
     */
    public BigDecimal getVlDocumento() {
        return vlDocumento;
    }

    /**
     * Set: vlDocumento.
     *
     * @param vlDocumento the vl documento
     */
    public void setVlDocumento(BigDecimal vlDocumento) {
        this.vlDocumento = vlDocumento;
    }

    /**
     * Get: vlHonorarioAdvogado.
     *
     * @return vlHonorarioAdvogado
     */
    public BigDecimal getVlHonorarioAdvogado() {
        return vlHonorarioAdvogado;
    }

    /**
     * Set: vlHonorarioAdvogado.
     *
     * @param vlHonorarioAdvogado the vl honorario advogado
     */
    public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
        this.vlHonorarioAdvogado = vlHonorarioAdvogado;
    }

    /**
     * Get: vlImpostoMovFinanceira.
     *
     * @return vlImpostoMovFinanceira
     */
    public BigDecimal getVlImpostoMovFinanceira() {
        return vlImpostoMovFinanceira;
    }

    /**
     * Set: vlImpostoMovFinanceira.
     *
     * @param vlImpostoMovFinanceira the vl imposto mov financeira
     */
    public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
        this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
    }

    /**
     * Get: vlImpostoRenda.
     *
     * @return vlImpostoRenda
     */
    public BigDecimal getVlImpostoRenda() {
        return vlImpostoRenda;
    }

    /**
     * Set: vlImpostoRenda.
     *
     * @param vlImpostoRenda the vl imposto renda
     */
    public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
        this.vlImpostoRenda = vlImpostoRenda;
    }

    /**
     * Get: vlInss.
     *
     * @return vlInss
     */
    public BigDecimal getVlInss() {
        return vlInss;
    }

    /**
     * Set: vlInss.
     *
     * @param vlInss the vl inss
     */
    public void setVlInss(BigDecimal vlInss) {
        this.vlInss = vlInss;
    }

    /**
     * Get: vlIof.
     *
     * @return vlIof
     */
    public BigDecimal getVlIof() {
        return vlIof;
    }

    /**
     * Set: vlIof.
     *
     * @param vlIof the vl iof
     */
    public void setVlIof(BigDecimal vlIof) {
        this.vlIof = vlIof;
    }

    /**
     * Get: vlIss.
     *
     * @return vlIss
     */
    public BigDecimal getVlIss() {
        return vlIss;
    }

    /**
     * Set: vlIss.
     *
     * @param vlIss the vl iss
     */
    public void setVlIss(BigDecimal vlIss) {
        this.vlIss = vlIss;
    }

    /**
     * Get: vlMora.
     *
     * @return vlMora
     */
    public BigDecimal getVlMora() {
        return vlMora;
    }

    /**
     * Set: vlMora.
     *
     * @param vlMora the vl mora
     */
    public void setVlMora(BigDecimal vlMora) {
        this.vlMora = vlMora;
    }

    /**
     * Get: vlMulta.
     *
     * @return vlMulta
     */
    public BigDecimal getVlMulta() {
        return vlMulta;
    }

    /**
     * Set: vlMulta.
     *
     * @param vlMulta the vl multa
     */
    public void setVlMulta(BigDecimal vlMulta) {
        this.vlMulta = vlMulta;
    }

    /**
     * Get: vlOutrasEntidades.
     *
     * @return vlOutrasEntidades
     */
    public BigDecimal getVlOutrasEntidades() {
        return vlOutrasEntidades;
    }

    /**
     * Set: vlOutrasEntidades.
     *
     * @param vlOutrasEntidades the vl outras entidades
     */
    public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
        this.vlOutrasEntidades = vlOutrasEntidades;
    }

    /**
     * Get: vlPrincipal.
     *
     * @return vlPrincipal
     */
    public BigDecimal getVlPrincipal() {
        return vlPrincipal;
    }

    /**
     * Set: vlPrincipal.
     *
     * @param vlPrincipal the vl principal
     */
    public void setVlPrincipal(BigDecimal vlPrincipal) {
        this.vlPrincipal = vlPrincipal;
    }

    /**
     * Get: vlrAgendamento.
     *
     * @return vlrAgendamento
     */
    public BigDecimal getVlrAgendamento() {
        return vlrAgendamento;
    }

    /**
     * Set: vlrAgendamento.
     *
     * @param vlrAgendamento the vlr agendamento
     */
    public void setVlrAgendamento(BigDecimal vlrAgendamento) {
        this.vlrAgendamento = vlrAgendamento;
    }

    /**
     * Get: vlReceita.
     *
     * @return vlReceita
     */
    public BigDecimal getVlReceita() {
        return vlReceita;
    }

    /**
     * Set: vlReceita.
     *
     * @param vlReceita the vl receita
     */
    public void setVlReceita(BigDecimal vlReceita) {
        this.vlReceita = vlReceita;
    }

    /**
     * Get: vlrEfetivacao.
     *
     * @return vlrEfetivacao
     */
    public BigDecimal getVlrEfetivacao() {
        return vlrEfetivacao;
    }

    /**
     * Set: vlrEfetivacao.
     *
     * @param vlrEfetivacao the vlr efetivacao
     */
    public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
        this.vlrEfetivacao = vlrEfetivacao;
    }

    /**
     * Get: vlTotal.
     *
     * @return vlTotal
     */
    public BigDecimal getVlTotal() {
        return vlTotal;
    }

    /**
     * Set: vlTotal.
     *
     * @param vlTotal the vl total
     */
    public void setVlTotal(BigDecimal vlTotal) {
        this.vlTotal = vlTotal;
    }

    /**
     * Get: vlTotalPagamentos.
     *
     * @return vlTotalPagamentos
     */
    public BigDecimal getVlTotalPagamentos() {
        return vlTotalPagamentos;
    }

    /**
     * Set: vlTotalPagamentos.
     *
     * @param vlTotalPagamentos the vl total pagamentos
     */
    public void setVlTotalPagamentos(BigDecimal vlTotalPagamentos) {
        this.vlTotalPagamentos = vlTotalPagamentos;
    }

    /**
     * Get: vlTotalPagamentosAutorizados.
     *
     * @return vlTotalPagamentosAutorizados
     */
    public BigDecimal getVlTotalPagamentosAutorizados() {
        return vlTotalPagamentosAutorizados;
    }

    /**
     * Set: vlTotalPagamentosAutorizados.
     *
     * @param vlTotalPagamentosAutorizados the vl total pagamentos autorizados
     */
    public void setVlTotalPagamentosAutorizados(
        BigDecimal vlTotalPagamentosAutorizados) {
        this.vlTotalPagamentosAutorizados = vlTotalPagamentosAutorizados;
    }

    /**
     * Get: dsOrigemPagamento.
     *
     * @return dsOrigemPagamento
     */
    public String getDsOrigemPagamento() {
        return dsOrigemPagamento;
    }

    /**
     * Set: dsOrigemPagamento.
     *
     * @param dsOrigemPagamento the ds origem pagamento
     */
    public void setDsOrigemPagamento(String dsOrigemPagamento) {
        this.dsOrigemPagamento = dsOrigemPagamento;
    }

    /**
     * Get: nossoNumero.
     *
     * @return nossoNumero
     */
    public String getNossoNumero() {
        return nossoNumero;
    }

    /**
     * Set: nossoNumero.
     *
     * @param nossoNumero the nosso numero
     */
    public void setNossoNumero(String nossoNumero) {
        this.nossoNumero = nossoNumero;
    }

    /**
     * Is disable argumentos consulta.
     *
     * @return true, if is disable argumentos consulta
     */
    public boolean isDisableArgumentosConsulta() {
        return disableArgumentosConsulta;
    }

    /**
     * Set: disableArgumentosConsulta.
     *
     * @param disableArgumentosConsulta the disable argumentos consulta
     */
    public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
        this.disableArgumentosConsulta = disableArgumentosConsulta;
    }

    /**
     * Is exibe beneficiario.
     *
     * @return true, if is exibe beneficiario
     */
    public boolean isExibeBeneficiario() {
        return exibeBeneficiario;
    }

    /**
     * Set: exibeBeneficiario.
     *
     * @param exibeBeneficiario the exibe beneficiario
     */
    public void setExibeBeneficiario(boolean exibeBeneficiario) {
        this.exibeBeneficiario = exibeBeneficiario;
    }

    /**
     * Get: carteira.
     *
     * @return carteira
     */
    public String getCarteira() {
        return carteira;
    }

    /**
     * Set: carteira.
     *
     * @param carteira the carteira
     */
    public void setCarteira(String carteira) {
        this.carteira = carteira;
    }

    /**
     * Get: cdIndentificadorJudicial.
     *
     * @return cdIndentificadorJudicial
     */
    public String getCdIndentificadorJudicial() {
        return cdIndentificadorJudicial;
    }

    /**
     * Set: cdIndentificadorJudicial.
     *
     * @param cdIndentificadorJudicial the cd indentificador judicial
     */
    public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
        this.cdIndentificadorJudicial = cdIndentificadorJudicial;
    }

    /**
     * Get: cdSituacaoTranferenciaAutomatica.
     *
     * @return cdSituacaoTranferenciaAutomatica
     */
    public String getCdSituacaoTranferenciaAutomatica() {
        return cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Set: cdSituacaoTranferenciaAutomatica.
     *
     * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
     */
    public void setCdSituacaoTranferenciaAutomatica(
        String cdSituacaoTranferenciaAutomatica) {
        this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Get: dtLimiteDescontoPagamento.
     *
     * @return dtLimiteDescontoPagamento
     */
    public String getDtLimiteDescontoPagamento() {
        return dtLimiteDescontoPagamento;
    }

    /**
     * Set: dtLimiteDescontoPagamento.
     *
     * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
     */
    public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
        this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
    }

    /**
     * Get: nrRemessa.
     *
     * @return nrRemessa
     */
    public Long getNrRemessa() {
        return nrRemessa;
    }

    /**
     * Set: nrRemessa.
     *
     * @param nrRemessa the nr remessa
     */
    public void setNrRemessa(Long nrRemessa) {
        this.nrRemessa = nrRemessa;
    }

    /**
     * Get: descTipoFavorecido.
     *
     * @return descTipoFavorecido
     */
    public String getDescTipoFavorecido() {
        return descTipoFavorecido;
    }

    /**
     * Set: descTipoFavorecido.
     *
     * @param descTipoFavorecido the desc tipo favorecido
     */
    public void setDescTipoFavorecido(String descTipoFavorecido) {
        this.descTipoFavorecido = descTipoFavorecido;
    }

    /**
     * Get: dsTipoBeneficiario.
     *
     * @return dsTipoBeneficiario
     */
    public String getDsTipoBeneficiario() {
        return dsTipoBeneficiario;
    }

    /**
     * Set: dsTipoBeneficiario.
     *
     * @param dsTipoBeneficiario the ds tipo beneficiario
     */
    public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
        this.dsTipoBeneficiario = dsTipoBeneficiario;
    }

    /**
     * Get: cdBancoOriginal.
     *
     * @return cdBancoOriginal
     */
    public Integer getCdBancoOriginal() {
        return cdBancoOriginal;
    }

    /**
     * Set: cdBancoOriginal.
     *
     * @param cdBancoOriginal the cd banco original
     */
    public void setCdBancoOriginal(Integer cdBancoOriginal) {
        this.cdBancoOriginal = cdBancoOriginal;
    }

    /**
     * Get: dsBancoOriginal.
     *
     * @return dsBancoOriginal
     */
    public String getDsBancoOriginal() {
        return dsBancoOriginal;
    }

    /**
     * Set: dsBancoOriginal.
     *
     * @param dsBancoOriginal the ds banco original
     */
    public void setDsBancoOriginal(String dsBancoOriginal) {
        this.dsBancoOriginal = dsBancoOriginal;
    }

    /**
     * Get: cdAgenciaBancariaOriginal.
     *
     * @return cdAgenciaBancariaOriginal
     */
    public Integer getCdAgenciaBancariaOriginal() {
        return cdAgenciaBancariaOriginal;
    }

    /**
     * Set: cdAgenciaBancariaOriginal.
     *
     * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
     */
    public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
        this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
    }

    /**
     * Get: cdDigitoAgenciaOriginal.
     *
     * @return cdDigitoAgenciaOriginal
     */
    public String getCdDigitoAgenciaOriginal() {
        return cdDigitoAgenciaOriginal;
    }

    /**
     * Set: cdDigitoAgenciaOriginal.
     *
     * @param cdDigitoAgenciaOriginal the cd digito agencia original
     */
    public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
        this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
    }

    /**
     * Get: dsAgenciaOriginal.
     *
     * @return dsAgenciaOriginal
     */
    public String getDsAgenciaOriginal() {
        return dsAgenciaOriginal;
    }

    /**
     * Set: dsAgenciaOriginal.
     *
     * @param dsAgenciaOriginal the ds agencia original
     */
    public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
        this.dsAgenciaOriginal = dsAgenciaOriginal;
    }

    /**
     * Get: cdContaBancariaOriginal.
     *
     * @return cdContaBancariaOriginal
     */
    public Long getCdContaBancariaOriginal() {
        return cdContaBancariaOriginal;
    }

    /**
     * Set: cdContaBancariaOriginal.
     *
     * @param cdContaBancariaOriginal the cd conta bancaria original
     */
    public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
        this.cdContaBancariaOriginal = cdContaBancariaOriginal;
    }

    /**
     * Get: cdDigitoContaOriginal.
     *
     * @return cdDigitoContaOriginal
     */
    public String getCdDigitoContaOriginal() {
        return cdDigitoContaOriginal;
    }

    /**
     * Set: cdDigitoContaOriginal.
     *
     * @param cdDigitoContaOriginal the cd digito conta original
     */
    public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
        this.cdDigitoContaOriginal = cdDigitoContaOriginal;
    }

    /**
     * Get: dsTipoContaOriginal.
     *
     * @return dsTipoContaOriginal
     */
    public String getDsTipoContaOriginal() {
        return dsTipoContaOriginal;
    }

    /**
     * Set: dsTipoContaOriginal.
     *
     * @param dsTipoContaOriginal the ds tipo conta original
     */
    public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
        this.dsTipoContaOriginal = dsTipoContaOriginal;
    }

    /**
     * Get: dtDevolucaoEstorno.
     *
     * @return dtDevolucaoEstorno
     */
    public String getDtDevolucaoEstorno() {
        return dtDevolucaoEstorno;
    }

    /**
     * Set: dtDevolucaoEstorno.
     *
     * @param dtDevolucaoEstorno the dt devolucao estorno
     */
    public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
        this.dtDevolucaoEstorno = dtDevolucaoEstorno;
    }

    /**
     * Get: dtLimitePagamento.
     *
     * @return dtLimitePagamento
     */
    public String getDtLimitePagamento() {
        return dtLimitePagamento;
    }

    /**
     * Set: dtLimitePagamento.
     *
     * @param dtLimitePagamento the dt limite pagamento
     */
    public void setDtLimitePagamento(String dtLimitePagamento) {
        this.dtLimitePagamento = dtLimitePagamento;
    }

    /**
     * Get: dsRastreado.
     *
     * @return dsRastreado
     */
    public String getDsRastreado() {
        return dsRastreado;
    }

    /**
     * Set: dsRastreado.
     *
     * @param dsRastreado the ds rastreado
     */
    public void setDsRastreado(String dsRastreado) {
        this.dsRastreado = dsRastreado;
    }

    /**
     * Get: dtFloatingPagamento.
     *
     * @return dtFloatingPagamento
     */
    public String getDtFloatingPagamento() {
        return dtFloatingPagamento;
    }

    /**
     * Set: dtFloatingPagamento.
     *
     * @param dtFloatingPagamento the dt floating pagamento
     */
    public void setDtFloatingPagamento(String dtFloatingPagamento) {
        this.dtFloatingPagamento = dtFloatingPagamento;
    }

    /**
     * Get: dtEfetivFloatPgto.
     *
     * @return dtEfetivFloatPgto
     */
    public String getDtEfetivFloatPgto() {
        return dtEfetivFloatPgto;
    }

    /**
     * Set: dtEfetivFloatPgto.
     *
     * @param dtEfetivFloatPgto the dt efetiv float pgto
     */
    public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
        this.dtEfetivFloatPgto = dtEfetivFloatPgto;
    }

    /**
     * Get: vlFloatingPagamento.
     *
     * @return vlFloatingPagamento
     */
    public BigDecimal getVlFloatingPagamento() {
        return vlFloatingPagamento;
    }

    /**
     * Set: vlFloatingPagamento.
     *
     * @param vlFloatingPagamento the vl floating pagamento
     */
    public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
        this.vlFloatingPagamento = vlFloatingPagamento;
    }

    /**
     * Get: cdOperacaoDcom.
     *
     * @return cdOperacaoDcom
     */
    public Long getCdOperacaoDcom() {
        return cdOperacaoDcom;
    }

    /**
     * Set: cdOperacaoDcom.
     *
     * @param cdOperacaoDcom the cd operacao dcom
     */
    public void setCdOperacaoDcom(Long cdOperacaoDcom) {
        this.cdOperacaoDcom = cdOperacaoDcom;
    }

    /**
     * Get: dsSituacaoDcom.
     *
     * @return dsSituacaoDcom
     */
    public String getDsSituacaoDcom() {
        return dsSituacaoDcom;
    }

    /**
     * Set: dsSituacaoDcom.
     *
     * @param dsSituacaoDcom the ds situacao dcom
     */
    public void setDsSituacaoDcom(String dsSituacaoDcom) {
        this.dsSituacaoDcom = dsSituacaoDcom;
    }

    /**
     * Get: exibeDcom.
     *
     * @return exibeDcom
     */
    public Boolean getExibeDcom() {
        return exibeDcom;
    }

    /**
     * Set: exibeDcom.
     *
     * @param exibeDcom the exibe dcom
     */
    public void setExibeDcom(Boolean exibeDcom) {
        this.exibeDcom = exibeDcom;
    }

    /**
     * Get: numControleInternoLote.
     *
     * @return numControleInternoLote
     */
    public Long getNumControleInternoLote() {
        return numControleInternoLote;
    }

    /**
     * Set: numControleInternoLote.
     *
     * @param numControleInternoLote the num controle interno lote
     */
    public void setNumControleInternoLote(Long numControleInternoLote) {
        this.numControleInternoLote = numControleInternoLote;
    }
}
