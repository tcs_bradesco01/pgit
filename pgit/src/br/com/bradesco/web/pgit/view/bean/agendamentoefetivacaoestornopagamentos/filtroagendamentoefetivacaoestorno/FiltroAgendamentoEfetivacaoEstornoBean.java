/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarClientesMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarClientesMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratoMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratoMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratosClienteMarqEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarContratosClienteMarqSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: FiltroAgendamentoEfetivacaoEstornoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FiltroAgendamentoEfetivacaoEstornoBean {
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo filtroIdentificaoService. */
	private IFiltroIdentificaoService filtroIdentificaoService;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoImpl. */
	private IFiltroAgendamentoEfetivacaoEstornoService filtroAgendamentoEfetivacaoEstornoImpl;	
	
	/** Atributo entradaConsultarListaClientePessoas. */
	private ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoas;
	
	/** Atributo saidaConsultarListaClientePessoas. */
	private ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoas;
	
	/** Atributo listaConsultarListaClientePessoas. */
	private List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoas;
	
	/** Atributo listaControleRadioClientes. */
	private List<SelectItem> listaControleRadioClientes = new ArrayList<SelectItem>();	
	
	/** Atributo listaConsultarListaContratosPessoas. */
	private List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas;
	
	/** Atributo consultarPagamentosIndividualServiceImpl. */
	private IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl;
	
	/** Atributo manterClientesOrgaoPublicoService. */
	private IManterClientesOrgaoPublicoService manterClientesOrgaoPublicoService;
	
	/** Atributo empresaGestoraFiltro. */
	private Long empresaGestoraFiltro;
	
	/** Atributo tipoContratoFiltro. */
	private Integer tipoContratoFiltro;
	
	/** Atributo numeroFiltro. */
	private String numeroFiltro;
	
	/** Atributo agenciaOperadoraFiltro. */
	private String agenciaOperadoraFiltro;
	
	/** Atributo situacaoFiltro. */
	private Integer situacaoFiltro;	
	
	/** Atributo dsNomeRazaoSocialParticipante. */
	private String dsNomeRazaoSocialParticipante; 
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;
	
	/** Atributo cpfCnpjFormatado. */
	private String cpfCnpjFormatado;
	
	/** Atributo itemFiltroSelecionado. */
	private String itemFiltroSelecionado;
	
	/** Atributo itemClienteSelecionado. */
	private Integer itemClienteSelecionado;
	
	/** Atributo itemClienteSelecionadoContrato. */
	private Integer itemClienteSelecionadoContrato;	
	
	/** Atributo paginaRetorno. */
	private String paginaRetorno;
	
	/** Atributo tipoFiltroSelecionado. */
	private String tipoFiltroSelecionado;
	
	/** Atributo tipoAntecipacao. */
	private Integer tipoAntecipacao;
	
	/** Atributo tipoRastreadoSelecionado. */
	private String tipoRastreadoSelecionado;
	
	/** Atributo cpfCnpjFiltroDesc. */
	private String cpfCnpjFiltroDesc;
	
	/** Atributo nomeRazaoFiltroDesc. */
	private String nomeRazaoFiltroDesc;

	/** Atributo empresaGestoraDescContrato. */
	private String empresaGestoraDescContrato;
	
	/** Atributo numeroDescContrato. */
	private String numeroDescContrato;
	
	/** Atributo descricaoTipoContratoDescContrato. */
	private String descricaoTipoContratoDescContrato;
	
	/** Atributo descricaoContratoDescContrato. */
	private String descricaoContratoDescContrato;
	
	/** Atributo situacaoDescContrato. */
	private String situacaoDescContrato;
	
	/** Atributo empresaGestoraDescCliente. */
	private String empresaGestoraDescCliente;
	
	/** Atributo numeroDescCliente. */
	private String numeroDescCliente;
	
	/** Atributo descricaoContratoDescCliente. */
	private String descricaoContratoDescCliente;
	
	/** Atributo situacaoDescCliente. */
	private String situacaoDescCliente;
	
	/** Atributo nrCpfCnpjCliente. */
	private String nrCpfCnpjCliente;
	
	/** Atributo descricaoRazaoSocialCliente. */
	private String descricaoRazaoSocialCliente;
	
	/** Atributo descricaoBancoContaDebito. */
	private String descricaoBancoContaDebito;
	
	/** Atributo descricaoAgenciaContaDebitoBancaria. */
	private String descricaoAgenciaContaDebitoBancaria;
	
	/** Atributo descricaoContaDebito. */
	private Long descricaoContaDebito;
	
	/** Atributo descricaoTipoContaDebito. */
	private String descricaoTipoContaDebito;
	
	/* Filtro Conta D�bito */
	/** Atributo bancoContaDebitoFiltro. */
	private Integer bancoContaDebitoFiltro;
	
	/** Atributo agenciaContaDebitoBancariaFiltro. */
	private Integer agenciaContaDebitoBancariaFiltro;
	
	/** Atributo contaBancariaContaDebitoFiltro. */
	private Long contaBancariaContaDebitoFiltro;
	
	/** Atributo digitoContaDebitoFiltro. */
	private String digitoContaDebitoFiltro;

	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora= new ArrayList<SelectItem>();
	
	/** Atributo listaEmpresaGestoraHash. */
	private Map<Long,String> listaEmpresaGestoraHash = new HashMap<Long,String>();
	
	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio; 
	
	/** Atributo clienteContratoSelecionado. */
	private boolean clienteContratoSelecionado;
	
	/** Atributo codPessoaParticipante. */
	private Long codPessoaParticipante;
	
	/** Atributo saidaValidarContratoPagSalPartOrgaoPublico. */
	private ValidarContratoPagSalPartOrgaoPublicoSaidaDTO saidaValidarContratoPagSalPartOrgaoPublico;

	/** Atributo empresaGestoraDescContratoOrgao. */
	private String empresaGestoraDescContratoOrgao;
	
	/** Atributo numeroDescContratoOrgao. */
	private String numeroDescContratoOrgao;
	
	/** Atributo descricaoContratoDescContratoOrgao. */
	private String descricaoContratoDescContratoOrgao;
	
	/** Atributo situacaoDescContratoOrgao. */
	private String situacaoDescContratoOrgao;
	
	/* Atributos Filtro Marq */
	// pdc -> marq.consultas.listarClientesMarq
	/** Atributo listaGridListaClientesMarq. */
	private List<ListarClientesMarqSaidaDTO> listaGridListaClientesMarq;	
	
	/** Atributo itemSelecionadoListaClientesMarq. */
	private Integer itemSelecionadoListaClientesMarq;
	
	/** Atributo listaControleRadioClientesMarq. */
	private List<SelectItem> listaControleRadioClientesMarq = new ArrayList<SelectItem>();
	
	//pdc -> marq.consultas.listarContratosClienteMarq
	/** Atributo listaContratosClienteMarq. */
	private List<ListarContratosClienteMarqSaidaDTO> listaContratosClienteMarq;	
	
	/** Atributo itemSelecionadoListaContratosClienteMarq. */
	private Integer itemSelecionadoListaContratosClienteMarq;
	
	/** Atributo listaControleRadioContratosClienteMarq. */
	private List<SelectItem> listaControleRadioContratosClienteMarq = new ArrayList<SelectItem>();
	
	//pdc -> marq.consultas.listarContratosMarq
	/** Atributo listarContratosMarq. */
	private List<ListarContratoMarqSaidaDTO> listarContratosMarq;	
	
	/** Atributo itemSelecionadoListaContratosMarq. */
	private Integer itemSelecionadoListaContratosMarq;
	
	/** Atributo listaControleRadioContratosMarq. */
	private List<SelectItem> listaControleRadioContratosMarq = new ArrayList<SelectItem>();
	
	//pdc -> pgit.consultas.consultarContratoClienteFiltro
	/** Atributo listaContratoCliente. */
	private List<ConsultarContratoClienteFiltroSaidaDTO> listaContratoCliente;	
	
	/** Atributo itemSelecionadoListaContratoCliente. */
	private Integer itemSelecionadoListaContratoCliente;
	
	/** Atributo listaControleRadioContratoCliente. */
	private List<SelectItem> listaControleRadioContratoCliente = new ArrayList<SelectItem>();
	
	/* Perfil */
	/** Atributo listaConsultarListaClientePessoasPerfil. */
	private List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoasPerfil;
	
	/** Atributo itemSelecionadoListaClientePerfil. */
	private Integer itemSelecionadoListaClientePerfil;
	
	/** Atributo listaControleRadioClientePerfil. */
	private List<SelectItem> listaControleRadioClientePerfil = new ArrayList<SelectItem>();
	
	//pdc -> pgit.consultas.consultarContratoClienteFiltro
	/** Atributo listaContratoClientePerfil. */
	private List<ConsultarContratoClienteFiltroSaidaDTO> listaContratoClientePerfil;	
	
	/** Atributo itemSelecionadoListaContratoClientePerfil. */
	private Integer itemSelecionadoListaContratoClientePerfil;
	
	/** Atributo listaControleRadioContratoClientePerfil. */
	private List<SelectItem> listaControleRadioContratoClientePerfil = new ArrayList<SelectItem>();	
	
	/** Atributo listarPerfilTrocaArquivo. */
	private List<ListarPerfilTrocaArquivoSaidaDTO> listarPerfilTrocaArquivo;	
	
	/** Atributo itemSelecionadoListaPerfilTroca. */
	private Integer itemSelecionadoListaPerfilTroca;
	
	/** Atributo listaControleRadioPerfilTroca. */
	private List<SelectItem> listaControleRadioPerfilTroca = new ArrayList<SelectItem>();
	
	//Perfil  - Filtrar por contrato
	/** Atributo listaConsultarListaContratosPessoasPerfil. */
	private List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoasPerfil;
	
	/** Atributo itemClienteSelecionadoContratoPerfil. */
	private Integer itemClienteSelecionadoContratoPerfil;		
	
	/** Atributo listaControleRadioContratosPessoasPerfil. */
	private List<SelectItem> listaControleRadioContratosPessoasPerfil = new ArrayList<SelectItem>();	
	
	//Atributos perfil
    /** Atributo cdPessoaPerfil. */
	private Long cdPessoaPerfil;
    
    /** Atributo cdCpfCnpjPessoaPerfil. */
    private String cdCpfCnpjPessoaPerfil;
    
    /** Atributo dsPessoaPerfil. */
    private String dsPessoaPerfil;
    
    /** Atributo cdPerfilTrocaArquivoPerfilCliente. */
    private Long cdPerfilTrocaArquivoPerfilCliente;    
    
    /** Atributo dsSituacaoPerfilCliente. */
    private String dsSituacaoPerfilCliente;
    
    /** Atributo cdPerfilTrocaArquivoPerfilContrato. */
    private Long cdPerfilTrocaArquivoPerfilContrato;
    
    /** Atributo dsSituacaoPerfilContrato. */
    private String dsSituacaoPerfilContrato;
    
	/* Atributo que controla se a p�gina "identificacaoClienteContratoAgendamento vai voltar para ela Principal ou
	 * Para tela de consulta de cliente (identificacaoClienteAgendamento)
	 */
	/** Atributo telaVoltarIdentificacaoClienteContrato. */
	private String telaVoltarIdentificacaoClienteContrato;
	
	/** Atributo telaVoltarIdentificacaoClienteContratoMarq. */
	private String telaVoltarIdentificacaoClienteContratoMarq;
	
	/** Atributo telaVoltarIdentificacaoPerfil. */
	private String telaVoltarIdentificacaoPerfil;	
	
	/** Atributo cpf. */
	private String cpf;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;
	
	//Pesquisa de Participante 
	
	/** Atributo codigoParticipanteFiltro. */
	private Long codigoParticipanteFiltro;
	
	/** Atributo nomeParticipanteFiltro. */
	private String nomeParticipanteFiltro;
	
	/** Atributo cpfCnpjParticipanteFiltro. */
	private String cpfCnpjParticipanteFiltro;
	
	/** Atributo corpoCpfCnpjParticipanteFiltro. */
	private Long corpoCpfCnpjParticipanteFiltro;
	
	/** Atributo filialCpfCnpjParticipanteFiltro. */
	private Integer filialCpfCnpjParticipanteFiltro;
	
	/** Atributo controleCpfCnpjParticipanteFiltro. */
	private Integer controleCpfCnpjParticipanteFiltro;
	
	/** Atributo listaParticipantes. */
	private List<ConsultarParticipanteContratoSaidaDTO>  listaParticipantes = new ArrayList<ConsultarParticipanteContratoSaidaDTO>();
	
	/** Atributo listaParticipantesControleRadio. */
	private List<SelectItem> listaParticipantesControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoListaParcipantes. */
	private Integer itemSelecionadoListaParcipantes;
	
	/** Atributo voltarParticipante. */
	private String voltarParticipante;
	
	/** Atributo sinalizaOrgaoPublico. */
	private boolean sinalizaOrgaoPublico = false;
	
	/* M�todos */
	
	
	/**
	 * Filtro agendamento efetivacao estorno bean.
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean() {
		super();
	}
	
	/**
	 * Carrega descricao conta.
	 */
	public void carregaDescricaoConta(){
	
		try{
			ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
			consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(getBancoContaDebitoFiltro());
			consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(getAgenciaContaDebitoBancariaFiltro());
			consultarDescBancoAgenciaContaEntradaDTO.setCdConta(getContaBancariaContaDebitoFiltro());
			consultarDescBancoAgenciaContaEntradaDTO.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
			ConsultarDescBancoAgenciaContaSaidaDTO  consultarDescBancoAgenciaContaSaidaDTO = getConsultasService().consultarDescBancoAgenciaConta(consultarDescBancoAgenciaContaEntradaDTO);
			setDescricaoTipoContaDebito(consultarDescBancoAgenciaContaSaidaDTO.getDsTipoConta());
			setDescricaoBancoContaDebito(consultarDescBancoAgenciaContaSaidaDTO.getDsBanco());
			setDescricaoAgenciaContaDebitoBancaria(consultarDescBancoAgenciaContaSaidaDTO.getDsAgencia());
		}catch(PdcAdapterFunctionalException e){
			setDescricaoTipoContaDebito("");
			setDescricaoBancoContaDebito("");
			setDescricaoAgenciaContaDebitoBancaria("");
		}
	}
	

	/**
	 * Limpar tela.
	 */
	public void limparTela() {
		limparFiltroPrincipal();
		listarEmpresaGestora();
		listarTipoContrato();
		setClienteContratoSelecionado(false);
	}	
	
	/**
	 * Limpar filtro principal.
	 */
	public void limparFiltroPrincipal(){
		setItemFiltroSelecionado("");
		limpar();
		
		if (getTipoFiltroSelecionado()!=null && getTipoFiltroSelecionado().equals("2")){
			setBancoContaDebitoFiltro(237);
		}
	}
	
	/**
	 * Limpar.
	 */
	public void limpar(){
		limparDadosContrato();
		limparCamposCliente();
		limparDadosContaDebito();
		limparDadosPerfil();
		setClienteContratoSelecionado(false);
		
		setCdPessoaJuridicaContrato(null);
		setCdTipoContratoNegocio(null);
		setNrSequenciaContratoNegocio(null);
		
		if(getItemFiltroSelecionado() != null && getItemFiltroSelecionado().equals("3")){
			getEntradaConsultarListaClientePessoas().setCdBanco(237);
		}
			
			
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());
		
		
	}
	
	/**
	 * Limpar dados cliente.
	 *
	 * @return the string
	 */
	public String limparDadosCliente(){		
		setItemFiltroSelecionado(null);
		setItemClienteSelecionado(null);
		setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		return getPaginaRetorno();
	}
	
	/**
	 * Limpar dados contrato.
	 */
	public void limparDadosContrato(){
		setEmpresaGestoraFiltro(2269651L);
		setTipoContratoFiltro(null);
		setNumeroFiltro("");
		setEmpresaGestoraDescContrato("");
		setNumeroDescContrato("");
		setDescricaoContratoDescContrato("");
		setSituacaoDescContrato("");
		setEmpresaGestoraDescContratoOrgao("");
		setNumeroDescContratoOrgao("");
		setDescricaoContratoDescContratoOrgao("");
		setSituacaoDescContratoOrgao("");
	}
	
	/**
	 * Limpar dados conta debito.
	 */
	public void limparDadosContaDebito(){
		setBancoContaDebitoFiltro(null);
		setAgenciaContaDebitoBancariaFiltro(null);
		setContaBancariaContaDebitoFiltro(null);
		setDigitoContaDebitoFiltro("");
	}
	
	/**
	 * Get: selectItemContratoCliente.
	 *
	 * @return selectItemContratoCliente
	 */
	public List<SelectItem> getSelectItemContratoCliente() {
		if (this.getListaContratoCliente() == null || this.getListaContratoCliente().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaContratoCliente().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}	
	
	/**
	 * Get: selectItemContrato.
	 *
	 * @return selectItemContrato
	 */
	public List<SelectItem> getSelectItemContrato() {
		if (this.getListaConsultarListaContratosPessoas() == null || this.getListaConsultarListaContratosPessoas().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaConsultarListaContratosPessoas().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}		
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
		try{				
			ConsultarListaContratosPessoasEntradaDTO entrada = new ConsultarListaContratosPessoasEntradaDTO();
			entrada.setCdClub(0L);
			entrada.setCdPessoaJuridicaContrato(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro() : 0L);
			entrada.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro() : 0);
			entrada.setNrSeqContratoNegocio(!getNumeroFiltro().equals("") ? Long.parseLong(getNumeroFiltro()) : 0);
			
			setListaConsultarListaContratosPessoas(getFiltroIdentificaoService().pesquisarContratos(entrada));
			
			setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoas().get(0).getCdPessoaJuridicaContrato());
			setCdTipoContratoNegocio(getListaConsultarListaContratosPessoas().get(0).getCdTipoContratoNegocio());
			setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoas().get(0).getNrSeqContratoNegocio());
			
			if(isSinalizaOrgaoPublico()){
				 ValidarContratoPagSalPartOrgaoPublicoEntradaDTO entradaValida = new ValidarContratoPagSalPartOrgaoPublicoEntradaDTO();
				 saidaValidarContratoPagSalPartOrgaoPublico = new ValidarContratoPagSalPartOrgaoPublicoSaidaDTO();
			
				 entradaValida.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(getCdPessoaJuridicaContrato()));
				 entradaValida.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(getCdTipoContratoNegocio()));
				 entradaValida.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(getNrSequenciaContratoNegocio()));
				 
				try{
					saidaValidarContratoPagSalPartOrgaoPublico = getManterClientesOrgaoPublicoService().validarParticipanteOrgaoPublico(entradaValida);
					
					if(saidaValidarContratoPagSalPartOrgaoPublico.getCodMensagem() != null && !"PGIT0040".equals(saidaValidarContratoPagSalPartOrgaoPublico.getCodMensagem())){
						BradescoFacesUtils.addInfoModalMessage("(" + saidaValidarContratoPagSalPartOrgaoPublico.getCodMensagem() + ") " + saidaValidarContratoPagSalPartOrgaoPublico.getMensagem(), false);
						limparCamposContratoOrgao();
						
						return "";
					}
					
				}catch(PdcAdapterFunctionalException p){
					BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
					limparCamposContratoOrgao();
					
				    return"";
				}
			}
			
			if(getListaConsultarListaContratosPessoas().size() > 1){
				setItemClienteSelecionadoContrato(null);
				return "identificacaoContratoAgendamento";
			}
			

			ConsultarListaContratosPessoasSaidaDTO saida = getListaConsultarListaContratosPessoas().get(0);

			setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(saida.getCdCpfCnpjParticipante(), saida.getCdFilialCnpjParticipante(), saida.getCdControleCpfCnpjParticipante()));
			setNomeRazaoParticipante(saida.getDsPessoaParticipante());
			
			//setEmpresaGestoraDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsPessoaJuridicaContrato());
			setEmpresaGestoraDescContrato(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
			setNumeroDescContrato(Long.toString(getListaConsultarListaContratosPessoas().get(0).getNrSeqContratoNegocio()));
			setDescricaoContratoDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsContrato());
			setSituacaoDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsSituacaoContratoNegocio());
			setDescricaoTipoContratoDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsTipoContratoNegocio());
			setClienteContratoSelecionado(true);	
			
			setEmpresaGestoraDescContratoOrgao(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
			setNumeroDescContratoOrgao(Long.toString(getListaConsultarListaContratosPessoas().get(0).getNrSeqContratoNegocio()));
			setDescricaoContratoDescContratoOrgao(getListaConsultarListaContratosPessoas().get(0).getDsContrato());
			setSituacaoDescContratoOrgao(getListaConsultarListaContratosPessoas().get(0).getDsSituacaoContratoNegocio());
			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaConsultarListaContratosPessoas(null);
			setItemClienteSelecionadoContrato(null);
			setClienteContratoSelecionado(false);
			setCdPessoaJuridicaContrato(null);
			setCdTipoContratoNegocio(null);
			setNrSequenciaContratoNegocio(null);			
			setEmpresaGestoraDescContrato("");
			setNumeroDescContrato("");
			setDescricaoContratoDescContrato("");
			setSituacaoDescContrato("");
			setEmpresaGestoraDescContratoOrgao("");
			setNumeroDescContratoOrgao("");
			setDescricaoContratoDescContratoOrgao("");
			setSituacaoDescContratoOrgao("");
		}
		
		return "";
	}
	
	/**
	 * Limpar campos contrato orgao.
	 */
	public void limparCamposContratoOrgao(){
		setEmpresaGestoraDescContratoOrgao("");
		setNumeroDescContratoOrgao("");
		setDescricaoContratoDescContratoOrgao("");
		setSituacaoDescContratoOrgao("");
	}
	
	/**
	 * Voltar contrato.
	 *
	 * @return the string
	 */
	public String voltarContrato(){
		return this.getPaginaRetorno();
	}
	
	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora(){
		try {
			this.listaEmpresaGestora = new ArrayList<SelectItem>();
			List<EmpresaConglomeradoSaidaDTO> list = new ArrayList<EmpresaConglomeradoSaidaDTO>();
			
			EmpresaConglomeradoEntradaDTO entrada = new EmpresaConglomeradoEntradaDTO();
			entrada.setCdSituacao(0);
			entrada.setNrOcorrencias(180);
			
			list = comboService.listarEmpresaConglomerado(entrada);
			listaEmpresaGestora.clear();
			
			if (list.size() == 0 || list.size() > 1){
				listaEmpresaGestora.add(new SelectItem(new Long(0), MessageHelperUtils.getI18nMessage("label_combo_selecione")));
			}
			for(EmpresaConglomeradoSaidaDTO combo : list){
				listaEmpresaGestora.add(new SelectItem(combo.getCodClub(), combo.getCodClub() + " - " + combo.getDsRazaoSocial()));
				listaEmpresaGestoraHash.put(combo.getCodClub(), combo.getDsRazaoSocial());
			}			
		}catch (PdcAdapterFunctionalException p){
			listaEmpresaGestora = new ArrayList<SelectItem>();
			listaEmpresaGestoraHash = new HashMap<Long,String>();
		}
	}	
	
	/**
	 * Voltar cliente.
	 *
	 * @return the string
	 */
	public String voltarCliente(){
		return getPaginaRetorno();
	}	
	
	/**
	 * Voltar contrato cliente.
	 *
	 * @return the string
	 */
	public String voltarContratoCliente(){
		return getTelaVoltarIdentificacaoClienteContrato(); 
	}	
	
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato(){
		try{
			listaTipoContrato.clear();
			List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();
	
			if(list.size() == 0 || list.size() > 1  ){
				listaEmpresaGestora.add(new SelectItem(0, MessageHelperUtils.getI18nMessage("label_combo_selecione")));
			}
			for (TipoContratoSaidaDTO saida : list){
				listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
			}
		}catch (PdcAdapterFunctionalException p){
			listaTipoContrato = new ArrayList<SelectItem>();
		}		
	}	
	
	/**
	 * Consultar clientes.
	 *
	 * @return the string
	 */
	public String consultarClientes(){
		setTelaVoltarIdentificacaoClienteContrato("");
		
		setItemClienteSelecionado(null);
		
		try {
			/* Foi criado outro Objeto para n�o setar os campos "banco" e "agencia da tela ,e sim apenas na hora de mandar ao PDC
			 * Este Objeto recebe o clone do Objeto que esta na tela */
			ConsultarListaClientePessoasEntradaDTO entradaTela;
			try{
				entradaTela = (ConsultarListaClientePessoasEntradaDTO) entradaConsultarListaClientePessoas.clone();
				
				//TODO Por telefone, Samuel informou para deixarmos o campo digito na tela,mas o PDC ainda n�o recebe, ele ia confirmar com a Beatriz
				entradaTela.setCdDigitoConta("");
				
			}catch(CloneNotSupportedException erro){
				entradaTela = new ConsultarListaClientePessoasEntradaDTO();
			}
			
			/* Caso o Filtro do cliente n�o seja pelo banco/agencia/conta, eu limpo os campos banco e agencia, pois eles podem estar preenchidos
			 com os valores retornados pelo PDC VerificaUsuarioLogado, que � o controle do cdGestorSistema */
			if(!getItemFiltroSelecionado().equals("3")){
				entradaTela.setCdBanco(0);
				entradaTela.setCdAgenciaBancaria(0);
			}
			//Efetua busca do cliente (pgic.favorecidos.manterFavorecidos.consultarListaClientePessoas)
			pesquisarClientes(entradaTela);
			
			this.listaControleRadioClientes =  new ArrayList<SelectItem>();
			for (int i = 0; i < getListaConsultarListaClientePessoas().size();i++) {
				this.listaControleRadioClientes.add(new SelectItem(i," "));
			}
			
			//Se a pesquisa retornar mais de um resultado
			if ( getListaConsultarListaClientePessoas().size() > 1 ){
				
				//Tela de contrato vai voltar para tela de Cliente
				setTelaVoltarIdentificacaoClienteContrato("identificacaoClienteAgendamento");
				return "identificacaoClienteAgendamento";	
			}
			else{
				//Se a pesquisa retornar apenas um resultado
				
				//Tela de contrato vai voltar para tela de Pesquisa (Filtro)
				setTelaVoltarIdentificacaoClienteContrato(getPaginaRetorno());
				if ( getListaConsultarListaClientePessoas().size() == 1 ){
					//Primeiro e unico registro
					setItemClienteSelecionado(0);				
					
					return efetuarPesquisarContrato();
				}
			}
			
			return "";
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "  + p.getMessage(), "/" + this.getPaginaRetorno() + ".jsf", BradescoViewExceptionActionType.PATH, false);
			limparAtributosCliente();
			setItemClienteSelecionado(null);
			setListaConsultarListaClientePessoas(null);
			return "";
		}
	}
	
	//M�todo chamado pelo bot�o "Selecionar" da tela "identificacaoClienteAgendamento" e dentro do m�todo consultarClientes
	/**
	 * Efetuar pesquisar contrato.
	 *
	 * @return the string
	 */
	public String efetuarPesquisarContrato(){
		try {
			//Efetua a busca do contrato - PGITIAHT (Pdc: pgit.consultas.consultarContratoClienteFiltro
			ConsultarContratoClienteFiltroEntradaDTO entrada = new ConsultarContratoClienteFiltroEntradaDTO();
			entrada.setCdClubPessoa(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getCdClub());
			entrada.setCdControleCpfPssoa(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getCdControleCnpj());
			entrada.setCdCpfCnpjPssoa(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getCdCpfCnpj());
			entrada.setCdFilialCnpjPssoa(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getCdFilialCnpj());
			
			setItemSelecionadoListaContratoCliente(null);
			setListaContratoCliente(getFiltroAgendamentoEfetivacaoEstornoImpl().consultarContratoClienteFiltro(entrada));
			
			return pesquisaContrato();
			
		} catch (PdcAdapterFunctionalException p) {
			//Depois do ok , permacener na mesma tela . - > analista Edna solicitou.
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			limparDadosContrato();
			setListaContratoCliente(null);
			return "";
		}
			
	}
	
	/**
	 * Selecionar contrato cliente.
	 *
	 * @return the string
	 */
	public String selecionarContratoCliente(){
		if(isSinalizaOrgaoPublico()){
			validarParticipanteOrgaoPublico();
			if(saidaValidarContratoPagSalPartOrgaoPublico!= null && saidaValidarContratoPagSalPartOrgaoPublico.getCodMensagem()!= null){
				if(!"PGIT0040".equals(saidaValidarContratoPagSalPartOrgaoPublico.getCodMensagem())){
					BradescoFacesUtils.addInfoModalMessage("(" + saidaValidarContratoPagSalPartOrgaoPublico.getCodMensagem() + ") " + saidaValidarContratoPagSalPartOrgaoPublico.getMensagem(), false);
					return "";
				}else{
					setarAtributosClienteContrato();
				    return getPaginaRetorno();
			    }
			}	
		}else{
			setarAtributosClienteContrato();
		    return getPaginaRetorno();
		}
		
		return "";
	}
	
	/**
	 * Validar participante orgao publico.
	 */
	public void validarParticipanteOrgaoPublico(){
		ValidarContratoPagSalPartOrgaoPublicoEntradaDTO entrada = new ValidarContratoPagSalPartOrgaoPublicoEntradaDTO();
		 saidaValidarContratoPagSalPartOrgaoPublico = new ValidarContratoPagSalPartOrgaoPublicoSaidaDTO();
	
		entrada.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaContratoCliente.get(itemSelecionadoListaContratoCliente).getCdPessoaJuridica()));
		entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaContratoCliente.get(itemSelecionadoListaContratoCliente).getCdTipoContrato()));
		entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaContratoCliente.get(itemSelecionadoListaContratoCliente).getNrSequenciaContrato()));

		try{
			saidaValidarContratoPagSalPartOrgaoPublico = getManterClientesOrgaoPublicoService().validarParticipanteOrgaoPublico(entrada);
			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return;
		}
		
	}
	
	/**
	 * Pesquisa contrato.
	 *
	 * @return the string
	 */
	public String pesquisaContrato(){
		//Se a pesquisa retornar mais de um resultado
		if (getListaContratoCliente().size() > 1){
			//Exibir a tela de Pesquisa de Contratos do Cliente 
			setCpf(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getCnpjOuCpfFormatado());
			setNomeRazao(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getDsNomeRazao());
			
			setItemSelecionadoListaContratoCliente(null);
			return "identificacaoClienteContratoAgendamento";
		}else{
			if(getListaContratoCliente().size() == 1){
				//Primeiro e unico registro
				setItemSelecionadoListaContratoCliente(0);
				
				setarAtributosClienteContrato();
				
				return getPaginaRetorno();
			}
		}
		return "";
	}
	
	/**
	 * Setar atributos cliente contrato.
	 */
	private void setarAtributosClienteContrato(){
		//Carrega Dados do Contrato
		/* Alterado por Vivian - E-mail Allan Henrique Scarabelot Poletto Enviado: quarta-feira, 11 de maio de 2011 18:16
		StringBuilder empresa = new StringBuilder("");
		empresa.append(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getCdPessoaJuridica());
		empresa.append("-");
		empresa.append(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsPessoaJuridica());
		*/
		setEmpresaGestoraDescCliente(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsPessoaJuridica());
		
		//clientes Org�o Publico federal
		setCodPessoaParticipante(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getCdPessoaParticipante());
		
		setNumeroDescCliente(Long.toString(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getNrSequenciaContrato()));
		setDescricaoContratoDescCliente(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsContrato());
		setSituacaoDescCliente(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsSituacaoContrato());

		//Carrega Dados do Cliente
		setNrCpfCnpjCliente(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getCnpjOuCpfFormatado());
		setDescricaoRazaoSocialCliente(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getDsNomeRazao());

		//Nas telas de filtro, as telas est�o ligadas a este DTO
		setSaidaConsultarListaClientePessoas(this.getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()));
		setClienteContratoSelecionado(true);
		setCdPessoaJuridicaContrato(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getCdPessoaJuridica());
		setCdTipoContratoNegocio(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getCdTipoContrato());
		setNrSequenciaContratoNegocio(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getNrSequenciaContrato());


		setCpfCnpjFormatado(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getCnpjOuCpfFormatado());
		setNomeRazaoParticipante(getListaConsultarListaClientePessoas().get(getItemClienteSelecionado()).getDsNomeRazao());
		
		setEmpresaGestoraDescContrato(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsPessoaJuridica());
		setNumeroDescContrato(Long.toString(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getNrSequenciaContrato()));
		setDescricaoContratoDescContrato(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsContrato());
		setSituacaoDescContrato(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsSituacaoContrato());
		setDescricaoTipoContratoDescContrato(getListaContratoCliente().get(getItemSelecionadoListaContratoCliente()).getDsTipoContrato());

	}
	
	/**
	 * Selecionar cliente.
	 *
	 * @return the string
	 */
	public String selecionarCliente() {
		return pesquisaContrato();
	}
	
	/**
	 * Selecionar contrato.
	 *
	 * @return the string
	 */
	public String selecionarContrato(){
		setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getNrSeqContratoNegocio());
		
		//Campos de descri��o,mostrados na tela
		//setEmpresaGestoraDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsPessoaJuridicaContrato());
		setEmpresaGestoraDescContrato(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
		setNumeroDescContrato(Long.toString(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getNrSeqContratoNegocio()));
		setDescricaoContratoDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsContrato());
		setSituacaoDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsSituacaoContratoNegocio());
		setClienteContratoSelecionado(true);		
		return this.getPaginaRetorno();
	}
	
	/**
	 * Limpar campos cliente.
	 */
	public void limparCamposCliente() {
		
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());

		setNumeroDescCliente("");
		setDescricaoContratoDescCliente("");
		setSituacaoDescCliente("");
		setNrCpfCnpjCliente("");
		setDescricaoRazaoSocialCliente("");
		setEmpresaGestoraDescCliente("");
		setCdCpfCnpjPessoaPerfil("");
		setDsPessoaPerfil("");
		setCdPerfilTrocaArquivoPerfilCliente(null);
		setDsSituacaoPerfilCliente("");
		setClienteContratoSelecionado(false);

	}
	
	/* M�todo utilizado na pesquisa de Cliente Marq - listarContratosClienteMarq() */	
	/**
	 * Listar clientes marq.
	 *
	 * @return the string
	 */
	public String listarClientesMarq(){
		try{				
			setItemSelecionadoListaClientesMarq(null);
			setItemSelecionadoListaContratosMarq(null);
			setTelaVoltarIdentificacaoClienteContratoMarq("");
			
			ListarClientesMarqEntradaDTO entrada = new ListarClientesMarqEntradaDTO();
			entrada.setCdAgenciaBancaria(getEntradaConsultarListaClientePessoas().getCdAgenciaBancaria());
			entrada.setCdBanco(getEntradaConsultarListaClientePessoas().getCdBanco());
			entrada.setCdContaBancaria(getEntradaConsultarListaClientePessoas().getCdContaBancaria());
			entrada.setCdControleCnpj(getEntradaConsultarListaClientePessoas().getCdControleCnpj());
			entrada.setCdCpfCnpj(getEntradaConsultarListaClientePessoas().getCdCpfCnpj());
			entrada.setCdFilialCnpj(getEntradaConsultarListaClientePessoas().getCdFilialCnpj());
			entrada.setDsNomeRazao(getEntradaConsultarListaClientePessoas().getDsNomeRazaoSocial());
			
			//Passar Zeros
			entrada.setCdTipoConta(0);
			entrada.setDtNascimentoFund("");
			
			setListaGridListaClientesMarq(getFiltroAgendamentoEfetivacaoEstornoImpl().listarClientesMarq(entrada));
			
			listaControleRadioClientesMarq = new ArrayList<SelectItem>();
			for (int i=0; i <getListaGridListaClientesMarq().size(); i++){
				this.listaControleRadioClientesMarq.add(new SelectItem(i," "));
			}
			
			//Quando a consulta retornar mais de um resultado
			if(getListaGridListaClientesMarq().size() > 1){
				//Tela de contrato vai voltar para tela de Pesquisa (Filtro)
				setTelaVoltarIdentificacaoClienteContratoMarq("identificacaoClienteAgendamentoMarq");
				
				return "identificacaoClienteAgendamentoMarq";
			}else{
				//Se a pesquisa retornar apenas um resultado
				
				//Tela de contrato vai voltar para tela de Pesquisa (Filtro)
				setTelaVoltarIdentificacaoClienteContratoMarq(getPaginaRetorno());
				
				if(getListaGridListaClientesMarq().size() == 1){				
					//Primeiro e unico registro
					setItemSelecionadoListaClientesMarq(0);			
					
					return listarContratoClientesMarq();
				}
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			limparAtributosCliente();
			setListaGridListaClientesMarq(null);
			setItemSelecionadoListaClientesMarq(null);
		}
		
		return "";		
	}
	
	/**
	 * Selecionar contrato cliente marq.
	 *
	 * @return the string
	 */
	public String selecionarContratoClienteMarq(){
		setarAtributosClienteContratoMarq();
		return getPaginaRetorno();
	}
		
	/* M�todo utilizado na pesquisa de Contratos Cliente - listarContratosClienteMarq()
	   M�todo chamado pelo bot�o "Selecionar" da tela PesquisaCliente e dentro do m�todo listarClientesMarq*/
	/**
	 * Listar contrato clientes marq.
	 *
	 * @return the string
	 */
	public String listarContratoClientesMarq(){
		try{
			ListarContratosClienteMarqEntradaDTO entrada = new ListarContratosClienteMarqEntradaDTO();
			entrada.setCdClub(getListaGridListaClientesMarq().get(getItemSelecionadoListaClientesMarq()).getCdClub());
			
			setListaContratosClienteMarq(getFiltroAgendamentoEfetivacaoEstornoImpl().listarContratosClienteMarq(entrada));
			
			listaControleRadioContratosClienteMarq = new ArrayList<SelectItem>();
			for (int i=0; i <getListaContratosClienteMarq().size(); i++){
				this.listaControleRadioContratosClienteMarq.add(new SelectItem(i," "));
			}
			return pesquisaContratoMarq();
		
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaContratosClienteMarq(null);
			setItemSelecionadoListaContratosClienteMarq(null);
			return "";
		}		

	}
	
	/**
	 * Pesquisa contrato marq.
	 *
	 * @return the string
	 */
	public String pesquisaContratoMarq(){
		// Se lista de Contrato retornar apenas um contrato 
		if(getListaContratosClienteMarq().size() > 1){
			//Exibir a tela de Pesquisa de Contratos do Cliente 
			setCpf(getListaGridListaClientesMarq().get(getItemSelecionadoListaClientesMarq()).getCnpjOuCpfFormatado());
			setNomeRazao(getListaGridListaClientesMarq().get(getItemSelecionadoListaClientesMarq()).getDsNomeRazao());
			
			setItemSelecionadoListaContratosClienteMarq(null);
			return "identificacaoClienteContratoAgendamentoMarq";
		}else{
			if(getListaContratosClienteMarq().size() == 1){
				//Primeiro e unico registro
				setItemSelecionadoListaContratosClienteMarq(0);
				
				setarAtributosClienteContratoMarq();
				
				return getPaginaRetorno();				
			}	
		}
		return "";
	}
	
	/**
	 * Setar atributos cliente contrato marq.
	 */
	private void setarAtributosClienteContratoMarq(){
		//Carrega Dados do Contrato
		/* Alterado por Vivian - E-mail Allan Henrique Scarabelot Poletto Enviado: quarta-feira, 11 de maio de 2011 18:16
		StringBuilder empresa = new StringBuilder("");
		empresa.append(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getCdPessoaJuridicaContrato());
		empresa.append("-");
		empresa.append(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getDsPessoaJuridicaContrato());
		*/
		setEmpresaGestoraDescCliente(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getDsPessoaJuridicaContrato());
		
		
		setNumeroDescCliente(Long.toString(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getNrSequenciaContratoNegocio()));
		setDescricaoContratoDescCliente(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getDsContrato());
		setSituacaoDescCliente(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getDsSituacaoContratoNegocio());

		//Carrega Dados do Cliente
		setNrCpfCnpjCliente(getListaGridListaClientesMarq().get(getItemSelecionadoListaClientesMarq()).getCnpjOuCpfFormatado());
		setDescricaoRazaoSocialCliente(getListaGridListaClientesMarq().get(getItemSelecionadoListaClientesMarq()).getDsNomeRazao());

		//Nas telas de filtro, as telas est�o ligadas a este DTO
		setClienteContratoSelecionado(true);
		setCdPessoaJuridicaContrato(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getListaContratosClienteMarq().get(getItemSelecionadoListaContratosClienteMarq()).getNrSequenciaContratoNegocio());
	}	
	
	/**
	 * Listar contratos marq.
	 *
	 * @return the string
	 */
	public String listarContratosMarq(){
		try{
			ListarContratoMarqEntradaDTO entrada = new ListarContratoMarqEntradaDTO();
			entrada.setCdPessoaJuridicaContrato(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro() : 0L);
			entrada.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro() : 0);
			entrada.setNrSequenciaContratoNegocio(!getNumeroFiltro().equals("") ? Long.parseLong(getNumeroFiltro()) : 0);
			
			setListarContratosMarq(getFiltroAgendamentoEfetivacaoEstornoImpl().listarContratoMarq(entrada));
			listaControleRadioContratosMarq = new ArrayList<SelectItem>();
			for (int i=0; i <getListarContratosMarq().size(); i++){
				this.listaControleRadioContratosMarq.add(new SelectItem(i," "));
			}
			
			if(getListarContratosMarq().size() > 1){
				setItemSelecionadoListaContratosMarq(null);
				return "identificacaoContratoAgendamentoMarq";
			}
			
			setCdPessoaJuridicaContrato(getListarContratosMarq().get(0).getCdPessoJuridicaContrato());
			setCdTipoContratoNegocio(getListarContratosMarq().get(0).getCdTipoContratoNegocio());
			setNrSequenciaContratoNegocio(getListarContratosMarq().get(0).getNrSequenciaContratoNegocio());
			
			setEmpresaGestoraDescContrato(getListarContratosMarq().get(0).getDsPessoaJuridicaContrato());
			setNumeroDescContrato(Long.toString(getListarContratosMarq().get(0).getNrSequenciaContratoNegocio()));
			setDescricaoContratoDescContrato(getListarContratosMarq().get(0).getDsContrato());
			setSituacaoDescContrato(getListarContratosMarq().get(0).getDsSituacaoContratoNegocio());
			setClienteContratoSelecionado(true);				
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListarContratosMarq(null);
			setItemSelecionadoListaContratosMarq(null);
			setClienteContratoSelecionado(false);
			setCdPessoaJuridicaContrato(null);
			setCdTipoContratoNegocio(null);
			setNrSequenciaContratoNegocio(null);			
			setEmpresaGestoraDescContrato("");
			setNumeroDescContrato("");
			setDescricaoContratoDescContrato("");
			setSituacaoDescContrato("");
		}			
		
		return "";
	}
	
	/**
	 * Selecionar contrato marq.
	 *
	 * @return the string
	 */
	public String selecionarContratoMarq(){
		setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getNrSeqContratoNegocio());
		
		//Campos de descri��o,mostrados na tela
		setEmpresaGestoraDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsPessoaJuridicaContrato());
		setNumeroDescContrato(Long.toString(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getNrSeqContratoNegocio()));
		setDescricaoContratoDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsContrato());
		setSituacaoDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsSituacaoContratoNegocio());
		
		setEmpresaGestoraDescContratoOrgao(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
		setNumeroDescContratoOrgao(Long.toString(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getNrSeqContratoNegocio()));
		setDescricaoContratoDescContratoOrgao(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsContrato());
		setSituacaoDescContratoOrgao(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsSituacaoContratoNegocio());
		
		setClienteContratoSelecionado(true);		
		return this.getPaginaRetorno();
	}	
	
	/**
	 * Limpar atributos cliente.
	 */
	public void limparAtributosCliente(){
		setEmpresaGestoraDescCliente("");
		setNumeroDescCliente("");
		setDescricaoContratoDescCliente("");
		setSituacaoDescCliente("");
		setNrCpfCnpjCliente("");
		setDescricaoRazaoSocialCliente("");
	}	
	
	/**
	 * Submit cliente.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitCliente(ActionEvent evt){
		return consultarClientes();
	}
	
	
	/**
	 * Submit contrato.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitContrato(ActionEvent evt){
		return consultarContrato();
	}

	
	/**
	 * Submit contrato perfil.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitContratoPerfil(ActionEvent evt){
		return consultarContratoPerfil();
	}

	
	/**
	 * Submit contrato cliente.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitContratoCliente(ActionEvent evt){
		return efetuarPesquisarContrato();
	}
	
	/**
	 * Submit perfil contrato.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitPerfilContrato(ActionEvent evt){
		return consultarContratoPerfil();
	}	
	
	/**
	 * Submit perfil cliente.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitPerfilCliente(ActionEvent evt){
		return consultarClientesPerfil();
	}
	
	
	
	/**
	 * Submit perfil contrato cliente.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitPerfilContratoCliente(ActionEvent evt){
		return efetuarPesquisarContratoPerfil();
	}
	
	/**
	 * Submit perfil cli.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitPerfilCli(ActionEvent evt){
		return efetuarPesquisarContratoPerfil();
	}
	
	
	
	
	/* FILTRO PERFIL */
	/**
	 * Consultar clientes perfil.
	 *
	 * @return the string
	 */
	public String consultarClientesPerfil(){
		setTelaVoltarIdentificacaoClienteContrato("");
		setItemSelecionadoListaClientePerfil(null);
		
		try {
			/* Foi criado outro Objeto para n�o setar os campos "banco" e "agencia da tela ,e sim apenas na hora de mandar ao PDC
			 * Este Objeto recebe o clone do Objeto que esta na tela */
			ConsultarListaClientePessoasEntradaDTO entradaTela;
			try{
				entradaTela = (ConsultarListaClientePessoasEntradaDTO) entradaConsultarListaClientePessoas.clone();
				
				//TODO Por telefone, Samuel informou para deixarmos o campo digito na tela,mas o PDC ainda n�o recebe, ele ia confirmar com a Beatriz 
				entradaTela.setCdDigitoConta("");
				
			}catch(CloneNotSupportedException erro){
				entradaTela = new ConsultarListaClientePessoasEntradaDTO();
			}			

			//Efetua busca do cliente (pgic.favorecidos.manterFavorecidos.consultarListaClientePessoas)
			pesquisarClientesPerfil(entradaTela);
			
			this.listaControleRadioClientePerfil   =  new ArrayList<SelectItem>();
			for (int i = 0; i < getListaConsultarListaClientePessoasPerfil().size();i++) {
				this.listaControleRadioClientePerfil .add(new SelectItem(i," "));
			}
			
			//Se a pesquisa retornar mais de um resultado
			if ( getListaConsultarListaClientePessoasPerfil().size() > 1 ){
				
				//Tela de contrato vai voltar para tela de Cliente
				setTelaVoltarIdentificacaoClienteContrato("identificacaoClienteAgendamentoPerfil");
				return "identificacaoClienteAgendamentoPerfil";	
			}
			else{
				//Se a pesquisa retornar apenas um resultado
				
				//Tela de contrato vai voltar para tela de Pesquisa (Filtro)
				setTelaVoltarIdentificacaoClienteContrato(getPaginaRetorno());
				if ( getListaConsultarListaClientePessoasPerfil().size() == 1 ){
					//Primeiro e unico registro
					setItemSelecionadoListaClientePerfil(0);				
					
					return efetuarPesquisarContratoPerfil();
				}
			}
			
			return "";
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), "/" + this.getPaginaRetorno() + ".jsf", BradescoViewExceptionActionType.PATH, false);
			limparAtributosCliente();
			setItemSelecionadoListaClientePerfil(null);
			setListaConsultarListaClientePessoasPerfil(null);
			return "";
		}
	}
	 
	//M�todo chamado pelo bot�o "Selecionar" da tela "identificacaoClienteAgendamento" e dentro do m�todo consultarClientes
	/**
	 * Efetuar pesquisar contrato perfil.
	 *
	 * @return the string
	 */
	public String efetuarPesquisarContratoPerfil(){
		try {		
			//Efetua a busca do contrato - PGITIAHT (Pdc: pgit.consultas.consultarContratoClienteFiltro
			ConsultarContratoClienteFiltroEntradaDTO entrada = new ConsultarContratoClienteFiltroEntradaDTO();
			entrada.setCdClubPessoa(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getCdClub());
			entrada.setCdControleCpfPssoa(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getCdControleCnpj());
			entrada.setCdCpfCnpjPssoa(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getCdCpfCnpj());
			entrada.setCdFilialCnpjPssoa(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getCdFilialCnpj());
			
			setListaContratoClientePerfil(getFiltroAgendamentoEfetivacaoEstornoImpl().consultarContratoClienteFiltro(entrada));
			
			this.listaControleRadioContratoClientePerfil   =  new ArrayList<SelectItem>();
			for (int i = 0; i < getListaContratoClientePerfil().size();i++) {
				this.listaControleRadioContratoClientePerfil .add(new SelectItem(i," "));
			}			
			
			return pesquisaContratoPerfil();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			limparDadosContrato();
			setItemSelecionadoListaContratoClientePerfil(null);
			setListaContratoClientePerfil(null);
			return "";
		}			
	}
	
	/**
	 * Pesquisa contrato perfil.
	 *
	 * @return the string
	 */
	public String pesquisaContratoPerfil(){
		//Se a pesquisa retornar mais de um resultado
		if (getListaContratoClientePerfil().size() > 1){
			//Exibir a tela de Pesquisa de Contratos do Cliente 
			setCpf(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getCnpjOuCpfFormatado());
			setNomeRazao(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getDsNomeRazao());
			setItemSelecionadoListaContratoClientePerfil(null);
			
			return "identificacaoClienteContratoAgendamentoPerfil";
		}else{
			if(getListaContratoClientePerfil().size() == 1){
				//Primeiro e unico registro
				setItemSelecionadoListaContratoClientePerfil(0);
				
				listarPerfil();
				
				if(getListarPerfilTrocaArquivo() == null || getListarPerfilTrocaArquivo().size() == 0){
					return "";
				}
				
				if(getListarPerfilTrocaArquivo().size() > 1){
					//Define para qual p�gina a tela de Perfil vai voltar
					if(getListaContratoClientePerfil().size() > 1){
						setTelaVoltarIdentificacaoPerfil("identificacaoClienteContratoAgendamentoPerfil");					
					}
					else{
						if(getListaConsultarListaClientePessoasPerfil().size() > 1){
							setTelaVoltarIdentificacaoPerfil("identificacaoClienteAgendamentoPerfil");
						}else{
							setTelaVoltarIdentificacaoPerfil(getPaginaRetorno());
						}
					}
					return "identificacaoPerfilCliente";
				}
				else{//Apenas um resultado
					setItemSelecionadoListaPerfilTroca(0);
					return setarAtributosPerfil();
				}
			}
		}
		return "";
	}
	
	/**
	 * Voltar perfil.
	 *
	 * @return the string
	 */
	public String voltarPerfil(){
		return getTelaVoltarIdentificacaoPerfil();
	}
	
	/**
	 * Listar perfil.
	 */
	public void listarPerfil(){
		try {				
			limparDadosPerfil();
			ListarPerfilTrocaArquivoEntradaDTO entrada = new ListarPerfilTrocaArquivoEntradaDTO();
			
			entrada.setCdPessoaJuridicaContrato(getTipoFiltroSelecionado().equals("0") ? 
					getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getCdPessoaJuridica() :
					getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getCdPessoaJuridicaContrato());
			
			entrada.setCdTipoContratoNegocio(getTipoFiltroSelecionado().equals("0") ? 
					getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getCdTipoContrato() :
					getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getCdTipoContratoNegocio());
			
			entrada.setNrSequenciaContratoNegocio(getTipoFiltroSelecionado().equals("0") ?
					getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getNrSequenciaContrato() : 
					getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getNrSeqContratoNegocio());
			
			if(getTipoFiltroSelecionado().equals("0")){
				entrada.setCdPessoa(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getCdClub());
			}
			else{
				entrada.setCdPessoa(0L);
			}
			
			setListarPerfilTrocaArquivo(getFiltroAgendamentoEfetivacaoEstornoImpl().listarPerfilTrocaArquivo(entrada));
			
			this.listaControleRadioPerfilTroca =  new ArrayList<SelectItem>();
			for (int i = 0; i < getListarPerfilTrocaArquivo().size();i++) {
				this.listaControleRadioPerfilTroca .add(new SelectItem(i," "));
			}		
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			limparDadosPerfil();
		}				
		
	}
	
	/**
	 * Consultar contrato perfil.
	 *
	 * @return the string
	 */
	public String consultarContratoPerfil(){
		try{				
			ConsultarListaContratosPessoasEntradaDTO entrada = new ConsultarListaContratosPessoasEntradaDTO();
			entrada.setCdClub(0L);
			entrada.setCdPessoaJuridicaContrato(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro() : 0L);
			entrada.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro() : 0);
			entrada.setNrSeqContratoNegocio(!getNumeroFiltro().equals("") ? Long.parseLong(getNumeroFiltro()) : 0);
			
			setListaConsultarListaContratosPessoasPerfil(getFiltroIdentificaoService().pesquisarContratos(entrada));
			
			this.listaControleRadioContratosPessoasPerfil =  new ArrayList<SelectItem>();
			for (int i = 0; i < getListaConsultarListaContratosPessoasPerfil().size();i++) {
				this.listaControleRadioContratosPessoasPerfil .add(new SelectItem(i," "));
			}	
			
			if(getListaConsultarListaContratosPessoasPerfil().size() > 1){
				setItemClienteSelecionadoContratoPerfil(null);
				setTelaVoltarIdentificacaoPerfil("identificacaoContratoAgendamentoPerfil");
				return "identificacaoContratoAgendamentoPerfil";
			}else{//Se for apenas um resultado
				if(getListaConsultarListaContratosPessoasPerfil().size() == 1){
					setItemClienteSelecionadoContratoPerfil(0);
					listarPerfil();
					
					if(getListarPerfilTrocaArquivo() == null || getListarPerfilTrocaArquivo().size() == 0){
						return "";
					}
					
					if(getListarPerfilTrocaArquivo().size() > 1){
						setTelaVoltarIdentificacaoPerfil(getPaginaRetorno());
						return "identificacaoPerfilContrato";
					}
					
					//Se for apenas um registro
					setItemSelecionadoListaPerfilTroca(0);
					return setarAtributosContratoPerfil();

				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaConsultarListaContratosPessoasPerfil(null);
			setItemClienteSelecionadoContratoPerfil(null);
			setClienteContratoSelecionado(false);
			setCdPessoaJuridicaContrato(null);
			setCdTipoContratoNegocio(null);
			setNrSequenciaContratoNegocio(null);			
			setEmpresaGestoraDescContrato("");
			setNumeroDescContrato("");
			setDescricaoContratoDescContrato("");
			setSituacaoDescContrato("");
			setEmpresaGestoraDescContratoOrgao("");
			setNumeroDescContratoOrgao("");
			setDescricaoContratoDescContratoOrgao("");
			setSituacaoDescContratoOrgao("");
		}
		
		return "";
	}	
	
	/**
	 * Perfil contrato.
	 *
	 * @return the string
	 */
	public String perfilContrato(){
		listarPerfil();
		
		if(getListarPerfilTrocaArquivo() == null || getListarPerfilTrocaArquivo().size() == 0){
			return "";
		}
		
		if(getListarPerfilTrocaArquivo().size() > 1){
			setTelaVoltarIdentificacaoPerfil("identificacaoContratoAgendamentoPerfil");
			return "identificacaoPerfilContrato";
		}
		
		//Se for apenas um registro
		setItemSelecionadoListaPerfilTroca(0);
		return setarAtributosContratoPerfil();
	}
	
	/**
	 * Perfil cliente contrato.
	 *
	 * @return the string
	 */
	public String perfilClienteContrato(){
		listarPerfil();
		
		if(getListarPerfilTrocaArquivo() == null || getListarPerfilTrocaArquivo().size() == 0){
			return "";
		}
		
		if(getListarPerfilTrocaArquivo().size() > 1){
			setTelaVoltarIdentificacaoPerfil("identificacaoClienteContratoAgendamentoPerfil");
			return "identificacaoPerfilCliente";
		}
		
		//Se for apenas um registro
		setItemSelecionadoListaPerfilTroca(0);
		return setarAtributosPerfil();
	}	
	
	/**
	 * Setar atributos contrato perfil.
	 *
	 * @return the string
	 */
	public String setarAtributosContratoPerfil(){
		setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getNrSeqContratoNegocio());

	    setCdPerfilTrocaArquivoPerfilContrato(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getCdPerfilTrocaArquivo());
	    setDsSituacaoPerfilContrato(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getDsSituacaoPerfil());
	   
		
		//setEmpresaGestoraDescContrato(getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getDsPessoaJuridicaContrato());
	    setEmpresaGestoraDescContrato(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
		setNumeroDescContrato(Long.toString(getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getNrSeqContratoNegocio()));
		setDescricaoContratoDescContrato(getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getDsContrato());
		setSituacaoDescContrato(getListaConsultarListaContratosPessoasPerfil().get(getItemClienteSelecionadoContratoPerfil()).getDsSituacaoContratoNegocio());
		setClienteContratoSelecionado(true);

	    setCdPessoaPerfil(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getCdPessoa());
	    setCdCpfCnpjPessoaPerfil("");
	    setDsPessoaPerfil("");
		
		return getPaginaRetorno();
	}
	
	/**
	 * Setar atributos perfil.
	 *
	 * @return the string
	 */
	public String setarAtributosPerfil(){
		setCdPessoaJuridicaContrato(getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getCdPessoaJuridica());
		setCdTipoContratoNegocio(getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getCdTipoContrato());
		setNrSequenciaContratoNegocio(getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getNrSequenciaContrato());
		
	    setCdPessoaPerfil(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getCdPessoa());
	    setCdCpfCnpjPessoaPerfil(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getCdCpfCnpjPessoa());
	    setDsPessoaPerfil(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getDsPessoa());
	    
	    setCdPerfilTrocaArquivoPerfilCliente(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getCdPerfilTrocaArquivo());
	    setDsSituacaoPerfilCliente(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getDsSituacaoPerfil());
	    
	    
		setEmpresaGestoraDescCliente(getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getDsPessoaJuridica());
		setNumeroDescCliente(Long.toString(getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getNrSequenciaContrato()));
		setDescricaoContratoDescCliente(getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getDsContrato());
		setSituacaoDescCliente(getListaContratoClientePerfil().get(getItemSelecionadoListaContratoClientePerfil()).getDsSituacaoContrato());
		
		setNrCpfCnpjCliente(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getCnpjOuCpfFormatado());
		setDescricaoRazaoSocialCliente(getListaConsultarListaClientePessoasPerfil().get(getItemSelecionadoListaClientePerfil()).getDsNomeRazao());

		setClienteContratoSelecionado(true);
	    
	    return getPaginaRetorno();
	}
	
	/**
	 * Limpar dados perfil.
	 */
	private void limparDadosPerfil(){
	    setCdPessoaPerfil(null);
	    setCdCpfCnpjPessoaPerfil("");
	    setDsPessoaPerfil("");
	    setCdPerfilTrocaArquivoPerfilCliente(null);
	    setDsSituacaoPerfilCliente("");
	    setCdPerfilTrocaArquivoPerfilContrato(null);
	    setDsSituacaoPerfilContrato("");
		setItemSelecionadoListaPerfilTroca(null);
		setListarPerfilTrocaArquivo(null);	    
	}
	
	
	/**
	 * Buscar participante.
	 *
	 * @return the string
	 */
	public String buscarParticipante(){
		try{		
			ConsultarParticipanteContratoEntradaDTO entrada = new ConsultarParticipanteContratoEntradaDTO();		
			
			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratonegocio(getNrSequenciaContratoNegocio());
			entrada.setCdControleCpfParticipante(getControleCpfCnpjParticipanteFiltro());
			entrada.setCdFilialCnpjParticipante(getFilialCpfCnpjParticipanteFiltro());
			entrada.setCdCpfCnpjParticipante(getCorpoCpfCnpjParticipanteFiltro());
			
			List<ConsultarParticipanteContratoSaidaDTO> listaParticipante = getConsultarPagamentosIndividualServiceImpl().consultarParticipanteContrato(entrada);
			
			if (listaParticipante.size() == 1){
				setNomeParticipanteFiltro(listaParticipante.get(0).getDsParticipante());
				setCpfCnpjParticipanteFiltro(CpfCnpjUtils.formatarCpfCnpj(listaParticipante.get(0).getCdCpfCnpjParticipante(), listaParticipante.get(0).getCdFilialCnpjParticipante(), listaParticipante.get(0).getCdControleCpfParticipante()));
				setCodigoParticipanteFiltro(listaParticipante.get(0).getCdParticipante());
			}else{
				setListaParticipantes(listaParticipante);
				setItemSelecionadoListaParcipantes(null);
				listaParticipantesControleRadio = new ArrayList<SelectItem>();

				for (int i = 0; i < getListaParticipantes().size(); i++) {
					this.listaParticipantesControleRadio.add(new SelectItem(i, " "));
				}
			
				return "identificacaoParticipanteAgendamento";
			}
			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setCodigoParticipanteFiltro(null);			
			setNomeParticipanteFiltro("");
			setCpfCnpjParticipanteFiltro("");
			setItemSelecionadoListaParcipantes(null);
		}
						
		return "";
	}
	
	
	/**
	 * Pesquisar participantes.
	 *
	 * @param evt the evt
	 */
	public void pesquisarParticipantes(ActionEvent evt){
		
		try{		
			ConsultarParticipanteContratoEntradaDTO entrada = new ConsultarParticipanteContratoEntradaDTO();		
			
			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratonegocio(getNrSequenciaContratoNegocio());
			entrada.setCdControleCpfParticipante(getControleCpfCnpjParticipanteFiltro());
			entrada.setCdFilialCnpjParticipante(getFilialCpfCnpjParticipanteFiltro());
			entrada.setCdCpfCnpjParticipante(getCorpoCpfCnpjParticipanteFiltro());
			
			List<ConsultarParticipanteContratoSaidaDTO> listaParticipante = getConsultarPagamentosIndividualServiceImpl().consultarParticipanteContrato(entrada);
						
			setListaParticipantes(listaParticipante);
			setItemSelecionadoListaParcipantes(null);
			listaParticipantesControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaParticipantes().size(); i++) {
				this.listaParticipantesControleRadio.add(new SelectItem(i, " "));
			}		
			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setCodigoParticipanteFiltro(null);
			setNomeParticipanteFiltro("");
			setCpfCnpjParticipanteFiltro("");
			setItemSelecionadoListaParcipantes(null);
		}
				
		
	}
	
	/**
	 * Voltar participante.
	 *
	 * @return the string
	 */
	public String voltarParticipante(){
		return getVoltarParticipante();
	}
	
	/**
	 * Selecionar participante.
	 *
	 * @return the string
	 */
	public String selecionarParticipante(){		
		setCodigoParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdParticipante());
		setCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCpfCnpj());
		setNomeParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getDsParticipante());
		setCorpoCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdCpfCnpjParticipante());
		setFilialCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdFilialCnpjParticipante());
		setControleCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdControleCpfParticipante());
		return getVoltarParticipante();
	}
	
	
	/**
	 * Limpar participante contrato.
	 */
	public void limparParticipanteContrato(){
		setCodigoParticipanteFiltro(null);
		setCpfCnpjParticipanteFiltro("");
		setNomeParticipanteFiltro("");
		setCorpoCpfCnpjParticipanteFiltro(null);
		setFilialCpfCnpjParticipanteFiltro(null);
		setControleCpfCnpjParticipanteFiltro(null);		
	}
	
	/**
	 * Limpar nome participante.
	 */
	public void limparNomeParticipante(){
		setNomeParticipanteFiltro("");
		setCpfCnpjParticipanteFiltro("");
	}
	/* Sets e Gets */
	/**
	 * Get: filtroIdentificaoService.
	 *
	 * @return filtroIdentificaoService
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoService() {
		return filtroIdentificaoService;
	}

	/**
	 * Set: filtroIdentificaoService.
	 *
	 * @param filtroIdentificaoService the filtro identificao service
	 */
	public void setFiltroIdentificaoService(
			IFiltroIdentificaoService filtroIdentificaoService) {
		this.filtroIdentificaoService = filtroIdentificaoService;
	}

	/**
	 * Filtro agendamento efetivacao estorno bean.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: entradaConsultarListaClientePessoas.
	 *
	 * @return entradaConsultarListaClientePessoas
	 */
	public ConsultarListaClientePessoasEntradaDTO getEntradaConsultarListaClientePessoas() {
		return entradaConsultarListaClientePessoas;
	}

	/**
	 * Set: entradaConsultarListaClientePessoas.
	 *
	 * @param entradaConsultarListaClientePessoas the entrada consultar lista cliente pessoas
	 */
	public void setEntradaConsultarListaClientePessoas(
			ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoas) {
		this.entradaConsultarListaClientePessoas = entradaConsultarListaClientePessoas;
	}

	/**
	 * Get: saidaConsultarListaClientePessoas.
	 *
	 * @return saidaConsultarListaClientePessoas
	 */
	public ConsultarListaClientePessoasSaidaDTO getSaidaConsultarListaClientePessoas() {
		return saidaConsultarListaClientePessoas;
	}

	/**
	 * Set: saidaConsultarListaClientePessoas.
	 *
	 * @param saidaConsultarListaClientePessoas the saida consultar lista cliente pessoas
	 */
	public void setSaidaConsultarListaClientePessoas(
			ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoas) {
		this.saidaConsultarListaClientePessoas = saidaConsultarListaClientePessoas;
	}

	/**
	 * Get: itemFiltroSelecionado.
	 *
	 * @return itemFiltroSelecionado
	 */
	public String getItemFiltroSelecionado() {
		return itemFiltroSelecionado;
	}

	/**
	 * Set: itemFiltroSelecionado.
	 *
	 * @param itemFiltroSelecionado the item filtro selecionado
	 */
	public void setItemFiltroSelecionado(String itemFiltroSelecionado) {
		this.itemFiltroSelecionado = itemFiltroSelecionado;
	}

	/**
	 * Get: itemClienteSelecionado.
	 *
	 * @return itemClienteSelecionado
	 */
	public Integer getItemClienteSelecionado() {
		return itemClienteSelecionado;
	}

	/**
	 * Set: itemClienteSelecionado.
	 *
	 * @param itemClienteSelecionado the item cliente selecionado
	 */
	public void setItemClienteSelecionado(Integer itemClienteSelecionado) {
		this.itemClienteSelecionado = itemClienteSelecionado;
	}

	/**
	 * Get: paginaRetorno.
	 *
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}
	
	/**
	 * Pesquisar clientes.
	 *
	 * @param entrada the entrada
	 */
	private void pesquisarClientes(ConsultarListaClientePessoasEntradaDTO entrada) {
		this.setListaConsultarListaClientePessoas(this.getFiltroIdentificaoService().pesquisarClientes(entrada));
	}
	
	/**
	 * Pesquisar clientes perfil.
	 *
	 * @param entrada the entrada
	 */
	private void pesquisarClientesPerfil(ConsultarListaClientePessoasEntradaDTO entrada) {
		this.setListaConsultarListaClientePessoasPerfil(this.getFiltroIdentificaoService().pesquisarClientes(entrada));
	}	
	
	/**
	 * Get: listaConsultarListaClientePessoas.
	 *
	 * @return listaConsultarListaClientePessoas
	 */
	public List<ConsultarListaClientePessoasSaidaDTO> getListaConsultarListaClientePessoas() {
		return listaConsultarListaClientePessoas;
	}

	/**
	 * Set: listaConsultarListaClientePessoas.
	 *
	 * @param listaConsultarListaClientePessoas the lista consultar lista cliente pessoas
	 */
	public void setListaConsultarListaClientePessoas(
			List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoas) {
		this.listaConsultarListaClientePessoas = listaConsultarListaClientePessoas;
	}

	/**
	 * Get: agenciaOperadoraFiltro.
	 *
	 * @return agenciaOperadoraFiltro
	 */
	public String getAgenciaOperadoraFiltro() {
		return agenciaOperadoraFiltro;
	}

	/**
	 * Set: agenciaOperadoraFiltro.
	 *
	 * @param agenciaOperadoraFiltro the agencia operadora filtro
	 */
	public void setAgenciaOperadoraFiltro(String agenciaOperadoraFiltro) {
		this.agenciaOperadoraFiltro = agenciaOperadoraFiltro;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: dsNomeRazaoSocialParticipante.
	 *
	 * @return dsNomeRazaoSocialParticipante
	 */
	public String getDsNomeRazaoSocialParticipante() {
		return dsNomeRazaoSocialParticipante;
	}

	/**
	 * Set: dsNomeRazaoSocialParticipante.
	 *
	 * @param dsNomeRazaoSocialParticipante the ds nome razao social participante
	 */
	public void setDsNomeRazaoSocialParticipante(
			String dsNomeRazaoSocialParticipante) {
		this.dsNomeRazaoSocialParticipante = dsNomeRazaoSocialParticipante;
	}

	/**
	 * Get: empresaGestoraFiltro.
	 *
	 * @return empresaGestoraFiltro
	 */
	public Long getEmpresaGestoraFiltro() {
		return empresaGestoraFiltro;
	}

	/**
	 * Set: empresaGestoraFiltro.
	 *
	 * @param empresaGestoraFiltro the empresa gestora filtro
	 */
	public void setEmpresaGestoraFiltro(Long empresaGestoraFiltro) {
		this.empresaGestoraFiltro = empresaGestoraFiltro;
	}

	/**
	 * Get: numeroFiltro.
	 *
	 * @return numeroFiltro
	 */
	public String getNumeroFiltro() {
		return numeroFiltro;
	}

	/**
	 * Set: numeroFiltro.
	 *
	 * @param numeroFiltro the numero filtro
	 */
	public void setNumeroFiltro(String numeroFiltro) {
		this.numeroFiltro = numeroFiltro;
	}

	/**
	 * Get: situacaoFiltro.
	 *
	 * @return situacaoFiltro
	 */
	public Integer getSituacaoFiltro() {
		return situacaoFiltro;
	}

	/**
	 * Set: situacaoFiltro.
	 *
	 * @param situacaoFiltro the situacao filtro
	 */
	public void setSituacaoFiltro(Integer situacaoFiltro) {
		this.situacaoFiltro = situacaoFiltro;
	}

	/**
	 * Get: tipoContratoFiltro.
	 *
	 * @return tipoContratoFiltro
	 */
	public Integer getTipoContratoFiltro() {
		return tipoContratoFiltro;
	}

	/**
	 * Set: tipoContratoFiltro.
	 *
	 * @param tipoContratoFiltro the tipo contrato filtro
	 */
	public void setTipoContratoFiltro(Integer tipoContratoFiltro) {
		this.tipoContratoFiltro = tipoContratoFiltro;
	}

	/**
	 * Get: tipoFiltroSelecionado.
	 *
	 * @return tipoFiltroSelecionado
	 */
	public String getTipoFiltroSelecionado() {
		return tipoFiltroSelecionado;
	}

	/**
	 * Set: tipoFiltroSelecionado.
	 *
	 * @param tipoFiltroSelecionado the tipo filtro selecionado
	 */
	public void setTipoFiltroSelecionado(String tipoFiltroSelecionado) {
		this.tipoFiltroSelecionado = tipoFiltroSelecionado;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: agenciaContaDebitoBancariaFiltro.
	 *
	 * @return agenciaContaDebitoBancariaFiltro
	 */
	public Integer getAgenciaContaDebitoBancariaFiltro() {
		return agenciaContaDebitoBancariaFiltro;
	}

	/**
	 * Set: agenciaContaDebitoBancariaFiltro.
	 *
	 * @param agenciaContaDebitoBancariaFiltro the agencia conta debito bancaria filtro
	 */
	public void setAgenciaContaDebitoBancariaFiltro(
			Integer agenciaContaDebitoBancariaFiltro) {
		this.agenciaContaDebitoBancariaFiltro = agenciaContaDebitoBancariaFiltro;
	}

	/**
	 * Get: bancoContaDebitoFiltro.
	 *
	 * @return bancoContaDebitoFiltro
	 */
	public Integer getBancoContaDebitoFiltro() {
		return bancoContaDebitoFiltro;
	}

	/**
	 * Set: bancoContaDebitoFiltro.
	 *
	 * @param bancoContaDebitoFiltro the banco conta debito filtro
	 */
	public void setBancoContaDebitoFiltro(Integer bancoContaDebitoFiltro) {
		this.bancoContaDebitoFiltro = bancoContaDebitoFiltro;
	}

	/**
	 * Get: contaBancariaContaDebitoFiltro.
	 *
	 * @return contaBancariaContaDebitoFiltro
	 */
	public Long getContaBancariaContaDebitoFiltro() {
		return contaBancariaContaDebitoFiltro;
	}

	/**
	 * Set: contaBancariaContaDebitoFiltro.
	 *
	 * @param contaBancariaContaDebitoFiltro the conta bancaria conta debito filtro
	 */
	public void setContaBancariaContaDebitoFiltro(
			Long contaBancariaContaDebitoFiltro) {
		this.contaBancariaContaDebitoFiltro = contaBancariaContaDebitoFiltro;
	}

	/**
	 * Get: cpfCnpjFiltroDesc.
	 *
	 * @return cpfCnpjFiltroDesc
	 */
	public String getCpfCnpjFiltroDesc() {
		return cpfCnpjFiltroDesc;
	}

	/**
	 * Set: cpfCnpjFiltroDesc.
	 *
	 * @param cpfCnpjFiltroDesc the cpf cnpj filtro desc
	 */
	public void setCpfCnpjFiltroDesc(String cpfCnpjFiltroDesc) {
		this.cpfCnpjFiltroDesc = cpfCnpjFiltroDesc;
	}

	/**
	 * Get: itemClienteSelecionadoContrato.
	 *
	 * @return itemClienteSelecionadoContrato
	 */
	public Integer getItemClienteSelecionadoContrato() {
		return itemClienteSelecionadoContrato;
	}

	/**
	 * Set: itemClienteSelecionadoContrato.
	 *
	 * @param itemClienteSelecionadoContrato the item cliente selecionado contrato
	 */
	public void setItemClienteSelecionadoContrato(
			Integer itemClienteSelecionadoContrato) {
		this.itemClienteSelecionadoContrato = itemClienteSelecionadoContrato;
	}

	/**
	 * Get: nomeRazaoFiltroDesc.
	 *
	 * @return nomeRazaoFiltroDesc
	 */
	public String getNomeRazaoFiltroDesc() {
		return nomeRazaoFiltroDesc;
	}

	/**
	 * Set: nomeRazaoFiltroDesc.
	 *
	 * @param nomeRazaoFiltroDesc the nome razao filtro desc
	 */
	public void setNomeRazaoFiltroDesc(String nomeRazaoFiltroDesc) {
		this.nomeRazaoFiltroDesc = nomeRazaoFiltroDesc;
	}

	/**
	 * Get: listaConsultarListaContratosPessoas.
	 *
	 * @return listaConsultarListaContratosPessoas
	 */
	public List<ConsultarListaContratosPessoasSaidaDTO> getListaConsultarListaContratosPessoas() {
		return listaConsultarListaContratosPessoas;
	}

	/**
	 * Set: listaConsultarListaContratosPessoas.
	 *
	 * @param listaConsultarListaContratosPessoas the lista consultar lista contratos pessoas
	 */
	public void setListaConsultarListaContratosPessoas(
			List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas) {
		this.listaConsultarListaContratosPessoas = listaConsultarListaContratosPessoas;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: descricaoContratoDescContrato.
	 *
	 * @return descricaoContratoDescContrato
	 */
	public String getDescricaoContratoDescContrato() {
		return descricaoContratoDescContrato;
	}

	/**
	 * Set: descricaoContratoDescContrato.
	 *
	 * @param descricaoContratoDescContrato the descricao contrato desc contrato
	 */
	public void setDescricaoContratoDescContrato(
			String descricaoContratoDescContrato) {
		this.descricaoContratoDescContrato = descricaoContratoDescContrato;
	}

	/**
	 * Get: empresaGestoraDescContrato.
	 *
	 * @return empresaGestoraDescContrato
	 */
	public String getEmpresaGestoraDescContrato() {
		return empresaGestoraDescContrato;
	}

	/**
	 * Set: empresaGestoraDescContrato.
	 *
	 * @param empresaGestoraDescContrato the empresa gestora desc contrato
	 */
	public void setEmpresaGestoraDescContrato(String empresaGestoraDescContrato) {
		this.empresaGestoraDescContrato = empresaGestoraDescContrato;
	}

	/**
	 * Get: numeroDescContrato.
	 *
	 * @return numeroDescContrato
	 */
	public String getNumeroDescContrato() {
		return numeroDescContrato;
	}

	/**
	 * Set: numeroDescContrato.
	 *
	 * @param numeroDescContrato the numero desc contrato
	 */
	public void setNumeroDescContrato(String numeroDescContrato) {
		this.numeroDescContrato = numeroDescContrato;
	}

	/**
	 * Get: situacaoDescContrato.
	 *
	 * @return situacaoDescContrato
	 */
	public String getSituacaoDescContrato() {
		return situacaoDescContrato;
	}

	/**
	 * Set: situacaoDescContrato.
	 *
	 * @param situacaoDescContrato the situacao desc contrato
	 */
	public void setSituacaoDescContrato(String situacaoDescContrato) {
		this.situacaoDescContrato = situacaoDescContrato;
	}



	/**
	 * Get: empresaGestoraDescCliente.
	 *
	 * @return empresaGestoraDescCliente
	 */
	public String getEmpresaGestoraDescCliente() {
		return empresaGestoraDescCliente;
	}

	/**
	 * Set: empresaGestoraDescCliente.
	 *
	 * @param empresaGestoraDescCliente the empresa gestora desc cliente
	 */
	public void setEmpresaGestoraDescCliente(String empresaGestoraDescCliente) {
		this.empresaGestoraDescCliente = empresaGestoraDescCliente;
	}

	/**
	 * Get: descricaoContratoDescCliente.
	 *
	 * @return descricaoContratoDescCliente
	 */
	public String getDescricaoContratoDescCliente() {
		return descricaoContratoDescCliente;
	}

	/**
	 * Set: descricaoContratoDescCliente.
	 *
	 * @param descricaoContratoDescCliente the descricao contrato desc cliente
	 */
	public void setDescricaoContratoDescCliente(String descricaoContratoDescCliente) {
		this.descricaoContratoDescCliente = descricaoContratoDescCliente;
	}

	/**
	 * Get: numeroDescCliente.
	 *
	 * @return numeroDescCliente
	 */
	public String getNumeroDescCliente() {
		return numeroDescCliente;
	}

	/**
	 * Set: numeroDescCliente.
	 *
	 * @param numeroDescCliente the numero desc cliente
	 */
	public void setNumeroDescCliente(String numeroDescCliente) {
		this.numeroDescCliente = numeroDescCliente;
	}

	/**
	 * Get: situacaoDescCliente.
	 *
	 * @return situacaoDescCliente
	 */
	public String getSituacaoDescCliente() {
		return situacaoDescCliente;
	}

	/**
	 * Set: situacaoDescCliente.
	 *
	 * @param situacaoDescCliente the situacao desc cliente
	 */
	public void setSituacaoDescCliente(String situacaoDescCliente) {
		this.situacaoDescCliente = situacaoDescCliente;
	}

	

	/**
	 * Get: digitoContaDebitoFiltro.
	 *
	 * @return digitoContaDebitoFiltro
	 */
	public String getDigitoContaDebitoFiltro() {
		return digitoContaDebitoFiltro;
	}

	/**
	 * Set: digitoContaDebitoFiltro.
	 *
	 * @param digitoContaDebitoFiltro the digito conta debito filtro
	 */
	public void setDigitoContaDebitoFiltro(String digitoContaDebitoFiltro) {
		this.digitoContaDebitoFiltro = digitoContaDebitoFiltro;
	}

	/**
	 * Is cliente contrato selecionado.
	 *
	 * @return true, if is cliente contrato selecionado
	 */
	public boolean isClienteContratoSelecionado() {
		return clienteContratoSelecionado;
	}

	/**
	 * Set: clienteContratoSelecionado.
	 *
	 * @param clienteContratoSelecionado the cliente contrato selecionado
	 */
	public void setClienteContratoSelecionado(boolean clienteContratoSelecionado) {
		this.clienteContratoSelecionado = clienteContratoSelecionado;
	}

	/**
	 * Get: descricaoRazaoSocialCliente.
	 *
	 * @return descricaoRazaoSocialCliente
	 */
	public String getDescricaoRazaoSocialCliente() {
		return descricaoRazaoSocialCliente;
	}

	/**
	 * Set: descricaoRazaoSocialCliente.
	 *
	 * @param descricaoRazaoSocialCliente the descricao razao social cliente
	 */
	public void setDescricaoRazaoSocialCliente(String descricaoRazaoSocialCliente) {
		this.descricaoRazaoSocialCliente = descricaoRazaoSocialCliente;
	}


	/**
	 * Get: nrCpfCnpjCliente.
	 *
	 * @return nrCpfCnpjCliente
	 */
	public String getNrCpfCnpjCliente() {
		return nrCpfCnpjCliente;
	}

	/**
	 * Set: nrCpfCnpjCliente.
	 *
	 * @param nrCpfCnpjCliente the nr cpf cnpj cliente
	 */
	public void setNrCpfCnpjCliente(String nrCpfCnpjCliente) {
		this.nrCpfCnpjCliente = nrCpfCnpjCliente;
	}

		/**
		 * Get: descricaoAgenciaContaDebitoBancaria.
		 *
		 * @return descricaoAgenciaContaDebitoBancaria
		 */
		public String getDescricaoAgenciaContaDebitoBancaria() {
		return descricaoAgenciaContaDebitoBancaria;
	}

	/**
	 * Set: descricaoAgenciaContaDebitoBancaria.
	 *
	 * @param descricaoAgenciaContaDebitoBancaria the descricao agencia conta debito bancaria
	 */
	public void setDescricaoAgenciaContaDebitoBancaria(
			String descricaoAgenciaContaDebitoBancaria) {
		this.descricaoAgenciaContaDebitoBancaria = descricaoAgenciaContaDebitoBancaria;
	}

	/**
	 * Get: descricaoBancoContaDebito.
	 *
	 * @return descricaoBancoContaDebito
	 */
	public String getDescricaoBancoContaDebito() {
		return descricaoBancoContaDebito;
	}

	/**
	 * Set: descricaoBancoContaDebito.
	 *
	 * @param descricaoBancoContaDebito the descricao banco conta debito
	 */
	public void setDescricaoBancoContaDebito(String descricaoBancoContaDebito) {
		this.descricaoBancoContaDebito = descricaoBancoContaDebito;
	}

	/**
	 * Get: descricaoContaDebito.
	 *
	 * @return descricaoContaDebito
	 */
	public Long getDescricaoContaDebito() {
		return descricaoContaDebito;
	}

	/**
	 * Set: descricaoContaDebito.
	 *
	 * @param descricaoContaDebito the descricao conta debito
	 */
	public void setDescricaoContaDebito(Long descricaoContaDebito) {
		this.descricaoContaDebito = descricaoContaDebito;
	}

	/**
	 * Get: descricaoTipoContaDebito.
	 *
	 * @return descricaoTipoContaDebito
	 */
	public String getDescricaoTipoContaDebito() {
		return descricaoTipoContaDebito;
	}

	/**
	 * Set: descricaoTipoContaDebito.
	 *
	 * @param descricaoTipoContaDebito the descricao tipo conta debito
	 */
	public void setDescricaoTipoContaDebito(String descricaoTipoContaDebito) {
		this.descricaoTipoContaDebito = descricaoTipoContaDebito;
	}

	/**
	 * Get: listaEmpresaGestoraHash.
	 *
	 * @return listaEmpresaGestoraHash
	 */
	public Map<Long, String> getListaEmpresaGestoraHash() {
		return listaEmpresaGestoraHash;
	}

	/**
	 * Set lista empresa gestora hash.
	 *
	 * @param listaEmpresaGestoraHash the lista empresa gestora hash
	 */
	public void setListaEmpresaGestoraHash(Map<Long, String> listaEmpresaGestoraHash) {
		this.listaEmpresaGestoraHash = listaEmpresaGestoraHash;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoImpl.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoImpl
	 */
	public IFiltroAgendamentoEfetivacaoEstornoService getFiltroAgendamentoEfetivacaoEstornoImpl() {
		return filtroAgendamentoEfetivacaoEstornoImpl;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoImpl.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoImpl the filtro agendamento efetivacao estorno impl
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoImpl(
			IFiltroAgendamentoEfetivacaoEstornoService filtroAgendamentoEfetivacaoEstornoImpl) {
		this.filtroAgendamentoEfetivacaoEstornoImpl = filtroAgendamentoEfetivacaoEstornoImpl;
	}

	/**
	 * Get: listaGridListaClientesMarq.
	 *
	 * @return listaGridListaClientesMarq
	 */
	public List<ListarClientesMarqSaidaDTO> getListaGridListaClientesMarq() {
		return listaGridListaClientesMarq;
	}

	/**
	 * Set: listaGridListaClientesMarq.
	 *
	 * @param listaGridListaClientesMarq the lista grid lista clientes marq
	 */
	public void setListaGridListaClientesMarq(
			List<ListarClientesMarqSaidaDTO> listaGridListaClientesMarq) {
		this.listaGridListaClientesMarq = listaGridListaClientesMarq;
	}

	/**
	 * Get: itemSelecionadoListaClientesMarq.
	 *
	 * @return itemSelecionadoListaClientesMarq
	 */
	public Integer getItemSelecionadoListaClientesMarq() {
		return itemSelecionadoListaClientesMarq;
	}

	/**
	 * Set: itemSelecionadoListaClientesMarq.
	 *
	 * @param itemSelecionadoListaClientesMarq the item selecionado lista clientes marq
	 */
	public void setItemSelecionadoListaClientesMarq(
			Integer itemSelecionadoListaClientesMarq) {
		this.itemSelecionadoListaClientesMarq = itemSelecionadoListaClientesMarq;
	}

	/**
	 * Get: listaControleRadioClientesMarq.
	 *
	 * @return listaControleRadioClientesMarq
	 */
	public List<SelectItem> getListaControleRadioClientesMarq() {
		return listaControleRadioClientesMarq;
	}

	/**
	 * Set: listaControleRadioClientesMarq.
	 *
	 * @param listaControleRadioClientesMarq the lista controle radio clientes marq
	 */
	public void setListaControleRadioClientesMarq(
			List<SelectItem> listaControleRadioClientesMarq) {
		this.listaControleRadioClientesMarq = listaControleRadioClientesMarq;
	}

	/**
	 * Get: itemSelecionadoListaContratosClienteMarq.
	 *
	 * @return itemSelecionadoListaContratosClienteMarq
	 */
	public Integer getItemSelecionadoListaContratosClienteMarq() {
		return itemSelecionadoListaContratosClienteMarq;
	}

	/**
	 * Set: itemSelecionadoListaContratosClienteMarq.
	 *
	 * @param itemSelecionadoListaContratosClienteMarq the item selecionado lista contratos cliente marq
	 */
	public void setItemSelecionadoListaContratosClienteMarq(
			Integer itemSelecionadoListaContratosClienteMarq) {
		this.itemSelecionadoListaContratosClienteMarq = itemSelecionadoListaContratosClienteMarq;
	}

	/**
	 * Get: listaControleRadioContratosClienteMarq.
	 *
	 * @return listaControleRadioContratosClienteMarq
	 */
	public List<SelectItem> getListaControleRadioContratosClienteMarq() {
		return listaControleRadioContratosClienteMarq;
	}

	/**
	 * Set: listaControleRadioContratosClienteMarq.
	 *
	 * @param listaControleRadioContratosClienteMarq the lista controle radio contratos cliente marq
	 */
	public void setListaControleRadioContratosClienteMarq(
			List<SelectItem> listaControleRadioContratosClienteMarq) {
		this.listaControleRadioContratosClienteMarq = listaControleRadioContratosClienteMarq;
	}

	/**
	 * Get: itemSelecionadoListaContratosMarq.
	 *
	 * @return itemSelecionadoListaContratosMarq
	 */
	public Integer getItemSelecionadoListaContratosMarq() {
		return itemSelecionadoListaContratosMarq;
	}

	/**
	 * Set: itemSelecionadoListaContratosMarq.
	 *
	 * @param itemSelecionadoListaContratosMarq the item selecionado lista contratos marq
	 */
	public void setItemSelecionadoListaContratosMarq(
			Integer itemSelecionadoListaContratosMarq) {
		this.itemSelecionadoListaContratosMarq = itemSelecionadoListaContratosMarq;
	}

	/**
	 * Get: listaControleRadioContratosMarq.
	 *
	 * @return listaControleRadioContratosMarq
	 */
	public List<SelectItem> getListaControleRadioContratosMarq() {
		return listaControleRadioContratosMarq;
	}

	/**
	 * Set: listaControleRadioContratosMarq.
	 *
	 * @param listaControleRadioContratosMarq the lista controle radio contratos marq
	 */
	public void setListaControleRadioContratosMarq(
			List<SelectItem> listaControleRadioContratosMarq) {
		this.listaControleRadioContratosMarq = listaControleRadioContratosMarq;
	}

	/**
	 * Get: listarContratosMarq.
	 *
	 * @return listarContratosMarq
	 */
	public List<ListarContratoMarqSaidaDTO> getListarContratosMarq() {
		return listarContratosMarq;
	}

	/**
	 * Set: listarContratosMarq.
	 *
	 * @param listarContratosMarq the listar contratos marq
	 */
	public void setListarContratosMarq(
			List<ListarContratoMarqSaidaDTO> listarContratosMarq) {
		this.listarContratosMarq = listarContratosMarq;
	}

	/**
	 * Get: itemSelecionadoListaContratoCliente.
	 *
	 * @return itemSelecionadoListaContratoCliente
	 */
	public Integer getItemSelecionadoListaContratoCliente() {
		return itemSelecionadoListaContratoCliente;
	}

	/**
	 * Set: itemSelecionadoListaContratoCliente.
	 *
	 * @param itemSelecionadoListaContratoCliente the item selecionado lista contrato cliente
	 */
	public void setItemSelecionadoListaContratoCliente(
			Integer itemSelecionadoListaContratoCliente) {
		this.itemSelecionadoListaContratoCliente = itemSelecionadoListaContratoCliente;
	}

	/**
	 * Get: listaContratoCliente.
	 *
	 * @return listaContratoCliente
	 */
	public List<ConsultarContratoClienteFiltroSaidaDTO> getListaContratoCliente() {
		return listaContratoCliente;
	}

	/**
	 * Set: listaContratoCliente.
	 *
	 * @param listaContratoCliente the lista contrato cliente
	 */
	public void setListaContratoCliente(
			List<ConsultarContratoClienteFiltroSaidaDTO> listaContratoCliente) {
		this.listaContratoCliente = listaContratoCliente;
	}

	/**
	 * Get: listaControleRadioContratoCliente.
	 *
	 * @return listaControleRadioContratoCliente
	 */
	public List<SelectItem> getListaControleRadioContratoCliente() {
		return listaControleRadioContratoCliente;
	}

	/**
	 * Set: listaControleRadioContratoCliente.
	 *
	 * @param listaControleRadioContratoCliente the lista controle radio contrato cliente
	 */
	public void setListaControleRadioContratoCliente(
			List<SelectItem> listaControleRadioContratoCliente) {
		this.listaControleRadioContratoCliente = listaControleRadioContratoCliente;
	}

	/**
	 * Get: telaVoltarIdentificacaoClienteContrato.
	 *
	 * @return telaVoltarIdentificacaoClienteContrato
	 */
	public String getTelaVoltarIdentificacaoClienteContrato() {
		return telaVoltarIdentificacaoClienteContrato;
	}

	/**
	 * Set: telaVoltarIdentificacaoClienteContrato.
	 *
	 * @param telaVoltarIdentificacaoClienteContrato the tela voltar identificacao cliente contrato
	 */
	public void setTelaVoltarIdentificacaoClienteContrato(
			String telaVoltarIdentificacaoClienteContrato) {
		this.telaVoltarIdentificacaoClienteContrato = telaVoltarIdentificacaoClienteContrato;
	}

	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Get: listaControleRadioClientes.
	 *
	 * @return listaControleRadioClientes
	 */
	public List<SelectItem> getListaControleRadioClientes() {
		return listaControleRadioClientes;
	}

	/**
	 * Set: listaControleRadioClientes.
	 *
	 * @param listaControleRadioClientes the lista controle radio clientes
	 */
	public void setListaControleRadioClientes(
			List<SelectItem> listaControleRadioClientes) {
		this.listaControleRadioClientes = listaControleRadioClientes;
	}

	/**
	 * Get: telaVoltarIdentificacaoClienteContratoMarq.
	 *
	 * @return telaVoltarIdentificacaoClienteContratoMarq
	 */
	public String getTelaVoltarIdentificacaoClienteContratoMarq() {
		return telaVoltarIdentificacaoClienteContratoMarq;
	}

	/**
	 * Set: telaVoltarIdentificacaoClienteContratoMarq.
	 *
	 * @param telaVoltarIdentificacaoClienteContratoMarq the tela voltar identificacao cliente contrato marq
	 */
	public void setTelaVoltarIdentificacaoClienteContratoMarq(
			String telaVoltarIdentificacaoClienteContratoMarq) {
		this.telaVoltarIdentificacaoClienteContratoMarq = telaVoltarIdentificacaoClienteContratoMarq;
	}

	/**
	 * Get: listaContratosClienteMarq.
	 *
	 * @return listaContratosClienteMarq
	 */
	public List<ListarContratosClienteMarqSaidaDTO> getListaContratosClienteMarq() {
		return listaContratosClienteMarq;
	}

	/**
	 * Set: listaContratosClienteMarq.
	 *
	 * @param listaContratosClienteMarq the lista contratos cliente marq
	 */
	public void setListaContratosClienteMarq(
			List<ListarContratosClienteMarqSaidaDTO> listaContratosClienteMarq) {
		this.listaContratosClienteMarq = listaContratosClienteMarq;
	}

	/**
	 * Get: itemSelecionadoListaClientePerfil.
	 *
	 * @return itemSelecionadoListaClientePerfil
	 */
	public Integer getItemSelecionadoListaClientePerfil() {
		return itemSelecionadoListaClientePerfil;
	}

	/**
	 * Set: itemSelecionadoListaClientePerfil.
	 *
	 * @param itemSelecionadoListaClientePerfil the item selecionado lista cliente perfil
	 */
	public void setItemSelecionadoListaClientePerfil(
			Integer itemSelecionadoListaClientePerfil) {
		this.itemSelecionadoListaClientePerfil = itemSelecionadoListaClientePerfil;
	}

	/**
	 * Get: listaConsultarListaClientePessoasPerfil.
	 *
	 * @return listaConsultarListaClientePessoasPerfil
	 */
	public List<ConsultarListaClientePessoasSaidaDTO> getListaConsultarListaClientePessoasPerfil() {
		return listaConsultarListaClientePessoasPerfil;
	}

	/**
	 * Set: listaConsultarListaClientePessoasPerfil.
	 *
	 * @param listaConsultarListaClientePessoasPerfil the lista consultar lista cliente pessoas perfil
	 */
	public void setListaConsultarListaClientePessoasPerfil(
			List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoasPerfil) {
		this.listaConsultarListaClientePessoasPerfil = listaConsultarListaClientePessoasPerfil;
	}

	/**
	 * Get: listaControleRadioClientePerfil.
	 *
	 * @return listaControleRadioClientePerfil
	 */
	public List<SelectItem> getListaControleRadioClientePerfil() {
		return listaControleRadioClientePerfil;
	}

	/**
	 * Set: listaControleRadioClientePerfil.
	 *
	 * @param listaControleRadioClientePerfil the lista controle radio cliente perfil
	 */
	public void setListaControleRadioClientePerfil(
			List<SelectItem> listaControleRadioClientePerfil) {
		this.listaControleRadioClientePerfil = listaControleRadioClientePerfil;
	}

	/**
	 * Get: itemSelecionadoListaContratoClientePerfil.
	 *
	 * @return itemSelecionadoListaContratoClientePerfil
	 */
	public Integer getItemSelecionadoListaContratoClientePerfil() {
		return itemSelecionadoListaContratoClientePerfil;
	}

	/**
	 * Set: itemSelecionadoListaContratoClientePerfil.
	 *
	 * @param itemSelecionadoListaContratoClientePerfil the item selecionado lista contrato cliente perfil
	 */
	public void setItemSelecionadoListaContratoClientePerfil(
			Integer itemSelecionadoListaContratoClientePerfil) {
		this.itemSelecionadoListaContratoClientePerfil = itemSelecionadoListaContratoClientePerfil;
	}

	/**
	 * Get: listaContratoClientePerfil.
	 *
	 * @return listaContratoClientePerfil
	 */
	public List<ConsultarContratoClienteFiltroSaidaDTO> getListaContratoClientePerfil() {
		return listaContratoClientePerfil;
	}

	/**
	 * Set: listaContratoClientePerfil.
	 *
	 * @param listaContratoClientePerfil the lista contrato cliente perfil
	 */
	public void setListaContratoClientePerfil(
			List<ConsultarContratoClienteFiltroSaidaDTO> listaContratoClientePerfil) {
		this.listaContratoClientePerfil = listaContratoClientePerfil;
	}

	/**
	 * Get: listaControleRadioContratoClientePerfil.
	 *
	 * @return listaControleRadioContratoClientePerfil
	 */
	public List<SelectItem> getListaControleRadioContratoClientePerfil() {
		return listaControleRadioContratoClientePerfil;
	}

	/**
	 * Set: listaControleRadioContratoClientePerfil.
	 *
	 * @param listaControleRadioContratoClientePerfil the lista controle radio contrato cliente perfil
	 */
	public void setListaControleRadioContratoClientePerfil(
			List<SelectItem> listaControleRadioContratoClientePerfil) {
		this.listaControleRadioContratoClientePerfil = listaControleRadioContratoClientePerfil;
	}

	/**
	 * Get: itemSelecionadoListaPerfilTroca.
	 *
	 * @return itemSelecionadoListaPerfilTroca
	 */
	public Integer getItemSelecionadoListaPerfilTroca() {
		return itemSelecionadoListaPerfilTroca;
	}

	/**
	 * Set: itemSelecionadoListaPerfilTroca.
	 *
	 * @param itemSelecionadoListaPerfilTroca the item selecionado lista perfil troca
	 */
	public void setItemSelecionadoListaPerfilTroca(
			Integer itemSelecionadoListaPerfilTroca) {
		this.itemSelecionadoListaPerfilTroca = itemSelecionadoListaPerfilTroca;
	}

	/**
	 * Get: listaControleRadioPerfilTroca.
	 *
	 * @return listaControleRadioPerfilTroca
	 */
	public List<SelectItem> getListaControleRadioPerfilTroca() {
		return listaControleRadioPerfilTroca;
	}

	/**
	 * Set: listaControleRadioPerfilTroca.
	 *
	 * @param listaControleRadioPerfilTroca the lista controle radio perfil troca
	 */
	public void setListaControleRadioPerfilTroca(
			List<SelectItem> listaControleRadioPerfilTroca) {
		this.listaControleRadioPerfilTroca = listaControleRadioPerfilTroca;
	}

	/**
	 * Get: listarPerfilTrocaArquivo.
	 *
	 * @return listarPerfilTrocaArquivo
	 */
	public List<ListarPerfilTrocaArquivoSaidaDTO> getListarPerfilTrocaArquivo() {
		return listarPerfilTrocaArquivo;
	}

	/**
	 * Set: listarPerfilTrocaArquivo.
	 *
	 * @param listarPerfilTrocaArquivo the listar perfil troca arquivo
	 */
	public void setListarPerfilTrocaArquivo(
			List<ListarPerfilTrocaArquivoSaidaDTO> listarPerfilTrocaArquivo) {
		this.listarPerfilTrocaArquivo = listarPerfilTrocaArquivo;
	}

	/**
	 * Get: cdCpfCnpjPessoaPerfil.
	 *
	 * @return cdCpfCnpjPessoaPerfil
	 */
	public String getCdCpfCnpjPessoaPerfil() {
		return cdCpfCnpjPessoaPerfil;
	}

	/**
	 * Set: cdCpfCnpjPessoaPerfil.
	 *
	 * @param cdCpfCnpjPessoaPerfil the cd cpf cnpj pessoa perfil
	 */
	public void setCdCpfCnpjPessoaPerfil(String cdCpfCnpjPessoaPerfil) {
		this.cdCpfCnpjPessoaPerfil = cdCpfCnpjPessoaPerfil;
	}

	/**
	 * Get: cdPessoaPerfil.
	 *
	 * @return cdPessoaPerfil
	 */
	public Long getCdPessoaPerfil() {
		return cdPessoaPerfil;
	}

	/**
	 * Set: cdPessoaPerfil.
	 *
	 * @param cdPessoaPerfil the cd pessoa perfil
	 */
	public void setCdPessoaPerfil(Long cdPessoaPerfil) {
		this.cdPessoaPerfil = cdPessoaPerfil;
	}

	/**
	 * Get: dsPessoaPerfil.
	 *
	 * @return dsPessoaPerfil
	 */
	public String getDsPessoaPerfil() {
		return dsPessoaPerfil;
	}

	/**
	 * Set: dsPessoaPerfil.
	 *
	 * @param dsPessoaPerfil the ds pessoa perfil
	 */
	public void setDsPessoaPerfil(String dsPessoaPerfil) {
		this.dsPessoaPerfil = dsPessoaPerfil;
	}

	/**
	 * Get: telaVoltarIdentificacaoPerfil.
	 *
	 * @return telaVoltarIdentificacaoPerfil
	 */
	public String getTelaVoltarIdentificacaoPerfil() {
		return telaVoltarIdentificacaoPerfil;
	}

	/**
	 * Set: telaVoltarIdentificacaoPerfil.
	 *
	 * @param telaVoltarIdentificacaoPerfil the tela voltar identificacao perfil
	 */
	public void setTelaVoltarIdentificacaoPerfil(
			String telaVoltarIdentificacaoPerfil) {
		this.telaVoltarIdentificacaoPerfil = telaVoltarIdentificacaoPerfil;
	}

	/**
	 * Get: itemClienteSelecionadoContratoPerfil.
	 *
	 * @return itemClienteSelecionadoContratoPerfil
	 */
	public Integer getItemClienteSelecionadoContratoPerfil() {
		return itemClienteSelecionadoContratoPerfil;
	}

	/**
	 * Set: itemClienteSelecionadoContratoPerfil.
	 *
	 * @param itemClienteSelecionadoContratoPerfil the item cliente selecionado contrato perfil
	 */
	public void setItemClienteSelecionadoContratoPerfil(
			Integer itemClienteSelecionadoContratoPerfil) {
		this.itemClienteSelecionadoContratoPerfil = itemClienteSelecionadoContratoPerfil;
	}

	/**
	 * Get: listaConsultarListaContratosPessoasPerfil.
	 *
	 * @return listaConsultarListaContratosPessoasPerfil
	 */
	public List<ConsultarListaContratosPessoasSaidaDTO> getListaConsultarListaContratosPessoasPerfil() {
		return listaConsultarListaContratosPessoasPerfil;
	}

	/**
	 * Set: listaConsultarListaContratosPessoasPerfil.
	 *
	 * @param listaConsultarListaContratosPessoasPerfil the lista consultar lista contratos pessoas perfil
	 */
	public void setListaConsultarListaContratosPessoasPerfil(
			List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoasPerfil) {
		this.listaConsultarListaContratosPessoasPerfil = listaConsultarListaContratosPessoasPerfil;
	}

	/**
	 * Get: listaControleRadioContratosPessoasPerfil.
	 *
	 * @return listaControleRadioContratosPessoasPerfil
	 */
	public List<SelectItem> getListaControleRadioContratosPessoasPerfil() {
		return listaControleRadioContratosPessoasPerfil;
	}

	/**
	 * Set: listaControleRadioContratosPessoasPerfil.
	 *
	 * @param listaControleRadioContratosPessoasPerfil the lista controle radio contratos pessoas perfil
	 */
	public void setListaControleRadioContratosPessoasPerfil(
			List<SelectItem> listaControleRadioContratosPessoasPerfil) {
		this.listaControleRadioContratosPessoasPerfil = listaControleRadioContratosPessoasPerfil;
	}

	/**
	 * Get: cdPerfilTrocaArquivoPerfilCliente.
	 *
	 * @return cdPerfilTrocaArquivoPerfilCliente
	 */
	public Long getCdPerfilTrocaArquivoPerfilCliente() {
		return cdPerfilTrocaArquivoPerfilCliente;
	}

	/**
	 * Set: cdPerfilTrocaArquivoPerfilCliente.
	 *
	 * @param cdPerfilTrocaArquivoPerfilCliente the cd perfil troca arquivo perfil cliente
	 */
	public void setCdPerfilTrocaArquivoPerfilCliente(
			Long cdPerfilTrocaArquivoPerfilCliente) {
		this.cdPerfilTrocaArquivoPerfilCliente = cdPerfilTrocaArquivoPerfilCliente;
	}

	/**
	 * Get: cdPerfilTrocaArquivoPerfilContrato.
	 *
	 * @return cdPerfilTrocaArquivoPerfilContrato
	 */
	public Long getCdPerfilTrocaArquivoPerfilContrato() {
		return cdPerfilTrocaArquivoPerfilContrato;
	}

	/**
	 * Set: cdPerfilTrocaArquivoPerfilContrato.
	 *
	 * @param cdPerfilTrocaArquivoPerfilContrato the cd perfil troca arquivo perfil contrato
	 */
	public void setCdPerfilTrocaArquivoPerfilContrato(
			Long cdPerfilTrocaArquivoPerfilContrato) {
		this.cdPerfilTrocaArquivoPerfilContrato = cdPerfilTrocaArquivoPerfilContrato;
	}

	/**
	 * Get: dsSituacaoPerfilCliente.
	 *
	 * @return dsSituacaoPerfilCliente
	 */
	public String getDsSituacaoPerfilCliente() {
		return dsSituacaoPerfilCliente;
	}

	/**
	 * Set: dsSituacaoPerfilCliente.
	 *
	 * @param dsSituacaoPerfilCliente the ds situacao perfil cliente
	 */
	public void setDsSituacaoPerfilCliente(String dsSituacaoPerfilCliente) {
		this.dsSituacaoPerfilCliente = dsSituacaoPerfilCliente;
	}

	/**
	 * Get: dsSituacaoPerfilContrato.
	 *
	 * @return dsSituacaoPerfilContrato
	 */
	public String getDsSituacaoPerfilContrato() {
		return dsSituacaoPerfilContrato;
	}

	/**
	 * Set: dsSituacaoPerfilContrato.
	 *
	 * @param dsSituacaoPerfilContrato the ds situacao perfil contrato
	 */
	public void setDsSituacaoPerfilContrato(String dsSituacaoPerfilContrato) {
		this.dsSituacaoPerfilContrato = dsSituacaoPerfilContrato;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultaService the consultas service
	 */
	public void setConsultasService(IConsultasService consultaService) {
		this.consultasService = consultaService;
	}

	/**
	 * Get: codigoParticipanteFiltro.
	 *
	 * @return codigoParticipanteFiltro
	 */
	public Long getCodigoParticipanteFiltro() {
		return codigoParticipanteFiltro;
	}

	/**
	 * Set: codigoParticipanteFiltro.
	 *
	 * @param codigoParticipanteFiltro the codigo participante filtro
	 */
	public void setCodigoParticipanteFiltro(Long codigoParticipanteFiltro) {
		this.codigoParticipanteFiltro = codigoParticipanteFiltro;
	}

	/**
	 * Get: controleCpfCnpjParticipanteFiltro.
	 *
	 * @return controleCpfCnpjParticipanteFiltro
	 */
	public Integer getControleCpfCnpjParticipanteFiltro() {
		return controleCpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: controleCpfCnpjParticipanteFiltro.
	 *
	 * @param controleCpfCnpjParticipanteFiltro the controle cpf cnpj participante filtro
	 */
	public void setControleCpfCnpjParticipanteFiltro(
			Integer controleCpfCnpjParticipanteFiltro) {
		this.controleCpfCnpjParticipanteFiltro = controleCpfCnpjParticipanteFiltro;
	}

	/**
	 * Get: corpoCpfCnpjParticipanteFiltro.
	 *
	 * @return corpoCpfCnpjParticipanteFiltro
	 */
	public Long getCorpoCpfCnpjParticipanteFiltro() {
		return corpoCpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: corpoCpfCnpjParticipanteFiltro.
	 *
	 * @param corpoCpfCnpjParticipanteFiltro the corpo cpf cnpj participante filtro
	 */
	public void setCorpoCpfCnpjParticipanteFiltro(
			Long corpoCpfCnpjParticipanteFiltro) {
		this.corpoCpfCnpjParticipanteFiltro = corpoCpfCnpjParticipanteFiltro;
	}

	/**
	 * Get: cpfCnpjParticipanteFiltro.
	 *
	 * @return cpfCnpjParticipanteFiltro
	 */
	public String getCpfCnpjParticipanteFiltro() {
		return cpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: cpfCnpjParticipanteFiltro.
	 *
	 * @param cpfCnpjParticipanteFiltro the cpf cnpj participante filtro
	 */
	public void setCpfCnpjParticipanteFiltro(String cpfCnpjParticipanteFiltro) {
		this.cpfCnpjParticipanteFiltro = cpfCnpjParticipanteFiltro;
	}

	/**
	 * Get: filialCpfCnpjParticipanteFiltro.
	 *
	 * @return filialCpfCnpjParticipanteFiltro
	 */
	public Integer getFilialCpfCnpjParticipanteFiltro() {
		return filialCpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: filialCpfCnpjParticipanteFiltro.
	 *
	 * @param filialCpfCnpjParticipanteFiltro the filial cpf cnpj participante filtro
	 */
	public void setFilialCpfCnpjParticipanteFiltro(
			Integer filialCpfCnpjParticipanteFiltro) {
		this.filialCpfCnpjParticipanteFiltro = filialCpfCnpjParticipanteFiltro;
	}

	/**
	 * Get: nomeParticipanteFiltro.
	 *
	 * @return nomeParticipanteFiltro
	 */
	public String getNomeParticipanteFiltro() {
		return nomeParticipanteFiltro;
	}

	/**
	 * Set: nomeParticipanteFiltro.
	 *
	 * @param nomeParticipanteFiltro the nome participante filtro
	 */
	public void setNomeParticipanteFiltro(String nomeParticipanteFiltro) {
		this.nomeParticipanteFiltro = nomeParticipanteFiltro;
	}

	/**
	 * Get: itemSelecionadoListaParcipantes.
	 *
	 * @return itemSelecionadoListaParcipantes
	 */
	public Integer getItemSelecionadoListaParcipantes() {
		return itemSelecionadoListaParcipantes;
	}

	/**
	 * Set: itemSelecionadoListaParcipantes.
	 *
	 * @param itemSelecionadoListaParcipantes the item selecionado lista parcipantes
	 */
	public void setItemSelecionadoListaParcipantes(
			Integer itemSelecionadoListaParcipantes) {
		this.itemSelecionadoListaParcipantes = itemSelecionadoListaParcipantes;
	}

	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<ConsultarParticipanteContratoSaidaDTO> getListaParticipantes() {
		return listaParticipantes;
	}

	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(
			List<ConsultarParticipanteContratoSaidaDTO> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}

	/**
	 * Get: listaParticipantesControleRadio.
	 *
	 * @return listaParticipantesControleRadio
	 */
	public List<SelectItem> getListaParticipantesControleRadio() {
		return listaParticipantesControleRadio;
	}

	/**
	 * Set: listaParticipantesControleRadio.
	 *
	 * @param listaParticipantesControleRadio the lista participantes controle radio
	 */
	public void setListaParticipantesControleRadio(
			List<SelectItem> listaParticipantesControleRadio) {
		this.listaParticipantesControleRadio = listaParticipantesControleRadio;
	}

	/**
	 * Get: voltarParticipante.
	 *
	 * @return voltarParticipante
	 */
	public String getVoltarParticipante() {
		return voltarParticipante;
	}

	/**
	 * Set: voltarParticipante.
	 *
	 * @param voltarParticipante the voltar participante
	 */
	public void setVoltarParticipante(String voltarParticipante) {
		this.voltarParticipante = voltarParticipante;
	}

	/**
	 * Get: consultarPagamentosIndividualServiceImpl.
	 *
	 * @return consultarPagamentosIndividualServiceImpl
	 */
	public IConsultarPagamentosIndividualService getConsultarPagamentosIndividualServiceImpl() {
		return consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Set: consultarPagamentosIndividualServiceImpl.
	 *
	 * @param consultarPagamentosIndividualServiceImpl the consultar pagamentos individual service impl
	 */
	public void setConsultarPagamentosIndividualServiceImpl(
			IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl) {
		this.consultarPagamentosIndividualServiceImpl = consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}

	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: tipoRastreadoSelecionado.
	 *
	 * @return tipoRastreadoSelecionado
	 */
	public String getTipoRastreadoSelecionado() {
		return tipoRastreadoSelecionado;
	}

	/**
	 * Set: tipoRastreadoSelecionado.
	 *
	 * @param tipoRastreadoSelecionado the tipo rastreado selecionado
	 */
	public void setTipoRastreadoSelecionado(String tipoRastreadoSelecionado) {
		this.tipoRastreadoSelecionado = tipoRastreadoSelecionado;
	}

	/**
	 * Get: tipoAntecipacao.
	 *
	 * @return tipoAntecipacao
	 */
	public Integer getTipoAntecipacao() {
		return tipoAntecipacao;
	}

	/**
	 * Set: tipoAntecipacao.
	 *
	 * @param tipoAntecipacao the tipo antecipacao
	 */
	public void setTipoAntecipacao(Integer tipoAntecipacao) {
		this.tipoAntecipacao = tipoAntecipacao;
	}

	/**
	 * Get: descricaoTipoContratoDescContrato.
	 *
	 * @return descricaoTipoContratoDescContrato
	 */
	public String getDescricaoTipoContratoDescContrato() {
		return descricaoTipoContratoDescContrato;
	}

	/**
	 * Set: descricaoTipoContratoDescContrato.
	 *
	 * @param descricaoTipoContratoDescContrato the descricao tipo contrato desc contrato
	 */
	public void setDescricaoTipoContratoDescContrato(
			String descricaoTipoContratoDescContrato) {
		this.descricaoTipoContratoDescContrato = descricaoTipoContratoDescContrato;
	}

	/**
	 * Get: codPessoaParticipante.
	 *
	 * @return codPessoaParticipante
	 */
	public Long getCodPessoaParticipante() {
		return codPessoaParticipante;
	}

	/**
	 * Set: codPessoaParticipante.
	 *
	 * @param codPessoaParticipante the cod pessoa participante
	 */
	public void setCodPessoaParticipante(Long codPessoaParticipante) {
		this.codPessoaParticipante = codPessoaParticipante;
	}

	/**
	 * Is sinaliza orgao publico.
	 *
	 * @return true, if is sinaliza orgao publico
	 */
	public boolean isSinalizaOrgaoPublico() {
		return sinalizaOrgaoPublico;
	}

	/**
	 * Set: sinalizaOrgaoPublico.
	 *
	 * @param sinalizaOrgaoPublico the sinaliza orgao publico
	 */
	public void setSinalizaOrgaoPublico(boolean sinalizaOrgaoPublico) {
		this.sinalizaOrgaoPublico = sinalizaOrgaoPublico;
	}

	/**
	 * Get: saidaValidarContratoPagSalPartOrgaoPublico.
	 *
	 * @return saidaValidarContratoPagSalPartOrgaoPublico
	 */
	public ValidarContratoPagSalPartOrgaoPublicoSaidaDTO getSaidaValidarContratoPagSalPartOrgaoPublico() {
		return saidaValidarContratoPagSalPartOrgaoPublico;
	}

	/**
	 * Set: saidaValidarContratoPagSalPartOrgaoPublico.
	 *
	 * @param saidaValidarContratoPagSalPartOrgaoPublico the saida validar contrato pag sal part orgao publico
	 */
	public void setSaidaValidarContratoPagSalPartOrgaoPublico(
			ValidarContratoPagSalPartOrgaoPublicoSaidaDTO saidaValidarContratoPagSalPartOrgaoPublico) {
		this.saidaValidarContratoPagSalPartOrgaoPublico = saidaValidarContratoPagSalPartOrgaoPublico;
	}

	/**
	 * Get: manterClientesOrgaoPublicoService.
	 *
	 * @return manterClientesOrgaoPublicoService
	 */
	public IManterClientesOrgaoPublicoService getManterClientesOrgaoPublicoService() {
		return manterClientesOrgaoPublicoService;
	}

	/**
	 * Set: manterClientesOrgaoPublicoService.
	 *
	 * @param manterClientesOrgaoPublicoService the manter clientes orgao publico service
	 */
	public void setManterClientesOrgaoPublicoService(
			IManterClientesOrgaoPublicoService manterClientesOrgaoPublicoService) {
		this.manterClientesOrgaoPublicoService = manterClientesOrgaoPublicoService;
	}

	/**
	 * Get: numeroDescContratoOrgao.
	 *
	 * @return numeroDescContratoOrgao
	 */
	public String getNumeroDescContratoOrgao() {
		return numeroDescContratoOrgao;
	}

	/**
	 * Set: numeroDescContratoOrgao.
	 *
	 * @param numeroDescContratoOrgao the numero desc contrato orgao
	 */
	public void setNumeroDescContratoOrgao(String numeroDescContratoOrgao) {
		this.numeroDescContratoOrgao = numeroDescContratoOrgao;
	}

	/**
	 * Get: descricaoContratoDescContratoOrgao.
	 *
	 * @return descricaoContratoDescContratoOrgao
	 */
	public String getDescricaoContratoDescContratoOrgao() {
		return descricaoContratoDescContratoOrgao;
	}

	/**
	 * Set: descricaoContratoDescContratoOrgao.
	 *
	 * @param descricaoContratoDescContratoOrgao the descricao contrato desc contrato orgao
	 */
	public void setDescricaoContratoDescContratoOrgao(
			String descricaoContratoDescContratoOrgao) {
		this.descricaoContratoDescContratoOrgao = descricaoContratoDescContratoOrgao;
	}

	/**
	 * Get: situacaoDescContratoOrgao.
	 *
	 * @return situacaoDescContratoOrgao
	 */
	public String getSituacaoDescContratoOrgao() {
		return situacaoDescContratoOrgao;
	}

	/**
	 * Set: situacaoDescContratoOrgao.
	 *
	 * @param situacaoDescContratoOrgao the situacao desc contrato orgao
	 */
	public void setSituacaoDescContratoOrgao(String situacaoDescContratoOrgao) {
		this.situacaoDescContratoOrgao = situacaoDescContratoOrgao;
	}

	/**
	 * Get: empresaGestoraDescContratoOrgao.
	 *
	 * @return empresaGestoraDescContratoOrgao
	 */
	public String getEmpresaGestoraDescContratoOrgao() {
		return empresaGestoraDescContratoOrgao;
	}

	/**
	 * Set: empresaGestoraDescContratoOrgao.
	 *
	 * @param empresaGestoraDescContratoOrgao the empresa gestora desc contrato orgao
	 */
	public void setEmpresaGestoraDescContratoOrgao(
			String empresaGestoraDescContratoOrgao) {
		this.empresaGestoraDescContratoOrgao = empresaGestoraDescContratoOrgao;
	}

}
