/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.duplarqretorno
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.duplarqretorno;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.IDuplArqRetornoService;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.AlterarDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.AlterarDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DetalharDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DetalharDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DuplicacaoArquivoRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.DuplicacaoArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.ExcluirDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.ExcluirDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.IncluirDuplicacaoArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.duplarqretorno.bean.IncluirDuplicacaoArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaLayoutArquivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaLayoutArquivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaTipoRetornoLayoutEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearparticipantes.BloqDesbloqParticipantesBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ManterContratoBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ParticipantesBean;

/**
 * Nome: DuplicacaoArquivoRetornoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DuplicacaoArquivoRetornoBean extends IdentificacaoClienteBean {
	
	/** Atributo CON_DUPLICACAO_ARQUIVO_RETORNO. */
	private static final String CON_DUPLICACAO_ARQUIVO_RETORNO = "conDuplicacaoArquivoRetorno";
	
	/** Atributo filtroTipoArquivoRetorno. */
	private Integer filtroTipoArquivoRetorno;
	
	/** Atributo filtroTipoLayout. */
	private Integer filtroTipoLayout;
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;
	
	/** Atributo btoConsultarParticipantes. */
	private boolean btoConsultarParticipantes;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo bloqDesbloqParticipantesBean. */
	private BloqDesbloqParticipantesBean bloqDesbloqParticipantesBean;
	
	/** Atributo manterContratoBean. */
	private ManterContratoBean manterContratoBean;
	
	/** Atributo manterContratoParticipantesBean. */
	private ParticipantesBean manterContratoParticipantesBean;

	/** Atributo dsTipoArquivo. */
	private String dsTipoArquivo;
	
	/** Atributo tipoArquivoFormatado. */
	private String tipoArquivoFormatado;
	
	/** Atributo cdTipoArquivo. */
	private Integer cdTipoArquivo;
	
	/** Atributo dsTipoLayout. */
	private String dsTipoLayout;
	
	/** Atributo tipoLayoutFormatado. */
	private String tipoLayoutFormatado;
	
	/** Atributo cdTipoLayout. */
	private Integer cdTipoLayout;
	
	/** Atributo duplicacaoParticipanteMesmoContrato. */
	private Integer duplicacaoParticipanteMesmoContrato;
	
	/** Atributo duplicacaoParticipanteMesmoContratoAux. */
	private Integer duplicacaoParticipanteMesmoContratoAux;
	
	/** Atributo cdParticipantes. */
	private Long cdParticipantes;
	
	/** Atributo tipoDuplicacao. */
	private Integer tipoDuplicacao;
	
	/** Atributo instrucao. */
	private Integer instrucao;
	
	/** Atributo cdCanalInclusao. */
	private Integer cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo dsContratoSaida. */
	private String dsContratoSaida;
    
	/** Atributo cdPessoaJuridVinc. */
	private String cdPessoaJuridVinc;
	
	/** Atributo cdTipoContratoVinc. */
	private String cdTipoContratoVinc;
	
	/** Atributo nrSeqContratoVinc. */
	private String nrSeqContratoVinc;
	
	/** Atributo cdPessoaJuridContrato. */
	private Integer cdPessoaJuridContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSeqContratoNegocio. */
	private String nrSeqContratoNegocio;
	
	/** Atributo dsPessoaJuridicaContrato. */
	private String dsPessoaJuridicaContrato;
	
	/** Atributo dsTipoContratoNegocio. */
	private String dsTipoContratoNegocio;
	
	/** Atributo dsSituacaoParticipacaoContrato. */
	private String dsSituacaoParticipacaoContrato;
	
	/** Atributo dsPessoaJuridicaVinc. */
	private String dsPessoaJuridicaVinc;
	
	/** Atributo dsTipoContratoVinc. */
	private String dsTipoContratoVinc;
	
	/** Atributo dsSituacaoParticipacaoVinc. */
	private String dsSituacaoParticipacaoVinc;
	
	/** Atributo dsTipoParticipacaoPessoa. */
	private String dsTipoParticipacaoPessoa;
	
	/** Atributo dsTipoParticipacaoPessoaVinc. */
	private String dsTipoParticipacaoPessoaVinc;
	
	/** Atributo dsPessoaJuridicaClienteContrato. */
	private String dsPessoaJuridicaClienteContrato;
	
	/** Atributo dsTipoContratoNegocioClienteContrato. */
	private String dsTipoContratoNegocioClienteContrato;
	
	/** Atributo nrSeqContratoNegocioClienteContrato. */
	private Long nrSeqContratoNegocioClienteContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo dsSituacaoContratoNegocio. */
	private String dsSituacaoContratoNegocio;
	
	/** Atributo cdPessoaJuridAnt. */
	private Long cdPessoaJuridAnt;
	
	/** Atributo cdTipoContratoAnt. */
	private Integer cdTipoContratoAnt;
	
	/** Atributo nrSeqContratoAnt. */
	private Long nrSeqContratoAnt;
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
		
	/** Atributo listaGridPesquisa. */
	private List<DuplicacaoArquivoRetornoSaidaDTO> listaGridPesquisa;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo duplicacaoArquivoRetornoImpl. */
	private IDuplArqRetornoService duplicacaoArquivoRetornoImpl;
	
	/** Atributo participantesDesc. */
	private String participantesDesc;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;	
	
	/** Atributo habilitaParticipante. */
	private boolean habilitaParticipante;
	
	/** Atributo habilitaFiltroDeCliente. */
	private boolean habilitaFiltroDeCliente;
	
	
	/** Atributo listaGrid. */
	private List<ListarParticipantesSaidaDTO> listaGrid;
	
	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle;
	
	/** Atributo itemSelecionadoGrid. */
	private Integer itemSelecionadoGrid;
	
	/** Atributo listaLayout. */
	private List<ConsultarListaLayoutArquivoContratoSaidaDTO> listaLayout;
	
	/** Atributo listaLayoutDTO. */
	private ConsultarListaLayoutArquivoContratoEntradaDTO listaLayoutDTO = new ConsultarListaLayoutArquivoContratoEntradaDTO();
	
	//combos
	/** Atributo listaTipoLayout. */
	private List<SelectItem> listaTipoLayout =  new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutHash. */
	private Map<Integer,String> listaTipoLayoutHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoArquivoRetornoFiltro. */
	private List<SelectItem> listaTipoArquivoRetornoFiltro =  new ArrayList<SelectItem>();
	
	/** Atributo listaTipoArquivoRetornoFiltroHash. */
	private Map<Integer,String> listaTipoArquivoRetornoFiltroHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoArquivoRetorno. */
	private List<SelectItem> listaTipoArquivoRetorno =  new ArrayList<SelectItem>();
	
	/** Atributo listaTipoArquivoRetornoHash. */
	private Map<Integer,String> listaTipoArquivoRetornoHash = new HashMap<Integer,String>();
	
	/** Atributo listaParticipante. */
	private List<SelectItem> listaParticipante =  new ArrayList<SelectItem>();
	
	/** Atributo listaParticipanteHash. */
	private Map<Long,String> listaParticipanteHash = new HashMap<Long,String>();
	
	/** Atributo listaRetorno. */
	private List<ConsultarListaTipoRetornoLayoutSaidaDTO> listaRetorno;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo retornoParticipantes. */
	private String retornoParticipantes;
	
	/** Atributo cdPessoa. */
	private Long cdPessoa;
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;
	
	/** Atributo nomeRazaoSocialParticipante. */
	private String nomeRazaoSocialParticipante;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa; 

	/** Atributo participanteCpfCnpj. */
	private String participanteCpfCnpj;
	
	/** Atributo participanteNomeRazaoSocial. */
	private String participanteNomeRazaoSocial;
	
	/** Atributo participanteNivelParticipacao. */
	private String participanteNivelParticipacao;
	
	/** Atributo participanteSituacaoParticipacao. */
	private String participanteSituacaoParticipacao;
	
	
	/** Atributo cpfCnpjParticipanteAux. */
	private String cpfCnpjParticipanteAux;
	
	/** Atributo nomeRazaoSocialParticipanteAux. */
	private String nomeRazaoSocialParticipanteAux;
	
	/** Atributo flagMesmoParticipante. */
	private String flagMesmoParticipante;

	/**
	 * Duplicacao arquivo retorno bean.
	 */
	public DuplicacaoArquivoRetornoBean() {
		super(CON_DUPLICACAO_ARQUIVO_RETORNO, "identificaoDuplArqRetornoCliente", "identificaoDuplArqRetornoContrato");
	}
	

	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		limparCampos();
		
		//carrega os combos da tela de consultar
//		try{
//			listarTipoLayout();
//		}catch(PdcAdapterFunctionalException e){
//			listaTipoLayout = new ArrayList<SelectItem>();
//		}
		
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());

		//carrega os combos do contrato
		this.setItemFiltroSelecionado(null);
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());
		this.setHabilitaFiltroDeContrato(true);
		this.setBloqueaTipoContrato(true);
		this.setDesabilataFiltro(true);
		this.setBloqueaRadio(false);
		this.loadEmpresaGestora();
		this.loadTipoContrato();
		setBtoAcionado(false);
		getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(2269651L);
	}
	
	/**
	 * Limpar pagina argumentos.
	 *
	 * @return the string
	 */
	public String limparPaginaArgumentos() {
		limparCliente();
		limparContrato();
		limparArgumentosPesquisa();
		return CON_DUPLICACAO_ARQUIVO_RETORNO;
	}

	/**
	 * Pesquisar.
	 *
	 * @param event the event
	 * @return the string
	 */
	public String pesquisar(ActionEvent event) {
		return "";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_DUPLICACAO_ARQUIVO_RETORNO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		
		return "AVANCAR_DETALHAR";
	}	
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		// participante habilitado.	
		if (getDuplicacaoParticipanteMesmoContrato() == null || getDuplicacaoParticipanteMesmoContrato() == 1){
			setHabilitaParticipante(true);
		}else{
			// participante n�o habilitado.	
			if (getDuplicacaoParticipanteMesmoContrato() == 2){
				setHabilitaParticipante(false);
			}
		}	

		this.setItemFiltroSelecionadoManutencao(null);
		this.setHabilitaFiltroDeContratoManutencao(true);
		this.setSaidaConsultarListaClientePessoasManutencao(new ConsultarListaClientePessoasSaidaDTO());
		this.setEntradaConsultarListaClientePessoasManutencao(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoasManutencao(new ConsultarListaContratosPessoasEntradaDTO());
		this.setSaidaConsultarListaContratosPessoasManutencao(new ConsultarListaContratosPessoasSaidaDTO());
		setPaginaClienteManutencao("identificaoIncDuplArqRetornoCliente");
		setPaginaRetornoManutencao("incDuplicacaoArquivoRetorno");
		setPaginaContratoManutencao("identificaoIncDuplArqRetornoContrato");
		
		listaTipoArquivoRetorno = new ArrayList<SelectItem>();
		setCdTipoArquivo(0);
		setCdTipoLayout(0);
		setDuplicacaoParticipanteMesmoContrato(null);
		setTipoDuplicacao(null);
		setInstrucao(null);
		setParticipantesDesc("");
		setCdParticipantes(0l);
		setBtoConsultarParticipantes(false);
		setCdPessoa(0L);
		setCpfCnpjParticipante("");
		setNomeRazaoSocialParticipante("");
		setDsPessoaJuridicaClienteContrato("");
		setDsTipoContratoNegocioClienteContrato("");
		setNrSeqContratoNegocioClienteContrato(null);
		setDsContrato("");
		setDsSituacaoContratoNegocio("");
		setParticipanteCpfCnpj("");
		setParticipanteNomeRazaoSocial("");
		setParticipanteNivelParticipacao("");
		setParticipanteSituacaoParticipacao("");

		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		try {
			preencheDados();
			listarTipoLayout();
			listarTipoArquivoRetorno();
			preencheDadosContratoAlterar();
	
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_DUPLICACAO_ARQUIVO_RETORNO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		
		// participante habilitado.	
		if (getDuplicacaoParticipanteMesmoContrato() == null || getDuplicacaoParticipanteMesmoContrato() == 1){
			setHabilitaParticipante(true);
		}else{
			// participante n?o habilitado.	
			if ( getDuplicacaoParticipanteMesmoContrato() == null || getDuplicacaoParticipanteMesmoContrato() == 2){
				setHabilitaParticipante(false);
			}
		}
		

		this.setItemFiltroSelecionadoManutencao(null);
		this.setHabilitaFiltroDeContratoManutencao(true);
		this.setSaidaConsultarListaClientePessoasManutencao(new ConsultarListaClientePessoasSaidaDTO());
		this.setEntradaConsultarListaClientePessoasManutencao(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoasManutencao(new ConsultarListaContratosPessoasEntradaDTO());
		this.setSaidaConsultarListaContratosPessoasManutencao(new ConsultarListaContratosPessoasSaidaDTO());
		setPaginaClienteManutencao("identificaoIncDuplArqRetornoCliente");
		setPaginaRetornoManutencao("altDuplicacaoArquivoRetorno");
		setPaginaContratoManutencao("identificaoIncDuplArqRetornoContrato");
		setDuplicacaoParticipanteMesmoContratoAux(getDuplicacaoParticipanteMesmoContrato());
		//setDuplicacaoParticipanteMesmoContrato(1);
		setCdPessoaJuridAnt(listaGridPesquisa.get(itemSelecionadoLista).getCdPessoaJuridVinc());
		setCdTipoContratoAnt(listaGridPesquisa.get(itemSelecionadoLista).getCdTipoContratoVinc());
		setNrSeqContratoAnt(listaGridPesquisa.get(itemSelecionadoLista).getNrSeqContratoVinc());
		
		return "AVANCAR_ALTERAR";
	}

	/**
	 * Preenche dados alterar.
	 */
	public void preencheDadosAlterar() {
		DetalharDuplicacaoArqRetornoEntradaDTO entradaDTO = new DetalharDuplicacaoArqRetornoEntradaDTO();
		DetalharDuplicacaoArqRetornoSaidaDTO saidaDTO = new DetalharDuplicacaoArqRetornoSaidaDTO();
		DuplicacaoArquivoRetornoSaidaDTO itemSelecionado = getListaGridPesquisa().get(getItemSelecionadoLista());

		//dados destino 
		entradaDTO.setCdPessoaJuridVinc(itemSelecionado.getCdPessoaJuridVinc());
		entradaDTO.setCdTipoContratoVinc(itemSelecionado.getCdTipoContratoVinc());
		entradaDTO.setNrSeqContratoVinc(itemSelecionado.getNrSeqContratoVinc());
		//dados origem
		entradaDTO.setCdPessoaJuridContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaDTO.setNrSeqContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());

		entradaDTO.setCdTipoLayoutArquivo(itemSelecionado.getCdTipoLayoutArquivo());
		entradaDTO.setCdTipoArquivoRetorno(itemSelecionado.getCdTipoArquivoRetorno());
		entradaDTO.setCdTipoParticipacaoPessoa(0);
		entradaDTO.setCdPessoa(0l);
		entradaDTO.setCdInstrucaoEnvioRetorno(itemSelecionado.getCdInstrucaoEnvioRetorno());
		entradaDTO.setCdIndicadorDuplicidadeRetorno(itemSelecionado.getCdIndicadorDuplicidadeRetorno());
		entradaDTO.setCdProdutoServicoOper(0); 

		saidaDTO = getDuplicacaoArquivoRetornoImpl().detalharDuplicacaoArqRetorno(entradaDTO);

		setDsContratoSaida(this.getSaidaConsultarListaContratosPessoas().getDsContrato());
		setCdTipoArquivo(saidaDTO.getCdTipoArquivoRetorno());
		setDsTipoArquivo(saidaDTO.getDsTipoArquivoRetorno());
		setTipoArquivoFormatado(PgitUtil.concatenarCampos(getCdTipoArquivo(), getDsTipoArquivo()));
		setCdTipoLayout(saidaDTO.getCdTipoLayoutArquivo());
		setDsTipoLayout(saidaDTO.getDsTipoLayoutArquivo());
		setTipoLayoutFormatado(PgitUtil.concatenarCampos(getCdTipoLayout(), getDsTipoLayout()));

		if(saidaDTO.getNrSeqContratoNegocio() == saidaDTO.getNrSeqContratoVinc()){
		    setDuplicacaoParticipanteMesmoContrato(1);
		    setParticipantesDesc(saidaDTO.getCdPessoa()+"-"+saidaDTO.getDsPessoa());
		    setCdParticipantes(saidaDTO.getCdPessoa());		    
		}
		// SE TIPO DE PARTICIPA��O FOR IGUAL A 0 ENT�O USU�RIO N�O SELECIONOU PARTICIPANTE E RADIO DEVE SER "N�O"
		else{
		    setDuplicacaoParticipanteMesmoContrato(2);		    
		    setParticipantesDesc("");
		    setCdParticipantes(0l);		    
		}

		setCpfCnpjParticipante("");
		setNomeRazaoSocialParticipante("");

		if (getDuplicacaoParticipanteMesmoContrato().intValue() == 1){
				setCpfCnpjParticipante(saidaDTO.getCpfCnpjParticipante());
				setNomeRazaoSocialParticipante(saidaDTO.getDsPessoa());
		}

		setTipoDuplicacao(saidaDTO.getCdIndicadorDuplicidadeRetorno());
		setInstrucao(saidaDTO.getCdInstrucaoEnvioRetorno());
		setCdPessoaJuridContrato(Integer.valueOf(String.valueOf(saidaDTO.getCdPessoaJuridContrato())));
		setCdTipoContratoNegocio(saidaDTO.getCdTipoContratoNegocio());
		setNrSeqContratoNegocio(String.valueOf(saidaDTO.getNrSeqContratoNegocio()));
		setNrSeqContratoVinc(String.valueOf(saidaDTO.getNrSeqContratoVinc()));
		setCdPessoaJuridVinc(String.valueOf(saidaDTO.getCdPessoaJuridVinc()));
		setCdTipoContratoVinc(String.valueOf(saidaDTO.getCdTipoContratoVinc()));
		setCdPessoa(saidaDTO.getCdPessoa());
		setCdTipoParticipacaoPessoa(saidaDTO.getCdTipoParticipacaoPessoa());
		setUsuarioInclusao(saidaDTO.getCdAutenSegrcInclusao());
		setUsuarioManutencao(saidaDTO.getCdAutenSegrcManutencao());
		setComplementoInclusao(saidaDTO.getNmOperFluxoInclusao().equals("0")? "" : saidaDTO.getNmOperFluxoInclusao());
		setComplementoManutencao(saidaDTO.getNmOperFluxoManutencao().equals("0")? "" : saidaDTO.getNmOperFluxoManutencao());
		setTipoCanalInclusao(saidaDTO.getCdCanalInclusao()==0? "" : saidaDTO.getCdCanalInclusao() + " - " +  saidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(saidaDTO.getCdCanalManutencao()==0? "" : saidaDTO.getCdCanalManutencao() + " - " + saidaDTO.getDsCanalManutencao()); 
		setDataHoraInclusao(saidaDTO.getHrInclusaoRegistro());
		setDataHoraManutencao(saidaDTO.getHrManutencaoRegistro());
		setCdCanalInclusao(saidaDTO.getCdCanalInclusao());
		setDsCanalInclusao(saidaDTO.getDsCanalInclusao());
		if(cdCanalInclusao == null){
			setCdCanalInclusao(null);
		}
		if(dsCanalInclusao == null){
			setCdCanalInclusao(null);
		}

		setBtoConsultarParticipantes(duplicacaoParticipanteMesmoContrato == 1 ? true : false);

		//pegar dados de origem do contrato
		ConsultarListaContratosPessoasEntradaDTO consultarListaContratosPessoasEntradaDTO = new ConsultarListaContratosPessoasEntradaDTO();
		consultarListaContratosPessoasEntradaDTO.setCdPessoaJuridicaContrato(Long.valueOf(saidaDTO.getCdPessoaJuridContrato()));
		consultarListaContratosPessoasEntradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContratoNegocio());
		consultarListaContratosPessoasEntradaDTO.setNrSeqContratoNegocio(Long.valueOf(saidaDTO.getNrSeqContratoNegocio()));
		consultarListaContratosPessoasEntradaDTO.setCdClub(this.getSaidaConsultarListaClientePessoas().getCdClub());

		try{
			ConsultarListaContratosPessoasSaidaDTO consultarListaContratosPessoasSaidaDTO = this.getFiltroIdentificaoService().detalharContratoOrigem(consultarListaContratosPessoasEntradaDTO);
			setDsPessoaJuridicaContrato(consultarListaContratosPessoasSaidaDTO.getDsPessoaJuridicaContrato());
			setDsTipoContratoNegocio(consultarListaContratosPessoasSaidaDTO.getDsTipoContratoNegocio());
			setDsSituacaoParticipacaoContrato(consultarListaContratosPessoasSaidaDTO.getDsSituacaoContratoNegocio());
			setNrSeqContratoNegocio(String.valueOf(consultarListaContratosPessoasSaidaDTO.getNrSeqContratoNegocio()));
			if (consultarListaContratosPessoasSaidaDTO.getDsTipoParticipacaoPessoa()!=null){
				setDsTipoParticipacaoPessoa(String.valueOf(consultarListaContratosPessoasSaidaDTO.getDsTipoParticipacaoPessoa()));
			}else{
				setDsTipoParticipacaoPessoa("");
			}

			//pegar dados do destino
			consultarListaContratosPessoasEntradaDTO = new ConsultarListaContratosPessoasEntradaDTO();
			consultarListaContratosPessoasEntradaDTO.setCdPessoaJuridicaContrato(Long.valueOf(saidaDTO.getCdPessoaJuridVinc()));
			consultarListaContratosPessoasEntradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContratoVinc());
			consultarListaContratosPessoasEntradaDTO.setNrSeqContratoNegocio(Long.valueOf(saidaDTO.getNrSeqContratoVinc()));
			consultarListaContratosPessoasEntradaDTO.setCdClub(this.getSaidaConsultarListaClientePessoas().getCdClub());

			consultarListaContratosPessoasSaidaDTO = this.getFiltroIdentificaoService().detalharContratoOrigem(consultarListaContratosPessoasEntradaDTO);

			setDsPessoaJuridicaVinc(consultarListaContratosPessoasSaidaDTO.getDsPessoaJuridicaContrato());
			setDsTipoContratoVinc(consultarListaContratosPessoasSaidaDTO.getDsTipoContratoNegocio());
			setDsSituacaoParticipacaoVinc(consultarListaContratosPessoasSaidaDTO.getDsSituacaoContratoNegocio());
			setNrSeqContratoVinc(String.valueOf(consultarListaContratosPessoasSaidaDTO.getNrSeqContratoNegocio()));			
		}catch(Exception e){
			setDsPessoaJuridicaContrato("");
			setDsTipoContratoNegocio("");
			setDsSituacaoParticipacaoContrato("");
			setNrSeqContratoNegocio("");
			setDsTipoParticipacaoPessoa("");
			setDsPessoaJuridicaVinc("");
			setDsTipoContratoVinc("");
			setDsSituacaoParticipacaoVinc("");
			setNrSeqContratoVinc("");			
			setDsTipoParticipacaoPessoaVinc("");
		}
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_DUPLICACAO_ARQUIVO_RETORNO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "AVANCAR_EXCLUIR";

	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoLista(null);

		return CON_DUPLICACAO_ARQUIVO_RETORNO;
	}
	
	/**
	 * Voltar cliente.
	 *
	 * @return the string
	 */
	public String voltarCliente(){
		setItemClienteSelecionado(null);
		
		return "VOLTAR";
	}
	
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR";
	}	
	
	/**
	 * Voltar participantes.
	 *
	 * @return the string
	 */
	public String voltarParticipantes(){
		return getRetornoParticipantes();
	}	
	
	/**
	 * Voltar cliente contrato.
	 *
	 * @return the string
	 */
	public String voltarClienteContrato(){
		return "INCLUIR";
	}

	/**
	 * Selecionar.
	 *
	 * @return the string
	 */
	public String selecionar(){
		ListarParticipantesSaidaDTO itemSelecionado = getListaGrid().get(getItemSelecionadoGrid());
		setParticipanteCpfCnpj(itemSelecionado.getCnpjOuCpfFormatado());
		setParticipanteNomeRazaoSocial(itemSelecionado.getNomeRazao());
		setParticipanteNivelParticipacao(itemSelecionado.getDsTipoParticipacao());
		setParticipanteSituacaoParticipacao(itemSelecionado.getDsSituacaoParticipacao());
		setCpfCnpjParticipante(itemSelecionado.getCnpjOuCpfFormatado());
		setNomeRazaoSocialParticipante(itemSelecionado.getNomeRazao());
		setCdPessoa(itemSelecionado.getCdPessoa());
		setCdTipoParticipacaoPessoa(itemSelecionado.getCdTipoParticipacao());
		
		setNrSeqContratoVinc(getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio().toString());
		setDsContrato(getSaidaConsultarListaContratosPessoas().getDsContrato());
		setDsSituacaoContratoNegocio(getSaidaConsultarListaContratosPessoas().getDsSituacaoContratoNegocio());
		setDsTipoContratoNegocioClienteContrato(getSaidaConsultarListaContratosPessoas().getDsTipoContratoNegocio());
		setNrSeqContratoNegocioClienteContrato(getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		
//		setDsPessoaJuridicaClienteContrato(itemSelecionado.getDsPessoaJuridica());
//		setDsTipoContratoNegocioClienteContrato(itemSelecionado.getDsTipoContrato());
//		setNrSeqContratoNegocioClienteContrato(itemSelecionado.getNrSequenciaContrato());
//		setDsContrato(itemSelecionado.getDsContrato());
//		setDsSituacaoContratoNegocio(itemSelecionado.getDsSituacaoContrato());

		return getRetornoParticipantes();
	}

	/**
	 * Limpar participantes.
	 */
	public void limparParticipantes() {
		setParticipanteCpfCnpj(null);
		setParticipanteNomeRazaoSocial(null);
		setParticipanteNivelParticipacao(null);
		setParticipanteSituacaoParticipacao(null);
		setCdPessoa(null);
		setCdTipoParticipacaoPessoa(null);
	}
	
	/**
	 * Selecionar cliente contrato.
	 *
	 * @return the string
	 */
	public String selecionarClienteContrato(){
		ListarContratosPgitSaidaDTO itemSelecionado = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		setDsPessoaJuridicaClienteContrato(itemSelecionado.getDsPessoaJuridica());
		setDsTipoContratoNegocioClienteContrato(itemSelecionado.getDsTipoContrato());
		setNrSeqContratoNegocioClienteContrato(itemSelecionado.getNrSequenciaContrato());
		setDsContrato(itemSelecionado.getDsContrato());
		setDsSituacaoContratoNegocio(itemSelecionado.getDsSituacaoContrato());

		ListarParticipantesSaidaDTO participanteSelecionado = manterContratoParticipantesBean.getListaGrid().get(manterContratoParticipantesBean.getItemSelecionadoGrid());
		setParticipanteCpfCnpj(participanteSelecionado.getCnpjOuCpfFormatado());
		setParticipanteNomeRazaoSocial(participanteSelecionado.getNomeRazao());
		setParticipanteNivelParticipacao(participanteSelecionado.getDsTipoParticipacao());
		setParticipanteSituacaoParticipacao(participanteSelecionado.getDsSituacaoParticipacao());
		setCdPessoa(participanteSelecionado.getCdPessoa());
		setCdTipoParticipacaoPessoa(participanteSelecionado.getCdTipoParticipacao());

		setHabilitaAvancar(true);

		return identificacaoClienteContratoBean.getPaginaRetorno();
	}
	
	/**
	 * Pesquisar participantes.
	 *
	 * @return the string
	 */
	public String pesquisarParticipantes(){
		return "";
	}
	
	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar(){
		return "VOLTAR_ALTERAR";
	}	
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		limparArgumentosPesquisa();
		return CON_DUPLICACAO_ARQUIVO_RETORNO;
	}
	
	/**
	 * Carrega grid confirmar.
	 *
	 * @return the string
	 */
	public String carregaGridConfirmar() {
		carregarGrid();

		return "conDuplicacaoArquivoRetorno";
	}
	
	/**
	 * Carregar grid.
	 *
	 * @return the string
	 */
	public String carregarGrid() {
		
		try{
			DuplicacaoArquivoRetornoEntradaDTO entradaDTO = new DuplicacaoArquivoRetornoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entradaDTO.setNrSeqContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entradaDTO.setCdTipoLayoutArquivo(getFiltroTipoLayout());
			entradaDTO.setCdTipoArquivoRetorno(getFiltroTipoArquivoRetorno());		
			entradaDTO.setCdClub(this.getSaidaConsultarListaClientePessoas().getCdClub());
			
			setListaGridPesquisa(getDuplicacaoArquivoRetornoImpl().listarDuplicacaoArqRetorno(entradaDTO));
			setBtoAcionado(true);
			this.listaControleRadio =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGridPesquisa().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			
			this.setHabilitaFiltroDeContrato(true);
			setHabilitaFiltroDeCliente(true);
			
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);			
			setHabilitaFiltroDeCliente(false);
			this.setHabilitaFiltroDeContrato(false);
			setListaGridPesquisa(null);			
			return "";
		}
		return "";
	}
	
	/**
	 * Carregar grid excluir.
	 *
	 * @return the string
	 */
	public String carregarGridExcluir() {
		carregarGrid();
		return CON_DUPLICACAO_ARQUIVO_RETORNO;
	}
	
	/**
	 * Is mostra botoes.
	 *
	 * @return true, if is mostra botoes
	 */
	public boolean isMostraBotoes() {
		return mostraBotoes;
	}
	
	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}
	
	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}
	
	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}
	
	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<DuplicacaoArquivoRetornoSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}
	
	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<DuplicacaoArquivoRetornoSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}
	
	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: participantesDesc.
	 *
	 * @return participantesDesc
	 */
	public String getParticipantesDesc() {
		return participantesDesc;
	}

	/**
	 * Set: participantesDesc.
	 *
	 * @param participantesDesc the participantes desc
	 */
	public void setParticipantesDesc(String participantesDesc) {
		this.participantesDesc = participantesDesc;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: concatTipoCanal.
	 *
	 * @return concatTipoCanal
	 */
	public String getConcatTipoCanal() {
		return String.valueOf(getTipoCanalInclusao() + " - " + getTipoCanalManutencao());
	}

	/**
	 * Pesquisar contratos.
	 *
	 * @return the string
	 */
	public String pesquisarContratos(){
		String retorno = "";
		consultarContratos();
		if(getListaConsultarListaContratosPessoas() != null){
	   		if(getListaConsultarListaContratosPessoas().size() != 0 && getListaConsultarListaContratosPessoas().size() != 1){
	   			retorno =  getPaginaContrato();
	   		}else{
				listarTipoLayout();
	   		}
	   		
	   		return retorno;
			}
		return retorno;
		}
	
	 /**
 	 * Selecionar contrato dup.
 	 *
 	 * @return the string
 	 */
 	public String selecionarContratoDup(){
	    	String retorno = "";
	   		selecionarContrato();
	   		retorno =  getPaginaRetorno();
	   		listarTipoLayoutContratoSelecionado();
	   		return retorno;
	    }
	
	
	/**
	 * Listar tipo layout.
	 */
	private void listarTipoLayout() {
		listaLayoutDTO = new ConsultarListaLayoutArquivoContratoEntradaDTO();
		try {
			
			// atributos de entrada.
			listaLayoutDTO.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			listaLayoutDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			listaLayoutDTO.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			listaLayoutDTO.setCdProduto(0);
			
			setListaLayout(getManterContratoImpl().consultarListaLayoutArquivoContrato(listaLayoutDTO));
			
			this.listaTipoLayout =  new ArrayList<SelectItem>();
			
			for(ConsultarListaLayoutArquivoContratoSaidaDTO combo : listaLayout){
				listaTipoLayoutHash.put(combo.getCdTipoLayout(), combo.getDsTipoLayout());
				listaTipoLayout.add(new SelectItem(combo.getCdTipoLayout(),combo.getDsTipoLayout()));
			}
			
			
		} catch (PdcAdapterFunctionalException p) {
			if(!"PGIT0003".equals(StringUtils.right(p.getCode(), 8))){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			}
			setListaLayout(new ArrayList<ConsultarListaLayoutArquivoContratoSaidaDTO>());

		}
		
	}
	
	/**
	 * Listar tipo layout contrato selecionado.
	 */
	private void listarTipoLayoutContratoSelecionado() {
		listaLayoutDTO = new ConsultarListaLayoutArquivoContratoEntradaDTO();
		try {
			
			// atributos de entrada.
			listaLayoutDTO.setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoas().get(getItemContratoSelecionado()).getCdPessoaJuridicaContrato());
			listaLayoutDTO.setCdTipoContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemContratoSelecionado()).getCdTipoContratoNegocio());
			listaLayoutDTO.setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemContratoSelecionado()).getNrSeqContratoNegocio());
			listaLayoutDTO.setCdProduto(0);
			
			setListaLayout(getManterContratoImpl().consultarListaLayoutArquivoContrato(listaLayoutDTO));
			
			this.listaTipoLayout =  new ArrayList<SelectItem>();
			
			for(ConsultarListaLayoutArquivoContratoSaidaDTO combo : listaLayout){
				listaTipoLayoutHash.put(combo.getCdTipoLayout(), combo.getDsTipoLayout());
				listaTipoLayout.add(new SelectItem(combo.getCdTipoLayout(),combo.getDsTipoLayout()));
			}
			
			
		} catch (PdcAdapterFunctionalException p) {
			if(!"PGIT0003".equals(StringUtils.right(p.getCode(), 8))){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			}
			setListaLayout(new ArrayList<ConsultarListaLayoutArquivoContratoSaidaDTO>());
			
		}
		
	}

	/**
	 * Get: listaTipoLayout.
	 *
	 * @return listaTipoLayout
	 */
	public List<SelectItem> getListaTipoLayout() {
		return listaTipoLayout;
	}

	/**
	 * Set: listaTipoLayout.
	 *
	 * @param listaTipoLayout the lista tipo layout
	 */
	public void setListaTipoLayout(List<SelectItem> listaTipoLayout) {
		this.listaTipoLayout = listaTipoLayout;
	}

	/**
	 * Get: listaTipoLayoutHash.
	 *
	 * @return listaTipoLayoutHash
	 */
	public Map<Integer, String> getListaTipoLayoutHash() {
		return listaTipoLayoutHash;
	}


	/**
	 * Set lista tipo layout hash.
	 *
	 * @param listaTipoLayoutHash the lista tipo layout hash
	 */
	public void setListaTipoLayoutHash(Map<Integer, String> listaTipoLayoutHash) {
		this.listaTipoLayoutHash = listaTipoLayoutHash;
	}


	/**
	 * Get: filtroTipoLayout.
	 *
	 * @return filtroTipoLayout
	 */
	public Integer getFiltroTipoLayout() {
		return filtroTipoLayout;
	}

	/**
	 * Set: filtroTipoLayout.
	 *
	 * @param filtroTipoLayout the filtro tipo layout
	 */
	public void setFiltroTipoLayout(Integer filtroTipoLayout) {
		this.filtroTipoLayout = filtroTipoLayout;
	}

	/**
	 * Listar tipo arquivo retorno filtro.
	 */
	public void listarTipoArquivoRetornoFiltro() {
		
		try{
			setFiltroTipoArquivoRetorno(0);
			getListaTipoArquivoRetornoFiltro().clear();
			if (getFiltroTipoLayout() != null && getFiltroTipoLayout() != 0){ 
				listaTipoArquivoRetornoFiltro = new ArrayList<SelectItem>();
					
				ConsultarListaTipoRetornoLayoutEntradaDTO listaDTO = new ConsultarListaTipoRetornoLayoutEntradaDTO();
				
				// atributos de entrada.
				listaDTO.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
				listaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
				listaDTO.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
				listaDTO.setCdTipoLayoutArquivo(getFiltroTipoLayout());
				listaDTO.setCdTipoTela(2);
				
				setListaRetorno(getManterContratoImpl().consultarListaTipoRetornoLayout(listaDTO));
				
				for (ConsultarListaTipoRetornoLayoutSaidaDTO saida : listaRetorno){
					listaTipoArquivoRetornoFiltroHash.put(saida.getCdTipoArquivoRetorno(), saida.getDsTipoArquivoRetorno());
					listaTipoArquivoRetornoFiltro.add(new SelectItem(saida.getCdTipoArquivoRetorno(), saida.getDsTipoArquivoRetorno()));
				}
			}else{
				listaTipoArquivoRetornoFiltro = new ArrayList<SelectItem>();
			}
		}catch(PdcAdapterFunctionalException p){
			listaTipoArquivoRetornoFiltro = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Listar tipo arquivo retorno.
	 */
	public void listarTipoArquivoRetorno() {
		
		try{
			setFiltroTipoArquivoRetorno(0);
			getListaTipoArquivoRetornoFiltro().clear();
			if (getCdTipoLayout() != null && getCdTipoLayout() != 0){ 
				listaTipoArquivoRetorno = new ArrayList<SelectItem>();
					
				ConsultarListaTipoRetornoLayoutEntradaDTO listaDTO = new ConsultarListaTipoRetornoLayoutEntradaDTO();
				
				// atributos de entrada.
				listaDTO.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
				listaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
				listaDTO.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
				listaDTO.setCdTipoLayoutArquivo(getCdTipoLayout());
				listaDTO.setCdTipoTela(2);
				
				setListaRetorno(getManterContratoImpl().consultarListaTipoRetornoLayout(listaDTO));
				
				for (ConsultarListaTipoRetornoLayoutSaidaDTO saida : listaRetorno){
					listaTipoArquivoRetornoHash.put(saida.getCdTipoArquivoRetorno(), saida.getDsTipoArquivoRetorno());
					listaTipoArquivoRetorno.add(new SelectItem(saida.getCdTipoArquivoRetorno(), saida.getDsTipoArquivoRetorno()));
				}
			}else{
				listaTipoArquivoRetorno = new ArrayList<SelectItem>();
			}
		}catch(PdcAdapterFunctionalException p){
			listaTipoArquivoRetorno = new ArrayList<SelectItem>();
		}
		
	}

	/**
	 * Get: duplicacaoArquivoRetornoImpl.
	 *
	 * @return duplicacaoArquivoRetornoImpl
	 */
	public IDuplArqRetornoService getDuplicacaoArquivoRetornoImpl() {
		return duplicacaoArquivoRetornoImpl;
	}

	/**
	 * Set: duplicacaoArquivoRetornoImpl.
	 *
	 * @param duplicacaoArquivoRetornoImpl the duplicacao arquivo retorno impl
	 */
	public void setDuplicacaoArquivoRetornoImpl(
			IDuplArqRetornoService duplicacaoArquivoRetornoImpl) {
		this.duplicacaoArquivoRetornoImpl = duplicacaoArquivoRetornoImpl;
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		DetalharDuplicacaoArqRetornoEntradaDTO entradaDTO = new DetalharDuplicacaoArqRetornoEntradaDTO();
		DetalharDuplicacaoArqRetornoSaidaDTO saidaDTO = new DetalharDuplicacaoArqRetornoSaidaDTO();
		DuplicacaoArquivoRetornoSaidaDTO itemSelecionado = getListaGridPesquisa().get(getItemSelecionadoLista());
		
		//dados destino 
		entradaDTO.setCdPessoaJuridVinc(itemSelecionado.getCdPessoaJuridVinc());
		entradaDTO.setCdTipoContratoVinc(itemSelecionado.getCdTipoContratoVinc());
		entradaDTO.setNrSeqContratoVinc(itemSelecionado.getNrSeqContratoVinc());
		//dados origem
		entradaDTO.setCdPessoaJuridContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaDTO.setNrSeqContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		
		entradaDTO.setCdTipoLayoutArquivo(itemSelecionado.getCdTipoLayoutArquivo());
		entradaDTO.setCdTipoArquivoRetorno(itemSelecionado.getCdTipoArquivoRetorno());
		entradaDTO.setCdTipoParticipacaoPessoa(0);
		entradaDTO.setCdPessoa(0l);
		entradaDTO.setCdInstrucaoEnvioRetorno(itemSelecionado.getCdInstrucaoEnvioRetorno());
		entradaDTO.setCdIndicadorDuplicidadeRetorno(itemSelecionado.getCdIndicadorDuplicidadeRetorno());
		entradaDTO.setCdProdutoServicoOper(0); 
		
		saidaDTO = getDuplicacaoArquivoRetornoImpl().detalharDuplicacaoArqRetorno(entradaDTO);
		
		setDsContratoSaida(this.getSaidaConsultarListaContratosPessoas().getDsContrato());
		setCdTipoArquivo(saidaDTO.getCdTipoArquivoRetorno());
		setDsTipoArquivo(saidaDTO.getDsTipoArquivoRetorno());
		setTipoArquivoFormatado(PgitUtil.concatenarCampos(getCdTipoArquivo(), getDsTipoArquivo()));
		setCdTipoLayout(saidaDTO.getCdTipoLayoutArquivo());
		setDsTipoLayout(saidaDTO.getDsTipoLayoutArquivo());
		setTipoLayoutFormatado(PgitUtil.concatenarCampos(getCdTipoLayout(), getDsTipoLayout()));

		if (saidaDTO.getNrSeqContratoNegocio().equals(saidaDTO.getNrSeqContratoVinc())) {
		    setDuplicacaoParticipanteMesmoContrato(1);
			setParticipantesDesc(saidaDTO.getCdPessoa() + "-" + saidaDTO.getDsPessoa());
		    setCdParticipantes(saidaDTO.getCdPessoa());		    
		} else {
			// SE TIPO DE PARTICIPA��O FOR IGUAL A 0 ENT�O USU�RIO N�O SELECIONOU PARTICIPANTE E RADIO DEVE SER "N�O"
		    setDuplicacaoParticipanteMesmoContrato(2);
		    setParticipantesDesc("");
		    setCdParticipantes(0l);
		}

		setCpfCnpjParticipante(saidaDTO.getCpfCnpjParticipante());
		setNomeRazaoSocialParticipante(saidaDTO.getDsPessoa());
		setCpfCnpjParticipanteAux(saidaDTO.getCpfCnpjParticipante());
		setNomeRazaoSocialParticipanteAux(saidaDTO.getDsPessoa());

		setTipoDuplicacao(saidaDTO.getCdIndicadorDuplicidadeRetorno());
		setInstrucao(saidaDTO.getCdInstrucaoEnvioRetorno());
		setCdPessoaJuridContrato(Integer.valueOf(String.valueOf(saidaDTO.getCdPessoaJuridContrato())));
		setCdTipoContratoNegocio(saidaDTO.getCdTipoContratoNegocio());
		setNrSeqContratoNegocio(String.valueOf(saidaDTO.getNrSeqContratoNegocio()));
		setNrSeqContratoVinc(String.valueOf(saidaDTO.getNrSeqContratoVinc()));
		setCdPessoaJuridVinc(String.valueOf(saidaDTO.getCdPessoaJuridVinc()));
		setCdTipoContratoVinc(String.valueOf(saidaDTO.getCdTipoContratoVinc()));
		setCdPessoa(saidaDTO.getCdPessoa());
		setCdTipoParticipacaoPessoa(saidaDTO.getCdTipoParticipacaoPessoa());
		setUsuarioInclusao(saidaDTO.getCdAutenSegrcInclusao());
		setUsuarioManutencao(saidaDTO.getCdAutenSegrcManutencao());
		setComplementoInclusao(saidaDTO.getNmOperFluxoInclusao().equals("0")? "" : saidaDTO.getNmOperFluxoInclusao());
		setComplementoManutencao(saidaDTO.getNmOperFluxoManutencao().equals("0")? "" : saidaDTO.getNmOperFluxoManutencao());
		setTipoCanalInclusao(saidaDTO.getCdCanalInclusao()==0? "" : saidaDTO.getCdCanalInclusao() + " - " +  saidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(saidaDTO.getCdCanalManutencao()==0? "" : saidaDTO.getCdCanalManutencao() + " - " + saidaDTO.getDsCanalManutencao()); 
		setDataHoraInclusao(saidaDTO.getHrInclusaoRegistro());
		setDataHoraManutencao(saidaDTO.getHrManutencaoRegistro());
		setCdCanalInclusao(saidaDTO.getCdCanalInclusao());
		setDsCanalInclusao(saidaDTO.getDsCanalInclusao());
		
		if (cdCanalInclusao == null) {
			setCdCanalInclusao(null);
		}
		if (dsCanalInclusao == null) {
			setCdCanalInclusao(null);
		}
	
		setBtoConsultarParticipantes(duplicacaoParticipanteMesmoContrato == 1 ? true : false);
		
		//pegar dados de origem do contrato
		ConsultarListaContratosPessoasEntradaDTO consultarListaContratosPessoasEntradaDTO = new ConsultarListaContratosPessoasEntradaDTO();
		consultarListaContratosPessoasEntradaDTO.setCdPessoaJuridicaContrato(Long.valueOf(saidaDTO.getCdPessoaJuridContrato()));
		consultarListaContratosPessoasEntradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContratoNegocio());
		consultarListaContratosPessoasEntradaDTO.setNrSeqContratoNegocio(Long.valueOf(saidaDTO.getNrSeqContratoNegocio()));
		consultarListaContratosPessoasEntradaDTO.setCdClub(this.getSaidaConsultarListaClientePessoas().getCdClub());
		
		try {
			ConsultarListaContratosPessoasSaidaDTO consultarListaContratosPessoasSaidaDTO = this.getFiltroIdentificaoService().detalharContratoOrigem(consultarListaContratosPessoasEntradaDTO);
			setDsPessoaJuridicaContrato(consultarListaContratosPessoasSaidaDTO.getDsPessoaJuridicaContrato());
			setDsTipoContratoNegocio(consultarListaContratosPessoasSaidaDTO.getDsTipoContratoNegocio());
			setDsSituacaoParticipacaoContrato(consultarListaContratosPessoasSaidaDTO.getDsSituacaoContratoNegocio());
			setNrSeqContratoNegocio(String.valueOf(consultarListaContratosPessoasSaidaDTO.getNrSeqContratoNegocio()));

			if (consultarListaContratosPessoasSaidaDTO.getDsTipoParticipacaoPessoa() != null) {
				setDsTipoParticipacaoPessoa(String.valueOf(consultarListaContratosPessoasSaidaDTO.getDsTipoParticipacaoPessoa()));
			} else {
				setDsTipoParticipacaoPessoa("");
			}

			//pegar dados do destino
			consultarListaContratosPessoasEntradaDTO = new ConsultarListaContratosPessoasEntradaDTO();
			consultarListaContratosPessoasEntradaDTO.setCdPessoaJuridicaContrato(Long.valueOf(saidaDTO.getCdPessoaJuridVinc()));
			consultarListaContratosPessoasEntradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContratoVinc());
			consultarListaContratosPessoasEntradaDTO.setNrSeqContratoNegocio(Long.valueOf(saidaDTO.getNrSeqContratoVinc()));
			consultarListaContratosPessoasEntradaDTO.setCdClub(this.getSaidaConsultarListaClientePessoas().getCdClub());
			
			consultarListaContratosPessoasSaidaDTO = this.getFiltroIdentificaoService().detalharContratoOrigem(consultarListaContratosPessoasEntradaDTO);
			
			setDsPessoaJuridicaVinc(consultarListaContratosPessoasSaidaDTO.getDsPessoaJuridicaContrato());
			setDsTipoContratoVinc(consultarListaContratosPessoasSaidaDTO.getDsTipoContratoNegocio());
			setDsSituacaoParticipacaoVinc(consultarListaContratosPessoasSaidaDTO.getDsSituacaoContratoNegocio());
			setNrSeqContratoVinc(String.valueOf(consultarListaContratosPessoasSaidaDTO.getNrSeqContratoNegocio()));		
			
		} catch (Exception e) {
			setDsPessoaJuridicaContrato("");
			setDsTipoContratoNegocio("");
			setDsSituacaoParticipacaoContrato("");
			setNrSeqContratoNegocio("");
			setDsTipoParticipacaoPessoa("");
			setDsPessoaJuridicaVinc("");
			setDsTipoContratoVinc("");
			setDsSituacaoParticipacaoVinc("");
			setNrSeqContratoVinc("");
			setDsTipoParticipacaoPessoaVinc("");
		}
	}

	/**
	 * Get: cdTipoArquivo.
	 *
	 * @return cdTipoArquivo
	 */
	public Integer getCdTipoArquivo() {
		return cdTipoArquivo;
	}

	/**
	 * Set: cdTipoArquivo.
	 *
	 * @param cdTipoArquivo the cd tipo arquivo
	 */
	public void setCdTipoArquivo(Integer cdTipoArquivo) {
		this.cdTipoArquivo = cdTipoArquivo;
	}

	/**
	 * Get: dsTipoArquivo.
	 *
	 * @return dsTipoArquivo
	 */
	public String getDsTipoArquivo() {
		return dsTipoArquivo;
	}

	/**
	 * Set: dsTipoArquivo.
	 *
	 * @param dsTipoArquivo the ds tipo arquivo
	 */
	public void setDsTipoArquivo(String dsTipoArquivo) {
		this.dsTipoArquivo = dsTipoArquivo;
	}

	/**
	 * Get: cdTipoLayout.
	 *
	 * @return cdTipoLayout
	 */
	public Integer getCdTipoLayout() {
		return cdTipoLayout;
	}

	/**
	 * Set: cdTipoLayout.
	 *
	 * @param cdTipoLayout the cd tipo layout
	 */
	public void setCdTipoLayout(Integer cdTipoLayout) {
		this.cdTipoLayout = cdTipoLayout;
	}

	/**
	 * Get: dsTipoLayout.
	 *
	 * @return dsTipoLayout
	 */
	public String getDsTipoLayout() {
		return dsTipoLayout;
	}

	/**
	 * Set: dsTipoLayout.
	 *
	 * @param dsTipoLayout the ds tipo layout
	 */
	public void setDsTipoLayout(String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		try {
			
			ExcluirDuplicacaoArqRetornoEntradaDTO entradaDTO = new ExcluirDuplicacaoArqRetornoEntradaDTO();
			DuplicacaoArquivoRetornoSaidaDTO itemSelecionado = getListaGridPesquisa().get(getItemSelecionadoLista());	
			
			
			entradaDTO.setCdPessoaJuridVinc(itemSelecionado.getCdPessoaJuridVinc());
			entradaDTO.setCdTipoContratoVinc(itemSelecionado.getCdTipoContratoVinc());
			entradaDTO.setNrSeqContratoVinc(itemSelecionado.getNrSeqContratoVinc());
			entradaDTO.setCdPessoaJuridContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entradaDTO.setNrSeqContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entradaDTO.setCdTipoLayoutArquivo(itemSelecionado.getCdTipoLayoutArquivo());
			entradaDTO.setCdTipoArquivoRetorno(itemSelecionado.getCdTipoArquivoRetorno());
			entradaDTO.setCdTipoParticipacaoPessoa(getCdTipoParticipacaoPessoa()); 
			entradaDTO.setCdPessoa(getCdPessoa());
			entradaDTO.setCdInstrucaoEnvioRetorno(itemSelecionado.getCdInstrucaoEnvioRetorno());
			entradaDTO.setCdIndicadorDuplicidadeRetorno(itemSelecionado.getCdIndicadorDuplicidadeRetorno());
			entradaDTO.setCdProdutoServicoOper(0);
			
			ExcluirDuplicacaoArqRetornoSaidaDTO excluirDuplicacaoArqRetornoSaidaDTO = getDuplicacaoArquivoRetornoImpl().excluirDuplicacaoArqRetorno(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirDuplicacaoArqRetornoSaidaDTO.getCodMensagem() + ") " + excluirDuplicacaoArqRetornoSaidaDTO.getMensagem(), "#{duplicacaoArquivoRetornoBean.carregarGridExcluir}", BradescoViewExceptionActionType.ACTION, false);

			//carregaGrid();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}


	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}

	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}

	/**
	 * Get: dsSituacaoParticipacaoContrato.
	 *
	 * @return dsSituacaoParticipacaoContrato
	 */
	public String getDsSituacaoParticipacaoContrato() {
		return dsSituacaoParticipacaoContrato;
	}

	/**
	 * Set: dsSituacaoParticipacaoContrato.
	 *
	 * @param dsSituacaoParticipacaoContrato the ds situacao participacao contrato
	 */
	public void setDsSituacaoParticipacaoContrato(
			String dsSituacaoParticipacaoContrato) {
		this.dsSituacaoParticipacaoContrato = dsSituacaoParticipacaoContrato;
	}

	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}

	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){
		if (getObrigatoriedade().equals("T")){
			setDsTipoLayout(tipoLayoutFormatado);
			setDsTipoArquivo(tipoArquivoFormatado);
			
			
			
			return "AVANCAR_ALTERAR_CONFIRMAR";
		}
		
		if(getDuplicacaoParticipanteMesmoContrato() == 1){
			setDsPessoaJuridicaClienteContrato(null);
    		setDsTipoContratoNegocioClienteContrato(null);
    		setNrSeqContratoNegocioClienteContrato(null);
    		setDsContrato(null);
    		setDsSituacaoContratoNegocio(null);
    		setParticipanteCpfCnpj(null);
    		setParticipanteNomeRazaoSocial(null);
    		setParticipanteNivelParticipacao(null);
    		setParticipanteSituacaoParticipacao(null);
		}
		
		return "AVANCAR_ALTERAR_CONFIRMAR";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		
		try {
			
			AlterarDuplicacaoArqRetornoEntradaDTO entradaDTO = new AlterarDuplicacaoArqRetornoEntradaDTO();

			entradaDTO.setCdPessoaJuridVinc(getCdPessoaJuridContrato().longValue());
			entradaDTO.setCdTipoContratoVinc(getCdTipoContratoNegocio());
			entradaDTO.setNrSeqContratoVinc( getDuplicacaoParticipanteMesmoContrato() == 1 ? getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio() : getNrSeqContratoNegocioClienteContrato());
			entradaDTO.setCdPessoaJuridContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entradaDTO.setNrSeqContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entradaDTO.setCdTipoLayoutArquivo(getCdTipoLayout());
			entradaDTO.setCdTipoArquivoRetorno(getCdTipoArquivo());
			entradaDTO.setCdTipoParticipacaoPessoa(getCdTipoParticipacaoPessoa());
			entradaDTO.setCdPessoa(getCdPessoa());
			entradaDTO.setCdInstrucaoEnvioRetorno(getInstrucao());
			entradaDTO.setCdIndicadorDuplicidadeRetorno(getTipoDuplicacao());
			entradaDTO.setCdPessoaJuridAnt(getCdPessoaJuridAnt());
			entradaDTO.setCdTipoContratoAnt(getCdTipoContratoAnt());
			entradaDTO.setNrSeqContratoAnt(getNrSeqContratoAnt());
			entradaDTO.setCdProdutoServicoOper(0); 
			
			AlterarDuplicacaoArqRetornoSaidaDTO alterarDuplicacaoArqRetornoSaidaDTO = getDuplicacaoArquivoRetornoImpl().alterarDuplicacaoArqRetorno(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + alterarDuplicacaoArqRetornoSaidaDTO.getCodMensagem() + ") " + alterarDuplicacaoArqRetornoSaidaDTO.getMensagem(), CON_DUPLICACAO_ARQUIVO_RETORNO, BradescoViewExceptionActionType.ACTION, false);

			carregarGrid();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";

	}
	
	/**
	 * Habilita participantes.
	 *
	 * @return the string
	 */
	public String habilitaParticipantes(){
		if(getDuplicacaoParticipanteMesmoContrato() == 2){
			setBtoConsultarParticipantes(false);
//			setDsPessoaJuridicaClienteContrato(null);
//			setDsTipoContratoNegocioClienteContrato(null);
//			setNrSeqContratoNegocioClienteContrato(null);
//			setDsContrato(null);
//			setDsSituacaoContratoNegocio(null);
//			setHabilitaAvancar(false);
			setCpfCnpjParticipante("");
			setNomeRazaoSocialParticipante("");
		}else{
			setBtoConsultarParticipantes(true);
//			setDsPessoaJuridicaClienteContrato(this.getSaidaConsultarListaContratosPessoas().getDsPessoaJuridicaContrato());
//			setDsTipoContratoNegocioClienteContrato(this.getSaidaConsultarListaContratosPessoas().getDsTipoContratoNegocio());
//			setNrSeqContratoNegocioClienteContrato(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
//			setDsContrato(this.getSaidaConsultarListaContratosPessoas().getDsContrato());
//			setDsSituacaoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getDsSituacaoContratoNegocio());
			if("S".equals(getFlagMesmoParticipante())){
				setDsPessoaJuridicaClienteContrato(null);
	    		setDsTipoContratoNegocioClienteContrato(null);
	    		setNrSeqContratoNegocioClienteContrato(null);
	    		setDsContrato(null);
	    		setDsSituacaoContratoNegocio(null);
	    		setParticipanteCpfCnpj(null);
	    		setParticipanteNomeRazaoSocial(null);
	    		setParticipanteNivelParticipacao(null);
	    		setParticipanteSituacaoParticipacao(null);
	    		setCpfCnpjParticipante("");
				setNomeRazaoSocialParticipante("");
				setCpfCnpjParticipante(getCpfCnpjParticipanteAux());
				setNomeRazaoSocialParticipante(getNomeRazaoSocialParticipanteAux());
			}
			setHabilitaAvancar(true);
		}
		
		return "";
	}
	
	
	/**
	 * Preenche dados contrato alterar.
	 */
	public void preencheDadosContratoAlterar(){
		setBtoConsultarParticipantes(true);
		if(getDuplicacaoParticipanteMesmoContrato() == 2){
    		setDsPessoaJuridicaClienteContrato(getListaGridPesquisa().get(itemSelecionadoLista).getDsPessoaJuridVinc());
    		setDsTipoContratoNegocioClienteContrato(getListaGridPesquisa().get(itemSelecionadoLista).getDsTipoContratoVinc());
    		setNrSeqContratoNegocioClienteContrato(getListaGridPesquisa().get(itemSelecionadoLista).getNrSeqContratoVinc());
    		setDsContrato(getSaidaConsultarListaContratosPessoas().getDsTipoContratoNegocio());
    		setDsSituacaoContratoNegocio(getSaidaConsultarListaContratosPessoas().getDsSituacaoContratoNegocio());
    		setParticipanteCpfCnpj(getSaidaConsultarListaContratosPessoas().getCnpjOuCpfFormatado());
    		setParticipanteNomeRazaoSocial(getSaidaConsultarListaContratosPessoas().getDsPessoaParticipante());
    		setParticipanteNivelParticipacao(getSaidaConsultarListaContratosPessoas().getDsTipoParticipacaoPessoa());
    		setParticipanteSituacaoParticipacao(getSaidaConsultarListaContratosPessoas().getDsSituacaoContratoNegocio());
    		setFlagMesmoParticipante("N");
    		setCpfCnpjParticipante("");
			setNomeRazaoSocialParticipante("");
			
		}else{
			setDsPessoaJuridicaClienteContrato(null);
    		setDsTipoContratoNegocioClienteContrato(null);
    		setNrSeqContratoNegocioClienteContrato(null);
    		setDsContrato(null);
    		setDsSituacaoContratoNegocio(null);
    		setParticipanteCpfCnpj(null);
    		setParticipanteNomeRazaoSocial(null);
    		setParticipanteNivelParticipacao(null);
    		setParticipanteSituacaoParticipacao(null);
    		
    		setFlagMesmoParticipante("S");
		}
		setHabilitaAvancar(true);
	}

	/**
	 * Pesquisar contrato incluir.
	 *
	 * @return the string
	 */
	public String pesquisarContratoIncluir(){
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteContrato2");
		identificacaoClienteContratoBean.setPaginaRetorno("incDuplicacaoArquivoRetorno");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();	
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setFlagDuplicacao("S");

//		return "PESQUISAR_CONTRATO_INCLUIR";
		return "selDuplicacaoArquivoRetorno";
	}

	/**
	 * Pesquisar contrato alterar.
	 *
	 * @return the string
	 */
	public String pesquisarContratoAlterar(){
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteContrato2");
		identificacaoClienteContratoBean.setPaginaRetorno("altDuplicacaoArquivoRetorno");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();	
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setFlagDuplicacao("S");

//		return "PESQUISAR_CONTRATO_INCLUIR";
		return "selDuplicacaoArquivoRetorno";
	}
	
	/**
	 * Participantes incluir.
	 *
	 * @return the string
	 */
	public String participantesIncluir(){
		setRetornoParticipantes("VOLTAR_INCLUIR");
		return consultarParticipantes();
	}
	
	/**
	 * Participantes alterar.
	 *
	 * @return the string
	 */
	public String participantesAlterar(){
		setRetornoParticipantes("VOLTAR_ALTERAR");
		return consultarParticipantes();
	}
	
	/**
	 * Consultar participantes.
	 *
	 * @return the string
	 */
	public String consultarParticipantes(){
		listaGrid = new ArrayList<ListarParticipantesSaidaDTO>();
		try{
			
			ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();		
			
			entradaDTO.setCodPessoaJuridica(PgitUtil.verificaLongNulo(getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato()));
			entradaDTO.setCodTipoContrato(PgitUtil.verificaIntegerNulo(getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio()));
			entradaDTO.setNrSequenciaContrato(PgitUtil.verificaLongNulo(getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio()));
			
			entradaDTO.setMaxOcorrencias(20);
			
			setListaGrid(getManterContratoImpl().listarParticipantes(entradaDTO));
			
			this.listaGridControle = new ArrayList<SelectItem>();
			for(int i = 0;i<getListaGrid().size();i++){
				listaGridControle.add(new SelectItem(i," "));
			}
			setItemSelecionadoGrid(null);			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoGrid(null);	
		}
		
		return "PARTICIPANTES";
	}

	/**
	 * Is habilita participante.
	 *
	 * @return true, if is habilita participante
	 */
	public boolean isHabilitaParticipante() {
		return habilitaParticipante;
	}


	/**
	 * Set: habilitaParticipante.
	 *
	 * @param habilitaParticipante the habilita participante
	 */
	public void setHabilitaParticipante(boolean habilitaParticipante) {
		this.habilitaParticipante = habilitaParticipante;
	}


	/**
	 * Get: listaParticipante.
	 *
	 * @return listaParticipante
	 */
	public List<SelectItem> getListaParticipante() {
		return listaParticipante;
	}


	/**
	 * Set: listaParticipante.
	 *
	 * @param listaParticipante the lista participante
	 */
	public void setListaParticipante(List<SelectItem> listaParticipante) {
		this.listaParticipante = listaParticipante;
	}
	


	/**
	 * Get: listaParticipanteHash.
	 *
	 * @return listaParticipanteHash
	 */
	public Map<Long, String> getListaParticipanteHash() {
		return listaParticipanteHash;
	}


	/**
	 * Set lista participante hash.
	 *
	 * @param listaParticipanteHash the lista participante hash
	 */
	public void setListaParticipanteHash(Map<Long, String> listaParticipanteHash) {
		this.listaParticipanteHash = listaParticipanteHash;
	}


	


	/**
	 * Get: dsPessoaJuridicaVinc.
	 *
	 * @return dsPessoaJuridicaVinc
	 */
	public String getDsPessoaJuridicaVinc() {
		return dsPessoaJuridicaVinc;
	}


	/**
	 * Set: dsPessoaJuridicaVinc.
	 *
	 * @param dsPessoaJuridicaVinc the ds pessoa juridica vinc
	 */
	public void setDsPessoaJuridicaVinc(String dsPessoaJuridicaVinc) {
		this.dsPessoaJuridicaVinc = dsPessoaJuridicaVinc;
	}


	/**
	 * Get: dsSituacaoParticipacaoVinc.
	 *
	 * @return dsSituacaoParticipacaoVinc
	 */
	public String getDsSituacaoParticipacaoVinc() {
		return dsSituacaoParticipacaoVinc;
	}


	/**
	 * Set: dsSituacaoParticipacaoVinc.
	 *
	 * @param dsSituacaoParticipacaoVinc the ds situacao participacao vinc
	 */
	public void setDsSituacaoParticipacaoVinc(String dsSituacaoParticipacaoVinc) {
		this.dsSituacaoParticipacaoVinc = dsSituacaoParticipacaoVinc;
	}


	/**
	 * Get: dsTipoContratoVinc.
	 *
	 * @return dsTipoContratoVinc
	 */
	public String getDsTipoContratoVinc() {
		return dsTipoContratoVinc;
	}


	/**
	 * Set: dsTipoContratoVinc.
	 *
	 * @param dsTipoContratoVinc the ds tipo contrato vinc
	 */
	public void setDsTipoContratoVinc(String dsTipoContratoVinc) {
		this.dsTipoContratoVinc = dsTipoContratoVinc;
	}

	/**
	 * Avancar incluir confirmar.
	 *
	 * @return the string
	 */
	public String avancarIncluirConfirmar(){
		if (getObrigatoriedade().equals("T")){
			setDsTipoLayout(PgitUtil.concatenarCampos(getCdTipoLayout(), (String)listaTipoLayoutHash.get(getCdTipoLayout())));
			setDsTipoArquivo(PgitUtil.concatenarCampos(getCdTipoArquivo(), (String)listaTipoArquivoRetornoHash.get(getCdTipoArquivo())));

			return "AVANCAR_INCLUIR_CONFIRMAR";
		}

		return "";
	}


	/**
	 * Get: dsTipoParticipacaoPessoa.
	 *
	 * @return dsTipoParticipacaoPessoa
	 */
	public String getDsTipoParticipacaoPessoa() {
		return dsTipoParticipacaoPessoa;
	}


	/**
	 * Set: dsTipoParticipacaoPessoa.
	 *
	 * @param dsTipoParticipacaoPessoa the ds tipo participacao pessoa
	 */
	public void setDsTipoParticipacaoPessoa(String dsTipoParticipacaoPessoa) {
		this.dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
	}


	/**
	 * Get: dsTipoParticipacaoPessoaVinc.
	 *
	 * @return dsTipoParticipacaoPessoaVinc
	 */
	public String getDsTipoParticipacaoPessoaVinc() {
		return dsTipoParticipacaoPessoaVinc;
	}


	/**
	 * Set: dsTipoParticipacaoPessoaVinc.
	 *
	 * @param dsTipoParticipacaoPessoaVinc the ds tipo participacao pessoa vinc
	 */
	public void setDsTipoParticipacaoPessoaVinc(String dsTipoParticipacaoPessoaVinc) {
		this.dsTipoParticipacaoPessoaVinc = dsTipoParticipacaoPessoaVinc;
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			IncluirDuplicacaoArqRetornoEntradaDTO entradaDTO = new IncluirDuplicacaoArqRetornoEntradaDTO();
			if(identificacaoClienteContratoBean.getItemSelecionadoLista() != null){
				ListarContratosPgitSaidaDTO itemSelecionado = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

				entradaDTO.setCdPessoaJuridVinc(itemSelecionado.getCdPessoaJuridica());
				entradaDTO.setCdTipoContratoVinc(itemSelecionado.getCdTipoContrato());
				entradaDTO.setNrSeqContratoVinc(itemSelecionado.getNrSequenciaContrato());
			}else{
				entradaDTO.setCdPessoaJuridVinc(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
				entradaDTO.setCdTipoContratoVinc(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
				entradaDTO.setNrSeqContratoVinc(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			}

			entradaDTO.setCdPessoaJuridContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entradaDTO.setNrSeqContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entradaDTO.setCdTipoLayoutArquivo(getCdTipoLayout());
			entradaDTO.setCdTipoArquivoRetorno(getCdTipoArquivo());
			entradaDTO.setCdTipoParticipacaoPessoa(getCdTipoParticipacaoPessoa());
			entradaDTO.setCdPessoa(getCdPessoa());
			entradaDTO.setCdInstrucaoEnvioRetorno(getInstrucao());
			entradaDTO.setCdIndicadorDuplicidadeRetorno(getTipoDuplicacao());
			entradaDTO.setCdProdutoServicoOper(0);

			IncluirDuplicacaoArqRetornoSaidaDTO incluirDuplicacaoArqRetornoSaidaDTO = getDuplicacaoArquivoRetornoImpl().incluirDuplicacaoArqRetorno(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + incluirDuplicacaoArqRetornoSaidaDTO.getCodMensagem() + ") " + incluirDuplicacaoArqRetornoSaidaDTO.getMensagem(), "#{duplicacaoArquivoRetornoBean.carregaGridConfirmar}", BradescoViewExceptionActionType.ACTION, false);

			//carregarGrid();
			setItemSelecionadoLista(null);
			setDuplicacaoParticipanteMesmoContrato(getDuplicacaoParticipanteMesmoContrato());
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";

	}
	
	/**
	 * Limpar dados cliente.
	 *
	 * @return the string
	 */
	public String limparDadosCliente(){
		limparCliente();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar dados contrato.
	 *
	 * @return the string
	 */
	public String limparDadosContrato(){
		limparContrato();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar argumentos pesquisa.
	 *
	 * @return the string
	 */
	public String limparArgumentosPesquisa(){
		setFiltroTipoArquivoRetorno(0);
		setFiltroTipoLayout(0);
		limparGrid();
		setBtoAcionado(false);
		return "";
	}
	
	/**
	 * Limpar grid.
	 */
	public void limparGrid(){
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
	}

	/**
	 * Get: cdParticipantes.
	 *
	 * @return cdParticipantes
	 */
	public Long getCdParticipantes() {
		return cdParticipantes;
	}


	/**
	 * Set: cdParticipantes.
	 *
	 * @param cdParticipantes the cd participantes
	 */
	public void setCdParticipantes(Long cdParticipantes) {
		this.cdParticipantes = cdParticipantes;
	}


	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#isHabilitaFiltroDeCliente()
	 */
	public boolean isHabilitaFiltroDeCliente() {
		return habilitaFiltroDeCliente;
	}


	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#setHabilitaFiltroDeCliente(boolean)
	 */
	public void setHabilitaFiltroDeCliente(boolean habilitaFiltroDeCliente) {
		this.habilitaFiltroDeCliente = habilitaFiltroDeCliente;
	}


	/**
	 * Limpar campos pesquisa cliente incluir.
	 */
	public void limparCamposPesquisaClienteIncluir(){
		
		this.getEntradaConsultarListaClientePessoasManutencao().setCdCpfCnpj(null);
		this.getEntradaConsultarListaClientePessoasManutencao().setCdFilialCnpj(null);
		this.getEntradaConsultarListaClientePessoasManutencao().setCdControleCnpj(null);
		this.getEntradaConsultarListaClientePessoasManutencao().setCdCpf(null);
		this.getEntradaConsultarListaClientePessoasManutencao().setCdControleCpf(null);
		this.getEntradaConsultarListaClientePessoasManutencao().setDsNomeRazaoSocial("");
		this.getEntradaConsultarListaClientePessoasManutencao().setCdBanco(null);
		this.getEntradaConsultarListaClientePessoasManutencao().setCdAgenciaBancaria(null);
		this.getEntradaConsultarListaClientePessoasManutencao().setCdContaBancaria(null);
		
		if (getItemFiltroSelecionadoManutencao().equals("3")){
			getEntradaConsultarListaClientePessoasManutencao().setCdBanco(237);
		}
	}
	
	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}


	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}


	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public Integer getCdCanalInclusao() {
		return cdCanalInclusao;
	}


	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(Integer cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}


	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}


	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}


	/**
	 * Get: dsContratoSaida.
	 *
	 * @return dsContratoSaida
	 */
	public String getDsContratoSaida() {
		return dsContratoSaida;
	}


	/**
	 * Set: dsContratoSaida.
	 *
	 * @param dsContratoSaida the ds contrato saida
	 */
	public void setDsContratoSaida(String dsContratoSaida) {
		this.dsContratoSaida = dsContratoSaida;
	}


	/**
	 * Get: listaTipoArquivoRetornoFiltro.
	 *
	 * @return listaTipoArquivoRetornoFiltro
	 */
	public List<SelectItem> getListaTipoArquivoRetornoFiltro() {
		return listaTipoArquivoRetornoFiltro;
	}


	/**
	 * Set: listaTipoArquivoRetornoFiltro.
	 *
	 * @param listaTipoArquivoRetornoFiltro the lista tipo arquivo retorno filtro
	 */
	public void setListaTipoArquivoRetornoFiltro(
			List<SelectItem> listaTipoArquivoRetornoFiltro) {
		this.listaTipoArquivoRetornoFiltro = listaTipoArquivoRetornoFiltro;
	}


	/**
	 * Get: listaTipoArquivoRetornoFiltroHash.
	 *
	 * @return listaTipoArquivoRetornoFiltroHash
	 */
	public Map<Integer, String> getListaTipoArquivoRetornoFiltroHash() {
		return listaTipoArquivoRetornoFiltroHash;
	}


	/**
	 * Set lista tipo arquivo retorno filtro hash.
	 *
	 * @param listaTipoArquivoRetornoFiltroHash the lista tipo arquivo retorno filtro hash
	 */
	public void setListaTipoArquivoRetornoFiltroHash(
			Map<Integer, String> listaTipoArquivoRetornoFiltroHash) {
		this.listaTipoArquivoRetornoFiltroHash = listaTipoArquivoRetornoFiltroHash;
	}


	/**
	 * Get: listaTipoArquivoRetorno.
	 *
	 * @return listaTipoArquivoRetorno
	 */
	public List<SelectItem> getListaTipoArquivoRetorno() {
		return listaTipoArquivoRetorno;
	}


	/**
	 * Set: listaTipoArquivoRetorno.
	 *
	 * @param listaTipoArquivoRetorno the lista tipo arquivo retorno
	 */
	public void setListaTipoArquivoRetorno(List<SelectItem> listaTipoArquivoRetorno) {
		this.listaTipoArquivoRetorno = listaTipoArquivoRetorno;
	}


	/**
	 * Get: listaTipoArquivoRetornoHash.
	 *
	 * @return listaTipoArquivoRetornoHash
	 */
	public Map<Integer, String> getListaTipoArquivoRetornoHash() {
		return listaTipoArquivoRetornoHash;
	}


	/**
	 * Set lista tipo arquivo retorno hash.
	 *
	 * @param listaTipoArquivoRetornoHash the lista tipo arquivo retorno hash
	 */
	public void setListaTipoArquivoRetornoHash(
			Map<Integer, String> listaTipoArquivoRetornoHash) {
		this.listaTipoArquivoRetornoHash = listaTipoArquivoRetornoHash;
	}


	/**
	 * Is bto consultar participantes.
	 *
	 * @return true, if is bto consultar participantes
	 */
	public boolean isBtoConsultarParticipantes() {
		return btoConsultarParticipantes;
	}


	/**
	 * Set: btoConsultarParticipantes.
	 *
	 * @param btoConsultarParticipantes the bto consultar participantes
	 */
	public void setBtoConsultarParticipantes(boolean btoConsultarParticipantes) {
		this.btoConsultarParticipantes = btoConsultarParticipantes;
	}


	/**
	 * Get: itemSelecionadoGrid.
	 *
	 * @return itemSelecionadoGrid
	 */
	public Integer getItemSelecionadoGrid() {
		return itemSelecionadoGrid;
	}


	/**
	 * Set: itemSelecionadoGrid.
	 *
	 * @param itemSelecionadoGrid the item selecionado grid
	 */
	public void setItemSelecionadoGrid(Integer itemSelecionadoGrid) {
		this.itemSelecionadoGrid = itemSelecionadoGrid;
	}


	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarParticipantesSaidaDTO> getListaGrid() {
		return listaGrid;
	}


	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarParticipantesSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}


	/**
	 * Get: listaGridControle.
	 *
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}


	/**
	 * Set: listaGridControle.
	 *
	 * @param listaGridControle the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}


	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}


	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}


	/**
	 * Get: retornoParticipantes.
	 *
	 * @return retornoParticipantes
	 */
	public String getRetornoParticipantes() {
		return retornoParticipantes;
	}


	/**
	 * Set: retornoParticipantes.
	 *
	 * @param retornoParticipantes the retorno participantes
	 */
	public void setRetornoParticipantes(String retornoParticipantes) {
		this.retornoParticipantes = retornoParticipantes;
	}


	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}


	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}


	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}


	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}


	/**
	 * Get: nomeRazaoSocialParticipante.
	 *
	 * @return nomeRazaoSocialParticipante
	 */
	public String getNomeRazaoSocialParticipante() {
		return nomeRazaoSocialParticipante;
	}


	/**
	 * Set: nomeRazaoSocialParticipante.
	 *
	 * @param nomeRazaoSocialParticipante the nome razao social participante
	 */
	public void setNomeRazaoSocialParticipante(String nomeRazaoSocialParticipante) {
		this.nomeRazaoSocialParticipante = nomeRazaoSocialParticipante;
	}


	/**
	 * Get: tipoArquivoFormatado.
	 *
	 * @return tipoArquivoFormatado
	 */
	public String getTipoArquivoFormatado() {
		return tipoArquivoFormatado;
	}


	/**
	 * Set: tipoArquivoFormatado.
	 *
	 * @param tipoArquivoFormatado the tipo arquivo formatado
	 */
	public void setTipoArquivoFormatado(String tipoArquivoFormatado) {
		this.tipoArquivoFormatado = tipoArquivoFormatado;
	}


	/**
	 * Get: tipoLayoutFormatado.
	 *
	 * @return tipoLayoutFormatado
	 */
	public String getTipoLayoutFormatado() {
		return tipoLayoutFormatado;
	}


	/**
	 * Set: tipoLayoutFormatado.
	 *
	 * @param tipoLayoutFormatado the tipo layout formatado
	 */
	public void setTipoLayoutFormatado(String tipoLayoutFormatado) {
		this.tipoLayoutFormatado = tipoLayoutFormatado;
	}


	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}


	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}


	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}


	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}


	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}


	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}


	/**
	 * Get: dsPessoaJuridicaClienteContrato.
	 *
	 * @return dsPessoaJuridicaClienteContrato
	 */
	public String getDsPessoaJuridicaClienteContrato() {
		return dsPessoaJuridicaClienteContrato;
	}


	/**
	 * Set: dsPessoaJuridicaClienteContrato.
	 *
	 * @param dsPessoaJuridicaClienteContrato the ds pessoa juridica cliente contrato
	 */
	public void setDsPessoaJuridicaClienteContrato(
			String dsPessoaJuridicaClienteContrato) {
		this.dsPessoaJuridicaClienteContrato = dsPessoaJuridicaClienteContrato;
	}


	/**
	 * Get: dsSituacaoContratoNegocio.
	 *
	 * @return dsSituacaoContratoNegocio
	 */
	public String getDsSituacaoContratoNegocio() {
		return dsSituacaoContratoNegocio;
	}


	/**
	 * Set: dsSituacaoContratoNegocio.
	 *
	 * @param dsSituacaoContratoNegocio the ds situacao contrato negocio
	 */
	public void setDsSituacaoContratoNegocio(String dsSituacaoContratoNegocio) {
		this.dsSituacaoContratoNegocio = dsSituacaoContratoNegocio;
	}


	/**
	 * Get: dsTipoContratoNegocioClienteContrato.
	 *
	 * @return dsTipoContratoNegocioClienteContrato
	 */
	public String getDsTipoContratoNegocioClienteContrato() {
		return dsTipoContratoNegocioClienteContrato;
	}


	/**
	 * Set: dsTipoContratoNegocioClienteContrato.
	 *
	 * @param dsTipoContratoNegocioClienteContrato the ds tipo contrato negocio cliente contrato
	 */
	public void setDsTipoContratoNegocioClienteContrato(
			String dsTipoContratoNegocioClienteContrato) {
		this.dsTipoContratoNegocioClienteContrato = dsTipoContratoNegocioClienteContrato;
	}


	/**
	 * Get: nrSeqContratoNegocioClienteContrato.
	 *
	 * @return nrSeqContratoNegocioClienteContrato
	 */
	public Long getNrSeqContratoNegocioClienteContrato() {
		return nrSeqContratoNegocioClienteContrato;
	}


	/**
	 * Set: nrSeqContratoNegocioClienteContrato.
	 *
	 * @param nrSeqContratoNegocioClienteContrato the nr seq contrato negocio cliente contrato
	 */
	public void setNrSeqContratoNegocioClienteContrato(
			Long nrSeqContratoNegocioClienteContrato) {
		this.nrSeqContratoNegocioClienteContrato = nrSeqContratoNegocioClienteContrato;
	}


	/**
	 * Get: listaLayout.
	 *
	 * @return listaLayout
	 */
	public List<ConsultarListaLayoutArquivoContratoSaidaDTO> getListaLayout() {
		return listaLayout;
	}


	/**
	 * Set: listaLayout.
	 *
	 * @param listaLayout the lista layout
	 */
	public void setListaLayout(List<ConsultarListaLayoutArquivoContratoSaidaDTO> listaLayout) {
		this.listaLayout = listaLayout;
	}


	/**
	 * Get: listaLayoutDTO.
	 *
	 * @return listaLayoutDTO
	 */
	public ConsultarListaLayoutArquivoContratoEntradaDTO getListaLayoutDTO() {
		return listaLayoutDTO;
	}


	/**
	 * Set: listaLayoutDTO.
	 *
	 * @param listaLayoutDTO the lista layout dto
	 */
	public void setListaLayoutDTO(ConsultarListaLayoutArquivoContratoEntradaDTO listaLayoutDTO) {
		this.listaLayoutDTO = listaLayoutDTO;
	}


	/**
	 * Get: listaRetorno.
	 *
	 * @return listaRetorno
	 */
	public List<ConsultarListaTipoRetornoLayoutSaidaDTO> getListaRetorno() {
		return listaRetorno;
	}


	/**
	 * Set: listaRetorno.
	 *
	 * @param listaRetorno the lista retorno
	 */
	public void setListaRetorno(List<ConsultarListaTipoRetornoLayoutSaidaDTO> listaRetorno) {
		this.listaRetorno = listaRetorno;
	}


	/**
	 * Get: participanteCpfCnpj.
	 *
	 * @return participanteCpfCnpj
	 */
	public String getParticipanteCpfCnpj() {
		return participanteCpfCnpj;
	}


	/**
	 * Set: participanteCpfCnpj.
	 *
	 * @param participanteCpfCnpj the participante cpf cnpj
	 */
	public void setParticipanteCpfCnpj(String participanteCpfCnpj) {
		this.participanteCpfCnpj = participanteCpfCnpj;
	}


	/**
	 * Get: participanteNomeRazaoSocial.
	 *
	 * @return participanteNomeRazaoSocial
	 */
	public String getParticipanteNomeRazaoSocial() {
		return participanteNomeRazaoSocial;
	}


	/**
	 * Set: participanteNomeRazaoSocial.
	 *
	 * @param participanteNomeRazaoSocial the participante nome razao social
	 */
	public void setParticipanteNomeRazaoSocial(String participanteNomeRazaoSocial) {
		this.participanteNomeRazaoSocial = participanteNomeRazaoSocial;
	}


	/**
	 * Get: participanteNivelParticipacao.
	 *
	 * @return participanteNivelParticipacao
	 */
	public String getParticipanteNivelParticipacao() {
		return participanteNivelParticipacao;
	}


	/**
	 * Set: participanteNivelParticipacao.
	 *
	 * @param participanteNivelParticipacao the participante nivel participacao
	 */
	public void setParticipanteNivelParticipacao(
			String participanteNivelParticipacao) {
		this.participanteNivelParticipacao = participanteNivelParticipacao;
	}


	/**
	 * Get: participanteSituacaoParticipacao.
	 *
	 * @return participanteSituacaoParticipacao
	 */
	public String getParticipanteSituacaoParticipacao() {
		return participanteSituacaoParticipacao;
	}


	/**
	 * Set: participanteSituacaoParticipacao.
	 *
	 * @param participanteSituacaoParticipacao the participante situacao participacao
	 */
	public void setParticipanteSituacaoParticipacao(
			String participanteSituacaoParticipacao) {
		this.participanteSituacaoParticipacao = participanteSituacaoParticipacao;
	}


	/**
	 * Get: bloqDesbloqParticipantesBean.
	 *
	 * @return bloqDesbloqParticipantesBean
	 */
	public BloqDesbloqParticipantesBean getBloqDesbloqParticipantesBean() {
		return bloqDesbloqParticipantesBean;
	}


	/**
	 * Set: bloqDesbloqParticipantesBean.
	 *
	 * @param bloqDesbloqParticipantesBean the bloq desbloq participantes bean
	 */
	public void setBloqDesbloqParticipantesBean(
			BloqDesbloqParticipantesBean bloqDesbloqParticipantesBean) {
		this.bloqDesbloqParticipantesBean = bloqDesbloqParticipantesBean;
	}


	/**
	 * Get: manterContratoBean.
	 *
	 * @return manterContratoBean
	 */
	public ManterContratoBean getManterContratoBean() {
		return manterContratoBean;
	}


	/**
	 * Set: manterContratoBean.
	 *
	 * @param manterContratoBean the manter contrato bean
	 */
	public void setManterContratoBean(ManterContratoBean manterContratoBean) {
		this.manterContratoBean = manterContratoBean;
	}


	/**
	 * Get: manterContratoParticipantesBean.
	 *
	 * @return manterContratoParticipantesBean
	 */
	public ParticipantesBean getManterContratoParticipantesBean() {
		return manterContratoParticipantesBean;
	}


	/**
	 * Set: manterContratoParticipantesBean.
	 *
	 * @param manterContratoParticipantesBean the manter contrato participantes bean
	 */
	public void setManterContratoParticipantesBean(ParticipantesBean manterContratoParticipantesBean) {
		this.manterContratoParticipantesBean = manterContratoParticipantesBean;
	}


	/**
	 * Get: duplicacaoParticipanteMesmoContratoAux.
	 *
	 * @return duplicacaoParticipanteMesmoContratoAux
	 */
	public Integer getDuplicacaoParticipanteMesmoContratoAux() {
		return duplicacaoParticipanteMesmoContratoAux;
	}


	/**
	 * Set: duplicacaoParticipanteMesmoContratoAux.
	 *
	 * @param duplicacaoParticipanteMesmoContratoAux the duplicacao participante mesmo contrato aux
	 */
	public void setDuplicacaoParticipanteMesmoContratoAux(
			Integer duplicacaoParticipanteMesmoContratoAux) {
		this.duplicacaoParticipanteMesmoContratoAux = duplicacaoParticipanteMesmoContratoAux;
	}


	/**
	 * Get: cpfCnpjParticipanteAux.
	 *
	 * @return cpfCnpjParticipanteAux
	 */
	public String getCpfCnpjParticipanteAux() {
		return cpfCnpjParticipanteAux;
	}


	/**
	 * Set: cpfCnpjParticipanteAux.
	 *
	 * @param cpfCnpjParticipanteAux the cpf cnpj participante aux
	 */
	public void setCpfCnpjParticipanteAux(String cpfCnpjParticipanteAux) {
		this.cpfCnpjParticipanteAux = cpfCnpjParticipanteAux;
	}


	/**
	 * Get: nomeRazaoSocialParticipanteAux.
	 *
	 * @return nomeRazaoSocialParticipanteAux
	 */
	public String getNomeRazaoSocialParticipanteAux() {
		return nomeRazaoSocialParticipanteAux;
	}


	/**
	 * Set: nomeRazaoSocialParticipanteAux.
	 *
	 * @param nomeRazaoSocialParticipanteAux the nome razao social participante aux
	 */
	public void setNomeRazaoSocialParticipanteAux(
			String nomeRazaoSocialParticipanteAux) {
		this.nomeRazaoSocialParticipanteAux = nomeRazaoSocialParticipanteAux;
	}


	/**
	 * Get: flagMesmoParticipante.
	 *
	 * @return flagMesmoParticipante
	 */
	public String getFlagMesmoParticipante() {
		return flagMesmoParticipante;
	}


	/**
	 * Set: flagMesmoParticipante.
	 *
	 * @param flagMesmoParticipante the flag mesmo participante
	 */
	public void setFlagMesmoParticipante(String flagMesmoParticipante) {
		this.flagMesmoParticipante = flagMesmoParticipante;
	}


	/**
	 * Get: cdPessoaJuridAnt.
	 *
	 * @return cdPessoaJuridAnt
	 */
	public Long getCdPessoaJuridAnt() {
		return cdPessoaJuridAnt;
	}


	/**
	 * Set: cdPessoaJuridAnt.
	 *
	 * @param cdPessoaJuridAnt the cd pessoa jurid ant
	 */
	public void setCdPessoaJuridAnt(Long cdPessoaJuridAnt) {
		this.cdPessoaJuridAnt = cdPessoaJuridAnt;
	}


	/**
	 * Get: cdTipoContratoAnt.
	 *
	 * @return cdTipoContratoAnt
	 */
	public Integer getCdTipoContratoAnt() {
		return cdTipoContratoAnt;
	}


	/**
	 * Set: cdTipoContratoAnt.
	 *
	 * @param cdTipoContratoAnt the cd tipo contrato ant
	 */
	public void setCdTipoContratoAnt(Integer cdTipoContratoAnt) {
		this.cdTipoContratoAnt = cdTipoContratoAnt;
	}


	/**
	 * Get: nrSeqContratoAnt.
	 *
	 * @return nrSeqContratoAnt
	 */
	public Long getNrSeqContratoAnt() {
		return nrSeqContratoAnt;
	}


	/**
	 * Set: nrSeqContratoAnt.
	 *
	 * @param nrSeqContratoAnt the nr seq contrato ant
	 */
	public void setNrSeqContratoAnt(Long nrSeqContratoAnt) {
		this.nrSeqContratoAnt = nrSeqContratoAnt;
	}
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: instrucao.
	 *
	 * @return instrucao
	 */
	public Integer getInstrucao() {
		return instrucao;
	}
	
	/**
	 * Set: instrucao.
	 *
	 * @param instrucao the instrucao
	 */
	public void setInstrucao(Integer instrucao) {
		this.instrucao = instrucao;
	}
	
	/**
	 * Get: duplicacaoParticipanteMesmoContrato.
	 *
	 * @return duplicacaoParticipanteMesmoContrato
	 */
	public Integer getDuplicacaoParticipanteMesmoContrato() {
		return duplicacaoParticipanteMesmoContrato;
	}
	
	/**
	 * Set: duplicacaoParticipanteMesmoContrato.
	 *
	 * @param duplicacaoParticipanteMesmoContrato the duplicacao participante mesmo contrato
	 */
	public void setDuplicacaoParticipanteMesmoContrato(
			Integer duplicacaoParticipanteMesmoContrato) {
		this.duplicacaoParticipanteMesmoContrato = duplicacaoParticipanteMesmoContrato;
	}
	
	/**
	 * Get: cdPessoaJuridContrato.
	 *
	 * @return cdPessoaJuridContrato
	 */
	public Integer getCdPessoaJuridContrato() {
		return cdPessoaJuridContrato;
	}

	/**
	 * Set: cdPessoaJuridContrato.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 */
	public void setCdPessoaJuridContrato(Integer cdPessoaJuridContrato) {
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
	}

	/**
	 * Get: cdPessoaJuridVinc.
	 *
	 * @return cdPessoaJuridVinc
	 */
	public String getCdPessoaJuridVinc() {
		return cdPessoaJuridVinc;
	}

	/**
	 * Set: cdPessoaJuridVinc.
	 *
	 * @param cdPessoaJuridVinc the cd pessoa jurid vinc
	 */
	public void setCdPessoaJuridVinc(String cdPessoaJuridVinc) {
		this.cdPessoaJuridVinc = cdPessoaJuridVinc;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: cdTipoContratoVinc.
	 *
	 * @return cdTipoContratoVinc
	 */
	public String getCdTipoContratoVinc() {
		return cdTipoContratoVinc;
	}

	/**
	 * Set: cdTipoContratoVinc.
	 *
	 * @param cdTipoContratoVinc the cd tipo contrato vinc
	 */
	public void setCdTipoContratoVinc(String cdTipoContratoVinc) {
		this.cdTipoContratoVinc = cdTipoContratoVinc;
	}

	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public String getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}

	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(String nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}

	/**
	 * Get: nrSeqContratoVinc.
	 *
	 * @return nrSeqContratoVinc
	 */
	public String getNrSeqContratoVinc() {
		return nrSeqContratoVinc;
	}

	/**
	 * Set: nrSeqContratoVinc.
	 *
	 * @param nrSeqContratoVinc the nr seq contrato vinc
	 */
	public void setNrSeqContratoVinc(String nrSeqContratoVinc) {
		this.nrSeqContratoVinc = nrSeqContratoVinc;
	}

	/**
	 * Get: tipoDuplicacao.
	 *
	 * @return tipoDuplicacao
	 */
	public Integer getTipoDuplicacao() {
		return tipoDuplicacao;
	}
	
	/**
	 * Set: tipoDuplicacao.
	 *
	 * @param tipoDuplicacao the tipo duplicacao
	 */
	public void setTipoDuplicacao(Integer tipoDuplicacao) {
		this.tipoDuplicacao = tipoDuplicacao;
	}
	
	/**
	 * Get: filtroTipoArquivoRetorno.
	 *
	 * @return filtroTipoArquivoRetorno
	 */
	public Integer getFiltroTipoArquivoRetorno() {
		return filtroTipoArquivoRetorno;
	}
	
	/**
	 * Set: filtroTipoArquivoRetorno.
	 *
	 * @param filtroTipoArquivoRetorno the filtro tipo arquivo retorno
	 */
	public void setFiltroTipoArquivoRetorno(Integer filtroTipoArquivoRetorno) {
		this.filtroTipoArquivoRetorno = filtroTipoArquivoRetorno;
	}
	
}