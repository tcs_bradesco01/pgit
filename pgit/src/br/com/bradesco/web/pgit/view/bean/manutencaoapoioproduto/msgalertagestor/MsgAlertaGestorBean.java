/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.msgalertagestor
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.msgalertagestor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultaMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultaMensagemSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.IdiomaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarRecursoCanalMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoProcessoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoProcessoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.IMsgAlertaGestorService;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.AlterarMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.AlterarMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.DetalharMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.DetalharMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ExcluirMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ExcluirMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.IncluirMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.IncluirMsgAlertaGestorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ListarMsgAlertaGestorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgalertagestor.bean.ListarMsgAlertaGestorSaidaDTO;

/**
 * Nome: MsgAlertaGestorBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MsgAlertaGestorBean {

    /** Atributo CON_MSG_ALERTA_GESTOR. */
    private static final String CON_MSG_ALERTA_GESTOR = "conMsgAlertaGestor";
    
    /** Atributo comboService. */
    private IComboService comboService;
    
    /** Atributo listarFuncBradescoService. */
    private IListarFuncBradescoService listarFuncBradescoService;
    
    /** Atributo msgAlertaGestorService. */
    private IMsgAlertaGestorService msgAlertaGestorService;
    
    /** Atributo hiddenConfirma. */
    private boolean hiddenConfirma;
    
    //Vari�veis consulta
    /** Atributo inicialCentroCustoFiltro. */
    private String inicialCentroCustoFiltro;

    /** Atributo centroCustoFiltro. */
    private String centroCustoFiltro;

    /** Atributo obrigatoriedadeFuncionario. */
    private String obrigatoriedadeFuncionario;

    /** Atributo listaCentroCustoFiltro. */
    private List<SelectItem> listaCentroCustoFiltro = new ArrayList<SelectItem>();

    /** Atributo listaCentroCustoHash. */
    private Map<String, String> listaCentroCustoHash = new HashMap<String, String>();

    /** Atributo funcionarioDTO. */
    private ListarFuncionarioSaidaDTO funcionarioDTO = new ListarFuncionarioSaidaDTO();

    /** Atributo codigoFuncionarioSelecionado. */
    private String codigoFuncionarioSelecionado;

    /** Atributo cdProcSist. */
    private int cdProcSist;

    /** Atributo nrSeqMensagemAlerta. */
    private int nrSeqMensagemAlerta;

    /** Atributo listaPrefixoFiltro. */
    private List<SelectItem> listaPrefixoFiltro = new ArrayList<SelectItem>();

    /** Atributo recursoFiltro. */
    private Integer recursoFiltro;

    /** Atributo listaRecursoFiltro. */
    private List<SelectItem> listaRecursoFiltro = new ArrayList<SelectItem>();

    /** Atributo listaRecursoHash. */
    private Map<Integer, String> listaRecursoHash = new HashMap<Integer, String>();

    /** Atributo idiomaFiltro. */
    private Integer idiomaFiltro;

    /** Atributo listaIdiomaFiltro. */
    private List<SelectItem> listaIdiomaFiltro = new ArrayList<SelectItem>();

    /** Atributo listaIdiomaHash. */
    private Map<Integer, String> listaIdiomaHash = new HashMap<Integer, String>();

    /** Atributo obrigatoriedadeConsultar. */
    private String obrigatoriedadeConsultar;

    /** Atributo tipoProcessoFiltro. */
    private Integer tipoProcessoFiltro;

    /** Atributo listaTipoProcessoFiltro. */
    private List<SelectItem> listaTipoProcessoFiltro = new ArrayList<SelectItem>();

    /** Atributo tipoMensagemFiltro. */
    private String tipoMensagemFiltro;

    /** Atributo listaGridPesquisa. */
    private List<ListarMsgAlertaGestorSaidaDTO> listaGridPesquisa;

    /** Atributo itemSelecionadoLista. */
    private Integer itemSelecionadoLista;

    /** Atributo listaControleRadio. */
    private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
    
    /** Atributo codFuncionarioFiltro. */
    private String codFuncionarioFiltro;

    /** Atributo listaTipoProcessoHash. */
    private Map<Integer, String> listaTipoProcessoHash = new HashMap<Integer, String>();

    /** Atributo listaTipoMensagemHash. */
    private Map<Integer, String> listaTipoMensagemHash = new HashMap<Integer, String>();
    

    //Incluir, alterar, detalhar
    /** Atributo obrigatoriedade. */
    private String obrigatoriedade;

    /** Atributo codigoFuncionarioDesc. */
    private String codigoFuncionarioDesc;

    /** Atributo nomeFuncionarioDesc. */
    private String nomeFuncionarioDesc;

    /** Atributo centroCusto. */
    private String centroCusto;

    /** Atributo centroCustoCombo. */
    private String centroCustoCombo;

    /** Atributo listaCentroCustoCombo. */
    private List<SelectItem> listaCentroCustoCombo = new ArrayList<SelectItem>();

    /** Atributo centroCustoDesc. */
    private String centroCustoDesc;

    /** Atributo tipoProcesso. */
    private Integer tipoProcesso;

    /** Atributo listaTipoProcesso. */
    private List<SelectItem> listaTipoProcesso = new ArrayList<SelectItem>();

    /** Atributo tipoProcessoDesc. */
    private String tipoProcessoDesc;

    /** Atributo prefixo. */
    private String prefixo;

    /** Atributo listaPrefixo. */
    private List<SelectItem> listaPrefixo = new ArrayList<SelectItem>();

    /** Atributo prefixoDesc. */
    private String prefixoDesc;

    /** Atributo recurso. */
    private Integer recurso;

    /** Atributo listaRecurso. */
    private List<SelectItem> listaRecurso = new ArrayList<SelectItem>();

    /** Atributo recursoDesc. */
    private String recursoDesc;

    /** Atributo idioma. */
    private Integer idioma;

    /** Atributo listaIdioma. */
    private List<SelectItem> listaIdioma = new ArrayList<SelectItem>();

    /** Atributo idiomaDesc. */
    private String idiomaDesc;

    /** Atributo tipoMensagem. */
    private String tipoMensagem;

    /** Atributo tipoMensagemDesc. */
    private String tipoMensagemDesc;

    /** Atributo listaTipoMensagem. */
    private List<SelectItem> listaTipoMensagem = new ArrayList<SelectItem>();

    /** Atributo meioTrasmissao. */
    private Integer meioTrasmissao;

    /** Atributo funcAtualDesc. */
    private String funcAtualDesc;

    /** Atributo codFuncionario. */
    private String codFuncionario;

    /** Atributo funcionarioDesc. */
    private String funcionarioDesc;

    /** Atributo codJuncao. */
    private String codJuncao;

    /** Atributo codSecao. */
    private String codSecao;

    /** Atributo listaControleFuncionarios. */
    private List<SelectItem> listaControleFuncionarios;

    /** Atributo btoAcionado. */
    private Boolean btoAcionado;
    
    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    /**
     * Limpar campos pesquisa.
     */
    public void limparCamposPesquisa() {
	setCodFuncionario("");
	setCodJuncao("");
	setCodSecao("");

    }

    /**
     * Carrega lista funcionarios.
     *
     * @return the string
     */
    public String carregaListaFuncionarios() {
	if (getObrigatoriedadeFuncionario().equals("T")) {
	    try {
		funcionarioDTO = new ListarFuncionarioSaidaDTO();
		setFuncionarioDTO(getListarFuncBradescoService().listarFuncionario(
			getCodFuncionario().equals("") ? 0 : Long.parseLong(getCodFuncionario())));
		setCodigoFuncionarioSelecionado(Long.toString(funcionarioDTO.getCdFuncionario()));
	    } catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		setCodigoFuncionarioSelecionado(null);

	    }
	}

	return "";
    }

    /**
     * Carrega lista funcionarios consulta.
     *
     * @return the string
     */
    public String carregaListaFuncionariosConsulta() {
	try {
	    funcionarioDTO = new ListarFuncionarioSaidaDTO();
	    setFuncionarioDTO(getListarFuncBradescoService().listarFuncionario(
		    getCodFuncionario().equals("") ? 0 : Long.parseLong(getCodFuncionario())));
	    setCodigoFuncionarioSelecionado(Long.toString(funcionarioDTO.getCdFuncionario()));
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
	    setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
	    setCodigoFuncionarioSelecionado(null);

	}

	return "";
    }

    /**
     * Limpar dados funcionarios.
     */
    public void limparDadosFuncionarios() {
	setCodFuncionario("");
	setCodJuncao("");
	setCodSecao("");
	setCodigoFuncionarioSelecionado("");
	funcionarioDTO = new ListarFuncionarioSaidaDTO();
	funcionarioDTO.setCdJuncao(null);

    }

    /**
     * Voltar pesquisa.
     *
     * @return the string
     */
    public String voltarPesquisa() {

	setItemSelecionadoLista(null);
	return "VOLTAR_DADOS";

    }

    /**
     * Limpar dados.
     *
     * @return the string
     */
    public String limparDados() {
	this.setCentroCustoFiltro("");
	setCodFuncionarioFiltro("");
	this.listaCentroCustoFiltro.clear();
	this.setInicialCentroCustoFiltro("");
	this.setTipoMensagemFiltro("");
	this.setTipoProcessoFiltro(null);
	this.setRecursoFiltro(null);
	this.setIdiomaFiltro(null);
	setObrigatoriedade("");
	setListaGridPesquisa(null);
	setItemSelecionadoLista(null);
	setCodFuncionario(null);
	funcionarioDTO.setCdFuncionario(null);
	funcionarioDTO.setCdJuncao(null);
	funcionarioDTO.setDsFuncionario(null);
	setBtoAcionado(false);

	return "ok";
    }

    /**
     * Limpar dados inc.
     *
     * @return the string
     */
    public String limparDadosInc() {
	this.setCentroCusto("");
	this.setTipoMensagem("");
	this.setTipoProcesso(null);
	this.setMeioTrasmissao(null);
	this.setRecurso(null);
	this.setIdioma(null);
	this.listaCentroCustoCombo.clear();
	limparDadosFuncionarios();
	setListaControleFuncionarios(null);
	setCentroCustoCombo("");

	return "ok";
    }

   

    /**
     * Listar tipo processo.
     */
    private void listarTipoProcesso() {
	try {
	    List<TipoProcessoSaidaDTO> listaTipoProcessoAux;

	    TipoProcessoEntradaDTO tipoProcessoEntradaDTO = new TipoProcessoEntradaDTO();

	    tipoProcessoEntradaDTO.setCdTipoProcsSistema(0);

	    listaTipoProcessoAux = comboService.listarTipoProcesso(tipoProcessoEntradaDTO);

	    for (int i = 0; i < listaTipoProcessoAux.size(); i++) {
		this.listaTipoProcessoFiltro.add(new SelectItem(String.valueOf(listaTipoProcessoAux.get(i).getCdTipoProcesso()), listaTipoProcessoAux
			.get(i).getDsTipoProcesso()));
		listaTipoProcessoHash.put(listaTipoProcessoAux.get(i).getCdTipoProcesso(), listaTipoProcessoAux.get(i).getDsTipoProcesso());
	    }
	} catch (PdcAdapterFunctionalException e) {
	    listaTipoProcessoFiltro = new ArrayList<SelectItem>();
	}

    }

   

    /**
     * Consultar.
     *
     * @return the string
     */
    public String consultar() {
	if (getObrigatoriedadeConsultar().equals("T")) {
	    carregaLista();
	    setItemSelecionadoLista(null);
	}

	return "ok";
    }

    /**
     * Limpar campos recurso.
     */
    public void limparCamposRecurso() {
	if (getRecursoFiltro() == null || getRecursoFiltro() == 0) {
	    setIdiomaFiltro(null);
	    setTipoMensagemFiltro("");
	}
    }

    /**
     * Limpar campos idioma.
     */
    public void limparCamposIdioma() {
	if (getIdiomaFiltro() == null || getIdiomaFiltro() == 0) {
	    setTipoMensagemFiltro("");
	}
    }

    /**
     * Limpar campos recurso combo.
     */
    public void limparCamposRecursoCombo() {
	if (getRecurso() == null || getRecurso() == 0) {
	    setIdioma(null);
	    setTipoMensagem("");
	}
    }

    /**
     * Limpar campos idioma combo.
     */
    public void limparCamposIdiomaCombo() {
	if (getIdioma() == null || getIdioma() == 0) {
	    setTipoMensagem("");
	}
    }

    /**
     * Carrega lista.
     */
    public void carregaLista() {

	try {

	    ListarMsgAlertaGestorEntradaDTO msgAlertaGestorEntradaDTO = new ListarMsgAlertaGestorEntradaDTO();

	    msgAlertaGestorEntradaDTO.setCentroCusto(getCentroCustoFiltro());
	    msgAlertaGestorEntradaDTO.setTipoProcesso(getTipoProcessoFiltro());

	    if (getIdiomaFiltro() == null || getIdiomaFiltro() == 0) {
		setIdiomaFiltro(0);
		msgAlertaGestorEntradaDTO.setIdioma(0);
	    } else {
		msgAlertaGestorEntradaDTO.setIdioma(getIdiomaFiltro());
	    }
	    // Acordado por Email - Prefixo ter� mesmo valor que vier em Centro de Custo
	    msgAlertaGestorEntradaDTO.setPrefixo(getCentroCustoFiltro());

	    if (getRecursoFiltro() == null || getRecursoFiltro() == 0) {
		msgAlertaGestorEntradaDTO.setRecurso(0);
	    } else {
		msgAlertaGestorEntradaDTO.setRecurso(getRecursoFiltro());
	    }
	    if (getTipoMensagemFiltro() == null || getTipoMensagemFiltro().equals("")) {
		// setTipoMensagemFiltro("0");
		msgAlertaGestorEntradaDTO.setTipoMensagem(0);
	    } else {
		msgAlertaGestorEntradaDTO.setTipoMensagem(Integer.parseInt(getTipoMensagemFiltro()));
	    }
	    msgAlertaGestorEntradaDTO.setCodPessoa(codFuncionarioFiltro == null || codFuncionarioFiltro.trim().equals("") ? "0" : codFuncionarioFiltro);

	    setListaGridPesquisa(getMsgAlertaGestorService().listarMensagemAlertaGestor(msgAlertaGestorEntradaDTO));
	    setBtoAcionado(true);

	    this.listaControleRadio = new ArrayList<SelectItem>();
	    for (int i = 0; i < getListaGridPesquisa().size(); i++) {
		this.listaControleRadio.add(new SelectItem(i, " "));
	    }

	    setItemSelecionadoLista(null);

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
	    setListaGridPesquisa(null);

	}

    }

    /**
     * Pesquisar.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisar(ActionEvent evt) {
	return "ok";
    }

    /**
     * Incluir.
     *
     * @return the string
     */
    public String incluir() {
	limparDadosInc();

	return "INCLUIR";
    }

    /**
     * Excluir.
     *
     * @return the string
     */
    public String excluir() {
	try {
	    setObrigatoriedade("");
	    preencheDados();

	} catch (Exception e1) {
	    FacesContext.getCurrentInstance().addMessage("erro", new FacesMessage(e1.getMessage()));
	    return "";
	}

	return "EXCLUIR";
    }

    /**
     * Alterar.
     *
     * @return the string
     */
    public String alterar() {
	setCodigoFuncionarioSelecionado("");
	limparDadosFuncionarios();

	try {
	    preencheDados();
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterMsgAlertaGestor",
		    BradescoViewExceptionActionType.ACTION, false);
	    return null;
	}

	return "ALTERAR";
    }

    /**
     * Avancar alterar.
     *
     * @return the string
     */
    public String avancarAlterar() {
	setObrigatoriedadeFuncionario("T");
	carregaListaFuncionarios();

	if (getCodigoFuncionarioSelecionado() != null) {
	    if (getObrigatoriedade().equals("T")) {
		ConsultaMensagemEntradaDTO mensagemEntrada = new ConsultaMensagemEntradaDTO();
		ConsultaMensagemSaidaDTO mensagemSaida = new ConsultaMensagemSaidaDTO();
		setCodigoFuncionarioDesc(getCodigoFuncionarioSelecionado());
		setNomeFuncionarioDesc(funcionarioDTO.getDsFuncionario());
		setIdiomaDesc((String) listaIdiomaHash.get(getIdioma()));
		setRecursoDesc((String) getListaRecursoHash().get(getRecurso()));

		mensagemEntrada.setCdSistema(getCentroCustoDesc());
		mensagemEntrada.setCdIdiomaTextoMensagem(getIdioma());
		mensagemEntrada.setCdRecursoGeradorMensagem(getRecurso());
		mensagemEntrada.setNrEventoMensagemNegocio(Integer.parseInt(getTipoMensagem()));
		mensagemSaida = comboService.consultarMensagem(mensagemEntrada);
		setTipoMensagemDesc(mensagemSaida.getDsEventoMensagemNegocio());
		return "AVANCAR_ALTERAR";
	    }
	}

	return "ok";
    }

    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {
	try {
	    preencheDados();
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MSG_ALERTA_GESTOR,
		    BradescoViewExceptionActionType.ACTION, false);
	    return null;
	}
	return "DETALHAR";
    }

    /**
     * Iniciar tela.
     *
     * @param evt the evt
     */
    public void iniciarTela(ActionEvent evt) {

	setCentroCustoFiltro("");
	setTipoProcessoFiltro(null);
	setCodFuncionarioFiltro("");
	setTipoMensagemFiltro("");
	setTipoProcesso(null);
	setTipoMensagem("");
	setListaGridPesquisa(null);
	setMeioTrasmissao(null);
	this.listaCentroCustoFiltro.clear();
	this.listaRecursoFiltro.clear();
	this.listaIdiomaFiltro.clear();

	setInicialCentroCustoFiltro("");

	this.limparDados();
	this.limparDadosInc();
	listarTipoProcesso();
	carregaListaRecurso();
	carregaListaIdioma();

    }

    /**
     * Consultar funcionario.
     *
     * @return the string
     */
    public String consultarFuncionario() {

	return "CONSULTAR_FUNC";

    }

    /**
     * Limpar incluir.
     *
     * @return the string
     */
    public String limparIncluir() {
	this.setCentroCusto("");
	this.setTipoMensagem("");
	this.setTipoProcesso(null);
	this.setMeioTrasmissao(null);
	this.setObrigatoriedade("");
	return "ok";
    }

    /**
     * Limpar alterar.
     *
     * @return the string
     */
    public String limparAlterar() {
	this.setCentroCusto("");
	this.setTipoMensagem("");
	this.setTipoProcesso(null);
	this.setMeioTrasmissao(null);
	this.setObrigatoriedade("");
	return "ok";
    }

    /**
     * Confirmar alterar.
     *
     * @return the string
     */
    public String confirmarAlterar() {
	if (getHiddenConfirma()) {
	    try {
		AlterarMsgAlertaGestorEntradaDTO msgAlertaGestorEntradaDTO = new AlterarMsgAlertaGestorEntradaDTO();
		msgAlertaGestorEntradaDTO.setCdProcsSistema(getCdProcSist());
		msgAlertaGestorEntradaDTO.setCentroCusto(getCentroCustoDesc());
		msgAlertaGestorEntradaDTO.setFuncionario(Long.valueOf(getCodigoFuncionarioDesc()));
		msgAlertaGestorEntradaDTO.setIdioma(getIdioma());
		msgAlertaGestorEntradaDTO.setMeioTransmissao(getMeioTrasmissao());
		// Passar mesmo valodr de Centro de Custo
		msgAlertaGestorEntradaDTO.setPrefixo(getCentroCustoDesc());
		msgAlertaGestorEntradaDTO.setRecurso(getRecurso());
		msgAlertaGestorEntradaDTO.setTipoMensagem(Integer.parseInt(getTipoMensagem()));
		msgAlertaGestorEntradaDTO.setNrSeqMensagemAlerta(getNrSeqMensagemAlerta());

		AlterarMsgAlertaGestorSaidaDTO msgAlertaGestorSaidaDTO = getMsgAlertaGestorService().alterarMensagemAlertaGestor(
			msgAlertaGestorEntradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + msgAlertaGestorSaidaDTO.getCodMensagem() + ") " + msgAlertaGestorSaidaDTO.getMensagem(),
			CON_MSG_ALERTA_GESTOR, BradescoViewExceptionActionType.ACTION, false);

		
		    carregaLista();
		    setItemSelecionadoLista(null);

	    } catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		return "";
	    }

	}

	return "";
    }

    /**
     * Avancar incluir.
     *
     * @return the string
     */
    public String avancarIncluir() {
	carregaListaFuncionariosConsulta();

	if (getCodigoFuncionarioSelecionado() != null) {
	    if (getObrigatoriedade().equals("T")) {
		ConsultaMensagemEntradaDTO mensagemEntrada = new ConsultaMensagemEntradaDTO();
		ConsultaMensagemSaidaDTO mensagemSaida = new ConsultaMensagemSaidaDTO();
		setCentroCustoDesc((String) getListaCentroCustoHash().get(getCentroCustoCombo()));
		// setTipoProcessoDesc( (String) getListaTipoProcessoHash().get(getTipoProcesso()));
		preencherEmpresaConglomeradoDesc();
		setRecursoDesc((String) getListaRecursoHash().get(getRecurso()));
		setIdiomaDesc((String) getListaIdiomaHash().get(getIdioma()));
		setCodigoFuncionarioDesc(getCodigoFuncionarioSelecionado());
		setNomeFuncionarioDesc(funcionarioDTO.getDsFuncionario());

		mensagemEntrada.setCdSistema(getCentroCustoCombo());
		mensagemEntrada.setCdIdiomaTextoMensagem(getIdioma());
		mensagemEntrada.setCdRecursoGeradorMensagem(getRecurso());
		mensagemEntrada.setNrEventoMensagemNegocio(Integer.parseInt(getTipoMensagem()));
		mensagemSaida = comboService.consultarMensagem(mensagemEntrada);
		setTipoMensagemDesc(mensagemSaida.getDsEventoMensagemNegocio());

		return "AVANCAR_INCLUIR";
	    }
	}

	return "";
    }

    /**
     * Preencher empresa conglomerado desc.
     */
    private void preencherEmpresaConglomeradoDesc() {
	setTipoProcessoDesc("");
	SelectItem selectItemTipoProcesso;

	for (int j = 0; j < getListaTipoProcessoFiltro().size(); j++) {

	    selectItemTipoProcesso = getListaTipoProcessoFiltro().get(j);

	    if (getTipoProcesso().toString().equals(selectItemTipoProcesso.getValue())) {
		setTipoProcessoDesc(selectItemTipoProcesso.getLabel());
		break;

	    }
	}

    }

    /**
     * Confirmar incluir.
     *
     * @return the string
     */
    public String confirmarIncluir() {
	if (getHiddenConfirma()) {
	    try {
		IncluirMsgAlertaGestorEntradaDTO incluirMsgAlertaGestorEntradaDTO = new IncluirMsgAlertaGestorEntradaDTO();

		incluirMsgAlertaGestorEntradaDTO.setCentroCusto(getCentroCustoCombo());
		incluirMsgAlertaGestorEntradaDTO.setTipoProcesso(getTipoProcesso());
		incluirMsgAlertaGestorEntradaDTO.setNrSeqMensagemAlerta(0);
		incluirMsgAlertaGestorEntradaDTO.setCdFuncionario(Long.parseLong(codFuncionario));

		// Salvar mesmo valor que Centro de Custo - Comunicado por Email
		incluirMsgAlertaGestorEntradaDTO.setPrefixo(getCentroCustoCombo());

		incluirMsgAlertaGestorEntradaDTO.setTipoMensagem(Integer.parseInt(getTipoMensagem()));
		incluirMsgAlertaGestorEntradaDTO.setRecurso(getRecurso());
		incluirMsgAlertaGestorEntradaDTO.setIdioma(getIdioma());
		incluirMsgAlertaGestorEntradaDTO.setMeioTransmissao(getMeioTrasmissao());

		IncluirMsgAlertaGestorSaidaDTO incluirMsgAlertaGestorSaidaDTO = getMsgAlertaGestorService().incluirMensagemAlertaGestor(
			incluirMsgAlertaGestorEntradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + incluirMsgAlertaGestorSaidaDTO.getCodMensagem() + ") "
			+ incluirMsgAlertaGestorSaidaDTO.getMensagem(), CON_MSG_ALERTA_GESTOR, BradescoViewExceptionActionType.ACTION, false);

		if(getListaGridPesquisa() != null && ! getListaGridPesquisa().equals("")){
		    carregaLista();
		    setItemSelecionadoLista(null);
		}else{
		    setListaGridPesquisa(null);
		}
		
	    } catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		return null;
	    }
	}

	return "";
    }

    /**
     * Confirmar excluir.
     *
     * @return the string
     */
    public String confirmarExcluir() {
	if (getHiddenConfirma()) {
	    try {

		ExcluirMsgAlertaGestorEntradaDTO msgAlertaGestorEntradaDTO = new ExcluirMsgAlertaGestorEntradaDTO();
		msgAlertaGestorEntradaDTO.setCdProcsSistema(getCdProcSist());
		msgAlertaGestorEntradaDTO.setCentroCusto(getCentroCustoDesc());
		msgAlertaGestorEntradaDTO.setFuncionario(Long.valueOf(getCodigoFuncionarioDesc()));
		msgAlertaGestorEntradaDTO.setIdioma(getIdioma());
		msgAlertaGestorEntradaDTO.setMeioTransmissao(getMeioTrasmissao());
		// Passar mesmo valor de Centro de Custo
		msgAlertaGestorEntradaDTO.setPrefixo(getCentroCustoDesc());
		msgAlertaGestorEntradaDTO.setRecurso(getRecurso());
		msgAlertaGestorEntradaDTO.setTipoMensagem(Integer.parseInt(getTipoMensagem()));
		msgAlertaGestorEntradaDTO.setNrSeqMensagemAlerta(getNrSeqMensagemAlerta());

		ExcluirMsgAlertaGestorSaidaDTO msgAlertaGestorSaidaDTO = getMsgAlertaGestorService().excluirMensagemAlertaGestor(
			msgAlertaGestorEntradaDTO);
		BradescoFacesUtils.addInfoModalMessage("(" + msgAlertaGestorSaidaDTO.getCodMensagem() + ") " + msgAlertaGestorSaidaDTO.getMensagem(),
			CON_MSG_ALERTA_GESTOR, BradescoViewExceptionActionType.ACTION, false);

			
		    carregaLista();
		    setItemSelecionadoLista(null);

	    } catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		return null;
	    }

	}

	return "";
    }

    /**
     * Limpar usuario.
     */
    public void limparUsuario() {
	funcionarioDTO.setCdFuncionario(null);
	funcionarioDTO.setDsFuncionario(null);
	funcionarioDTO.setCdJuncao(null);
    }

    /**
     * Limpar usuario consulta.
     */
    public void limparUsuarioConsulta() {
	if (getCodFuncionario() == null || getCodFuncionario().equals("")) {
	    funcionarioDTO.setCdFuncionario(null);
	    funcionarioDTO.setDsFuncionario(null);
	    funcionarioDTO.setCdJuncao(null);
	}
    }

    /**
     * Buscar centro custo.
     */
    public void buscarCentroCusto() {

	if (getInicialCentroCustoFiltro() != null && !getInicialCentroCustoFiltro().equals("")) {

	    this.listaCentroCustoFiltro = new ArrayList<SelectItem>();

	    List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
	    CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();

	    centroCustoEntradaDTO.setCdSistema(getInicialCentroCustoFiltro());

	    listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);

	    for (CentroCustoSaidaDTO combo : listaCentroCustoSaida) {
		this.listaCentroCustoFiltro.add(new SelectItem(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-" + combo.getDsCentroCusto()));
	    }
	} else {
	    this.listaCentroCustoFiltro = new ArrayList<SelectItem>();
	}

    }

    /**
     * Buscar centro custo incluir.
     */
    public void buscarCentroCustoIncluir() {

	if (getCentroCusto() != null && !getCentroCusto().equals("")) {

	    this.listaCentroCustoCombo = new ArrayList<SelectItem>();

	    List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
	    CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();

	    centroCustoEntradaDTO.setCdSistema(getCentroCusto());

	    listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);

	    listaCentroCustoHash.clear();
	    for (CentroCustoSaidaDTO combo : listaCentroCustoSaida) {
		listaCentroCustoHash.put(combo.getCdCentroCusto(), combo.getDsCentroCusto());
		this.listaCentroCustoCombo.add(new SelectItem(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-" + combo.getDsCentroCusto()));
	    }
	} else {
	    listaCentroCustoHash.clear();
	    this.listaCentroCustoCombo = new ArrayList<SelectItem>();
	}

    }


    /**
     * Carrega lista recurso.
     */
    public void carregaListaRecurso() {
	try {
	    this.listaRecursoFiltro = new ArrayList<SelectItem>();

	    List<ListarRecursoCanalMsgSaidaDTO> listaRecursoCanalMsgSaidaDTO = new ArrayList<ListarRecursoCanalMsgSaidaDTO>();

	    listaRecursoCanalMsgSaidaDTO = comboService.listarRecursoCanalMsg();

	    listaRecursoHash.clear();

	    for (ListarRecursoCanalMsgSaidaDTO combo : listaRecursoCanalMsgSaidaDTO) {
		listaRecursoHash.put(combo.getCdTipoCanal(), combo.getDsTipoCanal());
		this.listaRecursoFiltro.add(new SelectItem(combo.getCdTipoCanal(), combo.getDsTipoCanal()));
	    }
	} catch (PdcAdapterFunctionalException e) {
	    listaRecursoFiltro = new ArrayList<SelectItem>();
	}
    }

    /**
     * Carrega lista idioma.
     */
    public void carregaListaIdioma() {
	try {
	    this.listaIdiomaFiltro = new ArrayList<SelectItem>();

	    List<IdiomaSaidaDTO> listaIdiomaSaida = new ArrayList<IdiomaSaidaDTO>();

	    listaIdiomaSaida = comboService.listarIdioma();

	    listaIdiomaHash.clear();

	    for (IdiomaSaidaDTO combo : listaIdiomaSaida) {
		listaIdiomaHash.put(combo.getCdIdioma(), combo.getDsIdioma());
		this.listaIdiomaFiltro.add(new SelectItem(combo.getCdIdioma(), combo.getDsIdioma()));
	    }
	} catch (PdcAdapterFunctionalException e) {
	    listaIdiomaFiltro = new ArrayList<SelectItem>();
	}
    }



    /**
     * Preenche dados.
     */
    private void preencheDados() {
	ConsultaMensagemEntradaDTO mensagemEntrada = new ConsultaMensagemEntradaDTO();
	ConsultaMensagemSaidaDTO mensagemSaida = new ConsultaMensagemSaidaDTO();
	ListarMsgAlertaGestorSaidaDTO listarMsgAlertaGestorSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
	DetalharMsgAlertaGestorEntradaDTO detalharMsgAlertaGestorEntradaDTO = new DetalharMsgAlertaGestorEntradaDTO();

	detalharMsgAlertaGestorEntradaDTO.setCdFuncionario(listarMsgAlertaGestorSaidaDTO.getCdPessoa());
	detalharMsgAlertaGestorEntradaDTO.setCentroCusto(listarMsgAlertaGestorSaidaDTO.getCentroCusto());
	detalharMsgAlertaGestorEntradaDTO.setIdioma(listarMsgAlertaGestorSaidaDTO.getIdioma());
	detalharMsgAlertaGestorEntradaDTO.setNrSeqMensagemAlerta(listarMsgAlertaGestorSaidaDTO.getNrSeqMensagemAlerta());

	// passar Centro Custo para Prefixo, confirmado por email
	detalharMsgAlertaGestorEntradaDTO.setPrefixo(listarMsgAlertaGestorSaidaDTO.getCentroCusto());
	detalharMsgAlertaGestorEntradaDTO.setRecurso(listarMsgAlertaGestorSaidaDTO.getRecurso());
	detalharMsgAlertaGestorEntradaDTO.setTipoMensagem(listarMsgAlertaGestorSaidaDTO.getTipoMensagem());
	detalharMsgAlertaGestorEntradaDTO.setCdProcsSistema(listarMsgAlertaGestorSaidaDTO.getCdProcsSistema());

	DetalharMsgAlertaGestorSaidaDTO detalharMsgAlertaGestorSaidaDTO = getMsgAlertaGestorService().detalharMensagemAlertaGestaor(
		detalharMsgAlertaGestorEntradaDTO);

	setCentroCustoDesc(detalharMsgAlertaGestorSaidaDTO.getCentroCusto());

	setTipoProcessoDesc(detalharMsgAlertaGestorSaidaDTO.getDsTipoProcesso());
	setTipoProcesso(detalharMsgAlertaGestorSaidaDTO.getCdTipoProcesso());

	setRecurso(detalharMsgAlertaGestorSaidaDTO.getCdRecurso());
	setRecursoDesc(detalharMsgAlertaGestorSaidaDTO.getDsRecurso());

	setIdioma(detalharMsgAlertaGestorSaidaDTO.getCdIdioma());
	setIdiomaDesc((String) getListaIdiomaHash().get(detalharMsgAlertaGestorSaidaDTO.getCdIdioma()));

	setTipoMensagem(Integer.toString(detalharMsgAlertaGestorSaidaDTO.getCdTipoMensagem()));
	mensagemEntrada.setCdSistema(detalharMsgAlertaGestorEntradaDTO.getCentroCusto());
	mensagemEntrada.setCdIdiomaTextoMensagem(detalharMsgAlertaGestorEntradaDTO.getIdioma());
	mensagemEntrada.setCdRecursoGeradorMensagem(detalharMsgAlertaGestorEntradaDTO.getRecurso());
	mensagemEntrada.setNrEventoMensagemNegocio(detalharMsgAlertaGestorEntradaDTO.getTipoMensagem());
	mensagemSaida = comboService.consultarMensagem(mensagemEntrada);
	setTipoMensagemDesc(mensagemSaida.getDsEventoMensagemNegocio());

	setMeioTrasmissao(detalharMsgAlertaGestorSaidaDTO.getCdIndicadorMeioTransmissao());
	setCodigoFuncionarioDesc(Long.toString(detalharMsgAlertaGestorSaidaDTO.getCdFuncionario()));
	setCodFuncionario(String.valueOf(detalharMsgAlertaGestorSaidaDTO.getCdFuncionario()));
	funcionarioDTO = new ListarFuncionarioSaidaDTO();
	funcionarioDTO.setCdFuncionario(detalharMsgAlertaGestorSaidaDTO.getCdFuncionario());
	funcionarioDTO.setDsFuncionario(detalharMsgAlertaGestorSaidaDTO.getDsFuncionario());
	setCodigoFuncionarioSelecionado(String.valueOf(detalharMsgAlertaGestorSaidaDTO.getCdFuncionario()));
	setNomeFuncionarioDesc(detalharMsgAlertaGestorSaidaDTO.getDsFuncionario());
	setCdProcSist(detalharMsgAlertaGestorSaidaDTO.getCdProcSist());
	setNrSeqMensagemAlerta(listarMsgAlertaGestorSaidaDTO.getNrSeqMensagemAlerta());

	setDataHoraManutencao(detalharMsgAlertaGestorSaidaDTO.getDataHoraManutencao());
	setUsuarioManutencao(detalharMsgAlertaGestorSaidaDTO.getUsuarioManutencao());
	setTipoCanalManutencao(detalharMsgAlertaGestorSaidaDTO.getCdCanalManutencao() != 0 ? detalharMsgAlertaGestorSaidaDTO.getCdCanalManutencao()
		+ " - " + detalharMsgAlertaGestorSaidaDTO.getDsCanalManutencao() : "");
	setComplementoManutencao(detalharMsgAlertaGestorSaidaDTO.getComplementoManutencao() != null
		&& !detalharMsgAlertaGestorSaidaDTO.getComplementoManutencao().equals("0") ? detalharMsgAlertaGestorSaidaDTO
		.getComplementoManutencao() : "");

	setDataHoraInclusao(detalharMsgAlertaGestorSaidaDTO.getDataHoraInclusao());
	setUsuarioInclusao(detalharMsgAlertaGestorSaidaDTO.getUsuarioInclusao());
	setComplementoInclusao(detalharMsgAlertaGestorSaidaDTO.getComplementoInclusao() != null
		&& !detalharMsgAlertaGestorSaidaDTO.getComplementoInclusao().equals("0") ? detalharMsgAlertaGestorSaidaDTO.getComplementoInclusao()
		: "");
	setTipoCanalInclusao(detalharMsgAlertaGestorSaidaDTO.getCdCanalInclusao() != 0 ? detalharMsgAlertaGestorSaidaDTO.getCdCanalInclusao() + " - "
		+ detalharMsgAlertaGestorSaidaDTO.getDsCanalInclusao() : "");
    }

    
    
    
    
    //GETTERS E SETTERS
    /**
     * Get: codigoFuncionarioDesc.
     *
     * @return codigoFuncionarioDesc
     */
    public String getCodigoFuncionarioDesc() {
	return codigoFuncionarioDesc;
    }

    /**
     * Set: codigoFuncionarioDesc.
     *
     * @param codigoFuncionarioDesc the codigo funcionario desc
     */
    public void setCodigoFuncionarioDesc(String codigoFuncionarioDesc) {
	this.codigoFuncionarioDesc = codigoFuncionarioDesc;
    }

    /**
     * Get: nomeFuncionarioDesc.
     *
     * @return nomeFuncionarioDesc
     */
    public String getNomeFuncionarioDesc() {
	return nomeFuncionarioDesc;
    }

    /**
     * Set: nomeFuncionarioDesc.
     *
     * @param nomeFuncionarioDesc the nome funcionario desc
     */
    public void setNomeFuncionarioDesc(String nomeFuncionarioDesc) {
	this.nomeFuncionarioDesc = nomeFuncionarioDesc;
    }

    /**
     * Get: listaCentroCustoFiltro.
     *
     * @return listaCentroCustoFiltro
     */
    public List<SelectItem> getListaCentroCustoFiltro() {
	return listaCentroCustoFiltro;
    }
    
    /**
     * Get: codFuncionario.
     *
     * @return codFuncionario
     */
    public String getCodFuncionario() {
	return codFuncionario;
    }

    /**
     * Set: codFuncionario.
     *
     * @param codFuncionario the cod funcionario
     */
    public void setCodFuncionario(String codFuncionario) {
	this.codFuncionario = codFuncionario;
    }

    /**
     * Get: codJuncao.
     *
     * @return codJuncao
     */
    public String getCodJuncao() {
	return codJuncao;
    }

    /**
     * Set: codJuncao.
     *
     * @param codJuncao the cod juncao
     */
    public void setCodJuncao(String codJuncao) {
	this.codJuncao = codJuncao;
    }

    /**
     * Get: codSecao.
     *
     * @return codSecao
     */
    public String getCodSecao() {
	return codSecao;
    }

    /**
     * Set: codSecao.
     *
     * @param codSecao the cod secao
     */
    public void setCodSecao(String codSecao) {
	this.codSecao = codSecao;
    }

    /**
     * Get: listaControleFuncionarios.
     *
     * @return listaControleFuncionarios
     */
    public List<SelectItem> getListaControleFuncionarios() {
	return listaControleFuncionarios;
    }

    /**
     * Set: listaControleFuncionarios.
     *
     * @param listaControleFuncionarios the lista controle funcionarios
     */
    public void setListaControleFuncionarios(List<SelectItem> listaControleFuncionarios) {
	this.listaControleFuncionarios = listaControleFuncionarios;
    }

    /**
     * Get: idiomaDesc.
     *
     * @return idiomaDesc
     */
    public String getIdiomaDesc() {
	return idiomaDesc;
    }
    
    /**
     * Get: listaPrefixoFiltro.
     *
     * @return listaPrefixoFiltro
     */
    public List<SelectItem> getListaPrefixoFiltro() {
	listaPrefixoFiltro.clear();
	listaPrefixoFiltro.add(new SelectItem(Integer.parseInt("1"), "Prefixo Teste"));
	return listaPrefixoFiltro;
    }

    /**
     * Set: listaPrefixoFiltro.
     *
     * @param listaPrefixo the lista prefixo filtro
     */
    public void setListaPrefixoFiltro(List<SelectItem> listaPrefixo) {
	this.listaPrefixoFiltro = listaPrefixo;
    }

    /**
     * Get: listaRecursoFiltro.
     *
     * @return listaRecursoFiltro
     */
    public List<SelectItem> getListaRecursoFiltro() {
	return listaRecursoFiltro;
    }

    /**
     * Set: listaRecursoFiltro.
     *
     * @param listaRecurso the lista recurso filtro
     */
    public void setListaRecursoFiltro(List<SelectItem> listaRecurso) {
	this.listaRecursoFiltro = listaRecurso;
    }

    /**
     * Get: recursoFiltro.
     *
     * @return recursoFiltro
     */
    public Integer getRecursoFiltro() {
	return recursoFiltro;
    }

    /**
     * Set: recursoFiltro.
     *
     * @param recursoFiltro the recurso filtro
     */
    public void setRecursoFiltro(Integer recursoFiltro) {
	this.recursoFiltro = recursoFiltro;
    }

    /**
     * Get: idiomaFiltro.
     *
     * @return idiomaFiltro
     */
    public Integer getIdiomaFiltro() {
	return idiomaFiltro;
    }

    /**
     * Set: idiomaFiltro.
     *
     * @param idiomaFiltro the idioma filtro
     */
    public void setIdiomaFiltro(Integer idiomaFiltro) {
	this.idiomaFiltro = idiomaFiltro;
    }

    /**
     * Get: listaIdiomaFiltro.
     *
     * @return listaIdiomaFiltro
     */
    public List<SelectItem> getListaIdiomaFiltro() {
	return listaIdiomaFiltro;
    }

    /**
     * Set: listaIdiomaFiltro.
     *
     * @param listaIdiomaFiltro the lista idioma filtro
     */
    public void setListaIdiomaFiltro(List<SelectItem> listaIdiomaFiltro) {
	this.listaIdiomaFiltro = listaIdiomaFiltro;
    }

    /**
     * Get: listaIdiomaHash.
     *
     * @return listaIdiomaHash
     */
    Map<Integer, String> getListaIdiomaHash() {
	return listaIdiomaHash;
    }

    /**
     * Set lista idioma hash.
     *
     * @param listaIdiomaHash the lista idioma hash
     */
    void setListaIdiomaHash(Map<Integer, String> listaIdiomaHash) {
	this.listaIdiomaHash = listaIdiomaHash;
    }


    /**
     * Set: idiomaDesc.
     *
     * @param idiomaDesc the idioma desc
     */
    public void setIdiomaDesc(String idiomaDesc) {
	this.idiomaDesc = idiomaDesc;
    }

    /**
     * Get: prefixoDesc.
     *
     * @return prefixoDesc
     */
    public String getPrefixoDesc() {
	return prefixoDesc;
    }

    /**
     * Set: prefixoDesc.
     *
     * @param prefixoDesc the prefixo desc
     */
    public void setPrefixoDesc(String prefixoDesc) {
	this.prefixoDesc = prefixoDesc;
    }

    /**
     * Get: recursoDesc.
     *
     * @return recursoDesc
     */
    public String getRecursoDesc() {
	return recursoDesc;
    }

    /**
     * Set: recursoDesc.
     *
     * @param recursoDesc the recurso desc
     */
    public void setRecursoDesc(String recursoDesc) {
	this.recursoDesc = recursoDesc;
    }

    /**
     * Get: centroCustoCombo.
     *
     * @return centroCustoCombo
     */
    public String getCentroCustoCombo() {
	return centroCustoCombo;
    }

    /**
     * Set: centroCustoCombo.
     *
     * @param centroCustoCombo the centro custo combo
     */
    public void setCentroCustoCombo(String centroCustoCombo) {
	this.centroCustoCombo = centroCustoCombo;
    }

    /**
     * Get: listaCentroCustoCombo.
     *
     * @return listaCentroCustoCombo
     */
    public List<SelectItem> getListaCentroCustoCombo() {
	return listaCentroCustoCombo;
    }

    /**
     * Set: listaCentroCustoCombo.
     *
     * @param listaCentroCustoCombo the lista centro custo combo
     */
    public void setListaCentroCustoCombo(List<SelectItem> listaCentroCustoCombo) {
	this.listaCentroCustoCombo = listaCentroCustoCombo;
    }

    /**
     * Get: listaTipoProcesso.
     *
     * @return listaTipoProcesso
     */
    public List<SelectItem> getListaTipoProcesso() {
	return listaTipoProcesso;
    }

    /**
     * Set: listaTipoProcesso.
     *
     * @param listaTipoProcesso the lista tipo processo
     */
    public void setListaTipoProcesso(List<SelectItem> listaTipoProcesso) {
	this.listaTipoProcesso = listaTipoProcesso;
    }

    /**
     * Get: listaPrefixo.
     *
     * @return listaPrefixo
     */
    public List<SelectItem> getListaPrefixo() {
	listaPrefixo.clear();
	listaPrefixo.add(new SelectItem("1", "Prefixo Teste"));
	return listaPrefixo;
    }

    /**
     * Set: listaPrefixo.
     *
     * @param listaPrefixo the lista prefixo
     */
    public void setListaPrefixo(List<SelectItem> listaPrefixo) {
	this.listaPrefixo = listaPrefixo;
    }

    /**
     * Get: listaRecurso.
     *
     * @return listaRecurso
     */
    public List<SelectItem> getListaRecurso() {
	return listaRecurso;
    }

    /**
     * Set: listaRecurso.
     *
     * @param listaRecurso the lista recurso
     */
    public void setListaRecurso(List<SelectItem> listaRecurso) {
	this.listaRecurso = listaRecurso;
    }

    /**
     * Get: listaIdioma.
     *
     * @return listaIdioma
     */
    public List<SelectItem> getListaIdioma() {
	return listaIdioma;
    }

    /**
     * Set: listaIdioma.
     *
     * @param listaIdioma the lista idioma
     */
    public void setListaIdioma(List<SelectItem> listaIdioma) {
	this.listaIdioma = listaIdioma;
    }

    /**
     * Get: listaTipoMensagem.
     *
     * @return listaTipoMensagem
     */
    public List<SelectItem> getListaTipoMensagem() {
	return listaTipoMensagem;
    }

    /**
     * Set: listaTipoMensagem.
     *
     * @param listaTipoMensagem the lista tipo mensagem
     */
    public void setListaTipoMensagem(List<SelectItem> listaTipoMensagem) {
	this.listaTipoMensagem = listaTipoMensagem;
    }

    /**
     * Get: centroCustoDesc.
     *
     * @return centroCustoDesc
     */
    public String getCentroCustoDesc() {
	return centroCustoDesc;
    }

    /**
     * Set: centroCustoDesc.
     *
     * @param centroCustoDesc the centro custo desc
     */
    public void setCentroCustoDesc(String centroCustoDesc) {
	this.centroCustoDesc = centroCustoDesc;
    }

    /**
     * Get: funcAtualDesc.
     *
     * @return funcAtualDesc
     */
    public String getFuncAtualDesc() {
	return funcAtualDesc;
    }

    /**
     * Set: funcAtualDesc.
     *
     * @param funcAtualDesc the func atual desc
     */
    public void setFuncAtualDesc(String funcAtualDesc) {
	this.funcAtualDesc = funcAtualDesc;
    }

    /**
     * Get: funcionarioDesc.
     *
     * @return funcionarioDesc
     */
    public String getFuncionarioDesc() {
	return funcionarioDesc;
    }

    /**
     * Set: funcionarioDesc.
     *
     * @param funcionarioDesc the funcionario desc
     */
    public void setFuncionarioDesc(String funcionarioDesc) {
	this.funcionarioDesc = funcionarioDesc;
    }

    /**
     * Get: idioma.
     *
     * @return idioma
     */
    public Integer getIdioma() {
	return idioma;
    }

    /**
     * Set: idioma.
     *
     * @param idioma the idioma
     */
    public void setIdioma(Integer idioma) {
	this.idioma = idioma;
    }

    /**
     * Get: prefixo.
     *
     * @return prefixo
     */
    public String getPrefixo() {
	return prefixo;
    }

    /**
     * Set: prefixo.
     *
     * @param prefixo the prefixo
     */
    public void setPrefixo(String prefixo) {
	this.prefixo = prefixo;
    }

    /**
     * Get: recurso.
     *
     * @return recurso
     */
    public Integer getRecurso() {
	return recurso;
    }

    /**
     * Set: recurso.
     *
     * @param recurso the recurso
     */
    public void setRecurso(Integer recurso) {
	this.recurso = recurso;
    }

    /**
     * Get: listaControleRadio.
     *
     * @return listaControleRadio
     */
    public List<SelectItem> getListaControleRadio() {
	return listaControleRadio;
    }

    /**
     * Set: listaControleRadio.
     *
     * @param listaControleRadio the lista controle radio
     */
    public void setListaControleRadio(List<SelectItem> listaControleRadio) {
	this.listaControleRadio = listaControleRadio;
    }

    /**
     * Get: listaGridPesquisa.
     *
     * @return listaGridPesquisa
     */
    public List<ListarMsgAlertaGestorSaidaDTO> getListaGridPesquisa() {
	return listaGridPesquisa;
    }

    /**
     * Set: listaGridPesquisa.
     *
     * @param listaGridPesquisa the lista grid pesquisa
     */
    public void setListaGridPesquisa(List<ListarMsgAlertaGestorSaidaDTO> listaGridPesquisa) {
	this.listaGridPesquisa = listaGridPesquisa;
    }

    /**
     * Get: centroCustoFiltro.
     *
     * @return centroCustoFiltro
     */
    public String getCentroCustoFiltro() {
	return centroCustoFiltro;
    }

    /**
     * Set: centroCustoFiltro.
     *
     * @param centroCustoFiltro the centro custo filtro
     */
    public void setCentroCustoFiltro(String centroCustoFiltro) {
	this.centroCustoFiltro = centroCustoFiltro;
    }

    /**
     * Get: listaCentroCustoHash.
     *
     * @return listaCentroCustoHash
     */
    Map<String, String> getListaCentroCustoHash() {
	return listaCentroCustoHash;
    }

    /**
     * Set lista centro custo hash.
     *
     * @param listaCentroCustoHash the lista centro custo hash
     */
    void setListaCentroCustoHash(Map<String, String> listaCentroCustoHash) {
	this.listaCentroCustoHash = listaCentroCustoHash;
    }

    /**
     * Get: centroCusto.
     *
     * @return centroCusto
     */
    public String getCentroCusto() {
	return centroCusto;
    }

    /**
     * Set: centroCusto.
     *
     * @param centroCusto the centro custo
     */
    public void setCentroCusto(String centroCusto) {
	this.centroCusto = centroCusto;
    }

    /**
     * Get: tipoMensagem.
     *
     * @return tipoMensagem
     */
    public String getTipoMensagem() {
	return tipoMensagem;
    }

    /**
     * Set: tipoMensagem.
     *
     * @param tipoMensagem the tipo mensagem
     */
    public void setTipoMensagem(String tipoMensagem) {
	this.tipoMensagem = tipoMensagem;
    }

    /**
     * Get: tipoProcesso.
     *
     * @return tipoProcesso
     */
    public Integer getTipoProcesso() {
	return tipoProcesso;
    }

    /**
     * Set: tipoProcesso.
     *
     * @param tipoProcesso the tipo processo
     */
    public void setTipoProcesso(Integer tipoProcesso) {
	this.tipoProcesso = tipoProcesso;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
	return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
	this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
	return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
	this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
	return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
	this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
	return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
	this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
	return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
	this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
	return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
	this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
	return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
	this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
	return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
	this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: itemSelecionadoLista.
     *
     * @return itemSelecionadoLista
     */
    public Integer getItemSelecionadoLista() {
	return itemSelecionadoLista;
    }

    /**
     * Set: itemSelecionadoLista.
     *
     * @param itemSelecionadoLista the item selecionado lista
     */
    public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
	this.itemSelecionadoLista = itemSelecionadoLista;
    }

    /**
     * Get: meioTrasmissao.
     *
     * @return meioTrasmissao
     */
    public Integer getMeioTrasmissao() {
	return meioTrasmissao;
    }

    /**
     * Set: meioTrasmissao.
     *
     * @param meioTrasmissao the meio trasmissao
     */
    public void setMeioTrasmissao(Integer meioTrasmissao) {
	this.meioTrasmissao = meioTrasmissao;
    }

    /**
     * Get: obrigatoriedade.
     *
     * @return obrigatoriedade
     */
    public String getObrigatoriedade() {
	return obrigatoriedade;
    }

    /**
     * Set: obrigatoriedade.
     *
     * @param obrigatoriedade the obrigatoriedade
     */
    public void setObrigatoriedade(String obrigatoriedade) {
	this.obrigatoriedade = obrigatoriedade;
    }

    /**
     * Get: comboService.
     *
     * @return comboService
     */
    public IComboService getComboService() {
	return comboService;
    }

    /**
     * Set: comboService.
     *
     * @param comboService the combo service
     */
    public void setComboService(IComboService comboService) {
	this.comboService = comboService;
    }

    /**
     * Get: msgAlertaGestorService.
     *
     * @return msgAlertaGestorService
     */
    public IMsgAlertaGestorService getMsgAlertaGestorService() {
	return msgAlertaGestorService;
    }

    /**
     * Set: msgAlertaGestorService.
     *
     * @param msgAlertaGestorService the msg alerta gestor service
     */
    public void setMsgAlertaGestorService(IMsgAlertaGestorService msgAlertaGestorService) {
	this.msgAlertaGestorService = msgAlertaGestorService;
    }

    /**
     * Get: tipoProcessoDesc.
     *
     * @return tipoProcessoDesc
     */
    public String getTipoProcessoDesc() {
	return tipoProcessoDesc;
    }

    /**
     * Set: tipoProcessoDesc.
     *
     * @param tipoProcessoDesc the tipo processo desc
     */
    public void setTipoProcessoDesc(String tipoProcessoDesc) {
	this.tipoProcessoDesc = tipoProcessoDesc;
    }

    /**
     * Get: hiddenConfirma.
     *
     * @return hiddenConfirma
     */
    public boolean getHiddenConfirma() {
	return hiddenConfirma;
    }

    /**
     * Set: listaCentroCustoFiltro.
     *
     * @param listaCentroCustoFiltro the lista centro custo filtro
     */
    public void setListaCentroCustoFiltro(List<SelectItem> listaCentroCustoFiltro) {
	this.listaCentroCustoFiltro = listaCentroCustoFiltro;
    }

    /**
     * Get: obrigatoriedadeFuncionario.
     *
     * @return obrigatoriedadeFuncionario
     */
    public String getObrigatoriedadeFuncionario() {
	return obrigatoriedadeFuncionario;
    }

    /**
     * Set: obrigatoriedadeFuncionario.
     *
     * @param obrigatoriedadeFuncionario the obrigatoriedade funcionario
     */
    public void setObrigatoriedadeFuncionario(String obrigatoriedadeFuncionario) {
	this.obrigatoriedadeFuncionario = obrigatoriedadeFuncionario;
    }

    /**
     * Get: listaRecursoHash.
     *
     * @return listaRecursoHash
     */
    Map<Integer, String> getListaRecursoHash() {
	return listaRecursoHash;
    }

    /**
     * Set lista recurso hash.
     *
     * @param listaRecursoHash the lista recurso hash
     */
    void setListaRecursoHash(Map<Integer, String> listaRecursoHash) {
	this.listaRecursoHash = listaRecursoHash;
    }

    /**
     * Get: obrigatoriedadeConsultar.
     *
     * @return obrigatoriedadeConsultar
     */
    public String getObrigatoriedadeConsultar() {
	return obrigatoriedadeConsultar;
    }

    /**
     * Set: obrigatoriedadeConsultar.
     *
     * @param obrigatoriedadeConsultar the obrigatoriedade consultar
     */
    public void setObrigatoriedadeConsultar(String obrigatoriedadeConsultar) {
	this.obrigatoriedadeConsultar = obrigatoriedadeConsultar;
    }

    /**
     * Get: listarFuncBradescoService.
     *
     * @return listarFuncBradescoService
     */
    public IListarFuncBradescoService getListarFuncBradescoService() {
	return listarFuncBradescoService;
    }

    /**
     * Set: listarFuncBradescoService.
     *
     * @param listarFuncBradescoService the listar func bradesco service
     */
    public void setListarFuncBradescoService(IListarFuncBradescoService listarFuncBradescoService) {
	this.listarFuncBradescoService = listarFuncBradescoService;
    }

    /**
     * Get: funcionarioDTO.
     *
     * @return funcionarioDTO
     */
    public ListarFuncionarioSaidaDTO getFuncionarioDTO() {
	return funcionarioDTO;
    }

    /**
     * Set: funcionarioDTO.
     *
     * @param funcionarioDTO the funcionario dto
     */
    public void setFuncionarioDTO(ListarFuncionarioSaidaDTO funcionarioDTO) {
	this.funcionarioDTO = funcionarioDTO;
    }

    /**
     * Get: codigoFuncionarioSelecionado.
     *
     * @return codigoFuncionarioSelecionado
     */
    public String getCodigoFuncionarioSelecionado() {
	return codigoFuncionarioSelecionado;
    }

    /**
     * Set: codigoFuncionarioSelecionado.
     *
     * @param codigoFuncionarioSelecionado the codigo funcionario selecionado
     */
    public void setCodigoFuncionarioSelecionado(String codigoFuncionarioSelecionado) {
	this.codigoFuncionarioSelecionado = codigoFuncionarioSelecionado;
    }

    /**
     * Get: inicialCentroCustoFiltro.
     *
     * @return inicialCentroCustoFiltro
     */
    public String getInicialCentroCustoFiltro() {
	return inicialCentroCustoFiltro;
    }

    /**
     * Set: inicialCentroCustoFiltro.
     *
     * @param inicialCentroCustoFiltro the inicial centro custo filtro
     */
    public void setInicialCentroCustoFiltro(String inicialCentroCustoFiltro) {
	this.inicialCentroCustoFiltro = inicialCentroCustoFiltro;
    }

    /**
     * Get: listaTipoMensagemHash.
     *
     * @return listaTipoMensagemHash
     */
    Map<Integer, String> getListaTipoMensagemHash() {
	return listaTipoMensagemHash;
    }

    /**
     * Set lista tipo mensagem hash.
     *
     * @param listaTipoMensagemHash the lista tipo mensagem hash
     */
    void setListaTipoMensagemHash(Map<Integer, String> listaTipoMensagemHash) {
	this.listaTipoMensagemHash = listaTipoMensagemHash;
    }

    /**
     * Get: listaTipoProcessoHash.
     *
     * @return listaTipoProcessoHash
     */
    Map<Integer, String> getListaTipoProcessoHash() {
	return listaTipoProcessoHash;
    }

    /**
     * Set lista tipo processo hash.
     *
     * @param listaTipoProcessoHash the lista tipo processo hash
     */
    void setListaTipoProcessoHash(Map<Integer, String> listaTipoProcessoHash) {
	this.listaTipoProcessoHash = listaTipoProcessoHash;
    }

    /**
     * Get: tipoMensagemDesc.
     *
     * @return tipoMensagemDesc
     */
    public String getTipoMensagemDesc() {
	return tipoMensagemDesc;
    }

    /**
     * Set: tipoMensagemDesc.
     *
     * @param tipoMensagemDesc the tipo mensagem desc
     */
    public void setTipoMensagemDesc(String tipoMensagemDesc) {
	this.tipoMensagemDesc = tipoMensagemDesc;
    }

    /**
     * Set: hiddenConfirma.
     *
     * @param hiddenConfirma the hidden confirma
     */
    public void setHiddenConfirma(boolean hiddenConfirma) {
	this.hiddenConfirma = hiddenConfirma;
    }

    /**
     * Get: cdProcSist.
     *
     * @return cdProcSist
     */
    public int getCdProcSist() {
	return cdProcSist;
    }

    /**
     * Set: cdProcSist.
     *
     * @param cdProcSist the cd proc sist
     */
    public void setCdProcSist(int cdProcSist) {
	this.cdProcSist = cdProcSist;
    }

    /**
     * Get: nrSeqMensagemAlerta.
     *
     * @return nrSeqMensagemAlerta
     */
    public int getNrSeqMensagemAlerta() {
	return nrSeqMensagemAlerta;
    }

    /**
     * Set: nrSeqMensagemAlerta.
     *
     * @param nrSeqMensagemAlerta the nr seq mensagem alerta
     */
    public void setNrSeqMensagemAlerta(int nrSeqMensagemAlerta) {
	this.nrSeqMensagemAlerta = nrSeqMensagemAlerta;
    }

    /**
     * Get: btoAcionado.
     *
     * @return btoAcionado
     */
    public Boolean getBtoAcionado() {
	return btoAcionado;
    }

    /**
     * Set: btoAcionado.
     *
     * @param btoAcionado the bto acionado
     */
    public void setBtoAcionado(Boolean btoAcionado) {
	this.btoAcionado = btoAcionado;
    }

    /**
     * Get: codFuncionarioFiltro.
     *
     * @return codFuncionarioFiltro
     */
    public String getCodFuncionarioFiltro() {
        return codFuncionarioFiltro;
    }

    /**
     * Set: codFuncionarioFiltro.
     *
     * @param codFuncionarioFiltro the cod funcionario filtro
     */
    public void setCodFuncionarioFiltro(String codFuncionarioFiltro) {
        this.codFuncionarioFiltro = codFuncionarioFiltro;
    }
    
    /**
     * Set: listaTipoProcessoFiltro.
     *
     * @param listaTipoProcesso the lista tipo processo filtro
     */
    public void setListaTipoProcessoFiltro(List<SelectItem> listaTipoProcesso) {
	this.listaTipoProcessoFiltro = listaTipoProcesso;
    }
    
    /**
     * Get: listaTipoProcessoFiltro.
     *
     * @return listaTipoProcessoFiltro
     */
    public List<SelectItem> getListaTipoProcessoFiltro() {
	return listaTipoProcessoFiltro;

    }
    
    /**
     * Get: tipoMensagemFiltro.
     *
     * @return tipoMensagemFiltro
     */
    public String getTipoMensagemFiltro() {
	return tipoMensagemFiltro;
    }

    /**
     * Set: tipoMensagemFiltro.
     *
     * @param tipoMensagemFiltro the tipo mensagem filtro
     */
    public void setTipoMensagemFiltro(String tipoMensagemFiltro) {
	this.tipoMensagemFiltro = tipoMensagemFiltro;
    }

    /**
     * Get: tipoProcessoFiltro.
     *
     * @return tipoProcessoFiltro
     */
    public Integer getTipoProcessoFiltro() {
	return tipoProcessoFiltro;
    }

    /**
     * Set: tipoProcessoFiltro.
     *
     * @param tipoProcessoFiltro the tipo processo filtro
     */
    public void setTipoProcessoFiltro(Integer tipoProcessoFiltro) {
	this.tipoProcessoFiltro = tipoProcessoFiltro;
    }

}
