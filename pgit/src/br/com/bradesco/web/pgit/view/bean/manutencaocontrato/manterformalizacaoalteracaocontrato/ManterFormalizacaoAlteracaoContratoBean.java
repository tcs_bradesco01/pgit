/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterformalizacaoalteracaocontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterformalizacaoalteracaocontrato;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoService;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoServiceConstants;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.IImprimirAnexoPrimeiroContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.IImprimirAnexoSegundoContratoService;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.IImprimirFormalizacaoAlteracaoContratoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManutencaoMeioTransmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManutencaoMeioTransmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConRelacaoConManutencoesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConRelacaoConManutencoesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ExcluirManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ExcluirManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.IncluirManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.IncluirManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ParticipantesDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ParticipantesRelacaoConManutencoesDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.PerfilDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ServicosDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.VinculoDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.VinculoRelacaoConManutencoesDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoConfigContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoConfigContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterFormalizacaoAlteracaoContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterFormalizacaoAlteracaoContratoBean {

    // Constante utilizada para evitar redund�ncia apontada pelo Sonar
    /** Atributo FOR_MANTER_FORMALIZACAO_ALTERACAO_CONTRATO. */
    private static final String FOR_MANTER_FORMALIZACAO_ALTERACAO_CONTRATO = "forManterFormalizacaoAlteracaoContrato";

    /** Atributo solManutContratosServiceImpl. */
    private ISolManutContratosService solManutContratosServiceImpl;

    /** Atributo imprimirFormalizacaoAlteracaoContratoServiceImpl. */
    private IImprimirFormalizacaoAlteracaoContratoService imprimirFormalizacaoAlteracaoContratoServiceImpl;

    /** Atributo imprimirAnexoPrimeiroContratoService. */
    private IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService;

    /** Atributo imprimirAnexoSegundoContratoService. */
    private IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService;

    /** Atributo manterContratoService. */
    private IManterContratoService manterContratoService;

    /** Atributo identificacaoClienteContratoBean. */
    private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

    /** Atributo numeroFiltro. */
    private String numeroFiltro;

    /** Atributo codigoSituacaoFiltro. */
    private Integer codigoSituacaoFiltro;

    /** Atributo dataInicialPagamentoFiltro. */
    private Date dataInicialPagamentoFiltro;

    /** Atributo dataFinalPagamentoFiltro. */
    private Date dataFinalPagamentoFiltro;

    /** Atributo radioSelecaoFiltro. */
    private String radioSelecaoFiltro;

    /** Atributo existeDadosIncluir. */
    private boolean existeDadosIncluir;

    // dados do incluir
    /** Atributo radioInstrucao. */
    private Integer radioInstrucao;

    // Dados Cliente Representante e Contrato
    /** Atributo cpfCnpjMaster. */
    private String cpfCnpjMaster;

    /** Atributo dsGerenteResponsavel. */
    private String dsGerenteResponsavel;

    /** Atributo cpfCnpjParticipante. */
    private String cpfCnpjParticipante;

    /** Atributo nomeRazaoParticipante. */
    private String nomeRazaoParticipante;

    /** Atributo nomeRazaoSocialMaster. */
    private String nomeRazaoSocialMaster;

    /** Atributo grupoEconomicoMaster. */
    private String grupoEconomicoMaster;

    /** Atributo atividadeEconomicaMaster. */
    private String atividadeEconomicaMaster;

    /** Atributo segmentoMaster. */
    private String segmentoMaster;

    /** Atributo subSegmentoMaster. */
    private String subSegmentoMaster;

    /** Atributo cpfParticipante. */
    private String cpfParticipante;

    /** Atributo nomeParticipante. */
    private String nomeParticipante;

    /** Atributo dataNascimentoConstituicao. */
    private String dataNascimentoConstituicao;

    /** Atributo grupoEconomicoParticipante. */
    private String grupoEconomicoParticipante;

    /** Atributo tipoParticipacaoContrato. */
    private String tipoParticipacaoContrato;

    /** Atributo banco. */
    private String banco;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo cdBancoFormatado. */
    private String cdBancoFormatado;

    /** Atributo agencia. */
    private String agencia;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo cdAgenciaFormatado. */
    private String cdAgenciaFormatado;

    /** Atributo conta. */
    private String conta;

    /** Atributo tipoConta. */
    private String tipoConta;

    /** Atributo cpfCnpjTitular. */
    private String cpfCnpjTitular;

    /** Atributo nomeRazaoSocialConta. */
    private String nomeRazaoSocialConta;

    /** Atributo servico. */
    private String servico;

    /** Atributo modalidade. */
    private String modalidade;

    /** Atributo situacao. */
    private String situacao;

    /** Atributo situacaoFiltro. */
    private Integer situacaoFiltro;

    /**
     * Get: situacaoFiltro.
     *
     * @return situacaoFiltro
     */
    public Integer getSituacaoFiltro() {
        return situacaoFiltro;
    }

    /**
     * Set: situacaoFiltro.
     *
     * @param situacaoFiltro the situacao filtro
     */
    public void setSituacaoFiltro(Integer situacaoFiltro) {
        this.situacaoFiltro = situacaoFiltro;
    }

    /** Atributo itemSelecSituacao. */
    private String itemSelecSituacao;

    /** Atributo motivo. */
    private String motivo;

    /** Atributo acao. */
    private String acao;

    /** Atributo empresaGestora. */
    private String empresaGestora;

    /** Atributo cdEmpresa. */
    private Long cdEmpresa;

    /** Atributo tipo. */
    private String tipo;

    /** Atributo cdTipo. */
    private Integer cdTipo;

    /** Atributo numero. */
    private String numero;

    /** Atributo descricao. */
    private String descricao;

    /** Atributo motivoDesc. */
    private String motivoDesc;

    /** Atributo situacaoDesc. */
    private String situacaoDesc;

    /** Atributo participacao. */
    private String participacao;

    /** Atributo dsAgenciaOperadora. */
    private String dsAgenciaOperadora;

    // add em 12/8/2010.

    // Solicita��es
    /** Atributo dsAgenciaGestora. */
    private String dsAgenciaGestora;

    /** Atributo empresa. */
    private String empresa;

    /** Atributo descricaoContrato. */
    private String descricaoContrato;

    /** Atributo cpfCnpj. */
    private String cpfCnpj;

    /** Atributo nomeRazaoSocial. */
    private String nomeRazaoSocial;

    /** Atributo desabilitaCamposPesquisa. */
    private boolean desabilitaCamposPesquisa;

    /** Atributo listaGridSolicitacao. */
    private List<ListarManutencaoContratoSaidaDTO> listaGridSolicitacao;

    /** Atributo itemSelecionadoSolicitacao. */
    private Integer itemSelecionadoSolicitacao;

    /** Atributo indicadorTipoManutencao. */
    private Integer indicadorTipoManutencao;

    /** Atributo listaControleSolicitacao. */
    private List<SelectItem> listaControleSolicitacao = new ArrayList<SelectItem>();

    /** Atributo radioFiltroSolicitacoes. */
    private String radioFiltroSolicitacoes;

    /** Atributo tipoManutencaoFiltro. */
    private Integer tipoManutencaoFiltro;

    /** Atributo acaoFiltro. */
    private Integer acaoFiltro;

    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;

    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;

    /** Atributo desabilitaConsulta. */
    private boolean desabilitaConsulta;

    /** Atributo dtPeriodoInicio. */
    private Date dtPeriodoInicio;

    /** Atributo dtPeriodoFim. */
    private Date dtPeriodoFim;

    /* Detalhar - Excluir */
    /** Atributo numeroFormalizacao. */
    private String numeroFormalizacao;

    /** Atributo dtHraCadastramento. */
    private String dtHraCadastramento;

    /** Atributo dtHraAssinatura. */
    private String dtHraAssinatura;

    /** Atributo dsSituacaoFormalizacao. */
    private String dsSituacaoFormalizacao;

    /* Trilha de Auditoria */
    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    // Grid Consultar
    /** Atributo listaGridPesquisa. */
    private List<ConsultarManterFormAltContratoSaidaDTO> listaGridPesquisa;

    /** Atributo itemSelecionadoLista. */
    private Integer itemSelecionadoLista;

    /** Atributo listaControleRadio. */
    private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

    // Combo a ser carregada com os dados retornados pelo PDC
    /** Atributo listaSituacaoFiltro. */
    private List<SelectItem> listaSituacaoFiltro = new ArrayList<SelectItem>();

    /** Atributo listaSituacao. */
    private List<SelectItem> listaSituacao = new ArrayList<SelectItem>();

    // Combo de controle para apresentar as descri�oes.
    /** Atributo listaSituacaoFiltroHash. */
    private Map<Integer, String> listaSituacaoFiltroHash = new HashMap<Integer, String>();

    /* Grid Participantes */
    /** Atributo listaGridParticipantes. */
    private List<ParticipantesDTO> listaGridParticipantes;

    /* Grid Contas - V�nculo */
    /** Atributo listaGridVinculos. */
    private List<VinculoDTO> listaGridVinculos;

    /* Grid Participantes - Incluir */
    /** Atributo listaGridParticipantesIncluir. */
    private List<ParticipantesRelacaoConManutencoesDTO> listaGridParticipantesIncluir;

    /* Grid Contas - V�nculo - Incluir */
    /** Atributo listaGridVinculosIncluir. */
    private List<VinculoRelacaoConManutencoesDTO> listaGridVinculosIncluir;

    /* Grid Servi�os */
    /** Atributo listaGridServicos. */
    private List<ServicosDTO> listaGridServicos;

    //Grid Meio de Transmiss�o/Perfil
    /** Atributo listaGridMeioTransmissao. */
    private List<PerfilDTO> listaGridMeioTransmissao;

    // Atributos que controlar quantidade de registros para as 3 listas de DetalharAditivo e IncluirAditivo para as
    // Grids
    /** Atributo qtdeParticipantes. */
    private int qtdeParticipantes;

    /** Atributo qtdeVinculos. */
    private int qtdeVinculos;

    /** Atributo qtdeGridServicos. */
    private int qtdeGridServicos;

    /** Atributo qtdPerfis. */
    private Integer qtdPerfis;

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo formalizacaoAltContratoImpl. */
    private IManterformalizacaoalteracaocontratoService formalizacaoAltContratoImpl;

    /** Atributo mensagem. */
    private String mensagem;

    // perfil troca de arquivos
    /** Atributo dsCodTipoLayout. */
    private String dsCodTipoLayout;

    /** Atributo cdTipoLayoutArquivo. */
    private Integer cdTipoLayoutArquivo;

    /** Atributo dsLayoutProprio. */
    private String dsLayoutProprio;

    /** Atributo dsAplicFormat. */
    private String dsAplicFormat;

    //remessa
    /** Atributo dsCodMeioPrincipalRemessa. */
    private String dsCodMeioPrincipalRemessa;

    /** Atributo dsCodMeioAltrnRemessa. */
    private String dsCodMeioAltrnRemessa;

    //retorno
    /** Atributo cdCodMeioPrincipalRetorno. */
    private String cdCodMeioPrincipalRetorno;

    /** Atributo dsMeioAltrnRetorno. */
    private String dsMeioAltrnRetorno;

    //dados Controle

    /** Atributo dsUtilizacaoEmpresaVan. */
    private String dsUtilizacaoEmpresaVan;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo dsResponsavelCustoEmpresa. */
    private String dsResponsavelCustoEmpresa;

    /** Atributo pcCustoOrganizacaoTransmissao. */
    private BigDecimal pcCustoOrganizacaoTransmissao;

    /** The saida consultar manutencao config contrato. */
    private ConsultarManutencaoConfigContratoSaidaDTO saidaConsultarManutencaoConfigContrato;

    /** Atributo consultarManutencaoServicoContratoSaidaDTO. */
    private ConsultarManutencaoServicoContratoSaidaDTO consultarManutencaoServicoContratoSaidaDTO ;
    
    private ConsultarManutencaoParticipanteSaidaDTO consultarManutencaoParticipanteSaidaDTO;
    
    private ConRelacaoConManutencoesSaidaDTO saidaConRelacaoConManutencoes;
    
    private DetalharManterFormAltContratoSaidaDTO saidaDetalharManterFormAltContrato;
    
    private IncluirManterFormAltContratoSaidaDTO incluirManterFormAltContratoSaidaDTO;
    
    /** The contrato bytes. */
    private ByteArrayOutputStream contratoBytes = null;

    /** Atributo contratoServiceImpl. */
    private IContratoService contratoServiceImpl = null;
    
    /** The contrato exception. */
    private Exception contratoException = null;

    /**
     * Pesquisar.
     *
     * @param evt the evt
     */
    public void pesquisar(ActionEvent evt) {
        identificacaoClienteContratoBean.consultarContrato();
    }

    /**
     * Pesquisar solicitacao.
     *
     * @param evt the evt
     */
    public void pesquisarSolicitacao(ActionEvent evt) {
        consultarSolicitacoes();
    }

    /**
     * Consultar.
     *
     * @return the string
     */
    public String consultar() {

        carregaLista();
        return "ok";

    }
    
    /**
     * Consultar versoes paginacao.
     *
     * @param evt the evt
     */
    public void consultarVersoesPaginacao(ActionEvent evt) {
        consultar();
    }

    /**
     * Limpar dados.
     */
    public void limparDados() {

        setNumeroFiltro("");
        setCodigoSituacaoFiltro(0);

        if (getRadioSelecaoFiltro() != null) {
            setDesabilitaConsulta(false);				
        }
    }
    
    /**
     * Imprimir relatorio formalizacao.
     *
     * @return the string
     */
    public String imprimirRelatorioFormalizacao() {
        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String logoPath = servletContext.getRealPath(caminho);

        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();

        par.put("titulo", MessageHelperUtils.getI18nMessage("path_gerar_Versao_Contrato"));

        par.put("empresa", getEmpresaGestora());
        par.put("tipo", getTipo());
        par.put("numero", getNumero());
        par.put("descContrato", getDescricao());
        par.put("logoBradesco", logoPath);

        try {

            // M�todo que gera o relat�rio PDF.
            PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterFormalizacaoAlteracaoContrato",
                "imprimirFormalizacaoContrato", getListaGridPesquisa(), par);

        } /* Exce��o do relat�rio */
        catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
        }

        return "";
    }

    /**
     * Imprimir contrato.
     */
    public void imprimirContrato() {

        contratoBytes = new ByteArrayOutputStream();

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String pathRel = servletContext.getRealPath("/");

        /* Numero do Aditivo*/
        ConsultarManterFormAltContratoSaidaDTO consultarManterFormAltContratoSaidaDTO = getListaGridPesquisa().get(
            getItemSelecionadoLista());

        /* Dados do Contrato */
        ListarContratosPgitSaidaDTO saidaDTOContrato = identificacaoClienteContratoBean.getListaGridPesquisa().get(
            identificacaoClienteContratoBean.getItemSelecionadoLista());

        try {
            getContratoServiceImpl().imprimirContratoPrestacaoServico(
                saidaDTOContrato.getCdTipoContrato(),
                saidaDTOContrato.getCdPessoaJuridica(),
                saidaDTOContrato.getNrSequenciaContrato(), 
                consultarManterFormAltContratoSaidaDTO.getNrAditivoContratoNegocio(),
                contratoBytes, 
                pathRel);
        } catch (Exception e) {
            BradescoCommonServiceFactory.getLogManager().error(this.getClass(), e.getMessage(), e);
            contratoException = e;
            contratoBytes = null;
        }
    }
    
    /**
     * Imprimir contrato final.
     * 
     * @return the string
     *
     */
    public String imprimirContratoFinal() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        if (contratoBytes != null) {
            try {
                response.getOutputStream().write(contratoBytes.toByteArray());
                response.setHeader("Content-Disposition", "attachment; filename=contrato.pdf");
                response.setContentType("application/pdf");

                facesContext.renderResponse();
                facesContext.responseComplete();
            } catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
            }
        } else {
            if (contratoException instanceof PdcAdapterFunctionalException) {
                BradescoFacesUtils.addErrorModalMessage(contratoException.getMessage());
            } else {
                BradescoFacesUtils.addErrorModalMessage(MessageHelperUtils
                    .getI18nMessage(IContratoServiceConstants.ERRO_IMPRESSAO_CONTRATO));
            }
        }

        contratoException = null;
        contratoBytes = null;

        return "";
    }    

    /**
     * Imprimir versao contrato.
     */
    public void imprimirVersaoContrato() {

        contratoBytes = new ByteArrayOutputStream();

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String pathRel = servletContext.getRealPath("/");

        try {
            getContratoServiceImpl().imprimirContratoPrestacaoServico(
                incluirManterFormAltContratoSaidaDTO.getCdTipoContratoNegocio(),
                incluirManterFormAltContratoSaidaDTO.getCdpessoaJuridicaContrato(),
                incluirManterFormAltContratoSaidaDTO.getNrSequenciaContratoNegocio(), 
                incluirManterFormAltContratoSaidaDTO.getNrAditivoContratoNegocio(),
                contratoBytes, 
                pathRel);
        } catch (Exception e) {
            BradescoCommonServiceFactory.getLogManager().error(this.getClass(), e.getMessage(), e);
            contratoException = e;
            contratoBytes = null;
        }
    }

    /**
     * Inicia formalizacao.
     */
    public void iniciaFormalizacao() {
        limparDados();
        setRadioSelecaoFiltro("");
        setDesabilitaConsulta(false);
        consultar();
    }

    /**
     * Limpar dados radio.
     */
    public void limparDadosRadio() {
        // limparDados();
        setRadioSelecaoFiltro("");
        setItemSelecionadoLista(null);
        // limpar();
        setListaGridPesquisa(null);
        setDesabilitaConsulta(false);
    }

    /**
     * Limpar.
     */
    public void limpar() {
        setItemSelecionadoLista(null);
        iniciaFormalizacao();
        setListaGridPesquisa(null);
    }

    /**
     * Limpar campos.
     *
     * @return the string
     */
    public String limparCampos() {

        setRadioSelecaoFiltro("");
        setItemSelecionadoLista(null);
        setListaGridPesquisa(null);
        setDesabilitaConsulta(false);
        setDataInicialPagamentoFiltro(new Date());
        setDataFinalPagamentoFiltro(new Date());
        limparDados();

        return "";
    }

    /**
     * Carrega lista.
     */
    public void carregaLista() {
        setDesabilitaConsulta(false);
        listaGridPesquisa = new ArrayList<ConsultarManterFormAltContratoSaidaDTO>();
        setItemSelecionadoLista(null);

        // carregar DTO de entrada com os atributos da tela.

        try {

            ConsultarManterFormAltContratoEntradaDTO entrada = new ConsultarManterFormAltContratoEntradaDTO();

            // Atributos vindos da Tela de Consulta Inicial
            entrada.setCdPessoaJuridicaContrato(getCdEmpresa());
            entrada.setCdTipoContratoNegocio(getCdTipo());
            entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumero()));

            if (getRadioSelecaoFiltro() == null || getRadioSelecaoFiltro().equals("")) {
                entrada.setCdAcesso(0);
                entrada.setDtInicioInclusaoContrato("");
                entrada.setDtFimInclusaoContrato("");
            } else {
                if (getRadioSelecaoFiltro().equals("0")) {
                    entrada.setCdAcesso(1);
                    entrada.setDtInicioInclusaoContrato("");
                    entrada.setDtFimInclusaoContrato("");
                }
                if (getRadioSelecaoFiltro().equals("1")) {
                    entrada.setCdAcesso(2);
                    entrada.setDtInicioInclusaoContrato("");
                    entrada.setDtFimInclusaoContrato("");
                }
                if (getRadioSelecaoFiltro().equals("2")) {
                    entrada.setCdAcesso(3);
                    entrada.setDtInicioInclusaoContrato(FormatarData.formataDiaMesAno(getDataInicialPagamentoFiltro()));
                    entrada.setDtFimInclusaoContrato(FormatarData.formataDiaMesAno(getDataFinalPagamentoFiltro()));
                }
            }

            // Atributos da tela em foco.

            // numero
            entrada.setNrAditivoContratoNegocio((getNumeroFiltro() == null || getNumeroFiltro().equals("")) ? 0 : Long
                .parseLong(getNumeroFiltro()));

            // codigo da situacao
            entrada.setCdSituacaoAditivoContrato((getCodigoSituacaoFiltro() == null) ? 0 : getCodigoSituacaoFiltro());

            // comunicacao com IFace

            setListaGridPesquisa(getFormalizacaoAltContratoImpl().consultarListaManterFormAltContrato(entrada));

            this.listaControleRadio = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaGridPesquisa().size(); i++) {
                this.listaControleRadio.add(new SelectItem(i, " "));
            }

            setItemSelecionadoLista(null);
            setDesabilitaConsulta(true);
        } catch (PdcAdapterFunctionalException p) {
            listaGridPesquisa = new ArrayList<ConsultarManterFormAltContratoSaidaDTO>();
            setItemSelecionadoLista(null);
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }
    }

    /**
     * Carrega lista situacao filtro.
     */
    public void carregaListaSituacaoFiltro() {
        // carrega o combo de filtro Situacao
        this.listaSituacaoFiltro = new ArrayList<SelectItem>();
        List<ListarSituacaoAditivoContratoSaidaDTO> listaSaida = new ArrayList<ListarSituacaoAditivoContratoSaidaDTO>();

        listaSaida = comboService.listarSituacaoAditivoContrato();

        // limpa a lista hash
        listaSituacaoFiltroHash.clear();

        // Adiciona nas listas os itens retornados
        for (ListarSituacaoAditivoContratoSaidaDTO combo : listaSaida) {

            listaSituacaoFiltroHash.put(combo.getCdSituacao(), combo.getDsSituacao());

            this.listaSituacaoFiltro.add(new SelectItem(combo.getCdSituacao(), combo.getDsSituacao()));
        }
    }

    /**
     * Carsrega lista situacao solic.
     */
    public void carsregaListaSituacaoSolic() {
        List<ListarSituacaoSolicitacaoSaidaDTO> lista = new ArrayList<ListarSituacaoSolicitacaoSaidaDTO>();

        //A pedido de Denize na matriz, n�o chamar� mais o fluxo e os valores deste combo para a tela de solicita��es ser� fixo
        //lista = comboService.listarSituacaoSolicitacao();

        listaSituacao = new ArrayList<SelectItem>();

        listaSituacao.clear();

        listaSituacao.add(new SelectItem(1, "Pendente"));
        listaSituacao.add(new SelectItem(2, "Processada"));
        listaSituacao.add(new SelectItem(3, "Cancelada"));

    }

    /**
     * Campos data validos.
     *
     * @param dia the dia
     * @param mes the mes
     * @param ano the ano
     * @return true, if campos data validos
     */
    private boolean camposDataValidos(String dia, String mes, String ano) {
        // Valida campos para compor a data

        if (!PgitUtil.verificaStringNula(dia).equals("") && !PgitUtil.verificaStringNula(mes).equals("")
                        && !PgitUtil.verificaStringNula(ano).equals("")) {
            return true;
        }

        return false;

    }

    /**
     * Detalhar solicitacao.
     *
     * @return the string
     */
    public String detalharSolicitacao() {

        ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(
            getItemSelecionadoSolicitacao());

        try {

            setDsTipoManutencao(listarManutencaoContratoSaidaDTO.getDsTipoManutencaoContrato());
            setHrManutencaoRegistro(listarManutencaoContratoSaidaDTO.getHrManutencaoRegistro());

            if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 1) {
                setIndicadorTipoManutencao(1);
                preencheDadosParticipantes();
            } else if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 2) {
                setIndicadorTipoManutencao(2);
                preencheDadosConta();
            } else if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 3) {
                setIndicadorTipoManutencao(3);
                preencheDadosServico();
            } else if( listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 4){
                setIndicadorTipoManutencao(4);
                preencheDadosMeioTransmissao();
            } else if( listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 6){
                setIndicadorTipoManutencao(6);
                preencherConsultaManutencaoConfigContrato();
            } else if( listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 9){
                setIndicadorTipoManutencao(9);
                preencherConsultaManutencaoConfigContrato();
            } else{
                return "";
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                "conManterSolManutContrato2", BradescoViewExceptionActionType.ACTION, false);
            return null;
        }
        return "DETALHAR";
    }

    /**
     * Preenche dados conta.
     */
    private void preencheDadosConta() {

        ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(
            getItemSelecionadoSolicitacao());

        ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
        .getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

        ConsultarManutencaoContaEntradaDTO consultarManutencaoContaEntradaDTO = new ConsultarManutencaoContaEntradaDTO();

        consultarManutencaoContaEntradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
        consultarManutencaoContaEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
        consultarManutencaoContaEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO
            .getCdTipoManutencaoContrato());
        consultarManutencaoContaEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO
            .getCdManutencao());
        consultarManutencaoContaEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO
            .getNrSequenciaContrato());

        ConsultarManutencaoContaSaidaDTO consultarManutencaoContaSaidaDTO = solManutContratosServiceImpl
        .detalharContaContrato(consultarManutencaoContaEntradaDTO);

        setConta(consultarManutencaoContaSaidaDTO.getNrConta() + "-"
            + consultarManutencaoContaSaidaDTO.getCdDigitoConta());
        setBanco(consultarManutencaoContaSaidaDTO.getDsBanco());
        setCdBanco(consultarManutencaoContaSaidaDTO.getCdBanco());
        setCdBancoFormatado(getCdBanco() + " - " + getBanco());
        setAgencia(consultarManutencaoContaSaidaDTO.getDsComercialAgenciaContabil());
        setCdAgencia(consultarManutencaoContaSaidaDTO.getCdComercialAgenciaContabil());
        setCdAgenciaFormatado(getCdAgencia() + " - " + getAgencia());
        setTipoConta(consultarManutencaoContaSaidaDTO.getDsTipoContaContrato());
        setCpfCnpjTitular(consultarManutencaoContaSaidaDTO.getCdCpfCnpj());
        setNomeRazaoSocialConta(consultarManutencaoContaSaidaDTO.getNmPessoa());

        setSituacao(consultarManutencaoContaSaidaDTO.getDsSituacaoManutencaoContrato());
        setMotivo(consultarManutencaoContaSaidaDTO.getDsMotivoManutencaoContrato());
        setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao());

        setUsuarioInclusao(consultarManutencaoContaSaidaDTO.getCdUsuarioInclusao());
        setUsuarioManutencao(consultarManutencaoContaSaidaDTO.getCdusuarioManutencao());

        setComplementoInclusao(consultarManutencaoContaSaidaDTO.getCdOperacaoCanalInclusao().equals("0") ? ""
            : consultarManutencaoContaSaidaDTO.getCdOperacaoCanalInclusao());
        setComplementoManutencao(consultarManutencaoContaSaidaDTO.getCdOperacaoCanalManutencao().equals("0") ? ""
            : consultarManutencaoContaSaidaDTO.getCdOperacaoCanalManutencao());
        setTipoCanalInclusao(consultarManutencaoContaSaidaDTO.getCdTipoCanalInclusao() == 0 ? ""
            : consultarManutencaoContaSaidaDTO.getCdTipoCanalInclusao() + " - "
            + consultarManutencaoContaSaidaDTO.getDsCanalInclusao());
        setTipoCanalManutencao(consultarManutencaoContaSaidaDTO.getCdTipoCanalManutencao() == 0 ? ""
            : consultarManutencaoContaSaidaDTO.getCdTipoCanalManutencao() + " - "
            + consultarManutencaoContaSaidaDTO.getDsCanalManutencao());

        setDataHoraInclusao(consultarManutencaoContaSaidaDTO.getHrInclusaoRegistro());
        setDataHoraManutencao(consultarManutencaoContaSaidaDTO.getHrManutencaoRegistro());

    }

    /**
     * Preenche dados participantes.
     */
    private void preencheDadosParticipantes() {

        ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = 
            getListaGridSolicitacao().get(getItemSelecionadoSolicitacao());

        ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = 
            identificacaoClienteContratoBean.getListaGridPesquisa().get(
                identificacaoClienteContratoBean.getItemSelecionadoLista());

        ConsultarManutencaoParticipanteEntradaDTO entradaDTO = new ConsultarManutencaoParticipanteEntradaDTO();

        entradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
        entradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
        entradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato());
        entradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO.getCdManutencao());
        entradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());

        setConsultarManutencaoParticipanteSaidaDTO(new ConsultarManutencaoParticipanteSaidaDTO());
        consultarManutencaoParticipanteSaidaDTO = solManutContratosServiceImpl.detalharParticipanteContrato(entradaDTO);

        setCpfParticipante(consultarManutencaoParticipanteSaidaDTO.getCpfParticipante());
        setNomeParticipante(consultarManutencaoParticipanteSaidaDTO.getNomeParticipane());
        setDataNascimentoConstituicao(consultarManutencaoParticipanteSaidaDTO.getDataNascimento());
        setGrupoEconomicoParticipante(consultarManutencaoParticipanteSaidaDTO.getDsGrupoEconomicoParticipante());
        setTipoParticipacaoContrato(consultarManutencaoParticipanteSaidaDTO.getDsTipoParticipacaoPessoa());

        setSituacao(consultarManutencaoParticipanteSaidaDTO.getDsSituacaoManutencaoContrato());
        setMotivo(consultarManutencaoParticipanteSaidaDTO.getDsMotivoManutencaoContrato());
        setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao());

        setUsuarioInclusao(consultarManutencaoParticipanteSaidaDTO.getUsuarioInclusao());
        setUsuarioManutencao(consultarManutencaoParticipanteSaidaDTO.getUsuarioManutencao());

        setComplementoInclusao(consultarManutencaoParticipanteSaidaDTO.getComplementoInclusao().equals("0") ? ""
            : consultarManutencaoParticipanteSaidaDTO.getComplementoInclusao());
        setComplementoManutencao(consultarManutencaoParticipanteSaidaDTO.getComplementoManutencao().equals("0") ? ""
            : consultarManutencaoParticipanteSaidaDTO.getComplementoManutencao());
        setTipoCanalInclusao(consultarManutencaoParticipanteSaidaDTO.getCdCanalInclusao() == 0 ? ""
            : consultarManutencaoParticipanteSaidaDTO.getCdCanalInclusao() + " - "
            + consultarManutencaoParticipanteSaidaDTO.getDsCanalInclusao());
        setTipoCanalManutencao(consultarManutencaoParticipanteSaidaDTO.getCdCanalManutencao() == 0 ? ""
            : consultarManutencaoParticipanteSaidaDTO.getCdCanalManutencao() + " - "
            + consultarManutencaoParticipanteSaidaDTO.getDsCanalManutencao());

        setDataHoraInclusao(consultarManutencaoParticipanteSaidaDTO.getDataHoraInclusao());
        setDataHoraManutencao(consultarManutencaoParticipanteSaidaDTO.getDataHoraManutencao());

    }

    /**
     * Preenche dados servico.
     */
    private void preencheDadosServico() {

        ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(
            getItemSelecionadoSolicitacao());

        ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
        .getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

        ConsultarManutencaoServicoContratoEntradaDTO consultarManutencaoServicoContratoEntradaDTO = new ConsultarManutencaoServicoContratoEntradaDTO();

        consultarManutencaoServicoContratoEntradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO
            .getCdPessoaJuridica());
        consultarManutencaoServicoContratoEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO
            .getCdTipoContrato());
        consultarManutencaoServicoContratoEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO
            .getCdTipoManutencaoContrato());
        consultarManutencaoServicoContratoEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO
            .getCdManutencao());
        consultarManutencaoServicoContratoEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO
            .getNrSequenciaContrato());

        setConsultarManutencaoServicoContratoSaidaDTO(new ConsultarManutencaoServicoContratoSaidaDTO());
        consultarManutencaoServicoContratoSaidaDTO = solManutContratosServiceImpl
        .detalharServicoContrato(consultarManutencaoServicoContratoEntradaDTO);

        if ("pagamentos integrados".equals(consultarManutencaoServicoContratoSaidaDTO.getDsServico().toLowerCase())) {
            setServico(consultarManutencaoServicoContratoSaidaDTO.getDsModalidade());
            setModalidade("");
        } else {
            setServico(consultarManutencaoServicoContratoSaidaDTO.getDsServico());
            setModalidade(consultarManutencaoServicoContratoSaidaDTO.getDsModalidade());
        }

        setSituacao(consultarManutencaoServicoContratoSaidaDTO.getDsSituacao());
        setMotivo(consultarManutencaoServicoContratoSaidaDTO.getDsMotivo());
        setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao());

        setUsuarioInclusao(consultarManutencaoServicoContratoSaidaDTO.getUsuarioInclusao());
        setUsuarioManutencao(consultarManutencaoServicoContratoSaidaDTO.getUsuarioManutencao());

        setComplementoInclusao(consultarManutencaoServicoContratoSaidaDTO.getComplementoInclusao().equals("0") ? ""
            : consultarManutencaoServicoContratoSaidaDTO.getComplementoInclusao());
        setComplementoManutencao(consultarManutencaoServicoContratoSaidaDTO.getComplementoManutencao().equals("0") ? ""
            : consultarManutencaoServicoContratoSaidaDTO.getComplementoManutencao());
        setTipoCanalInclusao(consultarManutencaoServicoContratoSaidaDTO.getCdCanalInclusao() == 0 ? ""
            : consultarManutencaoServicoContratoSaidaDTO.getCdCanalInclusao() + " - "
            + consultarManutencaoServicoContratoSaidaDTO.getDsCanalInclusao());
        setTipoCanalManutencao(consultarManutencaoServicoContratoSaidaDTO.getCdCanalManutencao() == 0 ? ""
            : consultarManutencaoServicoContratoSaidaDTO.getCdCanalManutencao() + " - "
            + consultarManutencaoServicoContratoSaidaDTO.getDsCanalManutencao());

        setDataHoraInclusao(consultarManutencaoServicoContratoSaidaDTO.getDataHoraInclusao());
        setDataHoraManutencao(consultarManutencaoServicoContratoSaidaDTO.getDataHoraManutencao());

    }

    /**
     * Preencher consulta manutencao config contrato.
     */
    private void preencherConsultaManutencaoConfigContrato() {

        ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(
            getItemSelecionadoSolicitacao());

        ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
        .getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

        ConsultarManutencaoConfigContratoEntradaDTO entradaDTO = new ConsultarManutencaoConfigContratoEntradaDTO();

        entradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
        entradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
        entradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato());
        entradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO.getCdManutencao());
        entradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());

        setSaidaConsultarManutencaoConfigContrato(new ConsultarManutencaoConfigContratoSaidaDTO());
        saidaConsultarManutencaoConfigContrato = 
            solManutContratosServiceImpl.consultarManutencaoConfigContrato(entradaDTO);

        setSituacao(saidaConsultarManutencaoConfigContrato.getDsSituacaoManutencaoContrato());
        setMotivo(saidaConsultarManutencaoConfigContrato.getDsMotivoSituacaoManutencao());
        setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao());


        if(!"".equals(saidaConsultarManutencaoConfigContrato.getCdUsuarioInclusao())){
            setUsuarioInclusao(saidaConsultarManutencaoConfigContrato.getCdUsuarioInclusao());
        }else{
            setUsuarioInclusao(saidaConsultarManutencaoConfigContrato.getCdUsuarioInclusaoExterno());
        }

        if(!"".equals(saidaConsultarManutencaoConfigContrato.getCdUsuarioManutencao())){
            setUsuarioManutencao(saidaConsultarManutencaoConfigContrato.getCdUsuarioManutencao());
        }else{
            setUsuarioManutencao(saidaConsultarManutencaoConfigContrato.getCdUsuarioManutencaoExterno());
        }

        setComplementoInclusao(saidaConsultarManutencaoConfigContrato.getCdOperacaoCanalInclusao().equals("0") ? ""
            : saidaConsultarManutencaoConfigContrato.getCdOperacaoCanalInclusao());

        setComplementoManutencao(saidaConsultarManutencaoConfigContrato.getCdOperacaoCanalManutencao().equals("0") ? ""
            : saidaConsultarManutencaoConfigContrato.getCdOperacaoCanalManutencao());

        setTipoCanalInclusao(saidaConsultarManutencaoConfigContrato.getCdTipoCanalInclusao() == 0 ? ""
            : saidaConsultarManutencaoConfigContrato.getCdTipoCanalInclusao() + " - "
            + saidaConsultarManutencaoConfigContrato.getDsTipoCanalInclusao());

        setTipoCanalManutencao("0".equals(saidaConsultarManutencaoConfigContrato.getCdTipoCanalManutencao()) ? ""
            : saidaConsultarManutencaoConfigContrato.
            getCdTipoCanalManutencao() + " - "
            + saidaConsultarManutencaoConfigContrato.getDsTipoCanalManutencao());

        setDataHoraInclusao(saidaConsultarManutencaoConfigContrato.getHrInclusaoRegistro());
        setDataHoraManutencao(saidaConsultarManutencaoConfigContrato.getHrManutencaoRegistro());

    }

    /**
     * Preenche dados meio transmissao.
     */
    private void preencheDadosMeioTransmissao(){


        ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(
            getItemSelecionadoSolicitacao());

        ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
        .getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

        ConsultarManutencaoMeioTransmissaoEntradaDTO  consultarManutencaoMeioTransmissaoEntradaDTO =  new ConsultarManutencaoMeioTransmissaoEntradaDTO();

        consultarManutencaoMeioTransmissaoEntradaDTO.setCdpessoaJuridicaContrato(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
        consultarManutencaoMeioTransmissaoEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
        consultarManutencaoMeioTransmissaoEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato());
        consultarManutencaoMeioTransmissaoEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO.getCdManutencao());
        consultarManutencaoMeioTransmissaoEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());

        ConsultarManutencaoMeioTransmissaoSaidaDTO consultarManutencaoMeioTransmissaoSaidaDTO = solManutContratosServiceImpl.consultarManutencaoMeioTransmissao(consultarManutencaoMeioTransmissaoEntradaDTO);

        setDsCodMeioPrincipalRemessa(consultarManutencaoMeioTransmissaoSaidaDTO.getDsCodMeioPrincipalRemessa());
        setCdTipoLayoutArquivo(consultarManutencaoMeioTransmissaoSaidaDTO.getCdTipoLayoutArquivo());
        setDsLayoutProprio(consultarManutencaoMeioTransmissaoSaidaDTO.getDsLayoutProprio());
        setDsAplicFormat(consultarManutencaoMeioTransmissaoSaidaDTO.getDsAplicFormat());
        setDsCodMeioAltrnRemessa(consultarManutencaoMeioTransmissaoSaidaDTO.getDsCodMeioAlternRemessa());
        setDsCodTipoLayout(consultarManutencaoMeioTransmissaoSaidaDTO.getDsCodTipoLayout());
        setCdCodMeioPrincipalRetorno(consultarManutencaoMeioTransmissaoSaidaDTO.getCdCodMeioPrincipalRetorno());
        setDsMeioAltrnRetorno(consultarManutencaoMeioTransmissaoSaidaDTO.getDsMeioAltrnRetorno());
        setDsUtilizacaoEmpresaVan(consultarManutencaoMeioTransmissaoSaidaDTO.getDsUtilizacaoEmpresaVan());
        setDsEmpresa(consultarManutencaoMeioTransmissaoSaidaDTO.getDsEmpresa());
        setDsResponsavelCustoEmpresa(consultarManutencaoMeioTransmissaoSaidaDTO.getDsResponsavelCustoEmpresa());
        setPcCustoOrganizacaoTransmissao(consultarManutencaoMeioTransmissaoSaidaDTO.getPcCustoOrganizacaoTransmissao());



        setSituacao(consultarManutencaoMeioTransmissaoSaidaDTO.getDsSituacaoManutContr());
        setMotivo(consultarManutencaoMeioTransmissaoSaidaDTO.getDsMotivoManuContr());
        setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao());

        setUsuarioInclusao(consultarManutencaoMeioTransmissaoSaidaDTO.getCdUsuarioInclusao());
        setUsuarioManutencao(consultarManutencaoMeioTransmissaoSaidaDTO.getCdUsuarioManutencao());

        setComplementoInclusao(consultarManutencaoMeioTransmissaoSaidaDTO.getCdOperacaoCanalInclusao().equals("0") ? ""
            : consultarManutencaoMeioTransmissaoSaidaDTO.getCdOperacaoCanalInclusao());
        setComplementoManutencao(consultarManutencaoMeioTransmissaoSaidaDTO.getCdOperacaoCanalManutencao().equals("0") ? ""
            : consultarManutencaoMeioTransmissaoSaidaDTO.getCdOperacaoCanalManutencao());
        setTipoCanalInclusao(consultarManutencaoMeioTransmissaoSaidaDTO.getCdTipoCanalInclusao() == 0 ? ""
            : consultarManutencaoMeioTransmissaoSaidaDTO.getCdTipoCanalInclusao() + " - "
            + consultarManutencaoMeioTransmissaoSaidaDTO.getDsCanalInclusao());
        setTipoCanalManutencao("0".equals(consultarManutencaoMeioTransmissaoSaidaDTO.getCdTipoCanalManutencao()) ? ""
            : consultarManutencaoMeioTransmissaoSaidaDTO.getCdTipoCanalManutencao() + " - "
            + consultarManutencaoMeioTransmissaoSaidaDTO.getDsCanalManutencao());

        setDataHoraInclusao(consultarManutencaoMeioTransmissaoSaidaDTO.getHrInclusaoRegistro());
        setDataHoraManutencao(consultarManutencaoMeioTransmissaoSaidaDTO.getHrManutencaoRegistro());

    }

    /**
     * Confirmar excluir solicitacao.
     *
     * @return the string
     */
    public String confirmarExcluirSolicitacao() {

        try {

            CancelarManutencaoEntradaDTO cancelarManutencaoEntradaDTO = new CancelarManutencaoEntradaDTO();

            ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(
                getItemSelecionadoSolicitacao());

            ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
            .getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

            cancelarManutencaoEntradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
            cancelarManutencaoEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
            cancelarManutencaoEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO
                .getCdTipoManutencaoContrato());
            cancelarManutencaoEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO
                .getCdManutencao());
            cancelarManutencaoEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO
                .getNrSequenciaContrato());

            CancelarManutencaoSaidaDTO cancelarManutencaoSaidaDTO = getSolManutContratosServiceImpl()
            .cancelarManutencao(cancelarManutencaoEntradaDTO);
            BradescoFacesUtils.addInfoModalMessage("(" + cancelarManutencaoSaidaDTO.getCodMensagem() + ") "
                + cancelarManutencaoSaidaDTO.getMensagem(), "conManterSolManutContrato2",
                BradescoViewExceptionActionType.ACTION, false);

            consultarSolicitacoes();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            setItemSelecionadoSolicitacao(null);
            setListaGridSolicitacao(null);
            return null;
        }
        return "";
    }

    /**
     * Solicitacoes.
     *
     * @return the string
     */
    public String solicitacoes() {
        carsregaListaSituacaoSolic();
        setItemSelecSituacao(null);

        limparGridSolicitacoes();

        ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
            identificacaoClienteContratoBean.getItemSelecionadoLista());

        setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
        setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
        setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
        setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
        setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
        setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
        setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
        setCdEmpresa(saidaDTO.getCdPessoaJuridica());
        setTipo(saidaDTO.getDsTipoContrato());
        setCdTipo(saidaDTO.getCdTipoContrato());
        setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
        setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
        setMotivoDesc(saidaDTO.getDsMotivoSituacao());
        setParticipacao(saidaDTO.getCdTipoParticipacao());
        setDescricaoContrato(saidaDTO.getDsContrato());
        setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
        setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
        setDtPeriodoInicio(new Date());
        setDtPeriodoFim(new Date());

        if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
                        && !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
                        .getCnpjOuCpfFormatado().equals("")) {
            setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
            setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

        } else {
            setCpfCnpj("");
            setNomeRazaoSocial("");
        }

        return "SOLICITACOES";

    }

    /**
     * Limpar grid solicitacoes.
     *
     * @return the string
     */
    public String limparGridSolicitacoes() {

        limparDadosSolicitacoes();
        setListaGridSolicitacao(null);
        setItemSelecionadoSolicitacao(null);

        return "ok";

    }

    /**
     * Is desabilita botao.
     *
     * @return true, if is desabilita botao
     */
    public boolean isDesabilitaBotao() {
        if (listaGridPesquisa != null) {
            if (listaGridPesquisa.isEmpty()) {
                return true; 
            } else if (itemSelecionadoLista != null) {
                if (listaGridPesquisa.get(itemSelecionadoLista).getCdSituacaoAditivoContrato() == 2 ||
                                listaGridPesquisa.get(itemSelecionadoLista).getCdSituacaoAditivoContrato() == 3) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    
    /**
     * Is desabilita botao imprimir versao contrato.
     *
     * @return true, if is desabilita botao imprimir versao contrato
     */
    public boolean isDesabilitaBotaoImprimirVersaoContrato() {
        if (listaGridPesquisa != null) {
            if (listaGridPesquisa.isEmpty()) {
                return true; 
            } else if (itemSelecionadoLista != null) {
                if (listaGridPesquisa.get(itemSelecionadoLista).getCdSituacaoAditivoContrato() != 1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    
    /**
     * Consultar solicitacoes.
     *
     * @return the string
     */
    public String consultarSolicitacoes() {
        setDesabilitaCamposPesquisa(false);
        try {

            ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
            .getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

            ListarManutencaoContratoEntradaDTO entradaDTO = new ListarManutencaoContratoEntradaDTO();

            entradaDTO.setCdTipoManutencaoContrato(getTipoManutencaoFiltro() != null ? getTipoManutencaoFiltro() : 0);
            entradaDTO.setCdAcao(getAcaoFiltro() != null ? getAcaoFiltro() : 0);
            entradaDTO.setCdPessoaJuridicaContrato(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
            entradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
            entradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());
            entradaDTO.setDtInicio(getDtPeriodoInicio());
            entradaDTO.setDtFim(getDtPeriodoFim());
            entradaDTO.setCdSituacaoManutencaoContrato(getSituacaoFiltro() != null ? getSituacaoFiltro() : 0);

            setListaGridSolicitacao(getSolManutContratosServiceImpl().listarManutencaoContrato(entradaDTO));

            listaControleSolicitacao = new ArrayList<SelectItem>();

            for (int i = 0; i < listaGridSolicitacao.size(); i++) {
                listaControleSolicitacao.add(new SelectItem(i, ""));
            }

            setItemSelecionadoSolicitacao(null);
            setDesabilitaCamposPesquisa(true);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            setListaGridSolicitacao(null);
            setItemSelecionadoSolicitacao(null);

        }

        return "";
    }

    /**
     * Limpar dados solicitacoes.
     *
     * @return the string
     */
    public String limparDadosSolicitacoes() {

        setTipoManutencaoFiltro(0);
        setAcaoFiltro(0);
        setSituacaoFiltro(0);
        setListaGridSolicitacao(null);
        setItemSelecionadoSolicitacao(null);
        setDesabilitaCamposPesquisa(false);

        return "ok";

    }

    /**
     * Voltar solicitacoes.
     *
     * @return the string
     */
    public String voltarSolicitacoes() {
        setItemSelecionadoSolicitacao(null);

        return "VOLTAR_PESQUISAR";

    }

    /**
     * Aditivos.
     *
     * @return the string
     */
    public String aditivos() {

        ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
            identificacaoClienteContratoBean.getItemSelecionadoLista());

        setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
        setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
        setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
        setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
        setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
        setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
        setEmpresaGestora(String.valueOf(saidaDTO.getDsPessoaJuridica()));
        setCdEmpresa(saidaDTO.getCdPessoaJuridica());
        setTipo(saidaDTO.getDsTipoContrato());
        setCdTipo(saidaDTO.getCdTipoContrato());
        setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
        setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
        setMotivoDesc(saidaDTO.getDsMotivoSituacao());
        setParticipacao(saidaDTO.getCdTipoParticipacao());
        setDescricao(saidaDTO.getDsContrato());
        setDsAgenciaOperadora(saidaDTO.getDsAgenciaOperadora());
        // add em 12/8/2010.
        setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
        setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

        setListaGridPesquisa(null);

        // Carrega combo Situacao
        setDataInicialPagamentoFiltro(new Date());
        setDataFinalPagamentoFiltro(new Date());
        carregaListaSituacaoFiltro();
        limparDados();
        setRadioSelecaoFiltro("2");
        setDesabilitaConsulta(false);
        limparCampos();

        if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
                        && !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
                        .getCnpjOuCpfFormatado().equals("")) {
            setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
            setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

        } else {
            setCpfCnpj("");
            setNomeRazaoSocial("");
        }

        return "FORMALIZAR";
    }

    /**
     * Preenche dados.
     */
    private void preencheDados() {
        // Preenche os dados
        ConsultarManterFormAltContratoSaidaDTO consultarManterFormAltContratoSaidaDTO = getListaGridPesquisa().get(
            getItemSelecionadoLista());

        DetalharManterFormAltContratoEntradaDTO entradaDTO = new DetalharManterFormAltContratoEntradaDTO();

        entradaDTO.setNrAditivoContratoNegocio(consultarManterFormAltContratoSaidaDTO
            .getNrAditivoContratoNegocio());

        /* Dados do Contrato */

        ListarContratosPgitSaidaDTO saidaDTOContrato = identificacaoClienteContratoBean.getListaGridPesquisa().get(
            identificacaoClienteContratoBean.getItemSelecionadoLista());

        entradaDTO.setCdPessoaJuridicaContrato(saidaDTOContrato.getCdPessoaJuridica());
        entradaDTO.setCdTipoContratoNegocio(saidaDTOContrato.getCdTipoContrato());
        entradaDTO.setNrSequenciaContratoNegocio(saidaDTOContrato.getNrSequenciaContrato());

        /* Dados de Saida */
        setSaidaDetalharManterFormAltContrato(new DetalharManterFormAltContratoSaidaDTO());
        saidaDetalharManterFormAltContrato = getFormalizacaoAltContratoImpl()
        .detalharManterFormAltContratoSaidaDTO(entradaDTO);

        setDtHraCadastramento(FormatarData.formatarDataTrilha(saidaDetalharManterFormAltContrato.getHrInclusaoRegistro()));
        setDtHraAssinatura(FormatarData.formatarDataTrilha(saidaDetalharManterFormAltContrato.getHrAssinaturaAditivoContrato()));
        // setDsSituacaoFormalizacao((String) getListaSituacaoFiltroHash().get(getCodigoSituacaoFiltro()));
        setDsSituacaoFormalizacao(consultarManterFormAltContratoSaidaDTO.getDsSitucaoAditivoContrato());
        setNumeroFormalizacao(String.valueOf(consultarManterFormAltContratoSaidaDTO.getNrAditivoContratoNegocio()));

        /* Trilha de Auditoria */

        setDataHoraManutencao(saidaDetalharManterFormAltContrato.getHrManutencaoRegistro());
        setDataHoraInclusao(saidaDetalharManterFormAltContrato.getHrInclusaoRegistroTrilha());

        setTipoCanalInclusao(saidaDetalharManterFormAltContrato.getCdTipoCanalInclusaoTrilha() != 0 ? saidaDetalharManterFormAltContrato.getCdTipoCanalInclusaoTrilha() + " - "
            + saidaDetalharManterFormAltContrato.getDsTipoCanalInclusaoTrilha() : "");
        setTipoCanalManutencao(saidaDetalharManterFormAltContrato.getCdTipoCanalManutencao() == 0 ? "" : saidaDetalharManterFormAltContrato.getCdTipoCanalManutencao()
            + " - " + saidaDetalharManterFormAltContrato.getDsTipoCanalManutencao());

        setUsuarioInclusao(saidaDetalharManterFormAltContrato.getCdUsuarioInclusao());
        setUsuarioManutencao(saidaDetalharManterFormAltContrato.getCdUsuarioManutencao());

        setComplementoInclusao(saidaDetalharManterFormAltContrato.getCdOperacaoCanalManutencao() == null || saidaDetalharManterFormAltContrato.getDsTipoCanalInclusaoTrilha().equals("0") ? ""
            : saidaDetalharManterFormAltContrato.getDsTipoCanalInclusaoTrilha());
        setComplementoManutencao(saidaDetalharManterFormAltContrato.getCdOperacaoCanalManutencao() == null
            || saidaDetalharManterFormAltContrato.getDsTipoCanalManutencao().equals("0") ? "" : saidaDetalharManterFormAltContrato.getDsTipoCanalManutencao());

        setListaGridParticipantes(saidaDetalharManterFormAltContrato.getListaParticipantes());
        setListaGridVinculos(saidaDetalharManterFormAltContrato.getListaVinculos());
        setListaGridServicos(saidaDetalharManterFormAltContrato.getListaServicos());
        setListaGridMeioTransmissao(saidaDetalharManterFormAltContrato.getListaPerfis());

        // Seta Atributos para controlar quantidade de registrosna grid sem pagina��o
        setQtdeParticipantes(saidaDetalharManterFormAltContrato.getNumeroOcorrenciasParticipante() != null ? saidaDetalharManterFormAltContrato
            .getNumeroOcorrenciasParticipante() : 0);
        setQtdeVinculos(saidaDetalharManterFormAltContrato.getNumeroOcorrenciasVinculacao() != null ? saidaDetalharManterFormAltContrato.getNumeroOcorrenciasVinculacao()
            : 0);
        setQtdeGridServicos(saidaDetalharManterFormAltContrato.getNumeroOcorrenciasServicos() != null ? saidaDetalharManterFormAltContrato.getNumeroOcorrenciasServicos()
            : 0);

        setQtdPerfis(saidaDetalharManterFormAltContrato.getNrOcorrenciasPerfil() != null ? saidaDetalharManterFormAltContrato.getNrOcorrenciasPerfil():0);

    }

    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {

        try {
            preencheDados();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                FOR_MANTER_FORMALIZACAO_ALTERACAO_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
            return null;
        }
        return "DETALHAR";
    }

    /**
     * Incluir.
     *
     * @return the string
     */
    public String incluir() {
        setRadioInstrucao(null);
        setExisteDadosIncluir(false);

        return preencheIncluir();
    }

    /**
     * Preenche incluir.
     *
     * @return the string
     */
    public String preencheIncluir() {
        listaGridParticipantes = new ArrayList<ParticipantesDTO>();
        listaGridVinculosIncluir = new ArrayList<VinculoRelacaoConManutencoesDTO>();
        listaGridServicos = new ArrayList<ServicosDTO>();
        listaGridMeioTransmissao = new ArrayList<PerfilDTO>();
        try {
            ConRelacaoConManutencoesEntradaDTO entrada = new ConRelacaoConManutencoesEntradaDTO();

            entrada.setCdPessoaJuridicaContrato(getCdEmpresa());
            entrada.setCdTipoContratoNegocio(getCdTipo());
            entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumero()));

            setSaidaConRelacaoConManutencoes(new ConRelacaoConManutencoesSaidaDTO());
            saidaConRelacaoConManutencoes = getFormalizacaoAltContratoImpl().conRelacaoConManutencoes(entrada);
            setExisteDadosIncluir(true);
            setListaGridParticipantesIncluir(saidaConRelacaoConManutencoes.getListaParticipantes());
            setListaGridVinculosIncluir(saidaConRelacaoConManutencoes.getListaVinculos());
            setListaGridServicos(saidaConRelacaoConManutencoes.getListaServicos());
            setListaGridMeioTransmissao(saidaConRelacaoConManutencoes.getListaPerfis());

            // Seta Atributos para controlar quantidade de registrosna grid sem pagina��o
            setQtdeParticipantes(saidaConRelacaoConManutencoes.getNumeroOcorrenciasParticipante() != null ? saidaConRelacaoConManutencoes
                .getNumeroOcorrenciasParticipante() : 0);
            setQtdeVinculos(saidaConRelacaoConManutencoes.getNumeroOcorrenciasVinculacao() != null ? saidaConRelacaoConManutencoes.getNumeroOcorrenciasVinculacao() : 0);
            setQtdeGridServicos(saidaConRelacaoConManutencoes.getNumeroOcorrenciasServicos() != null ? saidaConRelacaoConManutencoes.getNumeroOcorrenciasServicos() : 0);
            setQtdPerfis(saidaConRelacaoConManutencoes.getNrOcorrenciasPerfil() != null ? saidaConRelacaoConManutencoes.getNrOcorrenciasPerfil():0);

            return "incConfManterForAltContrato";
        } catch (PdcAdapterFunctionalException p) {
            listaGridParticipantes = new ArrayList<ParticipantesDTO>();
            listaGridVinculosIncluir = new ArrayList<VinculoRelacaoConManutencoesDTO>();
            listaGridServicos = new ArrayList<ServicosDTO>();
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                "", BradescoViewExceptionActionType.ACTION, false);

            return "";
        }
    }

    /**
     * Excluir.
     *
     * @return the string
     */
    public String excluir() {

        try {
            preencheDados();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                FOR_MANTER_FORMALIZACAO_ALTERACAO_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
            return null;
        }
        return "EXCLUIR";
    }

    /**
     * Cancelar solicitacao.
     *
     * @return the string
     */
    public String cancelarSolicitacao() {

        ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(
            getItemSelecionadoSolicitacao());

        try {

            // seta label�s com campos do Grid para ser exibido no Excluir
            setDsTipoManutencao(listarManutencaoContratoSaidaDTO.getDsTipoManutencaoContrato());
            setHrManutencaoRegistro(listarManutencaoContratoSaidaDTO.getHrManutencaoRegistro());

            if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 1) {
                setIndicadorTipoManutencao(1);
                preencheDadosParticipantes();
            } else if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 2) {
                setIndicadorTipoManutencao(2);
                preencheDadosConta();
            } else if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 3) {
                setIndicadorTipoManutencao(3);
                preencheDadosServico();
            } else if( listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 4) {
                setIndicadorTipoManutencao(4);
                preencheDadosMeioTransmissao();
            } else if( listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 6) {
                setIndicadorTipoManutencao(6);
                preencherConsultaManutencaoConfigContrato();
            } else if( listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 9) {
                setIndicadorTipoManutencao(9);
                preencherConsultaManutencaoConfigContrato();
            }else{
                return "";
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                "conManterSolManutContrato2", BradescoViewExceptionActionType.ACTION, false);
            return null;
        }
        return "EXCLUIR";
    }

    /**
     * Voltar detalhar.
     *
     * @return the string
     */
    public String voltarDetalhar() {
        setItemSelecionadoLista(null);
        return "VOLTAR_DET";
    }

    /**
     * Voltar formalizacao.
     *
     * @return the string
     */
    public String voltarFormalizacao() {
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);
        
        getIdentificacaoClienteContratoBean().consultarContrato();
        
        return "VOLTAR_FOR";
    }

    /**
     * Voltar inc.
     *
     * @return the string
     */
    public String voltarInc() {
        setItemSelecionadoLista(null);
        return "forManterFormalizacaoAlteracaoContrato";
    }

    /**
     * Voltar imp aditivo.
     *
     * @return the string
     */
    public String voltarImpAditivo(){
        setItemSelecionadoLista(null);
        return FOR_MANTER_FORMALIZACAO_ALTERACAO_CONTRATO;
    }	

    /**
     * Voltar consultar.
     *
     * @return the string
     */
    public String voltarConsultar() {
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);
        return "VOLTAR_CONSULTAR";
    }

    /**
     * Voltar exc.
     *
     * @return the string
     */
    public String voltarExc() {
        setItemSelecionadoLista(null);
        return "VOLTAR_EXC";
    }

    /**
     * Voltar inc conf.
     *
     * @return the string
     */
    public String voltarIncConf() {
        return "VOLTAR_INC_CONF";
    }

    /**
     * Avancar inc.
     *
     * @return the string
     */
    public String avancarInc() {
        return "CONFIRMAR_INC";
    }

    /**
     * Confirmar excluir.
     *
     * @return the string
     */
    public String confirmarExcluir() {
        try {
            ExcluirManterFormAltContratoEntradaDTO entrada = atribuirEntradaExcluir();

            ExcluirManterFormAltContratoSaidaDTO saida = getFormalizacaoAltContratoImpl().excluirManterFormAltContratoSaidaDTO(entrada);
            BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
                "#{manterFormalizacaoAlteracaoContratoBean.limparDadosConfExc}", BradescoViewExceptionActionType.ACTION, false);

            return "";
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
            return null;
        }
    }

    /**
     * Atribuir entrada excluir.
     *
     * @return the excluir manter form alt contrato entrada dto
     */
    private ExcluirManterFormAltContratoEntradaDTO atribuirEntradaExcluir() {
        ExcluirManterFormAltContratoEntradaDTO entrada = new ExcluirManterFormAltContratoEntradaDTO();
        ConsultarManterFormAltContratoSaidaDTO itemSelecionado = listaGridPesquisa.get(itemSelecionadoLista);

        /* Dados do N�mero da Formaliza��o */
        entrada.setNumAditivoContratoNegocio(itemSelecionado.getNrAditivoContratoNegocio());

        /* Dados Contratos */
        List<ListarContratosPgitSaidaDTO> listaGridPesquisa = identificacaoClienteContratoBean.getListaGridPesquisa();
        ListarContratosPgitSaidaDTO itemSelecinadoCliente = listaGridPesquisa.get(identificacaoClienteContratoBean.getItemSelecionadoLista());

        entrada.setCodPessoaJuridica(itemSelecinadoCliente.getCdPessoaJuridica());
        entrada.setCodTipoContratoNegocio(itemSelecinadoCliente.getCdTipoContrato());
        entrada.setNumSeqContratoNegocio(itemSelecinadoCliente.getNrSequenciaContrato());
        return entrada;
    }

    /**
     * Limpar dados conf exc.
     *
     * @return the string
     */
    public String limparDadosConfExc() {
        carregaLista();
        return FOR_MANTER_FORMALIZACAO_ALTERACAO_CONTRATO;
    }

    /**
     * Confirmar incluir.
     *
     * @return the string
     */
    public String confirmarIncluir() {

        try {

            IncluirManterFormAltContratoEntradaDTO incluirManterFormAltContratoEntradaDTO = new IncluirManterFormAltContratoEntradaDTO();

            /* Dados Contratos */
            ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
                identificacaoClienteContratoBean.getItemSelecionadoLista());

            incluirManterFormAltContratoEntradaDTO.setCodPessoaJuridicaContrato(Integer.parseInt(String
                .valueOf(saidaDTO.getCdPessoaJuridica())));
            incluirManterFormAltContratoEntradaDTO.setCodTipoContratoNegocio(saidaDTO.getCdTipoContrato());
            incluirManterFormAltContratoEntradaDTO.setNumSeqContratoNegocio(saidaDTO.getNrSequenciaContrato());

            /* Dados da Instru��o do Contrato */
            incluirManterFormAltContratoEntradaDTO.setCodIndicadorNovoContrato(1);

            incluirManterFormAltContratoSaidaDTO = getFormalizacaoAltContratoImpl().incluirManterFormAltContratoSaidaDTO(incluirManterFormAltContratoEntradaDTO);

            setMensagem(incluirManterFormAltContratoSaidaDTO.getMensagem());

            carregaLista();
            setItemSelecionadoLista(null);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            return null;
        }

        return "IMPRIMIR_INC";
    }

    /**
     * Limpar pagina.
     *
     * @param evt the evt
     */
    public void limparPagina(ActionEvent evt) {

        identificacaoClienteContratoBean
        .setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
        identificacaoClienteContratoBean
        .setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
        identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

        // carrega os combos da tela de pesquisa
        identificacaoClienteContratoBean.listarEmpresaGestora();
        identificacaoClienteContratoBean.listarTipoContrato();
        identificacaoClienteContratoBean.listarSituacaoContrato();

        identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
        identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterForAltContrato");
        identificacaoClienteContratoBean.setPaginaRetorno("conManterForAltContrato");
        identificacaoClienteContratoBean.setItemClienteSelecionado(null);
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);

        identificacaoClienteContratoBean.setItemFiltroSelecionado("");
        identificacaoClienteContratoBean.setBloqueiaRadio(false);

        identificacaoClienteContratoBean.limparDadosPesquisaContrato();
        identificacaoClienteContratoBean.limparDadosCliente();
        identificacaoClienteContratoBean.setObrigatoriedade("T");
        identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
        identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

        setDesabilitaCamposPesquisa(false);

    }

    /**
     * Get: comboService.
     *
     * @return comboService
     */
    public IComboService getComboService() {
        return comboService;
    }

    /**
     * Set: comboService.
     *
     * @param comboService the combo service
     */
    public void setComboService(IComboService comboService) {
        this.comboService = comboService;
    }

    /**
     * Get: codigoSituacaoFiltro.
     *
     * @return codigoSituacaoFiltro
     */
    public Integer getCodigoSituacaoFiltro() {
        return codigoSituacaoFiltro;
    }

    /**
     * Set: codigoSituacaoFiltro.
     *
     * @param codigoSituacaoFiltro the codigo situacao filtro
     */
    public void setCodigoSituacaoFiltro(Integer codigoSituacaoFiltro) {
        this.codigoSituacaoFiltro = codigoSituacaoFiltro;
    }

    /**
     * Get: numeroFiltro.
     *
     * @return numeroFiltro
     */
    public String getNumeroFiltro() {
        return numeroFiltro;
    }

    /**
     * Set: numeroFiltro.
     *
     * @param numeroFiltro the numero filtro
     */
    public void setNumeroFiltro(String numeroFiltro) {
        this.numeroFiltro = numeroFiltro;
    }

    /**
     * Get: dataInicialPagamentoFiltro.
     *
     * @return dataInicialPagamentoFiltro
     */
    public Date getDataInicialPagamentoFiltro() {
        return dataInicialPagamentoFiltro;
    }

    /**
     * Set: dataInicialPagamentoFiltro.
     *
     * @param dataInicialPagamentoFiltro the data inicial pagamento filtro
     */
    public void setDataInicialPagamentoFiltro(Date dataInicialPagamentoFiltro) {
        this.dataInicialPagamentoFiltro = dataInicialPagamentoFiltro;
    }

    /**
     * Get: dataFinalPagamentoFiltro.
     *
     * @return dataFinalPagamentoFiltro
     */
    public Date getDataFinalPagamentoFiltro() {
        return dataFinalPagamentoFiltro;
    }

    /**
     * Set: dataFinalPagamentoFiltro.
     *
     * @param dataFinalPagamentoFiltro the data final pagamento filtro
     */
    public void setDataFinalPagamentoFiltro(Date dataFinalPagamentoFiltro) {
        this.dataFinalPagamentoFiltro = dataFinalPagamentoFiltro;
    }

    /**
     * Get: itemSelecionadoLista.
     *
     * @return itemSelecionadoLista
     */
    public Integer getItemSelecionadoLista() {
        return itemSelecionadoLista;
    }

    /**
     * Set: itemSelecionadoLista.
     *
     * @param itemSelecionadoLista the item selecionado lista
     */
    public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
        this.itemSelecionadoLista = itemSelecionadoLista;
    }

    /**
     * Get: listaControleRadio.
     *
     * @return listaControleRadio
     */
    public List<SelectItem> getListaControleRadio() {
        return listaControleRadio;
    }

    /**
     * Set: listaControleRadio.
     *
     * @param listaControleRadio the lista controle radio
     */
    public void setListaControleRadio(List<SelectItem> listaControleRadio) {
        this.listaControleRadio = listaControleRadio;
    }

    /**
     * Get: listaGridPesquisa.
     *
     * @return listaGridPesquisa
     */
    public List<ConsultarManterFormAltContratoSaidaDTO> getListaGridPesquisa() {
        return listaGridPesquisa;
    }

    /**
     * Set: listaGridPesquisa.
     *
     * @param listaGridPesquisa the lista grid pesquisa
     */
    public void setListaGridPesquisa(List<ConsultarManterFormAltContratoSaidaDTO> listaGridPesquisa) {
        this.listaGridPesquisa = listaGridPesquisa;
    }

    /**
     * Get: identificacaoClienteContratoBean.
     *
     * @return identificacaoClienteContratoBean
     */
    public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
        return identificacaoClienteContratoBean;
    }

    /**
     * Set: identificacaoClienteContratoBean.
     *
     * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
     */
    public void setIdentificacaoClienteContratoBean(IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
        this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
    }

    /**
     * Get: acao.
     *
     * @return acao
     */
    public String getAcao() {
        return acao;
    }

    /**
     * Set: acao.
     *
     * @param acao the acao
     */
    public void setAcao(String acao) {
        this.acao = acao;
    }

    /**
     * Get: agencia.
     *
     * @return agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Set: agencia.
     *
     * @param agencia the agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Get: atividadeEconomicaMaster.
     *
     * @return atividadeEconomicaMaster
     */
    public String getAtividadeEconomicaMaster() {
        return atividadeEconomicaMaster;
    }

    /**
     * Set: atividadeEconomicaMaster.
     *
     * @param atividadeEconomicaMaster the atividade economica master
     */
    public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
        this.atividadeEconomicaMaster = atividadeEconomicaMaster;
    }

    /**
     * Get: banco.
     *
     * @return banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Set: banco.
     *
     * @param banco the banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * Get: cdEmpresa.
     *
     * @return cdEmpresa
     */
    public Long getCdEmpresa() {
        return cdEmpresa;
    }

    /**
     * Set: cdEmpresa.
     *
     * @param cdEmpresa the cd empresa
     */
    public void setCdEmpresa(Long cdEmpresa) {
        this.cdEmpresa = cdEmpresa;
    }

    /**
     * Get: cdTipo.
     *
     * @return cdTipo
     */
    public Integer getCdTipo() {
        return cdTipo;
    }

    /**
     * Set: cdTipo.
     *
     * @param cdTipo the cd tipo
     */
    public void setCdTipo(Integer cdTipo) {
        this.cdTipo = cdTipo;
    }

    /**
     * Get: conta.
     *
     * @return conta
     */
    public String getConta() {
        return conta;
    }

    /**
     * Set: conta.
     *
     * @param conta the conta
     */
    public void setConta(String conta) {
        this.conta = conta;
    }

    /**
     * Get: cpfCnpjMaster.
     *
     * @return cpfCnpjMaster
     */
    public String getCpfCnpjMaster() {
        return cpfCnpjMaster;
    }

    /**
     * Set: cpfCnpjMaster.
     *
     * @param cpfCnpjMaster the cpf cnpj master
     */
    public void setCpfCnpjMaster(String cpfCnpjMaster) {
        this.cpfCnpjMaster = cpfCnpjMaster;
    }

    /**
     * Get: cpfCnpjTitular.
     *
     * @return cpfCnpjTitular
     */
    public String getCpfCnpjTitular() {
        return cpfCnpjTitular;
    }

    /**
     * Set: cpfCnpjTitular.
     *
     * @param cpfCnpjTitular the cpf cnpj titular
     */
    public void setCpfCnpjTitular(String cpfCnpjTitular) {
        this.cpfCnpjTitular = cpfCnpjTitular;
    }

    /**
     * Get: cpfParticipante.
     *
     * @return cpfParticipante
     */
    public String getCpfParticipante() {
        return cpfParticipante;
    }

    /**
     * Set: cpfParticipante.
     *
     * @param cpfParticipante the cpf participante
     */
    public void setCpfParticipante(String cpfParticipante) {
        this.cpfParticipante = cpfParticipante;
    }

    /**
     * Get: dataNascimentoConstituicao.
     *
     * @return dataNascimentoConstituicao
     */
    public String getDataNascimentoConstituicao() {
        return dataNascimentoConstituicao;
    }

    /**
     * Set: dataNascimentoConstituicao.
     *
     * @param dataNascimentoConstituicao the data nascimento constituicao
     */
    public void setDataNascimentoConstituicao(String dataNascimentoConstituicao) {
        this.dataNascimentoConstituicao = dataNascimentoConstituicao;
    }

    /**
     * Get: empresaGestora.
     *
     * @return empresaGestora
     */
    public String getEmpresaGestora() {
        return empresaGestora;
    }

    /**
     * Set: empresaGestora.
     *
     * @param empresaGestora the empresa gestora
     */
    public void setEmpresaGestora(String empresaGestora) {
        this.empresaGestora = empresaGestora;
    }

    /**
     * Get: grupoEconomicoMaster.
     *
     * @return grupoEconomicoMaster
     */
    public String getGrupoEconomicoMaster() {
        return grupoEconomicoMaster;
    }

    /**
     * Set: grupoEconomicoMaster.
     *
     * @param grupoEconomicoMaster the grupo economico master
     */
    public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
        this.grupoEconomicoMaster = grupoEconomicoMaster;
    }

    /**
     * Get: grupoEconomicoParticipante.
     *
     * @return grupoEconomicoParticipante
     */
    public String getGrupoEconomicoParticipante() {
        return grupoEconomicoParticipante;
    }

    /**
     * Set: grupoEconomicoParticipante.
     *
     * @param grupoEconomicoParticipante the grupo economico participante
     */
    public void setGrupoEconomicoParticipante(String grupoEconomicoParticipante) {
        this.grupoEconomicoParticipante = grupoEconomicoParticipante;
    }

    /**
     * Get: modalidade.
     *
     * @return modalidade
     */
    public String getModalidade() {
        return modalidade;
    }

    /**
     * Set: modalidade.
     *
     * @param modalidade the modalidade
     */
    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    /**
     * Get: motivo.
     *
     * @return motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * Set: motivo.
     *
     * @param motivo the motivo
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    /**
     * Get: motivoDesc.
     *
     * @return motivoDesc
     */
    public String getMotivoDesc() {
        return motivoDesc;
    }

    /**
     * Set: motivoDesc.
     *
     * @param motivoDesc the motivo desc
     */
    public void setMotivoDesc(String motivoDesc) {
        this.motivoDesc = motivoDesc;
    }

    /**
     * Get: nomeParticipante.
     *
     * @return nomeParticipante
     */
    public String getNomeParticipante() {
        return nomeParticipante;
    }

    /**
     * Set: nomeParticipante.
     *
     * @param nomeParticipante the nome participante
     */
    public void setNomeParticipante(String nomeParticipante) {
        this.nomeParticipante = nomeParticipante;
    }

    /**
     * Get: nomeRazaoSocialConta.
     *
     * @return nomeRazaoSocialConta
     */
    public String getNomeRazaoSocialConta() {
        return nomeRazaoSocialConta;
    }

    /**
     * Set: nomeRazaoSocialConta.
     *
     * @param nomeRazaoSocialConta the nome razao social conta
     */
    public void setNomeRazaoSocialConta(String nomeRazaoSocialConta) {
        this.nomeRazaoSocialConta = nomeRazaoSocialConta;
    }

    /**
     * Get: nomeRazaoSocialMaster.
     *
     * @return nomeRazaoSocialMaster
     */
    public String getNomeRazaoSocialMaster() {
        return nomeRazaoSocialMaster;
    }

    /**
     * Set: nomeRazaoSocialMaster.
     *
     * @param nomeRazaoSocialMaster the nome razao social master
     */
    public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
        this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
    }

    /**
     * Get: numero.
     *
     * @return numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Set: numero.
     *
     * @param numero the numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Get: participacao.
     *
     * @return participacao
     */
    public String getParticipacao() {
        return participacao;
    }

    /**
     * Set: participacao.
     *
     * @param participacao the participacao
     */
    public void setParticipacao(String participacao) {
        this.participacao = participacao;
    }

    /**
     * Get: segmentoMaster.
     *
     * @return segmentoMaster
     */
    public String getSegmentoMaster() {
        return segmentoMaster;
    }

    /**
     * Set: segmentoMaster.
     *
     * @param segmentoMaster the segmento master
     */
    public void setSegmentoMaster(String segmentoMaster) {
        this.segmentoMaster = segmentoMaster;
    }

    /**
     * Get: servico.
     *
     * @return servico
     */
    public String getServico() {
        return servico;
    }

    /**
     * Set: servico.
     *
     * @param servico the servico
     */
    public void setServico(String servico) {
        this.servico = servico;
    }

    /**
     * Get: situacao.
     *
     * @return situacao
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Set: situacao.
     *
     * @param situacao the situacao
     */
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * Get: situacaoDesc.
     *
     * @return situacaoDesc
     */
    public String getSituacaoDesc() {
        return situacaoDesc;
    }

    /**
     * Set: situacaoDesc.
     *
     * @param situacaoDesc the situacao desc
     */
    public void setSituacaoDesc(String situacaoDesc) {
        this.situacaoDesc = situacaoDesc;
    }

    /**
     * Get: subSegmentoMaster.
     *
     * @return subSegmentoMaster
     */
    public String getSubSegmentoMaster() {
        return subSegmentoMaster;
    }

    /**
     * Set: subSegmentoMaster.
     *
     * @param subSegmentoMaster the sub segmento master
     */
    public void setSubSegmentoMaster(String subSegmentoMaster) {
        this.subSegmentoMaster = subSegmentoMaster;
    }

    /**
     * Get: tipo.
     *
     * @return tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set: tipo.
     *
     * @param tipo the tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Get: tipoConta.
     *
     * @return tipoConta
     */
    public String getTipoConta() {
        return tipoConta;
    }

    /**
     * Set: tipoConta.
     *
     * @param tipoConta the tipo conta
     */
    public void setTipoConta(String tipoConta) {
        this.tipoConta = tipoConta;
    }

    /**
     * Get: tipoParticipacaoContrato.
     *
     * @return tipoParticipacaoContrato
     */
    public String getTipoParticipacaoContrato() {
        return tipoParticipacaoContrato;
    }

    /**
     * Set: tipoParticipacaoContrato.
     *
     * @param tipoParticipacaoContrato the tipo participacao contrato
     */
    public void setTipoParticipacaoContrato(String tipoParticipacaoContrato) {
        this.tipoParticipacaoContrato = tipoParticipacaoContrato;
    }

    /**
     * Get: listaSituacaoFiltro.
     *
     * @return listaSituacaoFiltro
     */
    public List<SelectItem> getListaSituacaoFiltro() {
        return listaSituacaoFiltro;
    }

    /**
     * Set: listaSituacaoFiltro.
     *
     * @param listaSituacaoFiltro the lista situacao filtro
     */
    public void setListaSituacaoFiltro(List<SelectItem> listaSituacaoFiltro) {
        this.listaSituacaoFiltro = listaSituacaoFiltro;
    }

    /**
     * Get: listaSituacaoFiltroHash.
     *
     * @return listaSituacaoFiltroHash
     */
    public Map<Integer, String> getListaSituacaoFiltroHash() {
        return listaSituacaoFiltroHash;
    }

    /**
     * Set lista situacao filtro hash.
     *
     * @param listaSituacaoFiltroHash the lista situacao filtro hash
     */
    public void setListaSituacaoFiltroHash(Map<Integer, String> listaSituacaoFiltroHash) {
        this.listaSituacaoFiltroHash = listaSituacaoFiltroHash;
    }

    /**
     * Get: radioInstrucao.
     *
     * @return radioInstrucao
     */
    public Integer getRadioInstrucao() {
        return radioInstrucao;
    }

    /**
     * Set: radioInstrucao.
     *
     * @param radioInstrucao the radio instrucao
     */
    public void setRadioInstrucao(Integer radioInstrucao) {
        this.radioInstrucao = radioInstrucao;
    }

    /**
     * Get: descricao.
     *
     * @return descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Set: descricao.
     *
     * @param descricao the descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Get: dsSituacaoFormalizacao.
     *
     * @return dsSituacaoFormalizacao
     */
    public String getDsSituacaoFormalizacao() {
        return dsSituacaoFormalizacao;
    }

    /**
     * Set: dsSituacaoFormalizacao.
     *
     * @param dsSituacaoFormalizacao the ds situacao formalizacao
     */
    public void setDsSituacaoFormalizacao(String dsSituacaoFormalizacao) {
        this.dsSituacaoFormalizacao = dsSituacaoFormalizacao;
    }

    /**
     * Get: dtHraAssinatura.
     *
     * @return dtHraAssinatura
     */
    public String getDtHraAssinatura() {
        return dtHraAssinatura;
    }

    /**
     * Set: dtHraAssinatura.
     *
     * @param dtHraAssinatura the dt hra assinatura
     */
    public void setDtHraAssinatura(String dtHraAssinatura) {
        this.dtHraAssinatura = dtHraAssinatura;
    }

    /**
     * Get: dtHraCadastramento.
     *
     * @return dtHraCadastramento
     */
    public String getDtHraCadastramento() {
        return dtHraCadastramento;
    }

    /**
     * Set: dtHraCadastramento.
     *
     * @param dtHraCadastramento the dt hra cadastramento
     */
    public void setDtHraCadastramento(String dtHraCadastramento) {
        this.dtHraCadastramento = dtHraCadastramento;
    }

    /**
     * Get: numeroFormalizacao.
     *
     * @return numeroFormalizacao
     */
    public String getNumeroFormalizacao() {
        return numeroFormalizacao;
    }

    /**
     * Set: numeroFormalizacao.
     *
     * @param numeroFormalizacao the numero formalizacao
     */
    public void setNumeroFormalizacao(String numeroFormalizacao) {
        this.numeroFormalizacao = numeroFormalizacao;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
        return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
        this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
        return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
        this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
        return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
        this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
        this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
        this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
        return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
        return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
        this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: radioSelecaoFiltro.
     *
     * @return radioSelecaoFiltro
     */
    public String getRadioSelecaoFiltro() {
        return radioSelecaoFiltro;
    }

    /**
     * Set: radioSelecaoFiltro.
     *
     * @param radioSelecaoFiltro the radio selecao filtro
     */
    public void setRadioSelecaoFiltro(String radioSelecaoFiltro) {
        this.radioSelecaoFiltro = radioSelecaoFiltro;
    }

    /**
     * Get: listaGridParticipantes.
     *
     * @return listaGridParticipantes
     */
    public List<ParticipantesDTO> getListaGridParticipantes() {
        return listaGridParticipantes;
    }

    /**
     * Set: listaGridParticipantes.
     *
     * @param listaGridParticipantes the lista grid participantes
     */
    public void setListaGridParticipantes(List<ParticipantesDTO> listaGridParticipantes) {
        this.listaGridParticipantes = listaGridParticipantes;
    }

    /**
     * Get: listaGridVinculos.
     *
     * @return listaGridVinculos
     */
    public List<VinculoDTO> getListaGridVinculos() {
        return listaGridVinculos;
    }

    /**
     * Set: listaGridVinculos.
     *
     * @param listaGridVinculos the lista grid vinculos
     */
    public void setListaGridVinculos(List<VinculoDTO> listaGridVinculos) {
        this.listaGridVinculos = listaGridVinculos;
    }

    /**
     * Get: listaGridServicos.
     *
     * @return listaGridServicos
     */
    public List<ServicosDTO> getListaGridServicos() {
        return listaGridServicos;
    }

    /**
     * Set: listaGridServicos.
     *
     * @param listaGridServicos the lista grid servicos
     */
    public void setListaGridServicos(List<ServicosDTO> listaGridServicos) {
        this.listaGridServicos = listaGridServicos;
    }

    /**
     * Is desabilita consulta.
     *
     * @return true, if is desabilita consulta
     */
    public boolean isDesabilitaConsulta() {
        return desabilitaConsulta;
    }

    /**
     * Set: desabilitaConsulta.
     *
     * @param desabilitaConsulta the desabilita consulta
     */
    public void setDesabilitaConsulta(boolean desabilitaConsulta) {
        this.desabilitaConsulta = desabilitaConsulta;
    }

    /**
     * Get: listaGridParticipantesIncluir.
     *
     * @return listaGridParticipantesIncluir
     */
    public List<ParticipantesRelacaoConManutencoesDTO> getListaGridParticipantesIncluir() {
        return listaGridParticipantesIncluir;
    }

    /**
     * Set: listaGridParticipantesIncluir.
     *
     * @param listaGridParticipantesIncluir the lista grid participantes incluir
     */
    public void setListaGridParticipantesIncluir(
        List<ParticipantesRelacaoConManutencoesDTO> listaGridParticipantesIncluir) {
        this.listaGridParticipantesIncluir = listaGridParticipantesIncluir;
    }

    /**
     * Get: listaGridVinculosIncluir.
     *
     * @return listaGridVinculosIncluir
     */
    public List<VinculoRelacaoConManutencoesDTO> getListaGridVinculosIncluir() {
        return listaGridVinculosIncluir;
    }

    /**
     * Set: listaGridVinculosIncluir.
     *
     * @param listaGridVinculosIncluir the lista grid vinculos incluir
     */
    public void setListaGridVinculosIncluir(List<VinculoRelacaoConManutencoesDTO> listaGridVinculosIncluir) {
        this.listaGridVinculosIncluir = listaGridVinculosIncluir;
    }

    /**
     * Is existe dados incluir.
     *
     * @return true, if is existe dados incluir
     */
    public boolean isExisteDadosIncluir() {
        return existeDadosIncluir;
    }

    /**
     * Get: dsAgenciaOperadora.
     *
     * @return dsAgenciaOperadora
     */
    public String getDsAgenciaOperadora() {
        return dsAgenciaOperadora;
    }

    /**
     * Set: dsAgenciaOperadora.
     *
     * @param dsAgenciaOperadora the ds agencia operadora
     */
    public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
        this.dsAgenciaOperadora = dsAgenciaOperadora;
    }

    /**
     * Get: dsAgenciaGestora.
     *
     * @return dsAgenciaGestora
     */
    public String getDsAgenciaGestora() {
        return dsAgenciaGestora;
    }

    /**
     * Set: dsAgenciaGestora.
     *
     * @param dsAgenciaGestora the ds agencia gestora
     */
    public void setDsAgenciaGestora(String dsAgenciaGestora) {
        this.dsAgenciaGestora = dsAgenciaGestora;
    }

    /**
     * Set: existeDadosIncluir.
     *
     * @param existeDadosIncluir the existe dados incluir
     */
    public void setExisteDadosIncluir(boolean existeDadosIncluir) {
        this.existeDadosIncluir = existeDadosIncluir;
    }

    /**
     * Get: qtdeGridServicos.
     *
     * @return qtdeGridServicos
     */
    public int getQtdeGridServicos() {
        return qtdeGridServicos;
    }

    /**
     * Set: qtdeGridServicos.
     *
     * @param qtdeGridServicos the qtde grid servicos
     */
    public void setQtdeGridServicos(int qtdeGridServicos) {
        this.qtdeGridServicos = qtdeGridServicos;
    }

    /**
     * Get: qtdeParticipantes.
     *
     * @return qtdeParticipantes
     */
    public int getQtdeParticipantes() {
        return qtdeParticipantes;
    }

    /**
     * Set: qtdeParticipantes.
     *
     * @param qtdeParticipantes the qtde participantes
     */
    public void setQtdeParticipantes(int qtdeParticipantes) {
        this.qtdeParticipantes = qtdeParticipantes;
    }

    /**
     * Get: cpfCnpj.
     *
     * @return cpfCnpj
     */
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Set: cpfCnpj.
     *
     * @param cpfCnpj the cpf cnpj
     */
    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    /**
     * Get: descricaoContrato.
     *
     * @return descricaoContrato
     */
    public String getDescricaoContrato() {
        return descricaoContrato;
    }

    /**
     * Set: descricaoContrato.
     *
     * @param descricaoContrato the descricao contrato
     */
    public void setDescricaoContrato(String descricaoContrato) {
        this.descricaoContrato = descricaoContrato;
    }

    /**
     * Get: empresa.
     *
     * @return empresa
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Set: empresa.
     *
     * @param empresa the empresa
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * Is desabilita campos pesquisa.
     *
     * @return true, if is desabilita campos pesquisa
     */
    public boolean isDesabilitaCamposPesquisa() {
        return desabilitaCamposPesquisa;
    }

    /**
     * Set: desabilitaCamposPesquisa.
     *
     * @param desabilitaCamposPesquisa the desabilita campos pesquisa
     */
    public void setDesabilitaCamposPesquisa(boolean desabilitaCamposPesquisa) {
        this.desabilitaCamposPesquisa = desabilitaCamposPesquisa;
    }

    /**
     * Get: nomeRazaoSocial.
     *
     * @return nomeRazaoSocial
     */
    public String getNomeRazaoSocial() {
        return nomeRazaoSocial;
    }

    /**
     * Set: nomeRazaoSocial.
     *
     * @param nomeRazaoSocial the nome razao social
     */
    public void setNomeRazaoSocial(String nomeRazaoSocial) {
        this.nomeRazaoSocial = nomeRazaoSocial;
    }

    /**
     * Get: qtdeVinculos.
     *
     * @return qtdeVinculos
     */
    public int getQtdeVinculos() {
        return qtdeVinculos;
    }

    /**
     * Get: acaoFiltro.
     *
     * @return acaoFiltro
     */
    public Integer getAcaoFiltro() {
        return acaoFiltro;
    }

    /**
     * Set: acaoFiltro.
     *
     * @param acaoFiltro the acao filtro
     */
    public void setAcaoFiltro(Integer acaoFiltro) {
        this.acaoFiltro = acaoFiltro;
    }

    /**
     * Get: indicadorTipoManutencao.
     *
     * @return indicadorTipoManutencao
     */
    public Integer getIndicadorTipoManutencao() {
        return indicadorTipoManutencao;
    }

    /**
     * Set: indicadorTipoManutencao.
     *
     * @param indicadorTipoManutencao the indicador tipo manutencao
     */
    public void setIndicadorTipoManutencao(Integer indicadorTipoManutencao) {
        this.indicadorTipoManutencao = indicadorTipoManutencao;
    }

    /**
     * Get: itemSelecionadoSolicitacao.
     *
     * @return itemSelecionadoSolicitacao
     */
    public Integer getItemSelecionadoSolicitacao() {
        return itemSelecionadoSolicitacao;
    }

    /**
     * Set: itemSelecionadoSolicitacao.
     *
     * @param itemSelecionadoSolicitacao the item selecionado solicitacao
     */
    public void setItemSelecionadoSolicitacao(Integer itemSelecionadoSolicitacao) {
        this.itemSelecionadoSolicitacao = itemSelecionadoSolicitacao;
    }

    /**
     * Get: listaControleSolicitacao.
     *
     * @return listaControleSolicitacao
     */
    public List<SelectItem> getListaControleSolicitacao() {
        return listaControleSolicitacao;
    }

    /**
     * Set: listaControleSolicitacao.
     *
     * @param listaControleSolicitacao the lista controle solicitacao
     */
    public void setListaControleSolicitacao(List<SelectItem> listaControleSolicitacao) {
        this.listaControleSolicitacao = listaControleSolicitacao;
    }

    /**
     * Get: listaGridSolicitacao.
     *
     * @return listaGridSolicitacao
     */
    public List<ListarManutencaoContratoSaidaDTO> getListaGridSolicitacao() {
        return listaGridSolicitacao;
    }

    /**
     * Set: listaGridSolicitacao.
     *
     * @param listaGridSolicitacao the lista grid solicitacao
     */
    public void setListaGridSolicitacao(List<ListarManutencaoContratoSaidaDTO> listaGridSolicitacao) {
        this.listaGridSolicitacao = listaGridSolicitacao;
    }

    /**
     * Get: radioFiltroSolicitacoes.
     *
     * @return radioFiltroSolicitacoes
     */
    public String getRadioFiltroSolicitacoes() {
        return radioFiltroSolicitacoes;
    }

    /**
     * Set: radioFiltroSolicitacoes.
     *
     * @param radioFiltroSolicitacoes the radio filtro solicitacoes
     */
    public void setRadioFiltroSolicitacoes(String radioFiltroSolicitacoes) {
        this.radioFiltroSolicitacoes = radioFiltroSolicitacoes;
    }

    /**
     * Get: solManutContratosServiceImpl.
     *
     * @return solManutContratosServiceImpl
     */
    public ISolManutContratosService getSolManutContratosServiceImpl() {
        return solManutContratosServiceImpl;
    }

    /**
     * Set: solManutContratosServiceImpl.
     *
     * @param solManutContratosServiceImpl the sol manut contratos service impl
     */
    public void setSolManutContratosServiceImpl(ISolManutContratosService solManutContratosServiceImpl) {
        this.solManutContratosServiceImpl = solManutContratosServiceImpl;
    }

    /**
     * Get: tipoManutencaoFiltro.
     *
     * @return tipoManutencaoFiltro
     */
    public Integer getTipoManutencaoFiltro() {
        return tipoManutencaoFiltro;
    }

    /**
     * Set: tipoManutencaoFiltro.
     *
     * @param tipoManutencaoFiltro the tipo manutencao filtro
     */
    public void setTipoManutencaoFiltro(Integer tipoManutencaoFiltro) {
        this.tipoManutencaoFiltro = tipoManutencaoFiltro;
    }

    /**
     * Set: qtdeVinculos.
     *
     * @param qtdeVinculos the qtde vinculos
     */
    public void setQtdeVinculos(int qtdeVinculos) {
        this.qtdeVinculos = qtdeVinculos;
    }

    /**
     * Get: dsTipoManutencao.
     *
     * @return dsTipoManutencao
     */
    public String getDsTipoManutencao() {
        return dsTipoManutencao;
    }

    /**
     * Set: dsTipoManutencao.
     *
     * @param dsTipoManutencao the ds tipo manutencao
     */
    public void setDsTipoManutencao(String dsTipoManutencao) {
        this.dsTipoManutencao = dsTipoManutencao;
    }

    /**
     * Get: hrManutencaoRegistro.
     *
     * @return hrManutencaoRegistro
     */
    public String getHrManutencaoRegistro() {
        return hrManutencaoRegistro;
    }

    /**
     * Set: hrManutencaoRegistro.
     *
     * @param hrManutencaoRegistro the hr manutencao registro
     */
    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
        this.hrManutencaoRegistro = hrManutencaoRegistro;
    }

    /**
     * Get: imprimirFormalizacaoAlteracaoContratoServiceImpl.
     *
     * @return imprimirFormalizacaoAlteracaoContratoServiceImpl
     */
    public IImprimirFormalizacaoAlteracaoContratoService getImprimirFormalizacaoAlteracaoContratoServiceImpl() {
        return imprimirFormalizacaoAlteracaoContratoServiceImpl;
    }

    /**
     * Set: imprimirFormalizacaoAlteracaoContratoServiceImpl.
     *
     * @param imprimirFormalizacaoAlteracaoContratoServiceImpl the imprimir formalizacao alteracao contrato service impl
     */
    public void setImprimirFormalizacaoAlteracaoContratoServiceImpl(
        IImprimirFormalizacaoAlteracaoContratoService imprimirFormalizacaoAlteracaoContratoServiceImpl) {
        this.imprimirFormalizacaoAlteracaoContratoServiceImpl = imprimirFormalizacaoAlteracaoContratoServiceImpl;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get: listaSituacao.
     *
     * @return listaSituacao
     */
    public List<SelectItem> getListaSituacao() {
        return listaSituacao;
    }

    /**
     * Set: listaSituacao.
     *
     * @param listaSituacao the lista situacao
     */
    public void setListaSituacao(List<SelectItem> listaSituacao) {
        this.listaSituacao = listaSituacao;
    }

    /**
     * Get: itemSelecSituacao.
     *
     * @return itemSelecSituacao
     */
    public String getItemSelecSituacao() {
        return itemSelecSituacao;
    }

    /**
     * Set: itemSelecSituacao.
     *
     * @param itemSelecSituacao the item selec situacao
     */
    public void setItemSelecSituacao(String itemSelecSituacao) {
        this.itemSelecSituacao = itemSelecSituacao;
    }

    /**
     * Get: dtPeriodoInicio.
     *
     * @return dtPeriodoInicio
     */
    public Date getDtPeriodoInicio() {
        return dtPeriodoInicio;
    }

    /**
     * Set: dtPeriodoInicio.
     *
     * @param dtPeriodoInicio the dt periodo inicio
     */
    public void setDtPeriodoInicio(Date dtPeriodoInicio) {
        this.dtPeriodoInicio = dtPeriodoInicio;
    }

    /**
     * Get: dtPeriodoFim.
     *
     * @return dtPeriodoFim
     */
    public Date getDtPeriodoFim() {
        return dtPeriodoFim;
    }

    /**
     * Set: dtPeriodoFim.
     *
     * @param dtPeriodoFim the dt periodo fim
     */
    public void setDtPeriodoFim(Date dtPeriodoFim) {
        this.dtPeriodoFim = dtPeriodoFim;
    }

    /**
     * Get: dsGerenteResponsavel.
     *
     * @return dsGerenteResponsavel
     */
    public String getDsGerenteResponsavel() {
        return dsGerenteResponsavel;
    }

    /**
     * Set: dsGerenteResponsavel.
     *
     * @param dsGerenteResponsavel the ds gerente responsavel
     */
    public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
        this.dsGerenteResponsavel = dsGerenteResponsavel;
    }

    /**
     * Get: cpfCnpjParticipante.
     *
     * @return cpfCnpjParticipante
     */
    public String getCpfCnpjParticipante() {
        return cpfCnpjParticipante;
    }

    /**
     * Set: cpfCnpjParticipante.
     *
     * @param cpfCnpjParticipante the cpf cnpj participante
     */
    public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
        this.cpfCnpjParticipante = cpfCnpjParticipante;
    }

    /**
     * Get: nomeRazaoParticipante.
     *
     * @return nomeRazaoParticipante
     */
    public String getNomeRazaoParticipante() {
        return nomeRazaoParticipante;
    }

    /**
     * Set: nomeRazaoParticipante.
     *
     * @param nomeRazaoParticipante the nome razao participante
     */
    public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
        this.nomeRazaoParticipante = nomeRazaoParticipante;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
        return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
        this.cdBanco = cdBanco;
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
        return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
        this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBancoFormatado.
     *
     * @return cdBancoFormatado
     */
    public String getCdBancoFormatado() {
        return cdBancoFormatado;
    }

    /**
     * Set: cdBancoFormatado.
     *
     * @param cdBancoFormatado the cd banco formatado
     */
    public void setCdBancoFormatado(String cdBancoFormatado) {
        this.cdBancoFormatado = cdBancoFormatado;
    }

    /**
     * Get: cdAgenciaFormatado.
     *
     * @return cdAgenciaFormatado
     */
    public String getCdAgenciaFormatado() {
        return cdAgenciaFormatado;
    }

    /**
     * Set: cdAgenciaFormatado.
     *
     * @param cdAgenciaFormatado the cd agencia formatado
     */
    public void setCdAgenciaFormatado(String cdAgenciaFormatado) {
        this.cdAgenciaFormatado = cdAgenciaFormatado;
    }

    /**
     * Get: imprimirAnexoPrimeiroContratoService.
     *
     * @return imprimirAnexoPrimeiroContratoService
     */
    public IImprimirAnexoPrimeiroContratoService getImprimirAnexoPrimeiroContratoService() {
        return imprimirAnexoPrimeiroContratoService;
    }

    /**
     * Set: imprimirAnexoPrimeiroContratoService.
     *
     * @param imprimirAnexoPrimeiroContratoService the imprimir anexo primeiro contrato service
     */
    public void setImprimirAnexoPrimeiroContratoService(
        IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService) {
        this.imprimirAnexoPrimeiroContratoService = imprimirAnexoPrimeiroContratoService;
    }

    /**
     * Get: imprimirAnexoSegundoContratoService.
     *
     * @return imprimirAnexoSegundoContratoService
     */
    public IImprimirAnexoSegundoContratoService getImprimirAnexoSegundoContratoService() {
        return imprimirAnexoSegundoContratoService;
    }

    /**
     * Set: imprimirAnexoSegundoContratoService.
     *
     * @param imprimirAnexoSegundoContratoService the imprimir anexo segundo contrato service
     */
    public void setImprimirAnexoSegundoContratoService(
        IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService) {
        this.imprimirAnexoSegundoContratoService = imprimirAnexoSegundoContratoService;
    }

    /**
     * Get: dsCodTipoLayout.
     *
     * @return dsCodTipoLayout
     */
    public String getDsCodTipoLayout() {
        return dsCodTipoLayout;
    }

    /**
     * Set: dsCodTipoLayout.
     *
     * @param dsCodTipoLayout the ds cod tipo layout
     */
    public void setDsCodTipoLayout(String dsCodTipoLayout) {
        this.dsCodTipoLayout = dsCodTipoLayout;
    }

    /**
     * Get: cdTipoLayoutArquivo.
     *
     * @return cdTipoLayoutArquivo
     */
    public Integer getCdTipoLayoutArquivo() {
        return cdTipoLayoutArquivo;
    }

    /**
     * Set: cdTipoLayoutArquivo.
     *
     * @param cdTipoLayoutArquivo the cd tipo layout arquivo
     */
    public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
        this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
    }

    /**
     * Get: dsLayoutProprio.
     *
     * @return dsLayoutProprio
     */
    public String getDsLayoutProprio() {
        return dsLayoutProprio;
    }

    /**
     * Set: dsLayoutProprio.
     *
     * @param dsLayoutProprio the ds layout proprio
     */
    public void setDsLayoutProprio(String dsLayoutProprio) {
        this.dsLayoutProprio = dsLayoutProprio;
    }

    /**
     * Get: dsAplicFormat.
     *
     * @return dsAplicFormat
     */
    public String getDsAplicFormat() {
        return dsAplicFormat;
    }

    /**
     * Set: dsAplicFormat.
     *
     * @param dsAplicFormat the ds aplic format
     */
    public void setDsAplicFormat(String dsAplicFormat) {
        this.dsAplicFormat = dsAplicFormat;
    }

    /**
     * Get: dsCodMeioPrincipalRemessa.
     *
     * @return dsCodMeioPrincipalRemessa
     */
    public String getDsCodMeioPrincipalRemessa() {
        return dsCodMeioPrincipalRemessa;
    }

    /**
     * Set: dsCodMeioPrincipalRemessa.
     *
     * @param dsCodMeioPrincipalRemessa the ds cod meio principal remessa
     */
    public void setDsCodMeioPrincipalRemessa(String dsCodMeioPrincipalRemessa) {
        this.dsCodMeioPrincipalRemessa = dsCodMeioPrincipalRemessa;
    }

    /**
     * Get: dsCodMeioAltrnRemessa.
     *
     * @return dsCodMeioAltrnRemessa
     */
    public String getDsCodMeioAltrnRemessa() {
        return dsCodMeioAltrnRemessa;
    }

    /**
     * Set: dsCodMeioAltrnRemessa.
     *
     * @param dsCodMeioAltrnRemessa the ds cod meio altrn remessa
     */
    public void setDsCodMeioAltrnRemessa(String dsCodMeioAltrnRemessa) {
        this.dsCodMeioAltrnRemessa = dsCodMeioAltrnRemessa;
    }


    /**
     * Get: dsMeioAltrnRetorno.
     *
     * @return dsMeioAltrnRetorno
     */
    public String getDsMeioAltrnRetorno() {
        return dsMeioAltrnRetorno;
    }

    /**
     * Set: dsMeioAltrnRetorno.
     *
     * @param dsMeioAltrnRetorno the ds meio altrn retorno
     */
    public void setDsMeioAltrnRetorno(String dsMeioAltrnRetorno) {
        this.dsMeioAltrnRetorno = dsMeioAltrnRetorno;
    }

    /**
     * Get: dsUtilizacaoEmpresaVan.
     *
     * @return dsUtilizacaoEmpresaVan
     */
    public String getDsUtilizacaoEmpresaVan() {
        return dsUtilizacaoEmpresaVan;
    }

    /**
     * Set: dsUtilizacaoEmpresaVan.
     *
     * @param dsUtilizacaoEmpresaVan the ds utilizacao empresa van
     */
    public void setDsUtilizacaoEmpresaVan(String dsUtilizacaoEmpresaVan) {
        this.dsUtilizacaoEmpresaVan = dsUtilizacaoEmpresaVan;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
        return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
        this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: dsResponsavelCustoEmpresa.
     *
     * @return dsResponsavelCustoEmpresa
     */
    public String getDsResponsavelCustoEmpresa() {
        return dsResponsavelCustoEmpresa;
    }

    /**
     * Set: dsResponsavelCustoEmpresa.
     *
     * @param dsResponsavelCustoEmpresa the ds responsavel custo empresa
     */
    public void setDsResponsavelCustoEmpresa(String dsResponsavelCustoEmpresa) {
        this.dsResponsavelCustoEmpresa = dsResponsavelCustoEmpresa;
    }

    /**
     * Set: cdCodMeioPrincipalRetorno.
     *
     * @param cdCodMeioPrincipalRetorno the cd cod meio principal retorno
     */
    public void setCdCodMeioPrincipalRetorno(String cdCodMeioPrincipalRetorno) {
        this.cdCodMeioPrincipalRetorno = cdCodMeioPrincipalRetorno;
    }

    /**
     * Get: cdCodMeioPrincipalRetorno.
     *
     * @return cdCodMeioPrincipalRetorno
     */
    public String getCdCodMeioPrincipalRetorno() {
        return cdCodMeioPrincipalRetorno;
    }

    /**
     * Set: pcCustoOrganizacaoTransmissao.
     *
     * @param pcCustoOrganizacaoTransmissao the pc custo organizacao transmissao
     */
    public void setPcCustoOrganizacaoTransmissao(
        BigDecimal pcCustoOrganizacaoTransmissao) {
        this.pcCustoOrganizacaoTransmissao = pcCustoOrganizacaoTransmissao;
    }

    /**
     * Get: manterContratoService.
     *
     * @return manterContratoService
     */
    public IManterContratoService getManterContratoService() {
        return manterContratoService;
    }

    /**
     * Set: manterContratoService.
     *
     * @param manterContratoService the manter contrato service
     */
    public void setManterContratoService(
        IManterContratoService manterContratoService) {
        this.manterContratoService = manterContratoService;
    }

    /**
     * Get: pcCustoOrganizacaoTransmissao.
     *
     * @return pcCustoOrganizacaoTransmissao
     */
    public BigDecimal getPcCustoOrganizacaoTransmissao() {
        return pcCustoOrganizacaoTransmissao;
    }

    /**
     * Get: listaGridMeioTransmissao.
     *
     * @return listaGridMeioTransmissao
     */
    public List<PerfilDTO> getListaGridMeioTransmissao() {
        return listaGridMeioTransmissao;
    }

    /**
     * Set: listaGridMeioTransmissao.
     *
     * @param listaGridMeioTransmissao the lista grid meio transmissao
     */
    public void setListaGridMeioTransmissao(List<PerfilDTO> listaGridMeioTransmissao) {
        this.listaGridMeioTransmissao = listaGridMeioTransmissao;
    }

    /**
     * Get: qtdPerfis.
     *
     * @return qtdPerfis
     */
    public Integer getQtdPerfis() {
        return qtdPerfis;
    }

    /**
     * Set: qtdPerfis.
     *
     * @param qtdPerfis the qtd perfis
     */
    public void setQtdPerfis(Integer qtdPerfis) {
        this.qtdPerfis = qtdPerfis;
    }

    /**
     * Get: formalizacaoAltContratoImpl.
     *
     * @return formalizacaoAltContratoImpl
     */
    public IManterformalizacaoalteracaocontratoService getFormalizacaoAltContratoImpl() {
        return formalizacaoAltContratoImpl;
    }

    /**
     * Set: formalizacaoAltContratoImpl.
     *
     * @param formalizacaoAltContratoImpl the formalizacao alt contrato impl
     */
    public void setFormalizacaoAltContratoImpl(IManterformalizacaoalteracaocontratoService formalizacaoAltContratoImpl) {
        this.formalizacaoAltContratoImpl = formalizacaoAltContratoImpl;
    }

    /**
     * Get saida consultar manutencao config contrato.
     *
     * @return the saida consultar manutencao config contrato
     */
    public ConsultarManutencaoConfigContratoSaidaDTO getSaidaConsultarManutencaoConfigContrato() {
        return saidaConsultarManutencaoConfigContrato;
    }

    /**
     * Set saida consultar manutencao config contrato.
     *
     * @param saidaConsultarManutencaoConfigContrato the saida consultar manutencao config contrato
     */
    public void setSaidaConsultarManutencaoConfigContrato(
        ConsultarManutencaoConfigContratoSaidaDTO saidaConsultarManutencaoConfigContrato) {
        this.saidaConsultarManutencaoConfigContrato = saidaConsultarManutencaoConfigContrato;
    }

    /**
     * Nome: setConsultarManutencaoServicoContratoSaidaDTO.
     *
     * @param consultarManutencaoServicoContratoSaidaDTO the consultar manutencao servico contrato saida dto
     */
    public void setConsultarManutencaoServicoContratoSaidaDTO(ConsultarManutencaoServicoContratoSaidaDTO consultarManutencaoServicoContratoSaidaDTO) {
        this.consultarManutencaoServicoContratoSaidaDTO = consultarManutencaoServicoContratoSaidaDTO;
    }

    /**
     * Nome: getConsultarManutencaoServicoContratoSaidaDTO.
     *
     * @return consultarManutencaoServicoContratoSaidaDTO
     */
    public ConsultarManutencaoServicoContratoSaidaDTO getConsultarManutencaoServicoContratoSaidaDTO() {
        return consultarManutencaoServicoContratoSaidaDTO;
    }

    /**
     * Nome: setConsultarManutencaoParticipanteSaidaDTO
     *
     * @param consultarManutencaoParticipanteSaidaDTO
     */
    public void setConsultarManutencaoParticipanteSaidaDTO(ConsultarManutencaoParticipanteSaidaDTO consultarManutencaoParticipanteSaidaDTO) {
        this.consultarManutencaoParticipanteSaidaDTO = consultarManutencaoParticipanteSaidaDTO;
    }

    /**
     * Nome: getConsultarManutencaoParticipanteSaidaDTO
     *
     * @return consultarManutencaoParticipanteSaidaDTO
     */
    public ConsultarManutencaoParticipanteSaidaDTO getConsultarManutencaoParticipanteSaidaDTO() {
        return consultarManutencaoParticipanteSaidaDTO;
    }

    /**
     * Nome: setSaidaConRelacaoConManutencoes
     *
     * @param saidaConRelacaoConManutencoes
     */
    public void setSaidaConRelacaoConManutencoes(ConRelacaoConManutencoesSaidaDTO saidaConRelacaoConManutencoes) {
        this.saidaConRelacaoConManutencoes = saidaConRelacaoConManutencoes;
    }

    /**
     * Nome: getSaidaConRelacaoConManutencoes
     *
     * @return saidaConRelacaoConManutencoes
     */
    public ConRelacaoConManutencoesSaidaDTO getSaidaConRelacaoConManutencoes() {
        return saidaConRelacaoConManutencoes;
    }

    public DetalharManterFormAltContratoSaidaDTO getSaidaDetalharManterFormAltContrato() {
        return saidaDetalharManterFormAltContrato;
    }

    public void setSaidaDetalharManterFormAltContrato(
        DetalharManterFormAltContratoSaidaDTO saidaDetalharManterFormAltContrato) {
        this.saidaDetalharManterFormAltContrato = saidaDetalharManterFormAltContrato;
    }

    /**
     * Nome: setContratoServiceImpl
     *
     * @param contratoServiceImpl
     */
    public void setContratoServiceImpl(IContratoService contratoServiceImpl) {
        this.contratoServiceImpl = contratoServiceImpl;
    }

    /**
     * Nome: getContratoServiceImpl
     *
     * @return contratoServiceImpl
     */
    public IContratoService getContratoServiceImpl() {
        return contratoServiceImpl;
    }
}
