package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberacaolotepagamentos;

import java.util.ArrayList;

import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.OcorrenciasLotePagamentosDTO;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.lotepagamentos.LotePagamentosBean;

public class ManterSolicitacaoLiberacaoLoteSemConsultaBean extends
		LotePagamentosBean {

	public static final int COD_SOLICITACAO_PAGAMENTO_INTEGRADO = 28;
	private static final String TELA_CONSULTA = "conSolicLiberacaoLotePgto";
	private static final String TELA_DETALHE = "detSolicLiberacaoLotePgto";
	private static final String TELA_EXCLUSAO = "excSolicLiberacaoLotePgto";
	private static final String TELA_INCLUSAO = "incSolicLiberacaoLotePgto";
	private static final String TELA_INCLUSAO_CONFIRMACAO = "confIncSolicLiberacaoLotePgto";
	private static final String TELA_DETALHE_OCORRENCIAS = "detSolicLiberacaoLotePgtoOcorrencias";

	@Override
	public void buscaInformacoesDetalhe() {
		DetalharLotePagamentosEntradaDTO entrada = prepararBuscaInformacoesDetalhe();

		DetalharLotePagamentosSaidaDTO saidaDetlahe = getManterSolicLiberacaoLotePgtoService()
				.detalharSolicLiberacaoLotePgto(entrada);

		consolidaInformacoesDetalhe(saidaDetlahe);

	}

	/**
	 * carrega Lista Lote Pagamento
	 *
	 * @param evt the evt
	 */
	public void carregaListaLotePagamento(ActionEvent evt) {
		carregaListaLotePagamento();
	}
	
	@Override
	public String carregaListaLotePagamento() {
		ConsultarLotePagamentosEntradaDTO entrada = preparaBuscaListaLotePagamento();

		try {
			setListaGridLotePagto(getManterSolicLiberacaoLotePgtoService()
					.consultarSolicLiberacaoLotePgto(entrada).getOcorrencias());

			carregaListaControleRadios();

			getPagamentosBean().setDisableArgumentosConsulta(true);

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);

			setListaGridLotePagto(new ArrayList<OcorrenciasLotePagamentosDTO>());
			setItemSelecionadoLista(null);
			getPagamentosBean().setDisableArgumentosConsulta(false);
		}

		return "";
	}

	@Override
	public String confirmarExcluir() {
		ExcluirLotePagamentosEntradaDTO entrada = prepararExclusaoLotePagamento();

		try {

			final ExcluirLotePagamentosSaidaDTO saida = getManterSolicLiberacaoLotePgtoService()
					.excluirSolicLiberacaoLotePgto(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),getTelaConsulta(),
					"#{manterSolicitacaoLiberacaoLoteSemConsultaBean.carregaListaLotePagamento}", false);

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}
		
		setItemSelecionadoLista(null);
		return "";
	}

	@Override
	public String confirmarIncluir() {
		IncluirLotePagamentosEntradaDTO entrada = prepararInclusaoLotePagamento();
		
		entrada.setCdTipoLayoutArquivo(getListaLote().get(getItemSelecao()).getCdTipoLayoutArquivo());
		entrada.setCdProdutoServicoOperacao(getListaLote().get(getItemSelecao()).getCdProdutoServicoOperacao());
		

		try {
			IncluirLotePagamentosSaidaDTO saida = getManterSolicLiberacaoLotePgtoService()
					.incluirSolicLiberacaoLotePgto(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),getTelaInclusao(),
					"", false);
			
			setItemSelecao(null);

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	@Override
	public String getBean() {
		return "#{manterSolicitacaoLiberacaoLoteSemConsultaBean.iniciarTela}";
	}

	@Override
	public String getTelaConsulta() {
		return TELA_CONSULTA;
	}

	@Override
	public String getTelaDetalhe() {
		return TELA_DETALHE;
	}

	@Override
	public String getTelaDetalheOcorrencias() {
		return TELA_DETALHE_OCORRENCIAS;
	}

	@Override
	public String getTelaExclusao() {
		return TELA_EXCLUSAO;
	}

	@Override
	public String getTelaInclusao() {
		return TELA_INCLUSAO;
	}

	@Override
	public String getTelaInclusaoConfirmacao() {
		return TELA_INCLUSAO_CONFIRMACAO;
	}

	@Override
	public Integer getCodigoSolicitacao() {
		return COD_SOLICITACAO_PAGAMENTO_INTEGRADO;
	}

}
