/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assoclinhatipomsg
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assoclinhatipomsg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.IAssocLinhaTipoMsgService;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.AlterarLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.AlterarLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.DetalharLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.DetalharLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ExcluirLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ExcluirLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.IncluirLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.IncluirLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ListarLinhaMsgComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclinhatipomsg.bean.ListarLinhaMsgComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.IdiomaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarRecursoCanalMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoMensagemSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ConsultarDescricaoMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ConsultarDescricaoMensagemSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: AssociacaoLinhasTipoMensagemBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssociacaoLinhasTipoMensagemBean {
	
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM. */
	private static final String CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM = "conManterAssociacaoLinhasTipoMensagem";
	
	/** Atributo manterAssociacaoLinhaTipoMensagemServiceImpl. */
	private IAssocLinhaTipoMsgService manterAssociacaoLinhaTipoMensagemServiceImpl;	

	/*Consulta*/
	/** Atributo listaPesquisaMensagem. */
	private List<ListarLinhaMsgComprovanteSaidaDTO> listaPesquisaMensagem;
	
	/** Atributo retornoLista. */
	private ListarLinhaMsgComprovanteSaidaDTO retornoLista = new ListarLinhaMsgComprovanteSaidaDTO();
	
	/** Atributo tipoMensagemFiltro. */
	private Integer tipoMensagemFiltro;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio;
	
	
	/*Incluir e Alterar*/	
	/** Atributo listaRecurso. */
	private List<SelectItem> listaRecurso = new ArrayList<SelectItem>();
	
	/** Atributo listaRecursoHash. */
	private Map<Integer, String> listaRecursoHash = new HashMap<Integer, String>();
	
	/** Atributo listaIdioma. */
	private List<SelectItem> listaIdioma = new ArrayList<SelectItem>();
	
	/** Atributo listaIdiomaHash. */
	private Map<Integer, String> listaIdiomaHash = new HashMap<Integer, String>();
	
	/** Atributo listaCentroCusto. */
	private List<SelectItem> listaCentroCusto= new ArrayList<SelectItem>();
	
	/** Atributo listaTipoMensagem. */
	private List<SelectItem> listaTipoMensagem = new ArrayList<SelectItem>();
	
	/** Atributo listaCentroCustoHash. */
	private Map<String, String> listaCentroCustoHash = new HashMap<String, String>();
	
	/** Atributo entradaMensagem. */
	private ConsultarDescricaoMensagemEntradaDTO entradaMensagem = new ConsultarDescricaoMensagemEntradaDTO();
	
	/** Atributo retornoMensagem. */
	private ConsultarDescricaoMensagemSaidaDTO retornoMensagem = new ConsultarDescricaoMensagemSaidaDTO();
	
	/** Atributo recurso. */
	private Integer recurso;
	
	/** Atributo tipoMensagem. */
	private Integer tipoMensagem;
	
	/** Atributo idioma. */
	private Integer idioma;
	
	/** Atributo codigoMensagem. */
	private String codigoMensagem;	
	
	/** Atributo recursoDesc. */
	private String recursoDesc;
	
	/** Atributo idiomaDesc. */
	private String idiomaDesc;
	
	/** Atributo codigoMensagemDesc. */
	private String codigoMensagemDesc;
	
	/** Atributo tipoMensagemDesc. */
	private String tipoMensagemDesc;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo ordemLinhaMensagem. */
	private Long ordemLinhaMensagem;
	
	
	
	/*Trilha de Auditoria*/
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;	
	
	
	
	/*Valida��o*/
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo centroCustoPesq. */
	private String centroCustoPesq;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo centroCustoDesc. */
	private String centroCustoDesc;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	
    /*Getters e Setters*/	
	
		
	/**
     * Get: listaIdioma.
     *
     * @return listaIdioma
     */
    public List<SelectItem> getListaIdioma() {
		
		this.listaIdioma = new ArrayList<SelectItem>();
		
		List<IdiomaSaidaDTO> listaIdiomaSaida = new ArrayList<IdiomaSaidaDTO>();
		
		listaIdiomaSaida = comboService.listarIdioma();
		
		listaIdiomaHash.clear();
		
		for(IdiomaSaidaDTO combo : listaIdiomaSaida){
			
			listaIdiomaHash.put(combo.getCdIdioma(),combo.getDsIdioma());
			
			this.listaIdioma.add(new SelectItem(combo.getCdIdioma(),combo.getDsIdioma()));
		}
		
		
		return this.listaIdioma;
	}

	/**
	 * Set: listaIdioma.
	 *
	 * @param listaIdioma the lista idioma
	 */
	public void setListaIdioma(List<SelectItem> listaIdioma) {
		this.listaIdioma = listaIdioma;
	}

	
	/**
	 * Get: listaRecurso.
	 *
	 * @return listaRecurso
	 */
	public List<SelectItem> getListaRecurso() {
		
		this.listaRecurso = new ArrayList<SelectItem>();
		
		List<ListarRecursoCanalMsgSaidaDTO> listaRecursoAux = new ArrayList<ListarRecursoCanalMsgSaidaDTO>();
		
		listaRecursoAux = comboService.listarRecursoCanalMsg();
		
		listaRecursoHash.clear();
		
		for(ListarRecursoCanalMsgSaidaDTO combo : listaRecursoAux){
			
			listaRecursoHash.put(combo.getCdTipoCanal(),combo.getDsTipoCanal());
			
			this.listaRecurso.add(new SelectItem(combo.getCdTipoCanal(),combo.getDsTipoCanal()));
		}
		
		
		return this.listaRecurso;
	}

	/**
	 * Set: listaRecurso.
	 *
	 * @param listaRecurso the lista recurso
	 */
	public void setListaRecurso(List<SelectItem> listaRecurso) {
		this.listaRecurso = listaRecurso;
	}

	/**
	 * Get: codigoMensagem.
	 *
	 * @return codigoMensagem
	 */
	public String getCodigoMensagem() {
		return codigoMensagem;
	}

	/**
	 * Set: codigoMensagem.
	 *
	 * @param codigoMensagem the codigo mensagem
	 */
	public void setCodigoMensagem(String codigoMensagem) {
		this.codigoMensagem = codigoMensagem;
	}

	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public Integer getIdioma() {
		return idioma;
	}

	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(Integer idioma) {
		this.idioma = idioma;
	}

	/**
	 * Get: ordemLinhaMensagem.
	 *
	 * @return ordemLinhaMensagem
	 */
	public Long getOrdemLinhaMensagem() {
		return ordemLinhaMensagem;
	}

	/**
	 * Set: ordemLinhaMensagem.
	 *
	 * @param ordemLinhaMensagem the ordem linha mensagem
	 */
	public void setOrdemLinhaMensagem(Long ordemLinhaMensagem) {
		this.ordemLinhaMensagem = ordemLinhaMensagem;
	}

	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public Integer getRecurso() {
		return recurso;
	}

	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(Integer recurso) {
		this.recurso = recurso;
	}

	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public Integer getTipoMensagem() {
		return tipoMensagem;
	}

	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(Integer tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	
	/**
	 * Get: manterAssociacaoLinhaTipoMensagemServiceImpl.
	 *
	 * @return manterAssociacaoLinhaTipoMensagemServiceImpl
	 */
	public IAssocLinhaTipoMsgService getManterAssociacaoLinhaTipoMensagemServiceImpl() {
		return manterAssociacaoLinhaTipoMensagemServiceImpl;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: codigoMensagemDesc.
	 *
	 * @return codigoMensagemDesc
	 */
	public String getCodigoMensagemDesc() {
		return codigoMensagemDesc;
	}

	/**
	 * Set: codigoMensagemDesc.
	 *
	 * @param codigoMensagemDesc the codigo mensagem desc
	 */
	public void setCodigoMensagemDesc(String codigoMensagemDesc) {
		this.codigoMensagemDesc = codigoMensagemDesc;
	}

	/**
	 * Get: idiomaDesc.
	 *
	 * @return idiomaDesc
	 */
	public String getIdiomaDesc() {
		return idiomaDesc;
	}

	/**
	 * Set: idiomaDesc.
	 *
	 * @param idiomaDesc the idioma desc
	 */
	public void setIdiomaDesc(String idiomaDesc) {
		this.idiomaDesc = idiomaDesc;
	}

	
	/**
	 * Get: recursoDesc.
	 *
	 * @return recursoDesc
	 */
	public String getRecursoDesc() {
		return recursoDesc;
	}

	/**
	 * Set: recursoDesc.
	 *
	 * @param recursoDesc the recurso desc
	 */
	public void setRecursoDesc(String recursoDesc) {
		this.recursoDesc = recursoDesc;
	}

	/**
	 * Get: tipoMensagemDesc.
	 *
	 * @return tipoMensagemDesc
	 */
	public String getTipoMensagemDesc() {
		return tipoMensagemDesc;
	}

	/**
	 * Set: tipoMensagemDesc.
	 *
	 * @param tipoMensagemDesc the tipo mensagem desc
	 */
	public void setTipoMensagemDesc(String tipoMensagemDesc) {
		this.tipoMensagemDesc = tipoMensagemDesc;
	}


	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}	
	
	
	
	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Set: manterAssociacaoLinhaTipoMensagemServiceImpl.
	 *
	 * @param manterAssociacaoLinhaTipoMensagemServiceImpl the manter associacao linha tipo mensagem service impl
	 */
	public void setManterAssociacaoLinhaTipoMensagemServiceImpl(
			IAssocLinhaTipoMsgService manterAssociacaoLinhaTipoMensagemServiceImpl) {
		this.manterAssociacaoLinhaTipoMensagemServiceImpl = manterAssociacaoLinhaTipoMensagemServiceImpl;
	}

	/**
	 * Get: listaPesquisaMensagem.
	 *
	 * @return listaPesquisaMensagem
	 */
	public List<ListarLinhaMsgComprovanteSaidaDTO> getListaPesquisaMensagem() {
		return listaPesquisaMensagem;
	}

	/**
	 * Set: listaPesquisaMensagem.
	 *
	 * @param listaPesquisaMensagem the lista pesquisa mensagem
	 */
	public void setListaPesquisaMensagem(
			List<ListarLinhaMsgComprovanteSaidaDTO> listaPesquisaMensagem) {
		this.listaPesquisaMensagem = listaPesquisaMensagem;
	}
	

	
	
	/* M�todos*/
	
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {	
		
		setTipoMensagemFiltro(0);
		setListaPesquisaMensagem(null);
		setItemSelecionadoLista(null);
		setListaTipoMensagem(null);
		preencheListaTipoMensagem();
		setBtoAcionado(false);
	}
	
	
	/**
	 * Preenche lista tipo mensagem.
	 *
	 * @return the list< select item>
	 */
	public List<SelectItem> preencheListaTipoMensagem() {	
		try{
			listaTipoMensagem = new ArrayList<SelectItem>();
			List<ListarTipoMensagemSaidaDTO> listaTipoMensagemSaida;
			
			ListarTipoMensagemEntradaDTO tipoMensagemEntradaDTO = new ListarTipoMensagemEntradaDTO();
			tipoMensagemEntradaDTO.setCdControleMensagemSalarial(0);
			tipoMensagemEntradaDTO.setCdTipoComprovanteSalarial(0);
			tipoMensagemEntradaDTO.setCdRestMensagemSalarial(0);
			tipoMensagemEntradaDTO.setCdTipoMensagemSalarial(0);
			tipoMensagemEntradaDTO.setNumeroOcorrencias(30);
		
			listaTipoMensagemSaida = comboService.listarTipoMensagem(tipoMensagemEntradaDTO);
			
			for (int i = 0; i < listaTipoMensagemSaida.size(); i++) {
				this.listaTipoMensagem.add(new SelectItem(listaTipoMensagemSaida.get(i).getCdControleMensagemSalarial(),PgitUtil.concatenarCampos(listaTipoMensagemSaida.get(i).getCdControleMensagemSalarial(), listaTipoMensagemSaida.get(i).getDsControleMensagemSalarial())));
															                                                                       
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoMensagem = new ArrayList<SelectItem>();
		}

		return this.listaTipoMensagem;
	}

	
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "DETALHAR";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		setTipoMensagem(0);		
		setRecurso(0);
		setIdioma(0);
		setCentroCustoPesq("");
		setCentroCusto("");
		setCodigoMensagem("");
		setListaCentroCusto(new ArrayList<SelectItem>());
		return "INCLUIR";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "ALTERAR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "EXCLUIR";
	}

	/**
	 * Voltar pesquisa.
	 *
	 * @return the string
	 */
	public String voltarPesquisa() {
		setItemSelecionadoLista(null);
		
		return "VOLTAR_PESQUISA";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Incluir confirmar.
	 *
	 * @return the string
	 */
	public String incluirConfirmar() {
		try {
			
			IncluirLinhaMsgComprovanteEntradaDTO incluirLinhaMsgComprovanteEntradaDTO = new IncluirLinhaMsgComprovanteEntradaDTO();
			
			incluirLinhaMsgComprovanteEntradaDTO.setTipoMensagem(getTipoMensagem()!=null && !getTipoMensagem().equals("")?getTipoMensagem():0);
			incluirLinhaMsgComprovanteEntradaDTO.setPrefixo(getCentroCusto());
			incluirLinhaMsgComprovanteEntradaDTO.setCodigoMensagem(entradaMensagem.getNrEventoMensagemNegocio()!= null && !entradaMensagem.getNrEventoMensagemNegocio().equals("") ? entradaMensagem.getNrEventoMensagemNegocio() : 0);
			incluirLinhaMsgComprovanteEntradaDTO.setOrdemLinhaMensagem(entradaMensagem.getOrdemLinhaMensagem());
			incluirLinhaMsgComprovanteEntradaDTO.setRecurso(getRecurso());
			incluirLinhaMsgComprovanteEntradaDTO.setIdioma(getIdioma());
			
			IncluirLinhaMsgComprovanteSaidaDTO incluirLinhaMsgComprovanteSaidaDTO = getManterAssociacaoLinhaTipoMensagemServiceImpl().incluirLinhaMsgComprovante(incluirLinhaMsgComprovanteEntradaDTO);		
			BradescoFacesUtils.addInfoModalMessage("(" +incluirLinhaMsgComprovanteSaidaDTO.getCodMensagem() + ") " + incluirLinhaMsgComprovanteSaidaDTO.getMensagem(), CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			
		return "";
		
	}
	
	/**
	 * Preencher tipo mensagem desc.
	 */
	private void preencherTipoMensagemDesc() {
		setTipoMensagemDesc("");
		SelectItem selectItemTipoMensagem;
		
		for (int j = 0; j < getListaTipoMensagem().size(); j++) {
			
			selectItemTipoMensagem = getListaTipoMensagem().get(j);
			
			if (getTipoMensagem().equals(selectItemTipoMensagem.getValue())){
				setTipoMensagemDesc(selectItemTipoMensagem.getLabel());
				break;
				
			}
		}
		
	
	}

	/**
	 * Excluir confirmar.
	 *
	 * @return the string
	 */
	public String excluirConfirmar() {
		try {
			ExcluirLinhaMsgComprovanteEntradaDTO excluirLinhaMsgComprovanteEntradaDTO = new ExcluirLinhaMsgComprovanteEntradaDTO();
			ListarLinhaMsgComprovanteSaidaDTO listarLinhaMsgComprovanteSaidaDTO = getListaPesquisaMensagem().get(getItemSelecionadoLista());
			
			excluirLinhaMsgComprovanteEntradaDTO.setTipoMensagem(listarLinhaMsgComprovanteSaidaDTO.getCdTipoMensagem());
			excluirLinhaMsgComprovanteEntradaDTO.setPrefixo(listarLinhaMsgComprovanteSaidaDTO.getPrefixo());
			excluirLinhaMsgComprovanteEntradaDTO.setCodigoMensagem(listarLinhaMsgComprovanteSaidaDTO.getCodigoLinhaMsgComprovante());
			excluirLinhaMsgComprovanteEntradaDTO.setRecurso(listarLinhaMsgComprovanteSaidaDTO.getCdRecurso());
			excluirLinhaMsgComprovanteEntradaDTO.setIdioma(listarLinhaMsgComprovanteSaidaDTO.getCdIdioma());
			
			ExcluirLinhaMsgComprovanteSaidaDTO excluirLinhaMsgComprovanteSaidaDTO = getManterAssociacaoLinhaTipoMensagemServiceImpl().excluirLinhaMsgComprovante(excluirLinhaMsgComprovanteEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirLinhaMsgComprovanteSaidaDTO.getCodMensagem() + ") " + excluirLinhaMsgComprovanteSaidaDTO.getMensagem(), CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM, BradescoViewExceptionActionType.ACTION, false);

			carregaLista();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		
			if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")){	
				preencherTipoMensagemDesc();
				setCentroCustoDesc((String)listaCentroCustoHash.get(getCentroCusto()));
				setRecursoDesc((String) listaRecursoHash.get(getRecurso()));
				setIdiomaDesc((String) listaIdiomaHash.get(getIdioma()));				
				
				entradaMensagem.setCdSistema(this.getCentroCusto());
				entradaMensagem.setCdIdiomaTextoMensagem(this.getIdioma());
				entradaMensagem.setCdRecursoGeradorMensagem(this.getRecurso());

				setRetornoMensagem(getManterAssociacaoLinhaTipoMensagemServiceImpl().consultarDescricaoMensagem(this.entradaMensagem));
				retornoMensagem.setOrdemLinhaMensagem(entradaMensagem.getOrdemLinhaMensagem());

				return "AVANCAR_INCLUIR";
			}
			return "";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {

		return "AVANCAR_ALTERAR";
		
	}

	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar() {
		return "VOLTAR_ALTERAR";
	}

	/**
	 * Alterar confirmar.
	 *
	 * @return the string
	 */
	public String alterarConfirmar() {
		
		try {
			
			AlterarLinhaMsgComprovanteEntradaDTO alterarLinhaMsgComprovanteEntradaDTO = new AlterarLinhaMsgComprovanteEntradaDTO();
			
			ListarLinhaMsgComprovanteSaidaDTO listarLinhaMsgComprovanteSaidaDTO = getListaPesquisaMensagem().get(getItemSelecionadoLista()); 
			
			alterarLinhaMsgComprovanteEntradaDTO.setTipoMensagem(listarLinhaMsgComprovanteSaidaDTO.getCdTipoMensagem());
			alterarLinhaMsgComprovanteEntradaDTO.setPrefixo(listarLinhaMsgComprovanteSaidaDTO.getPrefixo());
			alterarLinhaMsgComprovanteEntradaDTO.setCodigoMensagem(listarLinhaMsgComprovanteSaidaDTO.getCodigoLinhaMsgComprovante());
			alterarLinhaMsgComprovanteEntradaDTO.setRecurso(listarLinhaMsgComprovanteSaidaDTO.getCdRecurso());
			alterarLinhaMsgComprovanteEntradaDTO.setIdioma(listarLinhaMsgComprovanteSaidaDTO.getCdIdioma());
			alterarLinhaMsgComprovanteEntradaDTO.setOrdemLinhaMensagem(getOrdemLinhaMensagem());
			
			AlterarLinhaMsgComprovanteSaidaDTO incluirLinhaMsgComprovanteSaidaDTO = getManterAssociacaoLinhaTipoMensagemServiceImpl().alterarLinhaMsgComprovante(alterarLinhaMsgComprovanteEntradaDTO);		
			BradescoFacesUtils.addInfoModalMessage("(" +incluirLinhaMsgComprovanteSaidaDTO.getCodMensagem() + ") " + incluirLinhaMsgComprovanteSaidaDTO.getMensagem(),CON_MANTER_ASSOCIACAO_LINHAS_TIPO_MENSAGEM, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			
		return "";
		
	}

	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos() {
		this.setTipoMensagemFiltro(null);
		setListaPesquisaMensagem(null);
		setCentroCusto(null);
		setCentroCustoPesq(null);
		this.listaCentroCusto = new ArrayList<SelectItem>();
		setBtoAcionado(false);
		return "OK";
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "OK";
	}

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		try{
			
			ListarLinhaMsgComprovanteEntradaDTO entradaDTO = new ListarLinhaMsgComprovanteEntradaDTO();
			
			entradaDTO.setTipoMensagem(getTipoMensagemFiltro()!=null && !getTipoMensagemFiltro().equals("")?getTipoMensagemFiltro():0);
			entradaDTO.setCentroCusto(getCentroCusto() == null ? "" : getCentroCusto());
			setListaPesquisaMensagem(getManterAssociacaoLinhaTipoMensagemServiceImpl().listarLinhaMsgComprovante(entradaDTO));
			setBtoAcionado(true);
			listaControleRadio = new ArrayList<SelectItem>();
			for (int i = 0; i <= this.getListaPesquisaMensagem().size(); i++) {
				listaControleRadio.add(new SelectItem(i, " "));
			}

			setItemSelecionadoLista(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaPesquisaMensagem(null);

		}
		
		return "PESQUISA";
	}
	
	

	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
		ListarLinhaMsgComprovanteSaidaDTO listarLinhaMsgComprovanteSaidaDTO = getListaPesquisaMensagem().get(getItemSelecionadoLista());
		DetalharLinhaMsgComprovanteEntradaDTO detalharLinhaMsgComprovanteEntradaDTO = new DetalharLinhaMsgComprovanteEntradaDTO();
	
		detalharLinhaMsgComprovanteEntradaDTO.setTipoMensagem(listarLinhaMsgComprovanteSaidaDTO.getCdTipoMensagem());
		detalharLinhaMsgComprovanteEntradaDTO.setPrefixo(listarLinhaMsgComprovanteSaidaDTO.getPrefixo());
		detalharLinhaMsgComprovanteEntradaDTO.setCodigoMensagem(listarLinhaMsgComprovanteSaidaDTO.getCodigoLinhaMsgComprovante());
		detalharLinhaMsgComprovanteEntradaDTO.setRecurso(listarLinhaMsgComprovanteSaidaDTO.getCdRecurso());
		detalharLinhaMsgComprovanteEntradaDTO.setIdioma(listarLinhaMsgComprovanteSaidaDTO.getCdIdioma());
		
		DetalharLinhaMsgComprovanteSaidaDTO detalharLinhaMsgComprovanteSaidaDTO = getManterAssociacaoLinhaTipoMensagemServiceImpl().detalharLinhaMsgComprovante(detalharLinhaMsgComprovanteEntradaDTO);
		
		setTipoMensagem(detalharLinhaMsgComprovanteSaidaDTO.getCdTipoMensagem());
		setTipoMensagemDesc(detalharLinhaMsgComprovanteSaidaDTO.getDsTipoMensagem());		
		setRecurso(detalharLinhaMsgComprovanteSaidaDTO.getCdRecurso());
		setRecursoDesc(detalharLinhaMsgComprovanteSaidaDTO.getDsRecurso());
		setIdioma(detalharLinhaMsgComprovanteSaidaDTO.getCdIdioma());
		setIdiomaDesc(detalharLinhaMsgComprovanteSaidaDTO.getDsIdioma());
		setCodigoMensagem(String.valueOf(detalharLinhaMsgComprovanteSaidaDTO.getCdcodigoMensagem()));
		setCodigoMensagemDesc(detalharLinhaMsgComprovanteSaidaDTO.getCdcodigoMensagem() != 0 ? detalharLinhaMsgComprovanteSaidaDTO.getCdcodigoMensagem() + " - " + detalharLinhaMsgComprovanteSaidaDTO.getDsCodigoMensagem():"");
		setOrdemLinhaMensagem(detalharLinhaMsgComprovanteSaidaDTO.getOrdemLinhaMensagem());
		setUsuarioInclusao(detalharLinhaMsgComprovanteSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharLinhaMsgComprovanteSaidaDTO.getUsuarioManutencao());
		setComplementoInclusao(detalharLinhaMsgComprovanteSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharLinhaMsgComprovanteSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharLinhaMsgComprovanteSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharLinhaMsgComprovanteSaidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(detalharLinhaMsgComprovanteSaidaDTO.getCdTipoCanalInclusao()==0? "" : detalharLinhaMsgComprovanteSaidaDTO.getCdTipoCanalInclusao() + " - " + detalharLinhaMsgComprovanteSaidaDTO.getDsTipoCanalInclusao());
		setTipoCanalManutencao(detalharLinhaMsgComprovanteSaidaDTO.getCdTipoCanalManutencao()==0? "" : detalharLinhaMsgComprovanteSaidaDTO.getCdTipoCanalManutencao() + " - " + detalharLinhaMsgComprovanteSaidaDTO.getDsTipoCanalManutencao());
		setDataHoraInclusao(detalharLinhaMsgComprovanteSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharLinhaMsgComprovanteSaidaDTO.getDataHoraManutencao());
		setCentroCustoDesc(detalharLinhaMsgComprovanteSaidaDTO.getPrefixo());
		
	}

	/**
	 * Get: tipoMensagemFiltro.
	 *
	 * @return tipoMensagemFiltro
	 */
	public Integer getTipoMensagemFiltro() {
		return tipoMensagemFiltro;
	}

	/**
	 * Set: tipoMensagemFiltro.
	 *
	 * @param tipoMensagemFiltro the tipo mensagem filtro
	 */
	public void setTipoMensagemFiltro(Integer tipoMensagemFiltro) {
		this.tipoMensagemFiltro = tipoMensagemFiltro;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}

	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}

	/**
	 * Get: centroCustoDesc.
	 *
	 * @return centroCustoDesc
	 */
	public String getCentroCustoDesc() {
		return centroCustoDesc;
	}

	/**
	 * Set: centroCustoDesc.
	 *
	 * @param centroCustoDesc the centro custo desc
	 */
	public void setCentroCustoDesc(String centroCustoDesc) {
		this.centroCustoDesc = centroCustoDesc;
	}

	/**
	 * Get: centroCustoPesq.
	 *
	 * @return centroCustoPesq
	 */
	public String getCentroCustoPesq() {
		return centroCustoPesq;
	}

	/**
	 * Set: centroCustoPesq.
	 *
	 * @param centroCustoPesq the centro custo pesq
	 */
	public void setCentroCustoPesq(String centroCustoPesq) {
		this.centroCustoPesq = centroCustoPesq;
	}

	/**
	 * Buscar centro custo.
	 */
	public void buscarCentroCusto(){
		
		if (getCentroCustoPesq()!=null && !getCentroCustoPesq().equals("")){
			
			this.listaCentroCusto= new ArrayList<SelectItem>();
			
			List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
			CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();
			
			centroCustoEntradaDTO.setCdSistema(getCentroCustoPesq());
			
			listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);
			
			listaCentroCustoHash.clear();
			for(CentroCustoSaidaDTO combo : listaCentroCustoSaida){
				listaCentroCustoHash.put(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-"+combo.getDsCentroCusto());
				this.listaCentroCusto.add(new SelectItem(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-"+combo.getDsCentroCusto()));
			}
		}else{
			setListaCentroCusto(new ArrayList<SelectItem>());
		}	
			
	}

	/**
	 * Get: listaCentroCusto.
	 *
	 * @return listaCentroCusto
	 */
	public List<SelectItem> getListaCentroCusto() {
		return listaCentroCusto;
	}

	/**
	 * Set: listaCentroCusto.
	 *
	 * @param listaCentroCusto the lista centro custo
	 */
	public void setListaCentroCusto(List<SelectItem> listaCentroCusto) {
		this.listaCentroCusto = listaCentroCusto;
	}

	/**
	 * Get: listaTipoMensagem.
	 *
	 * @return listaTipoMensagem
	 */
	public List<SelectItem> getListaTipoMensagem() {
		return listaTipoMensagem;
	}

	/**
	 * Set: listaTipoMensagem.
	 *
	 * @param listaTipoMensagem the lista tipo mensagem
	 */
	public void setListaTipoMensagem(List<SelectItem> listaTipoMensagem) {
		this.listaTipoMensagem = listaTipoMensagem;
	}

	/**
	 * Get: entradaMensagem.
	 *
	 * @return entradaMensagem
	 */
	public ConsultarDescricaoMensagemEntradaDTO getEntradaMensagem() {
		return entradaMensagem;
	}

	/**
	 * Set: entradaMensagem.
	 *
	 * @param entradaMensagem the entrada mensagem
	 */
	public void setEntradaMensagem(
			ConsultarDescricaoMensagemEntradaDTO entradaMensagem) {
		this.entradaMensagem = entradaMensagem;
	}

	/**
	 * Get: retornoLista.
	 *
	 * @return retornoLista
	 */
	public ListarLinhaMsgComprovanteSaidaDTO getRetornoLista() {
		return retornoLista;
	}

	/**
	 * Set: retornoLista.
	 *
	 * @param retornoLista the retorno lista
	 */
	public void setRetornoLista(ListarLinhaMsgComprovanteSaidaDTO retornoLista) {
		this.retornoLista = retornoLista;
	}
	
	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: retornoMensagem.
	 *
	 * @return retornoMensagem
	 */
	public ConsultarDescricaoMensagemSaidaDTO getRetornoMensagem() {
		return retornoMensagem;
	}

	/**
	 * Set: retornoMensagem.
	 *
	 * @param retornoMensagem the retorno mensagem
	 */
	public void setRetornoMensagem(
			ConsultarDescricaoMensagemSaidaDTO retornoMensagem) {
		this.retornoMensagem = retornoMensagem;
	}


	
}
