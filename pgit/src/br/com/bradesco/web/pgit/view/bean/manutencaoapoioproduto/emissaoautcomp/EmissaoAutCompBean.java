/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.emissaoautcomp
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.emissaoautcomp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListaTipoServicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListaTipoServicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeContratoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoModalidadeEmissaoAvisoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoModalidadeEmissaoAvisoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.IEmissaoAutCompService;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.BloqDesbloqEmissaoAutAvisosComprvEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.BloqDesbloqEmissaoAutAvisosComprvSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharHistEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.DetalharHistEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ExcluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ExcluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.IncluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.IncluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarHistEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarHistEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarIncluirEmissaoAutCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarIncluirEmissaoAutCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarTipoServicoModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.ListarTipoServicoModalidadeOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean.OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: EmissaoAutCompBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmissaoAutCompBean {

    // Constante Utilizada para evitar redund�ncia apontada pelo Sonar
    /** Atributo EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES. */
    private static final String EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES = "conEmissaoAutomaticaAvisosComprovantes";

    // Vaviaveis Pesquisar!
    /** Atributo servicoEmissaoFiltro. */
    private Integer servicoEmissaoFiltro;

    /** Atributo tipoServicoFiltro. */
    private Integer tipoServicoFiltro;
    
    /** Atributo tipoServicoFiltroInclusao. */
    private Integer tipoServicoFiltroInclusao;

    /** Atributo listaTipoServicoFiltro. */
    private List<SelectItem> listaTipoServicoFiltro;

    /** Atributo listaTipoServicoFiltroHash. */
    private Map<Integer, String> listaTipoServicoFiltroHash = new HashMap<Integer, String>();

    /** Atributo modalidadeServicoFiltro. */
    private Integer modalidadeServicoFiltro;

    /** Atributo listaModalidadeServicoFiltro. */
    private List<SelectItem> listaModalidadeServicoFiltro;
    
    /** Atributo listaTipoModalidade. */
    private List<SelectItem> listaTipoModalidade;
    
    /** Atributo listaTipoModalidadeHash. */
    private Map<Integer, String> listaTipoModalidadeHash = new HashMap<Integer, String>();

    /** Atributo listaModalidadeServicoFiltroHash. */
    private Map<Integer, ListarServicoModalidadeEmissaoAvisoSaidaDTO> listaModalidadeServicoFiltroHash = new HashMap<Integer, ListarServicoModalidadeEmissaoAvisoSaidaDTO>();

    /** Atributo listaPesquisa. */
    private List<ListarEmissaoAutCompSaidaDTO> listaPesquisa;

    /** Atributo listaPesquisaInclusao. */
    private List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listaPesquisaInclusao;
    
    /** Atributo itemSelecionadoLista. */
    private Integer itemSelecionadoLista;

    /** Atributo listaPesquisaControle. */
    private List<SelectItem> listaPesquisaControle;

    /** Atributo identificacaoClienteBean. */
    private IdentificacaoClienteBean identificacaoClienteBean;

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo listaEmpresaGestora. */
    private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();

    /** Atributo tipoContrato. */
    private List<SelectItem> tipoContrato = new ArrayList<SelectItem>();

    /** Atributo emissaoAutCompServiceImpl. */
    private IEmissaoAutCompService emissaoAutCompServiceImpl;

    /** Atributo btoAcionado. */
    private Boolean btoAcionado;

    /** Atributo btoAcionadoHistorico. */
    private Boolean btoAcionadoHistorico;

    /** Atributo saidaDetalharEmissao. */
    private DetalharEmissaoAutCompSaidaDTO saidaDetalharEmissao = new DetalharEmissaoAutCompSaidaDTO();
    
    /** Atributo listaTipoServicoContratoEntradaDTO. */
    private ListaTipoServicoContratoEntradaDTO listaTipoServicoContratoEntradaDTO;

    /** Atributo listarTipoServicoModalidadeEmissaoAvisoEntradaDTO. */
    private ListarServicoModalidadeEmissaoAvisoEntradaDTO listarTipoServicoModalidadeEmissaoAvisoEntradaDTO;

    // Fim Variaveis Pesquisar!

    // Variaveis Incluir
    /** Atributo servicoEmissao. */
    private Integer servicoEmissao;

    /** Atributo listaServicoEmissao. */
    private List<SelectItem> listaServicoEmissao;

    /** Atributo listaServicoEmissaoHash. */
    private Map<Integer, String> listaServicoEmissaoHash = new HashMap<Integer, String>();

    /** Atributo tipoServico. */
    private Integer tipoServico;

    /** Atributo listaTipoServico. */
    private List<SelectItem> listaTipoServico;

    /** Atributo listaTipoServicoHash. */
    private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

    /** Atributo modalidadeServico. */
    private Integer modalidadeServico;

    /** Atributo listaModalidadeServico. */
    private List<SelectItem> listaModalidadeServico;

    /** Atributo listaModalidadeServicoHash. */
    private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO>();

    /** Atributo listaPesquisaIncluir. */
    private List<ListarIncluirEmissaoAutCompSaidaDTO> listaPesquisaIncluir;

    /** Atributo listaPesquisaIncluir2. */
    private List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listaPesquisaIncluir2 = new ArrayList<ListarTipoServicoModalidadeOcorrenciaSaidaDTO>();

    /** Atributo listaModalidades. */
    private List<ListarServicoModalidadeEmissaoAvisoSaidaDTO> listaModalidades = new ArrayList<ListarServicoModalidadeEmissaoAvisoSaidaDTO>();
    
    /** Atributo listaControleCheckIncluir. */
    private List<SelectItem> listaControleCheckIncluir;

    /** Atributo btnAvancar. */
    private boolean btnAvancar = false;

    /** Atributo obrigatoriedade. */
    private String obrigatoriedade;

    /** Atributo servicoEmissaoDesc. */
    private String servicoEmissaoDesc;

    /** Atributo tipoServicoDesc. */
    private String tipoServicoDesc;

    /** Atributo modalidadeServicoDesc. */
    private String modalidadeServicoDesc;

    /** Atributo situacaoVinculacao. */
    private int situacaoVinculacao;

    /** Atributo situacaoVinculacaoDesc. */
    private String situacaoVinculacaoDesc;

    /** Atributo situacaoVinculacaoHistorico. */
    private int situacaoVinculacaoHistorico;

    /** Atributo selecionarTodos. */
    private boolean selecionarTodos;

    /** Atributo bloquearDesbloquear. */
    private String bloquearDesbloquear;

    // Fim Variaveis Incluir

    // Variaveis Historico
    /** Atributo listaPesquisaHistorico. */
    private List<ListarHistEmissaoAutCompSaidaDTO> listaPesquisaHistorico;

    /** Atributo itemSelecionadoListaHistorico. */
    private Integer itemSelecionadoListaHistorico;

    /** Atributo listaPesquisaHistoricoControle. */
    private List<SelectItem> listaPesquisaHistoricoControle;

    /** Atributo dataInicial. */
    private Date dataInicial;

    /** Atributo dataFinal. */
    private Date dataFinal;

    /** Atributo dataManutencao. */
    private String dataManutencao;

    /** Atributo responsavel. */
    private String responsavel;

    /** Atributo tipoManutencao. */
    private int tipoManutencao;

    /** Atributo dsSituacaoVinc. */
    private String dsSituacaoVinc;

    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;

    // Fim Variaveis Historico

    // VARIAVEIS TRILHA
    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    /** Atributo desabilitaExcluir. */
    private Integer desabilitaExcluir = 2;
    
    /** Atributo listarHistEmissaoAutCompSaidaDTO. */
    private ListarHistEmissaoAutCompSaidaDTO listarHistEmissaoAutCompSaidaDTO = new ListarHistEmissaoAutCompSaidaDTO();

    // FIM VARIAVEIS TRILHA

    // Metodos!

	/**
     * Limpar dados cliente.
     *
     * @return the string
     */
    public String limparDadosCliente() {
	identificacaoClienteBean.limparCliente();
	limparArgumentosPesquisa();
	return "";
    }

    /**
     * Limpar argumentos pesquisa.
     *
     * @return the string
     */
    public String limparArgumentosPesquisa() {
	setServicoEmissaoFiltro(0);
	setTipoServicoFiltro(0);
	setModalidadeServicoFiltro(0);
	limparGrid();
	setBtoAcionado(false);
	return "";
    }

    /**
     * Limpar grid.
     */
    public void limparGrid() {
	listaPesquisa = new ArrayList<ListarEmissaoAutCompSaidaDTO>();
	setItemSelecionadoLista(null);
    }

    /**
     * Limpar dados contrato.
     *
     * @return the string
     */
    public String limparDadosContrato() {
	identificacaoClienteBean.limparContrato();
	limparArgumentosPesquisa();
	return "";
    }
    
    /**
     * Limpar dados contrato servico emissao.
     */
    public void limparDadosContratoServicoEmissao() {
    	identificacaoClienteBean.setDesabilataFiltro(true);
    	identificacaoClienteBean.setContratoSelecionado(false);		
		identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().setDsPessoaJuridicaContrato("");
		identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().setDsTipoContratoNegocio("");
		identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().setNrSeqContratoNegocio(null);
		identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().setDsContrato("");
		identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().setDsSituacaoContratoNegocio("");
    }

    /**
     * Habilita botao avancar.
     */
    public void habilitaBotaoAvancar() {

	setBtnAvancar(false);
	for (int i = 0; i < getListaPesquisaInclusao().size(); i++) {
	    if (getListaPesquisaInclusao().get(i).isCheck()) {
		setBtnAvancar(true);
		break;
	    }
	}
    }

    /**
     * Limpar bloq desblo.
     *
     * @return the string
     */
    public String limparBloqDesblo() {
	setBloquearDesbloquear("");
	return "";
    }

    /**
     * Confirmar bloq desblo.
     *
     * @return the string
     */
    public String confirmarBloqDesblo() {
	try {

	    BloqDesbloqEmissaoAutAvisosComprvEntradaDTO entradaDTO = new BloqDesbloqEmissaoAutAvisosComprvEntradaDTO();
	    ListarEmissaoAutCompSaidaDTO itemSelecionado = getListaPesquisa().get(getItemSelecionadoLista());
	    List<OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO> listaEntrada = new ArrayList<OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO>();

	    entradaDTO.setCdPessoaJuridicaContrato(getIdentificacaoClienteBean().getSaidaConsultarListaContratosPessoas()
		    .getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(getIdentificacaoClienteBean().getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
	    entradaDTO
		    .setNrSequenciaContratoNegocio(getIdentificacaoClienteBean().getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
	    entradaDTO.setCdProdutoServicoVinculo(getServicoEmissaoFiltro());

	    OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO ocorrenciasDTO = new OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO();
	    ocorrenciasDTO.setCdProdutoOperacaoRelacionado(itemSelecionado.getCdModalidadeServico());
	    ocorrenciasDTO.setCdProdutoServicoOperacao(itemSelecionado.getCdTipoServico());
	    ocorrenciasDTO.setCdRelacionamentoProdutoProduto(0);
	    ocorrenciasDTO.setCdSituacaoVinculacaoAviso(Integer.valueOf(getBloquearDesbloquear()));

	    listaEntrada.add(ocorrenciasDTO);

	    entradaDTO.setListaOcorrencias(listaEntrada);

	    BloqDesbloqEmissaoAutAvisosComprvSaidaDTO saidaDTO = getEmissaoAutCompServiceImpl().bloqDesbloqEmissaoAutAvisosComprv(entradaDTO);

	    BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
		    "conEmissaoAutomaticaAvisosComprovantes", BradescoViewExceptionActionType.ACTION, false);
	    carregaLista();

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
	}

	return "";
    }

    /**
     * Checar todos grid.
     */
    public void checarTodosGrid() {
		for (int i = 0; i < (this.listaPesquisaInclusao.size()); i++) {
		    this.listaPesquisaInclusao.get(i).setCheck(this.isSelecionarTodos());
		}
	
		this.btnAvancar = isSelecionarTodos();
    }

    /**
     * Limpar.
     *
     * @return the string
     */
    public String limpar() {
		identificacaoClienteBean.limparCliente();
		identificacaoClienteBean.limparContrato();
		limparArgumentosPesquisa();
		
		return "";
    }

    /**
     * Limpar pagina.
     *
     * @param evt the evt
     */
    public void limparPagina(ActionEvent evt) {

		if (identificacaoClienteBean == null) {
		    identificacaoClienteBean = new IdentificacaoClienteBean();
		}
	
		identificacaoClienteBean.setPaginaCliente("identificacaoEmissaoAutomaticaAvisosComprovantesCliente");
		identificacaoClienteBean.setPaginaContrato("identificacaoEmissaoAutomaticaAvisosComprovantesContrato");
		identificacaoClienteBean.setPaginaRetorno(EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES);
		identificacaoClienteBean.setItemClienteSelecionado(null);
		identificacaoClienteBean.setItemContratoSelecionado(null);
	
		setServicoEmissaoFiltro(null);
		setTipoServicoFiltro(null);
		setModalidadeServicoFiltro(null);
		setListaPesquisa(null);
		setListaPesquisaControle(null);
		setItemSelecionadoLista(null);
	
		this.setListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setTipoContrato(new ArrayList<SelectItem>());
		this.setListaServicoEmissao(new ArrayList<SelectItem>());
		this.setListaTipoServicoFiltro(new ArrayList<SelectItem>());
		this.setListaModalidadeServicoFiltro(new ArrayList<SelectItem>());
		setListaTipoModalidade(new ArrayList<SelectItem>());
	
		limparDados();
		listarEmpresaGestora();
		listarTipoContrato();
	
		setServicoEmissao(null);
		setTipoServico(null);
		setModalidadeServico(null);
		setListaPesquisaIncluir(null);
		setListaControleCheckIncluir(null);
		setBtnAvancar(false);
		setObrigatoriedade("");
		setTipoServicoDesc("");
		setModalidadeServicoDesc("");
		setSituacaoVinculacao(0);
		
		setListaPesquisaHistorico(null);
		setListaPesquisaHistoricoControle(null);
		setItemSelecionadoListaHistorico(null);
		setDataFinal(new Date());
		setDataInicial(new Date());
		setDataManutencao("");
		setResponsavel("");
		setTipoManutencao(0);
		setBtoAcionado(false);
		identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(2269651L);

    }

    /**
     * Listar servico emissao.
     */
    public void listarServicoEmissao() {
    	this.listaServicoEmissao = new ArrayList<SelectItem>();
    	try {
    	    List<ListaTipoServicoContratoSaidaDTO> list = new ArrayList<ListaTipoServicoContratoSaidaDTO>();
    	    listaTipoServicoContratoEntradaDTO = new ListaTipoServicoContratoEntradaDTO();

    	    listaTipoServicoContratoEntradaDTO.setCdNaturezaOperacaoPagamento(2);
    	    listaTipoServicoContratoEntradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
    	    listaTipoServicoContratoEntradaDTO.setCdProdutoOperacaoRelacionado(0);
    	    listaTipoServicoContratoEntradaDTO.setCdprodutoServicoOperacao(0);
    	    listaTipoServicoContratoEntradaDTO.setCdTipoContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
    	    listaTipoServicoContratoEntradaDTO.setNrSequenciaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
    	    listaTipoServicoContratoEntradaDTO.setNumeroOcorrencias(50);

    	    list = comboService.listarTipoServicoContrato(listaTipoServicoContratoEntradaDTO);
    	    listaServicoEmissao.clear();
    	    for (ListaTipoServicoContratoSaidaDTO combo : list) {
	    	listaServicoEmissaoHash.put(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado());
    		listaServicoEmissao.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));
    	    }
    	} catch (PdcAdapterFunctionalException e) {
    		if("PGIT1082".equals(StringUtils.right(e.getCode(), 8))  ){
	    		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(),"#{emissaoAutomaticaBean.limparDadosContratoServicoEmissao}",BradescoViewExceptionActionType.ACTION, false);
    		}else{
    			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
    		}
    		listaServicoEmissao = new ArrayList<SelectItem>();
    	}
    	
    }

    /**
     * Listar servico emissao contrato selecionado.
     */
    public void listarServicoEmissaoContratoSelecionado() {
    	this.listaServicoEmissao = new ArrayList<SelectItem>();
    	try {
    		List<ListaTipoServicoContratoSaidaDTO> list = new ArrayList<ListaTipoServicoContratoSaidaDTO>();
    		listaTipoServicoContratoEntradaDTO = new ListaTipoServicoContratoEntradaDTO();
    		
    		listaTipoServicoContratoEntradaDTO.setCdNaturezaOperacaoPagamento(2);
    		listaTipoServicoContratoEntradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteBean.getListaConsultarListaContratosPessoas().get(identificacaoClienteBean.getItemContratoSelecionado()).getCdPessoaJuridicaContrato());
    		listaTipoServicoContratoEntradaDTO.setCdProdutoOperacaoRelacionado(0);
    		listaTipoServicoContratoEntradaDTO.setCdprodutoServicoOperacao(0);
    		listaTipoServicoContratoEntradaDTO.setCdTipoContrato(identificacaoClienteBean.getListaConsultarListaContratosPessoas().get(identificacaoClienteBean.getItemContratoSelecionado()).getCdTipoContratoNegocio());
    		listaTipoServicoContratoEntradaDTO.setNrSequenciaContrato(identificacaoClienteBean.getListaConsultarListaContratosPessoas().get(identificacaoClienteBean.getItemContratoSelecionado()).getNrSeqContratoNegocio());
    		listaTipoServicoContratoEntradaDTO.setNumeroOcorrencias(50);
    		
    		list = comboService.listarTipoServicoContrato(listaTipoServicoContratoEntradaDTO);
    		listaServicoEmissao.clear();
    		for (ListaTipoServicoContratoSaidaDTO combo : list) {
    			listaServicoEmissaoHash.put(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado());
    			listaServicoEmissao.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));
    		}
    	} catch (PdcAdapterFunctionalException e) {
    		if("PGIT1082".equals(StringUtils.right(e.getCode(), 8))  ){
    			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(),"#{emissaoAutomaticaBean.limparDadosContratoServicoEmissao}",BradescoViewExceptionActionType.ACTION, false);
    		}else{
    			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
    		}
    		listaServicoEmissao = new ArrayList<SelectItem>();
    	}
    	
    }
    
    /**
     * Listar servico modalidade emissao.
     */
    public void listarServicoModalidadeEmissao() {
    	this.listaServicoEmissao = new ArrayList<SelectItem>();
    	setTipoServicoFiltro(0);
    	setModalidadeServicoFiltro(0);
    	try {
    		List<ListarServicoModalidadeEmissaoAvisoSaidaDTO> list = new ArrayList<ListarServicoModalidadeEmissaoAvisoSaidaDTO>();
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO = new ListarServicoModalidadeEmissaoAvisoEntradaDTO();
    		
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdNaturezaOperacaoPagamento(1);
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdProdutoServicoEmissao(0);
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdProdutoServicoOperacao(0);
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdTipoContrato(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setNrSequenciaContrato(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
    		listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setNrOcorrencias(50);
    		
    		list = comboService.listarServicoModalidadeEmissaoAviso(listarTipoServicoModalidadeEmissaoAvisoEntradaDTO);
    		listaServicoEmissao.clear();
    		for (ListarServicoModalidadeEmissaoAvisoSaidaDTO combo : list) {
    			listaServicoEmissaoHash.put(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado());
    			listaServicoEmissao.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));
    		}
    	} catch (PdcAdapterFunctionalException e) {
    		if("PGIT1082".equals(StringUtils.right(e.getCode(), 8))  ){
    			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(),"#{emissaoAutomaticaBean.limparDadosContratoServicoEmissao}",BradescoViewExceptionActionType.ACTION, false);
    		}else{
    			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
    		}
    		listaServicoEmissao = new ArrayList<SelectItem>();
    	}
    	
    }
    
    /**
     * Consultar contrato.
     *
     * @return the string
     */
    public String consultarContrato() {
    	String retorno = "";
   		identificacaoClienteBean.consultarContratos();
   		if(identificacaoClienteBean.getListaConsultarListaContratosPessoas() != null){
	   		if(identificacaoClienteBean.getListaConsultarListaContratosPessoas().size() != 0 && identificacaoClienteBean.getListaConsultarListaContratosPessoas().size() != 1){
	   			retorno =  identificacaoClienteBean.getPaginaContrato();
	   		}else{
	   			listarServicoEmissao();
	   		}
	   		
	   		return retorno;
   		}   
   		return retorno;
	}
    
    /**
     * Selecionar contrato.
     *
     * @return the string
     */
    public String selecionarContrato(){
    	String retorno = "";
   		identificacaoClienteBean.selecionarContrato();
   		retorno =  identificacaoClienteBean.getPaginaRetorno();
   		listarServicoEmissaoContratoSelecionado();
   		return retorno;
    }
    
    
    /**
     * Listar tipo servico filtro.
     */
    public void listarTipoServicoFiltro() {
    	this.listaTipoServicoFiltro = new ArrayList<SelectItem>();
    	setModalidadeServicoFiltro(0);
    	setTipoServicoFiltro(0);
    	setTipoServicoFiltroInclusao(0);
    	setModalidadeServico(0);
    	
    	try {
    	    List<ListarServicoModalidadeEmissaoAvisoSaidaDTO> list = new ArrayList<ListarServicoModalidadeEmissaoAvisoSaidaDTO>();
    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO = new ListarServicoModalidadeEmissaoAvisoEntradaDTO();

    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdNaturezaOperacaoPagamento(1);
    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdTipoContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setNrSequenciaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdProdutoServicoEmissao(servicoEmissaoFiltro);
    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setCdProdutoServicoOperacao(0);
    	    listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.setNrOcorrencias(50);
    	    
    	    
    	    list = comboService.listarServicoModalidadeEmissaoAviso(listarTipoServicoModalidadeEmissaoAvisoEntradaDTO);
    	    listaTipoServicoFiltro.clear();
    	    for (ListarServicoModalidadeEmissaoAvisoSaidaDTO combo : list) {
    		listaTipoServicoHash.put(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado());
    		listaTipoServicoFiltro.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));
    	    }
    	} catch (PdcAdapterFunctionalException e) {
    		listaTipoServicoFiltro = new ArrayList<SelectItem>();
    	}
    }

    

    /**
     * Listar tipo servico.
     */
    public void listarTipoServico() {

	this.listaTipoServico = new ArrayList<SelectItem>();

	try {
	    List<ListarServicosSaidaDTO> list = new ArrayList<ListarServicosSaidaDTO>();

	    list = comboService.listarTipoServicos(1);
	    listaTipoServico.clear();
	    for (ListarServicosSaidaDTO combo : list) {
		listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
		listaTipoServico.add(new SelectItem(combo.getCdServico(), combo.getDsServico()));
	    }
	} catch (PdcAdapterFunctionalException e) {
	    listaTipoServico = new ArrayList<SelectItem>();
	}
    }

    /**
     * Listar modalidade servico filtro.
     */
    public void listarModalidadeServicoFiltro() {
		setModalidadeServicoFiltro(0);
		listaTipoModalidade = new ArrayList<SelectItem>();
		listaModalidades = new ArrayList<ListarServicoModalidadeEmissaoAvisoSaidaDTO>();
		
		if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0) {
	
			ListarServicoModalidadeEmissaoAvisoEntradaDTO listarModalidadeEntradaDTO = new ListarServicoModalidadeEmissaoAvisoEntradaDTO();
	
		    listarModalidadeEntradaDTO.setCdNaturezaOperacaoPagamento(1);
		    listarModalidadeEntradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		    listarModalidadeEntradaDTO.setCdTipoContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		    listarModalidadeEntradaDTO.setNrSequenciaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		    listarModalidadeEntradaDTO.setCdProdutoServicoOperacao(tipoServicoFiltro);
		    listarModalidadeEntradaDTO.setCdProdutoServicoEmissao(servicoEmissaoFiltro);
		    listarModalidadeEntradaDTO.setNrOcorrencias(50);
		    listaModalidades = comboService.listarServicoModalidadeEmissaoAviso(listarModalidadeEntradaDTO);
	
		    listaTipoModalidadeHash.clear();
		    listaModalidadeServicoFiltroHash.clear();
		    for (ListarServicoModalidadeEmissaoAvisoSaidaDTO combo : listaModalidades) {
		    	listaTipoModalidade.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));
		    	listaTipoModalidadeHash.put(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado());
		    	listaModalidadeServicoFiltroHash.put(combo.getCdProdutoOperacaoRelacionado(), combo);
		    	}
		     } else {
				listaTipoModalidadeHash.clear();
				listaTipoModalidade = new ArrayList<SelectItem>();
				setTipoServicoFiltro(0);
		    }	
    }
    
    /**
     * Listar modalidade servico filtro inclusao.
     */
    public void listarModalidadeServicoFiltroInclusao() {
		setModalidadeServicoFiltro(0);
		listaTipoModalidade = new ArrayList<SelectItem>();
		listaModalidades = new ArrayList<ListarServicoModalidadeEmissaoAvisoSaidaDTO>();
	
		if (getTipoServicoFiltroInclusao() != null && getTipoServicoFiltroInclusao() != 0) {
	
			ListarServicoModalidadeEmissaoAvisoEntradaDTO listarModalidadeEntradaDTO = new ListarServicoModalidadeEmissaoAvisoEntradaDTO();
			
		    listarModalidadeEntradaDTO.setCdNaturezaOperacaoPagamento(1);
		    listarModalidadeEntradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		    listarModalidadeEntradaDTO.setCdTipoContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		    listarModalidadeEntradaDTO.setNrSequenciaContrato(identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		    listarModalidadeEntradaDTO.setCdProdutoServicoOperacao(tipoServicoFiltroInclusao);
		    listarModalidadeEntradaDTO.setCdProdutoServicoEmissao(servicoEmissaoFiltro);
		    listarModalidadeEntradaDTO.setNrOcorrencias(50);
		    listaModalidades = comboService.listarServicoModalidadeEmissaoAviso(listarModalidadeEntradaDTO);
	
		    listaTipoModalidadeHash.clear();
		    listaModalidadeServicoFiltroHash.clear();
		    for (ListarServicoModalidadeEmissaoAvisoSaidaDTO combo : listaModalidades) {
		    	listaTipoModalidade.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));
		    	listaTipoModalidadeHash.put(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado());
		    	listaModalidadeServicoFiltroHash.put(combo.getCdProdutoOperacaoRelacionado(), combo);
		    	}
		     } else {
				listaTipoModalidadeHash.clear();
				listaTipoModalidade = new ArrayList<SelectItem>();
				setTipoServicoFiltro(0);
		    }		
    }

    /**
     * Listar modalidade servico.
     */
    public void listarModalidadeServico() {

	listaModalidadeServico = new ArrayList<SelectItem>();

	if (getTipoServico() != null && getTipoServico() != 0) {

	    List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
	    ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();

	    listarModalidadeEntradaDTO.setCdServico(getTipoServico());

	    listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);

	    listaModalidadeServicoHash.clear();
	    for (ListarModalidadeSaidaDTO combo : listaModalidades) {
		listaModalidadeServicoHash.put(combo.getCdModalidade(), combo);
		this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(), combo.getDsModalidade()));
	    }
	} else {

	    listaModalidadeServicoHash.clear();
	    listaModalidadeServico.clear();
	    setTipoServico(0);
	}
    }

    /**
     * Iniciar tela incluir.
     */
    public void iniciarTelaIncluir() {
	setListaTipoServico(new ArrayList<SelectItem>());
	setListaModalidadeServico(new ArrayList<SelectItem>());
	listarTipoServico();
    }

    /**
     * Limpar dados.
     *
     * @return the string
     */
    public String limparDados() {
		setServicoEmissaoFiltro(0);
		setTipoServicoFiltro(0);
		setModalidadeServicoFiltro(0);
		setListaPesquisa(null);
		setListaPesquisaControle(null);
		setItemSelecionadoLista(null);
	
		this.identificacaoClienteBean.setBloqueaRadio(false);
		this.identificacaoClienteBean.setItemFiltroSelecionado("");
		this.identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
		this.identificacaoClienteBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteBean.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.identificacaoClienteBean.setDesabilataFiltro(true);
		setBtoAcionado(false);
		
		return EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES;
    }

    /**
     * Listar empresa gestora.
     */
    public void listarEmpresaGestora() {
	try {
	    this.listaEmpresaGestora = new ArrayList<SelectItem>();
	    List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();

	    list = comboService.listarEmpresasGestoras();
	    listaEmpresaGestora.clear();
	    for (EmpresaGestoraSaidaDTO combo : list) {
		listaEmpresaGestora.add(new SelectItem(combo.getCdPessoaJuridicaContrato(), combo.getDsCnpj()));
	    }
	} catch (PdcAdapterFunctionalException e) {
	    listaEmpresaGestora = new ArrayList<SelectItem>();
	}

    }

    /**
     * Listar tipo contrato.
     */
    public void listarTipoContrato() {
	try {
	    List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();

	    for (TipoContratoSaidaDTO saida : list) {
		tipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
	    }
	} catch (PdcAdapterFunctionalException e) {
	    tipoContrato = new ArrayList<SelectItem>();
	}
    }

    /**
     * Carrega lista.
     */
    public void carregaLista() {
    	listaPesquisa = new ArrayList<ListarEmissaoAutCompSaidaDTO>();
	
	    try {

		ListarEmissaoAutCompEntradaDTO entradaDTO = new ListarEmissaoAutCompEntradaDTO();

		entradaDTO.setContratoPssoaJuridContr(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
			.getCdPessoaJuridicaContrato());
		entradaDTO
			.setContratoTpoContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaDTO.setContratoSegContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());

		entradaDTO
			.setServicoEmissaoContratado(getServicoEmissaoFiltro() != null && getServicoEmissaoFiltro() != 0 ? getServicoEmissaoFiltro()
				: 0);
		entradaDTO.setQuantidadeOcorrenciaEnviadasInc(0);
		entradaDTO.setTipoServico(getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0 ? getTipoServicoFiltro() : 0);
		entradaDTO
			.setModalidadeServico(getModalidadeServicoFiltro() != null && getModalidadeServicoFiltro() != 0 ? getModalidadeServicoFiltro()
				: 0);
		entradaDTO
			.setTipoRelacionamento(getModalidadeServicoFiltro() != null && getModalidadeServicoFiltro() != 0 ? getListaModalidadeServicoFiltroHash().get(modalidadeServicoFiltro).getCdRelacionamentoProdutoProduto() : 0);

		setListaPesquisa(getEmissaoAutCompServiceImpl().listarEmissaoAutComp(entradaDTO));
		setBtoAcionado(true);
		this.listaPesquisaControle = new ArrayList<SelectItem>();

		for (int i = 0; i < getListaPesquisa().size(); i++) {
		    listaPesquisaControle.add(new SelectItem(i, " "));
		}

		this.identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
		this.setItemSelecionadoLista(null);

	    } catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		setListaPesquisa(null);
		setItemSelecionadoLista(null);
	    }
	}

    

    /**
     * Bloquear desbloquear.
     *
     * @return the string
     */
    public String bloquearDesbloquear() {
	try {
	    preencheDados();
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES, BradescoViewExceptionActionType.ACTION, false);
	    return null;
	}
	return "BLOQDESBLOQ";
    }

    /**
     * Limpar dados incluir.
     */
    public void limparDadosIncluir() {
	setServicoEmissao(0);
	setServicoEmissaoFiltro(0);
	setTipoServicoFiltroInclusao(0);
	setModalidadeServico(0);
	setListaPesquisaIncluir(null);
	setListaControleCheckIncluir(null);
	setSelecionarTodos(false);
	setBtnAvancar(false);
	listaPesquisaInclusao=  new ArrayList<ListarTipoServicoModalidadeOcorrenciaSaidaDTO>();
    }

    /**
     * Limpar dados historico.
     */
    public void limparDadosHistorico() {
	setDataInicial(new Date());
	setDataFinal(new Date());
	setBtoAcionadoHistorico(false);
	
	setListaPesquisaHistorico(null);
	setItemSelecionadoListaHistorico(null);
	
    }

    /**
     * Carrega lista incluir.
     */
    public void carregaListaIncluir() {
    listaPesquisaInclusao = new ArrayList<ListarTipoServicoModalidadeOcorrenciaSaidaDTO>();
	if (getObrigatoriedade().equals("V")) {
	    try {

	    	ListarTipoServicoModalidadeEntradaDTO entradaDTO = new ListarTipoServicoModalidadeEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		entradaDTO.setCdProdutoServicoContrato(getServicoEmissaoFiltro() != null && getServicoEmissaoFiltro() != 0 ? getServicoEmissaoFiltro(): 0);
		entradaDTO.setCdProdutoServicoOperacao(tipoServicoFiltroInclusao != null && tipoServicoFiltroInclusao != 0 ? tipoServicoFiltroInclusao : 0);
		entradaDTO.setCdProdutoOperacaoRelacionado(modalidadeServico != null && modalidadeServico != 0 ? modalidadeServico	: 0);

		setListaPesquisaInclusao(getEmissaoAutCompServiceImpl().listarTipoServicoModalidade(entradaDTO));
	
		this.listaControleCheckIncluir = new ArrayList<SelectItem>();

		for (int i = 0; i < getListaPesquisaInclusao().size(); i++) {
		    listaControleCheckIncluir.add(new SelectItem(i, " "));
		}

	    } catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		setListaPesquisaIncluir(null);

	    }
	}

	else {
	    return;
	}
    }

   

	/**
	 * Carrega lista historico.
	 */
	public void carregaListaHistorico() {
	try {
	    ListarHistEmissaoAutCompEntradaDTO entradaDTO = new ListarHistEmissaoAutCompEntradaDTO();

	    ListarEmissaoAutCompSaidaDTO listarSaidaDTO = getListaPesquisa().get(getItemSelecionadoLista());

	    entradaDTO.setContratoPssaoJuridContr(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
		    .getCdPessoaJuridicaContrato());
	    entradaDTO.setContratoTpoContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
	    entradaDTO.setContratoSeqContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
	    entradaDTO.setServicoEmissaoContratado(getServicoEmissaoFiltro() != null ? getServicoEmissaoFiltro() : 0);
	    entradaDTO.setTipoServico(listarSaidaDTO.getCdTipoServico());
	    entradaDTO.setModalidadeServico(listarSaidaDTO.getCdModalidadeServico());
	    entradaDTO.setTipoRelacionamento(listarSaidaDTO.getTipoRelacionamento());
	    entradaDTO.setPeriodoInicialManutencao(getDataInicial() != null ? FormatarData.formataDiaMesAno(getDataInicial()) : "");
	    entradaDTO.setPeriodoFinalManutencao(getDataFinal() != null ? FormatarData.formataDiaMesAno(getDataFinal()) : "");

	    setListaPesquisaHistorico(getEmissaoAutCompServiceImpl().listarHistEmissaoAutComp(entradaDTO));
	    setBtoAcionadoHistorico(true);
	    listaPesquisaHistoricoControle = new ArrayList<SelectItem>();

	    for (int i = 0; i < listaPesquisaHistorico.size(); i++) {
		listaPesquisaHistoricoControle.add(new SelectItem(i, " "));
	    }

	    setItemSelecionadoListaHistorico(null);

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
	    setListaPesquisaHistorico(null);
	    setItemSelecionadoListaHistorico(null);
	}
    }

    /**
     * Confirmar incluir.
     *
     * @return the string
     */
    public String confirmarIncluir() {
	try {
	    IncluirEmissaoAutCompEntradaDTO entradaDTO = new IncluirEmissaoAutCompEntradaDTO();

	    entradaDTO.setContratoPssoaJuridContr(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
	    entradaDTO.setContratoSegContrNegoc(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
	    entradaDTO.setContratoTpoContrNegoc(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
	    entradaDTO.setServicoEmissaoContratado(getServicoEmissaoFiltro());
	    entradaDTO.setQuantidadeOcorrenciasEnviadasInc(listaPesquisaIncluir2.size());
	    entradaDTO.setListaIncluir(getListaPesquisaIncluir2());

	    IncluirEmissaoAutCompSaidaDTO saidaDTO = getEmissaoAutCompServiceImpl().incluirEmissaoAutComp(entradaDTO);

	    BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodigoMensagem() + ") " + saidaDTO.getMensagem(),
		    EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES, BradescoViewExceptionActionType.ACTION, false);
	    if (getServicoEmissaoFiltro() != null && getServicoEmissaoFiltro() != 0) {
		carregaLista();
	    } else {
		setListaPesquisa(null);
		setItemSelecionadoLista(null);
	    }
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
	    return null;
	}
	return "";
    }

    /**
     * Confirmar excluir.
     *
     * @return the string
     */
    public String confirmarExcluir() {
	try {
	    ExcluirEmissaoAutCompEntradaDTO entradaDTO = new ExcluirEmissaoAutCompEntradaDTO();
	    ListarEmissaoAutCompSaidaDTO listarEmissaoAutCompSaidaDTO = listaPesquisa.get(getItemSelecionadoLista());

	    entradaDTO.setContratoPssoaJuridContr(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
	    entradaDTO.setContratoSegContrNegoc(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
	    entradaDTO.setContratoTpoContrNegoc(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
	    entradaDTO.setServicoEmissaoContratado(getServicoEmissaoFiltro());
	    entradaDTO.setTipoServico(listarEmissaoAutCompSaidaDTO.getCdTipoServico());
	    entradaDTO.setModalidadeServico(listarEmissaoAutCompSaidaDTO.getCdModalidadeServico());
	    entradaDTO.setTipoRelacionamento(listarEmissaoAutCompSaidaDTO.getTipoRelacionamento());

	    ExcluirEmissaoAutCompSaidaDTO saidaDTO = getEmissaoAutCompServiceImpl().excluirEmissaoAutComp(entradaDTO);

	    BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
		    EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES, BradescoViewExceptionActionType.ACTION, false);
	    if (getServicoEmissaoFiltro() != null && getServicoEmissaoFiltro() != 0) {
		carregaLista();
	    } else {
		setListaPesquisa(null);
		setItemSelecionadoLista(null);
	    }
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
	    return null;
	}
	return "";
    }

    /**
     * Pesquisar.
     *
     * @param evt the evt
     */
    public void pesquisar(ActionEvent evt) {
	return;
    }

    
    /**
     * Voltar.
     *
     * @return the string
     */
    public String voltar(){
    	setItemSelecionadoLista(null);
    	return "VOLTAR";
    }

    /**
     * Voltar incluir.
     *
     * @return the string
     */
    public String voltarIncluir() {
	for (int i = 0; i < getListaPesquisaInclusao().size(); i++) {
	    getListaPesquisaInclusao().get(i).setCheck(false);
		}
	setSelecionarTodos(false);
	return "VOLTAR";
    }

    /**
     * Voltar historico.
     *
     * @return the string
     */
    public String voltarHistorico() {
	setItemSelecionadoListaHistorico(null);
	return "VOLTAR";
    }

    /**
     * Avancar incluir.
     *
     * @return the string
     */
    public String avancarIncluir() {
	if (this.btnAvancar) {
	    if (getObrigatoriedade().equals("V")) {
	    	setServicoEmissaoDesc((String) listaServicoEmissaoHash.get(getServicoEmissaoFiltro()));
	    	listaPesquisaIncluir2.clear();
	    	for (int i = 0; i < getListaPesquisaInclusao().size(); i++) {
	    		if (getListaPesquisaInclusao().get(i).isCheck()) {
	    			listaPesquisaIncluir2.add(getListaPesquisaInclusao().get(i));
	    		}
	    	}
	    	return "AVANCAR_INCLUIR";
	    }
	}
	return "";
    }

    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {
	try {
	    preencheDados();
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES, BradescoViewExceptionActionType.ACTION, false);
	    return null;
	}
	return "DETALHAR";
    }

    /**
     * Incluir.
     *
     * @return the string
     */
    public String incluir() {
	iniciarTelaIncluir();
	setServicoEmissao(null);
	setBtnAvancar(false);
	setListaPesquisaIncluir(null);
	setListaControleCheckIncluir(null);
	setSelecionarTodos(false);
	setBtoAcionado(false);
	setTipoServicoFiltroInclusao(0);
	setModalidadeServico(0);
	setModalidadeServicoFiltro(0);
	setModalidadeServico(0);
	setServicoEmissaoFiltro(0);
	listaPesquisaInclusao=new ArrayList<ListarTipoServicoModalidadeOcorrenciaSaidaDTO>();
	return "INCLUIR";
    }

    /**
     * Excluir.
     *
     * @return the string
     */
    public String excluir() {
	try {
	    preencheDados();
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES, BradescoViewExceptionActionType.ACTION, false);
	    return null;
	}
	return "EXCLUIR";
    }

    /**
     * Historico.
     *
     * @return the string
     */
    public String historico() {

	try {
	    preencheDados();
	    setDataFinal(new Date());
	    setDataInicial(new Date());
	    setListaPesquisaHistorico(null);
	    setItemSelecionadoListaHistorico(null);
	    setBtoAcionadoHistorico(false);

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    EMISSAO_AUTOMATICA_AVISOS_COMPROVANTES, BradescoViewExceptionActionType.ACTION, false);
	    return null;
	}

	return "HISTORICO";
    }

    /**
     * Detalhar historico.
     *
     * @return the string
     */
    public String detalharHistorico() {
	try {
	    preencheDadosHist();
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    "hisEmissaoAutomaticaAvisosComprovantes", BradescoViewExceptionActionType.ACTION, false);
	    return null;
	}
	return "DETALHAR_HIST";
    }

    /**
     * Preenche dados.
     */
    public void preencheDados() {

	ListarEmissaoAutCompSaidaDTO listarEmissaoAutCompSaidaDTO = getListaPesquisa().get(getItemSelecionadoLista());

	DetalharEmissaoAutCompEntradaDTO detalharEmissaoAutCompEntradaDTO = new DetalharEmissaoAutCompEntradaDTO();

	detalharEmissaoAutCompEntradaDTO.setContratoPssoaJuridContr(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
		.getCdPessoaJuridicaContrato());
	detalharEmissaoAutCompEntradaDTO.setContratoTpoContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
		.getCdTipoContratoNegocio());
	detalharEmissaoAutCompEntradaDTO.setContratoSegContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
		.getNrSeqContratoNegocio());
	detalharEmissaoAutCompEntradaDTO.setServicoEmissaoContratado(getServicoEmissaoFiltro());
	detalharEmissaoAutCompEntradaDTO.setTipoServico(listarEmissaoAutCompSaidaDTO.getCdTipoServico());
	detalharEmissaoAutCompEntradaDTO.setModalidadeServico(listarEmissaoAutCompSaidaDTO.getCdModalidadeServico());
	detalharEmissaoAutCompEntradaDTO.setTipoRelacionamento(listarEmissaoAutCompSaidaDTO.getTipoRelacionamento());
	// detalharEmissaoAutCompEntradaDTO.setCdSituacaoVinculacaoOperacao(listarEmissaoAutCompSaidaDTO.getCdSituacaoVinculacaoOperacao());
        // Removido do PDC

	DetalharEmissaoAutCompSaidaDTO detalharEmissaoAutCompSaidaDTO = getEmissaoAutCompServiceImpl().detalharEmissaoAutComp(
		detalharEmissaoAutCompEntradaDTO);

	setServicoEmissao(detalharEmissaoAutCompSaidaDTO.getCdServicoEmissaoContratado());
	setServicoEmissaoDesc(detalharEmissaoAutCompSaidaDTO.getDsServicoEmissaoContratado());
	setTipoServico(detalharEmissaoAutCompSaidaDTO.getCdTipoServico());
	setTipoServicoDesc(detalharEmissaoAutCompSaidaDTO.getDsTipoServico());
	setModalidadeServico(detalharEmissaoAutCompSaidaDTO.getCdModalidadeServico());
	setModalidadeServicoDesc(detalharEmissaoAutCompSaidaDTO.getDsModalidadeServico());
	setSituacaoVinculacao(detalharEmissaoAutCompSaidaDTO.getSituacaoVinculacao());
	setSituacaoVinculacaoDesc(listarEmissaoAutCompSaidaDTO.getDsSituacaoVinculacaoOperacao());
	setBloquearDesbloquear(String.valueOf(detalharEmissaoAutCompSaidaDTO.getSituacaoVinculacao()));

	setUsuarioInclusao(detalharEmissaoAutCompSaidaDTO.getUsuarioInclusao());
	setUsuarioManutencao(detalharEmissaoAutCompSaidaDTO.getUsuarioManutencao());
	setComplementoInclusao(detalharEmissaoAutCompSaidaDTO.getComplementoInclusao().equals("0") ? "" : detalharEmissaoAutCompSaidaDTO
		.getComplementoInclusao());
	setComplementoManutencao(detalharEmissaoAutCompSaidaDTO.getComplementoManutencao().equals("0") ? "" : detalharEmissaoAutCompSaidaDTO
		.getComplementoManutencao());
	setTipoCanalInclusao(detalharEmissaoAutCompSaidaDTO.getCdCanalInclusao() == 0 ? "" : detalharEmissaoAutCompSaidaDTO.getCdCanalInclusao()
		+ " - " + detalharEmissaoAutCompSaidaDTO.getDsCanalInclusao());
	setTipoCanalManutencao(detalharEmissaoAutCompSaidaDTO.getCdCanalManutencao() == 0 ? "" : detalharEmissaoAutCompSaidaDTO
		.getCdCanalManutencao()
		+ " - " + detalharEmissaoAutCompSaidaDTO.getDsCanalManutencao());
	setDataHoraInclusao(detalharEmissaoAutCompSaidaDTO.getDataHoraInclusao());
	setDataHoraManutencao(detalharEmissaoAutCompSaidaDTO.getDataHoraManutencao());
    }

    /**
     * Preenche dados hist.
     */
    public void preencheDadosHist(){
		
		listarHistEmissaoAutCompSaidaDTO = getListaPesquisaHistorico().get(getItemSelecionadoListaHistorico());
		
		
		ListarEmissaoAutCompSaidaDTO listarEmissaoAutCompSaidaDTO = getListaPesquisa().get(getItemSelecionadoLista());
		
		DetalharHistEmissaoAutCompEntradaDTO detalharHistEmissaoAutCompEntradaDTO = new DetalharHistEmissaoAutCompEntradaDTO();
		
		detalharHistEmissaoAutCompEntradaDTO.setContratoPssaoJuridContr(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		detalharHistEmissaoAutCompEntradaDTO.setContratoTpoContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		detalharHistEmissaoAutCompEntradaDTO.setContratoSeqContrNegoc(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		detalharHistEmissaoAutCompEntradaDTO.setServicoEmissaoContratado(getServicoEmissaoFiltro());
		detalharHistEmissaoAutCompEntradaDTO.setTipoServico(listarEmissaoAutCompSaidaDTO.getCdTipoServico());
		detalharHistEmissaoAutCompEntradaDTO.setModalidadeServico(listarEmissaoAutCompSaidaDTO.getCdModalidadeServico());
		detalharHistEmissaoAutCompEntradaDTO.setTipoRelacionamento(listarEmissaoAutCompSaidaDTO.getTipoRelacionamento());
		detalharHistEmissaoAutCompEntradaDTO.setDataHoraInclusaoRegistroHistorico(listarHistEmissaoAutCompSaidaDTO.getDataManutencao());
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		detalharHistEmissaoAutCompEntradaDTO.setPeriodoFinalManutencao(sdf.format(getDataFinal()));
		detalharHistEmissaoAutCompEntradaDTO.setPeriodoInicialManutencao(sdf.format(getDataInicial()));
		
		DetalharHistEmissaoAutCompSaidaDTO detalharHistEmissaoAutCompSaidaDTO = getEmissaoAutCompServiceImpl().detalharHistEmissaoAutComp(detalharHistEmissaoAutCompEntradaDTO);
		
		setServicoEmissao(detalharHistEmissaoAutCompSaidaDTO.getCdServicoEmissaoContratado());
		setServicoEmissaoDesc(detalharHistEmissaoAutCompSaidaDTO.getDsServicoEmissaoContratado());
		setTipoServico(detalharHistEmissaoAutCompSaidaDTO.getCdTipoServico());
		setTipoServicoDesc(detalharHistEmissaoAutCompSaidaDTO.getDsTipoServico());
		setModalidadeServico(detalharHistEmissaoAutCompSaidaDTO.getCdModalidadeRelacionamento());
		setModalidadeServicoDesc(detalharHistEmissaoAutCompSaidaDTO.getDsModalidadeRelacionamento());
		setSituacaoVinculacaoHistorico(detalharHistEmissaoAutCompSaidaDTO.getSituacaoVinculacao());
		setResponsavel(listarHistEmissaoAutCompSaidaDTO.getResponsavel());
		setTipoManutencao(listarHistEmissaoAutCompSaidaDTO.getTipoManutencao());
		setDataManutencao(listarHistEmissaoAutCompSaidaDTO.getDataManutencaoDesc());
		setDsSituacaoVinc(detalharHistEmissaoAutCompSaidaDTO.getDsSituacaoVinculacao());
		setDsTipoManutencao(listarHistEmissaoAutCompSaidaDTO.getDescTipoManutencao());

		setUsuarioInclusao(detalharHistEmissaoAutCompSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharHistEmissaoAutCompSaidaDTO.getUsuarioManutencao());		
		setComplementoInclusao(detalharHistEmissaoAutCompSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharHistEmissaoAutCompSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharHistEmissaoAutCompSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharHistEmissaoAutCompSaidaDTO.getComplementoManutencao());		
		setTipoCanalInclusao(detalharHistEmissaoAutCompSaidaDTO.getCdCanalInclusao()==0? "" : detalharHistEmissaoAutCompSaidaDTO.getCdCanalInclusao() + " - " +  detalharHistEmissaoAutCompSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharHistEmissaoAutCompSaidaDTO.getCdCanalManutencao()==0? "" : detalharHistEmissaoAutCompSaidaDTO.getCdCanalManutencao() + " - " + detalharHistEmissaoAutCompSaidaDTO.getDsCanalManutencao());
		setDataHoraInclusao(detalharHistEmissaoAutCompSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharHistEmissaoAutCompSaidaDTO.getDataHoraManutencao());
	}

    // Fim Metodos !

    // Set�s Get�s
    /**
     * Get: listaServicoEmissao.
     *
     * @return listaServicoEmissao
     */
    public List<SelectItem> getListaServicoEmissao() {
	return listaServicoEmissao;
    }

    /**
     * Set: listaServicoEmissao.
     *
     * @param listaServicoEmissao the lista servico emissao
     */
    public void setListaServicoEmissao(List<SelectItem> listaServicoEmissao) {
	this.listaServicoEmissao = listaServicoEmissao;
    }

    /**
     * Get: servicoEmissaoFiltro.
     *
     * @return servicoEmissaoFiltro
     */
    public Integer getServicoEmissaoFiltro() {
	return servicoEmissaoFiltro;
    }

    /**
     * Set: servicoEmissaoFiltro.
     *
     * @param servicoEmissaoFiltro the servico emissao filtro
     */
    public void setServicoEmissaoFiltro(Integer servicoEmissaoFiltro) {
	this.servicoEmissaoFiltro = servicoEmissaoFiltro;
    }

    /**
     * Get: listaModalidadeServico.
     *
     * @return listaModalidadeServico
     */
    public List<SelectItem> getListaModalidadeServico() {
	return listaModalidadeServico;
    }

    /**
     * Set: listaModalidadeServico.
     *
     * @param listaModalidadeServico the lista modalidade servico
     */
    public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
	this.listaModalidadeServico = listaModalidadeServico;
    }

    /**
     * Get: listaTipoServico.
     *
     * @return listaTipoServico
     */
    public List<SelectItem> getListaTipoServico() {
	return listaTipoServico;
    }

    /**
     * Set: listaTipoServico.
     *
     * @param listaTipoServico the lista tipo servico
     */
    public void setListaTipoServico(List<SelectItem> listaTipoServico) {
	this.listaTipoServico = listaTipoServico;
    }

    /**
     * Get: modalidadeServicoFiltro.
     *
     * @return modalidadeServicoFiltro
     */
    public Integer getModalidadeServicoFiltro() {
	return modalidadeServicoFiltro;
    }

    /**
     * Set: modalidadeServicoFiltro.
     *
     * @param modalidadeServicoFiltro the modalidade servico filtro
     */
    public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
	this.modalidadeServicoFiltro = modalidadeServicoFiltro;
    }

    /**
     * Get: tipoServicoFiltro.
     *
     * @return tipoServicoFiltro
     */
    public Integer getTipoServicoFiltro() {
	return tipoServicoFiltro;
    }

    /**
     * Set: tipoServicoFiltro.
     *
     * @param tipoServicoFiltro the tipo servico filtro
     */
    public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
	this.tipoServicoFiltro = tipoServicoFiltro;
    }

    /**
     * Get: itemSelecionadoLista.
     *
     * @return itemSelecionadoLista
     */
    public Integer getItemSelecionadoLista() {
	return itemSelecionadoLista;
    }

    /**
     * Set: itemSelecionadoLista.
     *
     * @param itemSelecionadoLista the item selecionado lista
     */
    public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
	this.itemSelecionadoLista = itemSelecionadoLista;
    }

    /**
     * Get: listaPesquisa.
     *
     * @return listaPesquisa
     */
    public List<ListarEmissaoAutCompSaidaDTO> getListaPesquisa() {
	return listaPesquisa;
    }

    /**
     * Set: listaPesquisa.
     *
     * @param listaPesquisa the lista pesquisa
     */
    public void setListaPesquisa(List<ListarEmissaoAutCompSaidaDTO> listaPesquisa) {
	this.listaPesquisa = listaPesquisa;
    }

    /**
     * Get: listaPesquisaControle.
     *
     * @return listaPesquisaControle
     */
    public List<SelectItem> getListaPesquisaControle() {
	return listaPesquisaControle;
    }

    /**
     * Set: listaPesquisaControle.
     *
     * @param listaPesquisaControle the lista pesquisa controle
     */
    public void setListaPesquisaControle(List<SelectItem> listaPesquisaControle) {
	this.listaPesquisaControle = listaPesquisaControle;
    }

    /**
     * Get: identificacaoClienteBean.
     *
     * @return identificacaoClienteBean
     */
    public IdentificacaoClienteBean getIdentificacaoClienteBean() {
	return identificacaoClienteBean;
    }

    /**
     * Set: identificacaoClienteBean.
     *
     * @param identificacaoClienteBean the identificacao cliente bean
     */
    public void setIdentificacaoClienteBean(IdentificacaoClienteBean identificacaoClienteBean) {
	this.identificacaoClienteBean = identificacaoClienteBean;
    }

    /**
     * Get: listaEmpresaGestora.
     *
     * @return listaEmpresaGestora
     */
    public List<SelectItem> getListaEmpresaGestora() {
	return listaEmpresaGestora;
    }

    /**
     * Set: listaEmpresaGestora.
     *
     * @param preencherListaEmpresaGestora the lista empresa gestora
     */
    public void setListaEmpresaGestora(List<SelectItem> preencherListaEmpresaGestora) {
	this.listaEmpresaGestora = preencherListaEmpresaGestora;
    }

    /**
     * Get: tipoContrato.
     *
     * @return tipoContrato
     */
    public List<SelectItem> getTipoContrato() {
	return tipoContrato;
    }

    /**
     * Set: tipoContrato.
     *
     * @param preencherTipoContrato the tipo contrato
     */
    public void setTipoContrato(List<SelectItem> preencherTipoContrato) {
	this.tipoContrato = preencherTipoContrato;
    }

    /**
     * Get: modalidadeServico.
     *
     * @return modalidadeServico
     */
    public Integer getModalidadeServico() {
	return modalidadeServico;
    }

    /**
     * Set: modalidadeServico.
     *
     * @param modalidadeServico the modalidade servico
     */
    public void setModalidadeServico(Integer modalidadeServico) {
	this.modalidadeServico = modalidadeServico;
    }

    /**
     * Get: servicoEmissao.
     *
     * @return servicoEmissao
     */
    public Integer getServicoEmissao() {
	return servicoEmissao;
    }

    /**
     * Set: servicoEmissao.
     *
     * @param servicoEmissao the servico emissao
     */
    public void setServicoEmissao(Integer servicoEmissao) {
	this.servicoEmissao = servicoEmissao;
    }

    /**
     * Get: tipoServico.
     *
     * @return tipoServico
     */
    public Integer getTipoServico() {
	return tipoServico;
    }

    /**
     * Set: tipoServico.
     *
     * @param tipoServico the tipo servico
     */
    public void setTipoServico(Integer tipoServico) {
	this.tipoServico = tipoServico;
    }

    /**
     * Get: listaControleCheckIncluir.
     *
     * @return listaControleCheckIncluir
     */
    public List<SelectItem> getListaControleCheckIncluir() {
	return listaControleCheckIncluir;
    }

    /**
     * Set: listaControleCheckIncluir.
     *
     * @param listaControleCheckIncluir the lista controle check incluir
     */
    public void setListaControleCheckIncluir(List<SelectItem> listaControleCheckIncluir) {
	this.listaControleCheckIncluir = listaControleCheckIncluir;
    }

    /**
     * Get: listaPesquisaIncluir.
     *
     * @return listaPesquisaIncluir
     */
    public List<ListarIncluirEmissaoAutCompSaidaDTO> getListaPesquisaIncluir() {
	return listaPesquisaIncluir;
    }

    /**
     * Set: listaPesquisaIncluir.
     *
     * @param listaPesquisaIncluir the lista pesquisa incluir
     */
    public void setListaPesquisaIncluir(List<ListarIncluirEmissaoAutCompSaidaDTO> listaPesquisaIncluir) {
	this.listaPesquisaIncluir = listaPesquisaIncluir;
    }

    /**
     * Is btn avancar.
     *
     * @return true, if is btn avancar
     */
    public boolean isBtnAvancar() {
	return btnAvancar;
    }

    /**
     * Set: btnAvancar.
     *
     * @param btnAvancar the btn avancar
     */
    public void setBtnAvancar(boolean btnAvancar) {
	this.btnAvancar = btnAvancar;
    }

    /**
     * Get: obrigatoriedade.
     *
     * @return obrigatoriedade
     */
    public String getObrigatoriedade() {
	return obrigatoriedade;
    }

    /**
     * Set: obrigatoriedade.
     *
     * @param obrigatoriedade the obrigatoriedade
     */
    public void setObrigatoriedade(String obrigatoriedade) {
	this.obrigatoriedade = obrigatoriedade;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
	return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
	this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
	return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
	this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
	return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
	this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
	return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
	this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
	return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
	this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
	return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
	this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
	return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
	this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
	return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
	this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: listaPesquisaHistorico.
     *
     * @return listaPesquisaHistorico
     */
    public List<ListarHistEmissaoAutCompSaidaDTO> getListaPesquisaHistorico() {
	return listaPesquisaHistorico;
    }

    /**
     * Set: listaPesquisaHistorico.
     *
     * @param listaPesquisaHistorico the lista pesquisa historico
     */
    public void setListaPesquisaHistorico(List<ListarHistEmissaoAutCompSaidaDTO> listaPesquisaHistorico) {
	this.listaPesquisaHistorico = listaPesquisaHistorico;
    }

    /**
     * Get: listaPesquisaHistoricoControle.
     *
     * @return listaPesquisaHistoricoControle
     */
    public List<SelectItem> getListaPesquisaHistoricoControle() {
	return listaPesquisaHistoricoControle;
    }

    /**
     * Set: listaPesquisaHistoricoControle.
     *
     * @param listaPesquisaHistoricoControle the lista pesquisa historico controle
     */
    public void setListaPesquisaHistoricoControle(List<SelectItem> listaPesquisaHistoricoControle) {
	this.listaPesquisaHistoricoControle = listaPesquisaHistoricoControle;
    }

    /**
     * Get: comboService.
     *
     * @return comboService
     */
    public IComboService getComboService() {
	return comboService;
    }

    /**
     * Set: comboService.
     *
     * @param comboService the combo service
     */
    public void setComboService(IComboService comboService) {
	this.comboService = comboService;
    }

    /**
     * Get: servicoEmissaoDesc.
     *
     * @return servicoEmissaoDesc
     */
    public String getServicoEmissaoDesc() {
	return servicoEmissaoDesc;
    }

    /**
     * Set: servicoEmissaoDesc.
     *
     * @param servicoEmissaoDesc the servico emissao desc
     */
    public void setServicoEmissaoDesc(String servicoEmissaoDesc) {
	this.servicoEmissaoDesc = servicoEmissaoDesc;
    }

    /**
     * Get: itemSelecionadoListaHistorico.
     *
     * @return itemSelecionadoListaHistorico
     */
    public Integer getItemSelecionadoListaHistorico() {
	return itemSelecionadoListaHistorico;
    }

    /**
     * Set: itemSelecionadoListaHistorico.
     *
     * @param itemSelecionadoListaHistorico the item selecionado lista historico
     */
    public void setItemSelecionadoListaHistorico(Integer itemSelecionadoListaHistorico) {
	this.itemSelecionadoListaHistorico = itemSelecionadoListaHistorico;
    }

    /**
     * Get: listaModalidadeServicoFiltro.
     *
     * @return listaModalidadeServicoFiltro
     */
    public List<SelectItem> getListaModalidadeServicoFiltro() {
	return listaModalidadeServicoFiltro;
    }

    /**
     * Set: listaModalidadeServicoFiltro.
     *
     * @param listaModalidadeServicoFiltro the lista modalidade servico filtro
     */
    public void setListaModalidadeServicoFiltro(List<SelectItem> listaModalidadeServicoFiltro) {
	this.listaModalidadeServicoFiltro = listaModalidadeServicoFiltro;
    }

    /**
     * Get: listaTipoServicoFiltro.
     *
     * @return listaTipoServicoFiltro
     */
    public List<SelectItem> getListaTipoServicoFiltro() {
	return listaTipoServicoFiltro;
    }

    /**
     * Set: listaTipoServicoFiltro.
     *
     * @param listaTipoServicoFiltro the lista tipo servico filtro
     */
    public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
	this.listaTipoServicoFiltro = listaTipoServicoFiltro;
    }

    /**
     * Get: modalidadeServicoDesc.
     *
     * @return modalidadeServicoDesc
     */
    public String getModalidadeServicoDesc() {
	return modalidadeServicoDesc;
    }

    /**
     * Set: modalidadeServicoDesc.
     *
     * @param modalidadeServicoDesc the modalidade servico desc
     */
    public void setModalidadeServicoDesc(String modalidadeServicoDesc) {
	this.modalidadeServicoDesc = modalidadeServicoDesc;
    }

    /**
     * Get: situacaoVinculacao.
     *
     * @return situacaoVinculacao
     */
    public int getSituacaoVinculacao() {
	return situacaoVinculacao;
    }

    /**
     * Set: situacaoVinculacao.
     *
     * @param situacaoVinculacao the situacao vinculacao
     */
    public void setSituacaoVinculacao(int situacaoVinculacao) {
	this.situacaoVinculacao = situacaoVinculacao;
    }

    /**
     * Get: tipoServicoDesc.
     *
     * @return tipoServicoDesc
     */
    public String getTipoServicoDesc() {
	return tipoServicoDesc;
    }

    /**
     * Set: tipoServicoDesc.
     *
     * @param tipoServicoDesc the tipo servico desc
     */
    public void setTipoServicoDesc(String tipoServicoDesc) {
	this.tipoServicoDesc = tipoServicoDesc;
    }

    /**
     * Get: dataManutencao.
     *
     * @return dataManutencao
     */
    public String getDataManutencao() {
	return dataManutencao;
    }

    /**
     * Set: dataManutencao.
     *
     * @param dataManutencao the data manutencao
     */
    public void setDataManutencao(String dataManutencao) {
	this.dataManutencao = dataManutencao;
    }

    /**
     * Get: responsavel.
     *
     * @return responsavel
     */
    public String getResponsavel() {
	return responsavel;
    }

    /**
     * Set: responsavel.
     *
     * @param responsavel the responsavel
     */
    public void setResponsavel(String responsavel) {
	this.responsavel = responsavel;
    }

    /**
     * Get: tipoManutencao.
     *
     * @return tipoManutencao
     */
    public int getTipoManutencao() {
	return tipoManutencao;
    }

    /**
     * Set: tipoManutencao.
     *
     * @param tipoManutencao the tipo manutencao
     */
    public void setTipoManutencao(int tipoManutencao) {
	this.tipoManutencao = tipoManutencao;
    }

    /**
     * Get: emissaoAutCompServiceImpl.
     *
     * @return emissaoAutCompServiceImpl
     */
    public IEmissaoAutCompService getEmissaoAutCompServiceImpl() {
	return emissaoAutCompServiceImpl;
    }

    /**
     * Set: emissaoAutCompServiceImpl.
     *
     * @param emissaoAutCompServiceImpl the emissao aut comp service impl
     */
    public void setEmissaoAutCompServiceImpl(IEmissaoAutCompService emissaoAutCompServiceImpl) {
	this.emissaoAutCompServiceImpl = emissaoAutCompServiceImpl;
    }

     /**
      * Get: listaModalidadeServicoHash.
      *
      * @return listaModalidadeServicoHash
      */
     Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
	return listaModalidadeServicoHash;
    }

    /**
     * Set lista modalidade servico hash.
     *
     * @param listaModalidadeServicoHash the lista modalidade servico hash
     */
    void setListaModalidadeServicoHash(Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
	this.listaModalidadeServicoHash = listaModalidadeServicoHash;
    }

    /**
     * Get: listaTipoServicoFiltroHash.
     *
     * @return listaTipoServicoFiltroHash
     */
    Map<Integer, String> getListaTipoServicoFiltroHash() {
	return listaTipoServicoFiltroHash;
    }

    /**
     * Set lista tipo servico filtro hash.
     *
     * @param listaTipoServicoFiltroHash the lista tipo servico filtro hash
     */
    void setListaTipoServicoFiltroHash(Map<Integer, String> listaTipoServicoFiltroHash) {
	this.listaTipoServicoFiltroHash = listaTipoServicoFiltroHash;
    }

    /**
     * Get: listaTipoServicoHash.
     *
     * @return listaTipoServicoHash
     */
    Map<Integer, String> getListaTipoServicoHash() {
	return listaTipoServicoHash;
    }

    /**
     * Set lista tipo servico hash.
     *
     * @param listaTipoServicoHash the lista tipo servico hash
     */
    void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
	this.listaTipoServicoHash = listaTipoServicoHash;
    }

    /**
     * Get: listaServicoEmissaoHash.
     *
     * @return listaServicoEmissaoHash
     */
    Map<Integer, String> getListaServicoEmissaoHash() {
	return listaServicoEmissaoHash;
    }

    /**
     * Set lista servico emissao hash.
     *
     * @param listaServicoEmissaoHash the lista servico emissao hash
     */
    void setListaServicoEmissaoHash(Map<Integer, String> listaServicoEmissaoHash) {
	this.listaServicoEmissaoHash = listaServicoEmissaoHash;
    }

    /**
     * Get: dataFinal.
     *
     * @return dataFinal
     */
    public Date getDataFinal() {
	return dataFinal;
    }

    /**
     * Set: dataFinal.
     *
     * @param dataFinal the data final
     */
    public void setDataFinal(Date dataFinal) {
    	this.dataFinal = dataFinal;
    }

    /**
     * Get: dataInicial.
     *
     * @return dataInicial
     */
    public Date getDataInicial() {
    	return dataInicial;
    }

    /**
     * Set: dataInicial.
     *
     * @param dataInicial the data inicial
     */
    public void setDataInicial(Date dataInicial) {
    	this.dataInicial = dataInicial;
    }

   
    /**
     * Get: btoAcionado.
     *
     * @return btoAcionado
     */
    public Boolean getBtoAcionado() {
    	return btoAcionado;
    }

    /**
     * Set: btoAcionado.
     *
     * @param btoAcionado the bto acionado
     */
    public void setBtoAcionado(Boolean btoAcionado) {
    	this.btoAcionado = btoAcionado;
    }

    /**
     * Get: btoAcionadoHistorico.
     *
     * @return btoAcionadoHistorico
     */
    public Boolean getBtoAcionadoHistorico() {
    	return btoAcionadoHistorico;
    }

    /**
     * Set: btoAcionadoHistorico.
     *
     * @param btoAcionadoHistorico the bto acionado historico
     */
    public void setBtoAcionadoHistorico(Boolean btoAcionadoHistorico) {
    	this.btoAcionadoHistorico = btoAcionadoHistorico;
    }

    /**
     * Is selecionar todos.
     *
     * @return true, if is selecionar todos
     */
    public boolean isSelecionarTodos() {
    	return selecionarTodos;
    }

    /**
     * Set: selecionarTodos.
     *
     * @param selecionarTodos the selecionar todos
     */
    public void setSelecionarTodos(boolean selecionarTodos) {
    	this.selecionarTodos = selecionarTodos;
    }

    /**
     * Get: saidaDetalharEmissao.
     *
     * @return saidaDetalharEmissao
     */
    public DetalharEmissaoAutCompSaidaDTO getSaidaDetalharEmissao() {
    	return saidaDetalharEmissao;
    }

    /**
     * Set: saidaDetalharEmissao.
     *
     * @param saidaDetalharEmissao the saida detalhar emissao
     */
    public void setSaidaDetalharEmissao(DetalharEmissaoAutCompSaidaDTO saidaDetalharEmissao) {
    	this.saidaDetalharEmissao = saidaDetalharEmissao;
    }

    /**
     * Get: desabilitaExcluir.
     *
     * @return desabilitaExcluir
     */
    public Integer getDesabilitaExcluir() {
    	return desabilitaExcluir;
    }

    /**
     * Set: desabilitaExcluir.
     *
     * @param desabilitaExcluir the desabilita excluir
     */
    public void setDesabilitaExcluir(Integer desabilitaExcluir) {
    	this.desabilitaExcluir = desabilitaExcluir;
    }

    /**
     * Get: situacaoVinculacaoHistorico.
     *
     * @return situacaoVinculacaoHistorico
     */
    public int getSituacaoVinculacaoHistorico() {
    	return situacaoVinculacaoHistorico;
    }

    /**
     * Set: situacaoVinculacaoHistorico.
     *
     * @param situacaoVinculacaoHistorico the situacao vinculacao historico
     */
    public void setSituacaoVinculacaoHistorico(int situacaoVinculacaoHistorico) {
    	this.situacaoVinculacaoHistorico = situacaoVinculacaoHistorico;
    }

    /**
     * Get: bloquearDesbloquear.
     *
     * @return bloquearDesbloquear
     */
    public String getBloquearDesbloquear() {
    	return bloquearDesbloquear;
    }

    /**
     * Set: bloquearDesbloquear.
     *
     * @param bloquearDesbloquear the bloquear desbloquear
     */
    public void setBloquearDesbloquear(String bloquearDesbloquear) {
    	this.bloquearDesbloquear = bloquearDesbloquear;
    }

    /**
     * Get: situacaoVinculacaoDesc.
     *
     * @return situacaoVinculacaoDesc
     */
    public String getSituacaoVinculacaoDesc() {
    	return situacaoVinculacaoDesc;
    }

    /**
     * Set: situacaoVinculacaoDesc.
     *
     * @param situacaoVinculacaoDesc the situacao vinculacao desc
     */
    public void setSituacaoVinculacaoDesc(String situacaoVinculacaoDesc) {
    	this.situacaoVinculacaoDesc = situacaoVinculacaoDesc;
    }

    /**
     * Get: dsSituacaoVinc.
     *
     * @return dsSituacaoVinc
     */
    public String getDsSituacaoVinc() {
    	return dsSituacaoVinc;
    }

    /**
     * Set: dsSituacaoVinc.
     *
     * @param dsSituacaoVinc the ds situacao vinc
     */
    public void setDsSituacaoVinc(String dsSituacaoVinc) {
    	this.dsSituacaoVinc = dsSituacaoVinc;
    }

    /**
     * Get: dsTipoManutencao.
     *
     * @return dsTipoManutencao
     */
    public String getDsTipoManutencao() {
    	return dsTipoManutencao;
    }

    /**
     * Set: dsTipoManutencao.
     *
     * @param dsTipoManutencao the ds tipo manutencao
     */
    public void setDsTipoManutencao(String dsTipoManutencao) {
    	this.dsTipoManutencao = dsTipoManutencao;
    }
    
    /**
     * Get: listarHistEmissaoAutCompSaidaDTO.
     *
     * @return listarHistEmissaoAutCompSaidaDTO
     */
    public ListarHistEmissaoAutCompSaidaDTO getListarHistEmissaoAutCompSaidaDTO() {
		return listarHistEmissaoAutCompSaidaDTO;
	}

	/**
	 * Set: listarHistEmissaoAutCompSaidaDTO.
	 *
	 * @param listarHistEmissaoAutCompSaidaDTO the listar hist emissao aut comp saida dto
	 */
	public void setListarHistEmissaoAutCompSaidaDTO(
			ListarHistEmissaoAutCompSaidaDTO listarHistEmissaoAutCompSaidaDTO) {
		this.listarHistEmissaoAutCompSaidaDTO = listarHistEmissaoAutCompSaidaDTO;
	}

	/**
	 * Get: listaTipoServicoContratoEntradaDTO.
	 *
	 * @return listaTipoServicoContratoEntradaDTO
	 */
	public ListaTipoServicoContratoEntradaDTO getListaTipoServicoContratoEntradaDTO() {
		return listaTipoServicoContratoEntradaDTO;
	}

	/**
	 * Set: listaTipoServicoContratoEntradaDTO.
	 *
	 * @param listaTipoServicoContratoEntradaDTO the lista tipo servico contrato entrada dto
	 */
	public void setListaTipoServicoContratoEntradaDTO(
			ListaTipoServicoContratoEntradaDTO listaTipoServicoContratoEntradaDTO) {
		this.listaTipoServicoContratoEntradaDTO = listaTipoServicoContratoEntradaDTO;
	}

	/**
	 * Get: listaTipoModalidade.
	 *
	 * @return listaTipoModalidade
	 */
	public List<SelectItem> getListaTipoModalidade() {
		return listaTipoModalidade;
	}

	/**
	 * Set: listaTipoModalidade.
	 *
	 * @param listaTipoModalidade the lista tipo modalidade
	 */
	public void setListaTipoModalidade(List<SelectItem> listaTipoModalidade) {
		this.listaTipoModalidade = listaTipoModalidade;
	}

	/**
	 * Get: listaTipoModalidadeHash.
	 *
	 * @return listaTipoModalidadeHash
	 */
	public Map<Integer, String> getListaTipoModalidadeHash() {
		return listaTipoModalidadeHash;
	}

	/**
	 * Set lista tipo modalidade hash.
	 *
	 * @param listaTipoModalidadeHash the lista tipo modalidade hash
	 */
	public void setListaTipoModalidadeHash(
			Map<Integer, String> listaTipoModalidadeHash) {
		this.listaTipoModalidadeHash = listaTipoModalidadeHash;
	}

	/**
	 * Get: listaPesquisaIncluir2.
	 *
	 * @return listaPesquisaIncluir2
	 */
	public List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> getListaPesquisaIncluir2() {
		return listaPesquisaIncluir2;
	}

	/**
	 * Set: listaPesquisaIncluir2.
	 *
	 * @param listaPesquisaIncluir2 the lista pesquisa incluir2
	 */
	public void setListaPesquisaIncluir2(
			List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listaPesquisaIncluir2) {
		this.listaPesquisaIncluir2 = listaPesquisaIncluir2;
	}

	/**
	 * Get: tipoServicoFiltroInclusao.
	 *
	 * @return tipoServicoFiltroInclusao
	 */
	public Integer getTipoServicoFiltroInclusao() {
		return tipoServicoFiltroInclusao;
	}

	/**
	 * Set: tipoServicoFiltroInclusao.
	 *
	 * @param tipoServicoFiltroInclusao the tipo servico filtro inclusao
	 */
	public void setTipoServicoFiltroInclusao(Integer tipoServicoFiltroInclusao) {
		this.tipoServicoFiltroInclusao = tipoServicoFiltroInclusao;
	}

	/**
	 * Get: listaPesquisaInclusao.
	 *
	 * @return listaPesquisaInclusao
	 */
	public List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> getListaPesquisaInclusao() {
		return listaPesquisaInclusao;
	}

	/**
	 * Set: listaPesquisaInclusao.
	 *
	 * @param listaPesquisaInclusao the lista pesquisa inclusao
	 */
	public void setListaPesquisaInclusao(
			List<ListarTipoServicoModalidadeOcorrenciaSaidaDTO> listaPesquisaInclusao) {
		this.listaPesquisaInclusao = listaPesquisaInclusao;
	}

	/**
	 * Get: listaModalidadeServicoFiltroHash.
	 *
	 * @return listaModalidadeServicoFiltroHash
	 */
	public Map<Integer, ListarServicoModalidadeEmissaoAvisoSaidaDTO> getListaModalidadeServicoFiltroHash() {
		return listaModalidadeServicoFiltroHash;
	}

	/**
	 * Set lista modalidade servico filtro hash.
	 *
	 * @param listaModalidadeServicoFiltroHash the lista modalidade servico filtro hash
	 */
	public void setListaModalidadeServicoFiltroHash(
			Map<Integer, ListarServicoModalidadeEmissaoAvisoSaidaDTO> listaModalidadeServicoFiltroHash) {
		this.listaModalidadeServicoFiltroHash = listaModalidadeServicoFiltroHash;
	}

	/**
	 * Get: listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.
	 *
	 * @return listarTipoServicoModalidadeEmissaoAvisoEntradaDTO
	 */
	public ListarServicoModalidadeEmissaoAvisoEntradaDTO getListarTipoServicoModalidadeEmissaoAvisoEntradaDTO() {
		return listarTipoServicoModalidadeEmissaoAvisoEntradaDTO;
	}

	/**
	 * Set: listarTipoServicoModalidadeEmissaoAvisoEntradaDTO.
	 *
	 * @param listarTipoServicoModalidadeEmissaoAvisoEntradaDTO the listar tipo servico modalidade emissao aviso entrada dto
	 */
	public void setListarTipoServicoModalidadeEmissaoAvisoEntradaDTO(
			ListarServicoModalidadeEmissaoAvisoEntradaDTO listarTipoServicoModalidadeEmissaoAvisoEntradaDTO) {
		this.listarTipoServicoModalidadeEmissaoAvisoEntradaDTO = listarTipoServicoModalidadeEmissaoAvisoEntradaDTO;
	}

	/**
	 * Get: listaModalidades.
	 *
	 * @return listaModalidades
	 */
	public List<ListarServicoModalidadeEmissaoAvisoSaidaDTO> getListaModalidades() {
		return listaModalidades;
	}

	/**
	 * Set: listaModalidades.
	 *
	 * @param listaModalidades the lista modalidades
	 */
	public void setListaModalidades(
			List<ListarServicoModalidadeEmissaoAvisoSaidaDTO> listaModalidades) {
		this.listaModalidades = listaModalidades;
	}

}
